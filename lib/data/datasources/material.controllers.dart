import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class MaterialListControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    lookupMaterialCategory();
    fetchMaterialData();
    super.onInit();
  }

  var hasMore = true.obs;
  var isLoading = false.obs;

  TextEditingController search = TextEditingController();
  GlobalKey<FormBuilderState> filterFormKey = GlobalKey<FormBuilderState>();

  // TextEditingController materialCategory = TextEditingController();
  var currentFilter = ({}).obs;
  var materialData = [].obs;

  Future fetchMaterialData() async {
    var filterValue = filterFormKey.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': search.text},
      {
        'key': 'material_category_id',
        'value': currentFilter['material_category_id']
      },
      {
        'key': 'active',
        'value': BaseGlobalFuntion.statusCheck(currentFilter['active'] ?? "")
      },
      {'key': 'sort', 'value': 'DESC'},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/material',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        materialData.addAll(listData);
        page++;
      } else {
        hasMore.value = false;
      }
    });
  }

  // RELOAD DATA EQUIPMENT
  Future refreshMaterialData() async {
    page = 0;
    hasMore.value = true;
    materialData.value = [];
    await fetchMaterialData();
  }

  var viewMaterialData = ({}).obs;
  Future fetchViewProduction(materialId) async {
    await ApiService.get(
      isAuth: true,
      endpoint: '/view/material/$materialId',
    ).then((response) {
      isLoading.value = false;
      final viewData = response.data['data'];

      if (viewData.isNotEmpty) {
        viewMaterialData.value = viewData;
      }
    });
  }

  // ** LOOKUP DATA */

  var extendLookupItem = {"id": "null", "name": "Semua"};

  var dataLookupMaterialCategory = [].obs;
  Future lookupMaterialCategory({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id'],
      },
    ]);

    await ApiService.get(
      endpoint: '/lookup/material-category',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      dataLookupMaterialCategory.value = [extendLookupItem, ...listData];
    });
  }
}
