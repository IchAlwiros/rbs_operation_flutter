import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class ProductionScheduleControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    super.onInit();
    lookupCustomer();
    lookupProductMutu();
    fetchProductionSchedule();
  }

  var hasMore = true.obs;
  var isLoading = false.obs;

  TextEditingController search = TextEditingController();

  GlobalKey<FormBuilderState> filterFormKey = GlobalKey<FormBuilderState>();

  var extendLookupItem = {"id": "null", "name": "Semua"};

  var byDate = ({}).obs;
  var currentFilter = ({}).obs;
  var productionScheduleData = [].obs;
  var summaryProductionSchedule = ({}).obs;

  Future fetchProductionSchedule() async {
    var filterValue = filterFormKey.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'status_code[]',
        'value':
            BaseGlobalFuntion.statusCheck(currentFilter['status_code'] ?? '')
      },
      {'key': 'search', 'value': search.text},
      {'key': 'sort', 'value': 'DESC'},
      // {'key': 'product_name', 'value': currentFilter['product_name']},
      {'key': 'customer_id', 'value': currentFilter['customer']},
      {'key': 'product_id', 'value': currentFilter['product']},
      {'key': 'production_date[]', 'value': byDate['start-date']},
      {'key': 'production_date[]', 'value': byDate['end-date']},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/production/production-schedule',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];
      final summaryData = response.data['data']['summary'];
      summaryProductionSchedule.value = {...summaryData};

      if (listData.isNotEmpty) {
        productionScheduleData.addAll(listData);
        page++;
      } else {
        hasMore.value = false;
      }
    });
  }

  Future filterByRangeDate({dynamic startDate, dynamic endDate}) async {
    if (startDate.isNotEmpty && endDate.isNotEmpty) {
      byDate.addAll({
        "start-date": startDate,
        "end-date": endDate,
      });
    } else {
      byDate.value = {};
    }
  }

  // RELOAD DATA EQUIPMENT
  Future refreshDataProductionSchedule() async {
    page = 0;
    hasMore.value = true;
    productionScheduleData.value = [];
    summaryProductionSchedule.value = {};
    await fetchProductionSchedule();
  }

  // ** DATA LOOKUP

  var dataCustomer = [].obs;
  Future lookupCustomer({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);
    await ApiService.get(
      endpoint: '/lookup/master/customer',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataCustomer.value = [extendLookupItem, ...listData];
      }
    });
  }

  var dataProductMutu = [].obs;
  Future lookupProductMutu({dynamic searching}) async {
    isLoading.value = true;
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);
    await ApiService.get(
      endpoint: '/lookup/product',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataProductMutu.value = listData;
      }
    });
  }
}

class ViewScheduleControllers extends GetxController {
  var isLoading = false.obs;
  GlobalKey<FormBuilderState> viewScheduleFormKey =
      GlobalKey<FormBuilderState>();

  var viewProductionScedule = ({}).obs;
  Future fetchViewProductionScedule(docketId) async {
    await ApiService.get(
      isAuth: true,
      endpoint: '/view/production/production-schedule/$docketId',
    ).then((response) {
      isLoading.value = false;
      final viewData = response.data['data'];

      if (viewData.isNotEmpty) {
        viewProductionScedule.value = viewData;
      }
    });
  }

  var datalookupProduction = ([]).obs;
  Future lookupProduction(scheduleId) async {
    isLoading.value = true;
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'production_schedule_id', 'value': scheduleId},
    ]);
    await ApiService.get(
      isAuth: true,
      endpoint: '/lookup/production',
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        datalookupProduction.value = listData;
      }
    });
  }

  //** ADJUST VOLUME */
  Future adujsVolume(
      {required BuildContext loadingCtx, dynamic extendData}) async {
    var scheduleProductionOrderValue = viewScheduleFormKey.currentState?.value;
    isLoading.value = true;
    if ((viewScheduleFormKey.currentState?.validate() ?? false)) {
      // try {
      var bodyAdjust = {
        'id': extendData['id'],
        ...scheduleProductionOrderValue!
      };

      await ApiService.patch(
        endpoint: "/adjust/production/production-schedule",
        isAuth: true,
        request: bodyAdjust,
      ).then((response) async {
        await BaseGlobalFuntion.loadingMoment(loadingCtx);
        isLoading.value = false;
        if (response != null) {
          BaseInfo.log(
            isError: false,
            messageTitle: "Berhasil",
            message: 'Adjust Volume Berhasil',
            snackPosition: SnackPosition.TOP,
          );
          isLoading.value = false;

          Get.close(1);
          Future.delayed(const Duration(milliseconds: 500), () {
            Get.put(ViewScheduleControllers())
                .fetchViewProductionScedule(extendData['id']);
          });
        }
      });
    } else {
      isLoading.value = false;
      BaseInfo.log(
        isError: true,
        messageTitle: "Validation Check",
        message: 'Periksa input yang dimasukan',
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  //** CANCEL JADWAL */
  Future cancelJadwal(
      {required BuildContext loadingCtx, dynamic extendData}) async {
    var scheduleProductionOrderValue = viewScheduleFormKey.currentState?.value;
    isLoading.value = true;
    if ((viewScheduleFormKey.currentState?.validate() ?? false)) {
      // try {
      var bodyCancel = {
        'id': extendData['id'],
        ...scheduleProductionOrderValue!
      };

      await ApiService.post(
        endpoint: "/cancel/production/production-schedule",
        isAuth: true,
        request: bodyCancel,
      ).then((response) async {
        await BaseGlobalFuntion.loadingMoment(loadingCtx);
        isLoading.value = false;
        if (response != null) {
          BaseInfo.log(
            isError: false,
            messageTitle: "Berhasil",
            message: 'Jadwal Dibatalkan',
            snackPosition: SnackPosition.TOP,
          );
          isLoading.value = false;

          Get.close(1);
          Future.delayed(const Duration(milliseconds: 500), () {
            Get.put(ViewScheduleControllers())
                .fetchViewProductionScedule(extendData['id']);
          });
        } else {
          Get.close(1);
        }
      });
    } else {
      isLoading.value = false;
      BaseInfo.log(
        isError: true,
        messageTitle: "Validation Check",
        message: 'Periksa input yang dimasukan',
        snackPosition: SnackPosition.TOP,
      );
    }
  }
}

class AddScheduleControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    super.onInit();
    lookupProductionUnit();
    lookupProdukMutu();
    lookupSalesOrder();
  }

  GlobalKey<FormBuilderState> kFormKey = GlobalKey<FormBuilderState>();
  var isLoading = false.obs;
  var hasMore = true.obs;
  var currentFilter = ({}).obs;
  GlobalKey<FormBuilderState> formKeyFilter = GlobalKey<FormBuilderState>();

  var dataLookupPU = [].obs;
  Future lookupProductionUnit({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([]);
    await ApiService.get(
      endpoint: '/lookup/production-unit',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataLookupPU.value = listData;
      }
    });
  }

  var dataLookupSO = [].obs;
  Future lookupSalesOrder({dynamic searching}) async {
    isLoading.value = true;

    var filterValue = formKeyFilter.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'product_id', 'value': currentFilter['product_id']},
      {'key': 'status_code[]', 'value': "PENDING"},
      {'key': 'status_code[]', 'value': "PROGRESS"},
      {'key': 'sort', 'value': 'DESC'},
    ]);

    await ApiService.get(
      endpoint: '/lookup/sales/sales-order',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataLookupSO.value = [...dataLookupSO, ...listData];
      }
    });
  }

  var detailSalesOrder = ({}).obs;
  Future viewSalesOrder({dynamic salesOrderId}) async {
    isLoading.value = true;
    await ApiService.get(
      endpoint: '/view/sales/sales-order/$salesOrderId',
      isAuth: true,
    ).then((response) {
      if (response != null) {
        isLoading.value = false;
        final viewData = response.data['data'];
        detailSalesOrder.value = viewData;
      }
    });
  }

  var dataSODetail = [].obs;
  Future lookupSODetail({dynamic soId}) async {
    isLoading.value = true;
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'sales_order_id',
        'value': soId,
      },
    ]);
    await ApiService.get(
      endpoint: '/lookup/sales/sales-order-detail',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataSODetail.value = listData;
      }
    });
  }

  var dataProdukMutu = [].obs;
  Future lookupProdukMutu({dynamic searching}) async {
    isLoading.value = true;

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      {'key': 'search', 'value': searching},
    ]);

    await ApiService.get(
      endpoint: '/lookup/product',
      isAuth: true,
      queryParams: paramsInclude,
      limit: 5,
      page: 0,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataProdukMutu.value = listData;
      }
    });
  }

  Future refreshDataSo() async {
    page = 0;
    hasMore.value = true;
    dataLookupSO.clear();
    await lookupSalesOrder();
  }

  Future refreshDataSODetail() async {
    dataSODetail.clear();
    await lookupSODetail();
  }

  // ** ADDED SCHEDULE

  var bodySchedule = ({}).obs;
  var summarySchedule = ({}).obs;
  var cartSchedule = ([]).obs;

  Future scheduleProductionOrder({dynamic extendData}) async {
    var scheduleProductionOrderValue = kFormKey.currentState?.value;

    if ((kFormKey.currentState?.validate() ?? false)) {
      //** INISIASI AWAL ISI PADA CART JIKA MASIH KOSONG

      cartSchedule.value = [
        ...cartSchedule,
        {
          "product_name": extendData['product_name'],
          "product_id": extendData['product_id'],
          "quantity": double.parse(scheduleProductionOrderValue?['quantity']),
          "date":
              "${scheduleProductionOrderValue?['date']} ${scheduleProductionOrderValue?['time'] ?? "00:00:00"}",
        }
      ];

      bodySchedule.value = {
        "production_unit_id": extendData['production_unit_id'],
        "sales_order_id": extendData['sales_order_id'],
        "production_schedule": cartSchedule,
      };

      double totalQuantity =
          cartSchedule.fold(0, (sum, item) => sum + (item['quantity'] ?? 0));

      summarySchedule.value = {
        'total_jadwal': cartSchedule.length,
        'total_quantity': totalQuantity,
      };
      // print("INI DISINI");
      // print(cartSchedule);
      // print(bodySchedule);
      // print(summarySchedule);
      Get.back();
    } else {
      debugPrint('validation failed');
    }
  }

  Future addScheduleProduction(
      {dynamic saveData, required BuildContext loadingCtx}) async {
    isLoading.value = true;
    await ApiService.post(
      endpoint: "/create/production/production-schedule",
      isAuth: true,
      request: saveData,
    ).then((response) async {
      await BaseGlobalFuntion.loadingMoment(loadingCtx);
      isLoading.value = false;
      if (response == null) {
        Get.close(1);
      }
      if (response != null) {
        Get.close(3);
        BaseInfo.log(
          isError: false,
          messageTitle: "Berhasil",
          message: 'Jadwal berhasil dibuat',
          snackPosition: SnackPosition.TOP,
        );

        // REFRESH DATA LIST
        Future.delayed(const Duration(milliseconds: 500), () {
          Get.put(ProductionScheduleControllers())
              .refreshDataProductionSchedule();
        });
      }
    });
    // try {
    //   var response = await ApiService.post(
    //     endpoint: "/create/production/production-schedule",
    //     isAuth: true,
    //     request: saveData,
    //   );
    //   if (response.statusCode == 200) {
    //     //** KEMBALI KE 3 PAGE SEBELUMNNYA
    //     // isLoading.value = false;
    //     Get.close(2);

    //     //** RETURNED SNACKBAR
    //     BaseInfo.log(
    //       isError: false,
    //       messageTitle: "success",
    //       message: response.data['data']['message'] ?? "Jadwal berhasil dibuat",
    //       snackPosition: SnackPosition.TOP,
    //     );

    //     isLoading.value = false;

    //     // REFRESH DATA LIST
    //     Future.delayed(const Duration(milliseconds: 500), () {
    //       Get.put(ProductionScheduleControllers())
    //           .refreshDataProductionSchedule();
    //     });
    //   } else {
    //     isLoading.value = false;
    //     response = response.data;
    //     BaseInfo.log(
    //       isError: true,
    //       messageTitle: "Error",
    //       message: response['error_message'],
    //       snackPosition: SnackPosition.TOP,
    //     );
    //   }
    // } on DioException catch (err) {
    //   //  Get.close(1);
    //   BaseInfo.log(
    //       isError: true,
    //       messageTitle: "Something Wrong!",
    //       message: '${err.response?.data['error_message']}',
    //       snackPosition: SnackPosition.TOP);
    //   Get.back();
    //   isLoading.value = false;
    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }
}
