import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class ProductListControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    fetchProduct();
    super.onInit();
  }

  var hasMore = true.obs;
  var isLoading = false.obs;

  TextEditingController search = TextEditingController();
  GlobalKey<FormBuilderState> filterFormKey = GlobalKey<FormBuilderState>();

  var currentFilter = ({}).obs;
  var productData = [].obs;

  Future fetchProduct() async {
    var filterValue = filterFormKey.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'active',
        'value': BaseGlobalFuntion.statusCheck(currentFilter['active'] ?? '')
      },
      {'key': 'search', 'value': search.text},
      {'key': 'sort', 'value': 'DESC'},
      {
        'key': 'product_category_id',
        'value': currentFilter['product_category_id']
      },
    ]);
    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/product',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        productData.addAll(listData);
        page++;
      } else {
        hasMore.value = false;
      }
    });

    // try {
    //   isLoading.value = true;
    //   var response = await ApiService.list(
    //     endpoint: '/list/product',
    //     isAuth: true,
    //     page: page,
    //     limit: limit,
    //     queryParams: paramsInclude,
    //   );
    //   if (response.statusCode == 200) {
    //     isLoading.value = false;
    //     final listData = response.data['data']['data'];

    //     if (listData.isNotEmpty) {
    //       productData.addAll(listData);
    //       page++;
    //     } else {
    //       hasMore.value = false;
    //     }
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );
    // }
  }

  // RELOAD DATA EQUIPMENT
  Future refreshDataProduct() async {
    page = 0;
    hasMore.value = true;
    productData.value = [];
    await fetchProduct();
  }
}

class DetailsProductControllers extends GetxController {
  var isLoading = false.obs;

  var detailProduct = ({}).obs;
  Future viewDetailProduct({
    required int productId,
    required bool isLoadData,
  }) async {
    isLoading.value = isLoadData;
    await ApiService.get(
      endpoint: '/view/product/$productId',
      isAuth: true,
    ).then((response) {
      isLoading.value = false;
      final viewData = response.data['data'];

      if (viewData.isNotEmpty) {
        detailProduct.value = viewData;
      }
    });
    // try {
    //   var response = await ApiService.view(
    //     endpoint: '/view/product/$productId',
    //     isAuth: true,
    //   );

    //   if (response.statusCode == 200) {
    //     isLoading.value = false;
    //     detailProduct.value = response.data['data'];
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );
    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }
}
