import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class ApprovalControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    fetchAproval();
    super.onInit();
  }

  var hasMore = true.obs;
  var isLoading = false.obs;

  // APPROVAL CONTROLLERS
  TextEditingController search = TextEditingController();
  GlobalKey<FormBuilderState> formKeyFilter = GlobalKey<FormBuilderState>();
  var currentFilter = ({}).obs;
  var byStatus = [].obs;
  var byRangeDate = ({}).obs;
  var approvalData = [].obs;
  // var byDate = ({}).obs;

  Future fetchAproval() async {
    var filterValue = formKeyFilter.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': search.text},
      {'key': 'sort', 'value': 'DESC'},
      {'key': 'date[]', 'value': byRangeDate['start-date']},
      {'key': 'date[]', 'value': byRangeDate['end-date']},
    ]);
    isLoading.value = true;
    await ApiService.get(
      endpoint: '/approval/submission/credit',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: [...paramsInclude, ...byStatus],
    ).then((response) {
      final listData = response.data['data']['data'];

      isLoading.value = false;
      if (listData.isNotEmpty) {
        approvalData.addAll(listData);
        page++;
      } else {
        hasMore.value = false;
      }
    });
  }

  var detailApprovalData = ({}).obs;
  Future detailAproval({dynamic approvalId}) async {
    var filterValue = formKeyFilter.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    // final paramsInclude = BaseGlobalFuntion.paramsInclude([
    // {'key': 'search', 'value': search.text},
    // {'key': 'sort', 'value': 'DESC'},
    // {'key': 'date[]', 'value': byDate['start-date']},
    // {'key': 'date[]', 'value': byDate['end-date']},
    // ]);
    isLoading.value = true;
    await ApiService.get(
      endpoint: '/view/submission/credit',
      isAuth: true,
      pathParams: approvalId,
      // queryParams: [...paramsInclude, ...byStatus, ...byRangeDate],
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final viewData = response.data['data'];
        print(viewData);
        detailApprovalData.value = viewData;
      }
    });
  }

  // RELOAD DATA EQUIPMENT
  Future refreshDataApproval() async {
    page = 0;
    hasMore.value = true;
    approvalData.value = [];
    await fetchAproval();
  }

  // FILTER BY STATUS
  Future filterByStatus(List<String> statusCode) async {
    if (statusCode.isNotEmpty) {
      byStatus.value = statusCode;
    } else {
      byStatus.value = [];
    }
    await refreshDataApproval();
  }

  // FILTER BY RANGE DATE
  Future filterByRangeDate({dynamic startDate, dynamic endDate}) async {
    if (startDate.isNotEmpty && endDate.isNotEmpty) {
      byRangeDate.value = {
        "start-date": startDate,
        "end-date": endDate,
      };
    } else {
      byRangeDate.value = {};
    }
    await refreshDataApproval();
  }
}

class ApprovalDetailControllers extends GetxController {
  @override
  void onInit() {
    // fetchAproval();
    // detailAproval();
    super.onInit();
  }

  var isLoading = false.obs;

  var detailApprovalData = ({}).obs;
  Future detailAproval({dynamic approvalId}) async {
    isLoading.value = true;
    await ApiService.get(
      endpoint: '/view/submission/credit',
      isAuth: true,
      pathParams: approvalId,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final viewData = response.data['data'];
        detailApprovalData.value = viewData;
      }
    });
  }

  Future actionAproval({dynamic approvalBody, required loadingCtx}) async {
    isLoading.value = true;

    await ApiService.post(
      endpoint: '/approval/submission/credit',
      isAuth: true,
      request: approvalBody,
    ).then((response) async {
      await BaseGlobalFuntion.loadingMoment(loadingCtx);
      isLoading.value = false;
      if (response != null) {
        Get.close(2);
        BaseInfo.log(
          isError: false,
          messageTitle: "Berhasil",
          message: 'Approval Sudah Di Respon',
          snackPosition: SnackPosition.TOP,
        );
        // REFRESH DATA LIST
        Future.delayed(const Duration(milliseconds: 500), () {
          Get.put(ApprovalControllers()).refreshDataApproval();
        });
      }
    });
  }
}

class KreditControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    fetchCredit();
    super.onInit();
  }

  var hasMore = true.obs;
  var isLoading = false.obs;

  // APPROVAL CONTROLLERS
  TextEditingController search = TextEditingController();
  var byStatus = [].obs;
  var byRangeDate = [].obs;
  var approvalData = [].obs;

  Future fetchCredit() async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': search.text},
      {'key': 'sort', 'value': 'DESC'},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/submission/credit',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: [...paramsInclude, ...byStatus, ...byRangeDate],
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        approvalData.addAll(listData);
        page++;
      } else {
        hasMore.value = false;
      }
    });

    // try {
    //   var response = await ApiService.list(
    //       endpoint: '/list/submission/credit',
    //       isAuth: true,
    //       page: page,
    //       limit: limit,
    //       queryParams: [
    //         ...paramsInclude,
    //         ...byStatus,
    //       ]);

    //   if (response.statusCode == 200) {
    //     isLoading.value = false;
    //     final listData = response.data['data']['data'];

    //     // if (kDebugMode) {
    //     //   print(response.data);
    //     // }

    //     if (listData.isNotEmpty) {
    //       approvalData.addAll(listData);
    //       page++;
    //     } else {
    //       hasMore.value = false;
    //     }
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );
    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }

  // RELOAD DATA EQUIPMENT
  Future refreshDataApproval() async {
    page = 0;
    hasMore.value = true;
    approvalData.value = [];
    await fetchCredit();
  }

  // FILTER BY STATUS
  Future filterByStatus(List<String> statusCode) async {
    if (statusCode.isNotEmpty) {
      byStatus.value = statusCode;
    } else {
      byStatus.value = [];
    }
    await refreshDataApproval();
  }

  // FILTER BY RANGE DATE
  Future filterByRangeDate({dynamic startDate, dynamic endDate}) async {
    if (startDate.isNotEmpty && endDate.isNotEmpty) {
      byRangeDate.value = ['date[]=$startDate', 'date[]=$endDate'];
    } else {
      byRangeDate.value = [];
    }
    await refreshDataApproval();
  }
}
