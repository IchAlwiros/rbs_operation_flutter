import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';

class CustomerListControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    super.onInit();
    fetchCustomer();
  }

  var hasMore = true.obs;
  var isLoading = false.obs;

  TextEditingController search = TextEditingController();
  GlobalKey<FormBuilderState> filterFormKey = GlobalKey<FormBuilderState>();

  var currentFilter = ({}).obs;
  var customerData = [].obs;

  Future fetchCustomer() async {
    var filterValue = filterFormKey.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'active',
        'value': BaseGlobalFuntion.statusCheck(currentFilter['status'] ?? '')
      },
      {'key': 'search', 'value': search.text},
      {'key': 'sort', 'value': 'DESC'},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/master/customer',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        if (listData.isNotEmpty) {
          customerData.addAll(listData);
          page++;
        } else {
          hasMore.value = false;
        }
      }
    });
  }

  // RELOAD
  Future refreshDataCustomer() async {
    page = 0;
    hasMore.value = true;
    customerData.value = [];
    await fetchCustomer();
  }
}

// ** DETAILS CUSTOMER
class DetailsCustomerControllers extends GetxController {
  var isLoading = false.obs;

  var viewCustomerData = ({}).obs;
  Future viewCustomer({dynamic customerId}) async {
    isLoading.value = true;
    await ApiService.get(
      endpoint: '/view/master/customer',
      pathParams: "$customerId",
      isAuth: true,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final viewData = response.data['data'];
        viewCustomerData.value = viewData;
      }
    });
  }

  var customerProject = [].obs;
  Future viewCustomerProject({dynamic customerId}) async {
    customerProject.value = [];
    isLoading.value = true;
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'customer_id', 'value': customerId},
      {'key': 'sort', 'value': 'DESC'},
    ]);

    await ApiService.get(
      endpoint: '/list/master/customer-project',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        customerProject.value = listData;
      }
    });
  }

  Future refreshDataProyek(customerId) async {
    customerProject.value = [];
    await viewCustomerProject(customerId: customerId);
  }
}

// ** ADD CUSTOMER
class AddCustomerControllers extends GetxController {
  @override
  void onInit() {
    lookupProductionUnit();

    if (ApiService.productionUnitGlobal != null ||
        LocalDataSource.getLocalVariable(key: 'lastestProduction') != null) {
      lookupCustomerCategory();
    }

    super.onInit();
  }

  var currentStep = 0.obs;
  // var dataPreview = ({}).obs;
  var dataCustomerTemp = ({}).obs;
  // var dataMapCustomerHelper = ({}).obs;
  var isLoading = false.obs;
  final GlobalKey<FormBuilderState> fkAddCustomer =
      GlobalKey<FormBuilderState>();
  final GlobalKey<FormBuilderState> fkAddProyek = GlobalKey<FormBuilderState>();

  var currentFilter = ({}).obs;
  TextEditingController productionUnitController = TextEditingController();
  TextEditingController customerTypeController = TextEditingController();
  // TextEditingController detailAddressControllers = TextEditingController();

  void addCustomer({dynamic nextStep, required dynamic addressMap}) {
    var formValue = fkAddCustomer.currentState?.value;

    if (fkAddCustomer.currentState?.validate() ?? false) {
      dataCustomerTemp.value = {
        "production_unit_id": formValue!['production_unit_id'] ?? "",
        "customer_category_id": formValue['customer_category_id'] ?? "",
        "name": formValue['customer_name'] ?? "",
        "phone": formValue['phone'] ?? "",
        "email": formValue['email'] ?? "",
        "address": formValue['address'],
        ...addressMap,
        "active": "1",
        "access_payment": formValue['payment_type_code'].map((metode) {
          return {'code': metode, 'allowed': true};
        }).toList(),
      };
      // Get.to(() => const AddProyekCustomer());
      // NEXT STEP CURRENT STEP + 1 IF VALIDATE;
      nextStep();
    } else {
      BaseInfo.log(
        isError: true,
        messageTitle: "Validation Check",
        message: 'Periksa input yang dimasukan',
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  var dataProyekTemp = ([]).obs;
  TextEditingController addressProjectControllers = TextEditingController();
  Future addCustomerProject({required dynamic addressMap}) async {
    var formValue = fkAddProyek.currentState?.value;

    if (fkAddProyek.currentState?.validate() ?? false) {
      isLoading.value = true;
      dataProyekTemp.value = [
        ...dataProyekTemp,
        {
          "name": formValue!['customer_proyek'] ?? "",
          "pic_name": formValue['pic_proyek'] ?? "",
          "pic_phone": formValue['pic_phone'] ?? "",
          "address": formValue['proyek_address'] ?? "",
          ...addressMap,
        }
      ];
      Get.back();
      Future.delayed(const Duration(microseconds: 600), () {
        isLoading.value = false;
        // BaseInfo.log(
        //   isError: false,
        //   messageTitle: "Ditambahkan",
        //   message: "Proyek ditambahkan",
        //   snackPosition: SnackPosition.TOP,
        // );

        fkAddProyek.currentState?.reset();
        addressProjectControllers.clear();
      });
    } else {
      BaseInfo.log(
        isError: true,
        messageTitle: "Validation Check",
        message: 'Periksa input yang dimasukan',
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  Future addDataCustomer({required BuildContext loadingCtx}) async {
    // if (fkAddProyek.currentState?.validate() ?? false) {
    isLoading.value = true;
    final body = {
      ...dataCustomerTemp,
      "customer_project": dataProyekTemp,
    };

    await ApiService.post(
      endpoint: "/create/master/customer",
      isAuth: true,
      request: body,
    ).then((response) async {
      await BaseGlobalFuntion.loadingMoment(loadingCtx);
      isLoading.value = false;
      if (response != null) {
        isLoading.value = false;
        Get.close(1);
        BaseInfo.log(
          isError: false,
          messageTitle: "Berhasil",
          message: "Pelanggan ditambahkan",
          snackPosition: SnackPosition.TOP,
        );

        // REFRESH DATA LIST
        Future.delayed(const Duration(milliseconds: 500), () {
          Get.put(CustomerListControllers()).refreshDataCustomer();
        });
      }
    });
    // try {

    //   var response = await ApiService.post(
    //     endpoint: "/create/master/customer",
    //     isAuth: true,
    //     request: body,
    //   );
    //   if (response.statusCode == 200) {
    //     isLoading.value = false;
    //     Get.close(2);
    //     await BaseGlobalFuntion.loadingMoment(loadingCtx);
    //     BaseInfo.log(
    //       isError: false,
    //       messageTitle: "Berhasil",
    //       message: "Pelanggan ditambahkan",
    //       snackPosition: SnackPosition.TOP,
    //     );

    //     // REFRESH DATA LIST
    //     Future.delayed(const Duration(milliseconds: 500), () {
    //       Get.put(CustomerListControllers()).refreshDataCustomer();
    //     });
    //   } else {
    //     isLoading.value = false;
    //     response = response.data;
    //     BaseInfo.log(
    //       isError: true,
    //       messageTitle: "Error",
    //       message: response['error_message'],
    //       snackPosition: SnackPosition.TOP,
    //     );
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );
    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }

  // var indexEdit = ({}).obs;
  // Future editDataProyek({dynamic dataIndex}) async {
  //   print(fkAddProyek.currentState!.value);
  //   print(dataProyekTemp[indexEdit['index']]);
  // }

  // Future helperMaps(
  //     {dynamic lat, dynamic lng, TextEditingController? addressPreview}) async {
  //   try {
  //     List<Placemark> placemarks = await placemarkFromCoordinates(lat, lng);

  //     addressPreview?.text =
  //         '${placemarks[0].street}, ${placemarks[0].subLocality},${placemarks[0].locality},${placemarks[0].subAdministrativeArea}, ${placemarks[0].administrativeArea}';

  //     var body = {
  //       "city_name": placemarks[0].subAdministrativeArea,
  //       "district_name": placemarks[0]
  //           .locality!
  //           .toLowerCase()
  //           .replaceAll(RegExp(r'kecamatan|kec\.|Kec'), '')
  //           .trimLeft(),
  //       "latitude": lat,
  //       "longitude": lng,
  //       "postal_code": placemarks[0].postalCode,
  //       "province_name": placemarks[0].administrativeArea,
  //       "sub_district_name": placemarks[0].subLocality,
  //     };

  //     var response = await ApiService.post(
  //       endpoint: "/helper/regional",
  //       isAuth: true,
  //       request: body,
  //     );

  //     final dataRegional = response.data['data'];

  //     dataMapCustomerHelper.value = {
  //       "latitude": lat,
  //       "longitude": lng,
  //       "province_id": dataRegional['province'] == null
  //           ? ""
  //           : dataRegional['province']['id'],
  //       "city_id":
  //           dataRegional['city'] == null ? "" : dataRegional['city']['id'],
  //       "district_id": dataRegional['district'] == null
  //           ? ""
  //           : dataRegional['district']['id'],
  //       "sub_district_id": dataRegional['sub_district'] == null
  //           ? ""
  //           : dataRegional['sub_district']['id'],
  //       "province_name": dataRegional['province'] == null
  //           ? placemarks[0].administrativeArea
  //           : dataRegional['province']['name'],
  //       "city_name": dataRegional['city'] == null
  //           ? placemarks[0].locality
  //           : dataRegional['city']['name'],
  //       "district_name": dataRegional['district'] == null
  //           ? placemarks[0].subLocality
  //           : dataRegional['district']['name'],
  //       "sub_district_name": dataRegional['sub_district'] == null
  //           ? placemarks[0].subLocality
  //           : dataRegional['sub_district']['name'],
  //     };
  //   } on DioException catch (err) {
  //     isLoading.value = false;
  //     BaseInfo.log(
  //       isError: true,
  //       messageTitle: "Something Wrong!",
  //       message: '${err.response?.data['error_message']}',
  //       snackPosition: SnackPosition.TOP,
  //     );
  //     if (err.response?.statusCode == 401) {
  //       await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
  //         'permissions',
  //         'token',
  //         'profile',
  //         'lastestProduction',
  //         'production_unit',
  //         'corporate',
  //       ]);
  //       Get.offAllNamed('/login');
  //     }
  //   }
  // }

  // ** DATA LOOKUP
  var dataLookupPU = [].obs;
  Future lookupProductionUnit({dynamic searching}) async {
    // try {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([]);

    await ApiService.get(
      endpoint: '/lookup/production-unit',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final lookupData = response.data['data']['data'];
        dataLookupPU.value = lookupData;
      }
    });
  }

  var dataLookupCustomerCategory = [].obs;
  Future lookupCustomerCategory({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      {'key': 'search', 'value': searching},
    ]);

    await ApiService.get(
      endpoint: '/lookup/customer-category',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final lookupData = response.data['data']['data'];
        dataLookupCustomerCategory.value = lookupData;
      }
    });
    // try {

    //   var response = await ApiService.list(
    //     endpoint: '/lookup/customer-category',
    //     isAuth: true,
    //     queryParams: paramsInclude,
    //   );

    //   if (response.statusCode == 200) {
    //     final listData = response.data['data']['data'];
    //     dataLookupCustomerCategory.value = listData;
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );
    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }
}

// ** EDIT CUSTOMER
class UpdateCustomerControllers extends GetxController {
  @override
  void onInit() {
    lookupProductionUnit();

    if (ApiService.productionUnitGlobal != null ||
        LocalDataSource.getLocalVariable(key: 'lastestProduction') != null) {
      lookupCustomerCategory();
    }

    super.onInit();
  }

  // var dataMapCustomerHelper = ({}).obs;
  var isLoading = false.obs;
  final GlobalKey<FormBuilderState> fkAddCustomer =
      GlobalKey<FormBuilderState>();
  final GlobalKey<FormBuilderState> fkAddProyek = GlobalKey<FormBuilderState>();

  var currentFilter = ({}).obs;
  TextEditingController productionUnitController = TextEditingController();
  TextEditingController customerTypeController = TextEditingController();
  // TextEditingController detailAddressControllers = TextEditingController();

  Future updateCustomer({
    dynamic extendData,
    required BuildContext loadingCtx,
    required dynamic addressMaps,
  }) async {
    var formValue = fkAddCustomer.currentState?.value;

    isLoading.value = true;
    if (fkAddCustomer.currentState?.validate() ?? false) {
      var locationId = {
        "latitude": addressMaps['latitude'] ?? extendData['latitude'],
        "longitude": addressMaps['longitude'] ?? extendData['longitude'],
        "province_id": addressMaps['province_id'] ?? extendData['province_id'],
        "city_id": addressMaps['city_id'] ?? extendData['city_id'],
        "district_id": addressMaps['district_id'] ?? extendData['district_id'],
        "sub_district_id":
            addressMaps['sub_district_id'] ?? extendData['sub_district_id'],
      };
      final paymentMethod =
          (formValue!['payment_type_code'] ?? extendData['access_payment'])
              .map((metode) {
                return metode is String
                    ? {'code': metode, 'allowed': true}
                    : {'code': metode['code'], 'allowed': metode['allowed']};
              })
              .where((item) => item["code"] != "")
              .toList();

      var body = {
        "id": extendData['id'],
        "production_unit_id":
            formValue['production_unit_id'] ?? extendData['production_unit_id'],
        "customer_category_id": formValue['customer_category_id'] ??
            extendData['customer_category_id'],
        "name": formValue['customer_name'] ?? extendData['name'],
        "phone": formValue['phone'] ?? extendData['phone'],
        "email": formValue['email'] ?? extendData['email'],
        "address": formValue['address'] ?? extendData['address'],
        ...locationId,
        "active": "1",
        "access_payment": paymentMethod
      };

      await ApiService.patch(
        endpoint: "/update/master/customer",
        isAuth: true,
        request: body,
      ).then((response) async {
        await BaseGlobalFuntion.loadingMoment(loadingCtx);
        isLoading.value = false;
        if (response != null) {
          Get.close(2);
          BaseInfo.log(
            isError: false,
            messageTitle: "Berhasil",
            message: 'Data pelanggan berhasil diubah',
            snackPosition: SnackPosition.TOP,
          );

          // REFRESH DATA LIST
          Future.delayed(const Duration(milliseconds: 500), () {
            Get.put(CustomerListControllers()).refreshDataCustomer();
          });
        }
      });
    } else {
      isLoading.value = false;
      BaseInfo.log(
        isError: true,
        messageTitle: "Validation Check",
        message: 'Periksa input yang dimasukan',
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  // ** DATA LOOKUP
  var dataLookupPU = [].obs;
  Future lookupProductionUnit({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([]);
    await ApiService.get(
      endpoint: '/lookup/production-unit',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final lookupData = response.data['data']['data'];
        dataLookupPU.value = lookupData;
      }
    });
  }

  var dataLookupCustomerCategory = [].obs;
  Future lookupCustomerCategory({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      {'key': 'search', 'value': searching},
    ]);

    await ApiService.get(
      endpoint: '/lookup/customer-category',
      isAuth: true,
      queryParams: paramsInclude,
      page: 0,
      limit: 5,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupCustomerCategory.value = listData;
      }
    });
  }
}

// ** ADD PROJECT
class AddCustomerProjectControllers extends GetxController {
  // @override
  // void onInit() {
  //   super.onInit();
  //   // lookupProductionUnit();
  // }

  var isLoading = false.obs;
  final GlobalKey<FormBuilderState> fkAddProject =
      GlobalKey<FormBuilderState>();

  Future addProject({
    required BuildContext loadingCtx,
    dynamic detailCustomer,
    required dynamic addressMaps,
  }) async {
    var formValue = fkAddProject.currentState?.value;

    isLoading.value = true;
    if (fkAddProject.currentState?.validate() ?? false) {
      final body = {
        'production_unit_id': detailCustomer['production_unit_id'],
        'customer_id': detailCustomer['id'],
        'name': formValue!['project_name'] ?? '',
        'pic_name': formValue['project_pic'] ?? '',
        'pic_phone': formValue['phone'] ?? '',
        'address': formValue['address'] ?? '',
        'active': formValue['active'] ?? '',
        ...addressMaps,
      };

      await ApiService.post(
        endpoint: "/create/master/customer-project",
        isAuth: true,
        request: body,
      ).then((response) async {
        await BaseGlobalFuntion.loadingMoment(loadingCtx);
        isLoading.value = false;
        if (response != null) {
          Get.close(1);
          BaseInfo.log(
            isError: false,
            messageTitle: "Berhasil",
            message: 'Proyek berhasil ditambahkan',
            snackPosition: SnackPosition.TOP,
          );

          // REFRESH DATA LIST
          Future.delayed(const Duration(milliseconds: 500), () async {
            final detailProyek = Get.put(DetailsCustomerControllers());

            await detailProyek.refreshDataProyek(detailCustomer['id']);
          });
        }
      });
    } else {
      isLoading.value = false;
      BaseInfo.log(
        isError: true,
        messageTitle: "Validation Check",
        message: 'Periksa input yang dimasukan',
        snackPosition: SnackPosition.TOP,
      );
    }
  }
}
