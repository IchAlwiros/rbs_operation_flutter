import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class ForgotPwdControllers extends GetxController {
  final GlobalKey<FormBuilderState> fkForgotPwd = GlobalKey<FormBuilderState>();
  var isLoading = false.obs;

  Future sendEmailVerification(
      {dynamic extendData, required BuildContext loadingCtx}) async {
    var formValue = fkForgotPwd.currentState?.value;

    isLoading.value = true;
    if (fkForgotPwd.currentState?.validate() ?? false) {
      await ApiService.post(
        endpoint: "/password/forgot",
        isAuth: false,
        request: formValue!,
      ).then((response) async {
        await BaseGlobalFuntion.loadingMoment(loadingCtx);
        isLoading.value = false;

        if (response != null) {
          Get.close(1);
          BaseInfo.log(
            isError: false,
            messageTitle: "Berhasil",
            message: 'verifikasi email telah dikirim',
            snackPosition: SnackPosition.TOP,
          );
        }
      });
    } else {
      BaseInfo.log(
        isError: true,
        messageTitle: "Validation Check",
        message: 'Periksa input yang dimasukan',
        snackPosition: SnackPosition.TOP,
      );
      isLoading.value = false;
    }
  }
}
