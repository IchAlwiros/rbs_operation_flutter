import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';

class ProjectListControllers extends GetxController {
  int limit = 25;
  int page = 0;
  @override
  void onInit() {
    super.onInit();
    lookupCustomer();
    fetchProjek();
  }

  var hasMore = true.obs;
  var isLoading = false.obs;

  TextEditingController search = TextEditingController();
  GlobalKey<FormBuilderState> filterFormKey = GlobalKey<FormBuilderState>();

  var currentFilter = ({}).obs;
  var projectData = [].obs;

  Future fetchProjek() async {
    var filterValue = filterFormKey.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'active',
        'value': BaseGlobalFuntion.statusCheck(currentFilter['active'] ?? '')
      },
      {'key': 'search', 'value': search.text},
      {'key': 'sort', 'value': 'DESC'},
      {'key': 'customer_id', 'value': "${currentFilter['customer_id'] ?? ''}"},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/master/customer-project',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        projectData.addAll(listData);
        page++;
      } else {
        hasMore.value = false;
      }
    });

    // try {
    //   isLoading.value = true;

    //   var response = await ApiService.list(
    //     endpoint: '/list/master/customer-project',
    //     isAuth: true,
    //     page: page,
    //     limit: limit,
    //     queryParams: paramsInclude,
    //   );

    //   if (response.statusCode == 200) {
    //     isLoading.value = false;
    //     final listData = response.data['data']['data'];

    //     if (listData.isNotEmpty) {
    //       projectData.addAll(listData);
    //       page++;
    //     } else {
    //       hasMore.value = false;
    //     }
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }

  // RELOAD DATA EQUIPMENT
  Future refreshDataProject() async {
    page = 0;
    hasMore.value = true;
    projectData.value = [];
    await fetchProjek();
  }

  // ** LOOKUP DATA */

  var extendLookupItem = {"id": "null", "name": "Semua"};
  var dataCustomers = [].obs;
  Future lookupCustomer({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);
    await ApiService.get(
      endpoint: '/lookup/master/customer',
      isAuth: true,
      queryParams: paramsInclude,
      limit: 5,
      page: 0,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataCustomers.value = [extendLookupItem, ...listData];
      }
    });
    // try {
    //   var response = await ApiService.list(
    //     endpoint: '/lookup/master/customer',
    //     isAuth: true,
    //     queryParams: paramsInclude,
    //     limit: 5,
    //     page: 0,
    //   );

    //   if (response.statusCode == 200) {
    //     dataCustomers.value = [
    //       extendLookupItem,
    //       ...response.data['data']['data']
    //     ];
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }
}

// ** DETAIL PROJECT

class DetailsProjectControllers extends GetxController {
  var isLoading = false.obs;

  var detailProject = ({}).obs;
  Future viewdetailProject(int projectId) async {
    isLoading.value = true;
    await ApiService.get(
      endpoint: '/view/master/customer-project/$projectId',
      isAuth: true,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final viewData = response.data['data'];
        detailProject.value = viewData;
      }
    });
    // try {
    //   var response = await ApiService.view(
    //     endpoint: '/view/master/customer-project/$projectId',
    //     isAuth: true,
    //   );

    //   if (response.statusCode == 200) {
    //     detailProject.value = response.data['data'];
    //     isLoading.value = false;
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }
}

// ** ADD PROJECT
class AddProjectControllers extends GetxController {
  @override
  void onInit() {
    super.onInit();
    // fetchCustomer();
    lookupProductionUnit();
    // if (ApiService.productionUnitGlobal != null ||
    //     ApiService.productionUnitGlobal != null) {
    //   lookupCustomer();
    // }
    if (ApiService.productionUnitGlobal != null ||
        LocalDataSource.getLocalVariable(key: 'lastestProduction') != null) {
      lookupCustomer();
    }
  }

  var dataPreview = ({}).obs;
  var isLoading = false.obs;
  final GlobalKey<FormBuilderState> fkAddProject =
      GlobalKey<FormBuilderState>();

  var currentFilter = ({}).obs;
  TextEditingController productionUnitController = TextEditingController();
  TextEditingController customerController = TextEditingController();

  Future addProject({
    required BuildContext loadingCtx,
    dynamic addressMaps,
  }) async {
    var formValue = fkAddProject.currentState?.value;

    isLoading.value = true;
    if (fkAddProject.currentState?.validate() ?? false) {
      final body = {
        'name': formValue!['project_name'] ?? '',
        'pic_name': formValue['project_pic'] ?? '',
        'pic_phone': formValue['phone'] ?? '',
        'production_unit_id': formValue['production_unit_id'] ?? '',
        'customer_id': formValue['customer_id'] ?? '',
        'address': formValue['address'] ?? '',
        'active': formValue['active'] ?? '',
        ...addressMaps,
      };

      await ApiService.post(
        endpoint: "/create/master/customer-project",
        isAuth: true,
        request: body,
      ).then((response) async {
        await BaseGlobalFuntion.loadingMoment(loadingCtx);
        isLoading.value = false;

        if (response != null) {
          Get.close(1);
          BaseInfo.log(
            isError: false,
            messageTitle: "Berhasil",
            message: 'Proyek ditambahkan',
            snackPosition: SnackPosition.TOP,
          );

          // REFRESH DATA LIST
          Future.delayed(const Duration(milliseconds: 500), () {
            Get.put(ProjectListControllers()).refreshDataProject();
          });
        }
      });
    } else {
      isLoading.value = false;
      // BaseInfo.log(
      //   isError: true,
      //   messageTitle: "Error Validation",
      //   message: "please check again",
      //   snackPosition: SnackPosition.TOP,
      // );
    }
  }

  // ** DATA LOOKUP
  var dataLookupPU = [].obs;
  Future lookupProductionUnit({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([]);
    await ApiService.get(
      endpoint: '/lookup/production-unit',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupPU.value = listData;
      }
    });
    // try {
    //   var response = await ApiService.list(
    //     endpoint: '/lookup/production-unit',
    //     isAuth: true,
    //     queryParams: paramsInclude,
    //   );

    //   if (response.statusCode == 200) {
    //     dataLookupPU.addAll(response.data['data']['data']);
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }

  var dataLookupCustomer = [].obs;
  Future lookupCustomer({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      {'key': 'search', 'value': searching},
      {'key': 'limit', 'value': 10},
    ]);
    await ApiService.get(
      endpoint: '/lookup/master/customer',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupCustomer.value = listData;
      }
    });
    // try {
    //   var response = await ApiService.list(
    //     endpoint: '/lookup/master/customer',
    //     isAuth: true,
    //     queryParams: paramsInclude,
    //   );

    //   if (response.statusCode == 200) {
    //     final listData = response.data['data']['data'];
    //     dataLookupCustomer.value = listData;
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }
}

// ** EDIT PROJECT
class EditProjectControllers extends GetxController {
  @override
  void onInit() {
    super.onInit();
    // fetchCustomer();
    lookupProductionUnit();
    // if (ApiService.productionUnitGlobal != null ||
    //     ApiService.productionUnitGlobal != null) {
    //   lookupCustomer();
    // }
    if (ApiService.productionUnitGlobal != null ||
        LocalDataSource.getLocalVariable(key: 'lastestProduction') != null) {
      lookupCustomer();
    }
  }

  // var dataMapProjectHelper = ({}).obs;
  var dataPreview = ({}).obs;
  var isLoading = false.obs;
  final GlobalKey<FormBuilderState> fkEditProyek =
      GlobalKey<FormBuilderState>();

  var currentFilter = ({}).obs;
  TextEditingController productionUnitController = TextEditingController();
  TextEditingController customerController = TextEditingController();
  // TextEditingController detailAddressControllers = TextEditingController();

  Future updateProject(
      {dynamic currentData,
      required BuildContext loadingCtx,
      dynamic addressMaps}) async {
    var formValue = fkEditProyek.currentState?.value;

    isLoading.value = true;
    if (fkEditProyek.currentState?.validate() ?? false) {
      // try {

      var locationId = {
        "latitude": addressMaps['latitude'] ?? currentData['latitude'],
        "longitude": addressMaps['longitude'] ?? currentData['longitude'],
        "province_id": addressMaps['province_id'] ?? currentData['province_id'],
        "city_id": addressMaps['city_id'] ?? currentData['city_id'],
        "district_id": addressMaps['district_id'] ?? currentData['district_id'],
        "sub_district_id":
            addressMaps['sub_district_id'] ?? currentData['sub_district_id'],
      };

      final body = {
        'id': currentData['id'],
        'name': formValue!['project_name'] ?? currentData['name'],
        'pic_name': formValue['project_pic'] ?? currentData['pic_name'],
        'pic_phone': formValue['phone'] ?? currentData['pic_phone'],
        'production_unit_id': formValue['production_unit_id'] ??
            currentData['production_unit_id'],
        'customer_id': formValue['customer_id'] ?? currentData['customer_id'],
        'address': formValue['address'] ?? currentData['address'],
        'active': formValue['active'] ?? currentData['active'],
        ...locationId
      };

      await ApiService.patch(
        endpoint: "/update/master/customer-project",
        isAuth: true,
        request: body,
      ).then((response) async {
        await BaseGlobalFuntion.loadingMoment(loadingCtx);
        isLoading.value = false;

        if (response != null) {
          Get.close(2);
          BaseInfo.log(
            isError: false,
            messageTitle: "Berhasil",
            message: 'Data proyek berhasil diubah',
            snackPosition: SnackPosition.TOP,
          );

          // REFRESH DATA LIST
          Future.delayed(const Duration(milliseconds: 300), () {
            Get.put(ProjectListControllers()).refreshDataProject();
          });
        }
      });
    } else {
      isLoading.value = false;
      // BaseInfo.log(
      //   isError: true,
      //   messageTitle: "Validation Check",
      //   message: 'Periksa input yang dimasukan',
      //   snackPosition: SnackPosition.TOP,
      // );
    }
  }

  // Future<dynamic> helperMaps(
  //     {dynamic lat, dynamic lng, TextEditingController? addressPreview}) async {
  //   try {
  //     List<Placemark> placemarks = await placemarkFromCoordinates(lat, lng);

  //     addressPreview?.text =
  //         '${placemarks[0].street}, ${placemarks[0].subLocality},${placemarks[0].locality},${placemarks[0].subAdministrativeArea}, ${placemarks[0].administrativeArea}';

  //     var body = {
  //       "city_name": placemarks[0].subAdministrativeArea,
  //       "district_name": placemarks[0]
  //           .locality!
  //           .toLowerCase()
  //           .replaceAll(RegExp(r'kecamatan|kec\.|Kec'), '')
  //           .trimLeft(),
  //       "latitude": lat,
  //       "longitude": lng,
  //       "postal_code": placemarks[0].postalCode,
  //       "province_name": placemarks[0].administrativeArea,
  //       "sub_district_name": placemarks[0].subLocality,
  //     };

  //     var response = await ApiService.post(
  //       endpoint: "/helper/regional",
  //       isAuth: true,
  //       request: body,
  //     );

  //     final dataRegional = response.data['data'];

  //     dataMapProjectHelper.value = {
  //       "latitude": lat,
  //       "longitude": lng,
  //       "province_id": dataRegional['province'] == null
  //           ? ""
  //           : dataRegional['province']['id'],
  //       "city_id":
  //           dataRegional['city'] == null ? "" : dataRegional['city']['id'],
  //       "district_id": dataRegional['district'] == null
  //           ? ""
  //           : dataRegional['district']['id'],
  //       "sub_district_id": dataRegional['sub_district'] == null
  //           ? ""
  //           : dataRegional['sub_district']['id'],
  //       "province_name": dataRegional['province'] == null
  //           ? placemarks[0].administrativeArea
  //           : dataRegional['province']['name'],
  //       "city_name": dataRegional['city'] == null
  //           ? placemarks[0].locality
  //           : dataRegional['city']['name'],
  //       "district_name": dataRegional['district'] == null
  //           ? placemarks[0].subLocality
  //           : dataRegional['district']['name'],
  //       "sub_district_name": dataRegional['sub_district'] == null
  //           ? placemarks[0].subLocality
  //           : dataRegional['sub_district']['name'],
  //     };
  //   } on DioException catch (err) {
  //     isLoading.value = false;
  //     BaseInfo.log(
  //       isError: true,
  //       messageTitle: "Something Wrong!",
  //       message: '${err.response?.data['error_message']}',
  //       snackPosition: SnackPosition.TOP,
  //     );
  //     if (err.response?.statusCode == 401) {
  //       await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
  //         'permissions',
  //         'token',
  //         'profile',
  //         'lastestProduction',
  //         'production_unit',
  //         'corporate',
  //       ]);
  //       Get.offAllNamed('/login');
  //     }
  //   }
  // }

  // ** DATA LOOKUP
  var dataLookupPU = [].obs;
  Future lookupProductionUnit({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([]);
    await ApiService.get(
      endpoint: '/lookup/production-unit',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupPU.value = listData;
      }
    });
  }

  var dataLookupCustomer = [].obs;
  Future lookupCustomer({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      {'key': 'search', 'value': searching},
      {'key': 'limit', 'value': 10},
    ]);
    await ApiService.get(
      endpoint: '/lookup/master/customer',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataLookupCustomer.value = listData;
      }
    });
    // try {
    //   var response = await ApiService.list(
    //     endpoint: '/lookup/master/customer',
    //     isAuth: true,
    //     queryParams: paramsInclude,
    //   );

    //   if (response.statusCode == 200) {
    //     final listData = response.data['data']['data'];
    //     dataLookupCustomer.value = listData;
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }
}
