import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/main.dart';
import 'package:rbs_mobile_operation/core/extentions/notification_manager.dart';

late AndroidNotificationChannel channel;
late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
bool isFlutterLocalNotificationsInitialized = false;

// Future<void> handleBackGrounMessage(RemoteMessage? message) async {
//   if (message == null) return;
//   navigatorKey.currentState?.pushNamed(
//     '/view-tm',
//     arguments: message,
//   );
//   print(message.notification?.title);
//   print(message.notification?.body);
// }

// class FirebaseApi {
//   final _firebasseMessaging = FirebaseMessaging.instance;

//   handleMessage(RemoteMessage? message) {
//     if (message == null) return;
//     navigatorKey.currentState?.pushNamed(
//       '${message.notification?.title}',
//       arguments: message,
//     );
//     print(message.notification?.title);
//   }

//   Future<void> setupFlutterNotifications() async {
//     if (isFlutterLocalNotificationsInitialized) {
//       return;
//     }
//     channel = const AndroidNotificationChannel(
//       'high_importance_channel', // id
//       'High Importance Notifications', // title
//       description:
//           'This channel is used for important notifications.', // description
//       importance: Importance.high,
//     );

//     flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

//     /// Create an Android Notification Channel.
//     ///
//     /// We use this channel in the `AndroidManifest.xml` file to override the
//     /// default FCM channel to enable heads up notifications.
//     await flutterLocalNotificationsPlugin
//         .resolvePlatformSpecificImplementation<
//             AndroidFlutterLocalNotificationsPlugin>()
//         ?.createNotificationChannel(channel);

//     /// Update the iOS foreground notification presentation options to allow
//     /// heads up notifications.
//     await FirebaseMessaging.instance
//         .setForegroundNotificationPresentationOptions(
//       alert: true,
//       badge: true,
//       sound: true,
//     );
//     isFlutterLocalNotificationsInitialized = true;
//   }

//   initPushNotification() {
//     _firebasseMessaging.getInitialMessage().then(handleMessage);

//     FirebaseMessaging.onMessageOpenedApp.listen(handleMessage);
//     // FirebaseMessaging.onBackgroundMessage(handleBackGrounMessage);
//     FirebaseMessaging.onMessage.listen(showFlutterNotification);
//     // FirebaseMessaging.instance.getInitialMessage().then(handleMessage);
//   }

//   void showFlutterNotification(RemoteMessage message) {
//     RemoteNotification? notification = message.notification;
//     AndroidNotification? android = message.notification?.android;
//     if (notification != null && android != null && !kIsWeb) {
//       NotificationManager().simpleNotificationShow(
//           newMessage: "Firebase Notification",
//           titleMessage: "New Message Here!");
//     }
//   }

//   Future<void> initNotifications() async {
//     await _firebasseMessaging.requestPermission();
//     final fcmToken = await _firebasseMessaging.getToken();
//     await setupFlutterNotifications();
//     initPushNotification();

//     if (fcmToken != null) {
//       print('token: $fcmToken');
//     }
//   }

//   // function to handle received messages

//   // function to initialize foreground and background settings
// }

class FirebaseMessangingRemoteDatasource {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  Future<void> initialize() async {
    await _firebaseMessaging.requestPermission(
      alert: true,
      badge: true,
      sound: true,
    );

    await _firebaseMessaging.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );

    const initializationSettingsAndroid =
        AndroidInitializationSettings('app_logo');
    final initializationSettingsIOS = DarwinInitializationSettings(
        requestAlertPermission: true,
        requestBadgePermission: true,
        requestSoundPermission: true,
        onDidReceiveLocalNotification:
            (int id, String? title, String? body, String? payload) async {
          // showNotification(id: id, title: title, body: body, payLoad: payload);
        });

    final initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onDidReceiveNotificationResponse:
            (NotificationResponse notificationResponse) async {});

    final fcmToken = await _firebaseMessaging.getToken();

    print('FCM Token: $fcmToken');

    // if (await AuthLocalDatasource().getAuthData() != null) {
    //   AuthRemoteDataSource().updateFcmToken(fcmToken!);
    // }

    FirebaseMessaging.instance.getInitialMessage();
    FirebaseMessaging.onMessage.listen((message) {
      print(message.notification?.body);
      print(message.notification?.title);
    });

    FirebaseMessaging.onMessage.listen(firebaseBackgroundHendler);
    FirebaseMessaging.onBackgroundMessage(_firebaseMessaagingBackgroundHandler);
    FirebaseMessaging.onMessageOpenedApp.listen(firebaseBackgroundHendler);
  }

  Future showNotification(
      {int id = 0, String? title, String? body, String? payload}) async {
    return flutterLocalNotificationsPlugin.show(
        id,
        title,
        body,
        const NotificationDetails(
            android: AndroidNotificationDetails('com.tri.rbsoperation', 'app',
                importance: Importance.max),
            iOS: DarwinNotificationDetails()));
  }

  Future<void> _firebaseMessaagingBackgroundHandler(
      RemoteMessage message) async {
    await Firebase.initializeApp();

    // FirebaseMessangingRemoteDatasource().firebaseBackgroundHendler(message);
  }

  Future<void> firebaseBackgroundHendler(RemoteMessage message) async {
    showNotification(
      title: message.notification!.title,
      body: message.notification!.body,
    );
  }
}
