import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class PaymentControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    super.onInit();
    lookupCustomer();
    fetchPayment();
  }

  var hasMore = true.obs;
  var isLoading = false.obs;

  TextEditingController search = TextEditingController();
  GlobalKey<FormBuilderState> filterFormKey = GlobalKey<FormBuilderState>();

  var currentFilter = ({}).obs;
  var paymentData = [].obs;

  Future fetchPayment() async {
    var filterValue = filterFormKey.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'status_code[]',
        'value': BaseGlobalFuntion.statusCheck(currentFilter['active'] ?? '')
      },
      {'key': 'search', 'value': search.text},
      {'key': 'product_name', 'value': currentFilter['product_name']},
      {'key': 'customer_id', 'value': currentFilter['customer']},
      {'key': 'sort', 'value': 'DESC'},
    ]);
    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/sales/payment',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        paymentData.addAll(listData);
        page++;
      } else {
        hasMore.value = false;
      }
    });
  }

  // RELOAD DATA EQUIPMENT
  Future refreshDataPayment() async {
    page = 0;
    hasMore.value = true;
    paymentData.value = [];
    await fetchPayment();
  }

  // ** LOOKUP DATA

  var extendLookupItem = {"id": "null", "name": "Semua"};

  var dataLookupCustomer = [].obs;
  Future lookupCustomer({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id'],
      },
      {
        'key': 'search',
        'value': searching,
      },
    ]);

    await ApiService.get(
      endpoint: '/lookup/master/customer',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataLookupCustomer.value = [extendLookupItem, ...listData];
      }
    });
  }
}
