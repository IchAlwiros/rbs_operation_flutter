import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';

class SalesListController extends GetxController {
  int limit = 25;
  int page = 0;
  @override
  void onInit() {
    fetchOrderData();
    lookupCustomer();
    lookupProduct();

    super.onInit();
  }

  var hasMore = true.obs;
  var isLoading = false.obs;

  var extendLookupItem = {"id": "null", "name": "Semua"};

  TextEditingController search = TextEditingController();
  GlobalKey<FormBuilderState> salesFormKeyFilter =
      GlobalKey<FormBuilderState>();

  var byDate = ({}).obs;
  var currentFilter = ({}).obs;
  var orderData = [].obs;
  var summaryOrder = Rx<Map<String, dynamic>>({});

  Future fetchOrderData() async {
    var filterValue = salesFormKeyFilter.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'status_code[]',
        'value':
            BaseGlobalFuntion.statusCheck(currentFilter['status_code'] ?? '')
      },
      {'key': 'search', 'value': search.text},
      {'key': 'sort', 'value': 'DESC'},
      {'key': 'customer_project_id', 'value': currentFilter['proyek_id']},
      {'key': 'customer_id', 'value': currentFilter['customer']},
      {'key': 'product_id', 'value': currentFilter['product']},
      {'key': 'date[]', 'value': byDate['start-date']},
      {'key': 'date[]', 'value': byDate['end-date']},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/sales/sales-order-product',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        final summaryData = response.data['data']['summary'];
        summaryOrder.value = {...summaryData};
        orderData.addAll(listData);
        page++;
      } else {
        hasMore.value = false;
      }
    });
  }

  // RELOAD DATA EQUIPMENT
  Future refreshDataOrder() async {
    page = 0;
    hasMore.value = true;
    orderData.value = [];
    await fetchOrderData();
  }

  // ** FILLER RANGE DATE
  Future filterByRangeDate({dynamic startDate, dynamic endDate}) async {
    if (startDate.isNotEmpty && endDate.isNotEmpty) {
      byDate.addAll({
        "start-date": startDate,
        "end-date": endDate,
      });
    } else {
      byDate.value = {};
    }
  }

  // ** DATA LOOKUP
  var dataCustomer = [].obs;
  Future lookupCustomer({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);
    await ApiService.get(
      endpoint: '/lookup/master/customer',
      isAuth: true,
      queryParams: paramsInclude,
      limit: 5,
      page: 0,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataCustomer.value = [extendLookupItem, ...listData];
      }
    });
  }

  var dataProduct = [].obs;
  Future lookupProduct({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);

    await ApiService.get(
      endpoint: '/lookup/product',
      isAuth: true,
      queryParams: paramsInclude,
      limit: 5,
      page: 0,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataProduct.value = [extendLookupItem, ...listData];
      }
    });
  }
}

// ** DETAILS SALES ORDER
class DetailsOrderControllers extends GetxController {
  var isLoading = false.obs;

  var viewDataSO = ({}).obs;
  Future viewSalesOrder(int selesOrderId) async {
    await ApiService.get(
      endpoint: '/view/sales/sales-order/$selesOrderId',
      isAuth: true,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final viewData = response.data['data'];
        viewDataSO.value = viewData;
      }
    });
  }

  var detailOrder = [].obs;
  Future viewDetailOrder(int selesOrderId) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'sales_order_id', 'value': selesOrderId},
    ]);
    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/sales/sales-order-detail',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        detailOrder.value = listData;
      }
    });
  }
}

// ** ADD SALES ORDER
class AddSalesControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    lookupProductionUnit();

    if (ApiService.productionUnitGlobal != null ||
        LocalDataSource.getLocalVariable(key: 'lastestProduction') != null) {
      fetchProduct();
      lookupProductCategory();
      lookupSalesman();
      lookupCastingType();
      lookupCustomer();
    }

    super.onInit();
  }

  var productionUnit = ({}).obs;

  var hasMore = true.obs;
  var isLoading = false.obs;
  var currentFilter = ({}).obs;

  TextEditingController search = TextEditingController();
  TextEditingController customerController = TextEditingController();
  TextEditingController salesmanController = TextEditingController();
  TextEditingController projectController = TextEditingController();
  TextEditingController salesDate = TextEditingController();
  TextEditingController datelineDate = TextEditingController();

  final GlobalKey<FormBuilderState> salesKeyAddOrder =
      GlobalKey<FormBuilderState>(debugLabel: '_salesAddOrderKey');

  var productData = [].obs;
  var categoryId = 0.obs;
  Future fetchProduct({dynamic productionUnitId, dynamic categoryId}) async {
    var filterValue = salesKeyAddOrder.currentState?.value;
    isLoading.value = true;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    if (productionUnit.isNotEmpty) {
      currentFilter.value = productionUnit;
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      {
        'key': 'product_category_id',
        'value': currentFilter['product_category_id'],
        // 'value': categoryId ?? currentFilter['product_category_id']
      },
      {'key': 'search', 'value': search.text},
      {'key': 'sort', 'value': 'DESC'},
    ]);

    await ApiService.get(
      endpoint: '/lookup/product',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      if (response != null) {
        final listData = response.data['data']['data'];
        productData.value = [...productData, ...listData];
        page++;
      } else {
        hasMore.value = false;
      }
    });
  }

  // RELOAD DATA EQUIPMENT
  Future refreshDataProduct({dynamic categoryId}) async {
    page = 0;
    hasMore.value = true;
    productData.clear();
    await fetchProduct(categoryId: categoryId);
  }

  // ** LOOKUP DATA

  var dataLookupPU = [].obs;
  Future lookupProductionUnit({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([]);
    isLoading.value = true;
    await ApiService.get(
      endpoint: '/lookup/production-unit',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupPU.value = listData;
      }
    });
  }

  var dataLookupSalesman = [].obs;
  Future lookupSalesman({dynamic searching}) async {
    if (productionUnit.isNotEmpty) {
      currentFilter.value = productionUnit;
    }
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      {'key': 'search', 'value': searching},
    ]);
    await ApiService.get(
      endpoint: '/lookup/salesman',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupSalesman.value = listData;
      }
    });
  }

  var dataLookupCustomer = [].obs;
  Future lookupCustomer({dynamic searching}) async {
    if (productionUnit.isNotEmpty) {
      currentFilter.value = productionUnit;
    }
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      {'key': 'search', 'value': searching},
    ]);
    await ApiService.get(
      endpoint: '/lookup/master/customer',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupCustomer.value = listData;
      }
    });
  }

  var dataLookupCastingType = [].obs;
  Future lookupCastingType({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      {'key': 'search', 'value': searching},
    ]);
    await ApiService.get(
      endpoint: '/lookup/casting-category',
      isAuth: true,
      limit: 5,
      page: 0,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupCastingType.value = listData;
      }
    });
  }

  var dataLookupCustomerProject = [].obs;
  Future lookupCustomerProject({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
      {'key': 'limit', 'value': 5},
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      {'key': 'customer_id', 'value': currentFilter['customer_id']}
    ]);
    await ApiService.get(
      endpoint: '/lookup/master/customer-project',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupCustomerProject.value = listData;
      }
    });
  }

  var dataLookupProductCategory = [].obs;
  Future<void> lookupProductCategory({productionUnitId}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'production_unit_id', 'value': productionUnitId}
      // {'key': 'limit', 'value': 5},
    ]);
    await ApiService.get(
      endpoint: "/lookup/product-category",
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupProductCategory.value = listData;
      }
    });
  }

  // ** ADD & CART METHOD

  var cart = [].obs;
  var cartSummary = ({}).obs;

  Future addSalesOrder({dynamic extendData}) async {
    var salesOrderValue = salesKeyAddOrder.currentState?.value;

    if ((salesKeyAddOrder.currentState?.validate() ?? false)) {
      //** INISIASI AWAL ISI PADA CART JIKA MASIH KOSONG
      if (cart.isNotEmpty) {
        // print("masuk baru");
        // print(salesOrderValue);
        cart.add({
          ...extendData,
          "casting_category_id": salesOrderValue?['casting_category'],
          "quantity": double.parse(salesOrderValue?['volume']),
          "unit_price":
              int.parse(salesOrderValue?['price'].replaceAll(',', '')),
          // "sub_total_price": double.parse(salesOrderValue?['price'] *
          //     double.parse(salesOrderValue?['volume']))
        });
      } else {
        //** INISIASI MEMASUKAN ITEM LAIN PADA CART
        cart.value = [
          {
            ...extendData,
            "casting_category_id": salesOrderValue?['casting_category'],
            "quantity": double.parse(salesOrderValue?['volume']),
            "unit_price":
                int.parse(salesOrderValue?['price'].replaceAll(',', '')),
            // "sub_total_price": double.parse(
            //     salesOrderValue?['price'] * salesOrderValue?['volume'])
          }
        ];
      }

      double totalQuantity =
          cart.fold(0, (sum, item) => sum + (item['quantity'] ?? 0));

      double totalPrice = cart.fold(
          0, (sum, item) => sum + (item['unit_price'] * item['quantity'] ?? 0));

      cartSummary.value = {
        'production_unit_name': productionUnit['production_unit_name'],
        'volume_total': totalQuantity,
        'price_total': totalPrice,
      };

      Get.close(1);
    } else {
      debugPrint('validation failed');
    }
  }

  Future deleteItemCart(index) async {
    //** MEMPERBAHARUI NILAI PRICE TOTAL & VOLUME TOTAL JIKA ADA ITEM YANG DIHAPUS
    cartSummary['price_total'] =
        cartSummary['price_total'] - cart[index]['unit_price'];
    cartSummary['volume_total'] =
        cartSummary['volume_total'] - cart[index]['quantity'];

    //** MENGEHAPUS DATA TERSEBUT DARI CART
    cart.remove(cart[index]);
  }

  Future previewCheckoutSalesOrder() async {
    var formValue = salesKeyAddOrder.currentState?.value;

    //** MEMPERBAHARUI NILAI UNIT PRICE TIAP ITEM CART DENGAN PPN/TANPA PPN

    var isTax = formValue?['tax'] ?? '0'; // MEMBERI DEFAULT TAX 0
    cart.forEach((item) {
      if (isTax == "0") {
        final unitPrice = item['unit_price'];
        double ppn = unitPrice * 11 / 100;
        double totalWithPPN = unitPrice + ppn;
        item['unit_price_preview'] = totalWithPPN.toInt();
      } else {
        final unitPrice = item['unit_price'];
        double ppn = 11 / 111 * unitPrice;
        double totalWithPPN = unitPrice - ppn;
        item['unit_price_preview'] = totalWithPPN.toInt();
        // item['unit_price_preview'] = item['unit_price'];
      }
    });

    //** MEMPERBAHARUI NILAI PRICE TOTAL TIAP ITEM SUMMARY DENGAN PPN/TANPA PPN
    if (isTax == "0") {
      final totalPrice = cartSummary['price_total'];
      double ppn = totalPrice * 11 / 100;
      double totalWithPPN = totalPrice + ppn;
      cartSummary['price_total_preview'] = totalWithPPN;
      cartSummary['ppn_total_preview'] = ppn;
    } else {
      final totalPrice = cartSummary['price_total'];
      double ppn = 11 / 111 * totalPrice;
      double totalWithPPN = totalPrice - ppn;
      cartSummary['price_total_preview'] = totalWithPPN + ppn;
      cartSummary['ppn_total_preview'] = ppn;

      // cartSummary['ppn_total_preview'] = 0;
      // cartSummary['price_total_preview'] = cartSummary['price_total'];
    }
    // print(cartSummary);
  }

  var currentDataTemp = ({}).obs;
  var isTop = false.obs;
  TextEditingController tolerance = TextEditingController();
  Future checkoutOrder({required BuildContext loadingCtx}) async {
    var formValue = salesKeyAddOrder.currentState?.value;
    isLoading.value = true;
    if (salesKeyAddOrder.currentState?.validate() ?? false) {
      // ** MENAMPUNG SEMUA BODY DARI FORM DAN CART

      final body = {
        ...productionUnit,
        ...formValue!,
        // "quotation_number": formValue!['quotation_number'] ?? "",
        // "customer_po_number": formValue!['customer_po_number'] ?? "",
        "customer_id": currentDataTemp['pelanggan']?['id'],
        "salesman_id": currentDataTemp['salesman']?['id'],
        "customer_project_id": currentDataTemp['proyek']?['id'],
        "date": salesDate.text,
        "payment_type_code": currentDataTemp['payment'],
        "tax": currentDataTemp['ppn'] ?? 0,
        "auto_close": currentDataTemp['auto_close'] ?? 0,
        "tolerance": currentDataTemp['tolerance'] ?? "",
        "deadline": currentDataTemp['dateline'] ?? datelineDate.text,
        "order_detail": cart,
        // ...formValue,
      };

      // isLoading.value = false;
      // print(body);
      await ApiService.post(
        endpoint: "/create/sales/sales-order",
        isAuth: true,
        request: body,
      ).then((response) async {
        await BaseGlobalFuntion.loadingMoment(loadingCtx);
        isLoading.value = false;
        if (response != null) {
          //** KEMBALI KE 3 PAGE SEBELUMNNYA
          Get.close(3);

          //** RETURNED SNACKBAR
          BaseInfo.log(
            isError: false,
            messageTitle: "Something Wrong!",
            message:
                response.data['data']['message'] ?? "Order berhasil dibuat",
            snackPosition: SnackPosition.TOP,
          );

          // ** REFRESH NEW DATA ORDER SALES
          Future.delayed(const Duration(milliseconds: 500), () {
            Get.put(SalesListController()).refreshDataOrder();
          });
        }
      });
    } else {
      isLoading.value = false;
      BaseInfo.log(
        isError: true,
        messageTitle: "Validation Check",
        message: 'Periksa input yang dimasukan',
        snackPosition: SnackPosition.TOP,
      );
      Get.back();
    }
  }
}
