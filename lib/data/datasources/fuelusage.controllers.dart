import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';

class FuelUsageListControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    fetchFuelUsage();
    lookupEquipment();
    super.onInit();
  }

  var hasMore = true.obs;
  var isLoading = false.obs;

  var byDate = [].obs;

  TextEditingController search = TextEditingController();
  GlobalKey<FormBuilderState> filterFormKey = GlobalKey<FormBuilderState>();

  var fuelUsageData = [].obs;
  var currentFilter = ({}).obs;

  Future fetchFuelUsage() async {
    var filterValue = filterFormKey.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'status_code',
        'value': BaseGlobalFuntion.statusCheck(currentFilter['status'] ?? '')
      },
      {'key': 'equipment_id', 'value': currentFilter['equipment_id']},
      {'key': 'search', 'value': search.text},
      {'key': 'sort', 'value': 'DESC'},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/inventory/fuel-usage',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: [...paramsInclude, ...byDate],
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        fuelUsageData.addAll(listData);
        page++;
      } else {
        hasMore.value = false;
      }
    });
  }

  // RELOAD DATA EQUIPMENT
  Future refreshFuelUsage() async {
    page = 0;
    hasMore.value = true;
    fuelUsageData.value = [];
    await fetchFuelUsage();
  }

  // FILTER BY RANGE DATE
  TextEditingController startDate = TextEditingController();
  TextEditingController endDate = TextEditingController();

  Future filterByRangeDate() async {
    if (startDate.text.isNotEmpty && endDate.text.isNotEmpty) {
      byDate.value = ['date[]=${startDate.text}', 'date[]=${endDate.text}'];
    } else {
      byDate.value = [];
    }
    await refreshFuelUsage();
  }

  // ** DETAIL FUEL

  var detailFuelUsage = ({}).obs;
  Future viewdetailFuel(int fuelUsageId) async {
    isLoading.value = true;
    await ApiService.get(
      endpoint: '/view/inventory/fuel-usage/$fuelUsageId',
      isAuth: true,
    ).then((response) {
      isLoading.value = false;
      final viewData = response.data['data'];

      if (viewData.isNotEmpty) {
        detailFuelUsage.value = viewData;
      }
    });
  }

  // ** LOOKUP DATA
  var extendLookupItem = {"id": "null", "name": "Semua"};
  var dataLookupEquipment = [].obs;
  Future lookupEquipment({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id'],
      },
      {'key': 'search', 'value': searching},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/lookup/equipment',
      isAuth: true,
      queryParams: paramsInclude,
      limit: 8,
      page: 0,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final lookupData = response.data['data']['data'];
        dataLookupEquipment.value = [extendLookupItem, ...lookupData];
      }
    });
  }
}

// ** ADD PROJECT
class AddFuelUsageControllers extends GetxController {
  @override
  void onInit() {
    super.onInit();
    lookupProductionUnit();

    if (ApiService.productionUnitGlobal != null ||
        LocalDataSource.getLocalVariable(key: 'lastestProduction') != null) {
      // lookupWarehouse();
      lookupEquipment();
      lookupFuel();
    }
  }

  var isLoading = false.obs;
  var selisihJarak = 0.0.obs;

  var currentFilter = ({}).obs;
  TextEditingController productionUnitController = TextEditingController();
  TextEditingController materialController = TextEditingController();
  TextEditingController initialFlowMeterControllers = TextEditingController();
  TextEditingController lastFlowMeterControllers = TextEditingController();
  TextEditingController initialOdometerControllers = TextEditingController();
  TextEditingController actualOdometerControllers = TextEditingController();
  TextEditingController warehouseController = TextEditingController();
  TextEditingController equipmentController = TextEditingController();

  final GlobalKey<FormBuilderState> fkAddStockTake =
      GlobalKey<FormBuilderState>();

  Future addStockTake({required BuildContext loadingCtx}) async {
    var formValue = fkAddStockTake.currentState?.value;

    isLoading.value = true;
    if (fkAddStockTake.currentState?.validate() ?? false) {
      // try {
      final body = {
        ...formValue!,
        'start_flow_meter':
            formValue['start_flow_meter'].replaceAll(RegExp(r'00$|[.,]'), ''),
        'start_odometer':
            formValue['start_odometer'].replaceAll(RegExp(r'00$|[.,]'), ''),
        'end_flow_meter':
            formValue['end_flow_meter'].replaceAll(RegExp(r'00$|[.,]'), ''),
        'actual_miliage':
            formValue['actual_miliage'].replaceAll(RegExp(r'00$|[.,]'), '') ??
                "0"
      };

      await ApiService.post(
        endpoint: "/create/inventory/fuel-usage",
        isAuth: true,
        request: body,
      ).then((response) async {
        await BaseGlobalFuntion.loadingMoment(loadingCtx);
        isLoading.value = false;
        if (response != null) {
          Get.close(1);
          BaseInfo.log(
            isError: false,
            messageTitle: "Berhasil",
            message: 'Penggunaan bahan bakar ditambahkan',
            snackPosition: SnackPosition.TOP,
          );
          // REFRESH DATA LIST
          Future.delayed(const Duration(milliseconds: 500), () {
            Get.put(FuelUsageListControllers()).refreshFuelUsage();
          });
        }
      });
    } else {
      isLoading.value = false;
      BaseInfo.log(
        isError: true,
        messageTitle: "Validation Check",
        message: 'Periksa input yang dimasukan',
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  // ** DATA LOOKUP
  var dataLookupPU = [].obs;
  Future lookupProductionUnit({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);

    await ApiService.get(
      endpoint: '/lookup/production-unit',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataLookupPU.value = response.data['data']['data'];
      }
    });

    // try {
    //   var response = await ApiService.list(
    //       endpoint: '/lookup/production-unit',
    //       isAuth: true,
    //       queryParams: [
    //         ...paramsInclude,
    //       ]);

    //   if (response.statusCode == 200) {
    //     // dataLookupPU.addAll(response.data['data']['data']);
    //     dataLookupPU.value = response.data['data']['data'];
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }

  var dataLookupFuel = [].obs;
  Future lookupFuel({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
    ]);

    await ApiService.get(
      endpoint: '/lookup/fuel',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataLookupFuel.value = listData;
      }
    });
    // try {

    //   var response = await ApiService.list(
    //     endpoint: '/lookup/fuel',
    //     isAuth: true,
    //     queryParams: paramsInclude,
    //   );

    //   if (response.statusCode == 200) {
    //     dataLookupFuel.value = response.data['data']['data'];
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }

  var dataLookupWarehouse = [].obs;
  Future lookupWarehouse({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      {'key': 'material_id', 'value': currentFilter['material_id']},
    ]);
    await ApiService.get(
      endpoint: '/lookup/warehouse-material',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataLookupWarehouse.value = response.data['data']['data'];
      }
    });

    // try {

    //   var response = await ApiService.list(
    //     endpoint: '/lookup/warehouse-material',
    //     isAuth: true,
    //     queryParams: paramsInclude,
    //   );

    //   if (response.statusCode == 200) {
    //     dataLookupWarehouse.value = response.data['data']['data'];
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }

  var dataLookupEquipment = [].obs;
  Future lookupEquipment({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
    ]);

    await ApiService.get(
      endpoint: '/lookup/equipment',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataLookupEquipment.value = listData;
      }
    });

    // try {

    //   var response = await ApiService.list(
    //     endpoint: '/lookup/equipment',
    //     isAuth: true,
    //     queryParams: paramsInclude,
    //   );

    //   if (response.statusCode == 200) {
    //     dataLookupEquipment.value = response.data['data']['data'];
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }

  var dataLastFlow = ({}).obs;
  Future viewLastFlow({dynamic searching, dynamic warehouseId}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
    ]);

    await ApiService.get(
      endpoint: '/inventory/flow-meter/$warehouseId',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final viewData = response.data['data'];
        dataLastFlow.value = viewData;

        initialFlowMeterControllers.text =
            BaseGlobalFuntion.currencyLocalConvert(
          nominal: viewData['last_flow_meter'],
          isLocalCurency: false,
          decimalDigits: 0,
        );

        // response.data['data']['last_flow_meter'].toString();
      }
    });
    // try {

    //   var response = await ApiService.view(
    //     endpoint: '/inventory/flow-meter/$warehouseId',
    //     isAuth: true,
    //     queryParams: paramsInclude,
    //   );

    //   if (response.statusCode == 200) {
    //     dataLastFlow.value = response.data['data'];
    //     initialFlowMeterControllers.text =
    //         response.data['data']['last_flow_meter'].toString();
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }
}
