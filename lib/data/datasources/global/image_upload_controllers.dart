import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class ImageUploadControllers extends GetxController {
  var isLoading = false.obs;
  var photoUploadData = ({}).obs;
  TextEditingController photoEditingController = TextEditingController();

  Uint8List? image;
  File? selectedIMage;
  // Future<void> changePhoto({required bool pickedGalleryType}) async {
  //   isLoading.value = true;
  //   try {
  //     final returnImage = await ImagePicker().pickImage(
  //       source: pickedGalleryType == true
  //           ? ImageSource.gallery
  //           : ImageSource.camera,
  //     );
  //     if (returnImage == null) {
  //       isLoading.value = false;
  //       return;
  //     }
  //     // if (returnImage == null) return;
  //     selectedIMage = File(returnImage.path);
  //     image = File(returnImage.path).readAsBytesSync();

  //     // print("INI DI PICK");
  //     // print(selectedIMage!.path);
  //     // print(returnImage.name);
  //     // print("==============");

  //     var response = await ApiService.uploadSingleFile(
  //       endpoint: "/upload/file",
  //       filePath: selectedIMage!.path.toString(),
  //       fileName: returnImage.name,
  //     );

  //     if (response.statusCode == 200) {
  //       isLoading.value = false;

  //       photoUploadData.value = response.data['data'];
  //       photoEditingController.text = response.data['data']["file"];

  //       Get.back();
  //     }
  //   } on DioException catch (err) {
  //     isLoading.value = false;
  //     BaseInfo.log(
  //       isError: true,
  //       messageTitle: "Something Wrong!",
  //       message: '${err.response?.data['error_message']}',
  //       snackPosition: SnackPosition.TOP,
  //     );
  //     if (err.response?.statusCode == 401) {
  //       await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
  //         'permissions',
  //         'token',
  //         'profile',
  //         'lastestProduction',
  //         'production_unit',
  //         'corporate',
  //       ]);
  //       Get.offAllNamed('/login');
  //     }
  //   }
  // }

  Future<void> uploadPhoto({required bool pickedGalleryType}) async {
    isLoading.value = true;
    final returnImage = await ImagePicker().pickImage(
      source:
          pickedGalleryType == true ? ImageSource.gallery : ImageSource.camera,
    );
    if (returnImage == null) {
      isLoading.value = false;
      return;
    }
    // if (returnImage == null) return;
    selectedIMage = File(returnImage.path);
    image = File(returnImage.path).readAsBytesSync();

    await ApiService.uploadSingleFile2(
      endpoint: "/upload/file",
      filePath: selectedIMage!.path.toString(),
      fileName: returnImage.name,
    ).then((response) {
      isLoading.value = false;
      if (response != null) {
        photoUploadData.value = response.data['data'];
        photoEditingController.text = response.data['data']["file"];

        Get.back();
      }
    });
  }
}
