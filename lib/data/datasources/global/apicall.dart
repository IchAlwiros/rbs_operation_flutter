import 'package:dio/dio.dart' as dio;
import 'package:rbs_mobile_operation/core/constants/variables.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:mime/mime.dart';

import 'package:http_parser/http_parser.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';

class ApiService {
  static final dioService = dio.Dio();
  static String? token = "";
  static dynamic productionUnitGlobal;

  static get({
    required String endpoint,
    required bool isAuth,
    int? page,
    int? limit,
    List<String>? queryParams,
    dynamic pathParams,
  }) async {
    // USE TOKEN INTIALIZE
    token = LocalDataSource.getLocalVariable(key: 'token');

    // TO CHECK IF TOKEN AUTH IS NULL
    if (isAuth == true && token == null) {
      Get.offAllNamed('/login');
    }

    //  USE PAGINATE PARAMS
    final paginateParams = page != null || limit != null
        ? 'offset=${page! * 10}&limit=$limit'
        : "";

    if (queryParams != null) {
      // UNTUK MENCARI QUERY PARAMS PRODUCTION UNIT ID YANG SUDAH DI INCLUDEKAN
      int index = queryParams
          .indexWhere((element) => element.contains('production_unit_id'));

      if (index == -1) {
        queryParams = [
          ...queryParams,
          ...BaseGlobalFuntion.paramsInclude([
            {
              'key': 'production_unit_id',
              'value': productionUnitGlobal?['id'] ??
                  LocalDataSource.getLocalVariable(
                    key: 'lastestProduction',
                  )?['id'],
            }
          ])
        ];
      }
    }

    // TO USE ANY QUERY PARAMS
    String queryString = queryParams?.join('&') ?? "";

    String fullUrl = '$baseURLRBS$endpoint';

    if (pathParams != null) {
      fullUrl += "/$pathParams";
    }

    if (paginateParams.isNotEmpty) {
      fullUrl += '?$paginateParams';
    }

    if (queryString.isNotEmpty) {
      if (paginateParams.isNotEmpty) {
        fullUrl += '&$queryString';
      } else {
        fullUrl += '?$queryString';
      }
    }

    var headers = {
      "Content-Type": "application/json",
    };
    if (isAuth == true) {
      headers["Authorization"] = token!;
    }

    try {
      print(fullUrl);
      return await dioService
          .get(fullUrl,
              options: dio.Options(
                responseType: dio.ResponseType.json,
                method: "GET",
                headers: headers,
              ))
          .then((response) {
        // isLoading = false;

        return response;
      });
    } on dio.DioException catch (err) {
      switch (err.response?.statusCode) {
        case 422:
          BaseInfo.log(
            isError: true,
            messageTitle: "Something Wrong!",
            message: '${err.response?.data['error_message']}',
            snackPosition: SnackPosition.TOP,
          );
          break;
        case 403:
          BaseInfo.log(
            isError: true,
            messageTitle: "Something Wrong!",
            message: '${err.response?.data['error_message']}',
            snackPosition: SnackPosition.TOP,
          );
          break;
        case 404:
          BaseInfo.log(
            isError: true,
            messageTitle: "Not Found!",
            message: '${err.response?.data['error_message']}',
            snackPosition: SnackPosition.TOP,
          );
          break;
        case 500:
          BaseInfo.log(
            isError: true,
            messageTitle: "Internal Server Error!",
            message: 'Internal Server Error!',
            snackPosition: SnackPosition.TOP,
          );
          break;
        case 401:
          await LocalDataSource.removeLocalVariable(keys: '', manyVariable: [
            'permissions',
            'token',
            'profile',
            'lastestProduction',
            'production_unit',
            'corporate',
          ]);
          Get.offAllNamed('/login');
          break;
        default:
          BaseInfo.log(
            isError: true,
            messageTitle: "Internal Server Error!",
            message: 'Internal Server Error!',
            snackPosition: SnackPosition.TOP,
          );
          break;
      }
    }

    // return await dioService
    //     .get(fullUrl,
    //         options: dio.Options(
    //           responseType: dio.ResponseType.json,
    //           method: "GET",
    //           headers: headers,
    //         ))
    //     .then((response) {
    //   return response;
    // }).catchError((err) {
    //   return BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );
    // });
  }

  static Future post({
    required String endpoint,
    required Map<dynamic, dynamic> request,
    bool isAuth = true,
  }) async {
    // USE TOKEN INTIALIZE
    token = LocalDataSource.getLocalVariable(key: 'token');

    // TO CHECK IF TOKEN AUTH IS NULL
    if (isAuth == true && token == null) {
      Get.offAllNamed('/login');
    }

    var headers = {
      "Content-Type": "application/json",
    };
    if (isAuth == true) {
      headers["Authorization"] = token!;
    }

    print('$baseURLRBS$endpoint');

    try {
      return await dioService
          .post('$baseURLRBS$endpoint',
              data: request,
              options: dio.Options(
                responseType: dio.ResponseType.json,
                method: "POST",
                headers: headers,
              ))
          .then((response) {
        return response;
      });
    } on dio.DioException catch (err) {
      print("INI ERRROR NYA");
      print(err.response);
      switch (err.response?.statusCode) {
        case 422:
          BaseInfo.log(
            isError: true,
            messageTitle: "Something Wrong!",
            message: '${err.response?.data['error_message']}',
            snackPosition: SnackPosition.TOP,
          );
          break;
        case 403:
          BaseInfo.log(
            isError: true,
            messageTitle: "Something Wrong!",
            message: '${err.response?.data['error_message']}',
            snackPosition: SnackPosition.TOP,
          );
          break;

        case 404:
          BaseInfo.log(
            isError: true,
            messageTitle: "Not Found!",
            message: '${err.response?.data['error_message']}',
            snackPosition: SnackPosition.TOP,
          );
          break;
        case 500:
          BaseInfo.log(
            isError: true,
            messageTitle: "Internal Server Error!",
            message: 'Internal Server Error!',
            snackPosition: SnackPosition.TOP,
          );
          break;
        case 401:
          await LocalDataSource.removeLocalVariable(keys: '', manyVariable: [
            'permissions',
            'token',
            'profile',
            'lastestProduction',
            'production_unit',
            'corporate',
          ]);
          Get.offAllNamed('/login');
          break;
        default:
          BaseInfo.log(
            isError: true,
            messageTitle: "Internal Server Error!",
            message: 'Internal Server Error!',
            snackPosition: SnackPosition.TOP,
          );
          break;
      }
    }
  }

  static Future patch({
    required String endpoint,
    required Map<dynamic, dynamic> request,
    bool isAuth = true,
  }) async {
    // USE TOKEN INTIALIZE
    token = LocalDataSource.getLocalVariable(key: 'token');

    // TO CHECK IF TOKEN AUTH IS NULL
    if (isAuth == true && token == null) {
      Get.offAllNamed('/login');
    }

    var headers = {
      "Content-Type": "application/json",
    };
    if (isAuth == true) {
      headers["Authorization"] = token!;
    }

    print('$baseURLRBS$endpoint');

    try {
      return await dioService
          .patch('$baseURLRBS$endpoint',
              data: request,
              options: dio.Options(
                responseType: dio.ResponseType.json,
                method: "PATCH",
                headers: headers,
              ))
          .then((response) {
        return response;
      });
    } on dio.DioException catch (err) {
      switch (err.response?.statusCode) {
        case 422:
          BaseInfo.log(
            isError: true,
            messageTitle: "Something Wrong!",
            message: '${err.response?.data['error_message']}',
            snackPosition: SnackPosition.TOP,
          );
          break;
        case 400:
          BaseInfo.log(
            isError: true,
            messageTitle: "Something Wrong!",
            message: '${err.response?.data['error_message']}',
            snackPosition: SnackPosition.TOP,
          );
          break;

        case 404:
          BaseInfo.log(
            isError: true,
            messageTitle: "Not Found!",
            message: '${err.response?.data['error_message']}',
            snackPosition: SnackPosition.TOP,
          );
          break;
        case 500:
          BaseInfo.log(
            isError: true,
            messageTitle: "Internal Server Error!",
            message: 'Internal Server Error!',
            snackPosition: SnackPosition.TOP,
          );
          break;
        case 401:
          await LocalDataSource.removeLocalVariable(keys: '', manyVariable: [
            'permissions',
            'token',
            'profile',
            'lastestProduction',
            'production_unit',
            'corporate',
          ]);
          Get.offAllNamed('/login');
          break;
        default:
          BaseInfo.log(
            isError: true,
            messageTitle: "Internal Server Error!",
            message: 'Internal Server Error!',
            snackPosition: SnackPosition.TOP,
          );
          break;
      }
    }
  }

  // static Future downloadFile({
  //   required String endpoint,
  //   required Map<dynamic, dynamic> request,
  //   bool isAuth = true,
  // }) async {
  //   // USE TOKEN INTIALIZE
  //   token = LocalDataSource.getLocalVariable(key: 'token');

  //   // TO CHECK IF TOKEN AUTH IS NULL
  //   if (isAuth == true && token == null) {
  //     Get.offAllNamed('/login');
  //   }

  //   var headers = {
  //     "Content-Type": "application/json",
  //   };
  //   if (isAuth == true) {
  //     headers["Authorization"] = token!;
  //   }

  //   print('$baseURLRBS$endpoint');

  //   try {
  //     return await dioService
  //         .post('$baseURLRBS$endpoint',
  //             data: request,
  //             options: dio.Options(
  //               responseType: dio.ResponseType.json,
  //               method: "POST",
  //               headers: headers,
  //             ))
  //         .then((response) {
  //       return response;
  //     });
  //   } on dio.DioException catch (err) {
  //     print("INI ERRROR");
  //     print(err.response);
  //     switch (err.response?.statusCode) {
  //       case 422:
  //         BaseInfo.log(
  //           isError: true,
  //           messageTitle: "Something Wrong!",
  //           message: '${err.response?.data['error_message']}',
  //           snackPosition: SnackPosition.TOP,
  //         );
  //         break;
  //       case 404:
  //         BaseInfo.log(
  //           isError: true,
  //           messageTitle: "Not Found!",
  //           message: '${err.response?.data['error_message']}',
  //           snackPosition: SnackPosition.TOP,
  //         );
  //         break;
  //       case 500:
  //         BaseInfo.log(
  //           isError: true,
  //           messageTitle: "Internal Server Error!",
  //           message: 'Internal Server Error!',
  //           snackPosition: SnackPosition.TOP,
  //         );
  //         break;
  //       case 401:
  //         await LocalDataSource.removeLocalVariable(keys: '', manyVariable: [
  //           'permissions',
  //           'token',
  //           'profile',
  //           'lastestProduction',
  //           'production_unit',
  //           'corporate',
  //         ]);
  //         Get.offAllNamed('/login');
  //         break;
  //       default:
  //         BaseInfo.log(
  //           isError: true,
  //           messageTitle: "Internal Server Error!",
  //           message: 'Internal Server Error!',
  //           snackPosition: SnackPosition.TOP,
  //         );
  //         break;
  //     }
  //   }
  // }

  // static Future uploadSingleFile({
  //   required String endpoint,
  //   required String filePath,
  //   required String fileName,
  // }) async {
  //   String fullUrl = '$baseURLRBS$endpoint';

  //   // USE TOKEN INTIALIZE
  //   token = LocalDataSource.getLocalVariable(key: 'token');
  //   // token =
  //   //     'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxNiwiYXBpX3Rva2VuIjoiZGQyMWI2NmFkYzJlMDgxNzZiNGM4MjExMmJhZjhjMjViYzg2NDBmZCIsImlhdCI6MTcwMDA0MzU5MH0.Y8dHGOFdT-oukvyx5dxg7jP40jsxS-GbzcMQ1GDPBk0';
  //   var headers = {
  //     "Authorization": token!,
  //     'Content-Type': 'multipart/form-data'
  //   };

  //   var detectFileType = lookupMimeType(filePath)!.split('/');

  //   final formData = dio.FormData.fromMap({
  //     'file': await dio.MultipartFile.fromFile(
  //       filePath,
  //       filename: fileName,
  //       contentType: MediaType(detectFileType.first, detectFileType.last),
  //     ),
  //   });

  //   // print(detectFileType.last);
  //   return await dioService
  //       .post(fullUrl,
  //           // 'http://192.168.1.6:7000/upload/file',
  //           data: formData,
  //           options: dio.Options(
  //             responseType: dio.ResponseType.json,
  //             method: "POST",
  //             headers: headers,
  //           ))
  //       .then((response) {
  //     return response;
  //   });
  // }

  static Future uploadSingleFile2({
    required String endpoint,
    required String filePath,
    required String fileName,
  }) async {
    String fullUrl = '$baseURLRBS$endpoint';

    // USE TOKEN INTIALIZE
    token = LocalDataSource.getLocalVariable(key: 'token');

    final headers = {
      "Authorization": token!,
      'Content-Type': 'multipart/form-data'
    };

    final detectFileType = lookupMimeType(filePath)!.split('/');

    final formData = dio.FormData.fromMap({
      'file': await dio.MultipartFile.fromFile(
        filePath,
        filename: fileName,
        contentType: MediaType(detectFileType.first, detectFileType.last),
      ),
    });

    try {
      return await dioService
          .post(fullUrl,
              data: formData,
              options: dio.Options(
                responseType: dio.ResponseType.json,
                method: "POST",
                headers: headers,
              ))
          .then((response) {
        return response;
      });
    } on dio.DioException catch (err) {
      switch (err.response?.statusCode) {
        case 422:
          BaseInfo.log(
            isError: true,
            messageTitle: "Something Wrong!",
            message: '${err.response?.data['error_message']}',
            snackPosition: SnackPosition.TOP,
          );
          break;
        case 404:
          BaseInfo.log(
            isError: true,
            messageTitle: "Not Found!",
            message: '${err.response?.data['error_message']}',
            snackPosition: SnackPosition.TOP,
          );
          break;
        case 500:
          BaseInfo.log(
            isError: true,
            messageTitle: "Internal Server Error!",
            message: 'Internal Server Error!',
            snackPosition: SnackPosition.TOP,
          );
          break;
        case 401:
          await LocalDataSource.removeLocalVariable(keys: '', manyVariable: [
            'permissions',
            'token',
            'profile',
            'lastestProduction',
            'production_unit',
            'corporate',
          ]);
          Get.offAllNamed('/login');
          break;
        default:
          BaseInfo.log(
            isError: true,
            messageTitle: "Internal Server Error!",
            message: 'Internal Server Error!',
            snackPosition: SnackPosition.TOP,
          );
          break;
      }
    }
  }
}
