import 'package:dio/dio.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class MasterLocationDetail extends GetxController {
  @override
  void onInit() {
    fetchProvice();
    if (dataProvince.isNotEmpty) {
      fetchCity();
      fetchdataDistric();
      fetchdataSubDistric();
    }
    super.onInit();
  }

  var isLoading = false.obs;

//* MASTER FUTURE PICKER ADDRESS
  Future<void> fetchAllAddress() async {
    try {
      isLoading.value = true;
      dataProvince.clear();
      dataCity.clear();
      dataDistric.clear();
      dataSubDistric.clear();
      await fetchProvice();
      await fetchCity();
      await fetchdataDistric();
      await fetchdataSubDistric();
      isLoading.value = false;
    } on DioException catch (err) {
      if (kDebugMode) {
        print("DISINI");
        print(err);
      }
    }
  }

  TextEditingController searchFilter = TextEditingController();

  //* MASTER DATA PROVINCE
  var dataProvince = [].obs;
  Future<void> fetchProvice() async {
    isLoading.value = true;
    await ApiService.get(
      isAuth: true,
      endpoint: "/lookup/province",
      queryParams: ["search=${searchFilter.text}"],
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataProvince.value = listData;
      }
    });
  }

  //* MASTER DATA CITY
  var dataCity = [].obs;
  Future<void> fetchCity() async {
    isLoading.value = true;
    await ApiService.get(
      isAuth: true,
      endpoint: "/lookup/city",
      queryParams: ["search=${searchFilter.text}"],
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataCity.value = listData;
      }
    });
  }

  //* MASTER DATA DISTRIC
  var dataDistric = [].obs;
  Future<void> fetchdataDistric() async {
    isLoading.value = true;
    await ApiService.get(
      isAuth: true,
      endpoint: "/lookup/district",
      queryParams: ["search=${searchFilter.text}"],
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataDistric.value = listData;
      }
    });
  }

  //* MASTER DATA SUB DISTRIC
  var dataSubDistric = [].obs;
  Future<void> fetchdataSubDistric() async {
    isLoading.value = true;
    await ApiService.get(
      isAuth: true,
      endpoint: "/lookup/sub-district",
      queryParams: ["search=${searchFilter.text}"],
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataSubDistric.value = listData;
      }
    });
  }
}
