import 'package:geolocator/geolocator.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class LocationCoordinatesControllers extends GetxController {
  // var currentLat = 0.0.obs;
  // var currentLong = 0.0.obs;

  @override
  void onInit() async {
    super.onInit();
    checkLocationPermission();
    // handleLocationPermission();
    getCurrentPosition();
    // await checkLocationPermission().then((value) => initalCurrentLocation());
  }

  Future<void> checkLocationPermission() async {
    LocationPermission permission;

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
  }

  Future<bool> handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are disabled. Please enable the services
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Location permissions are denied
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      // Location permissions are permanently denied, we cannot request permissions.

      return false;
    }
    return true;
  }

  var isLoading = false;
  Future<Position?> getCurrentPosition({BuildContext? loadingCtx}) async {
    isLoading = true;
    try {
      // printInfo(info: "DISINININININ");
      final hasPermission = await handleLocationPermission();
      if (!hasPermission) return null;

      // await Geolocator.getLastKnownPosition();

      // if (loadingCtx != null && isLoading == true) {
      //   await BaseGlobalFuntion.loadingMoment(loadingCtx);
      // }

      return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.medium,
      ).then((value) async {
        // printInfo(info: "DISINI AJA");
        return value;
      });
    } catch (e) {
      return null;
    }
  }

  // Future<void> initalCurrentLocation() async {
  //   Position position = await Geolocator.getCurrentPosition(
  //     desiredAccuracy: LocationAccuracy.medium,
  //   );

  //   currentLat.value = position.latitude;
  //   currentLong.value = position.longitude;

  //   if (kDebugMode) {
  //     print(currentLat.value);
  //     print(currentLong.value);
  //   }
  // }
}
