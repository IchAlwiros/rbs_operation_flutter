import 'package:geocoding/geocoding.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';

class HelperMapsControllers {
  var dataMapProjectHelper = ({}).obs;
  TextEditingController detailAddressControllers = TextEditingController();

  Future<dynamic> helperMaps({
    dynamic lat,
    dynamic lng,
    TextEditingController? addressPreview,
  }) async {
    List<Placemark> placemarks = await placemarkFromCoordinates(lat, lng);

    addressPreview?.text =
        '${placemarks[0].street}, ${placemarks[0].subLocality},${placemarks[0].locality},${placemarks[0].subAdministrativeArea}, ${placemarks[0].administrativeArea}';

    var body = {
      "city_name": placemarks[0].subAdministrativeArea,
      "district_name": placemarks[0]
          .locality!
          .toLowerCase()
          .replaceAll(RegExp(r'kecamatan|kec\.|Kec'), '')
          .trimLeft(),
      "latitude": lat,
      "longitude": lng,
      "postal_code": placemarks[0].postalCode,
      "province_name": placemarks[0].administrativeArea,
      "sub_district_name": placemarks[0].subLocality,
    };

    await ApiService.post(
      endpoint: "/helper/regional",
      isAuth: true,
      request: body,
    ).then((response) async {
      final dataRegional = response.data['data'];

      dataMapProjectHelper.value = {
        "latitude": lat,
        "longitude": lng,
        "province_id": dataRegional['province'] == null
            ? ""
            : dataRegional['province']['id'],
        "city_id":
            dataRegional['city'] == null ? "" : dataRegional['city']['id'],
        "district_id": dataRegional['district'] == null
            ? ""
            : dataRegional['district']['id'],
        "sub_district_id": dataRegional['sub_district'] == null
            ? ""
            : dataRegional['sub_district']['id'],
        "province_name": dataRegional['province'] == null
            ? placemarks[0].administrativeArea
            : dataRegional['province']['name'],
        "city_name": dataRegional['city'] == null
            ? placemarks[0].locality
            : dataRegional['city']['name'],
        "district_name": dataRegional['district'] == null
            ? placemarks[0].subLocality
            : dataRegional['district']['name'],
        "sub_district_name": dataRegional['sub_district'] == null
            ? placemarks[0].subLocality
            : dataRegional['sub_district']['name'],
      };
    });
  }
}
