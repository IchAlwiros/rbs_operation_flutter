import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';

class PurchaseOrderListControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    lookupVendor();
    lookupMaterial();
    fetchPurchaseOrder();
    super.onInit();
  }

  var hasMore = true.obs;
  var isLoading = false.obs;

  // var byStatus = [].obs;

  TextEditingController search = TextEditingController();
  GlobalKey<FormBuilderState> filterFormKey = GlobalKey<FormBuilderState>();

  var currentFilter = ({}).obs;
  var byDate = ({}).obs;
  var purchaseData = [].obs;

  Future fetchPurchaseOrder() async {
    var filterValue = filterFormKey.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'status_code[]',
        'value': BaseGlobalFuntion.statusCheck(currentFilter['status'] ?? '')
      },
      {'key': 'search', 'value': search.text},
      {'key': 'material_id', 'value': currentFilter['material']},
      {'key': 'vendor_id', 'value': currentFilter['vendor']},
      {'key': 'date[]', 'value': byDate['date']},
      {'key': 'sort', 'value': 'DESC'},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/inventory/purchase-order-by-material',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        purchaseData.addAll(listData);
        page++;
      } else {
        hasMore.value = false;
      }
    });
  }

  // RELOAD DATA EQUIPMENT
  Future refreshPurchaseOrder() async {
    page = 0;
    hasMore.value = true;
    purchaseData.value = [];
    await fetchPurchaseOrder();
  }

  // FILTER BY RANGE DATE

  Future filterByRangeDate({dynamic dateValue}) async {
    if (dateValue.isNotEmpty) {
      byDate.addAll({
        "date": dateValue,
      });
    } else {
      byDate.value = {};
    }

    // await refreshPurchaseOrder();
  }

  // ** LOOKUP DATA */

  var extendLookupItem = {"id": "null", "name": "Semua"};
  var dataLookupVendor = [].obs;
  Future lookupVendor({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/lookup/vendor',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupVendor.value = [extendLookupItem, ...listData];
      }
    });
  }

  var dataLookupMaterial = [].obs;
  Future lookupMaterial({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);
    await ApiService.get(
      endpoint: '/lookup/material',
      isAuth: true,
      queryParams: paramsInclude,
      limit: 5,
      page: 0,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupMaterial.value = [extendLookupItem, ...listData];
      }
    });
  }
}

class DetailPurchaseOrderControllers extends GetxController {
  var isLoading = false.obs;

  var detailPurchaseOrder = ({}).obs;
  Future viewPurchaseOrder(int purchaseId) async {
    await ApiService.get(
      endpoint: '/view/inventory/purchase-order/$purchaseId',
      isAuth: true,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final viewData = response.data['data'];
        detailPurchaseOrder.value = viewData;
      }
    });
  }

  var dataListPOMaterial = [].obs;
  Future listPOeMaterial(int purchaseId) async {
    isLoading.value = true;
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'purchase_order_id',
        'value': purchaseId,
      },
    ]);

    await ApiService.get(
      endpoint: '/list/inventory/purchase-order-detail',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataListPOMaterial.value = listData;
      }
    });
  }
}

// ** ADD PURCHASE ORDER
class AddPurchaseOrderControllers extends GetxController {
  @override
  void onInit() {
    super.onInit();
    lookupProductionUnit();
    addMaterilPO();

    // lookupWarehouse();
    if (ApiService.productionUnitGlobal != null ||
        LocalDataSource.getLocalVariable(key: 'lastestProduction') != null) {
      lookuVendor();
      currentFilter({
        "production_unit_id": ApiService.productionUnitGlobal?['id'] ??
            LocalDataSource.getLocalVariable(key: 'lastestProduction')?['id'],
      });
      temporaryDataPo({
        "production_unit_id": ApiService.productionUnitGlobal?['id'] ??
            LocalDataSource.getLocalVariable(key: 'lastestProduction')?['id'],
        "production_unit_name": ApiService.productionUnitGlobal?['name'] ??
            LocalDataSource.getLocalVariable(key: 'lastestProduction')?['name'],
      });
    }
  }

  var isLoading = false.obs;
  var totalOrder = 0.0.obs;
  var currentFilter = ({}).obs;
  var temporaryDataPo = ({}).obs;
  var tempDataMaterial = [].obs;

  TextEditingController productionUnitControllers = TextEditingController();
  TextEditingController vendorControllers = TextEditingController();
  TextEditingController materialControllers = TextEditingController();
  TextEditingController datePOControllers = TextEditingController();

  TextEditingController volumeControllers = TextEditingController();
  TextEditingController unitPriceControllers = TextEditingController();
  TextEditingController subtotalControllers = TextEditingController();

  final GlobalKey<FormBuilderState> fkAddPO = GlobalKey<FormBuilderState>();
  final GlobalKey<FormBuilderState> fkMaterialPO =
      GlobalKey<FormBuilderState>();

// ** ADD CART MATERIAL PO
  Future addPO({dynamic otherData, required BuildContext loadingCtx}) async {
    var dataPO = fkAddPO.currentState?.value;
    isLoading.value = true;
    if (fkAddPO.currentState?.validate() ?? false) {
      // try {
      var body = {
        ...dataPO!,
        // 'purchase_order_detail': temporaryDataPo['purchase_order_detail'],
        'purchase_order_detail': tempDataMaterial
      };

      await ApiService.post(
        endpoint: "/create/inventory/purchase-order",
        isAuth: true,
        request: body,
      ).then((response) async {
        await BaseGlobalFuntion.loadingMoment(loadingCtx);
        isLoading.value = false;

        if (response != null) {
          Get.close(2);
          BaseInfo.log(
            isError: false,
            messageTitle: "Berhasil",
            message: 'Purchase order ditambahkan',
            snackPosition: SnackPosition.TOP,
          );
          // REFRESH DATA LIST
          Future.delayed(const Duration(milliseconds: 500), () {
            Get.put(PurchaseOrderListControllers()).refreshPurchaseOrder();
          });
        }
      });
    } else {
      isLoading.value = false;
      BaseInfo.log(
        isError: true,
        messageTitle: "Validation Check",
        message: 'Periksa input yang dimasukan',
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  // ** ADD CART MATERIAL PO
  Future addMaterilPO({dynamic otherData}) async {
    var dataMaterialPO = fkMaterialPO.currentState?.value;

    if (fkMaterialPO.currentState?.validate() ?? false) {
      var dataAddCart = {
        ...otherData,
        ...dataMaterialPO!,
        'unit_price':
            double.parse(dataMaterialPO['unit_price'].replaceAll(',', ''))
      };

      // temporaryDataPo['purchase_order_detail'].addAll([dataAddCart]);
      if (temporaryDataPo['purchase_order_detail'] != null) {
        temporaryDataPo['purchase_order_detail'].addAll([dataAddCart]);
      } else {
        // temporaryDataPo.addAll({
        //   'purchase_order_detail': [dataAddCart]
        // });

        tempDataMaterial.addAll({dataAddCart});
      }
      Get.close(1);
    } else {
      //   // Get.back();
      //   isLoading.value = false;
      debugPrint('validation failed');
    }
  }

  // ** DATA LOOKUP
  var dataLookupPU = [].obs;
  Future lookupProductionUnit({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);
    await ApiService.get(
      endpoint: '/lookup/production-unit',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupPU.value = listData;
      }
    });
  }

  var dataLookupVendor = [].obs;
  Future lookuVendor({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
    ]);
    await ApiService.get(
      endpoint: '/lookup/vendor',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupVendor.value = listData;
      }
    });
    // try {
    //   var response = await ApiService.list(
    //     endpoint: '/lookup/vendor',
    //     isAuth: true,
    //     queryParams: paramsInclude,
    //   );

    //   if (response.statusCode == 200) {
    //     var listData = response.data['data']['data'];
    //     dataLookupVendor.value = listData;
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }

  var dataLookupMaterial = [].obs;
  Future lookupMaterial({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
    ]);
    await ApiService.get(
      endpoint: '/lookup/material',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupMaterial.value = listData;
      }
    });
  }
}
