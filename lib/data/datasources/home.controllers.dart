import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

class HomeControllers extends GetxController {
  @override
  void onInit() {
    super.onInit();
    getBenner();
    lookupProductionUnit();
    updateFCMToken();
    isInternatConnect();
    // fetchProduct();
  }

  RxBool isInternetConnect = true.obs;
  var isLoading = true.obs;

  var extendLookupItem = {"id": "null", "name": "Semua"};

  /// For Checking Internet Connection
  isInternatConnect() async {
    isLoading.value = false;
    isInternetConnect.value = await InternetConnectionChecker().hasConnection;
  }

  //* HOME DATA LOOKUP PRODUKSI
  var dataLookupPU = [].obs;
  Future<void> lookupProductionUnit() async {
    isLoading.value = true;
    await ApiService.get(
      endpoint: "/lookup/production-unit",
      isAuth: true,
    ).then((response) {
      isLoading.value = false;
      if (response != null) {
        final listData = response.data['data']['data'];

        dataLookupPU.value = [
          extendLookupItem,
          ...listData,
        ];
      }
    });
  }

  var dataGetBanner = [].obs;
  Future<void> getBenner() async {
    isLoading.value = true;
    await ApiService.get(
      endpoint: "/information/banner",
      isAuth: true,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data'];
        // print(response.data['data']);
        dataGetBanner.addAll(listData);
      }
    });
  }

  final _firebasseMessaging = FirebaseMessaging.instance;
  Future<void> updateFCMToken() async {
    isLoading.value = true;
    final fcmToken = await _firebasseMessaging.getToken();

    await ApiService.patch(endpoint: "/fcm", isAuth: true, request: {
      "fcm_token": fcmToken,
    }).then((response) {
      isLoading.value = false;

      if (response != null) {
        // final listData = response.data['data'];
        print(response.data);
        // print(response.data['data']);
        // dataGetBanner.addAll(listData);
      }
    });
  }

  // ** RELOAD DATA PU
  Future refreshDataPU() async {
    dataLookupPU.clear();
    await lookupProductionUnit();
  }
}
