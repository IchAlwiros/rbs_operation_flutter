import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';

class MaterialUsageListControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    lookupMaterial();
    lookupWarehouse();
    fetchmaterialUsage();
    super.onInit();
  }

  var hasMore = true.obs;
  var isLoading = false.obs;

  TextEditingController search = TextEditingController();
  GlobalKey<FormBuilderState> filterFormKey = GlobalKey<FormBuilderState>();
  var byDate = ({}).obs;
  var currentFilter = ({}).obs;
  var materialUsageData = [].obs;

  Future fetchmaterialUsage() async {
    var filterValue = filterFormKey.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': search.text},
      {'key': 'material_id', 'value': currentFilter['material']},
      {'key': 'warehouse_id', 'value': currentFilter['warehouse']},
      {'key': 'sort', 'value': 'DESC'},
      {'key': 'date[]', 'value': byDate['start-date']},
      {'key': 'date[]', 'value': byDate['end-date']},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/inventory/material-usage',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        materialUsageData.addAll(listData);
        page++;
      } else {
        hasMore.value = false;
      }
    });
  }

  // RELOAD DATA EQUIPMENT
  Future refreshMaterialUsage() async {
    page = 0;
    hasMore.value = true;
    materialUsageData.value = [];
    await fetchmaterialUsage();
  }

  // FILTER BY RANGE DATE

  Future filterByRangeDate({dynamic startDate, dynamic endDate}) async {
    if (startDate.isNotEmpty && endDate.isNotEmpty) {
      byDate.addAll({
        "start-date": startDate,
        "end-date": endDate,
      });
    } else {
      byDate.value = {};
    }
    // await refreshMaterialUsage();
  }

  // ** LOOKUP DATA */

  var extendLookupItem = {"id": "null", "name": "Semua"};
  var dataLookupWarehouse = [].obs;
  Future lookupWarehouse({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);

    await ApiService.get(
      endpoint: '/lookup/warehouse',
      isAuth: true,
      queryParams: paramsInclude,
      limit: 5,
      page: 0,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupWarehouse.value = [extendLookupItem, ...listData];
      }
    });
  }

  var dataLookupMaterial = [].obs;
  Future lookupMaterial({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);
    await ApiService.get(
      endpoint: '/lookup/material',
      isAuth: true,
      queryParams: paramsInclude,
      limit: 5,
      page: 0,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupMaterial.value = [extendLookupItem, ...listData];
      }
    });
  }
}

// ** ADD MATERIAL USAGE
class AddMaterialUsageControllers extends GetxController {
  @override
  void onInit() {
    super.onInit();
    lookupProductionUnit();

    if (ApiService.productionUnitGlobal != null ||
        LocalDataSource.getLocalVariable(key: 'lastestProduction') != null) {
      lookupMaterial();
    }
  }

  var dataPreview = ({}).obs;
  var isLoading = false.obs;
  var selisihVolume = 0.0.obs;
  var uomName = ({}).obs;

  var currentFilter = ({}).obs;

  TextEditingController productionUnitController = TextEditingController();
  TextEditingController materialController = TextEditingController();
  TextEditingController timeControllers = TextEditingController();
  TextEditingController stokTakeDate = TextEditingController();
  TextEditingController initialVolumControllers = TextEditingController();
  TextEditingController initialNewVolumControllers = TextEditingController();
  TextEditingController warehouseController = TextEditingController();

  final GlobalKey<FormBuilderState> fkAddStockTake =
      GlobalKey<FormBuilderState>();

  Future addMaterialUsage({required BuildContext loadingCtx}) async {
    var formValue = fkAddStockTake.currentState?.value;

    isLoading.value = true;
    if (fkAddStockTake.currentState?.validate() ?? false) {
      // try {
      final body = {
        ...formValue!,
        'date': "${formValue['date']} ${formValue['time']}"
      };
      await ApiService.post(
        endpoint: "/create/inventory/material-usage",
        isAuth: true,
        request: body,
      ).then((response) async {
        await BaseGlobalFuntion.loadingMoment(loadingCtx);
        isLoading.value = false;
        if (response != null) {
          Get.close(1);
          BaseInfo.log(
            isError: false,
            messageTitle: "Berhasil",
            message: 'Penggunaan material ditambahkan',
            snackPosition: SnackPosition.TOP,
          );
          // REFRESH DATA LIST
          Future.delayed(const Duration(milliseconds: 500), () {
            Get.put(MaterialUsageListControllers()).refreshMaterialUsage();
          });
        }
      });
    } else {
      isLoading.value = false;
      BaseInfo.log(
        isError: true,
        messageTitle: "Validation Check",
        message: 'Periksa input yang dimasukan',
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  // ** DATA LOOKUP
  var dataLookupPU = [].obs;
  Future lookupProductionUnit({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);
    await ApiService.get(
      endpoint: '/lookup/production-unit',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupPU.value = listData;
      }
    });

    // try {
    //   var response = await ApiService.list(
    //       endpoint: '/lookup/production-unit',
    //       isAuth: true,
    //       queryParams: [
    //         ...paramsInclude,
    //       ]);

    //   if (response.statusCode == 200) {
    //     // dataLookupPU.addAll(response.data['data']['data']);
    //     dataLookupPU.value = response.data['data']['data'];
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }

  var dataLookupMaterial = [].obs;
  Future lookupMaterial({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
    ]);
    await ApiService.get(
      endpoint: '/lookup/material',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupMaterial.value = listData;
      }
    });
    // try {
    //   var response = await ApiService.list(
    //     endpoint: '/lookup/material',
    //     isAuth: true,
    //     queryParams: paramsInclude,
    //   );

    //   if (response.statusCode == 200) {
    //     dataLookupMaterial.addAll(response.data['data']['data']);
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }

  var dataLookupWarehouse = [].obs;
  Future lookupWarehouse({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      {'key': 'material_id', 'value': currentFilter['material_id']},
    ]);

    await ApiService.get(
      endpoint: '/lookup/warehouse-material',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupWarehouse.value = listData;
      }
    });
    // try {
    //   var response = await ApiService.list(
    //     endpoint: '/lookup/warehouse-material',
    //     isAuth: true,
    //     queryParams: paramsInclude,
    //   );

    //   if (response.statusCode == 200) {
    //     dataLookupWarehouse.value = response.data['data']['data'];
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }
}
