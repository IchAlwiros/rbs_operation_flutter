import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class MaterialIncomingListControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    lookupMaterial();
    lookupWarehouse();
    fetchmaterialUsage();
    super.onInit();
  }

  var hasMore = true.obs;
  var isLoading = false.obs;

  TextEditingController search = TextEditingController();
  GlobalKey<FormBuilderState> filterFormKey = GlobalKey<FormBuilderState>();
  var currentFilter = ({}).obs;
  var materialIncomingData = [].obs;

  Future fetchmaterialUsage() async {
    var filterValue = filterFormKey.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': search.text},
      {'key': 'material_id', 'value': currentFilter['material_id']},
      {'key': 'warehouse_id', 'value': currentFilter['warehouse_id']},
      {'key': 'sort', 'value': 'DESC'},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/inventory/incoming-material',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        materialIncomingData.addAll(listData);
        page++;
      } else {
        hasMore.value = false;
      }
    });
  }

  // RELOAD DATA EQUIPMENT
  Future refreshMaterialIncoming() async {
    page = 0;
    hasMore.value = true;
    materialIncomingData.value = [];
    await fetchmaterialUsage();
  }

  // ** LOOKUP DATA */
  var extendLookupItem = {"id": "null", "name": "Semua"};
  var dataLookupWarehouse = [].obs;
  Future lookupWarehouse({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id'],
      },
      {'key': 'search', 'value': searching},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/lookup/warehouse',
      isAuth: true,
      limit: 5,
      page: 0,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final lookupData = response.data['data']['data'];

        dataLookupWarehouse.value = [extendLookupItem, ...lookupData];
      }
    });
  }

  var dataLookupMaterial = [].obs;
  Future lookupMaterial({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id'],
      },
      {'key': 'search', 'value': searching},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/lookup/material',
      isAuth: true,
      queryParams: paramsInclude,
      limit: 5,
      page: 0,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final lookupData = response.data['data']['data'];
        dataLookupMaterial.value = [extendLookupItem, ...lookupData];
      }
    });
  }
}

class DetailMaterialIncomingControllers extends GetxController {
  var isLoading = false.obs;

  var detailIncomingMaterial = ({}).obs;
  Future viewDetailIncomingMatrial(int incomingId) async {
    await ApiService.get(
      endpoint: '/view/inventory/incoming-material/$incomingId',
      isAuth: true,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final viewData = response.data['data'];
        detailIncomingMaterial.value = viewData;
      }
    });
  }
}

// ** ADD MATERIAL INCOMING
class AddMaterialIncomingControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    super.onInit();
    lookupProductionUnit();
    lookupPurchaseOrder();
    // lookupWarehouse();
    // if (ApiService.productionUnitGlobal != null ||
    //     BaseGlobalVariable.getLocalVariable(key: 'lastestProduction') != null) {
    //   lookupPurchaseOrder();
    // }
  }

  var hasMore = true.obs;
  var isLoading = false.obs;
  var currentFilter = ({}).obs;
  var productionUnit = ({}).obs;
  var dataPreview = ({}).obs;
  var cartMaterialPO = [].obs;

  TextEditingController timeControllers = TextEditingController();
  TextEditingController dateControllers = TextEditingController();
  TextEditingController warehouseController = TextEditingController();

  TextEditingController docketNumberController = TextEditingController();
  TextEditingController armadaController = TextEditingController();
  TextEditingController noPolisiController = TextEditingController();
  TextEditingController driverController = TextEditingController();
  TextEditingController noDriverController = TextEditingController();
  // TextEditingController initialNewVolumControllers = TextEditingController();

  TextEditingController searchPOController = TextEditingController();

  final GlobalKey<FormBuilderState> fkAddMaterialIncoming =
      GlobalKey<FormBuilderState>();

  final GlobalKey<FormBuilderState> fkMaterialCart =
      GlobalKey<FormBuilderState>();

  Future addMaterialIncomingPO(
      {dynamic extendData, required BuildContext loadingCtx}) async {
    var formValue = fkAddMaterialIncoming.currentState?.value;

    isLoading.value = true;
    if (fkAddMaterialIncoming.currentState?.validate() ?? false) {
      // try {
      final body = {
        ...formValue!,
        ...dataPreview,
        'date': "${formValue['date']} ${formValue['time']}",
        'incoming_material_detail': cartMaterialPO,
        'attachment': extendData['file'],
      };

      print(body);

      await ApiService.post(
        endpoint: "/create/inventory/incoming-material",
        isAuth: true,
        request: body,
      ).then((response) async {
        await BaseGlobalFuntion.loadingMoment(loadingCtx);
        isLoading.value = false;
        if (response != null) {
          Get.close(4);
          BaseInfo.log(
            isError: false,
            messageTitle: "Berhasil",
            message: 'material ditambahkan',
            snackPosition: SnackPosition.TOP,
          );

          // REFRESH DATA LIST
          Future.delayed(const Duration(milliseconds: 500), () {
            Get.put(MaterialIncomingListControllers())
                .refreshMaterialIncoming();
          });
        }
      });
    } else {
      Get.back();
      BaseInfo.log(
        isError: true,
        messageTitle: "Validation Check",
        message: 'Periksa input yang dimasukan',
        snackPosition: SnackPosition.TOP,
      );
      isLoading.value = false;
    }
  }

  // ** ADD CART MATERIAL PO
  Future addMaterilPO({dynamic otherData}) async {
    var dataMaterialPO = fkMaterialCart.currentState?.value;
    if (fkMaterialCart.currentState?.validate() ?? false) {
      var dataAddCart = {
        ...otherData,
        ...dataMaterialPO!,
      };

      cartMaterialPO.addAll([dataAddCart]);
      Get.close(1);
    } else {
      // Get.back();
      isLoading.value = false;
      debugPrint('validation failed');
    }
  }

  // ** DELETE MATERIAL
  Future deleteMaterialPO(index) async {
    //** MENGEHAPUS DATA TERSEBUT DARI CART
    cartMaterialPO.remove(cartMaterialPO[index]);
  }

  // ** DATA LOOKUP
  var dataLookupPU = [].obs;
  Future lookupProductionUnit({dynamic searching}) async {
    isLoading.value = true;
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);
    await ApiService.get(
      endpoint: '/lookup/production-unit',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupPU.value = listData;
      }
    });

    // try {
    //   var response = await ApiService.list(
    //     endpoint: '/lookup/production-unit',
    //     isAuth: true,
    //     queryParams: paramsInclude,
    //   );

    //   if (response.statusCode == 200) {
    //     isLoading.value = false;
    //     // dataLookupPU.addAll(response.data['data']['data']);
    //     dataLookupPU.value = response.data['data']['data'];
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }

  var dataPO = [].obs;
  Future lookupPurchaseOrder({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searchPOController.text},
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      {'key': 'status_code[]', 'value': "PENDING"},
      {'key': 'status_code[]', 'value': 'PROGRESS'},
    ]);
    isLoading.value = true;
    await ApiService.get(
      endpoint: '/lookup/purchase-order',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataPO.value = [...dataPO, ...listData];
        page++;
      } else {
        hasMore.value = false;
      }
    });
    // try {

    //   var response = await ApiService.list(
    //     endpoint: '/lookup/purchase-order',
    //     isAuth: true,
    //     queryParams: paramsInclude,
    //     page: page,
    //     limit: limit,
    //   );

    //   if (response.statusCode == 200) {
    //     isLoading.value = false;
    //     final listData = response.data['data']['data'];

    //     if (listData.isNotEmpty) {
    //       dataPO.value = [...dataPO, ...listData];
    //       page++;
    //     } else {
    //       hasMore.value = false;
    //     }
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }

  // RELOAD DATA EQUIPMENT
  Future refreshLookupPO() async {
    page = 0;
    hasMore.value = true;
    dataPO.value = [];
    await lookupPurchaseOrder();
  }

  var dataPOdetail = [].obs;
  Future lookupPOdetail({dynamic searching, dynamic poId}) async {
    isLoading.value = true;
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      {'key': 'purchase_order_id', 'value': poId},
    ]);
    await ApiService.get(
      endpoint: '/lookup/purchase-order-detail',
      isAuth: true,
      page: 0,
      limit: 6,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataPOdetail.value = listData;
      }
    });
    // try {
    //   var response = await ApiService.list(
    //     endpoint: '/lookup/purchase-order-detail',
    //     isAuth: true,
    //     queryParams: paramsInclude,
    //     page: 0,
    //     limit: 6,
    //   );

    //   if (response.statusCode == 200) {
    //     isLoading.value = false;
    //     var listData = response.data['data']['data'];
    //     // print(response.data['data']['data']);
    //     dataPOdetail.value = listData;
    //     // print(response.data['data']['data']);
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );

    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }

  var dataLookupWarehouse = [].obs;
  Future lookupWarehouse({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      {
        'key': 'material_category_id',
        'value': currentFilter['material_category_id']
      },
    ]);
    await ApiService.get(
      endpoint: '/lookup/warehouse-material-category',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupWarehouse.value = listData;
      }
    });
  }
}
