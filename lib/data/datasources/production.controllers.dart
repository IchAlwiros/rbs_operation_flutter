import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class ProductionListControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    super.onInit();

    lookupCustomers();
    lookupProductCategory();
    fetchProduction();
  }

  var byDate = ({}).obs;
  var hasMore = true.obs;
  var isLoading = false.obs;

  var extendLookupItem = {"id": "null", "name": "Semua"};

  TextEditingController search = TextEditingController();

  GlobalKey<FormBuilderState> filterFormKey = GlobalKey<FormBuilderState>();

  var currentFilter = ({}).obs;
  var productionData = [].obs;
  var summaryProduction = ({}).obs;

  Future fetchProduction() async {
    var filterValue = filterFormKey.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'status_code[]',
        'value': BaseGlobalFuntion.statusCheck(currentFilter['status'] ?? '')
      },
      {'sort': 'sort', 'value': 'DESC'},
      {'key': 'search', 'value': search.text},
      {'key': 'product_category_id', 'value': currentFilter['product_id']},
      {'key': 'customer_id', 'value': currentFilter['customer']},
      {'key': 'date[]', 'value': byDate['start-date']},
      {'key': 'date[]', 'value': byDate['end-date']},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/production',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];
      final summaryData = response.data['data']['summary'];
      summaryProduction.value = {...summaryData};

      if (listData.isNotEmpty) {
        productionData.addAll(listData);
        page++;
      } else {
        hasMore.value = false;
      }
    });
  }

  // RELOAD DATA EQUIPMENT
  Future refreshDataProduction() async {
    page = 0;
    hasMore.value = true;
    productionData.value = [];
    await fetchProduction();
  }

  Future filterByRangeDate({dynamic startDate, dynamic endDate}) async {
    if (startDate.isNotEmpty && endDate.isNotEmpty) {
      byDate.addAll({
        "start-date": startDate,
        "end-date": endDate,
      });
    } else {
      byDate.value = {};
    }
  }

  var dataCustomers = [].obs;
  Future lookupCustomers({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id'],
      },
      {
        'key': 'search',
        'value': searching,
      },
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/lookup/master/customer',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataCustomers.value = [extendLookupItem, ...listData];
      }
    });
  }

  var dataLookupProductCategory = [].obs;
  Future lookupProductCategory({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/lookup/product-category',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataLookupProductCategory.value = [extendLookupItem, ...listData];
      }
    });
  }
}

// ** DETAILS PRODUCTION
class DetailsProductionControllers extends GetxController {
  var isLoading = false.obs;

  var viewProductionData = ({}).obs;
  var materialList = [].obs;
  Future fetchViewProduction(docketId) async {
    await ApiService.get(
      isAuth: true,
      endpoint: '/view/production/$docketId',
    ).then((response) {
      isLoading.value = false;
      final viewData = response.data['data'];

      materialList.value = viewData['material_usage'];

      if (viewData.isNotEmpty) {
        viewProductionData.value = viewData;
      }
    });
  }
}
