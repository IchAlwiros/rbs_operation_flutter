import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';
import 'package:url_launcher/url_launcher.dart';

class AuthControllers extends GetxController {
  // * AUTH CHECK HAVE REGIST TOKEN
  Future<void> checkAuth(BuildContext context) async {
    Timer(const Duration(milliseconds: 2500), () async {
      final token = LocalDataSource.getLocalVariable(key: "token");
      token != null ? Get.offAllNamed('/dashboard') : Get.offAllNamed('/login');
    });
  }

  // * AUTH LOGIN
  GlobalKey<FormBuilderState> authFormKey = GlobalKey<FormBuilderState>();
  var isLoading = false.obs;
  final _firebasseMessaging = FirebaseMessaging.instance;

  Future<dynamic> loginAuth() async {
    isLoading.value = true;
    if (authFormKey.currentState?.validate() ?? false) {
      final fcmToken = await _firebasseMessaging.getToken();

      final body = {
        ...authFormKey.currentState!.value,
        'fcm_token': fcmToken,
      };

      await ApiService.post(
        endpoint: "/login",
        isAuth: false,
        request: body,
      ).then((response) {
        isLoading.value = false;

        print("TOKEN USER : ${response.data['data']['token']}");
        if (response != null) {
          LocalDataSource.setLocalVariable(key: "fcmToken", value: fcmToken);
          LocalDataSource.setLocalVariable(
              key: "lastestProduction",
              value: response.data['data']['production_unit']);
          LocalDataSource.setLocalVariable(
              key: "token", value: response.data['data']['token']);
          LocalDataSource.setLocalVariable(
              key: "profile", value: response.data['data']['profile']);
          LocalDataSource.setLocalVariable(
              key: "production_unit",
              value: response.data['data']['production_unit']);
          LocalDataSource.setLocalVariable(
              key: "corporate", value: response.data['data']['corporate']);
          LocalDataSource.setLocalVariable(
              key: "permissions", value: response.data['data']['permissions']);

          BaseInfo.log(
            isError: false,
            messageTitle: "success",
            message: response.data['data']['message'] ?? "Login Berhasil",
            snackPosition: SnackPosition.TOP,
          );

          // GO TO NEXT PAGE
          Get.offAllNamed('/dashboard');
          isLoading.value = false;
        }
      });
    } else {
      isLoading.value = false;
      BaseInfo.log(
        isError: true,
        messageTitle: "Error Validation",
        message: "please check again",
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  // * AUTH LOGOUT
  Future<void> onLogout() async {
    await LocalDataSource.removeLocalVariable(keys: '', manyVariable: [
      'permissions',
      'token',
      'profile',
      'lastestProduction',
      'production_unit',
      'corporate',
    ]);

    ApiService.productionUnitGlobal = null;
    Get.offAllNamed('/login');

    // await BaseGlobalVariable.removeVariable(
    //     keys: '', manyVariable: ['permissions', 'token', 'profile']).then(
    //   (value) => Get.offAllNamed('/login'),
    // );
  }

// ** GOOGLE AUTH

  static User? user = FirebaseAuth.instance.currentUser;

  Future<User?> loginWithGoogle() async {
    isLoading.value = true;
    final fcmToken = await _firebasseMessaging.getToken();
    const List<String> scopes = <String>[
      'openid openid email',
      'email'
      // 'https://www.googleapis.com/auth/contacts.readonly',
    ];

    final googleAccount = await GoogleSignIn(
      scopes: scopes,
    ).signIn().catchError((onError) => null);

    if (googleAccount == null) {
      isLoading.value = false;
      return null;
    }

    final googleAuth = await googleAccount.authentication;

    final credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    final userCredential = await FirebaseAuth.instance.signInWithCredential(
      credential,
    );

    // final idToken = await userCredential.user!.getIdToken();

    final body = {
      "provider": "GOOGLE",
      "email": userCredential.user!.email ?? "",
      "token": userCredential.credential!.accessToken ?? "",
      'fcm_token': fcmToken,
      // "Accesstoken": userCredential.credential!.accessToken ?? ""
    };

    // print(userCredential.credential!.accessToken);
    // printInfo(info: '${userCredential.credential!.accessToken}');

    // idtokkoon.value = idToken!;

    // print("iNIN tokES ${idToken}");

    // print("iNIN tokEN APAKAH SAMA? ${idToken == idToken}");
    // print("INI TOKEN : $idToken");
    // print(body);

    // isLoading.value = false;

    await ApiService.post(
      endpoint: "/login",
      isAuth: false,
      request: body,
    ).then((response) async {
      // await Baselo.loadingMoment(loadingCtx);
      isLoading.value = false;

      if (response != null) {
        isLoading.value = false;

        LocalDataSource.setLocalVariable(key: "fcmToken", value: fcmToken);
        LocalDataSource.setLocalVariable(
            key: "lastestProduction",
            value: response.data['data']['production_unit']);
        LocalDataSource.setLocalVariable(
            key: "token", value: response.data['data']['token']);
        LocalDataSource.setLocalVariable(
            key: "profile", value: response.data['data']['profile']);
        LocalDataSource.setLocalVariable(
            key: "production_unit",
            value: response.data['data']['production_unit']);
        LocalDataSource.setLocalVariable(
            key: "corporate", value: response.data['data']['corporate']);
        LocalDataSource.setLocalVariable(
            key: "permissions", value: response.data['data']['permissions']);

        BaseInfo.log(
          isError: false,
          messageTitle: "Berhasil",
          message: "Login",
          snackPosition: SnackPosition.TOP,
        );
        Get.offAllNamed('/dashboard');
      } else {
        signOut();
      }
    });

    // signOut();

    // try {
    //   var response = await ApiService.post(
    //     endpoint: "/login",
    //     isAuth: false,
    //     request: body,
    //   );

    //   if (response.statusCode == 200) {
    //     print('Lanjut');
    //   } else {
    //     BaseInfo.log(
    //       isError: true,
    //       messageTitle: "Login Failed",
    //       message: "please check again or register again",
    //       snackPosition: SnackPosition.TOP,
    //     );
    //   }
    //   isLoading.value = false;
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   // BaseInfo.log(
    //   //   isError: true,
    //   //   messageTitle: "Error",
    //   //   message:
    //   //       "${err.response!.data['error_message'] ?? "Account Anda belum terdaftar"}",
    //   //   snackPosition: SnackPosition.TOP,
    //   // );
    //   if (err.response?.statusCode == 401) {
    //     await LocalDataSource.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }

    //   BaseInfo.log(
    //     isError: false,
    //     messageTitle: "Something Wrong!",
    //     message: "${err.response?.data['error_message']}",
    //     durationMilisecons: 2000,
    //     backgroundColor: AppColor.kGreyColor,
    //     iconLeading: Assets.icons.googleAuth.image(scale: 16),
    //     // Image.asset(
    //     //   "assets/icons/google_auth.png",
    //     //   scale: 16,
    //     // ),
    //     widgetMessage: Row(
    //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //       children: [
    //         Text(
    //           "kami tidak menemukan email anda",
    //           style: BaseTextStyle.customTextStyle(
    //             kfontSize: 12,
    //             kfontWeight: FontWeight.w500,
    //             kcolor: AppColor.kWhiteColor,
    //           ),
    //         ),
    //         TextButton(
    //           onPressed: () {
    //             const link = "https://app.rbsmixsolution.com/#/register";

    //             launchUrl(Uri.parse(link),
    //                 mode: LaunchMode.externalApplication);
    //           },
    //           child: Text(
    //             "Register Sekarang!",
    //             style: BaseTextStyle.customTextStyle(
    //               kfontSize: 12,
    //               kfontWeight: FontWeight.w500,
    //               kcolor: AppColor.kWhiteColor,
    //             ),
    //           ),
    //         )
    //       ],
    //     ),
    //     snackPosition: SnackPosition.TOP,
    //   );
    //   signOut();
    // }

    // if (kDebugMode) {
    //   // print("INI ID TOKEN SIGN IN");
    //   // print(googleAuth.idToken);
    //   print("INI CREDENTIAL NYA:");
    //   // print(userCredential.user);
    //   print(body);
    // }

    return userCredential.user;
  }

  static Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
    await GoogleSignIn().signOut();
  }
}
