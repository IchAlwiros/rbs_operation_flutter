import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';

class StockTakeListControllers extends GetxController {
  int limit = 25;
  int page = 0;

  @override
  void onInit() {
    lookupMaterial();
    lookupWarehouse();
    lookupProductionUnit();
    fetchStockTake();
    super.onInit();
  }

  var hasMore = true.obs;
  var isLoading = false.obs;

  TextEditingController search = TextEditingController();

  GlobalKey<FormBuilderState> filterFormKey = GlobalKey<FormBuilderState>();
  var byDate = ({}).obs;
  var currentFilter = ({}).obs;
  var stockTakeData = [].obs;

  Future fetchStockTake() async {
    var filterValue = filterFormKey.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': search.text},
      {'key': 'sort', 'value': 'DESC'},
      {'key': 'material_id', 'value': currentFilter['material']},
      {'key': 'warehouse_id', 'value': currentFilter['warehouse']},
      {'key': 'production_unit_id', 'value': currentFilter['unit_production']},
      {'key': 'date[]', 'value': byDate['start-date']},
      {'key': 'date[]', 'value': byDate['end-date']},
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/inventory/opname-stock',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        stockTakeData.addAll(listData);
        page++;
      } else {
        hasMore.value = false;
      }
    });
  }

  // RELOAD DATA EQUIPMENT
  Future refreshStockTake() async {
    page = 0;
    hasMore.value = true;
    stockTakeData.value = [];
    await fetchStockTake();
  }

  // FILTER BY RANGE DATE

  Future filterByRangeDate({dynamic startDate, dynamic endDate}) async {
    if (startDate.isNotEmpty && endDate.isNotEmpty) {
      byDate.addAll({
        "start-date": startDate,
        "end-date": endDate,
      });
    } else {
      byDate.value = {};
    }
    // await refreshStockTake();
  }

  // ** LOOKUP DATA */

  var extendLookupItem = {"id": "null", "name": "Semua"};
  var dataLookupWarehouse = [].obs;
  Future lookupWarehouse({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);
    await ApiService.get(
      endpoint: '/lookup/warehouse',
      isAuth: true,
      queryParams: paramsInclude,
      limit: 5,
      page: 0,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupWarehouse.value = [extendLookupItem, ...listData];
      }
    });
  }

  var dataLookupMaterial = [].obs;
  Future lookupMaterial({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);

    await ApiService.get(
      endpoint: '/lookup/material',
      isAuth: true,
      queryParams: paramsInclude,
      limit: 5,
      page: 0,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupMaterial.value = [extendLookupItem, ...listData];
      }
    });
  }

  var dataLookupProductionUnit = [].obs;
  Future lookupProductionUnit({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([]);
    isLoading.value = true;
    await ApiService.get(
      endpoint: "/lookup/production-unit",
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupProductionUnit.value = [extendLookupItem, ...listData];
      }
    });
  }

  // ** DETAIL STOCK TAKE

  var detailStockTake = ({}).obs;
  Future viewStockTake(int stockTakeId) async {
    isLoading.value = true;
    await ApiService.get(
      endpoint: '/view/inventory/opname-stock/$stockTakeId',
      isAuth: true,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final viewData = response.data['data'];
        detailStockTake.value = viewData;
      }
    });
  }
}

// ** ADD STOCK TAKE
class AddStockTakeControllers extends GetxController {
  @override
  void onInit() {
    super.onInit();
    lookupProductionUnit();

    if (ApiService.productionUnitGlobal != null ||
        LocalDataSource.getLocalVariable(key: 'lastestProduction') != null) {
      lookupMaterial();
    }
  }

  var dataPreview = ({}).obs;
  var isLoading = false.obs;
  var selisihVolume = 0.0.obs;

  var currentFilter = ({}).obs;

  TextEditingController productionUnitController = TextEditingController();
  TextEditingController materialController = TextEditingController();
  TextEditingController timeControllers = TextEditingController();
  TextEditingController stokTakeDate = TextEditingController();
  TextEditingController initialVolumControllers = TextEditingController();
  TextEditingController initialNewVolumControllers = TextEditingController();
  TextEditingController warehouseController = TextEditingController();

  final GlobalKey<FormBuilderState> fkAddStockTake =
      GlobalKey<FormBuilderState>();

  Future addStockTake({required BuildContext loadingCtx}) async {
    var formValue = fkAddStockTake.currentState?.value;

    isLoading.value = true;
    if (fkAddStockTake.currentState?.validate() ?? false) {
      // try {
      final body = {
        ...formValue!,
        'volume_before': formValue['volume_before'].replaceAll(',', ''),
        "new_quantity": formValue['new_quantity'].replaceAll(',', ''),
        'date': "${formValue['date']} ${formValue['time']}"
      };

      await ApiService.post(
        endpoint: "/create/inventory/opname-stock",
        isAuth: true,
        request: body,
      ).then((response) async {
        await BaseGlobalFuntion.loadingMoment(loadingCtx);
        isLoading.value = false;

        if (response != null) {
          Get.close(1);
          BaseInfo.log(
            isError: false,
            messageTitle: "Berhasil",
            message: 'Stock take ditambahkan',
            snackPosition: SnackPosition.TOP,
          );

          // REFRESH DATA LIST
          Future.delayed(const Duration(milliseconds: 500), () {
            Get.put(StockTakeListControllers()).refreshStockTake();
          });
        }
      });
    } else {
      isLoading.value = false;
      BaseInfo.log(
        isError: true,
        messageTitle: "Validation Check",
        message: 'Periksa input yang dimasukan',
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  // ** DATA LOOKUP
  var dataLookupPU = [].obs;
  Future lookupProductionUnit({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);

    await ApiService.get(
      endpoint: '/lookup/production-unit',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupPU.value = listData;
      }
    });
  }

  var dataLookupMaterial = [].obs;
  Future lookupMaterial({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
    ]);
    await ApiService.get(
      endpoint: '/lookup/material',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupMaterial.value = listData;
      }
    });
  }

  var dataLookupWarehouse = [].obs;
  Future lookupWarehouse({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
      {
        'key': 'production_unit_id',
        'value': currentFilter['production_unit_id']
      },
      // {'key': 'material_id', 'value': currentFilter['material_id']},
    ]);
    await ApiService.get(
      endpoint: '/view/inventory/material-stock',
      isAuth: true,
      queryParams: paramsInclude,
      pathParams: currentFilter['material_id'],
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data'];
        dataLookupWarehouse.value = listData.map((item) {
          return {
            "id": item["warehouse_id"],
            "name": item["warehouse_name"],
            "quantity": item["quantity"],
            "last_update": item["last_update"]
          };
        }).toList();
      }
    });
  }

  var dataViewInventory = ({}).obs;
  Future viewInventory({
    dynamic searching,
    dynamic materialId,
    dynamic productionUnitId,
  }) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
      {'key': 'production_unit_id', 'value': productionUnitId},
    ]);

    await ApiService.get(
      endpoint: '/view/inventory/material/$materialId',
      isAuth: true,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final viewData = response.data['data'];
        dataViewInventory.value = viewData;
        initialVolumControllers.text = BaseGlobalFuntion.currencyLocalConvert(
          nominal: viewData['current_stock'],
          isLocalCurency: false,
          decimalDigits: 0,
        );
        // response.data['data']['current_stock'].toString();
        // initialNewVolumControllers.text = initialVolumControllers.text;
      }
    });
  }
}
