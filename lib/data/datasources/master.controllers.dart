// import 'package:rbs_mobile_operation/logics/services/apicall.dart';
// import 'package:rbs_mobile_operation/resources/app_main_resources.dart';
// import 'package:rbs_mobile_operation/utils/global/global_function.dart';

// class MasterControllers extends GetxController {
//   @override
//   void onInit() {
//     super.onInit();

//     fetchCustomer();
//     fetchMaterialCategory();
//     fetchWarehouseData();
//     fetchMaterialData();
//     fetchReligion();
//     fetchUnitPlant();
//     fetchVendorData();
//     fetchProductMutuLookup();
//     fetchProdukKategori();
//     fetchLookupCastingCategory();
//   }

//   var dataReligion = [].obs;
//   Future<void> fetchReligion() async {
//     try {
//       var response = await ApiService.list(
//         isAuth: true,
//         endpoint: "/lookup/religion",
//       );

//       if (response.statusCode == 200) {
//         final lookupData = response.data['data']['data'];
//         dataReligion.addAll(lookupData);
//       }
//     } catch (e) {
//       BaseInfo.log(
//         isError: true,
//         messageTitle: "Error",
//         message: 'Something Wrong!',
//         snackPosition: SnackPosition.TOP,
//       );
//     }
//   }

//   var customerLookup = [].obs;
//   Future fetchCustomer() async {
//     try {
//       var response = await ApiService.list(
//         endpoint: '/lookup/master/customer',
//         isAuth: true,
//       );
//       if (response.statusCode == 200) {
//         final listData = response.data['data']['data'];
//         customerLookup.addAll(listData);

//         // if (kDebugMode) {
//         //   print(listData);
//         // }
//       }
//     } catch (e) {
//       BaseInfo.log(
//         isError: true,
//         messageTitle: "Error",
//         message: 'Something Wrong!',
//         snackPosition: SnackPosition.TOP,
//       );
//     }
//   }

//   var dataLookupCustomerProject = [].obs;
//   Future fetchLookupCustomerProject(customerId) async {
//     final paramsInclude = BaseGlobalFuntion.paramsInclude([
//       {'key': 'customer_id', 'value': customerId}
//     ]);
//     try {
//       var response = await ApiService.list(
//           endpoint: '/lookup/master/customer-project',
//           isAuth: true,
//           queryParams: [
//             ...paramsInclude,
//           ]);

//       if (response.statusCode == 200) {
//         final listData = response.data['data']['data'];
//         dataLookupCustomerProject.value = [...listData];
//       }
//     } catch (e) {
//       BaseInfo.log(
//         isError: true,
//         messageTitle: "Error",
//         message: 'Something Wrong!',
//         snackPosition: SnackPosition.TOP,
//       );
//     }
//   }

//   var dataDetailCustomer = ({}).obs;
//   Future fetchLookupCustomer(customerId) async {
//     // final paramsInclude = BaseGlobalFuntion.paramsInclude([
//     //   {'key': 'customer_id', 'value': customerId}
//     // ]);
//     try {
//       var response = await ApiService.list(
//         endpoint: '/view/master/customer/$customerId',
//         isAuth: true,
//       );

//       if (response.statusCode == 200) {
//         // print(response.data['data']);

//         dataDetailCustomer.value = response.data['data'];
//         // print(dataDetailCustomer);
//       }
//     } catch (e) {
//       BaseInfo.log(
//         isError: true,
//         messageTitle: "Error",
//         message: 'Something Wrong!',
//         snackPosition: SnackPosition.TOP,
//       );
//     }
//   }

//   var materialCategoryLookup = [].obs;
//   Future fetchMaterialCategory() async {
//     try {
//       var response = await ApiService.list(
//         endpoint: '/lookup/material-category',
//         isAuth: true,
//       );
//       if (response.statusCode == 200) {
//         final listData = response.data['data']['data'];
//         materialCategoryLookup.addAll(listData);
//       }
//     } catch (e) {
//       BaseInfo.log(
//         isError: true,
//         messageTitle: "Error",
//         message: 'Something Wrong!',
//         snackPosition: SnackPosition.TOP,
//       );
//     }
//   }

//   var warehouseLookup = [].obs;
//   Future fetchWarehouseData() async {
//     try {
//       var response = await ApiService.list(
//         endpoint: '/lookup/inventory/warehouse-material',
//         isAuth: true,
//       );
//       if (response.statusCode == 200) {
//         final listData = response.data['data']['data'];
//         warehouseLookup.addAll(listData);
//       }
//     } catch (e) {
//       BaseInfo.log(
//         isError: true,
//         messageTitle: "Error",
//         message: 'Something Wrong!',
//         snackPosition: SnackPosition.TOP,
//       );
//     }
//   }

//   var materialLookup = [].obs;
//   Future fetchMaterialData() async {
//     try {
//       var response = await ApiService.list(
//         endpoint: '/lookup/material',
//         isAuth: true,
//       );
//       if (response.statusCode == 200) {
//         final listData = response.data['data']['data'];
//         materialLookup.addAll(listData);

//         // print(materialLookup);
//       }
//     } catch (e) {
//       BaseInfo.log(
//         isError: true,
//         messageTitle: "Error",
//         message: 'Something Wrong!',
//         snackPosition: SnackPosition.TOP,
//       );
//     }
//   }

//   var productMutuLookup = [].obs;
//   Future fetchProductMutuLookup() async {
//     try {
//       var response = await ApiService.list(
//         endpoint: '/lookup/product',
//         isAuth: true,
//       );
//       if (response.statusCode == 200) {
//         final listData = response.data['data']['data'];
//         productMutuLookup.addAll(listData);
//       }
//     } catch (e) {
//       BaseInfo.log(
//         isError: true,
//         messageTitle: "Error",
//         message: 'Something Wrong!',
//         snackPosition: SnackPosition.TOP,
//       );
//     }
//   }

//   var vendorLookup = [].obs;
//   Future fetchVendorData() async {
//     try {
//       var response = await ApiService.list(
//         endpoint: '/lookup/vendor',
//         isAuth: true,
//       );
//       if (response.statusCode == 200) {
//         final listData = response.data['data']['data'];
//         vendorLookup.addAll(listData);
//       }
//     } catch (e) {
//       BaseInfo.log(
//         isError: true,
//         messageTitle: "Error",
//         message: 'Something Wrong!',
//         snackPosition: SnackPosition.TOP,
//       );
//     }
//   }

//   // MASTER DATA UNIT PRODUKSI
//   var dataUnitPlant = [].obs;
//   Future<void> fetchUnitPlant() async {
//     try {
//       var response = await ApiService.list(
//         isAuth: true,
//         endpoint: "/lookup/production-unit",
//       );
//       if (response.statusCode == 200) {
//         final listData = response.data['data']['data'];
//         // dataUnitPlant.addAll([
//         //   {"id": 0, "name": "Semua Unit Produksi"},
//         //   ...listData,
//         // ]);
//         dataUnitPlant.value = [
//           {"id": null, "name": "Semua"},
//           ...listData
//         ];
//       }
//     } catch (e) {
//       BaseInfo.log(
//         isError: true,
//         messageTitle: "Error",
//         message: 'Something Wrong!',
//         snackPosition: SnackPosition.TOP,
//       );
//     }
//   }

//   // MASTER DATA PRODUK KATEGORI
//   var dataProdukKategori = [].obs;
//   Future<void> fetchProdukKategori({limit = 25, offset = 0}) async {
//     try {
//       var response = await ApiService.list(
//         isAuth: true,
//         endpoint: "/lookup/product-category",
//         limit: limit,
//         page: offset,
//       );
//       if (response.statusCode == 200) {
//         final listData = response.data['data']['data'];
//         dataProdukKategori.addAll(listData);
//       }
//     } catch (e) {
//       BaseInfo.log(
//         isError: true,
//         messageTitle: "Error",
//         message: 'Something Wrong!',
//         snackPosition: SnackPosition.TOP,
//       );
//     }
//   }

//   // MASTER DATA PRODUK KATEGORI
//   var dataLookupCastingCategory = [].obs;
//   Future<void> fetchLookupCastingCategory() async {
//     try {
//       var response = await ApiService.list(
//         isAuth: true,
//         endpoint: "/lookup/casting-category",
//       );
//       if (response.statusCode == 200) {
//         final listData = response.data['data']['data'];

//         dataLookupCastingCategory.addAll(listData);
//       }
//     } catch (e) {
//       BaseInfo.log(
//         isError: true,
//         messageTitle: "Error",
//         message: 'Something Wrong!',
//         snackPosition: SnackPosition.TOP,
//       );
//     }
//   }
// }
