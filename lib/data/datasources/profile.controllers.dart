import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class ProfileControllers extends GetxController {
  // TextEditingController photoEditingController = TextEditingController();

  var isLoading = false.obs;

  @override
  void onInit() {
    myProfile();
    fetchReligion();
    super.onInit();
  }

  //* MY PROFILE
  var profileData = ({}).obs;
  Future<void> myProfile() async {
    isLoading.value = true;
    await ApiService.get(
      isAuth: true,
      endpoint: "/profile",
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final viewData = response.data['data'];
        profileData.value = viewData;
      }
    });
  }

  Future refreshDataProfile() async {
    profileData.clear();
    await myProfile();
  }

  RxMap<String, dynamic> collectLocationIdentifier = <String, dynamic>{}.obs;
  GlobalKey<FormBuilderState> profileFormEditKey =
      GlobalKey<FormBuilderState>();
  var photoUploadData = ({}).obs;

  Future<void> submitEditProfile(
      {required BuildContext loadingCtx, dynamic extendData}) async {
    // try {
    isLoading.value = true;
    if (profileFormEditKey.currentState?.saveAndValidate() ?? false) {
      var data = profileFormEditKey.currentState?.value;
      var body = {};

      // if (data!.isEmpty) {
      //   body = {
      //     // 'username':
      //     'username': profileData['username'],
      //     'name': profileData['name'],
      //     'email': profileData['email'],
      //     'phone': profileData['phone'],
      //     'address': profileData['address'],
      //     'gender': profileData['gender'],
      //     'birth_date': profileData['birth_date'],
      //     'religion_id': data['religion'],
      //     'province_id': profileData['province_id'],
      //     'city_id': profileData['city_id'],
      //     'district_id': profileData['district_id'],
      //     'sub_district_id': profileData['sub_district_id'],
      //     'photo': profileData['photo']
      //   };
      // } else {
      //   body = {
      //     ...data,
      //     'gender': data['gender'] == 'Laki-laki'
      //         ? "L"
      //         : data['gender'] == 'Perempuan'
      //             ? "P"
      //             : "O",
      //     'birth_date': data['birth_date'],
      //     'religion_id': data['religion'],
      //     'province_id': collectLocationIdentifier['provinceId'] ??
      //         profileData['province_id'],
      //     'city_id':
      //         collectLocationIdentifier['cityId'] ?? profileData['city_id'],
      //     'district_id': collectLocationIdentifier['districtId'] ??
      //         profileData['district_id'],
      //     'sub_district_id': collectLocationIdentifier['subDistrictId'] ??
      //         profileData['sub_district_id'],
      //     'photo': extendData['file'] ?? profileData['photo']
      //   };
      // }

      // if (data!.isEmpty) {
      //   body = {
      //     // 'username':
      //     'username': profileData['username'],
      //     'name': profileData['name'],
      //     'email': profileData['email'],
      //     'phone': profileData['phone'],
      //     'address': profileData['address'],
      //     'gender': profileData['gender'],
      //     'birth_date': profileData['birth_date'],
      //     'religion_id': data['religion'],
      //     'photo': profileData['photo'],
      //   };
      //   if (profileData['province_id'] != null ||
      //       profileData['province_id'] != "null" &&
      //           profileData['city_id'] != null ||
      //       profileData['city_id'] != "null" &&
      //           profileData['district_id'] != null ||
      //       profileData['district_id'] != "null" &&
      //           profileData['sub_district_id'] != null ||
      //       profileData['sub_district_id'] != 'null') {
      //     body = {
      //       ...body,
      //       'province_id': profileData['province_id'],
      //       'city_id': profileData['city_id'],
      //       'district_id': profileData['district_id'],
      //       'sub_district_id': profileData['sub_district_id'],
      //     };
      //   }
      // } else {
      //   body = {
      //     ...data,
      //     'gender': data['gender'] == 'Laki-laki'
      //         ? "L"
      //         : data['gender'] == 'Perempuan'
      //             ? "P"
      //             : "O",
      //     'birth_date': data['birth_date'],
      //     'religion_id': data['religion'],
      //     'photo': extendData['file'] ?? profileData['photo']
      //   };

      //   if (collectLocationIdentifier['provinceId'] ??
      //       profileData['province_id'] != null &&
      //           collectLocationIdentifier['cityId'] ??
      //       profileData['city_id'] != null &&
      //           collectLocationIdentifier['districtId'] ??
      //       profileData['district_id'] != null &&
      //           collectLocationIdentifier['subDistrictId'] ??
      //       profileData['sub_district_id'] != null) {
      //     body = {
      //       ...body,
      //       'province_id': collectLocationIdentifier['provinceId'] ??
      //           profileData['province_id'],
      //       'city_id':
      //           collectLocationIdentifier['cityId'] ?? profileData['city_id'],
      //       'district_id': collectLocationIdentifier['districtId'] ??
      //           profileData['district_id'],
      //       'sub_district_id': collectLocationIdentifier['subDistrictId'] ??
      //           profileData['sub_district_id'],
      //     };
      //   }
      // }

      if (data!.isEmpty) {
        body = {
          'username': profileData['username'],
          'name': profileData['name'],
          'email': profileData['email'],
          'phone': profileData['phone'],
          'address': profileData['address'],
          'gender': profileData['gender'],
          'birth_date': profileData['birth_date'],
          'photo': profileData['photo'],
        };

        bool isReligionValid = profileData['religion'] != null &&
            profileData['religion'] != "null";
        bool isProvinceValid = profileData['province_id'] != null &&
            profileData['province_id'] != "null";
        bool isCityValid =
            profileData['city_id'] != null && profileData['city_id'] != "null";
        bool isDistrictValid = profileData['district_id'] != null &&
            profileData['district_id'] != "null";
        bool isSubDistrictValid = profileData['sub_district_id'] != null &&
            profileData['sub_district_id'] != "null";

        if (isReligionValid) {
          body.addAll({
            'religion_id': data['religion'],
          });
        }

        if (isProvinceValid &&
            isCityValid &&
            isDistrictValid &&
            isSubDistrictValid) {
          body.addAll({
            'province_id': profileData['province_id'],
            'city_id': profileData['city_id'],
            'district_id': profileData['district_id'],
            'sub_district_id': profileData['sub_district_id'],
          });
        }
      } else {
        body = {
          ...data,
          'gender': data['gender'] == 'Laki-laki'
              ? "L"
              : data['gender'] == 'Perempuan'
                  ? "P"
                  : "O",
          'birth_date': data['birth_date'],
          'photo': extendData['file'] ?? profileData['photo']
        };

        bool isReligionValid =
            data['religion'] != null && data['religion'] != 'null';

        bool isProvinceValid = (collectLocationIdentifier['provinceId'] ??
                profileData['province_id']) !=
            null;
        bool isCityValid =
            (collectLocationIdentifier['cityId'] ?? profileData['city_id']) !=
                null;
        bool isDistrictValid = (collectLocationIdentifier['districtId'] ??
                profileData['district_id']) !=
            null;
        bool isSubDistrictValid = (collectLocationIdentifier['subDistrictId'] ??
                profileData['sub_district_id']) !=
            null;

        if (isReligionValid) {
          body.addAll({
            'religion_id': data['religion'],
          });
        }

        if (isProvinceValid &&
            isCityValid &&
            isDistrictValid &&
            isSubDistrictValid) {
          body.addAll({
            'province_id': collectLocationIdentifier['provinceId'] ??
                profileData['province_id'],
            'city_id':
                collectLocationIdentifier['cityId'] ?? profileData['city_id'],
            'district_id': collectLocationIdentifier['districtId'] ??
                profileData['district_id'],
            'sub_district_id': collectLocationIdentifier['subDistrictId'] ??
                profileData['sub_district_id'],
          });
        }
      }
      print(body);
      await ApiService.patch(
        isAuth: true,
        endpoint: "/change/profile",
        request: body,
      ).then((response) async {
        await BaseGlobalFuntion.loadingMoment(loadingCtx);
        isLoading.value = false;
        if (response != null) {
          Get.close(1);

          final jsonData = response.data['data'];

          profileData.value = jsonData; // Perbarui profileData dengan data baru

          Future.delayed(const Duration(milliseconds: 300), () {
            Get.put(ProfileControllers()).refreshDataProfile();
          });

          BaseInfo.log(
            isError: false,
            messageTitle: "Something Wrong!",
            message: 'profile anda berhasil diperbaharui',
            snackPosition: SnackPosition.TOP,
          );
        }
      });
    } else {
      isLoading.value = false;

      BaseInfo.log(
        isError: true,
        messageTitle: "Error Validation",
        message: 'tolong periksa kembali',
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  GlobalKey<FormBuilderState> changePwdKey = GlobalKey<FormBuilderState>();
  Future<void> changePassword({required BuildContext loadingCtx}) async {
    // try {
    var data = changePwdKey.currentState?.value;
    if (changePwdKey.currentState?.saveAndValidate() ?? false) {
      final body = {
        "current_password": data!['old_password'] ?? '',
        "password": data['new_password'] ?? '',
        "confirm_password": data['verify_password'] ?? '',
      };

      if (data['new_password'] != data['verify_password']) {
        Get.back();
      }

      // var response = await ApiService.patch(
      //   isAuth: true,
      //   endpoint: "/change/password",
      //   request: body,
      // );

      // if (response.statusCode == 200) {
      //   final responseData = response.data['data'];
      //   Get.back();
      //   Get.back();
      //   BaseInfo.log(
      //     isError: false,
      //     messageTitle: "Something Wrong!",
      //     message: '${responseData['message']}',
      //     snackPosition: SnackPosition.TOP,
      //   );
      // }
      await ApiService.patch(
        isAuth: true,
        endpoint: "/change/password",
        request: body,
      ).then((response) async {
        await BaseGlobalFuntion.loadingMoment(loadingCtx);
        isLoading.value = false;
        if (response != null) {
          final responseData = response.data['data'];
          Get.close(2);
          BaseInfo.log(
            isError: false,
            messageTitle: "Something Wrong!",
            message: '${responseData['message']}',
            snackPosition: SnackPosition.TOP,
          );
        }
      });
    } else {
      isLoading.value = false;
      Get.back();
      BaseInfo.log(
        isError: true,
        messageTitle: "Error Validation",
        message: 'periksa kembali input anda',
        snackPosition: SnackPosition.TOP,
      );
    }

    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );
    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }

  // ** DATA LOOKUP
  var dataLookupReligion = [].obs;
  Future<void> fetchReligion() async {
    await ApiService.get(
      isAuth: true,
      endpoint: "/lookup/religion",
      limit: 5,
      page: 0,
    ).then((response) {
      isLoading.value = false;
      final listData = response.data['data']['data'];

      if (listData.isNotEmpty) {
        dataLookupReligion.value = listData;
      }
    });
    // try {
    //   var response = await ApiService.list(
    //     isAuth: true,
    //     endpoint: "/lookup/religion",
    //   );

    //   if (response.statusCode == 200) {
    //     final lookupData = response.data['data']['data'];
    //     dataLookupReligion.addAll(lookupData);
    //   }
    // } on DioException catch (err) {
    //   isLoading.value = false;
    //   BaseInfo.log(
    //     isError: true,
    //     messageTitle: "Something Wrong!",
    //     message: '${err.response?.data['error_message']}',
    //     snackPosition: SnackPosition.TOP,
    //   );
    //   if (err.response?.statusCode == 401) {
    //     await BaseGlobalVariable.removeLocalVariable(keys: '', manyVariable: [
    //       'permissions',
    //       'token',
    //       'profile',
    //       'lastestProduction',
    //       'production_unit',
    //       'corporate',
    //     ]);
    //     Get.offAllNamed('/login');
    //   }
    // }
  }
}
