import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class StockReportListControllers extends GetxController {
  int limit = 25;
  int page = 0;
  @override
  void onInit() {
    fetchMaterialAnalysis();
    lookupWarehouse();
    super.onInit();
  }

  var hasMore = true.obs;
  var isLoading = false.obs;

  TextEditingController search = TextEditingController();
  GlobalKey<FormBuilderState> formKeyFilter = GlobalKey<FormBuilderState>();
  var currentFilter = ({}).obs;
  var materialAnalysis = [].obs;

  Future fetchMaterialAnalysis() async {
    var filterValue = formKeyFilter.currentState?.value;

    if (filterValue != null) {
      currentFilter.addAll(filterValue);
    }

    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': search.text},
      {'key': 'sort', 'value': 'DESC'},
      {'key': 'warehouse_id', 'value': currentFilter['warehouse']}
    ]);

    isLoading.value = true;
    await ApiService.get(
      endpoint: '/list/material-stock',
      isAuth: true,
      page: page,
      limit: limit,
      queryParams: paramsInclude,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        materialAnalysis.addAll(listData);
        page++;
      } else {
        hasMore.value = false;
      }
    });
  }

  // RELOAD DATA EQUIPMENT
  Future refreshMaterialAnalysis() async {
    page = 0;
    hasMore.value = true;
    materialAnalysis.value = [];
    await fetchMaterialAnalysis();
  }

  // ** LOOKUP DATA */
  var extendLookupItem = {"id": "null", "name": "Semua"};
  var dataLookupWarehouse = [].obs;
  Future lookupWarehouse({dynamic searching}) async {
    final paramsInclude = BaseGlobalFuntion.paramsInclude([
      {'key': 'search', 'value': searching},
    ]);
    await ApiService.get(
      endpoint: '/lookup/warehouse',
      isAuth: true,
      queryParams: paramsInclude,
      limit: 5,
      page: 0,
    ).then((response) {
      isLoading.value = false;

      if (response != null) {
        final listData = response.data['data']['data'];
        dataLookupWarehouse.value = [extendLookupItem, ...listData];
      }
    });
  }
}
