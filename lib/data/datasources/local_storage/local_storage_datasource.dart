import 'package:get_storage/get_storage.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class LocalDataSource {
  // STORE DATA IN LOCAL WITH SHAREDPREFERANCE
  static Future<void> setVariable({
    required String key,
    required value,
    bool stringList = false,
  }) async {
    final prefs = await SharedPreferences.getInstance();

    // final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    // final SharedPreferences prefs = await _prefs;

    prefs.reload();
    if (stringList) {
      prefs.reload();
      List<dynamic> values = value;
      await prefs.setStringList(key, [...values]);
    } else {
      prefs.reload();
      await prefs.setString(key, value);
    }
  }

  static Future<void> getVariable({
    required String keys,
    bool listString = false,
  }) async {
    final prefs = await SharedPreferences.getInstance();
    // final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    // final SharedPreferences? prefs = await _prefs;

    if (listString) {
      prefs.getStringList(keys);
    } else {
      prefs.getString(keys);
    }
  }

  static Future removeVariable({
    required String keys,
    List<String>? manyVariable,
  }) async {
    // final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    // final SharedPreferences? prefs = await _prefs;
    final prefs = await SharedPreferences.getInstance();

    if (manyVariable != null) {
      for (var i = 0; i < manyVariable.length; i++) {
        prefs.remove(manyVariable[i]);
      }
    } else {
      return prefs.remove(keys);
    }
  }

  // STORE DATA IN LOCAL WITH GET STORAGE
  static final localStorage = GetStorage();
  static setLocalVariable({required String key, required dynamic value}) {
    return localStorage.write(key, value);
  }

  static getLocalVariable({required String key}) {
    return localStorage.read(key);
  }

  static removeLocalVariable({
    required String keys,
    List<String>? manyVariable,
  }) {
    if (manyVariable != null) {
      for (var i = 0; i < manyVariable.length; i++) {
        localStorage.remove(manyVariable[i]);
      }
    } else {
      return localStorage.remove(keys);
    }
  }
}
