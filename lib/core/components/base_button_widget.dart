import 'dart:ui';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/core/components/base_loading_widget.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:intl/intl.dart';

class BaseButtonWidget {
  static Widget primaryIconButton({
    required VoidCallback onPressed,
    required String title,
    Widget? assetIcon,
    bool isLoading = false,
    bool isDisable = false,
    bool rightIcon = false,
    required Color kcolor,
    Color iconColor = Colors.white,
    IconData? iconData,
    double width = 150,
    double height = 50,
    double? rounded,
    TextStyle? textStyle,
    double iconSize = 20,
  }) {
    final checkIconDisable = isDisable == true
        ? Icon(iconData, size: iconSize, color: iconColor)
        : Icon(iconData, size: iconSize, color: iconColor);

    final checkIconLoading = isLoading == false
        ? iconData != null || assetIcon != null
            ? assetIcon ??
                Icon(
                  iconData,
                  size: iconSize,
                  color: iconColor,
                )
            : Icon(
                Icons.info_outline_sharp,
                color: iconColor,
              )
        : BaseLoadingWidget.circularProgress(
            width: 24,
            height: 24,
            color: iconColor,
          );

    final buttonActiveCheck =
        isDisable == true || isLoading == true ? null : onPressed;
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: AppColor.kGreyColor.withOpacity(0.2),
            offset: const Offset(0, 0),
            blurRadius: 2,
            spreadRadius: 1,
          )
        ],
      ),
      child: rightIcon
          ? ElevatedButton(
              style: BaseButtonStyle.customElevatedButtonStyle(
                kcolorButton: kcolor,
                kradius: rounded ?? 10,
              ),
              onPressed: buttonActiveCheck,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    title,
                    style: textStyle,
                  ),
                  const SizedBox(width: 1),
                  isDisable == true ? checkIconDisable : checkIconLoading,
                ],
              ),
            )
          : ElevatedButton.icon(
              style: BaseButtonStyle.customElevatedButtonStyle(
                kcolorButton: kcolor,
                kradius: rounded ?? 10,
              ),
              onPressed: buttonActiveCheck,
              label: Text(
                title,
                style: textStyle,
              ),
              icon: isDisable == true ? checkIconDisable : checkIconLoading,
            ),
    );
  }

  static Widget buttonFilter({
    required List<String> title,
    // List<Map<String, dynamic>>? activeted,
    required initialActive,
    required List<VoidCallback> onPressed,
    double height = 15.0,
    double width = 105.0,
    Color bColor = Colors.blue,
    Color contentColor = Colors.white10,
    IconData? iconData,
  }) {
    // DEFAULT POINT
    var selectedButton = 1.obs;
    selectedButton.value = initialActive;

    void stateSelected(index) {
      selectedButton.value = index + 1;
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: List.generate(
        title.length,
        (index) {
          return Padding(
            padding: EdgeInsets.only(right: 6, left: index == 0 ? 10 : 0),
            // padding: EdgeInsets.only(right: 0, left: index == 0 ? 0 : 0),
            child: Obx(
              () => ConstrainedBox(
                constraints: const BoxConstraints.tightFor(height: 35),
                child: ElevatedButton(
                  onPressed: () async {
                    onPressed[index]();
                    stateSelected(index);
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      if (iconData != null) ...[
                        Icon(
                          iconData,
                          color: contentColor,
                        ),
                      ],
                      const SizedBox(width: 3),
                      Text(
                        title[index],
                        style: BaseTextStyle.customTextStyle(
                          kcolor: selectedButton.value == index + 1
                              ? bColor
                              : contentColor,
                          kfontWeight: FontWeight.w500,
                          kfontSize: 12,
                        ),
                      )
                    ],
                  ),

                  style: ElevatedButton.styleFrom(
                    backgroundColor: selectedButton.value == index + 1
                        ? contentColor
                        : bColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    // fixedSize: Size(0, 20),
                  ),
                  // style: BaseButtonStyle.customElevatedButtonStyle(
                  //   kcolorButton:
                  //       selectedButton.value == index + 1 ? contentColor : bColor,
                  //   kradius: 7,

                  // ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  static Widget rangeDatePicker({
    required BuildContext context,
    required String title,
    required TextEditingController startDateController,
    required TextEditingController endDateController,
    required Function onChanged,
    ButtonStyle? buttonStyle,
    double height = 70.0,
    double? width,
    Color buttonColor = Colors.blueGrey,
    Color textColor = Colors.white,
  }) {
    var stringDate = ({}).obs;

    DateTime? startDate = DateTime.now();
    DateTime? endDate = DateTime.now();

    stringDate['start-date'] = startDate.subtract(const Duration(days: 7));
    stringDate['end-date'] = endDate;

    void onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
      if (args.value is PickerDateRange) {
        startDateController.text =
            DateFormat('yyyy-MM-dd').format(args.value.startDate);
        endDateController.text =
            DateFormat('yyyy-MM-dd').format(args.value.endDate ?? endDate);

        startDate = args.value.startDate ?? DateTime.now();
        endDate = args.value.endDate ?? DateTime.now();

        stringDate['start-date'] = args.value?.startDate ?? startDate;
        stringDate['end-date'] = args.value?.endDate ?? endDate;
      }
    }

    void onSaveDate() {
      final date = BaseGlobalFuntion.formatDateRange(startDate, endDate);
      stringDate['convert-date'] = date;
      startDateController.text = DateFormat('yyyy-MM-dd').format(startDate!);
      endDateController.text = DateFormat('yyyy-MM-dd').format(endDate!);
    }

    void onCancelDate() {
      stringDate.value = {};
      startDateController.clear();
      endDateController.clear();
    }

    // BUTTOn MAIN CHANGE RANGEDATE
    final defaultDatePicker = ElevatedButton(
      style: buttonStyle,
      onPressed: () => showDialog<String>(
        context: context,
        builder: (BuildContext context) => Dialog(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SfDateRangePicker(
                  onSelectionChanged: onSelectionChanged,
                  selectionMode: DateRangePickerSelectionMode.range,
                  initialSelectedRange: PickerDateRange(
                    // DateTime.now().subtract(const Duration(days: 7)),
                    // DateTime.now(),
                    startDate,
                    endDate,
                  ),
                  todayHighlightColor: AppColor.kPrimaryColor,
                  selectionColor: AppColor.kPrimaryColor,
                  rangeSelectionColor: AppColor.kUnavaliableColor,
                  startRangeSelectionColor: AppColor.kPrimaryColor,
                  endRangeSelectionColor: AppColor.kPrimaryColor,
                  rangeTextStyle: BaseTextStyle.customTextStyle(
                    kfontSize: 15,
                    kcolor: AppColor.kPrimaryColor.withOpacity(0.5),
                    kfontWeight: FontWeight.w700,
                  ),
                  selectionTextStyle: BaseTextStyle.customTextStyle(
                    kfontSize: 16,
                    kcolor: AppColor.kWhiteColor,
                  ),
                  headerStyle: DateRangePickerHeaderStyle(
                    textStyle: BaseTextStyle.customTextStyle(
                      kfontSize: 14,
                      kcolor: AppColor.kPrimaryColor,
                    ),
                  ),
                  monthViewSettings: DateRangePickerMonthViewSettings(
                    viewHeaderStyle: DateRangePickerViewHeaderStyle(
                        textStyle: BaseTextStyle.customTextStyle(
                      kfontSize: 15,
                      kcolor: AppColor.kPrimaryColor.withOpacity(0.5),
                      kfontWeight: FontWeight.w700,
                    )),
                  ),
                  monthCellStyle: DateRangePickerMonthCellStyle(
                    textStyle: BaseTextStyle.customTextStyle(
                      kfontSize: 14,
                      kcolor: AppColor.kPrimaryColor,
                    ),
                    todayTextStyle: BaseTextStyle.customTextStyle(
                      kfontSize: 14,
                      kcolor: AppColor.kPrimaryColor,
                    ),
                  ),
                  yearCellStyle: DateRangePickerYearCellStyle(
                    textStyle: BaseTextStyle.customTextStyle(
                      kfontSize: 14,
                      kcolor: AppColor.kPrimaryColor,
                    ),
                    disabledDatesTextStyle: BaseTextStyle.customTextStyle(
                      kfontSize: 14,
                      kcolor: AppColor.kGreyColor,
                    ),
                    todayTextStyle: BaseTextStyle.customTextStyle(
                      kfontSize: 15,
                      kcolor: AppColor.kPrimaryColor,
                    ),
                    leadingDatesTextStyle: BaseTextStyle.customTextStyle(
                      kfontSize: 14,
                      kcolor: AppColor.kPrimaryColor,
                    ),
                  ),
                ),
                const SizedBox(height: 15),
                Obx(
                  () => SizedBox(
                    width: double.infinity,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Row(
                          children: [
                            Container(
                              width: 15,
                              height: 15,
                              decoration: BoxDecoration(
                                color: AppColor.kSuccesColor,
                                borderRadius: BorderRadius.circular(2),
                              ),
                            ),
                            const SizedBox(width: 10),
                            Text(
                              stringDate['start-date'] == null
                                  ? ""
                                  : DateFormat('yyyy-MM-dd')
                                      .format(stringDate['start-date']),
                              style: BaseTextStyle.customTextStyle(
                                kfontWeight: FontWeight.w500,
                                kcolor: AppColor.kPrimaryColor,
                                kfontSize: 13,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Container(
                              width: 15,
                              height: 15,
                              decoration: BoxDecoration(
                                color: AppColor.kPrimaryColor,
                                borderRadius: BorderRadius.circular(2),
                              ),
                            ),
                            const SizedBox(width: 10),
                            Text(
                              stringDate['end-date'] == null
                                  ? ""
                                  : DateFormat('yyyy-MM-dd')
                                      .format(stringDate['end-date']),
                              style: BaseTextStyle.customTextStyle(
                                kfontWeight: FontWeight.w500,
                                kcolor: AppColor.kPrimaryColor,
                                kfontSize: 13,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(height: 15),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    TextButton(
                      onPressed: () async {
                        onCancelDate();
                        onChanged();
                        Get.back();
                      },
                      child: Text(
                        'Clear',
                        style: BaseTextStyle.customTextStyle(
                          kfontWeight: FontWeight.w600,
                          kcolor: AppColor.kPrimaryColor,
                          kfontSize: 15,
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: () async {
                        onSaveDate();
                        onChanged();
                        Get.back();
                      },
                      child: Text(
                        'Save',
                        style: BaseTextStyle.customTextStyle(
                          kfontWeight: FontWeight.w600,
                          kcolor: AppColor.kPrimaryColor,
                          kfontSize: 15,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
      child: Obx(
        () => Row(
          mainAxisAlignment: MainAxisAlignment.center,
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (stringDate['convert-date'] == null) ...[
              Icon(
                Icons.calendar_month_rounded,
                color: textColor,
              ),
              const SizedBox(width: 5),
            ],
            Text(
              stringDate['convert-date'] ?? title,

              // textAlign: TextAlign.right,
              style: BaseTextStyle.customTextStyle(
                kfontSize: 12,
                kfontWeight: FontWeight.w500,
                kcolor: textColor,
              ),
            ),
          ],
        ),
      ),
    );

    return defaultDatePicker;
  }

  static Widget formDatePicker({
    required BuildContext context,
    required String title,
    required TextEditingController selectDateController,
    // required Function onChanged,
    required Function({
      dynamic dateValue,
    }) onChange,
    required String fieldName,
    ButtonStyle? buttonStyle,
    double height = 70.0,
    double? width,
    Color buttonColor = Colors.blueGrey,
    Color textColor = Colors.white,
    String? currentValue,
    Function(String?)? onChangedField,
    String? Function(String?)? validator,
    bool enabled = true,
    bool enablePastDates = true,
  }) {
    DateTime? selectedDate = DateTime.now();
    // var stringDate;

    // TextEditingController temporaryDate = TextEditingController();

    // if (startDateController.text == "" || startDateController.text.isEmpty) {
    //   startDateController.text = DateFormat('yyyy-MM-dd').format(selectedDate);
    // }

    void onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
      // startDateController.text = DateFormat('yyyy-MM-dd').format(args.value);
      selectedDate =
          DateTime.parse(DateFormat('yyyy-MM-dd').format(args.value));

      // stringDate = startDateController.text;
      onChange(dateValue: selectDateController.text);

      // Get.back();
    }

    void onSaveDate() {
      if (selectedDate != null) {
        selectDateController.text =
            DateFormat('yyyy-MM-dd').format(selectedDate!);
        // print(stringDate);
        // startDateController.text = stringDate.value;
      }
      // Get.back();
    }

    void onCancelDate() {
      selectDateController.clear();
      selectedDate = null;
      onChange(dateValue: selectDateController.text);
    }

    // FORM MAIN CHANGE RANGEDATE
    final defaultDatePicker = FormBuilderTextField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      name: fieldName,
      controller: selectDateController,
      enabled: enabled,
      textAlignVertical: TextAlignVertical.top,
      readOnly: true,
      onChanged: onChangedField,
      validator: validator,
      style: BaseTextStyle.customTextStyle(
        kfontSize: 15,
        kfontWeight: FontWeight.w500,
        kcolor: AppColor.kPrimaryColor,
      ),
      decoration: BaseFormTextFieldStyle.basicDateDecoration(
        labelText: currentValue != ""
            ? currentValue ?? 'Pilih tanggal'
            : "Pilih tanggal",
        suffixIcon: const Icon(
          Icons.calendar_month_rounded,
          color: AppColor.kPrimaryColor,
        ),
      ),
      onTap: () {
        showDialog<String>(
          context: context,
          builder: (BuildContext context) => Dialog(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SfDateRangePicker(
                    view: DateRangePickerView.month,
                    onSelectionChanged: onSelectionChanged,
                    selectionMode: DateRangePickerSelectionMode.single,
                    initialSelectedDate: selectedDate,
                    enablePastDates: enablePastDates,
                    todayHighlightColor: AppColor.kPrimaryColor,
                    selectionColor: AppColor.kPrimaryColor,
                    rangeSelectionColor: AppColor.kUnavaliableColor,
                    startRangeSelectionColor: AppColor.kPrimaryColor,
                    endRangeSelectionColor: AppColor.kPrimaryColor,
                    selectionTextStyle: BaseTextStyle.customTextStyle(
                      kfontSize: 16,
                      kcolor: AppColor.kWhiteColor,
                    ),
                    headerStyle: DateRangePickerHeaderStyle(
                      textStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 14,
                        kcolor: AppColor.kPrimaryColor,
                      ),
                    ),
                    monthViewSettings: DateRangePickerMonthViewSettings(
                      viewHeaderStyle: DateRangePickerViewHeaderStyle(
                          textStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 15,
                        kcolor: AppColor.kPrimaryColor.withOpacity(0.5),
                        kfontWeight: FontWeight.w700,
                      )),
                    ),
                    monthCellStyle: DateRangePickerMonthCellStyle(
                      textStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 14,
                        kcolor: AppColor.kPrimaryColor,
                      ),
                      todayTextStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 14,
                        kcolor: AppColor.kPrimaryColor,
                      ),
                    ),
                    yearCellStyle: DateRangePickerYearCellStyle(
                      textStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 14,
                        kcolor: AppColor.kPrimaryColor,
                      ),
                      disabledDatesTextStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 14,
                        kcolor: AppColor.kGreyColor,
                      ),
                      todayTextStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 15,
                        kcolor: AppColor.kPrimaryColor,
                      ),
                      leadingDatesTextStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 14,
                        kcolor: AppColor.kPrimaryColor,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      TextButton(
                        onPressed: () async {
                          onCancelDate();
                          Get.back();
                        },
                        child: Text(
                          'Clear',
                          style: BaseTextStyle.customTextStyle(
                            kfontWeight: FontWeight.w600,
                            kcolor: AppColor.kPrimaryColor,
                            kfontSize: 13,
                          ),
                        ),
                      ),
                      TextButton(
                        onPressed: () async {
                          onSaveDate();
                          Get.back();
                        },
                        child: Text(
                          'Simpan',
                          style: BaseTextStyle.customTextStyle(
                            kfontWeight: FontWeight.w600,
                            kcolor: AppColor.kPrimaryColor,
                            kfontSize: 13,
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
    );

    return defaultDatePicker;
  }

  static Widget formDatePicker2({
    required BuildContext context,
    required String title,
    required TextEditingController selectDateController,
    // required Function onChanged,
    required Function({
      dynamic dateValue,
    }) onChange,
    required String fieldName,
    bool isRequired = false,
    String? hintText,
    ButtonStyle? buttonStyle,
    double height = 70.0,
    double? width,
    Color buttonColor = Colors.blueGrey,
    Color textColor = Colors.white,
    String? currentValue,
    Function(String?)? onChangedField,
    String? Function(String?)? validator,
    bool enabled = true,
    bool enablePastDates = true,
  }) {
    DateTime? selectedDate = DateTime.now();

    // if (currentValue != null) {
    //   selectDateController.text = currentValue;
    // }

    // print(selectDateController.text);

    if (selectDateController.text == "") {
      selectDateController.text =
          DateFormat('yyyy-MM-dd').format(DateTime.now());
    }

    void onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
      selectedDate =
          DateTime.parse(DateFormat('yyyy-MM-dd').format(args.value));
    }

    void onSaveDate() {
      if (selectedDate != null) {
        selectDateController.text =
            DateFormat('yyyy-MM-dd').format(selectedDate!);
        onChange(dateValue: selectDateController.text);
      }
    }

    void onCancelDate() {
      selectDateController.clear();
      selectedDate = null;
      onChange(dateValue: selectDateController.text);
    }

    // FORM MAIN CHANGE RANGEDATE
    final defaultDatePicker = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              title,
              style: BaseTextStyle.customTextStyle(
                kcolor: AppColor.kPrimaryColor,
                kfontSize: 12,
                kfontWeight: FontWeight.w500,
              ),
            ),
            isRequired
                ? Text(
                    "*",
                    style: BaseTextStyle.customTextStyle(
                      kcolor: AppColor.kErrorColor,
                      kfontSize: 15,
                      kfontWeight: FontWeight.w600,
                    ),
                  )
                : const SizedBox(),
          ],
        ),
        const SizedBox(height: 3),
        FormBuilderTextField(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          name: fieldName,
          controller: selectDateController,
          enabled: enabled,
          textAlignVertical: TextAlignVertical.top,
          readOnly: true,
          onChanged: onChangedField,
          validator: validator,
          style: BaseTextStyle.customTextStyle(
            kfontSize: 15,
            kfontWeight: FontWeight.w500,
            kcolor: AppColor.kPrimaryColor,
          ),
          decoration: InputDecoration(
            filled: true,
            fillColor: AppColor.kGreyColor.withOpacity(0.2),
            suffixIcon: const Icon(
              Icons.calendar_month_rounded,
              color: AppColor.kPrimaryColor,
            ),
            floatingLabelBehavior: FloatingLabelBehavior.always,
            contentPadding:
                const EdgeInsets.only(bottom: -10.0, top: 20, left: 10),
            hintText: hintText ?? 'Isi disini',
            hintStyle: hintText != null
                ? hintText.toLowerCase().contains('pilih')
                    ? BaseTextStyle.hintTextStyle
                    : BaseTextStyle.customTextStyle(
                        kcolor: AppColor.kPrimaryColor,
                        kfontSize: 14,
                        kfontWeight: FontWeight.w500,
                      )
                : BaseTextStyle.hintTextStyle,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: const BorderSide(
                color: AppColor.kPrimaryColor,
              ), //menunaktifkan borde
              // borderSide: BorderSide.none,
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: const BorderSide(
                color: AppColor.kPrimaryColor,
              ), //menunaktifkan border
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: const BorderSide(
                color: AppColor.kErrorColor,
              ), //menunaktifkan border
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: const BorderSide(
                color: AppColor.kErrorColor,
              ), //menunaktifkan border
            ),
            errorStyle: BaseTextStyle.customTextStyle(
              kfontSize: 12,
              kcolor: AppColor.kFailedColor,
              kfontWeight: FontWeight.w500,
            ),
          ),
          onTap: () {
            showDialog<String>(
              context: context,
              builder: (BuildContext context) => Dialog(
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SfDateRangePicker(
                        view: DateRangePickerView.month,
                        onSelectionChanged: onSelectionChanged,
                        selectionMode: DateRangePickerSelectionMode.single,
                        initialSelectedDate: selectedDate,
                        enablePastDates: enablePastDates,
                        todayHighlightColor: AppColor.kPrimaryColor,
                        selectionColor: AppColor.kPrimaryColor,
                        rangeSelectionColor: AppColor.kUnavaliableColor,
                        startRangeSelectionColor: AppColor.kPrimaryColor,
                        endRangeSelectionColor: AppColor.kPrimaryColor,
                        selectionTextStyle: BaseTextStyle.customTextStyle(
                          kfontSize: 16,
                          kcolor: AppColor.kWhiteColor,
                        ),
                        headerStyle: DateRangePickerHeaderStyle(
                          textStyle: BaseTextStyle.customTextStyle(
                            kfontSize: 14,
                            kcolor: AppColor.kPrimaryColor,
                          ),
                        ),
                        monthViewSettings: DateRangePickerMonthViewSettings(
                          viewHeaderStyle: DateRangePickerViewHeaderStyle(
                              textStyle: BaseTextStyle.customTextStyle(
                            kfontSize: 15,
                            kcolor: AppColor.kPrimaryColor.withOpacity(0.5),
                            kfontWeight: FontWeight.w700,
                          )),
                        ),
                        monthCellStyle: DateRangePickerMonthCellStyle(
                          textStyle: BaseTextStyle.customTextStyle(
                            kfontSize: 14,
                            kcolor: AppColor.kPrimaryColor,
                          ),
                          todayTextStyle: BaseTextStyle.customTextStyle(
                            kfontSize: 14,
                            kcolor: AppColor.kPrimaryColor,
                          ),
                        ),
                        yearCellStyle: DateRangePickerYearCellStyle(
                          textStyle: BaseTextStyle.customTextStyle(
                            kfontSize: 14,
                            kcolor: AppColor.kPrimaryColor,
                          ),
                          disabledDatesTextStyle: BaseTextStyle.customTextStyle(
                            kfontSize: 14,
                            kcolor: AppColor.kGreyColor,
                          ),
                          todayTextStyle: BaseTextStyle.customTextStyle(
                            kfontSize: 15,
                            kcolor: AppColor.kPrimaryColor,
                          ),
                          leadingDatesTextStyle: BaseTextStyle.customTextStyle(
                            kfontSize: 14,
                            kcolor: AppColor.kPrimaryColor,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          TextButton(
                            onPressed: () async {
                              onCancelDate();
                              Get.back();
                            },
                            child: Text(
                              'Clear',
                              style: BaseTextStyle.customTextStyle(
                                kfontWeight: FontWeight.w600,
                                kcolor: AppColor.kPrimaryColor,
                                kfontSize: 13,
                              ),
                            ),
                          ),
                          TextButton(
                            onPressed: () async {
                              onSaveDate();
                              Get.back();
                            },
                            child: Text(
                              'Simpan',
                              style: BaseTextStyle.customTextStyle(
                                kfontWeight: FontWeight.w600,
                                kcolor: AppColor.kPrimaryColor,
                                kfontSize: 13,
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ],
    );

    return defaultDatePicker;
  }

  static Widget formDateRangePicker({
    required BuildContext context,
    required String title,
    required TextEditingController startDateController,
    required TextEditingController endDateController,
    // required Function onChanged,
    required Function({
      dynamic startDate,
      dynamic endDate,
    }) onChange,
    required String fieldName,
    String? currentValue,
    ButtonStyle? buttonStyle,
    double height = 70.0,
    double? width,
    Color buttonColor = Colors.blueGrey,
    Color textColor = Colors.white,
    String? initialValue,
    Function(String?)? onChangedField,
    String? Function(String?)? validator,
    bool enabled = true,
    bool enablePastDates = true,
  }) {
    TextEditingController editingController = TextEditingController();

    if (currentValue != null) {
      editingController.text = currentValue;
    }

    DateTime? startDate = DateTime.now();
    DateTime? endDate = DateTime.now();

    void onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
      if (args.value is PickerDateRange) {
        startDateController.text =
            DateFormat('yyyy-MM-dd').format(args.value.startDate);
        endDateController.text =
            DateFormat('yyyy-MM-dd').format(args.value.endDate ?? endDate);

        startDate = args.value.startDate ?? DateTime.now();
        endDate = args.value.endDate ?? DateTime.now();
      }
    }

    void onSaveDate() {
      final date = BaseGlobalFuntion.formatDateRange(startDate, endDate);
      editingController.text = date;
      startDateController.text = DateFormat('yyyy-MM-dd').format(startDate!);
      endDateController.text = DateFormat('yyyy-MM-dd').format(endDate!);
      onChange(
        startDate: startDateController.text,
        endDate: endDateController.text,
      );
    }

    void onCancelDate() {
      editingController.clear();
      startDateController.clear();
      endDateController.clear();
      onChange(
        startDate: startDateController.text,
        endDate: endDateController.text,
      );
    }

    // FORM MAIN CHANGE RANGEDATE
    final defaultDatePicker = SizedBox(
      height: height,
      child: FormBuilderTextField(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        name: fieldName,
        controller: editingController,
        enabled: enabled,
        textAlignVertical: TextAlignVertical.center,
        readOnly: true,
        onChanged: onChangedField,
        validator: validator,
        style: BaseTextStyle.customTextStyle(
          kfontSize: 15,
          kfontWeight: FontWeight.w500,
          kcolor: AppColor.kPrimaryColor,
        ),
        decoration: BaseFormTextFieldStyle.basicDateDecoration(
            labelText: 'Pilih tanggal',
            suffixIcon: const Icon(
              Icons.calendar_month_rounded,
              color: AppColor.kPrimaryColor,
            )),
        onTap: () {
          showDialog<String>(
            context: context,
            builder: (BuildContext context) => Dialog(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SfDateRangePicker(
                      onSelectionChanged: onSelectionChanged,
                      selectionMode: DateRangePickerSelectionMode.range,
                      initialSelectedRange: PickerDateRange(
                        // DateTime.now().subtract(const Duration(days: 7)),
                        // DateTime.now(),
                        startDate,
                        endDate,
                      ),
                      enablePastDates: enablePastDates,
                      todayHighlightColor: AppColor.kPrimaryColor,
                      selectionColor: AppColor.kPrimaryColor,
                      rangeSelectionColor: AppColor.kUnavaliableColor,
                      startRangeSelectionColor: AppColor.kPrimaryColor,
                      endRangeSelectionColor: AppColor.kPrimaryColor,
                      selectionTextStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 16,
                        kcolor: AppColor.kWhiteColor,
                      ),
                      headerStyle: DateRangePickerHeaderStyle(
                        textStyle: BaseTextStyle.customTextStyle(
                          kfontSize: 14,
                          kcolor: AppColor.kPrimaryColor,
                        ),
                      ),
                      monthViewSettings: DateRangePickerMonthViewSettings(
                        viewHeaderStyle: DateRangePickerViewHeaderStyle(
                            textStyle: BaseTextStyle.customTextStyle(
                          kfontSize: 15,
                          kcolor: AppColor.kPrimaryColor.withOpacity(0.5),
                          kfontWeight: FontWeight.w700,
                        )),
                      ),
                      monthCellStyle: DateRangePickerMonthCellStyle(
                        textStyle: BaseTextStyle.customTextStyle(
                          kfontSize: 14,
                          kcolor: AppColor.kPrimaryColor,
                        ),
                        todayTextStyle: BaseTextStyle.customTextStyle(
                          kfontSize: 14,
                          kcolor: AppColor.kPrimaryColor,
                        ),
                      ),
                      yearCellStyle: DateRangePickerYearCellStyle(
                        textStyle: BaseTextStyle.customTextStyle(
                          kfontSize: 14,
                          kcolor: AppColor.kPrimaryColor,
                        ),
                        disabledDatesTextStyle: BaseTextStyle.customTextStyle(
                          kfontSize: 14,
                          kcolor: AppColor.kGreyColor,
                        ),
                        todayTextStyle: BaseTextStyle.customTextStyle(
                          kfontSize: 15,
                          kcolor: AppColor.kPrimaryColor,
                        ),
                        leadingDatesTextStyle: BaseTextStyle.customTextStyle(
                          kfontSize: 14,
                          kcolor: AppColor.kPrimaryColor,
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        TextButton(
                          onPressed: () async {
                            onCancelDate();
                            Get.back();
                          },
                          child: Text(
                            'Clear',
                            style: BaseTextStyle.customTextStyle(
                              kfontWeight: FontWeight.w500,
                              kcolor: AppColor.kFailedColor,
                              kfontSize: 15,
                            ),
                          ),
                        ),
                        TextButton(
                          onPressed: () async {
                            onSaveDate();
                            Get.back();
                          },
                          child: Text(
                            'Simpan',
                            style: BaseTextStyle.customTextStyle(
                              kfontWeight: FontWeight.w700,
                              kcolor: AppColor.kPrimaryColor,
                              kfontSize: 15,
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );

    return defaultDatePicker;
  }

  static Widget formDateRangePicker2({
    required BuildContext context,
    required String title,
    required TextEditingController startDateController,
    required TextEditingController endDateController,
    // required Function onChanged,
    required Function({
      dynamic startDate,
      dynamic endDate,
    }) onChange,
    required String fieldName,
    String? hintText,
    bool isRequired = false,
    String? currentValue,
    ButtonStyle? buttonStyle,
    double height = 70.0,
    double? width,
    Color buttonColor = Colors.blueGrey,
    Color textColor = Colors.white,
    String? initialValue,
    Function(String?)? onChangedField,
    String? Function(String?)? validator,
    bool enabled = true,
    bool enablePastDates = true,
  }) {
    TextEditingController editingController = TextEditingController();

    if (currentValue != null) {
      editingController.text = currentValue;
    }

    DateTime? startDate = DateTime.now();
    DateTime? endDate = DateTime.now();

    void onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
      if (args.value is PickerDateRange) {
        startDateController.text =
            DateFormat('yyyy-MM-dd').format(args.value.startDate);
        endDateController.text =
            DateFormat('yyyy-MM-dd').format(args.value.endDate ?? endDate);

        startDate = args.value.startDate ?? DateTime.now();
        endDate = args.value.endDate ?? DateTime.now();
      }
    }

    void onSaveDate() {
      final date = BaseGlobalFuntion.formatDateRange(startDate, endDate);
      editingController.text = date;
      startDateController.text = DateFormat('yyyy-MM-dd').format(startDate!);
      endDateController.text = DateFormat('yyyy-MM-dd').format(endDate!);
      onChange(
        startDate: startDateController.text,
        endDate: endDateController.text,
      );
    }

    void onCancelDate() {
      editingController.clear();
      startDateController.clear();
      endDateController.clear();
      onChange(
        startDate: startDateController.text,
        endDate: endDateController.text,
      );
    }

    // FORM MAIN CHANGE RANGEDATE
    final defaultDatePicker = SizedBox(
      height: height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                title,
                style: BaseTextStyle.customTextStyle(
                  kcolor: AppColor.kPrimaryColor,
                  kfontSize: 12,
                  kfontWeight: FontWeight.w500,
                ),
              ),
              isRequired
                  ? Text(
                      "*",
                      style: BaseTextStyle.customTextStyle(
                        kcolor: AppColor.kErrorColor,
                        kfontSize: 15,
                        kfontWeight: FontWeight.w600,
                      ),
                    )
                  : const SizedBox(),
            ],
          ),
          const SizedBox(height: 3),
          FormBuilderTextField(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            name: fieldName,
            controller: editingController,
            enabled: enabled,
            textAlignVertical: TextAlignVertical.center,
            readOnly: true,
            onChanged: onChangedField,
            validator: validator,
            style: BaseTextStyle.customTextStyle(
              kfontSize: 15,
              kfontWeight: FontWeight.w500,
              kcolor: AppColor.kPrimaryColor,
            ),
            decoration: InputDecoration(
              filled: true,
              fillColor: AppColor.kGreyColor.withOpacity(0.2),
              suffixIcon: const Icon(
                Icons.calendar_month_rounded,
                color: AppColor.kPrimaryColor,
              ),
              floatingLabelBehavior: FloatingLabelBehavior.always,
              contentPadding:
                  const EdgeInsets.only(bottom: -10.0, top: 20, left: 10),
              hintText: hintText ?? 'Isi disini',
              hintStyle: hintText != null
                  ? hintText.toLowerCase().contains('pilih')
                      ? BaseTextStyle.hintTextStyle
                      : BaseTextStyle.customTextStyle(
                          kcolor: AppColor.kPrimaryColor,
                          kfontSize: 14,
                          kfontWeight: FontWeight.w500,
                        )
                  : BaseTextStyle.hintTextStyle,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(
                  color: AppColor.kPrimaryColor,
                ), //menunaktifkan borde
                // borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(
                  color: AppColor.kPrimaryColor,
                ), //menunaktifkan border
              ),
              errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(
                  color: AppColor.kErrorColor,
                ), //menunaktifkan border
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(
                  color: AppColor.kErrorColor,
                ), //menunaktifkan border
              ),
              errorStyle: BaseTextStyle.customTextStyle(
                kfontSize: 12,
                kcolor: AppColor.kFailedColor,
                kfontWeight: FontWeight.w500,
              ),
            ),
            onTap: () {
              showDialog<String>(
                context: context,
                builder: (BuildContext context) => Dialog(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SfDateRangePicker(
                          onSelectionChanged: onSelectionChanged,
                          selectionMode: DateRangePickerSelectionMode.range,
                          initialSelectedRange: PickerDateRange(
                            // DateTime.now().subtract(const Duration(days: 7)),
                            // DateTime.now(),
                            startDate,
                            endDate,
                          ),
                          enablePastDates: enablePastDates,
                          todayHighlightColor: AppColor.kPrimaryColor,
                          selectionColor: AppColor.kPrimaryColor,
                          rangeSelectionColor: AppColor.kUnavaliableColor,
                          startRangeSelectionColor: AppColor.kPrimaryColor,
                          endRangeSelectionColor: AppColor.kPrimaryColor,
                          selectionTextStyle: BaseTextStyle.customTextStyle(
                            kfontSize: 16,
                            kcolor: AppColor.kWhiteColor,
                          ),
                          headerStyle: DateRangePickerHeaderStyle(
                            textStyle: BaseTextStyle.customTextStyle(
                              kfontSize: 14,
                              kcolor: AppColor.kPrimaryColor,
                            ),
                          ),
                          monthViewSettings: DateRangePickerMonthViewSettings(
                            viewHeaderStyle: DateRangePickerViewHeaderStyle(
                                textStyle: BaseTextStyle.customTextStyle(
                              kfontSize: 15,
                              kcolor: AppColor.kPrimaryColor.withOpacity(0.5),
                              kfontWeight: FontWeight.w700,
                            )),
                          ),
                          monthCellStyle: DateRangePickerMonthCellStyle(
                            textStyle: BaseTextStyle.customTextStyle(
                              kfontSize: 14,
                              kcolor: AppColor.kPrimaryColor,
                            ),
                            todayTextStyle: BaseTextStyle.customTextStyle(
                              kfontSize: 14,
                              kcolor: AppColor.kPrimaryColor,
                            ),
                          ),
                          yearCellStyle: DateRangePickerYearCellStyle(
                            textStyle: BaseTextStyle.customTextStyle(
                              kfontSize: 14,
                              kcolor: AppColor.kPrimaryColor,
                            ),
                            disabledDatesTextStyle:
                                BaseTextStyle.customTextStyle(
                              kfontSize: 14,
                              kcolor: AppColor.kGreyColor,
                            ),
                            todayTextStyle: BaseTextStyle.customTextStyle(
                              kfontSize: 15,
                              kcolor: AppColor.kPrimaryColor,
                            ),
                            leadingDatesTextStyle:
                                BaseTextStyle.customTextStyle(
                              kfontSize: 14,
                              kcolor: AppColor.kPrimaryColor,
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            TextButton(
                              onPressed: () async {
                                onCancelDate();
                                Get.back();
                              },
                              child: Text(
                                'Clear',
                                style: BaseTextStyle.customTextStyle(
                                  kfontWeight: FontWeight.w500,
                                  kcolor: AppColor.kFailedColor,
                                  kfontSize: 15,
                                ),
                              ),
                            ),
                            TextButton(
                              onPressed: () async {
                                onSaveDate();
                                Get.back();
                              },
                              child: Text(
                                'Simpan',
                                style: BaseTextStyle.customTextStyle(
                                  kfontWeight: FontWeight.w700,
                                  kcolor: AppColor.kPrimaryColor,
                                  kfontSize: 15,
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );

    return defaultDatePicker;
  }

  static Widget formModalLookup({
    required BuildContext context,
    String? title,
    required TextEditingController filterTextController,
    // required TextEditingController fieldTextController,
    required TextEditingController searchTextController,
    required data,
    required Map<String, dynamic> valueConsume,
    required String fieldName,
    required Function({dynamic valueItem, dynamic searchValue}) onChange,
    TextStyle? textStyle,
    bool enabled = true,
    dynamic validator,
  }) {
    // var indexSelected = 0.obs;

    TextEditingController controllerView = TextEditingController();

    var valuesView = 0.obs;

    // void setValueDropdown(dynamic lastId, dynamic newViewData, dynamic newId) {
    //   if (lastId != newId) {
    //     controllerView.text = newViewData;
    //   }
    // }

    // FORM MAIN CHANGE RANGEDATE
    final defaultDatePicker = FormBuilderTextField(
      autovalidateMode: AutovalidateMode.disabled,
      name: fieldName,
      controller: searchTextController,
      enabled: enabled,
      textAlignVertical: TextAlignVertical.top,
      readOnly: true,
      validator: validator,
      style: BaseTextStyle.customTextStyle(
        kfontSize: 15,
        kfontWeight: FontWeight.w500,
        kcolor: AppColor.kPrimaryColor,
      ),
      decoration: BaseFormTextFieldStyle.basicStyle2(labelText: title ?? ""),
      onTap: () {
        showDialog<String>(
          context: context,
          builder: (BuildContext context) => PopScope(
            canPop: true,
            child: Dialog(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          title ?? "",
                          style: BaseTextStyle.customTextStyle(
                            kfontWeight: FontWeight.w600,
                            kcolor: AppColor.kPrimaryColor,
                          ),
                        ),
                        ElevatedButton.icon(
                          icon: const Icon(Icons.restore),
                          label: Text(
                            'Reset',
                            style: textStyle,
                          ),
                          onPressed: () {
                            filterTextController.clear();
                            controllerView.clear();
                            // onChange('');
                            onChange(valueItem: '', searchValue: '');
                            Get.back();
                          },
                        ),
                      ],
                    ),
                    const SizedBox(height: 5),
                    TextFormField(
                      // controller: searchTextController,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: AppColor.kSoftGreyColor,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide.none,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(
                            color: AppColor.kPrimaryColor,
                            style: BorderStyle.solid,
                          ), //menunaktifkan border
                        ),
                      ),
                      onChanged: (value) {
                        // onChange(value);
                        onChange(valueItem: {}, searchValue: value);
                      },
                    ),
                    Obx(() => Text(valuesView.value.toString())),
                    ElevatedButton(
                        onPressed: () {
                          print(valuesView);
                          print(controllerView.text);
                        },
                        child: Text("MM")),
                    Flexible(
                      child: Obx(
                        () => ListView.builder(
                          itemCount: data.length,
                          itemBuilder: (context, index) {
                            if (index < data.length) {
                              return ListTile(
                                onTap: () {
                                  // indexSelected.value = index;

                                  filterTextController.text = data[index]
                                          [valueConsume['value']]
                                      .toString();

                                  // SET CONTROLLER TEXT VIEW FIELD
                                  // controllerView.text =
                                  //     data[index][valueConsume['view']];

                                  // MENCOBA MENYIMPAN VALUE DAN VIEW nya PDA OBS

                                  // valuesView.value =
                                  //     data[index][valueConsume['view']];
                                  // if (controllerView.text.isEmpty) {
                                  //   controllerView.text =
                                  //       data[index][valueConsume['view']];

                                  //   print('disini');
                                  //   // print("disini");
                                  // }
                                  // onChange(filterTextController.text);
                                  onChange(valueItem: {
                                    'id': filterTextController.text,
                                    'name': data[index][valueConsume['view']]
                                        .toString(),
                                  }, searchValue: '');
                                  // print(data[index][valueConsume['view']]);

                                  // searchTextController.clear();

                                  Get.back();
                                },
                                title: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      BaseGlobalFuntion.convertTextLong(
                                          text: data[index]
                                                  [valueConsume['view']]
                                              .toString(),
                                          maxLength: 20),
                                      style: textStyle,
                                    ),
                                    // if (indexSelected.value == index &&
                                    //     filterTextController
                                    //         .text.isNotEmpty) ...[
                                    //   // Text("${indexSelected.value}"),
                                    //   const Icon(
                                    //     Icons.check_circle,
                                    //     color:
                                    //         AppColor.kPrimaryColor,
                                    //   )
                                    // ]
                                  ],
                                ),
                              );
                            } else {
                              return BaseLoadingWidget.cardShimmer(
                                height: 10,
                                shimmerheight: 10,
                                width: double.infinity,
                                baseColor: AppColor.kSoftGreyColor,
                                highlightColor: AppColor.kWhiteColor,
                                itemCount: 1,
                              );
                            }
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );

    return defaultDatePicker;
  }

  // static theFuntion({
  //   required Function onChange,
  // }) {
  //   onChange = ({dynamic param1, dynamic param2}) {};
  // }

  static simpleFilter({
    required BuildContext context,
    String? title,
    required iconData,
    required TextEditingController filterTextController,
    required Function() onChanged,
    required data,
    required dataCount,
    required Map<String, dynamic> valueConsume,
    TextStyle? textStyle,
  }) {
    var indexSelected = 0.obs;

    final simple = IconButton(
      icon: iconData,
      onPressed: () => showDialog<String>(
        context: context,
        builder: (BuildContext context) => Dialog(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      title ?? "",
                      style: BaseTextStyle.customTextStyle(
                        kfontWeight: FontWeight.w600,
                        kcolor: AppColor.kPrimaryColor,
                      ),
                    ),
                    ElevatedButton.icon(
                      icon: const Icon(Icons.restore),
                      label: Text(
                        'Reset',
                        style: textStyle,
                      ),
                      onPressed: () {
                        filterTextController.clear();
                        onChanged();
                        Get.back();
                      },
                    ),
                  ],
                ),
                Expanded(
                  child: ListView.builder(
                      itemCount: dataCount,
                      itemBuilder: (context, index) => Obx(
                            () => ListTile(
                              onTap: () {
                                indexSelected.value = index;
                                filterTextController.text = data[index]
                                        [valueConsume['value']]
                                    .toString();
                                onChanged();
                                Get.back();
                              },
                              title: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    BaseGlobalFuntion.convertTextLong(
                                        text: data[index][valueConsume['view']]
                                            .toString(),
                                        maxLength: 20),
                                    style: textStyle,
                                  ),
                                  if (indexSelected.value == index &&
                                      filterTextController.text.isNotEmpty) ...[
                                    // Text("${indexSelected.value}"),
                                    const Icon(
                                      Icons.check_circle,
                                      color: AppColor.kPrimaryColor,
                                    )
                                  ]
                                ],
                              ),
                            ),
                          )),
                ),
              ],
            ),
          ),
        ),
      ),
    );

    return simple;
  }

  static filterStyle2({
    required BuildContext context,
    iconData,
    required Widget formFilter,
  }) {
    final simple = IconButton(
        icon: iconData,
        onPressed: () {
          showModalBottomSheet<void>(
              context: context,
              backgroundColor: AppColor.kGreyColor.withOpacity(0.1),
              builder: (BuildContext context) {
                return Container(
                  // height: 200,
                  padding: const EdgeInsets.all(20),
                  margin: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: AppColor.kWhiteColor,
                    borderRadius: BorderRadius.circular(25),
                  ),
                  child: formFilter,
                );
              });
        });

    return simple;
  }

  static dropdownButton({
    required fieldName,
    required valueController,
    required valueData,
    String spesificFieldView = 'name',
    bool isRequired = false,
    // String? initalPreview,
    String? label,
    double height = 50.0,
    required Function(String) searchOnChange,
    // String hintString = 'Select Item',
    dynamic value,
    // Color colorFilled = AppColor.kWhiteColor,
    String? hintText,
    String? Function(String?)? validator,
    required Function({
      dynamic valueItem,
      dynamic searchValue,
    }) onChange,
  }) {
    TextEditingController searchController = TextEditingController();

    if (value != null) {
      valueController.text = value['id'].toString();
    }

    return Stack(
      children: [
        SizedBox(
          height: height,
          child: FormBuilderTextField(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            name: fieldName,
            controller: valueController,
            enabled: true,
            readOnly: true,
            style: TextStyle(color: Colors.white.withOpacity(0.1)),
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide.none,
              ),
            ),
          ),
        ),
        SizedBox(
          // height: height,
          child: DropdownButtonFormField2<String>(
            // value: "{id: 1, name: Sample - Plant IDM}",
            value: value?.toString(),
            isExpanded: true,
            alignment: Alignment.center,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: validator,

            items: List.generate(
              valueData.length,
              (index) => DropdownMenuItem<String>(
                onTap: () {
                  onChange(
                    valueItem: "${valueData[index]['id']}",
                    searchValue: "${valueData[index][spesificFieldView]}",
                  );
                  valueController.text = "${valueData[index]['id']}";
                },
                value: {
                  'id': valueData[index]['id'],
                  'name': valueData[index][spesificFieldView]
                }.toString(),
                child: SizedBox(
                  width: double.infinity,
                  child: Text(
                    valueData[index][spesificFieldView].toString(),
                    maxLines: 1,
                    style: BaseTextStyle.customTextStyle(
                      kcolor: AppColor.kPrimaryColor,
                      kfontSize: 16,
                      kfontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ),
            ),
            onChanged: (value) {
              // print(value);
              // print("ini value di AAA");
              // onChange(searchValue: value);
              // print(value);
            },
            onSaved: (value) {
              // print(value);
              // selectedValue.value = value.toString();
            },
            iconStyleData: const IconStyleData(
              icon: Icon(
                Icons.arrow_drop_down,
                color: Colors.blueGrey,
                size: 20,
              ),
            ),

            menuItemStyleData: const MenuItemStyleData(
              height: 40,
            ),

            dropdownSearchData: DropdownSearchData(
              searchController: searchController,
              searchInnerWidgetHeight: 50,
              searchInnerWidget: Container(
                height: 40,
                margin: const EdgeInsets.only(
                  top: 8,
                  bottom: 0,
                  right: 5,
                  left: 5,
                ),
                child: TextFormField(
                  onChanged: (value) {
                    // if (searchOnChange != null) {
                    // }
                    // print("MASUK");
                    // print(value);
                    // print(valueData);
                    searchOnChange(value);
                  },
                  expands: true,
                  maxLines: null,
                  controller: searchController,
                  decoration: InputDecoration(
                    isDense: true,
                    contentPadding: const EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical: 8,
                    ),
                    hintText: 'Search for an item...',
                    hintStyle: BaseTextStyle.customTextStyle(
                      kfontSize: 12,
                      kfontWeight: FontWeight.w500,
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                  ),
                ),
              ),
              searchMatchFn: (item, searchValue) {
                bool findName(String jsonString, String? searchTerm) {
                  if (searchTerm != null || jsonString.isNotEmpty) {
                    // CHANGE STRING DATA KEY VALUE WITH ""
                    jsonString = jsonString = jsonString.replaceAllMapped(
                      RegExp(r'(\w+):([^,}\s]+)', multiLine: true),
                      (match) => '"${match.group(1)}":"${match.group(2)}"',
                    );

                    // CHANGE STRING DATA TO JSON DATA
                    dynamic jsonData = jsonDecode(jsonString);

                    // RETURN DATA SEARCH *BOOLEAN*
                    return jsonData['name'].toLowerCase().contains(searchTerm);
                  }

                  return true;
                }

                return findName(
                    item.value.toString().replaceAll(' ', ''), searchValue);
              },
            ),
            //This to clear the search value when you close the menu
            onMenuStateChange: (isOpen) {
              if (!isOpen) {
                searchController.clear();
              }
            },

            decoration: InputDecoration(
              filled: true,
              fillColor: AppColor.kGreyColor.withOpacity(0.2),
              floatingLabelBehavior: FloatingLabelBehavior.always,
              contentPadding:
                  const EdgeInsets.only(bottom: -10.0, top: 20, left: 10),
              label: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 5.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        label ?? "",
                        style: BaseTextStyle.customTextStyle(
                          kcolor: AppColor.kPrimaryColor,
                          kfontSize: 14,
                          kfontWeight: FontWeight.w500,
                        ),
                      ),
                      isRequired
                          ? Text(
                              "*",
                              style: BaseTextStyle.customTextStyle(
                                kcolor: AppColor.kErrorColor,
                                kfontSize: 15,
                                kfontWeight: FontWeight.w600,
                              ),
                            )
                          : const SizedBox(),
                    ],
                  )),

              // border: OutlineInputBorder(
              //   borderRadius: BorderRadius.circular(10),
              //   borderSide: BorderSide.none,
              // ),
              hintText: hintText ?? "Isi disini",
              hintStyle: hintText != null
                  ? hintText.toLowerCase().contains('pilih')
                      ? BaseTextStyle.hintTextStyle
                      : BaseTextStyle.customTextStyle(
                          kcolor: AppColor.kPrimaryColor,
                          kfontSize: 14,
                          kfontWeight: FontWeight.w500,
                        )
                  : BaseTextStyle.hintTextStyle,
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(
                  color: AppColor.kPrimaryColor,
                ), //menunaktifkan borde
                // borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(
                  color: AppColor.kPrimaryColor,
                ), //menunaktifkan border
              ),
              errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(
                  color: AppColor.kErrorColor,
                ), //menunaktifkan border
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(
                  color: AppColor.kErrorColor,
                ), //menunaktifkan border
              ),
              errorStyle: BaseTextStyle.customTextStyle(
                kfontSize: 12,
                kcolor: AppColor.kFailedColor,
                kfontWeight: FontWeight.w500,
              ),
            ),
            dropdownStyleData: DropdownStyleData(
              width: 320,
              maxHeight: 160,
              padding: const EdgeInsets.all(0.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
            ),
          ),
        ),
      ],
    );
  }

  static dropdownButton2({
    required fieldName,
    required valueController,
    required valueData,
    String spesificFieldView = 'name',
    bool isRequired = false,
    String? label,
    double height = 50.0,
    required Function(String) searchOnChange,
    String? hintText,
    String? Function(dynamic)? validator,
    required Function({
      dynamic valueItem,
      dynamic searchValue,
    }) onChange,
    dynamic value,
    String? currentValue,
  }) {
    TextEditingController currentData = TextEditingController(
      text: currentValue,
    );

    TextEditingController searchController = TextEditingController();

    if (value == null || value.isEmpty) {
      value = null;
    } else {
      if (value != null) {
        valueController.text = value['id'].toString();
      }
    }

    return Stack(
      children: [
        SizedBox(
          height: height,
          child: FormBuilderTextField(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            name: fieldName,
            controller: valueController,
            enabled: true,
            readOnly: true,
            style: TextStyle(color: Colors.white.withOpacity(0.1)),
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide.none,
              ),
            ),
          ),
        ),
        SizedBox(
          // height: height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    label ?? "",
                    style: BaseTextStyle.customTextStyle(
                      kcolor: AppColor.kPrimaryColor,
                      kfontSize: 12,
                      kfontWeight: FontWeight.w500,
                    ),
                  ),
                  isRequired
                      ? Text(
                          "*",
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kErrorColor,
                            kfontSize: 15,
                            kfontWeight: FontWeight.w600,
                          ),
                        )
                      : const SizedBox(),
                ],
              ),
              const SizedBox(height: 2),
              // Obx(
              //   () {
              // print(valueData);
              // return
              DropdownButtonFormField2<dynamic>(
                // value: "{id: 1, name: Sample - Plant IDM}",
                value: value?.toString(),
                isExpanded: true,
                alignment: Alignment.center,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: validator,

                items: List.generate(valueData.length, (index) {
                  // print(value);
                  // print(valueData[index]);
                  return DropdownMenuItem<dynamic>(
                    onTap: () {
                      onChange(
                        valueItem: "${valueData[index]['id']}",
                        searchValue: "${valueData[index][spesificFieldView]}",
                      );
                      valueController.text = "${valueData[index]['id']}";
                    },
                    value: {
                      'id': valueData[index]['id'],
                      'name': valueData[index][spesificFieldView]
                    }.toString(),
                    child: SizedBox(
                      width: double.infinity,
                      child: Text(
                        valueData[index][spesificFieldView].toString(),
                        maxLines: 1,
                        style: BaseTextStyle.customTextStyle(
                          kcolor: AppColor.kPrimaryColor,
                          kfontSize: 16,
                          kfontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  );
                }),
                onChanged: (value) {
                  // print(value);
                  // value = value;
                  // print(value);
                  // print("ini value di AAA");
                  // onChange(searchValue: value);
                  // print(value);
                },
                onSaved: (value) {
                  // print(value);
                  // selectedValue.value = value.toString();
                },
                iconStyleData: const IconStyleData(
                  icon: Icon(
                    Icons.arrow_drop_down,
                    color: Colors.blueGrey,
                    size: 20,
                  ),
                ),

                menuItemStyleData: const MenuItemStyleData(
                  height: 40,
                ),

                dropdownSearchData: DropdownSearchData(
                  searchController: searchController,
                  searchInnerWidgetHeight: 50,
                  searchInnerWidget: Container(
                    height: 40,
                    margin: const EdgeInsets.only(
                      top: 8,
                      bottom: 0,
                      right: 5,
                      left: 5,
                    ),
                    child: TextFormField(
                      onChanged: (value) {
                        // if (searchOnChange != null) {
                        // }
                        // print("MASUK");
                        // print(value);
                        // print(valueData);

                        searchOnChange(value);
                      },
                      expands: true,
                      maxLines: null,
                      controller: searchController,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: const EdgeInsets.symmetric(
                          horizontal: 10,
                          vertical: 8,
                        ),
                        hintText: 'Cari...',
                        hintStyle: BaseTextStyle.customTextStyle(
                          kfontSize: 12,
                          kfontWeight: FontWeight.w500,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                    ),
                  ),
                  // searchMatchFn: (item, searchValue) {
                  //   bool findName(String jsonString, String? searchTerm) {
                  //     if (searchTerm != null || jsonString.isNotEmpty) {
                  //       // CHANGE STRING DATA KEY VALUE WITH ""
                  //       jsonString = jsonString = jsonString.replaceAllMapped(
                  //         RegExp(r'(\w+):([^,}\s]+)', multiLine: true),
                  //         (match) =>
                  //             '"${match.group(1)}":"${match.group(2)}"',
                  //       );

                  //       // CHANGE STRING DATA TO JSON DATA
                  //       dynamic jsonData = jsonDecode(jsonString);

                  //       // RETURN DATA SEARCH *BOOLEAN*
                  //       return jsonData['name']
                  //           .toLowerCase()
                  //           .contains(searchTerm);
                  //     }

                  //     return true;
                  //   }

                  //   return findName(item.value.toString().replaceAll(' ', ''),
                  //       searchValue);
                  // },
                ),
                //This to clear the search value when you close the menu
                onMenuStateChange: (isOpen) {
                  if (!isOpen) {
                    searchController.clear();
                  }
                },

                decoration: InputDecoration(
                  filled: true,
                  fillColor: AppColor.kGreyColor2,
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  contentPadding:
                      const EdgeInsets.only(bottom: -10.0, top: 20, left: 10),
                  hintText: currentData.text == ""
                      ? hintText ?? "Isi disini"
                      : currentData.text,
                  hintStyle: hintText != null
                      ? hintText.toLowerCase().contains('pilih') &&
                              currentData.text == ""
                          ? BaseTextStyle.hintTextStyle
                          : BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kPrimaryColor,
                              kfontSize: 14,
                              kfontWeight: FontWeight.w500,
                            )
                      : BaseTextStyle.hintTextStyle,
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                        color: Colors.grey), //menunaktifkan borde
                    // borderSide: BorderSide.none,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                        color: Colors.grey), //menunaktifkan border
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                      color: AppColor.kErrorColor,
                    ), //menunaktifkan border
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                      color: AppColor.kErrorColor,
                    ), //menunaktifkan border
                  ),
                  errorStyle: BaseTextStyle.customTextStyle(
                    kfontSize: 12,
                    kcolor: AppColor.kFailedColor,
                    kfontWeight: FontWeight.w500,
                  ),
                ),
                dropdownStyleData: DropdownStyleData(
                  width: 320,
                  maxHeight: 160,
                  padding: const EdgeInsets.all(0.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                  ),
                ),
                // );
                // },
              ),
            ],
          ),
        ),
      ],
    );
  }

  static dropdownSearch({
    // required dynamic valueDropdown,
    required Future<List<dynamic>> Function(String)? asyncItems,
    required String fieldName,
    required controller,
    required Function({
      dynamic data,
      dynamic valueItem,
      dynamic searchValue,
    }) onChange,
    String spesificFieldView = 'name',
    String? Function(dynamic)? validator,
    dynamic initialValue,
    String? hintText,
    String? label,
    String? currentValue,
    bool isRequired = false,
    bool enabled = true,
  }) {
    TextEditingController currentData = TextEditingController(
      text: currentValue,
    );

    // print(controller.text);
    // print(currentData.text == "");
    // print(controller.text == "");
    if (initialValue != null) {
      // print(initialValue);
      controller.text = initialValue['id'].toString();
      currentData.text = initialValue['name'];
      // controller.text = currentValue['id'].toString();
      // print(initialValue);
    }
    // if (value == null || value.isEmpty) {
    //   value = null;
    // } else {
    //   if (value != null) {
    //     controller.text = value['id'].toString();
    //   }
    // }

    return Stack(
      children: [
        SizedBox(
          height: 50,
          child: FormBuilderTextField(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            name: fieldName,
            controller: controller,
            enabled: true,
            readOnly: true,
            style: TextStyle(color: Colors.white.withOpacity(0.1)),
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide.none,
              ),
            ),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  label ?? "",
                  style: BaseTextStyle.customTextStyle(
                    kcolor: AppColor.kPrimaryColor,
                    kfontSize: 12,
                    kfontWeight: FontWeight.w600,
                  ),
                ),
                isRequired
                    ? Text(
                        "*",
                        style: BaseTextStyle.customTextStyle(
                          kcolor: AppColor.kErrorColor,
                          kfontSize: 15,
                          kfontWeight: FontWeight.w600,
                        ),
                      )
                    : const SizedBox(),
              ],
            ),
            const SizedBox(height: 2),
            DropdownSearch(
              autoValidateMode: AutovalidateMode.onUserInteraction,
              asyncItems: asyncItems,
              selectedItem: initialValue,
              enabled: enabled,

              // (String filter) async {
              //   salesListController.lookupCustomer(searching: filter);
              //   return salesListController.dataCustomer;
              // },
              itemAsString: (u) {
                if (spesificFieldView != 'name') {
                  return u?[spesificFieldView];
                } else {
                  return u?['name'];
                }
              },
              onChanged: (data) {
                if (spesificFieldView != 'name') {
                  onChange(
                    data: data,
                    searchValue: data['name'],
                    valueItem: data['id'],
                  );
                } else {
                  onChange(
                    data: data,
                    searchValue: data[spesificFieldView],
                    valueItem: data['id'],
                  );
                }
                controller.text = "${data['id']}";
              },
              dropdownBuilder: (context, selectedItem) {
                return selectedItem != null
                    ? Text(
                        (spesificFieldView != 'name')
                            ? selectedItem[spesificFieldView]
                            : selectedItem?['name'],
                        style: BaseTextStyle.customTextStyle(
                          kcolor: AppColor.kPrimaryColor,
                          kfontSize: 12,
                          kfontWeight: FontWeight.w500,
                        ),
                      )
                    : Text(
                        currentData.text == ""
                            ? hintText ?? "Isi disini"
                            : currentData.text,
                        style: hintText != null
                            ? hintText.toLowerCase().contains('pilih') &&
                                    currentData.text == ""
                                ? BaseTextStyle.hintTextStyle
                                : BaseTextStyle.customTextStyle(
                                    kcolor: AppColor.kPrimaryColor,
                                    kfontSize: 12,
                                    kfontWeight: FontWeight.w500,
                                  )
                            : BaseTextStyle.hintTextStyle,
                      );
              },
              compareFn: (item1, item2) {
                return true;
              },

              popupProps: PopupProps.menu(
                isFilterOnline: true,
                showSearchBox: true,
                showSelectedItems: true,
                searchDelay: const Duration(milliseconds: 500),
                searchFieldProps: TextFieldProps(
                    decoration: InputDecoration(
                  constraints: const BoxConstraints(maxHeight: 45),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                      color: AppColor.kGreyColor,
                    ), //menunaktifkan borde
                    // borderSide: BorderSide.none,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                      color: AppColor.kGreyColor,
                    ), //menunaktifkan borde
                    // borderSide: BorderSide.none,
                  ),
                  hintText: 'Pencarian..',
                  hintStyle: BaseTextStyle.hintTextStyle,
                )),
              ),
              validator: validator,
              dropdownDecoratorProps: DropDownDecoratorProps(
                dropdownSearchDecoration: InputDecoration(
                  filled: true,
                  fillColor: enabled
                      ? AppColor.kGreyColor2
                      : const Color.fromARGB(177, 225, 225, 225),
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  contentPadding:
                      const EdgeInsets.only(bottom: -10.0, top: 20, left: 10),
                  hintText: currentData.text == ""
                      ? hintText ?? "Isi disini"
                      : currentData.text,
                  hintStyle: hintText != null
                      ? hintText.toLowerCase().contains('pilih') &&
                              currentData.text == ""
                          ? BaseTextStyle.hintTextStyle
                          : BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kPrimaryColor,
                              kfontSize: 14,
                              kfontWeight: FontWeight.w500,
                            )
                      : BaseTextStyle.hintTextStyle,
                  disabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                        color: Colors.grey), //menunaktifkan borde
                    // borderSide: BorderSide.none,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                        color: Colors.grey), //menunaktifkan borde
                    // borderSide: BorderSide.none,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                        color: Colors.grey), //menunaktifkan border
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                      color: AppColor.kErrorColor,
                    ), //menunaktifkan border
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                      color: AppColor.kErrorColor,
                    ), //menunaktifkan border
                  ),
                  errorStyle: BaseTextStyle.customTextStyle(
                    kfontSize: 12,
                    kcolor: AppColor.kFailedColor,
                    kfontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  static Widget timePicker({
    required BuildContext context,
    required String fieldName,
    required TextEditingController valueController,
    String title = 'Jam',
    String? hintText,
    bool isRequired = false,
    double? width,
    Function(String?)? onChangedField,
    bool enabled = true,
    double height = 60.0,
    Color textButtonColor = Colors.blue,
    Color buttonPickerColor = Colors.blue,
    String? Function(String?)? validator,
  }) {
    var date = DateTime.now().obs;
    var temporaryValue = ''.obs;

    if (valueController.text == "") {
      valueController.text = DateFormat('HH:mm').format(DateTime.now());
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              title,
              style: BaseTextStyle.customTextStyle(
                kcolor: AppColor.kPrimaryColor,
                kfontSize: 12,
                kfontWeight: FontWeight.w500,
              ),
            ),
            isRequired
                ? Text(
                    "*",
                    style: BaseTextStyle.customTextStyle(
                      kcolor: AppColor.kErrorColor,
                      kfontSize: 15,
                      kfontWeight: FontWeight.w600,
                    ),
                  )
                : const SizedBox(),
          ],
        ),
        const SizedBox(height: 3),
        FormBuilderTextField(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          name: fieldName,
          controller: valueController,
          enabled: enabled,
          textAlignVertical: TextAlignVertical.top,
          readOnly: true,
          onChanged: onChangedField,
          validator: validator,
          style: BaseTextStyle.customTextStyle(
            kfontSize: 15,
            kfontWeight: FontWeight.w500,
            kcolor: AppColor.kPrimaryColor,
          ),
          decoration: InputDecoration(
            filled: true,
            fillColor: AppColor.kGreyColor.withOpacity(0.2),
            suffixIcon: const Icon(
              Icons.access_time,
              color: AppColor.kPrimaryColor,
            ),
            floatingLabelBehavior: FloatingLabelBehavior.always,
            contentPadding:
                const EdgeInsets.only(bottom: -10.0, top: 20, left: 10),
            hintText: hintText ?? 'Isi disini',
            hintStyle: hintText != null
                ? hintText.toLowerCase().contains('pilih')
                    ? BaseTextStyle.hintTextStyle
                    : BaseTextStyle.customTextStyle(
                        kcolor: AppColor.kPrimaryColor,
                        kfontSize: 14,
                        kfontWeight: FontWeight.w500,
                      )
                : BaseTextStyle.hintTextStyle,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: const BorderSide(
                color: AppColor.kPrimaryColor,
              ), //menunaktifkan borde
              // borderSide: BorderSide.none,
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: const BorderSide(
                color: AppColor.kPrimaryColor,
              ), //menunaktifkan border
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: const BorderSide(
                color: AppColor.kErrorColor,
              ), //menunaktifkan border
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: const BorderSide(
                color: AppColor.kErrorColor,
              ), //menunaktifkan border
            ),
            errorStyle: BaseTextStyle.customTextStyle(
              kfontSize: 12,
              kcolor: AppColor.kFailedColor,
              kfontWeight: FontWeight.w500,
            ),
          ),
          onTap: () {
            showCupertinoModalPopup<void>(
              context: context,
              builder: (BuildContext context) => Container(
                height: 300,
                padding: const EdgeInsets.only(top: 6.0),
                // The Bottom margin is provided to align the popup above the system
                // navigation bar.
                margin: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom,
                ),
                // Provide a background color for the popup.
                decoration: BoxDecoration(
                  color: CupertinoColors.systemBackground.resolveFrom(context),
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15),
                  ),
                ),
                // Use a SafeArea widget to avoid system overlaps.
                child: SafeArea(
                  top: false,
                  child: Container(
                    height: 200,
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      children: [
                        Flexible(
                          child: CupertinoTheme(
                            data: CupertinoThemeData(
                              textTheme: CupertinoTextThemeData(
                                  dateTimePickerTextStyle:
                                      BaseTextStyle.customTextStyle(
                                kcolor: AppColor.kPrimaryColor,
                                kfontSize: 15,
                                kfontWeight: FontWeight.w500,
                              )),
                            ),
                            child: CupertinoDatePicker(
                              backgroundColor: AppColor.kWhiteColor,
                              initialDateTime: date.value,
                              mode: CupertinoDatePickerMode.time,
                              use24hFormat: true,
                              // This shows day of week alongside day of month
                              // showDayOfWeek: true,
                              // This is called when the user changes the date.
                              onDateTimeChanged: (DateTime newDate) {
                                temporaryValue.value =
                                    DateFormat('HH:mm').format(newDate);
                              },
                            ),
                          ),
                        ),
                        const SizedBox(height: 15),
                        ElevatedButton(
                          onPressed: () {
                            if (temporaryValue.isEmpty) {
                              valueController.text =
                                  DateFormat('HH:mm').format(DateTime.now());
                            } else {
                              valueController.text = temporaryValue.value;
                            }
                            Get.back();
                          },
                          style: ElevatedButton.styleFrom(
                              side: const BorderSide(
                                width: 1.0,
                                color: AppColor.kPrimaryColor,
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              )),
                          child: Text(
                            "Pilih Jam",
                            style: BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kPrimaryColor,
                              kfontSize: 13,
                              kfontWeight: FontWeight.w500,
                            ),
                          ),
                          // style: customElevatedButtonStyle(
                          //   kcolorButton: buttonPickerColor,
                          //   kradius: 7,
                          // ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
