// ignore_for_file: use_build_context_synchronously

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_place_picker_mb/google_maps_place_picker.dart';
import 'package:rbs_mobile_operation/core/constants/variables.dart';
import 'package:rbs_mobile_operation/data/datasources/profile.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class BaseExtendWidget {
  static PreferredSize appBar({
    required String title,
    required BuildContext context,
    // double preferredSize = 100.0,
    bool autoFocusSearch = false,
    // FocusNode? focusNode,
    // List<Widget>? actions,

    void Function()? onBack,
    Function()? extendBack,
    bool centerTitle = false,
    bool isAllFill = false,
    bool isSearchFill = false,
    String searchText = "Cari",
    IconButton? extendIconButton,
    TextEditingController? searchController,
    void Function()? onSearchChange,
    void Function()? onFillterChange,
  }) {
    var isShowSearch = false.obs;
    var isChangedIcon = false.obs;

    final searchTextField = Obx(() {
      return isShowSearch.isTrue
          ? Flexible(
              child: BaseTextFieldWidget.roundedStyleTextField(
                // focusNode: focusNode,
                textEditingController:
                    searchController ?? TextEditingController(),
                onChange: (val) {
                  onSearchChange!();
                  isChangedIcon.value = searchController!.text.isNotEmpty;
                },
                autoFocus: autoFocusSearch,
                prefixIcon: onBack == null
                    ? null
                    : IconButton(
                        iconSize: 15,
                        onPressed: onBack,
                        icon: const Icon(
                          Icons.arrow_back_ios_rounded,
                          color: AppColor.kGreyColor,
                        ),
                      ),
                suffixIcon: IconButton(
                  iconSize: 15,
                  onPressed: () {
                    isShowSearch.value = false;
                    FocusManager.instance.primaryFocus?.unfocus();
                    // focusNode!.unfocus();
                  },
                  icon: const Icon(
                    Icons.close,
                    color: AppColor.kGreyColor,
                  ),
                ),
                radius: 8,
                height: 35,
                hintText: searchText,
              ),
            )
          : const SizedBox();
    });

    // final topPadding = MediaQuery.of(context).padding.top;
    return PreferredSize(
      preferredSize: const Size.fromHeight(100),
      // preferredSize: const Size.fromHeight(100),
      child: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            AppBar(
              elevation: 0,
              automaticallyImplyLeading: false,
              titleSpacing: 5,
              toolbarHeight: 75,
              primary: false,
              centerTitle: centerTitle,
              backgroundColor: AppColor.kPrimaryColor,
              title: Text(
                title,
                style: BaseTextStyle.customTextStyle(
                  kfontSize: 13,
                  kcolor: AppColor.kWhiteColor,
                  kfontWeight: FontWeight.w700,
                ),
              ),
              leading: IconButton(
                icon: const Icon(
                  Icons.chevron_left,
                  color: AppColor.kWhiteColor,
                  size: 25,
                ),
                onPressed: () {
                  if (extendBack != null) extendBack();
                  Get.back();
                },
              ),

              // actions: actions,
              actions: [
                if (extendIconButton != null) ...[extendIconButton],
                if (isAllFill || isSearchFill == true) ...[
                  Obx(() {
                    if (MediaQuery.of(context).viewInsets.bottom != 0) {
                      isShowSearch.value = true;
                      // MediaQuery.of(context).viewInsets.bottom != 0;
                    }

                    // if (MediaQuery.of(context).viewInsets.bottom != 0
                    //     isShowSearch.value == true) {
                    //   isShowSearch.value =
                    //       MediaQuery.of(context).viewInsets.bottom != 0;
                    // }
                    // // else {
                    // //   isShowSearch.value =
                    // //       MediaQuery.of(context).viewInsets.bottom != 0;
                    // // }

                    // if (isShowSearch.value == false) {
                    //   WidgetsBinding.instance.addPostFrameCallback((_) {
                    //     isShowSearch.value = true;
                    //   });
                    // }

                    return Container(
                      height: 80,
                      padding: const EdgeInsets.only(
                        // top: 15.0,
                        // bottom: 2,
                        right: 10.0,
                        left: 10.0,
                      ),
                      // width: 200,
                      width: isShowSearch.isTrue
                          ? MediaQuery.of(context).size.width
                          // ? double.infinity
                          : null,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          searchTextField,
                          SizedBox(width: isShowSearch.isFalse ? 5 : 0),
                          isShowSearch.isFalse
                              ? ConstrainedBox(
                                  constraints: const BoxConstraints.tightFor(
                                      height: 35, width: 35),
                                  child: IconButton.filled(
                                    iconSize: 19,
                                    color: AppColor.kWhiteColor,
                                    onPressed: () {
                                      isShowSearch.value = true;

                                      // focusNode!.requestFocus();
                                      // autoFocusSearch = true;
                                    },
                                    icon: const Icon(Icons.search),
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              : const SizedBox(),
                          SizedBox(width: isSearchFill == false ? 5 : 0),
                          if (isSearchFill == false)
                            ConstrainedBox(
                              constraints: const BoxConstraints.tightFor(
                                  height: 35, width: 35),
                              child: IconButton.filled(
                                iconSize: 19,
                                color: AppColor.kWhiteColor,
                                onPressed: () {
                                  onFillterChange!();
                                },
                                icon: const Icon(Icons.filter_list),
                                style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                        ],
                      ),
                    );
                  }),
                ]
              ],
            ),
          ],
        ),
      ),
    );
  }

  static AppBar appBarBottomTab({
    required String title,
    List<Widget>? actions,
    TabBar? bottomTabbar,
  }) {
    return AppBar(
      title: Text(
        title,
        style: BaseTextStyle.customTextStyle(
          kfontSize: 14,
          kcolor: AppColor.kWhiteColor,
          kfontWeight: FontWeight.w700,
        ),
      ),
      bottom: bottomTabbar,
      leading: IconButton(
        icon: const Icon(
          Icons.chevron_left,
          color: AppColor.kWhiteColor,
          size: 25,
        ),
        onPressed: () => Get.back(),
      ),
      backgroundColor: AppColor.kPrimaryColor,
      actions: actions,
      flexibleSpace: Container(
        decoration: const BoxDecoration(
          color: AppColor.kPrimaryColor,
        ),
      ),
    );
  }

  static Widget viewPhoto({
    required BuildContext context,
    required ImageProvider imageProvider,
    BoxDecoration? backgroundDecoration,
    dynamic minScale,
    dynamic maxScale,
  }) {
    return Container(
      constraints: BoxConstraints.expand(
        height: MediaQuery.of(context).size.height,
      ),
      child: PhotoView(
        imageProvider: imageProvider,
        backgroundDecoration: backgroundDecoration,
        minScale: minScale,
        maxScale: maxScale,
        heroAttributes: const PhotoViewHeroAttributes(tag: "someTag"),
      ),
    );
  }

  // static Widget contentText({
  //   required String content,
  //   int limitText = 15,
  //   Color textColor = Colors.black,
  //   FontWeight textWeight = FontWeight.w500,
  //   double textSize = 15,
  // }) {
  //   String truncatedText = content.length > limitText
  //       ? "${content.substring(0, limitText)}..."
  //       : content;

  //   return Text(
  //     truncatedText,
  //     style: BaseTextStyle.customTextStyle(
  //       kcolor: textColor,
  //       kfontWeight: textWeight,
  //       kfontSize: textSize,
  //     ),
  //   );
  // }

  static Widget detailContent({
    String? title,
    required List<Map<String, dynamic>> data,
    bool isEdit = false,
    Color titleColor = Colors.black,
    double borderRadius = 10,
    bool isLoading = true,
    TextEditingController? textEditingController,
  }) {
    final contentWidgets = data
        .map((row) => isLoading == true
            ? Shimmer.fromColors(
                baseColor: AppColor.kSoftGreyColor,
                highlightColor: AppColor.kWhiteColor,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 90,
                      height: 20,
                      margin: const EdgeInsets.only(top: 10),
                      decoration: BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: 20,
                      margin: const EdgeInsets.only(top: 10),
                      decoration: BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                  ],
                ))
            : Container(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BaseTextWidget.customText(
                      text: row['header'],
                      color: AppColor.kPrimaryColor,
                      fontSize: 13,
                      fontWeight: FontWeight.w600,
                      maxLengthText: 25,
                    ),
                    if (row['content'] != null) ...[
                      if (row['content'] is String) ...{
                        BaseTextWidget.customText(
                          text: row['content'],
                          color: AppColor.kPrimaryColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                          isLongText: row['type'] == 'longtext',
                        ),
                      } else ...{
                        row['content']
                      }
                    ] else ...[
                      BaseTextWidget.customText(
                        text: '-',
                        color: AppColor.kPrimaryColor,
                      ),
                    ]
                  ],
                ),
              ))
        .toList();

    return Container(
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        borderRadius: BorderRadius.circular(borderRadius),
        border: Border.all(color: AppColor.kGreyColor),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (title != null) ...{
            Text(
              title,
              style: BaseTextStyle.customTextStyle(
                kcolor: AppColor.kPrimaryColor,
                kfontSize: 14,
                kfontWeight: FontWeight.w600,
              ),
            ),
            const Divider(),
          },
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: contentWidgets,
          )
        ],
      ),
    );
  }

  static Widget containerWrapper({
    required List<Widget> listItemWidget,
    bool isRowContent = false,
    MainAxisAlignment mainAxisAlignment = MainAxisAlignment.start,
    CrossAxisAlignment crossAxisAlignment = CrossAxisAlignment.start,
    double width = double.infinity,
    double marginContainer = 15.0,
    double borderRadius = 15,
    EdgeInsetsGeometry paddingContent = const EdgeInsets.all(10.0),
  }) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: marginContainer),
      child: Container(
        width: width,
        padding: paddingContent,
        decoration: BoxDecoration(
          color: AppColor.kWhiteColor,
          boxShadow: BaseShadowStyle.customBoxshadow,
          borderRadius: BorderRadius.circular(borderRadius),
        ),
        child: isRowContent != false
            ? Row(
                mainAxisAlignment: mainAxisAlignment,
                crossAxisAlignment: crossAxisAlignment,
                children: [...listItemWidget],
              )
            : Column(
                mainAxisAlignment: mainAxisAlignment,
                crossAxisAlignment: crossAxisAlignment,
                children: [...listItemWidget],
              ),
      ),
    );
  }

  static Widget locationPicker({
    required ProfileControllers collectInputController,
    required BuildContext context,
    required controllerData,
  }) {
    var currentStep = 0.obs;
    var locationPreview = "".obs;
    var isChanged = false.obs;
    Map<String, dynamic> identifier = {};

    controllerData.fetchAllAddress();

    continueStep() {
      if (currentStep < 3) {
        controllerData.searchFilter.clear();
        currentStep.value = currentStep.value + 1; //currentStep+=1;
      }
    }

    cancelStep() {
      if (currentStep > 0) {
        currentStep.value = currentStep.value - 1; //currentStep-=1;
      }
    }

    onStepTapped(int value) {
      currentStep.value = value;
    }

    Widget controlBuilders(context, details) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            if (currentStep.value == 3) ...[
              // ElevatedButton(
              //     onPressed: () {
              //       identifier['preview'] = locationPreview.value;
              //       collectInputController.collectLocationIdentifier
              //           .assignAll(identifier);

              //       Navigator.pop(context);
              //     },
              //     style: ElevatedButton.styleFrom(
              //       backgroundColor: kPrimaryColor,
              //       // fixedSize: const Size(150, 50),
              //       shape: RoundedRectangleBorder(
              //         borderRadius: BorderRadius.circular(10),
              //       ),
              //     ),
              //     child: Text(
              //       "Gunakan Data",
              //       style: customTextStyle(
              //         kcolor: kWhiteColor,
              //         kfontSize: 14,
              //         kfontWeight: FontWeight.w600,
              //       ),
              //     ))
            ]
            // ElevatedButton(
            //   onPressed: () {
            //     details.onStepContinue();
            //   },
            //   child: const Text('Next'),
            // ),
            // const SizedBox(width: 10),
            // OutlinedButton(
            //   onPressed: details.onStepCancel,
            //   child: const Text('Back'),
            // ),
          ],
        ),
      );
    }

    String setPreviewLocation({
      String? dataName,
      int? dataId,
      int? tab,
    }) {
      String joined = "";

      switch (tab) {
        case 1:
          identifier['provinceId'] = dataId;
          identifier['province'] = dataName;
          break;
        case 2:
          identifier['cityId'] = dataId;
          identifier['city'] = dataName;
          break;
        case 3:
          identifier['districtId'] = dataId;
          identifier['district'] = dataName;
          break;
        default:
          identifier['subDistrictId'] = dataId;
          identifier['subDistrict'] = dataName;
      }

      final stringCity = identifier['city'] ?? "";
      final stringDistrict = identifier['district'] ?? "";
      final stringSubDistrict = identifier['subDistrict'] ?? "";

      joined = [
        "${identifier['province'] ?? ""}",
        if (stringCity.isNotEmpty) stringCity,
        if (stringDistrict.isNotEmpty) stringDistrict,
        if (stringSubDistrict.isNotEmpty) stringSubDistrict,
      ].join(", ");

      return locationPreview.value = joined;
    }

    Future onRefresh() async {
      controllerData.fetchAllAddress();
    }

    Widget listOfAddress(
        {required addressData, required int tab, bool isFinish = false}) {
      return Obx(
        () => RefreshIndicator(
          onRefresh: onRefresh,
          child: controllerData.isLoading.value
              ? const SpinKitThreeBounce(
                  size: 30.0,
                  color: AppColor.kPrimaryColor,
                )
              : addressData.isEmpty
                  ? Center(
                      child: BaseLoadingWidget.noMoreDataWidget(height: 100))
                  : SizedBox(
                      height: 300,
                      child: ListView.builder(
                        // shrinkWrap: true,
                        physics: const AlwaysScrollableScrollPhysics(),
                        itemCount: addressData.length,
                        itemBuilder: (context, index) {
                          if (addressData.length > 0) {
                            return ListTile(
                              onTap: () {
                                continueStep();
                                setPreviewLocation(
                                  tab: tab,
                                  dataId: addressData[index]['id'],
                                  dataName: addressData[index]['name'],
                                );

                                if (isFinish) {
                                  //** INPUT ALL FINNISH *//
                                  identifier['preview'] = locationPreview.value;
                                  collectInputController
                                      .collectLocationIdentifier
                                      .assignAll(identifier);
                                  isChanged.value == false;
                                  Navigator.pop(context);
                                }
                              },
                              title: Text(
                                addressData[index]['name'],
                                style: BaseTextStyle.customTextStyle(
                                  kfontSize: 12,
                                  kfontWeight: FontWeight.w500,
                                ),
                              ),
                            );
                          } else {
                            return const SpinKitThreeBounce(
                              size: 30.0,
                              color: AppColor.kPrimaryColor,
                            );
                          }
                        },
                      ),
                    ),
        ),
      );
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Obx(
          () => locationPreview.value.isNotEmpty
              ? Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Text(
                    // addNewlines(locationPreview.value, 50),
                    locationPreview.value,
                    textAlign: TextAlign.center,
                    style: BaseTextStyle.customTextStyle(
                      kcolor: AppColor.kPrimaryColor,
                      kfontSize: 12,
                      kfontWeight: FontWeight.w600,
                    ),
                  ),
                )
              : const SizedBox(),
        ),
        BaseTextFieldWidget.roundedStyleTextField(
          textEditingController: controllerData.searchFilter,
          onChange: (val) {
            BaseGlobalFuntion.debounceRun(
              action: () {
                controllerData.fetchAllAddress();
              },
              duration: const Duration(milliseconds: 500),
            );
          },
          autoFocus: false,
          suffixIcon: IconButton(
            iconSize: 20,
            onPressed: () {},
            icon: const Icon(
              Icons.search,
              color: AppColor.kGreyColor,
            ),
          ),
          radius: 8,
          height: 35,
          hintText: "Cari Daerah",
        ),
        const SizedBox(height: 5),
        SizedBox(
          height: 300,
          child: Obx(
            () => Stepper(
              elevation: 0, //Horizontal Impact
              // margin: const EdgeInsets.all(0), //vertical impact
              controlsBuilder: controlBuilders,
              type: StepperType.horizontal,

              physics: const ScrollPhysics(),
              onStepTapped: onStepTapped,
              onStepContinue: continueStep,
              onStepCancel: cancelStep,
              currentStep: currentStep.value, //0, 1, 2
              steps: [
                Step(
                  title: Text(
                    currentStep.value == 0 ? 'PROVINSI' : '',
                    style: BaseTextStyle.customTextStyle(
                      kfontSize: 12,
                      kfontWeight: FontWeight.w600,
                    ),
                  ),
                  content: listOfAddress(
                    addressData: controllerData.dataProvince,
                    tab: 1,
                  ),
                  isActive: currentStep >= 0,
                  state: currentStep >= 0
                      ? StepState.complete
                      : StepState.disabled,
                ),
                Step(
                  title: Text(
                    currentStep.value == 1 ? 'KABUPATEN' : '',
                    style: BaseTextStyle.customTextStyle(
                      kfontSize: 12,
                      kfontWeight: FontWeight.w600,
                    ),
                  ),
                  content: listOfAddress(
                    addressData: controllerData.dataCity,
                    tab: 2,
                  ),
                  isActive: currentStep >= 0,
                  state: currentStep >= 1
                      ? StepState.complete
                      : StepState.disabled,
                ),
                Step(
                  title: Text(
                    currentStep.value == 2 ? 'KECAMATAN' : '',
                    style: BaseTextStyle.customTextStyle(
                      kfontSize: 12,
                      kfontWeight: FontWeight.w600,
                    ),
                  ),
                  content: listOfAddress(
                    addressData: controllerData.dataDistric,
                    tab: 3,
                  ),
                  isActive: currentStep >= 0,
                  state: currentStep >= 2
                      ? StepState.complete
                      : StepState.disabled,
                ),
                Step(
                  title: Text(
                    currentStep.value == 3 ? 'DESA' : '',
                    style: BaseTextStyle.customTextStyle(
                      kfontSize: 12,
                      kfontWeight: FontWeight.w600,
                    ),
                  ),
                  content: listOfAddress(
                    addressData: controllerData.dataSubDistric,
                    tab: 4,
                    isFinish: true,
                  ),
                  isActive: currentStep >= 0,
                  state: currentStep >= 3
                      ? StepState.complete
                      : StepState.disabled,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  static modalBottomFilter({
    required BuildContext context,
    required Widget filterForm,
    required void Function()? onReset,
    required void Function() onFilter,
    required valueFilter,
  }) {
    return showModalBottomSheet<void>(
      context: context,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0))),
      backgroundColor: AppColor.kWhiteColor,
      builder: (BuildContext context) {
        return Container(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 10,
                width: 40,
                margin: const EdgeInsets.only(bottom: 5),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: AppColor.kGreyColor,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: Text(
                      "Filter",
                      style: BaseTextStyle.customTextStyle(
                        kcolor: AppColor.kPrimaryColor,
                        kfontSize: 15,
                        kfontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  // Obx(() {
                  //   // return valueFilter.isNotEmpty
                  //   return
                  valueFilter.values.any((value) => value != null)
                      ? ConstrainedBox(
                          constraints:
                              const BoxConstraints.tightFor(height: 30),
                          child: ElevatedButton.icon(
                            icon: const Icon(Icons.restore),
                            label: Text(
                              'Reset',
                              style: BaseTextStyle.customTextStyle(
                                kcolor: AppColor.kPrimaryColor,
                                kfontSize: 12,
                              ),
                            ),
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            onPressed: onReset,
                          ),
                        )
                      : const SizedBox()
                  // }),
                ],
              ),
              const SizedBox(height: 10),
              Flexible(
                child: ListView(
                  shrinkWrap: true,
                  physics: const AlwaysScrollableScrollPhysics(),
                  children: [filterForm],
                ),
              ),
              const SizedBox(height: 10),
              Obx(() {
                valueFilter.isNotEmpty;
                return Button.filled(
                  onPressed: onFilter,
                  color: AppColor.kPrimaryColor,
                  width: double.infinity,
                  icon: const Icon(
                    Icons.filter_alt_rounded,
                    color: AppColor.kWhiteColor,
                  ),
                  // height: 50,
                  // textStyle: BaseTextStyle.customTextStyle(
                  //   kcolor: AppColor.kWhiteColor,
                  //   kfontSize: 12,
                  // ),
                  label: "Terapkan Filter",
                );
              }),
            ],
          ),
        );
      },
    );
  }

  static mapsPicker({
    required BuildContext context,
    required Future<dynamic> Function({
      dynamic lat,
      dynamic lng,
      TextEditingController? addressPreview,
    }) mapsHelper,
    required TextEditingController detailMapsControllers,
    required dynamic collectMapsData,
  }) async {
    Position position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.medium,
    );
    bool selectedPoint = false;

    TextEditingController tempFullAddress = TextEditingController();

    return showModalBottomSheet<void>(
        context: context,
        isScrollControlled: true,
        backgroundColor: AppColor.kGreyColor.withOpacity(0.1),
        builder: (BuildContext context) {
          if (selectedPoint == false) {
            print("SELECTED FALSE");
            mapsHelper(
              lat: position.latitude,
              lng: position.longitude,
              // addressPreview: detailMapsControllers,
              addressPreview: tempFullAddress,
            );
          }

          return Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: AppColor.kWhiteColor,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Column(
              // crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: 200,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: PlacePicker(
                      apiKey: gMapsApiKey,
                      initialPosition: LatLng(
                        // locationCoordinatesControllers.currentLat.value,
                        // locationCoordinatesControllers.currentLong.value,
                        position.latitude,
                        position.longitude,
                      ),
                      selectedPlaceWidgetBuilder:
                          (context, selectedPlace, state, isSearchBarFocused) {
                        WidgetsBinding.instance.addPostFrameCallback((_) {
                          if (state != SearchingState.Searching) {
                            if (selectedPlace != null) {
                              selectedPoint = true;
                              mapsHelper(
                                lat: selectedPlace!.geometry!.location.lat,
                                lng: selectedPlace.geometry!.location.lng,
                              );

                              tempFullAddress.text =
                                  selectedPlace.formattedAddress!;
                            }
                          }
                        });

                        return FloatingCard(
                          bottomPosition:
                              MediaQuery.of(context).size.height * 0.05,
                          leftPosition:
                              MediaQuery.of(context).size.width * 0.02,
                          rightPosition:
                              MediaQuery.of(context).size.width * 0.02,
                          width: MediaQuery.of(context).size.width * 0.9,
                          borderRadius: BorderRadius.circular(12.0),
                          child: state == SearchingState.Searching
                              ? const Padding(
                                  padding: EdgeInsets.all(8.0),
                                  child: Center(
                                      child: CircularProgressIndicator()),
                                )
                              : Container(
                                  padding: const EdgeInsets.all(8.0),
                                  width: double.infinity,
                                  child: Column(
                                    children: [
                                      Text(
                                        selectedPlace!.formattedAddress ?? "",
                                        style: BaseTextStyle.customTextStyle(
                                          kfontSize: 12,
                                          kcolor: AppColor.kPrimaryColor,
                                          kfontWeight: FontWeight.w400,
                                        ),
                                      ),
                                      // const SizedBox(height: 10),
                                    ],
                                  ),
                                ),
                        );
                      },
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                Column(
                  children: [
                    Obx(() => CustomTextField.style2(
                          title: 'Kota/Kabupaten',
                          fieldName: 'kota',
                          readOnly: true,
                          controller: TextEditingController(
                              text: collectMapsData['city_name']),
                          onChange: ({value}) {},
                          textInputAction: TextInputAction.next,
                        )),
                    const SizedBox(height: 5),
                    Obx(() => CustomTextField.style2(
                          title: 'Desa/Kelurahan',
                          fieldName: 'desa',
                          readOnly: true,
                          controller: TextEditingController(
                            text: collectMapsData['district_name'],
                          ),
                          onChange: ({value}) {},
                          textInputAction: TextInputAction.next,
                        )),
                    const SizedBox(height: 5),
                    Obx(() => CustomTextField.style2(
                          title: 'Kecamatan',
                          fieldName: 'kecamatan',
                          readOnly: true,
                          controller:
                              // subDisticControllers,
                              TextEditingController(
                            text: collectMapsData['sub_district_name'],
                          ),
                          onChange: ({value}) {},
                          textInputAction: TextInputAction.next,
                        )),
                    const SizedBox(height: 5),
                    CustomTextField.style2(
                      title: 'Alamat Lengkap',
                      hintText: "Masukan Alamat",
                      fieldName: 'address',
                      readOnly: true,
                      isLongText: true,
                      // controller: detailMapsControllers,
                      controller: tempFullAddress,
                      onChange: ({value}) {},
                      textInputAction: TextInputAction.done,
                    ),
                  ],
                ),
                const SizedBox(height: 10),
                SizedBox(
                  height: 60.0,
                  width: double.infinity,
                  child: ElevatedButton(
                    onPressed: () async {
                      detailMapsControllers.text = tempFullAddress.text;
                      Get.back();
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: AppColor.kPrimaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    child: Text(
                      "Pilih Lokasi",
                      style: BaseTextStyle.customTextStyle(
                        // kfontSize: 12,
                        kcolor: AppColor.kWhiteColor,
                        kfontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
