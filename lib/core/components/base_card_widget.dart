import 'package:rbs_mobile_operation/core/core.dart';

class BaseCardWidget {
  static Widget cardMonitoring({
    required String title,
    required String content,
    Color? kcontentColor,
  }) {
    return Flexible(
      child: Container(
        padding: const EdgeInsets.all(7),
        decoration: BoxDecoration(
          color: AppColor.kWhiteColor,
          borderRadius: BorderRadius.circular(5.0),
          boxShadow: BaseShadowStyle.customBoxshadow,
        ),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BaseTextWidget.customText(
                  text: title,
                  fontSize: 10,
                  color: kcontentColor,
                  fontWeight: FontWeight.w500,
                ),
                BaseTextWidget.customText(
                  text: content,
                  fontSize: 13,
                  color: kcontentColor,
                  fontWeight: FontWeight.w500,
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }

  static Widget cardInfoDetail({
    required String titleInfo,
    required List<Map<String, dynamic>> contentList,
    bool isLoading = false,
  }) {
    final listContent = List.generate(contentList.length, (index) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (isLoading == false) ...[
            Text(
              contentList[index]['title'] ?? "",
              style: BaseTextStyle.customTextStyle(
                kfontSize: 13,
                kcolor: AppColor.kPrimaryColor,
              ),
            ),
            contentList[index]['widget'],
            const SizedBox(height: 10),
          ] else ...[
            BaseLoadingWidget.cardShimmer(
              height: 40,
              width: double.infinity,
              shimmerheight: 40,
              baseColor: AppColor.kSoftGreyColor,
              highlightColor: AppColor.kWhiteColor,
              itemCount: 1,
            ),
          ]
          // const Divider()
        ],
      );
    });

    return Stack(
      children: [
        Positioned(
          top: 0.0,
          left: 15.0,
          child: Container(
            height: 50,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: AppColor.kInactiveColor,
              borderRadius: BorderRadius.circular(10),
              boxShadow: BaseShadowStyle.customBoxshadow,
            ),
            child: Text(
              titleInfo,
              style: BaseTextStyle.customTextStyle(
                kfontWeight: FontWeight.w600,
                kfontSize: 14,
                kcolor: AppColor.kPrimaryColor,
              ),
            ),
          ),
        ),
        Container(
          width: double.infinity,
          // height: 400,
          margin: const EdgeInsets.only(left: 10, right: 10, top: 35),
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 15),
          decoration: BoxDecoration(
            color: AppColor.kWhiteColor,
            borderRadius: BorderRadius.circular(10),
            boxShadow: BaseShadowStyle.customBoxshadow,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ...listContent,
            ],
          ),
        ),
      ],
    );
  }

  static Widget cardStyle1({
    required String title,
    String? subTitle1,
    Widget? trailing,
    Widget? leading,
    String? headerString,
    Widget? subTitle2,
    Map<String, dynamic>? statusHeader,
    Function()? ontap,
  }) {
    return GestureDetector(
      onTap: ontap,
      child: SizedBox(
        child: Stack(
          children: [
            Container(
              margin: const EdgeInsets.only(top: 10),
              padding:
                  headerString != null ? const EdgeInsets.only(top: 10) : null,
              decoration: BoxDecoration(
                color: AppColor.kWhiteColor,
                borderRadius: const BorderRadius.all(Radius.circular(8)),
                border: Border.all(color: AppColor.kAvailableColor),
              ),
              child: ListTile(
                leading: leading,
                title: BaseTextWidget.customText(
                  text: title,
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                  isLongText: true,
                  overflow: TextOverflow.ellipsis,
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BaseTextWidget.customText(
                      text: subTitle1,
                      fontSize: 12,
                      fontWeight: FontWeight.w500,
                      isLongText: true,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                    subTitle2 ?? const SizedBox(),
                  ],
                ),
                trailing: trailing,
              ),
            ),
            Positioned(
              top: 0.0,
              right: 20.0,
              child: statusHeader != null
                  ? Container(
                      padding: const EdgeInsets.all(3.5),
                      decoration: BoxDecoration(
                        color: statusHeader['color'] ??
                            AppColor.kBadgeSuccessColor,
                        border: Border.all(
                            color: statusHeader['titleColor'] ??
                                AppColor.kSuccesColor),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(5)),
                      ),
                      child: Text(
                        statusHeader['title'] ?? "",
                        style: BaseTextStyle.customTextStyle(
                          kfontSize: 9,
                          kcolor: statusHeader['titleColor'] ??
                              AppColor.kSuccesColor,
                          kfontWeight: FontWeight.w600,
                        ),
                      ),
                    )
                  : const SizedBox(),
            ),
            if (headerString != null)
              Positioned(
                top: 0.0,
                left: 14.0,
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 2, vertical: 3),
                  decoration: BoxDecoration(
                    color: AppColor.kWhiteColor,
                    borderRadius: const BorderRadius.all(Radius.circular(5)),
                    border: Border.all(color: AppColor.kGreyColor),
                  ),
                  child: BaseTextWidget.customText(
                    text: headerString,
                    fontSize: 12,
                    // longTextClamp: true,
                    isLongText: true,
                  ),
                ),
              )
          ],
        ),
      ),
    );
  }

  static Widget cardStyle2({
    required String title,
    String? trailing,
    required String headerString,
    String? avatar,
    String? subTitle1,
    String? subTitle2,
    List<Widget>? badgeTag,
    Map<String, dynamic>? statusHeader,
    Function()? ontap,
  }) {
    return GestureDetector(
      onTap: ontap,
      child: SizedBox(
        child: Stack(
          children: [
            Container(
              margin:
                  const EdgeInsets.only(top: 15, left: 5, right: 5, bottom: 10),
              padding: const EdgeInsets.only(top: 15, bottom: 6),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.all(Radius.circular(10)),
                boxShadow: BaseShadowStyle.customBoxshadow,
                color: AppColor.kWhiteColor,
              ),
              child: ListTile(
                leading: const CircleAvatar(),
                title: Text(
                  title,
                  style: BaseTextStyle.customTextStyle(
                    kfontWeight: FontWeight.w600,
                    kfontSize: 14,
                  ),
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      subTitle1 ?? "",
                      style: BaseTextStyle.customTextStyle(
                        kfontWeight: FontWeight.w500,
                        kfontSize: 12,
                      ),
                    ),
                    Text(
                      subTitle2 ?? "",
                      style: BaseTextStyle.customTextStyle(
                        kfontWeight: FontWeight.w500,
                        kfontSize: 12,
                      ),
                    ),
                    const SizedBox(height: 10),
                    Row(
                      children:
                          badgeTag == null ? [const SizedBox()] : badgeTag,
                    )
                  ],
                ),
                // trailing: Text(
                //   trailing,
                //   textAlign: TextAlign.center,
                //   style: BaseTextStyle.customTextStyle(
                //     kfontWeight: FontWeight.w600,
                //     kfontSize: 12,
                //   ),
                // ),
              ),
            ),
            Positioned(
              top: 0.0,
              right: 15.0,
              child: statusHeader != null
                  ? Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: statusHeader['color'] ??
                            AppColor.kBadgeSuccessColor,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(5)),
                      ),
                      child: Text(
                        statusHeader['title'] ?? "",
                        style: BaseTextStyle.customTextStyle(
                          kfontSize: 9,
                          kcolor: statusHeader['titleColor'] ??
                              AppColor.kSuccesColor,
                          kfontWeight: FontWeight.w600,
                        ),
                      ),
                    )
                  : const SizedBox(),
            ),
            Positioned(
              top: 0.0,
              left: 15.0,
              child: trailing != null
                  ? Container(
                      padding: const EdgeInsets.all(10),
                      decoration: const BoxDecoration(
                        color: AppColor.kInactiveColor,
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      child: Text(
                        trailing,
                        style: BaseTextStyle.customTextStyle(
                          kfontSize: 9,
                          kcolor: AppColor.kPrimaryColor,
                          kfontWeight: FontWeight.w600,
                        ),
                      ),
                    )
                  : const SizedBox(),
            )
          ],
        ),
      ),
    );
  }

  // static cardStyle3({
  //   String? title,
  //   String? subtitle,
  //   String? bottomSubtitle,
  //   String? trailling,
  //   TextStyle? traillingTextStyle,
  //   Widget? extendWidget,
  //   Function()? ontap,
  // }) {
  //   return GestureDetector(
  //     onTap: ontap,
  //     child: Container(
  //       padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
  //       decoration: BoxDecoration(
  //         borderRadius: BorderRadius.circular(8),
  //         // boxShadow: BaseShadowStyle.customBoxshadow,
  //         border: Border.all(color: AppColor.kAvailableColor),
  //         color: AppColor.kWhiteColor,
  //       ),
  //       child: Column(
  //         // mainAxisSize: MainAxisSize.min,
  //         crossAxisAlignment: CrossAxisAlignment.start,
  //         children: [
  //           Row(
  //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //             children: [
  //               Flexible(
  //                 child: Column(
  //                   children: [
  //                     BaseTextWidget.customText(
  //                       text: title,
  //                       fontSize: 14,
  //                       fontWeight: FontWeight.w600,
  //                       maxLines: 1,
  //                       overflow: TextOverflow.ellipsis,
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //               Flexible(
  //                 child: Column(
  //                   children: [
  //                     BaseTextWidget.customText(
  //                       text: trailling,
  //                       style: traillingTextStyle,
  //                       maxLengthText: 25,
  //                       textAlign: TextAlign.center,
  //                       fontWeight: FontWeight.w600,
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //             ],
  //           ),
  //           BaseTextWidget.customText(
  //             text: subtitle,
  //             fontSize: 12,
  //             fontWeight: FontWeight.w500,
  //             maxLengthText: 25,
  //           ),
  //           const Divider(thickness: 0.5),
  //           Row(
  //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //             crossAxisAlignment: CrossAxisAlignment.start,
  //             children: [
  //               BaseTextWidget.customText(
  //                 text: bottomSubtitle,
  //                 fontSize: 12,
  //                 fontWeight: FontWeight.w500,
  //                 maxLengthText: 25,
  //               ),
  //               if (extendWidget != null) ...[
  //                 extendWidget,
  //               ]
  //             ],
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }

  static cardStyle4({
    String? title,
    String? subtitle,
    Widget? bottomSubtitle,
    Widget? bottomSubtitle2,
    Widget? trailling,
    String? topLabel,
    Widget? extendWidget,
    Function()? ontap,
  }) {
    final cardContent = GestureDetector(
      onTap: ontap,
      child: Container(
        margin: topLabel != null ? const EdgeInsets.only(top: 20) : null,
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(color: AppColor.kAvailableColor),
          color: AppColor.kWhiteColor,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Column(
                    children: [
                      BaseTextWidget.customText(
                        text: title,
                        fontWeight: FontWeight.w600,
                        fontSize: 14,
                        isLongText: true,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
                trailling ?? const SizedBox()
              ],
            ),
            BaseTextWidget.customText(
              text: subtitle,
              fontSize: 12,
              fontWeight: FontWeight.w500,
              isLongText: true,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
            const Divider(thickness: 0.2),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      bottomSubtitle ?? const SizedBox(),
                      bottomSubtitle2 ?? const SizedBox(),
                    ],
                  ),
                ),
                if (extendWidget != null) ...[
                  extendWidget,
                ]
              ],
            ),
          ],
        ),
      ),
    );
    return Stack(
      children: [
        if (topLabel != null) ...[
          Positioned(
              top: 2.0,
              left: 16.0,
              child: Container(
                padding: const EdgeInsets.all(5),
                decoration: const BoxDecoration(
                  color: AppColor.kInactiveColor,
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                child: Text(
                  topLabel ?? "",
                  style: BaseTextStyle.customTextStyle(
                    kfontSize: 9,
                    kcolor: AppColor.kPrimaryColor,
                    kfontWeight: FontWeight.w600,
                  ),
                ),
              )),
        ],
        cardContent,
      ],
    );
  }

  static cardStyle5({
    required BuildContext context,
    String? subtitle,
    String? title,
    String? expandTitle,
    String? expandSubtitle,
    String? bottomSubtitle,
    String? trailling,
    TextStyle? traillingTextStyle,
    String? topLabel,
    Widget? extendWidget,
    TextStyle? expandTitleStyle,
    TextStyle? expandSubtitleStyle,
    double paddingTop = 20,
  }) {
    final cardContent = Card(
      margin: topLabel != null
          ? EdgeInsets.only(top: paddingTop)
          : const EdgeInsets.all(0),
      child: Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: ExpansionTile(
          tilePadding: const EdgeInsets.symmetric(horizontal: 8),
          title: Text(
            title ?? "",
            style: BaseTextStyle.customTextStyle(
              kfontSize: 14,
              kcolor: AppColor.kPrimaryColor,
              kfontWeight: FontWeight.w600,
            ),
          ),
          subtitle: Text(
            subtitle ?? "",
            style: BaseTextStyle.customTextStyle(
              kfontSize: 13,
              kfontWeight: FontWeight.w500,
            ),
          ),
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        expandTitle ?? "",
                        style: expandTitleStyle ??
                            BaseTextStyle.customTextStyle(
                              kfontSize: 14,
                              kfontWeight: FontWeight.w600,
                            ),
                      ),
                      Text(
                        trailling ?? "",
                        style: traillingTextStyle,
                      ),
                    ],
                  ),
                  Text(
                    expandSubtitle ?? "",
                    style: expandSubtitleStyle ??
                        BaseTextStyle.customTextStyle(
                          kfontSize: 12,
                          kfontWeight: FontWeight.w500,
                        ),
                  ),
                  const Divider(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        bottomSubtitle ?? "",
                        style: BaseTextStyle.customTextStyle(
                          kfontSize: 12,
                          kfontWeight: FontWeight.w500,
                        ),
                      ),
                      if (extendWidget != null) ...[
                        extendWidget,
                      ]
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );

    return Stack(
      children: [
        if (topLabel != null) ...[
          Positioned(
            top: 1.5,
            left: 12.0,
            child: Container(
              padding: const EdgeInsets.all(4),
              decoration: const BoxDecoration(
                color: AppColor.kInactiveColor,
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              child: Text(
                topLabel,
                style: BaseTextStyle.customTextStyle(
                  kfontSize: 9,
                  kcolor: AppColor.kPrimaryColor,
                  kfontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
        ],
        cardContent,
      ],
    );
  }

  static cardAnalyticBasic1({
    required BuildContext context,
    String? title,
    String? subtitle,
    List<Widget>? childrenContent,
    double paddingTop = 30,
  }) {
    return Card(
      margin: EdgeInsets.only(top: paddingTop),
      child: Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: ExpansionTile(
          title: Text(
            title ?? "",
            style: BaseTextStyle.customTextStyle(
              kfontSize: 14,
              kcolor: AppColor.kPrimaryColor,
              kfontWeight: FontWeight.w600,
            ),
          ),
          subtitle: Text(
            subtitle ?? "",
            style: BaseTextStyle.customTextStyle(
              kfontSize: 13,
              kfontWeight: FontWeight.w400,
            ),
          ),
          childrenPadding: const EdgeInsets.all(10),
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("ASDDD"),
                Text("ASDDD"),
              ],
            ),
            const Divider(),
            GridView.count(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              crossAxisCount: 3,
              children: childrenContent ?? [],
            ),

            // if (childrenContent != null) ...childrenContent
          ],
        ),
      ),
    );
  }

  static simpleInformationCard1({
    String? title,
    String? subtitle,
    String? centerContent,
    TextStyle? titleTextStyle,
    TextStyle? subtitleTextStyle,
    TextStyle? centerContentTextStyle,
    Color? colorContainer,
    double? padding,
  }) {
    return SizedBox(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title ?? "",
            style: titleTextStyle,
          ),
          Text(
            subtitle ?? "",
            style: subtitleTextStyle,
          ),
          Container(
            margin: const EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(padding ?? 5),
            decoration: BoxDecoration(
              color: colorContainer,
              borderRadius: BorderRadius.circular(5),
              boxShadow: BaseShadowStyle.customBoxshadow,
            ),
            child: Text(
              centerContent ?? "",
              textAlign: TextAlign.center,
              style: centerContentTextStyle,
            ),
          )
        ],
      ),
    );
  }

  static detailInfoCard({
    required String title,
    required List<dynamic> contentList,
    bool isLoading = false,
  }) {
    final resultWidget = contentList
        .map((item) => Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BaseTextWidget.customText(
                  text: item['title'],
                  fontSize: 13,
                  fontWeight: FontWeight.w600,
                  color: AppColor.kPrimaryColor,
                ),
                BaseTextWidget.customText(
                  text: item['content'],
                  fontSize: 12,
                  fontWeight: FontWeight.w500,
                ),
              ],
            ))
        .toList();

    return isLoading
        ? Container(
            // margin: const EdgeInsets.symmetric(horizontal: 8, vertical: 0),
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: AppColor.kWhiteColor,
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              border: Border.all(color: AppColor.kAvailableColor),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Shimmer.fromColors(
                    baseColor: AppColor.kSoftGreyColor,
                    highlightColor: AppColor.kWhiteColor,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 120,
                          height: 20,
                          decoration: BoxDecoration(
                            color: Colors.amber,
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                        const Divider(),
                        ...List.generate(
                          5, // Jumlah container yang ingin ditampilkan
                          (index) => Container(
                            width: double.infinity,
                            height: 20,
                            margin: const EdgeInsets.only(bottom: 5),
                            decoration: BoxDecoration(
                              color: Colors.amber,
                              borderRadius: BorderRadius.circular(5),
                            ),
                          ),
                        ),
                      ],
                    )),
              ],
            ),
          )
        : Container(
            // margin: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
              color: AppColor.kWhiteColor,
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              border: Border.all(color: AppColor.kAvailableColor),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: BaseTextStyle.customTextStyle(
                    kcolor: AppColor.kPrimaryColor,
                    kfontSize: 14,
                    kfontWeight: FontWeight.w600,
                  ),
                ),
                const Divider(),
                ...resultWidget,
              ],
            ),
          );
  }

  static detailInfoCard2({
    String? title,
    required List<dynamic> contentList,
    bool isLoading = false,
  }) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        borderRadius: const BorderRadius.all(Radius.circular(8)),
        border: Border.all(color: AppColor.kAvailableColor),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (title != null) ...{
                Text(
                  title,
                  style: BaseTextStyle.customTextStyle(
                    kcolor: AppColor.kPrimaryColor,
                    kfontSize: 14,
                    kfontWeight: FontWeight.w600,
                  ),
                ),
                const Divider(),
              },
              ...contentList.map((item) {
                return item['extend'] ??
                    Column(
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            item['icon'] ?? const SizedBox(),
                            const SizedBox(width: 15),
                            Flexible(
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ...{
                                    if (item['title'] != null)
                                      Text(
                                        item['title'] ?? "",
                                        style: BaseTextStyle.customTextStyle(
                                          kcolor: AppColor.kPrimaryColor,
                                          kfontSize: 12,
                                          kfontWeight: FontWeight.w600,
                                        ),
                                      ),
                                  },
                                  isLoading
                                      ? Shimmer.fromColors(
                                          baseColor: AppColor.kSoftGreyColor,
                                          highlightColor: AppColor.kWhiteColor,
                                          child: Container(
                                            width: 100,
                                            height: 18,
                                            margin:
                                                const EdgeInsets.only(top: 1),
                                            decoration: BoxDecoration(
                                              color: Colors.amber,
                                              borderRadius:
                                                  BorderRadius.circular(5),
                                            ),
                                          ),
                                        )
                                      : item['content'] ?? const Text("")
                                ],
                              ),
                            ),
                          ],
                        ),
                        contentList.last == item
                            ? const SizedBox()
                            : const Divider(thickness: 0.5),
                      ],
                    );
              }).toList(),
            ],
          ),
        ],
      ),
    );
  }
}
