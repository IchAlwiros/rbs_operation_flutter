import 'package:flutter/services.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/components/base_text_widget.dart';

class BaseTextFieldWidget {
  static Widget roundedStyleTextField({
    required TextEditingController textEditingController,
    required String hintText,
    List<TextInputFormatter>? inputFormatter,
    void Function(String?)? onChange,
    String? Function(String?)? validator,
    double radius = 5.0,
    double width = double.infinity,
    double height = 50,
    Widget? suffixIcon,
    Widget? prefixIcon,
    bool? autoFocus,
    FocusNode? focusNode,
    borderSide,
  }) {
    return SizedBox(
      width: width,
      height: height,
      child: TextFormField(
        focusNode: focusNode,
        cursorColor: AppColor.kPrimaryColor,
        controller: textEditingController,
        onChanged: onChange,
        validator: validator,
        inputFormatters: inputFormatter,
        autofocus: autoFocus ?? false,
        decoration: BaseTextFieldWidgetStyle.decoratorStyle1(
          borderSide: borderSide,
          suffixIcon: suffixIcon,
          prefixIcon: prefixIcon,
          radius: radius,
          hintText: hintText,
          horizontal: 15,
          vertical: 0,
        ),
        style: BaseTextStyle.customTextStyle(
          kcolor: const Color.fromRGBO(31, 79, 148, 1),
          kfontSize: 12,
          kfontWeight: FontWeight.w500,
        ),
      ),
    );
  }

  static textFieldFormBuilder({
    required String title,
    required String fieldName,
    bool isRequired = false,
    bool secureViewText = false,
    bool isLongText = false,
    String? initialValue,
    double radius = 7,
    void Function()? onClearText,
    bool suffixOnError = false,
    Widget? extendSuffixItem,
    TextEditingController? controller,
    Function(String?)? onChanged,
    void Function()? onTap,
    String? Function(String?)? validator,
    TextInputType? keyboardType,
    TextInputAction? textInputAction,
    List<TextInputFormatter>? inputFormater,
    bool enabled = true,
    bool readOnly = false,
    InputDecoration? inputDecoration,
    String? hintText,
  }) {
    var isObsecure = true.obs;

    return SizedBox(
        width: double.infinity,
        child: Obx(
          () => FormBuilderTextField(
            onTap: onTap,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            name: fieldName,
            controller: controller,
            // enabled: enabled,
            readOnly: readOnly,
            initialValue: initialValue,
            maxLines: isLongText == false ? 1 : 4,
            // expands: expandHeight > 75,
            textAlignVertical: TextAlignVertical.top,
            obscureText:
                secureViewText == true ? isObsecure.value : !isObsecure.value,
            inputFormatters: inputFormater,
            decoration: inputDecoration ??
                InputDecoration(
                  // alignLabelWithHint: expandHeight >= 75,
                  alignLabelWithHint: isLongText,

                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  filled: true,
                  fillColor: enabled
                      ? AppColor.kGreyColor.withOpacity(0.2)
                      : AppColor.kGreyColor,
                  label: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        title,
                        style: BaseTextStyle.customTextStyle(
                          kcolor: AppColor.kPrimaryColor,
                          kfontSize: 14,
                          kfontWeight: FontWeight.w500,
                        ),
                      ),
                      isRequired
                          ? Text(
                              "*",
                              style: BaseTextStyle.customTextStyle(
                                kcolor: AppColor.kErrorColor,
                                kfontSize: 15,
                                kfontWeight: FontWeight.w600,
                              ),
                            )
                          : const SizedBox(),
                    ],
                  ),
                  // labelText: title,
                  // labelStyle: BaseTextStyle.customTextStyle(
                  //   kfontSize: 14,
                  //   kfontWeight: FontWeight.w500,
                  //   kcolor: suffixOnError
                  //       ? AppColor.kFailedColor
                  //       : AppColor.kPrimaryColor,
                  // ),
                  // contentPadding:
                  //     const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                  // suffixIconConstraints: const BoxConstraints(minWidth: 30),
                  contentPadding:
                      const EdgeInsets.only(bottom: -10.0, top: 20, left: 10),
                  // prefixIconConstraints: const BoxConstraints(
                  //   minWidth: 5,
                  //   minHeight: 5,
                  // ),
                  // isDense: true,
                  // suffixIconConstraints: const BoxConstraints(
                  //   minWidth: 2,
                  //   minHeight: 2,
                  // ),
                  hintText: hintText ?? "Isi disini",
                  hintStyle: BaseTextStyle.hintTextStyle,

                  suffixIcon: secureViewText == true
                      ? IconButton(
                          icon: Icon(
                            isObsecure.value == true
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: isObsecure.value == true
                                ? AppColor.kPrimaryColor
                                : AppColor.kGreyColor,
                          ),
                          onPressed: () {
                            isObsecure.value = !isObsecure.value;
                          },
                          splashColor: Colors.transparent,
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              margin: const EdgeInsets.all(1),
                              decoration: BoxDecoration(
                                // color: AppColor.kPrimaryColor,
                                borderRadius: BorderRadius.circular(7),
                              ),
                              child: IconButton(
                                onPressed: () {
                                  if (onClearText != null) {
                                    onClearText();
                                  }
                                  if (controller != null) {
                                    controller.clear();
                                  }
                                },
                                padding: const EdgeInsets.all(-100),
                                splashColor: AppColor.kPrimaryColor,
                                icon: const Icon(
                                  Icons.close_rounded,
                                  color: AppColor.kGreyColor,
                                  size: 20,
                                ),
                              ),
                            ),
                            if (extendSuffixItem != null) extendSuffixItem,
                          ],
                        ),

                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(radius),
                    borderSide: const BorderSide(
                      color: AppColor.kPrimaryColor,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(radius),
                    borderSide: const BorderSide(
                      color: AppColor.kPrimaryColor,
                    ), //menunaktifkan border
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(radius),
                    borderSide: const BorderSide(
                      color: AppColor.kErrorColor,
                    ), //menunaktifkan border
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(radius),
                    borderSide: const BorderSide(
                      color: AppColor.kErrorColor,
                    ), //menunaktifkan border
                  ),
                  errorStyle: BaseTextStyle.customTextStyle(
                    kfontSize: 12,
                    kcolor: AppColor.kFailedColor,
                    kfontWeight: FontWeight.w500,
                  ),
                ),
            onChanged: onChanged,
            // valueTransformer: (text) => num.tryParse(text),
            validator: validator,
            style: BaseTextStyle.customTextStyle(
              kfontSize: 15,
              kfontWeight: FontWeight.w500,
              kcolor: AppColor.kPrimaryColor,
            ),

            keyboardType: keyboardType,
            textInputAction: textInputAction,
          ),
        ));
  }

  static textFieldFormBuilder2({
    required String title,
    required String fieldName,
    bool isRequired = false,
    bool secureViewText = false,
    bool isLongText = false,
    String? initialValue,
    double radius = 7,
    void Function()? onClearText,
    bool suffixOnError = false,
    Widget? extendSuffixItem,
    Widget? extendPrefixItem,
    TextEditingController? controller,
    Function({
      dynamic value,
    })? onChange,
    void Function()? onTap,
    String? Function(String?)? validator,
    TextInputType? keyboardType,
    TextInputAction? textInputAction,
    List<TextInputFormatter>? inputFormater,
    bool enabled = true,
    bool readOnly = false,
    // InputDecoration? inputDecoration,
    bool style2 = false,
    String? hintText,
    void Function(String?)? onSubmitted,
  }) {
    var isObsecure = true.obs;

    // if (style2) {
    //   Obx(() {
    //     inputDecoration =
    //     return const SizedBox();
    //   });
    // }

    return SizedBox(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  title,
                  style: BaseTextStyle.customTextStyle(
                    kcolor: AppColor.kPrimaryColor,
                    kfontSize: 12,
                    kfontWeight: FontWeight.w500,
                  ),
                ),
                Text(
                  isRequired ? "*" : " ",
                  style: BaseTextStyle.customTextStyle(
                    kcolor: AppColor.kErrorColor,
                    kfontSize: 15,
                    kfontWeight: FontWeight.w600,
                  ),
                )
              ],
            ),
            const SizedBox(height: 3),
            Obx(
              () => FormBuilderTextField(
                onTap: onTap,
                // onSaved: onSaved,
                onSubmitted: onSubmitted,
                onTapOutside: (event) =>
                    FocusManager.instance.primaryFocus!.unfocus(),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                name: fieldName,
                controller: controller,
                // enabled: enabled,
                readOnly: readOnly,
                initialValue: initialValue,
                maxLines: isLongText == false ? 1 : 4,
                // expands: expandHeight > 75,
                textAlignVertical: TextAlignVertical.top,
                obscureText: secureViewText == true
                    ? isObsecure.value
                    : !isObsecure.value,
                inputFormatters: inputFormater,
                onChanged: (value) {
                  onChange!(value: value);
                },
                // valueTransformer: (text) => num.tryParse(text),
                validator: validator,
                style: BaseTextStyle.customTextStyle(
                  kfontSize: 15,
                  kfontWeight: FontWeight.w500,
                  kcolor: AppColor.kPrimaryColor,
                ),

                keyboardType: keyboardType,
                textInputAction: textInputAction,
                decoration: style2
                    ? InputDecoration(
                        // alignLabelWithHint: expandHeight >= 75,
                        alignLabelWithHint: isLongText,

                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        filled: true,
                        fillColor: enabled
                            ? Color.fromARGB(255, 239, 244, 255)
                            : AppColor.kGreyColor,

                        // contentPadding: const EdgeInsets.only(bottom: -10.0, top: 20, left: 10),

                        hintText: hintText ?? "Isi disini",
                        hintStyle: BaseTextStyle.hintTextStyle,
                        prefixIcon: extendPrefixItem,
                        suffixIcon: secureViewText == true
                            ? IconButton(
                                icon: Icon(
                                  isObsecure.value == true
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  color: isObsecure.value == true
                                      ? AppColor.kPrimaryColor
                                      : AppColor.kGreyColor,
                                ),
                                onPressed: () {
                                  isObsecure.value = !isObsecure.value;
                                },
                                splashColor: Colors.transparent,
                              )
                            : Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  if (readOnly == false) ...{
                                    Container(
                                      margin: const EdgeInsets.all(1),
                                      decoration: BoxDecoration(
                                        // color: AppColor.kPrimaryColor,
                                        borderRadius: BorderRadius.circular(7),
                                      ),
                                      child: IconButton(
                                        onPressed: () {
                                          if (onClearText != null) {
                                            onClearText();
                                          }
                                          if (controller != null) {
                                            controller.clear();
                                          }
                                        },
                                        padding: const EdgeInsets.all(-100),
                                        splashColor: AppColor.kPrimaryColor,
                                        icon: const Icon(
                                          Icons.close_rounded,
                                          color: AppColor.kGreyColor,
                                          size: 20,
                                        ),
                                      ),
                                    ),
                                  },
                                  if (extendSuffixItem != null)
                                    extendSuffixItem,
                                ],
                              ),

                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: const BorderSide(
                            color: AppColor.kErrorColor,
                          ), //menunaktifkan border
                        ),
                        focusedErrorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: const BorderSide(
                            color: AppColor.kErrorColor,
                          ), //menunaktifkan border
                        ),
                        errorStyle: BaseTextStyle.customTextStyle(
                          kfontSize: 12,
                          kcolor: AppColor.kFailedColor,
                          kfontWeight: FontWeight.w500,
                        ),
                      )
                    : InputDecoration(
                        // alignLabelWithHint: expandHeight >= 75,
                        alignLabelWithHint: isLongText,

                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        filled: true,
                        fillColor: enabled
                            ? AppColor.kGreyColor2
                            : Color.fromARGB(128, 209, 209, 209),
                        contentPadding: const EdgeInsets.only(
                            bottom: -10.0, top: 20, left: 10),

                        hintText: hintText ?? "Isi disini",
                        hintStyle: BaseTextStyle.hintTextStyle,
                        prefixIcon: extendPrefixItem,
                        suffixIcon: secureViewText == true
                            ? IconButton(
                                icon: Icon(
                                  isObsecure.value == true
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  color: isObsecure.value == true
                                      ? AppColor.kPrimaryColor
                                      : AppColor.kGreyColor,
                                ),
                                onPressed: () {
                                  isObsecure.value = !isObsecure.value;
                                },
                                splashColor: Colors.transparent,
                              )
                            : Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  if (readOnly == false) ...{
                                    Container(
                                      margin: const EdgeInsets.all(1),
                                      decoration: BoxDecoration(
                                        // color: AppColor.kPrimaryColor,
                                        borderRadius: BorderRadius.circular(7),
                                      ),
                                      child: IconButton(
                                        onPressed: () {
                                          if (onClearText != null) {
                                            onClearText();
                                          }
                                          if (controller != null) {
                                            controller.clear();
                                          }
                                        },
                                        padding: const EdgeInsets.all(-100),
                                        splashColor: AppColor.kPrimaryColor,
                                        icon: const Icon(
                                          Icons.close_rounded,
                                          color: AppColor.kGreyColor,
                                          size: 20,
                                        ),
                                      ),
                                    ),
                                  },
                                  if (extendSuffixItem != null)
                                    extendSuffixItem,
                                ],
                              ),

                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(radius),
                          borderSide: const BorderSide(
                            color: AppColor.kPrimaryColor,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(radius),
                          borderSide: const BorderSide(
                            color: AppColor.kPrimaryColor,
                          ), //menunaktifkan border
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(radius),
                          borderSide: const BorderSide(
                            color: AppColor.kErrorColor,
                          ), //menunaktifkan border
                        ),
                        focusedErrorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(radius),
                          borderSide: const BorderSide(
                            color: AppColor.kErrorColor,
                          ), //menunaktifkan border
                        ),
                        errorStyle: BaseTextStyle.customTextStyle(
                          kfontSize: 12,
                          kcolor: AppColor.kFailedColor,
                          kfontWeight: FontWeight.w500,
                        ),
                      ),
              ),
            ),
          ],
        ));
  }

  static selectFieldFormBuilder({
    required String title,
    required String fieldName,
    required dynamic dataSelect,
    String? initialValue,
    void Function(String?)? onChanged,
    String? Function(String?)? validator,
    bool isRequired = false,
    // required Function({
    //   dynamic valueItem,
    //   dynamic searchValue,
    // }) onChange
  }) {
    return FormBuilderChoiceChip<String>(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      decoration: InputDecoration(
        label: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              title,
              style: BaseTextStyle.customTextStyle(
                kcolor: AppColor.kPrimaryColor,
                kfontSize: 14,
                kfontWeight: FontWeight.w500,
              ),
            ),
            isRequired
                ? Text(
                    "*",
                    style: BaseTextStyle.customTextStyle(
                      kcolor: AppColor.kErrorColor,
                      kfontSize: 15,
                      kfontWeight: FontWeight.w600,
                    ),
                  )
                : const SizedBox(),
          ],
        ),
        errorStyle: BaseTextStyle.customTextStyle(
          kfontSize: 13,
          kcolor: AppColor.kFailedColor,
          kfontWeight: FontWeight.w500,
        ),
      ),
      name: fieldName,
      initialValue: initialValue,
      selectedColor: AppColor.kInactiveColor,
      showCheckmark: false,
      onChanged: onChanged,
      spacing: 6.0,
      options: List.generate(dataSelect.length, (index) {
        return FormBuilderChipOption(
          value: "${dataSelect[index]['value']}", //"{isPPN[index]['value']}",
          avatar: dataSelect[index]['isActive'] == true
              ? const Icon(
                  Icons.radio_button_checked_rounded,
                  color: AppColor.kPrimaryColor,
                )
              : const Icon(
                  Icons.radio_button_off_rounded,
                  color: AppColor.kPrimaryColor,
                ),
          child: BaseTextWidget.customText(
            text: "${dataSelect[index]['text']}",
            longTextClamp: true,
            maxLengthText: 25,
            fontWeight: FontWeight.w500,
            fontSize: 12,
          ),
        );
      }),
      validator: validator,
    );
  }

  static disableTextForm({
    required String disabledText,
    String? label,
    bool isRequired = false,
    String? Function(String?)? validator,
  }) {
    return TextFormField(
      enabled: false,
      readOnly: true,
      validator: validator,
      decoration: InputDecoration(
        floatingLabelBehavior: FloatingLabelBehavior.always,
        label: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              label ?? "",
              style: BaseTextStyle.customTextStyle(
                kcolor: AppColor.kPrimaryColor,
                kfontSize: 14,
                kfontWeight: FontWeight.w500,
              ),
            ),
            isRequired
                ? Text(
                    "*",
                    style: BaseTextStyle.customTextStyle(
                      kcolor: AppColor.kErrorColor,
                      kfontSize: 15,
                      kfontWeight: FontWeight.w600,
                    ),
                  )
                : const SizedBox(),
          ],
        ),
        filled: true,
        fillColor: AppColor.kGreyColor,
        contentPadding: const EdgeInsets.only(bottom: -10.0, top: 20, left: 10),
        disabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide.none,
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide.none,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: const BorderSide(
            color: AppColor.kPrimaryColor,
          ), //menunaktifkan border
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: const BorderSide(
            color: AppColor.kErrorColor,
          ), //menunaktifkan border
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: const BorderSide(
            color: AppColor.kErrorColor,
          ), //menunaktifkan border
        ),
        errorStyle: BaseTextStyle.customTextStyle(
          kfontSize: 12,
          kcolor: AppColor.kFailedColor,
          kfontWeight: FontWeight.w500,
        ),
        hintText: disabledText,
        hintStyle: BaseTextStyle.hintTextStyle,
      ),
    );
  }

  static disableTextForm2({
    required String disabledText,
    String? label,
    bool isRequired = false,
    String? Function(String?)? validator,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              label ?? "",
              style: BaseTextStyle.customTextStyle(
                kcolor: AppColor.kPrimaryColor,
                kfontSize: 12,
                kfontWeight: FontWeight.w500,
              ),
            ),
            isRequired
                ? Text(
                    "*",
                    style: BaseTextStyle.customTextStyle(
                      kcolor: AppColor.kErrorColor,
                      kfontSize: 15,
                      kfontWeight: FontWeight.w600,
                    ),
                  )
                : const SizedBox(),
          ],
        ),
        const SizedBox(height: 2),
        TextFormField(
          enabled: false,
          readOnly: true,
          validator: validator,
          decoration: InputDecoration(
            floatingLabelBehavior: FloatingLabelBehavior.always,
            // label:
            filled: true,
            fillColor: AppColor.kGreyColor.withOpacity(0.3),
            contentPadding:
                const EdgeInsets.only(bottom: -10.0, top: 20, left: 10),
            disabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: BorderSide.none,
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: const BorderSide(
                color: AppColor.kGreyColor,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: const BorderSide(
                color: AppColor.kGreyColor,
              ), //menunaktifkan border
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: const BorderSide(
                color: AppColor.kErrorColor,
              ), //menunaktifkan border
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(8),
              borderSide: const BorderSide(
                color: AppColor.kErrorColor,
              ), //menunaktifkan border
            ),
            errorStyle: BaseTextStyle.customTextStyle(
              kfontSize: 12,
              kcolor: AppColor.kFailedColor,
              kfontWeight: FontWeight.w500,
            ),
            hintText: disabledText,
            hintStyle: BaseTextStyle.hintTextStyle,
          ),
        ),
      ],
    );
  }

  static locationFieldFormBuilder({
    required BuildContext context,
    required controllerField,
    required String title,
    bool havePermission = true,
    bool suffixOnError = false,
    String? initialValue,
    String? Function(String?)? validator,
    controllerData,
    bool isRequired = false,
    bool enabled = true,
    bool readOnly = false,
    String? hintText,
    void Function()? onClearText,
  }) {
    TextEditingController textEditingLocation = TextEditingController();

    final locationText = "".obs;
    locationText.value =
        "${controllerField.profileData.value['province_name']}, ${controllerField.profileData.value['city_name']}, ${controllerField.profileData.value['district_name']}, ${controllerField.profileData.value['sub_district_name']}";

    textEditingLocation.text =
        controllerField.collectLocationIdentifier['preview'] ??
            locationText.value ??
            "Pilih Lokasi";

    Future<void> showAddress(BuildContext context) {
      return showModalBottomSheet(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(15.0))),
        backgroundColor: AppColor.kWhiteColor,
        // backgroundColor: Colors.transparent,
        context: context,
        isScrollControlled: true,
        builder: (context) => Padding(
          padding: EdgeInsets.only(
              top: 20,
              right: 10,
              left: 10,
              bottom: MediaQuery.of(context).viewInsets.bottom),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Pilih Alamat",
                      style: BaseTextStyle.customTextStyle(
                        kfontSize: 14,
                        kfontWeight: FontWeight.w600,
                      ),
                    ),
                    IconButton.filledTonal(
                      iconSize: 20,
                      onPressed: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        controllerData.searchFilter.clear();
                        Get.back();
                      },
                      icon: const Icon(Icons.close),
                    ),
                  ],
                ),
                const Divider(),
                BaseExtendWidget.locationPicker(
                  context: context,
                  collectInputController: controllerField,
                  controllerData: controllerData,
                )
              ],
            ),
          ),
        ),
      );
    }

    return InkWell(
      onTap: () => havePermission ? showAddress(context) : null,
      child: IgnorePointer(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  title,
                  style: BaseTextStyle.customTextStyle(
                    kcolor: AppColor.kPrimaryColor,
                    kfontSize: 12,
                    kfontWeight: FontWeight.w600,
                  ),
                ),
                // Text(
                //   isRequired ? "*" : " ",
                //   style: BaseTextStyle.customTextStyle(
                //     kcolor: AppColor.kErrorColor,
                //     kfontSize: 15,
                //     kfontWeight: FontWeight.w600,
                //   ),
                // )
              ],
            ),
            const SizedBox(height: 4),
            TextFormField(
              controller: textEditingLocation,
              readOnly: true,
              maxLines: 4,
              textAlignVertical: TextAlignVertical.top,
              decoration: InputDecoration(
                // alignLabelWithHint: expandHeight >= 75,
                alignLabelWithHint: true,

                floatingLabelBehavior: FloatingLabelBehavior.always,
                filled: true,
                fillColor: enabled
                    ? AppColor.kGreyColor2
                    : Color.fromARGB(128, 209, 209, 209),
                contentPadding:
                    const EdgeInsets.only(bottom: -10.0, top: 20, left: 10),

                hintText: hintText ?? "Isi disini",
                hintStyle: BaseTextStyle.hintTextStyle,

                suffixIcon: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    if (readOnly == false) ...{
                      Container(
                        margin: const EdgeInsets.all(1),
                        decoration: BoxDecoration(
                          // color: AppColor.kPrimaryColor,
                          borderRadius: BorderRadius.circular(7),
                        ),
                        child: IconButton(
                          onPressed: () {
                            if (onClearText != null) {
                              onClearText();
                            }
                            if (controllerData != null) {
                              textEditingLocation.clear();
                            }
                          },
                          padding: const EdgeInsets.all(-100),
                          splashColor: AppColor.kPrimaryColor,
                          icon: const Icon(
                            Icons.close_rounded,
                            color: AppColor.kGreyColor,
                            size: 20,
                          ),
                        ),
                      ),
                    },
                    // if  (extendSuffixItem != null) extendSuffixItem,
                  ],
                ),

                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(7),
                  borderSide: const BorderSide(
                    color: AppColor.kPrimaryColor,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(7),
                  borderSide: const BorderSide(
                    color: AppColor.kPrimaryColor,
                  ), //menunaktifkan border
                ),
                errorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(7),
                  borderSide: const BorderSide(
                    color: AppColor.kErrorColor,
                  ), //menunaktifkan border
                ),
                focusedErrorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(7),
                  borderSide: const BorderSide(
                    color: AppColor.kErrorColor,
                  ), //menunaktifkan border
                ),
                errorStyle: BaseTextStyle.customTextStyle(
                  kfontSize: 12,
                  kcolor: AppColor.kFailedColor,
                  kfontWeight: FontWeight.w500,
                ),
              ),
              validator: validator,
              style: BaseTextStyle.customTextStyle(
                kcolor: AppColor.kPrimaryColor,
                kfontSize: 15,
                kfontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
