import 'package:rbs_mobile_operation/core/assets/assets.gen.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class BaseLoadingWidget {
  static Widget cardShimmer(
      {required Color baseColor,
      required Color highlightColor,
      required int itemCount,
      double? shimmerheight,
      double roundedShimer = 10,
      double height = 200.0,
      double width = 200.0}) {
    return SizedBox(
      width: width,
      height: height,
      child: Shimmer.fromColors(
        baseColor: baseColor,
        highlightColor: highlightColor,
        child: ListView.separated(
          physics: const NeverScrollableScrollPhysics(),
          itemCount: itemCount,
          separatorBuilder: (context, index) => const Divider(height: 10),
          itemBuilder: (context, index) {
            return Container(
              height: shimmerheight,
              decoration: BoxDecoration(
                color: AppColor.kPrimaryColor,
                borderRadius: BorderRadius.circular(8),
              ),
            );
          },
        ),
      ),
    );
  }

  static Widget noLoadData() {
    return Container(
      padding: const EdgeInsets.all(10),
      height: 50,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Expanded(
              child: Padding(
            padding: EdgeInsets.only(right: 5.0),
            child: Divider(height: 20.0, thickness: 1.5),
          )),
          Text("Tidak ada lagi data dimuat",
              style: BaseTextStyle.customTextStyle(
                kfontSize: 12,
                kfontWeight: FontWeight.w500,
                kcolor: AppColor.kSoftBlueColor,
              )),
          const Expanded(
              child: Padding(
            padding: EdgeInsets.only(left: 5.0),
            child: Divider(height: 20.0, thickness: 1.5),
          )),
        ],
      ),
    );
  }

  static Widget rowCardShimmer({
    required Color baseColor,
    required Color highlightColor,
    Axis scrollDirection = Axis.vertical,
    double? shimmerheight,
    double roundedShimer = 10,
    double height = 200.0,
    double width = 200.0,
  }) {
    return SizedBox(
      width: width,
      height: height,
      child: Shimmer.fromColors(
        baseColor: Colors.grey.shade300,
        highlightColor: Colors.grey.shade100,
        child: Container(
          decoration: BoxDecoration(
            color: AppColor.kAvailableColor,
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }

  static Widget circularProgress({
    EdgeInsets? padding,
    required double width,
    required double height,
    Color color = AppColor.kPrimaryColor,
  }) {
    return Container(
      width: width,
      height: height,
      padding: padding,
      child: CircularProgressIndicator(
        color: color,
      ),
    );
  }

  static Widget noMoreDataWidget({double height = 200}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        // SvgPicture.asset(
        //   "assets/images/img-nodata.svg",
        //   height: height,
        // ),
        // Lottie.asset(
        //   'assets/images/nodata.json',
        //   fit: BoxFit.cover,
        //   height: height,
        // ),
        Assets.images.nodata.lottie(fit: BoxFit.cover, height: height),
        Text(
          'Data Tidak Tersedia',
          style: BaseTextStyle.customTextStyle(
            kfontSize: 14,
            kcolor: AppColor.kGreyColor,
            kfontWeight: FontWeight.w600,
          ),
        )
      ],
    );
  }
}
