import 'package:quickalert/quickalert.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class LogWidget {
  static snackbarInfo({
    required bool isError,
    required String messageTitle,
    required String message,
    Widget? widgetMessage,
    Widget? iconLeading,
    int durationMilisecons = 3000,
    Color? backgroundColor,
    required SnackPosition snackPosition,
  }) {
    return Future.delayed(const Duration(milliseconds: 500), () {
      // ignore: avoid_print
      print(Get.isSnackbarOpen);
      if (!Get.isSnackbarOpen) {
        Get.rawSnackbar(
          messageText: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Flexible(
                child: Column(
                  children: [
                    BaseTextWidget.customText(
                      text: message,
                      color: Colors.white,
                      isLongText: true,
                      fontWeight: FontWeight.w500,
                    ),
                  ],
                ),
              ),
            ],
          ),
          forwardAnimationCurve: Curves.elasticInOut,
          reverseAnimationCurve: Curves.easeOut,
          isDismissible: true,
          duration: Duration(milliseconds: durationMilisecons),
          icon: iconLeading ??
              const Icon(
                Icons.info,
                color: AppColor.kWhiteColor,
              ),
          barBlur: 20,
          margin: const EdgeInsets.only(top: 15, right: 15, left: 15),
          padding: const EdgeInsets.all(15),
          snackPosition: snackPosition,
          borderRadius: 5,
          backgroundColor: isError == true
              ? AppColor.kErrorColor
              : backgroundColor ?? AppColor.kSuccesColor,
        );
      }
    });
  }
}
