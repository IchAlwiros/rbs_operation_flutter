import 'package:rbs_mobile_operation/core/core.dart';

// class CustomDialog extends StatelessWidget {
//   final String title, content, positiveBtnText, negativeBtnText;
//   final GestureTapCallback positiveBtnPressed;

//   CustomDialog({
//     super.key,
//     required this.title,
//     required this.content,
//     required this.positiveBtnText,
//     required this.negativeBtnText,
//     required this.positiveBtnPressed,
//   });

//   @override
//   Widget build(BuildContext context) {
//     return Dialog(
//       elevation: 0,
//       backgroundColor: Colors.transparent,
//       child: _buildDialogContent(context),
//     );
//   }
// }

class CustomDialog extends StatelessWidget {
  final String title, content, positiveBtnText, negativeBtnText;
  final GestureTapCallback positiveBtnPressed;

  const CustomDialog({
    super.key,
    required this.title,
    required this.content,
    required this.positiveBtnText,
    required this.negativeBtnText,
    required this.positiveBtnPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      // child: _buildDialogContent(context),
      child: Button.filled(
          onPressed: () => showGeneralDialog(
                barrierDismissible: false,
                context: context,
                barrierColor: Colors.black54, // space around dialog
                transitionDuration: Duration(milliseconds: 800),
                transitionBuilder: (context, a1, a2, child) {
                  return ScaleTransition(
                    scale: CurvedAnimation(
                        parent: a1,
                        curve: Curves.elasticOut,
                        reverseCurve: Curves.easeOutCubic),
                    child: CustomDialog(
                      // our custom dialog
                      title: "Here goes title",
                      content:
                          "Here goes the content of dialog. Here goes the content of dialog. Here goes the content of dialog.",
                      positiveBtnText: "Done",
                      negativeBtnText: "Cancel",
                      positiveBtnPressed: () {
                        // Do something here
                        Navigator.of(context).pop();
                      },
                    ),
                  );
                },
                pageBuilder: (BuildContext context, Animation animation,
                    Animation secondaryAnimation) {
                  return SizedBox();
                },
              ),
          label: "label"),
    );
  }
}

// Widget _buildDialogContent(BuildContext context) {
//   return Stack(
//     alignment: Alignment.topCenter,
//     children: <Widget>[
//       // Bottom rectangular box,
//       // Top circle view, // it should be written below to appear above
//     ],
//   );
// }