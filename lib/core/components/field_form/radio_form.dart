import 'package:rbs_mobile_operation/core/core.dart';

class CustomRadioFormField extends StatelessWidget {
  const CustomRadioFormField({
    super.key,
    required this.title,
    required this.fieldName,
    required this.dataSelect,
    this.initialValue,
    this.onChanged,
    this.validator,
    this.isRequired = false,
  });

  final String title;
  final String fieldName;
  final dynamic dataSelect;
  final String? initialValue;
  final void Function(String?)? onChanged;
  final String? Function(String?)? validator;
  final bool isRequired;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              title,
              style: BaseTextStyle.customTextStyle(
                kcolor: AppColor.kPrimaryColor,
                kfontSize: 12,
                kfontWeight: FontWeight.w600,
              ),
            ),
            isRequired
                ? Text(
                    "*",
                    style: BaseTextStyle.customTextStyle(
                      kcolor: AppColor.kErrorColor,
                      kfontSize: 15,
                      kfontWeight: FontWeight.w600,
                    ),
                  )
                : const SizedBox(),
          ],
        ),
        FormBuilderChoiceChip<String>(
          // backgroundColor: Colors.black,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.only(top: 0, bottom: 3),
            errorStyle: BaseTextStyle.customTextStyle(
              kfontSize: 13,
              kcolor: AppColor.kFailedColor,
              kfontWeight: FontWeight.w500,
            ),
          ),
          name: fieldName,
          initialValue: initialValue,
          selectedColor: AppColor.kInactiveColor,
          showCheckmark: false,
          onChanged: onChanged,
          spacing: 6.0,
          options: List.generate(dataSelect.length, (index) {
            return FormBuilderChipOption(
              value:
                  "${dataSelect[index]['value']}", //"{isPPN[index]['value']}",
              avatar: dataSelect[index]['isActive'] == true
                  ? const Icon(
                      Icons.radio_button_checked_rounded,
                      color: AppColor.kPrimaryColor,
                    )
                  : const Icon(
                      Icons.radio_button_off_rounded,
                      color: AppColor.kPrimaryColor,
                    ),
              child: BaseTextWidget.customText(
                text: "${dataSelect[index]['text']}",
                longTextClamp: true,
                maxLengthText: 25,
                fontWeight: FontWeight.w500,
                fontSize: 12,
              ),
            );
          }),
          validator: validator,
        ),
      ],
    );
  }
}
