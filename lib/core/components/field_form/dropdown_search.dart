import 'package:dropdown_search/dropdown_search.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class CustomDropDownSearch extends StatelessWidget {
  const CustomDropDownSearch({
    super.key,
    required this.fieldName,
    required this.controller,
    required this.asyncItems,
    required this.onChange,
    this.spesificFieldView = 'name',
    this.validator,
    this.initialValue,
    this.hintText = 'Pilih item',
    this.label,
    this.currentValue,
    this.isRequired = false,
    this.enabled = true,
  });

  final Future<List<dynamic>> Function(String)? asyncItems;
  final String fieldName;
  final TextEditingController controller;
  final Function({
    dynamic data,
    dynamic valueItem,
    dynamic searchValue,
  }) onChange;
  final String spesificFieldView;
  final String? Function(dynamic)? validator;
  final dynamic initialValue;
  final String hintText;
  final String? label;
  final String? currentValue;
  final bool isRequired;
  final bool enabled;

  @override
  Widget build(BuildContext context) {
    TextEditingController currentData = TextEditingController(
      text: currentValue,
    );

    // print(controller.text);
    // print(currentData.text == "");
    // print(controller.text == "");
    if (initialValue != null) {
      // print(initialValue);
      controller.text = initialValue['id'].toString();
      currentData.text = initialValue['name'];
      // controller.text = currentValue['id'].toString();
      // print(initialValue);
    }
    // if (value == null || value.isEmpty) {
    //   value = null;
    // } else {
    //   if (value != null) {
    //     controller.text = value['id'].toString();
    //   }
    // }

    return Stack(
      children: [
        SizedBox(
          height: 50,
          child: FormBuilderTextField(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            name: fieldName,
            controller: controller,
            enabled: true,
            readOnly: true,
            style: TextStyle(color: Colors.white.withOpacity(0.1)),
            decoration: InputDecoration(
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide.none,
              ),
            ),
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  label ?? "",
                  style: BaseTextStyle.customTextStyle(
                    kcolor: AppColor.kPrimaryColor,
                    kfontSize: 12,
                    kfontWeight: FontWeight.w600,
                  ),
                ),
                isRequired
                    ? Text(
                        "*",
                        style: BaseTextStyle.customTextStyle(
                          kcolor: AppColor.kErrorColor,
                          kfontSize: 15,
                          kfontWeight: FontWeight.w600,
                        ),
                      )
                    : const SizedBox(),
              ],
            ),
            const SizedBox(height: 2),
            DropdownSearch(
              autoValidateMode: AutovalidateMode.onUserInteraction,
              asyncItems: asyncItems,
              selectedItem: initialValue,
              enabled: enabled,

              // (String filter) async {
              //   salesListController.lookupCustomer(searching: filter);
              //   return salesListController.dataCustomer;
              // },
              itemAsString: (u) {
                if (spesificFieldView != 'name') {
                  return u?[spesificFieldView];
                } else {
                  return u?['name'];
                }
              },
              onChanged: (data) {
                if (spesificFieldView != 'name') {
                  onChange(
                    data: data,
                    searchValue: data['name'],
                    valueItem: data['id'],
                  );
                } else {
                  onChange(
                    data: data,
                    searchValue: data[spesificFieldView],
                    valueItem: data['id'],
                  );
                }
                controller.text = "${data['id']}";
              },
              dropdownBuilder: (context, selectedItem) {
                return selectedItem != null
                    ? Text(
                        (spesificFieldView != 'name')
                            ? selectedItem[spesificFieldView]
                            : selectedItem?['name'],
                        style: BaseTextStyle.customTextStyle(
                          kcolor: AppColor.kPrimaryColor,
                          kfontSize: 12,
                          kfontWeight: FontWeight.w500,
                        ),
                      )
                    : Text(
                        currentData.text == ""
                            ? hintText ?? "Isi disini"
                            : currentData.text,
                        style: hintText != null
                            ? hintText.toLowerCase().contains('pilih') &&
                                    currentData.text == ""
                                ? BaseTextStyle.hintTextStyle
                                : BaseTextStyle.customTextStyle(
                                    kcolor: AppColor.kPrimaryColor,
                                    kfontSize: 12,
                                    kfontWeight: FontWeight.w500,
                                  )
                            : BaseTextStyle.hintTextStyle,
                      );
              },
              compareFn: (item1, item2) {
                return true;
              },

              popupProps: PopupProps.menu(
                isFilterOnline: true,
                showSearchBox: true,
                showSelectedItems: true,
                searchDelay: const Duration(milliseconds: 500),
                searchFieldProps: TextFieldProps(
                    decoration: InputDecoration(
                  constraints: const BoxConstraints(maxHeight: 45),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                      color: AppColor.kGreyColor,
                    ), //menunaktifkan borde
                    // borderSide: BorderSide.none,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                      color: AppColor.kGreyColor,
                    ), //menunaktifkan borde
                    // borderSide: BorderSide.none,
                  ),
                  hintText: 'Pencarian..',
                  hintStyle: BaseTextStyle.hintTextStyle,
                )),
              ),
              validator: validator,
              dropdownDecoratorProps: DropDownDecoratorProps(
                dropdownSearchDecoration: InputDecoration(
                  filled: true,
                  fillColor: enabled
                      ? AppColor.kGreyColor2
                      : const Color.fromARGB(177, 225, 225, 225),
                  floatingLabelBehavior: FloatingLabelBehavior.always,
                  contentPadding:
                      const EdgeInsets.only(bottom: -10.0, top: 20, left: 10),
                  hintText: currentData.text == ""
                      ? hintText ?? "Isi disini"
                      : currentData.text,
                  hintStyle: hintText != null
                      ? hintText.toLowerCase().contains('pilih') &&
                              currentData.text == ""
                          ? BaseTextStyle.hintTextStyle
                          : BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kPrimaryColor,
                              kfontSize: 14,
                              kfontWeight: FontWeight.w500,
                            )
                      : BaseTextStyle.hintTextStyle,
                  disabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                        width: 1.2, color: Colors.grey), //menunaktifkan borde
                    // borderSide: BorderSide.none,
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                        width: 1.2, color: Colors.grey), //menunaktifkan borde
                    // borderSide: BorderSide.none,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                        width: 1.2, color: Colors.grey), //menunaktifkan border
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                      color: AppColor.kErrorColor,
                    ), //menunaktifkan border
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: const BorderSide(
                      color: AppColor.kErrorColor,
                    ), //menunaktifkan border
                  ),
                  errorStyle: BaseTextStyle.customTextStyle(
                    kfontSize: 12,
                    kcolor: AppColor.kFailedColor,
                    kfontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
