import 'package:flutter/services.dart';
import 'package:rbs_mobile_operation/core/core.dart';

enum TextFieldStyleType { style1, style2 }

class CustomTextField extends StatelessWidget {
  const CustomTextField.style1({
    super.key,
    required this.title,
    required this.fieldName,
    this.style = TextFieldStyleType.style1,
    this.controller,
    this.isRequired = false,
    this.secureViewText = false,
    this.isLongText = false,
    this.initialValue,
    this.radius = 7,
    this.onClearText,
    this.suffixOnError = false,
    this.extendSuffixItem,
    this.extendPrefixItem,
    this.onChange,
    this.onTap,
    this.validator,
    this.keyboardType,
    this.textInputAction,
    this.inputFormater,
    this.enabled = true,
    this.readOnly = false,
    this.hintText,
    this.onSubmitted,
  });

  const CustomTextField.style2({
    super.key,
    required this.title,
    required this.fieldName,
    this.style = TextFieldStyleType.style2,
    this.controller,
    this.isRequired = false,
    this.secureViewText = false,
    this.isLongText = false,
    this.initialValue,
    this.radius = 7,
    this.onClearText,
    this.suffixOnError = false,
    this.extendSuffixItem,
    this.extendPrefixItem,
    this.onChange,
    this.onTap,
    this.validator,
    this.keyboardType,
    this.textInputAction,
    this.inputFormater,
    this.enabled = true,
    this.readOnly = false,
    this.hintText,
    this.onSubmitted,
  });

  final String title;
  final String fieldName;
  final TextFieldStyleType style;
  final TextEditingController? controller;
  final bool isRequired;
  final bool secureViewText;
  final bool isLongText;
  final String? initialValue;
  final double radius;
  final void Function()? onClearText;
  final bool suffixOnError;
  final Widget? extendSuffixItem;
  final Widget? extendPrefixItem;
  final Function({
    dynamic value,
  })? onChange;
  final void Function()? onTap;
  final String? Function(String?)? validator;
  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;
  final List<TextInputFormatter>? inputFormater;
  final bool enabled;
  final bool readOnly;
  final String? hintText;
  final void Function(String?)? onSubmitted;

  @override
  Widget build(BuildContext context) {
    var isObsecure = true.obs;

    // if (style2) {
    //   Obx(() {
    //     inputDecoration =
    //     return const SizedBox();
    //   });
    // }

    return SizedBox(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  title,
                  style: BaseTextStyle.customTextStyle(
                    kcolor: AppColor.kPrimaryColor,
                    kfontSize: 12,
                    kfontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  isRequired ? "*" : " ",
                  style: BaseTextStyle.customTextStyle(
                    kcolor: AppColor.kErrorColor,
                    kfontSize: 15,
                    kfontWeight: FontWeight.w600,
                  ),
                )
              ],
            ),
            const SizedBox(height: 3),
            Obx(
              () => FormBuilderTextField(
                onTap: onTap,
                // onSaved: onSaved,
                onSubmitted: onSubmitted,
                onTapOutside: (event) =>
                    FocusManager.instance.primaryFocus!.unfocus(),
                autovalidateMode: AutovalidateMode.onUserInteraction,
                name: fieldName,
                controller: controller,
                // enabled: enabled,
                readOnly: readOnly,
                initialValue: initialValue,
                maxLines: isLongText == false ? 1 : 4,
                // expands: expandHeight > 75,
                textAlignVertical: TextAlignVertical.top,
                obscureText: secureViewText == true
                    ? isObsecure.value
                    : !isObsecure.value,
                inputFormatters: inputFormater,
                onChanged: (value) {
                  onChange!(value: value);
                },
                // valueTransformer: (text) => num.tryParse(text),
                validator: validator,
                style: BaseTextStyle.customTextStyle(
                  kfontSize: 15,
                  kfontWeight: FontWeight.w500,
                  kcolor: AppColor.kPrimaryColor,
                ),

                keyboardType: keyboardType,
                textInputAction: textInputAction,
                decoration: style == TextFieldStyleType.style1
                    ? InputDecoration(
                        // alignLabelWithHint: expandHeight >= 75,
                        alignLabelWithHint: isLongText,

                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        filled: true,
                        fillColor: enabled
                            ? Color.fromARGB(255, 239, 244, 255)
                            : AppColor.kGreyColor,

                        // contentPadding: const EdgeInsets.only(bottom: -10.0, top: 20, left: 10),

                        hintText: hintText ?? "Isi disini",
                        hintStyle: BaseTextStyle.hintTextStyle,
                        prefixIcon: extendPrefixItem,
                        suffixIcon: secureViewText == true
                            ? IconButton(
                                icon: Icon(
                                  isObsecure.value == true
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  color: isObsecure.value == true
                                      ? AppColor.kPrimaryColor
                                      : AppColor.kGreyColor,
                                ),
                                onPressed: () {
                                  isObsecure.value = !isObsecure.value;
                                },
                                splashColor: Colors.transparent,
                              )
                            : Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  if (readOnly == false) ...{
                                    Container(
                                      margin: const EdgeInsets.all(1),
                                      decoration: BoxDecoration(
                                        // color: AppColor.kPrimaryColor,
                                        borderRadius: BorderRadius.circular(7),
                                      ),
                                      child: IconButton(
                                        onPressed: () {
                                          if (onClearText != null) {
                                            onClearText!();
                                          }
                                          if (controller != null) {
                                            controller!.clear();
                                          }
                                        },
                                        padding: const EdgeInsets.all(-100),
                                        splashColor: AppColor.kPrimaryColor,
                                        icon: const Icon(
                                          Icons.close_rounded,
                                          color: AppColor.kGreyColor,
                                          size: 20,
                                        ),
                                      ),
                                    ),
                                  },
                                  if (extendSuffixItem != null)
                                    extendSuffixItem!,
                                ],
                              ),

                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: const BorderSide(
                            color: AppColor.kGreyColor,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: BorderSide.none,
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: const BorderSide(
                            color: AppColor.kErrorColor,
                          ), //menunaktifkan border
                        ),
                        focusedErrorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15),
                          borderSide: const BorderSide(
                            color: AppColor.kErrorColor,
                          ), //menunaktifkan border
                        ),
                        errorStyle: BaseTextStyle.customTextStyle(
                          kfontSize: 12,
                          kcolor: AppColor.kFailedColor,
                          kfontWeight: FontWeight.w500,
                        ),
                      )
                    : InputDecoration(
                        // alignLabelWithHint: expandHeight >= 75,
                        alignLabelWithHint: isLongText,

                        floatingLabelBehavior: FloatingLabelBehavior.always,
                        filled: true,
                        fillColor: enabled
                            ? AppColor.kGreyColor2
                            : Color.fromARGB(128, 209, 209, 209),
                        contentPadding: const EdgeInsets.only(
                            bottom: -10.0, top: 20, left: 10),

                        hintText: hintText ?? "Isi disini",
                        hintStyle: BaseTextStyle.hintTextStyle,
                        prefixIcon: extendPrefixItem,
                        suffixIcon: secureViewText == true
                            ? IconButton(
                                icon: Icon(
                                  isObsecure.value == true
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  color: isObsecure.value == true
                                      ? AppColor.kPrimaryColor
                                      : AppColor.kGreyColor,
                                ),
                                onPressed: () {
                                  isObsecure.value = !isObsecure.value;
                                },
                                splashColor: Colors.transparent,
                              )
                            : Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  if (readOnly == false) ...{
                                    Container(
                                      margin: const EdgeInsets.all(1),
                                      decoration: BoxDecoration(
                                        // color: AppColor.kPrimaryColor,
                                        borderRadius: BorderRadius.circular(7),
                                      ),
                                      child: IconButton(
                                        onPressed: () {
                                          if (onClearText != null) {
                                            onClearText!();
                                          }
                                          if (controller != null) {
                                            controller!.clear();
                                          }
                                        },
                                        padding: const EdgeInsets.all(-100),
                                        splashColor: AppColor.kPrimaryColor,
                                        icon: const Icon(
                                          Icons.close_rounded,
                                          color: AppColor.kGreyColor,
                                          size: 20,
                                        ),
                                      ),
                                    ),
                                  },
                                  if (extendSuffixItem != null)
                                    extendSuffixItem!,
                                ],
                              ),

                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(radius),
                          borderSide: const BorderSide(
                            color: AppColor.kPrimaryColor,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(radius),
                          borderSide: const BorderSide(
                            color: AppColor.kPrimaryColor,
                          ), //menunaktifkan border
                        ),
                        errorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(radius),
                          borderSide: const BorderSide(
                            color: AppColor.kErrorColor,
                          ), //menunaktifkan border
                        ),
                        focusedErrorBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(radius),
                          borderSide: const BorderSide(
                            color: AppColor.kErrorColor,
                          ), //menunaktifkan border
                        ),
                        errorStyle: BaseTextStyle.customTextStyle(
                          kfontSize: 12,
                          kcolor: AppColor.kFailedColor,
                          kfontWeight: FontWeight.w500,
                        ),
                      ),
              ),
            ),
          ],
        ));
  }
}
