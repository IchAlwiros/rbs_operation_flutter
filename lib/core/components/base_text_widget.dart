import 'package:google_fonts/google_fonts.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:readmore/readmore.dart';

class BaseTextWidget {
  static Text customText({
    required dynamic text,
    double fontSize = 12,
    dynamic color,
    dynamic letterSpacing,
    FontWeight fontWeight = FontWeight.w500,
    bool longTextClamp = false,
    int maxLengthText = 25,
    TextAlign? textAlign,
    TextStyle? style,
    bool isLongText = false, // TRUE JIKA INGIN PANJANG TANPA ...
    String? extendText,
    String? extendOnFirst,
    int? maxLines,
    TextOverflow? overflow,
    bool localCurency = false,
    FontStyle? fontStyle,
  }) {
    if (text != null) {
      if (text is num) {
        text < 0
            // ignore: prefer_interpolation_to_compose_strings
            ? text = "-" +
                BaseGlobalFuntion.currencyLocalConvert(
                  nominal: double.parse(text.toString().replaceAll('-', '')),
                  isLocalCurency: localCurency,
                )
            : text = BaseGlobalFuntion.currencyLocalConvert(
                nominal: text,
                isLocalCurency: localCurency,
              );
      } else {
        if (!isLongText) {
          // JIKA ISLONGTEXT FALSE MAKA HENDLE CLAMP AKTIF DENGAN PARAMETER MAXLENGTH NYA
          if (longTextClamp) {
            if (text.length > maxLengthText) {
              text = "${text.substring(0, maxLengthText)}...";
            }
          } else {
            if (text.length > maxLengthText) {
              int index = maxLengthText;

              while (index < text.length) {
                if (index > maxLengthText * 2) {
                  // Jika sudah lebih dari dua kali maxLengthText, tambahkan elipsis
                  text = "${text.substring(0, maxLengthText * 2)}...";
                  break;
                }

                text = '${text.substring(0, index)}\n${text.substring(index)}';
                index += maxLengthText +
                    1; // Menambah satu untuk karakter baris baru
              }
            }
          }
        }
      }
    } else {
      text = text.toString();
      text = "-";
    }

    if (text == 'null') {
      text = text.toString();
      text = "-";
    }

    if (text == '') {
      text = text.toString();
      text = "-";
    }

    if (extendText != null) {
      text = text + extendText;
    }

    if (extendOnFirst != null) {
      text = extendOnFirst + text;
    }

    return Text(
      text,
      style: style ??
          TextStyle(
            letterSpacing: letterSpacing,
            fontSize: fontSize,
            fontWeight: fontWeight,
            color: color,
            fontStyle: fontStyle,
          ),
      // GoogleFonts.poppins(
      //   letterSpacing: letterSpacing,
      //   fontSize: fontSize,
      //   fontWeight: fontWeight,
      //   color: color,
      //   fontStyle: fontStyle,
      // ),
      textAlign: textAlign,
      maxLines: maxLines,
      overflow: overflow,
    );
  }

  static Widget customReadmore({
    required dynamic text,
    FontWeight? fontWeight,
    double? fontSize,
    double? letterSpacing,
    Color? color,
    String trimCollapsedText = 'read more',
    String trimExpandedText = 'less',
  }) {
    return ReadMoreText(
      text + ' ',
      trimLines: 2,
      trimMode: TrimMode.Line,
      trimCollapsedText: trimCollapsedText,
      trimExpandedText: trimExpandedText,
      style: GoogleFonts.poppins(
        letterSpacing: letterSpacing,
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: color,
      ),
      moreStyle: GoogleFonts.poppins(
        fontSize: 12,
        fontWeight: FontWeight.w500,
        color: AppColor.kSoftBlueColor,
      ),
      lessStyle: GoogleFonts.poppins(
        fontSize: 12,
        fontWeight: FontWeight.w500,
        color: AppColor.kSoftBlueColor,
      ),
    );
  }

  static TextSpan customTextSpan({
    required dynamic text,
    double fontSize = 12,
    dynamic color,
    dynamic letterSpacing,
    FontWeight fontWeight = FontWeight.w500,
    bool longTextClamp = false,
    int maxLengthText = 25,
    TextAlign? textAlign,
    TextStyle? style,
    bool isLongText = false, // TRUE JIKA INGIN PANJANG TANPA ...
    String? extendText,
    bool localCurency = false,
  }) {
    if (text != null) {
      if (text is num) {
        text < 0
            // ignore: prefer_interpolation_to_compose_strings
            ? text = "-" +
                BaseGlobalFuntion.currencyLocalConvert(
                  nominal: double.parse(text.toString().replaceAll('-', '')),
                  isLocalCurency: localCurency,
                )
            : text = BaseGlobalFuntion.currencyLocalConvert(
                nominal: text,
                isLocalCurency: localCurency,
              );
      } else {
        if (longTextClamp) {
          if (text.length > maxLengthText) {
            text = "${text.substring(0, maxLengthText)}...";
          }
        } else {
          if (text.length > maxLengthText) {
            int index = maxLengthText;

            while (index < text.length) {
              if (!isLongText) {
                if (index > maxLengthText * 2) {
                  // Jika sudah lebih dari dua kali maxLengthText, tambahkan elipsis
                  text = "${text.substring(0, maxLengthText * 2)}...";
                  break;
                }
              }
              text = '${text.substring(0, index)}\n${text.substring(index)}';
              index +=
                  maxLengthText + 1; // Menambah satu untuk karakter baris baru
            }
          }
        }
      }
    } else {
      text = text.toString();
      text = "-";
    }

    if (text == 'null') {
      text = text.toString();
      text = "-";
    }

    if (extendText != null) {
      text = text + extendText;
    }

    return TextSpan(
      text: text,
      style: style ??
          TextStyle(
            letterSpacing: letterSpacing,
            fontSize: fontSize,
            fontWeight: fontWeight,
            color: color,
          ),
    );
  }
}
