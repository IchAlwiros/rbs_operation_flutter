import 'package:quickalert/quickalert.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class BaseInfo {
  static basicSnacbar({
    required bool isError,
    required String messageTitle,
    required String message,
    Widget? widgetMessage,
    Widget? iconLeading,
    int durationMilisecons = 2000,
    Color? backgroundColor,
    required SnackPosition snackPosition,
  }) {
    return Get.rawSnackbar(
      // message,
      // '',
      // '',
      // '',
      // titleText: Row(
      //   children: [
      //     Flexible(
      //       child: Column(
      //         // crossAxisAlignment: CrossAxisAlignment.end,
      //         // mainAxisAlignment: MainAxisAlignment.end,
      //         // mainAxisSize: MainAxisSize.min,
      //         children: [
      //           BaseTextWidget.customText(
      //             text: "Terjadi Kesalahan!",
      //             color: Colors.white,
      //             isLongText: true,
      //             fontWeight: FontWeight.w700,
      //           ),
      //         ],
      //       ),
      //     ),
      //   ],
      // ),

      // titleText: const SizedBox(),
      messageText: Row(
        // mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(
            child: Column(
              children: [
                BaseTextWidget.customText(
                  text: message,
                  color: Colors.white,
                  isLongText: true,
                  fontWeight: FontWeight.w500,
                ),
              ],
            ),
          ),
        ],
      ),
      forwardAnimationCurve: Curves.elasticInOut,
      reverseAnimationCurve: Curves.easeOut,
      isDismissible: true,
      duration: Duration(milliseconds: durationMilisecons),
      icon: iconLeading ??
          const Icon(
            Icons.info,
            color: AppColor.kWhiteColor,
          ),
      barBlur: 20,
      margin: const EdgeInsets.only(top: 15, right: 15, left: 15),
      padding: const EdgeInsets.all(15),
      snackPosition: snackPosition,
      borderRadius: 5,
      backgroundColor: isError == true
          ? AppColor.kErrorColor
          : backgroundColor ?? AppColor.kSuccesColor,
    );
  }

  static log({
    required bool isError,
    required String messageTitle,
    required String message,
    Widget? widgetMessage,
    Widget? iconLeading,
    int durationMilisecons = 3000,
    Color? backgroundColor,
    required SnackPosition snackPosition,
  }) {
    return Future.delayed(const Duration(milliseconds: 500), () {
      // ignore: avoid_print
      print(Get.isSnackbarOpen);
      if (!Get.isSnackbarOpen) {
        Get.rawSnackbar(
          messageText: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Flexible(
                child: Column(
                  children: [
                    BaseTextWidget.customText(
                      text: message,
                      color: Colors.white,
                      isLongText: true,
                      fontWeight: FontWeight.w500,
                    ),
                  ],
                ),
              ),
            ],
          ),
          forwardAnimationCurve: Curves.elasticInOut,
          reverseAnimationCurve: Curves.easeOut,
          isDismissible: true,
          duration: Duration(milliseconds: durationMilisecons),
          icon: iconLeading ??
              const Icon(
                Icons.info,
                color: AppColor.kWhiteColor,
              ),
          barBlur: 20,
          margin: const EdgeInsets.only(top: 15, right: 15, left: 15),
          padding: const EdgeInsets.all(15),
          snackPosition: snackPosition,
          borderRadius: 5,
          backgroundColor: isError == true
              ? AppColor.kErrorColor
              : backgroundColor ?? AppColor.kSuccesColor,
        );
      }
    });
  }

  static simpleDialog({
    required String topTitle,
    required String contentDescription,
    required BuildContext context,
    required Function() onKlikButton,
  }) {
    QuickAlert.show(
      context: context,
      type: QuickAlertType.success,
      title: '',
      widget: SizedBox(
        height: 80,
        child: Column(
          children: [
            Text(
              topTitle,
              textAlign: TextAlign.center,
              style: BaseTextStyle.customTextStyle(
                kcolor: AppColor.kPrimaryColor,
                kfontSize: 18,
                kfontWeight: FontWeight.w600,
              ),
            ),
            const SizedBox(height: 20),
            Text(
              contentDescription,
              textAlign: TextAlign.center,
              style: BaseTextStyle.customTextStyle(
                kcolor: AppColor.kPrimaryColor,
                kfontSize: 12,
                kfontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
      confirmBtnTextStyle: BaseTextStyle.customTextStyle(
        kcolor: AppColor.kWhiteColor,
        kfontWeight: FontWeight.w600,
        kfontSize: 12,
      ),
      confirmBtnColor: AppColor.kPrimaryColor,
      onConfirmBtnTap: onKlikButton,
    );
  }

  static simpleConfirmDialog({
    required String topTitle,
    required String contentDescription,
    required BuildContext context,
    required Function() onKlikButton,
  }) {
    QuickAlert.show(
      context: context,
      type: QuickAlertType.confirm,
      title: '',
      widget: SizedBox(
        height: 80,
        child: Column(
          children: [
            Text(
              topTitle,
              textAlign: TextAlign.center,
              style: BaseTextStyle.customTextStyle(
                kcolor: AppColor.kPrimaryColor,
                kfontSize: 18,
                kfontWeight: FontWeight.w600,
              ),
            ),
            const SizedBox(height: 20),
            Text(
              contentDescription,
              textAlign: TextAlign.center,
              style: BaseTextStyle.customTextStyle(
                kcolor: AppColor.kPrimaryColor,
                kfontSize: 12,
                kfontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
      confirmBtnTextStyle: BaseTextStyle.customTextStyle(
        kcolor: AppColor.kWhiteColor,
        kfontWeight: FontWeight.w600,
        kfontSize: 15,
      ),
      confirmBtnColor: AppColor.kPrimaryColor,
      cancelBtnTextStyle: BaseTextStyle.customTextStyle(
        kcolor: AppColor.kGreyColor,
        kfontWeight: FontWeight.w600,
        kfontSize: 15,
      ),
      onConfirmBtnTap: onKlikButton,
    );
  }
}
