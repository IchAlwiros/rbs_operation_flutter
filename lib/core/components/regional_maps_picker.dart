import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_place_picker_mb/google_maps_place_picker.dart';
import 'package:rbs_mobile_operation/core/constants/constans.dart';
import 'package:rbs_mobile_operation/core/constants/variables.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class GMapsPicker extends StatefulWidget {
  final Future<dynamic> Function({
    dynamic lat,
    dynamic lng,
    TextEditingController? addressPreview,
  }) mapsHelper;

  // final Position? position;

  final TextEditingController detailMapsControllers;
  final dynamic collectMapsData;
  const GMapsPicker({
    // required this.position,
    required this.mapsHelper,
    required this.detailMapsControllers,
    required this.collectMapsData,
    super.key,
  });

  @override
  State<GMapsPicker> createState() => _GMapsPickerState();
}

class _GMapsPickerState extends State<GMapsPicker> {
  Position? position;
  var isLoading = false.obs;

  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
  }

  // _getCurrentLocation() async {
  //   Position currentPosition = await Geolocator.getCurrentPosition(
  //       desiredAccuracy: LocationAccuracy.high);

  //   // Position? knowLastLoaction = await Geolocator.getLastKnownPosition();

  //   setState(() {
  //     position = knowLastLoaction ?? currentPosition;
  //   });
  // }

  Future<void> _getCurrentLocation() async {
    isLoading.value = true;
    try {
      LocationPermission permission;

      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
      }

      // if (await permission)

      if (permission == LocationPermission.deniedForever) {
        // Permissions are denied forever, handle appropriately.
        Get.back();
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }
      Position currentPosition = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high,
      );

      Position? knowLastLoaction = await Geolocator.getLastKnownPosition();
      isLoading.value = false;
      widget.mapsHelper(
        lat: currentPosition.latitude,
        lng: currentPosition.longitude,
        // addressPreview: detailMapsControllers,
        addressPreview: tempFullAddress,
      );

      setState(() {
        position = knowLastLoaction ?? currentPosition;
      });
    } catch (error) {
      // Handle location retrieval errors gracefully
      BaseInfo.log(
        isError: true,
        messageTitle: "Gagal",
        message: 'Lokasi anda tidak ditemukan',
        snackPosition: SnackPosition.TOP,
      );
      // You can also show a user-friendly error message or provide alternative options here
    }
  }

  bool selectedPoint = false;
  TextEditingController tempFullAddress = TextEditingController();

  var searcingState = false.obs;
  var isShowBottomInfo = false.obs;
  var selectedPlaceStr = "".obs;

  final DraggableScrollableController sheetController =
      DraggableScrollableController();

  // @override
  // void initState() {
  //   currentPosition();
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    // if (selectedPoint == false) {
    //   // print("SELECTED FALSE");
    //   widget.mapsHelper(
    //     lat: position!.latitude,
    //     lng: position!.longitude,
    //     // addressPreview: detailMapsControllers,
    //     addressPreview: tempFullAddress,
    //   );
    // }
    return Scaffold(
      body: position == null
          ? const Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SpinKitThreeBounce(
                    size: 30.0,
                    color: AppColor.kPrimaryColor,
                  ),
                  SizedBox(height: 15),
                  Text("Loading...")
                ],
              ),
            )
          : Stack(
              children: [
                // borderRadius: BorderRadius.circular(10),

                PlacePicker(
                  apiKey: gMapsApiKey,
                  automaticallyImplyAppBarLeading: false,
                  // searchForInitialValue: false,

                  initialPosition: LatLng(
                    // locationCoordinatesControllers.currentLat.value,
                    // locationCoordinatesControllers.currentLong.value,
                    position?.latitude ?? 0.0,
                    position?.longitude ?? 0.0,
                  ),

                  selectedPlaceWidgetBuilder:
                      (context, selectedPlace, state, isSearchBarFocused) {
                    WidgetsBinding.instance.addPostFrameCallback((_) {
                      if (state != SearchingState.Searching) {
                        if (selectedPlace != null) {
                          selectedPoint = true;
                          widget.mapsHelper(
                            lat: selectedPlace.geometry!.location.lat,
                            lng: selectedPlace.geometry!.location.lng,
                          );

                          tempFullAddress.text =
                              selectedPlace.formattedAddress!;
                          searcingState.value = false;
                          // printInfo(info: "${selectedPlace.formattedAddress}");
                        }
                      } else {
                        searcingState.value = state == SearchingState.Searching;
                      }
                    });

                    return FloatingCard(
                        topPosition: MediaQuery.of(context).size.height * 0.20,
                        // bottomPosition: MediaQuery.of(context).size.height * 0.05,
                        leftPosition: MediaQuery.of(context).size.width * 0.02,
                        rightPosition: MediaQuery.of(context).size.width * 0.02,
                        width: MediaQuery.of(context).size.width * 0.9,
                        borderRadius: BorderRadius.circular(12.0),
                        child: state == SearchingState.Searching
                            ? const SizedBox()

                            // const Padding(
                            //     padding: EdgeInsets.all(8.0),
                            //     child: Center(child: CircularProgressIndicator()),
                            //   )
                            : const SizedBox()

                        // Container(
                        //     padding: const EdgeInsets.all(8.0),
                        //     width: double.infinity,
                        //     child: Column(
                        //       children: [
                        //         Text(
                        //           selectedPlace!.formattedAddress ?? "",
                        //           style: BaseTextStyle.customTextStyle(
                        //             kfontSize: 12,
                        //             kcolor: AppColor.kPrimaryColor,
                        //             kfontWeight: FontWeight.w400,
                        //           ),
                        //         ),
                        //         // const SizedBox(height: 10),
                        //       ],
                        //     ),
                        //   ),
                        );
                  },
                ),

                // Column(
                //   mainAxisAlignment: MainAxisAlignment.end,
                //   children: [
                //     Container(
                //       height: 200,
                //       padding: const EdgeInsets.all(10),
                //       margin: const EdgeInsets.all(10),
                //       decoration: BoxDecoration(
                //         color: AppColor.kWhiteColor,
                //         borderRadius: BorderRadius.circular(15),
                //       ),
                //       child: SingleChildScrollView(
                //         child: Column(
                //           // crossAxisAlignment: CrossAxisAlignment.start,
                //           mainAxisSize: MainAxisSize.min,
                //           children: [
                //             const SizedBox(height: 10),
                //             Column(
                //               children: [
                //                 Obx(() => CustomTextField.style2(
                //                       title: 'Kota/Kabupaten',
                //                       fieldName: 'kota',
                //                       readOnly: true,
                //                       controller: TextEditingController(
                //                           text: widget.collectMapsData['city_name']),
                //                       onChange: ({value}) {},
                //                       textInputAction: TextInputAction.next,
                //                     )),
                //                 const SizedBox(height: 5),
                //                 Obx(() => CustomTextField.style2(
                //                       title: 'Desa/Kelurahan',
                //                       fieldName: 'desa',
                //                       readOnly: true,
                //                       controller: TextEditingController(
                //                         text: widget.collectMapsData['district_name'],
                //                       ),
                //                       onChange: ({value}) {},
                //                       textInputAction: TextInputAction.next,
                //                     )),
                //                 const SizedBox(height: 5),
                //                 Obx(() => CustomTextField.style2(
                //                       title: 'Kecamatan',
                //                       fieldName: 'kecamatan',
                //                       readOnly: true,
                //                       controller:
                //                           // subDisticControllers,
                //                           TextEditingController(
                //                         text: widget
                //                             .collectMapsData['sub_district_name'],
                //                       ),
                //                       onChange: ({value}) {},
                //                       textInputAction: TextInputAction.next,
                //                     )),
                //                 const SizedBox(height: 5),
                //                 CustomTextField.style2(
                //                   title: 'Alamat Lengkap',
                //                   hintText: "Masukan Alamat",
                //                   fieldName: 'address',
                //                   readOnly: true,
                //                   isLongText: true,
                //                   // controller: detailMapsControllers,
                //                   controller: tempFullAddress,
                //                   onChange: ({value}) {},
                //                   textInputAction: TextInputAction.done,
                //                 ),
                //               ],
                //             ),
                //             const SizedBox(height: 10),
                //             SizedBox(
                //               height: 60.0,
                //               width: double.infinity,
                //               child: ElevatedButton(
                //                 onPressed: () async {
                //                   widget.detailMapsControllers.text =
                //                       tempFullAddress.text;
                //                   Get.back();
                //                 },
                //                 style: ElevatedButton.styleFrom(
                //                   backgroundColor: AppColor.kPrimaryColor,
                //                   shape: RoundedRectangleBorder(
                //                     borderRadius: BorderRadius.circular(10),
                //                   ),
                //                 ),
                //                 child: Text(
                //                   "Pilih Lokasi",
                //                   style: BaseTextStyle.customTextStyle(
                //                     // kfontSize: 12,
                //                     kcolor: AppColor.kWhiteColor,
                //                     kfontWeight: FontWeight.w500,
                //                   ),
                //                 ),
                //               ),
                //             ),
                //           ],
                //         ),
                //       ),
                //     ),
                //   ],
                // ),

                DraggableScrollableSheet(
                  initialChildSize: 0.3,
                  minChildSize: 0.30,
                  maxChildSize: 0.8,
                  controller: sheetController,
                  builder: (BuildContext context, scrollController) {
                    return Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            InkWell(
                              onTap: () {
                                Get.back();
                              },
                              child: Container(
                                padding: const EdgeInsets.all(12),
                                margin: const EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 8),
                                decoration: BoxDecoration(
                                    color: AppColor.kWhiteColor,
                                    borderRadius: BorderRadius.circular(20),
                                    boxShadow: [
                                      BoxShadow(
                                        color:
                                            AppColor.kGreyColor.withOpacity(1),
                                        offset: const Offset(-1, 1),
                                        blurRadius: 16,
                                        spreadRadius: 0,
                                      )
                                    ]),
                                child: const Icon(
                                  Icons.arrow_back,
                                  color: AppColor.kPrimaryColor,
                                ),
                              ),
                            ),
                            // IconButton.filled(
                            //     style: IconButton.styleFrom(
                            //       backgroundColor: AppColor.kWhiteColor,
                            //     ),
                            //     onPressed: () {},
                            //     icon: const Icon(
                            //       Icons.arrow_back,
                            //       color: AppColor.kPrimaryColor,
                            //     )),

                            InkWell(
                              onTap: () {
                                printInfo(info: "${sheetController.size}");
                                if (sheetController.size == 0.3) {
                                  isShowBottomInfo.value = true;
                                } else {
                                  isShowBottomInfo.value = false;
                                }

                                isShowBottomInfo.value == true
                                    ? sheetController.animateTo(
                                        0.8,
                                        duration:
                                            const Duration(milliseconds: 200),
                                        curve: Curves.bounceIn,
                                      )
                                    : sheetController.animateTo(
                                        0.2,
                                        duration:
                                            const Duration(milliseconds: 200),
                                        curve: Curves.bounceIn,
                                      );
                              },
                              child: Obx(() => Container(
                                    padding: const EdgeInsets.all(12),
                                    margin: const EdgeInsets.symmetric(
                                        vertical: 5, horizontal: 8),
                                    decoration: BoxDecoration(
                                        color: AppColor.kWhiteColor,
                                        borderRadius: BorderRadius.circular(20),
                                        boxShadow: [
                                          BoxShadow(
                                            color: AppColor.kGreyColor
                                                .withOpacity(1),
                                            offset: const Offset(-1, 1),
                                            blurRadius: 16,
                                            spreadRadius: 0,
                                          )
                                        ]),
                                    child: Icon(
                                      isShowBottomInfo.value == true
                                          ? Icons.arrow_drop_down_rounded
                                          : Icons.arrow_drop_up_rounded,
                                    ),
                                  )),
                            ),
                            // Obx(
                            //   () => IconButton.filled(
                            //     onPressed: () {
                            //       isShowBottomInfo.value = !isShowBottomInfo.value;

                            //       isShowBottomInfo.value == true
                            //           ? sheetController.animateTo(
                            //               0.8,
                            //               duration: const Duration(milliseconds: 200),
                            //               curve: Curves.bounceIn,
                            //             )
                            //           : sheetController.animateTo(
                            //               0.2,
                            //               duration: const Duration(milliseconds: 200),
                            //               curve: Curves.bounceIn,
                            //             );
                            //     },
                            //     icon: Icon(
                            //       isShowBottomInfo.value == true
                            //           ? Icons.arrow_drop_down_rounded
                            //           : Icons.arrow_drop_up_rounded,
                            //     ),
                            //   ),
                            // )
                          ],
                        ),
                        Expanded(
                          child: Container(
                            clipBehavior: Clip.hardEdge,
                            decoration: BoxDecoration(
                              color: Theme.of(context).canvasColor,
                              borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10),
                              ),
                            ),
                            child: CustomScrollView(
                              controller: scrollController,
                              // shrinkWrap: true,
                              slivers: [
                                SliverToBoxAdapter(
                                  child: Center(
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: Theme.of(context).hintColor,
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(10)),
                                      ),
                                      height: 4,
                                      width: 40,
                                      margin: const EdgeInsets.symmetric(
                                          vertical: 10),
                                    ),
                                  ),
                                ),
                                // const SliverAppBar(
                                //   title: Text('My App'),
                                //   primary: false,
                                //   pinned: true,
                                //   centerTitle: false,
                                // ),
                                SliverList.list(
                                  children: [
                                    // FilledButton.tonal(
                                    //   onPressed: () {
                                    //     // sheetController.animateTo(
                                    //     //   0.8,
                                    //     //   duration: const Duration(milliseconds: 200),
                                    //     //   curve: Curves.bounceIn,
                                    //     // );
                                    //   },
                                    //   child:

                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Obx(
                                        () => searcingState.value == true
                                            ? const Center(
                                                child:
                                                    CircularProgressIndicator(),
                                              )
                                            : Text(
                                                tempFullAddress.text,
                                              ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        // crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          const SizedBox(height: 10),
                                          Column(
                                            children: [
                                              Obx(() => CustomTextField.style2(
                                                    title: 'Kota/Kabupaten',
                                                    fieldName: 'kota',
                                                    readOnly: true,
                                                    controller:
                                                        TextEditingController(
                                                            text: widget
                                                                    .collectMapsData[
                                                                'city_name']),
                                                    onChange: ({value}) {},
                                                    textInputAction:
                                                        TextInputAction.next,
                                                  )),
                                              const SizedBox(height: 5),
                                              Obx(() => CustomTextField.style2(
                                                    title: 'Desa/Kelurahan',
                                                    fieldName: 'desa',
                                                    readOnly: true,
                                                    controller:
                                                        TextEditingController(
                                                      text: widget
                                                              .collectMapsData[
                                                          'district_name'],
                                                    ),
                                                    onChange: ({value}) {},
                                                    textInputAction:
                                                        TextInputAction.next,
                                                  )),
                                              const SizedBox(height: 5),
                                              Obx(() => CustomTextField.style2(
                                                    title: 'Kecamatan',
                                                    fieldName: 'kecamatan',
                                                    readOnly: true,
                                                    controller:
                                                        // subDisticControllers,
                                                        TextEditingController(
                                                      text: widget
                                                              .collectMapsData[
                                                          'sub_district_name'],
                                                    ),
                                                    onChange: ({value}) {},
                                                    textInputAction:
                                                        TextInputAction.next,
                                                  )),
                                              const SizedBox(height: 5),
                                              CustomTextField.style2(
                                                title: 'Alamat Lengkap',
                                                hintText: "Masukan Alamat",
                                                fieldName: 'address',
                                                readOnly: true,
                                                isLongText: true,
                                                // controller: detailMapsControllers,
                                                controller: tempFullAddress,
                                                onChange: ({value}) {},
                                                textInputAction:
                                                    TextInputAction.done,
                                              ),
                                            ],
                                          ),
                                          const SizedBox(height: 10),
                                          // SizedBox(
                                          //   height: 60.0,
                                          //   width: double.infinity,
                                          //   child: ElevatedButton(
                                          //     onPressed: () async {
                                          //       widget.detailMapsControllers.text =
                                          //           tempFullAddress.text;
                                          //       Get.back();
                                          //     },
                                          //     style: ElevatedButton.styleFrom(
                                          //       backgroundColor:
                                          //           AppColor.kPrimaryColor,
                                          //       shape: RoundedRectangleBorder(
                                          //         borderRadius:
                                          //             BorderRadius.circular(10),
                                          //       ),
                                          //     ),
                                          //     child: Text(
                                          //       "Pilih Lokasi",
                                          //       style: BaseTextStyle.customTextStyle(
                                          //         // kfontSize: 12,
                                          //         kcolor: AppColor.kWhiteColor,
                                          //         kfontWeight: FontWeight.w500,
                                          //       ),
                                          //     ),
                                          //   ),
                                          // ),
                                        ],
                                      ),
                                    ),
                                    // ),
                                    // const ListTile(title: Text('Jane Doe')),
                                    // const ListTile(title: Text('Jack Reacher')),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ],
            ),
      bottomNavigationBar: Container(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
        child: SizedBox(
          height: 60.0,
          width: double.infinity,
          child: Button.filled(
            onPressed: () async {
              widget.detailMapsControllers.text = tempFullAddress.text;
              Get.back();
            },
            disabled: isLoading.isTrue,
            // style: ElevatedButton.styleFrom(
            //   backgroundColor: AppColor.kPrimaryColor,
            //   shape: RoundedRectangleBorder(
            //     borderRadius: BorderRadius.circular(10),
            //   ),
            // ),
            label: "Pilih Lokasi",
            // child: Text(
            //   "Pilih Lokasi",
            //   style: BaseTextStyle.customTextStyle(
            //     // kfontSize: 12,
            //     kcolor: AppColor.kWhiteColor,
            //     kfontWeight: FontWeight.w500,
            //   ),
            // ),
          ),
        ),
      ),
    );
  }
}
