import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({
    super.key,
    required this.title,
    // this.leading,
    // this.actions,

    this.height = kToolbarHeight,
    this.extendBack,
    this.autoFocusSearch = false,
    this.onBack,
    this.searchController,
    this.extendIconButton,
    this.onFillterChange,
    this.onSearchChange,
    this.isAllFill = false,
    this.isSearchFill = false,
    this.searchText = "Cari",
  });

  final String title;
  // final Widget? leading;
  // final List<Widget>? actions;
  final double height;
  final bool autoFocusSearch;
  final void Function()? onBack;
  final Function()? extendBack;
  final bool isAllFill;
  final bool isSearchFill;
  final String searchText;
  final IconButton? extendIconButton;
  final TextEditingController? searchController;
  final void Function()? onSearchChange;
  final void Function()? onFillterChange;

  @override
  Widget build(BuildContext context) {
    var isShowSearch = false.obs;
    var isChangedIcon = false.obs;

    final searchTextField = Obx(() {
      return isShowSearch.isTrue
          ? Flexible(
              child: BaseTextFieldWidget.roundedStyleTextField(
                // focusNode: focusNode,
                textEditingController:
                    searchController ?? TextEditingController(),
                onChange: (val) {
                  onSearchChange!();
                  isChangedIcon.value = searchController!.text.isNotEmpty;
                },
                autoFocus: false,
                prefixIcon: onBack == null
                    ? null
                    : IconButton(
                        iconSize: 15,
                        onPressed: onBack,
                        icon: const Icon(
                          Icons.arrow_back_ios_rounded,
                          color: AppColor.kGreyColor,
                        ),
                      ),
                suffixIcon: IconButton(
                  iconSize: 15,
                  onPressed: () {
                    isShowSearch.value = false;
                    FocusManager.instance.primaryFocus?.unfocus();
                    // focusNode!.unfocus();
                  },
                  icon: const Icon(
                    Icons.close,
                    color: AppColor.kGreyColor,
                  ),
                ),
                radius: 8,
                height: 35,
                hintText: searchText,
              ),
            )
          : const SizedBox();
    });

    return PreferredSize(
      preferredSize: Size.fromHeight(height),
      child: SafeArea(
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            // Expanded(child: Text("asdasd")),
            AppBar(
              automaticallyImplyLeading: false,
              elevation: 0,
              titleSpacing: 0,
              toolbarHeight: 75,
              primary: false,
              title: Text(
                title,
                style: BaseTextStyle.customTextStyle(
                  kfontSize: 16,
                  kcolor: AppColor.kWhiteColor,
                  kfontWeight: FontWeight.w700,
                ),
              ),
              leading: IconButton(
                icon: const Icon(
                  Icons.chevron_left,
                  color: AppColor.kWhiteColor,
                  size: 25,
                ),
                onPressed: () {
                  // if (extendBack != null) extendBack();
                  Get.back();
                },
              ),
              backgroundColor: AppColor.kErrorColor,
              actions: [
                if (extendIconButton != null) ...[extendIconButton!],
                if (isAllFill || isSearchFill == true) ...[
                  Obx(() {
                    // if (MediaQuery.of(context).viewInsets.bottom != 0) {
                    //   isShowSearch.value = true;
                    //   // MediaQuery.of(context).viewInsets.bottom != 0;
                    // }

                    // if (MediaQuery.of(context).viewInsets.bottom != 0
                    //     isShowSearch.value == true) {
                    //   isShowSearch.value =
                    //       MediaQuery.of(context).viewInsets.bottom != 0;
                    // }
                    // // else {
                    // //   isShowSearch.value =
                    // //       MediaQuery.of(context).viewInsets.bottom != 0;
                    // // }

                    // if (isShowSearch.value == false) {
                    //   WidgetsBinding.instance.addPostFrameCallback((_) {
                    //     isShowSearch.value = true;
                    //   });
                    // }

                    return Container(
                      height: 80,
                      padding: const EdgeInsets.only(
                          top: 0, bottom: 0, right: 10.0, left: 10.0),
                      // width: 200,
                      width: isShowSearch.isTrue
                          ? MediaQuery.of(context).size.width
                          // ? double.infinity
                          : null,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          searchTextField,
                          SizedBox(width: isShowSearch.isFalse ? 5 : 0),
                          isShowSearch.isFalse
                              ? ConstrainedBox(
                                  constraints: const BoxConstraints.tightFor(
                                      height: 35, width: 35),
                                  child: IconButton.filled(
                                    iconSize: 19,
                                    color: AppColor.kWhiteColor,
                                    onPressed: () {
                                      isShowSearch.value = true;

                                      // focusNode!.requestFocus();
                                      // autoFocusSearch = true;
                                    },
                                    icon: const Icon(Icons.search),
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              : const SizedBox(),
                          SizedBox(width: isSearchFill == false ? 5 : 0),
                          if (isSearchFill == false)
                            ConstrainedBox(
                              constraints: const BoxConstraints.tightFor(
                                  height: 35, width: 35),
                              child: IconButton.filled(
                                iconSize: 19,
                                color: AppColor.kWhiteColor,
                                onPressed: () {
                                  onFillterChange!();
                                },
                                icon: const Icon(Icons.filter_list),
                                style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                        ],
                      ),
                    );
                  }),
                ],
              ],
            ),
          ],
        ),
      ),

      // Container(
      //   margin: edge,
      //   height: height,
      //   color: AppColor.kWhiteColor,
      //   child: Column(
      //     mainAxisSize: MainAxisSize.min,
      //     mainAxisAlignment: MainAxisAlignment.center,
      //     // crossAxisAlignment: CrossAxisAlignment.start,
      //     children: [
      //       Text(
      //         title,
      //         style: BaseTextStyle.customTextStyle(
      //           kfontSize: 16,
      //           kcolor: AppColor.kPrimaryColor,
      //           kfontWeight: FontWeight.w700,
      //         ),
      //       ),
      //       Row(
      //         // crossAxisAlignment: CrossAxisAlignment.center,
      //         children: [
      //           if (leading != null)
      //             // Positioned(
      //             //   left: 0,
      //             //   top: (height - kToolbarHeight) / 2,
      //             //   bottom: (height - kToolbarHeight) / 2,
      //             // child:
      //             // leading!,
      //             IconButton(
      //               icon: const Icon(
      //                 Icons.chevron_left,
      //                 color: AppColor.kPrimaryColor,
      //                 size: 25,
      //               ),
      //               onPressed: () {
      //                 // if (extendBack != null) extendBack();
      //                 Get.back();
      //               },
      //             ),
      //           // ),
      //           // Center(
      //           //   child: title,
      //           // ),
      //           if (actions != null)
      //             // Positioned(
      //             //   right: 0,
      //             //   top: (height - kToolbarHeight) / 2,
      //             //   bottom: (height - kToolbarHeight) / 2,
      //             // child:
      //             Row(
      //               mainAxisSize: MainAxisSize.min,
      //               children: actions!,
      //             ),
      //           // ),
      //         ],
      //       ),
      //       Text(
      //         title,
      //         style: BaseTextStyle.customTextStyle(
      //           kfontSize: 16,
      //           kcolor: AppColor.kPrimaryColor,
      //           kfontWeight: FontWeight.w700,
      //         ),
      //       ),
      //     ],
      //   ),
      // ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height);
}
