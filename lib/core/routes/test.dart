// import 'package:flutter/material.dart';
// import 'package:rbs_mobile_operation/resources/app_main_resources.dart';

// class TestWidget extends StatelessWidget {
//   const TestWidget({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: ShaderMask(
//         shaderCallback: (Rect bounds) {
//           return LinearGradient(
//             colors: [
//               Colors.white,
//               AppColor.kSoftGreyColor.withOpacity(0.1)
//             ],
//             stops: [0.9, 1],
//             tileMode: TileMode.mirror,
//           ).createShader(bounds);
//         },
//         child: Container(
//           height: 100,
//           child: ListView.builder(
//             scrollDirection: Axis.horizontal,
//             itemCount: 10,
//             itemBuilder: (context, index) => Card(
//               color: Colors.red,
//               child: Center(
//                 child: Text("012345678910 - 0123"),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

import 'package:dropdown_search/dropdown_search.dart';
import 'package:rbs_mobile_operation/data/datasources/salesorder.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class TestDropdown extends StatelessWidget {
  const TestDropdown({super.key});

  @override
  Widget build(BuildContext context) {
    SalesListController salesListController = Get.put(SalesListController());

    return Scaffold(
      body: ListView(children: [
        DropdownSearch(
          asyncItems: (String filter) async {
            salesListController.lookupCustomer(searching: filter);
            return salesListController.dataCustomer;
          },
          itemAsString: (u) {
            return u?['name'];
          },
          onChanged: (data) => print(data),
          dropdownBuilder: (context, selectedItem) {
            return Text(
              selectedItem?['name'] ?? "Pilih",
            );
          },
          compareFn: (item1, item2) {
            return true;
          },
          popupProps: const PopupProps.menu(
            isFilterOnline: true,
            showSearchBox: true,
            showSelectedItems: true,
          ),
          dropdownDecoratorProps: DropDownDecoratorProps(
            dropdownSearchDecoration: InputDecoration(
              filled: true,
              fillColor: AppColor.kGreyColor2,
              floatingLabelBehavior: FloatingLabelBehavior.always,
              contentPadding:
                  const EdgeInsets.only(bottom: -10.0, top: 20, left: 10),
              hintText: "Isi disini",
              hintStyle: BaseTextStyle.customTextStyle(
                kcolor: AppColor.kPrimaryColor,
                kfontSize: 14,
                kfontWeight: FontWeight.w500,
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(
                  color: AppColor.kPrimaryColor,
                ), //menunaktifkan borde
                // borderSide: BorderSide.none,
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(
                  color: AppColor.kPrimaryColor,
                ), //menunaktifkan border
              ),
              errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(
                  color: AppColor.kErrorColor,
                ), //menunaktifkan border
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(
                  color: AppColor.kErrorColor,
                ), //menunaktifkan border
              ),
              errorStyle: BaseTextStyle.customTextStyle(
                kfontSize: 12,
                kcolor: AppColor.kFailedColor,
                kfontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
        // BaseButtonWidget.dropdownSearch(
        //   fieldName: 'fieldds',
        //   controller: TextEditingController(),
        //   asyncItems: (String filter) async {
        //     salesListController.lookupCustomer(searching: filter);
        //     return salesListController.dataCustomer;
        //   },
        // ),
        // Row(
        //   children: [
        //     Expanded(
        //       child: DropdownSearch<int>(
        //         items: [1, 2, 3, 4, 5, 6, 7],
        //       ),
        //     ),
        //     Padding(padding: EdgeInsets.all(4)),
        //     Expanded(
        //       child: DropdownSearch<int>.multiSelection(
        //         clearButtonProps: ClearButtonProps(isVisible: true),
        //         items: [1, 2, 3, 4, 5, 6, 7],
        //       ),
        //     )
        //   ],
        // ),
      ]),
    );
  }
}
