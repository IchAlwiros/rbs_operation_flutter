import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/view/app_view_resource.dart';
import 'package:rbs_mobile_operation/core/routes/test.dart';
import 'package:rbs_mobile_operation/view/forgot_password/forgot_password.dart';
import 'package:rbs_mobile_operation/view/kredit&aproval/approval/list.dart';
import 'package:rbs_mobile_operation/view/login.dart';

// * MODULAR CONFIG NAVIGATION
class Navigation {
  //*  INITIALIZATION ROUTE NAME
  static List<Map<String, dynamic>> routeName = [
    {'/': const SplashPage()},
    // {'/login': const Login()},
    // {'/': const SignInView()},
    {'/login': const SignInView()},
    {'/forgot-password': const ForgotPassword()},
    {'/dashboard': const IndexNavigation()},
    {'/nofitscreen': const NotificationScreen()},
    {'/sales-order': const SalesOrderList()},
    // {'/sales-order': const TestDropdown()},
    {'/project-customer': const ProjectCustomerList()},
    {'/data-customer': const CustomerDataList()},
    {'/kredit-approval': const AprovePage()}, //KreditApprovalList()
    {'/product-data': const ProductList()},
    {'/payment-history': const PaymentHistoryList()},
    {'/production-data': const ProductionList()},
    {'/production-schedule': const ProductionScheduleList()},
    {'/incoming-material': const MaterialIncomingList()},
    {'/usage-material': const MaterialUsageList()},
    {'/usage-fuel': const FuelUsageList()},
    {'/stock-opname': const StockTakeList()},
    {'/material-data': const MaterialDataList()},
    {'/stock-analytic': const MaterialStockList()},
    {'/purchase-order': const PurchaseOrder()},
  ];

  //*  INITIALIZATION GET PAGE NAVIGATION
  static List<GetPage> initialAllRoute = routeName.map((route) {
    final String key = route.keys.first;
    final dynamic value = route[key];
    return GetPage(
      name: key,
      page: () => value,
      transition: Transition.cupertino,
      transitionDuration: const Duration(milliseconds: 500),
    );
  }).toList();
}
