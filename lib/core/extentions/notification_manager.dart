import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationManager {
  final FlutterLocalNotificationsPlugin notificationsPlugin =
      FlutterLocalNotificationsPlugin();

  Future<void> initNotification() async {
    AndroidInitializationSettings initializationSettingsAndroid =
        const AndroidInitializationSettings('app_logo');

    DarwinInitializationSettings initializationIos =
        DarwinInitializationSettings(
      requestAlertPermission: true,
      requestBadgePermission: true,
      requestSoundPermission: true,
      onDidReceiveLocalNotification: (id, title, body, payload) {},
    );
    InitializationSettings initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationIos);
    await notificationsPlugin.initialize(
      initializationSettings,
      onDidReceiveNotificationResponse: (details) {},
    );
  }

  // SIMPLE NOTIF STYLE
  Future<void> simpleNotificationShow({
    required String newMessage,
    String? titleMessage,
  }) async {
    AndroidNotificationDetails androidNotificationDetails =
        const AndroidNotificationDetails('Channel_id', 'Channel_title',
            priority: Priority.high,
            importance: Importance.max,
            icon: 'app_logo',
            channelShowBadge: true,
            largeIcon: DrawableResourceAndroidBitmap('app_logo'));

    NotificationDetails notificationDetails =
        NotificationDetails(android: androidNotificationDetails);
    await notificationsPlugin.show(
        0, titleMessage ?? 'New Notification', newMessage, notificationDetails);
  }

  // MULTIPLE NOTIF
  Future<void> multipleNotificationShow() async {
    AndroidNotificationDetails androidNotificationDetails =
        const AndroidNotificationDetails(
      'Channel_id',
      'Channel_title',
      priority: Priority.high,
      importance: Importance.max,
      groupKey: 'commonMessage',
    );

    NotificationDetails notificationDetails =
        NotificationDetails(android: androidNotificationDetails);
    await notificationsPlugin.show(
        0, 'New Notification', 'User 1 send message', notificationDetails);

    Future.delayed(const Duration(milliseconds: 1000), () {
      notificationsPlugin.show(
          1, 'New Notification', 'User 2 send message', notificationDetails);
    });

    Future.delayed(const Duration(milliseconds: 1500), () {
      notificationsPlugin.show(
          2, 'New Notification', 'User 3 send message', notificationDetails);
    });

    List<String> lines = ['user1', 'user2', 'user3'];

    InboxStyleInformation inboxStyleInformation = InboxStyleInformation(
      lines,
      contentTitle: '${lines.length} massages',
      summaryText: "Rbs Bussines Compile",
    );

    AndroidNotificationDetails androidNotificationSpesific =
        AndroidNotificationDetails(
      "groupChannelId",
      "groupChannelTitle",
      styleInformation: inboxStyleInformation,
      groupKey: "commonMessage",
      setAsGroupSummary: true,
    );
    NotificationDetails platformChannelSpe =
        NotificationDetails(android: androidNotificationSpesific);
    await notificationsPlugin.show(
        3, "title", "${lines.length} messages", platformChannelSpe);
  }
}
