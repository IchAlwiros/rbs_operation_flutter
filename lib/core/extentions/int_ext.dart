import 'package:intl/intl.dart';

extension IntegerExt on int {
  String get currencyFormatRp {
    final formatter = NumberFormat.currency(
      locale: 'en_US', // Ubah locale ke en_US
      symbol: 'Rp. ',
      decimalDigits: 2,
    );
    return formatter
        .format(this)
        .replaceAll(',', '#')
        .replaceAll('.', ',')
        .replaceAll('#', '.');
  }
}
