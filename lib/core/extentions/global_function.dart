import 'package:flutter/services.dart';
import 'package:get_storage/get_storage.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:intl/intl.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

// import 'package:rbs_mobile_operation/utils/infrastructure/constant_variable.dart';
// import 'package:intl/date_symbol_data_local.dart';

class BaseGlobalFuntion {
  static final localStorage = GetStorage();

  static ThousandsSeparatorInputFormatter
      get thousandsSeparatorInputFormatter =>
          ThousandsSeparatorInputFormatter();

  static get rupiahSeparatorInputFormatter => RupiahSeparatorInputFormatter();

  static statusColor(dynamic statusCode) {
    dynamic colorBase;

    if (statusCode == null) {
      colorBase = {
        'background_color': AppColor.kGreyColor,
        'text_color': AppColor.kPrimaryColor,
      };
    } else {
      if (statusCode == 'SUCCESS' ||
          statusCode == 'APPROVE' ||
          statusCode == 'PASS') {
        colorBase = {
          'background_color': AppColor.kBadgeSuccessColor,
          'text_color': AppColor.kSuccesColor,
        };
      }
      if (statusCode == 'PROGRESS' || statusCode == 'PROGRESS') {
        colorBase = {
          'background_color': AppColor.kBadgeProfressColor,
          'text_color': AppColor.kProgressColor,
        };
      }
      if (statusCode == 'PENDING' || statusCode == 'PENDING') {
        colorBase = {
          'background_color': AppColor.kBadgeWarningColor,
          'text_color': AppColor.kWarningColor,
        };
      }
      if (statusCode == 'CANCEL' || statusCode == 'REJECT') {
        colorBase = {
          'background_color': AppColor.kBadgeFailedColor,
          'text_color': AppColor.kFailedColor,
        };
      }

      if (statusCode == '1') {
        colorBase = {
          'background_color': AppColor.kBadgeSuccessColor,
          'text_color': AppColor.kSuccesColor,
        };
      }

      if (statusCode == '0') {
        colorBase = {
          'background_color': AppColor.kBadgeFailedColor,
          'text_color': AppColor.kFailedColor,
        };
      }
    }
    return colorBase;
  }

  static statusCheck(dynamic statusCode) {
    if (statusCode == 'Aktif') {
      return 1;
    } else if (statusCode == 'Non Aktif') {
      return 0;
    } else if (statusCode == 'Tidak Aktif') {
      return 0;
    } else if (statusCode == '1') {
      return 'Aktif';
    } else if (statusCode == '0') {
      return 'Tidak Aktif';
    } else if (statusCode == 'Success') {
      return statusCode.toUpperCase();
    } else if (statusCode == 'Cancel') {
      return statusCode.toUpperCase();
    } else if (statusCode == 'Sukses') {
      return 'SUCCESS';
    } else if (statusCode == 'Menunggu') {
      return 'PENDING';
    } else if (statusCode == 'Diproses') {
      return 'PROGRESS';
    } else if (statusCode == 'Dibatalkan') {
      return 'CANCEL';
    } else {
      return '';
    }
  }

  static List<String> paramsInclude(List<Map<String, dynamic>> inputParams) {
    List<Map<String, String>> params = [];

    for (var param in inputParams) {
      if (param.containsKey('key') && param.containsKey('value')) {
        if (param.containsKey('key') &&
            param.containsKey('value') &&
            param['value'] != null &&
            param['value'] != "null" &&
            param['value'].toString().isNotEmpty) {
          params.add({
            'key': param['key'].toString(),
            'value': param['value'].toString(),
          });
        }
      }
    }

    List<String> result =
        params.map((param) => '${param['key']}=${param['value']}').toList();

    return result;
  }

  static List<Map<String, dynamic>> listStringFuntion(
      List<Map<String, dynamic>> inputParams) {
    List<Map<String, dynamic>> params = [];

    for (var param in inputParams) {
      if (param.containsKey('key') &&
          param.containsKey('value') &&
          param['value'] != null &&
          param['value'].toString().isNotEmpty) {
        params.add({
          'string': param['key'].toString(),
          'action': param['value'],
        });
      }
    }

    return params;
  }

  static fillManange(List<dynamic> filterData) {
    return [
      {'id': null, "name": "Semua"},
      ...filterData
    ];
  }

  static Timer? _timer;
  static void debounceRun({
    required VoidCallback action,
    required Duration duration,
  }) {
    bool isActive = _timer?.isActive ?? false;
    if (isActive) {
      _timer?.cancel();
    }
    _timer = Timer(duration, action);
  }

  static datetimeConvert(dynamic initialDate, String format) {
    if (initialDate == null) {
      return "";
    } else {
      if (initialDate.isNotEmpty || format.isNotEmpty) {
        DateTime dateTime = DateTime.parse(initialDate).toLocal();
        final formattedDateTime = DateFormat(format).format(dateTime);
        return formattedDateTime;
      } else {
        return "";
      }
    }
  }

  static formatDateRange(dynamic startDate, dynamic endDate) {
    String stringValue = "";

    if (startDate == null ||
        endDate == null ||
        startDate == "" ||
        endDate == "") {
      return "";
    }

    // PARAMETER CHECK TYPE && NULL VARIABLE
    if (startDate is! DateTime || endDate is! DateTime) {
      startDate = DateTime.parse(startDate);
      endDate = DateTime.parse(endDate);
    }
    final startYear = DateFormat('yyyy').format(startDate);
    final endYear = DateFormat('yyyy').format(endDate);

    final startMonth = DateFormat('MM').format(startDate);
    final endMonth = DateFormat('MM').format(endDate);

    if (startMonth == endMonth && startYear == endYear) {
      return stringValue =
          "${DateFormat('dd').format(startDate)} - ${DateFormat('dd MMM yyyy').format(endDate)}";
    } else if (startYear == endYear) {
      return stringValue =
          "${DateFormat('dd MMM').format(startDate)} - ${DateFormat('dd MMM yyyy').format(endDate)}";
    } else {
      return stringValue =
          "${DateFormat('dd MMM yyyy').format(startDate)} - ${DateFormat('dd MMM yyyy').format(endDate)}";
    }
  }

  static List<Map<String, dynamic>> hasPermission(
      List<Map<String, dynamic>> routeModule) {
    var permissionId = localStorage.read('permissions');

    return routeModule.map((route) {
      var roleId = localStorage.read('profile')['role_id'];

      if (roleId == -1) {
        return {...route, 'show': true};
      } else {
        if (permissionId != null && roleId != -1) {
          if (permissionId.contains("${route['name']}")) {
            return {...route, 'show': true};
          } else {
            return {...route, 'show': false};
          }
        } else {
          return {...route, 'show': true};
        }
      }
    }).toList();
  }

  static List<Widget> setterMenuOther({
    required List list,
    required BuildContext context,
    required dynamic title,
  }) {
    List temporary = [];

    if (list.length > 10) {
      // JIKA MODUL YANG ADA ITU LEBIH DARI 5 MAKA MODUL SELANJUTNYA AKAN DI POTONG DAN DI MASUKAN KE TEMPORARY
      // DAN MODUL KE-5 NYA AKAN DI POTONG DAN DIPINDAHKAN KE TEMPORARY
      temporary.addAll(list.getRange(9, list.length));
      list.removeRange(9, list.length);
    }

    if (temporary.length > 0) {
      // JIKA PADA TEMPORARY NYA ADA ISINYA MAKA AKAN DI BUATKAN MODUL BARU UNTUK MENAMPUNG
      // YAITU MODUL LAINNYA
      list.add(
        {
          'title': 'Menu\nLainnya',
          'name': 'lainnya',
          // 'icon': 'assets/icons/other.svg',
          'icon': Assets.icons.rbsMenulainnyaSvg.svg(width: 30),
          'path': '/lainnya',
          'show': true
        },
      );
    }

    return list.map((item) {
      return GestureDetector(
        //  MEMASUKAN SEMUA ITEM PADA TIAP MODUL MENU LAINNYA
        onTap: () {
          // JIKA PADA ITEM LIST TERSEBUT ADA PATH DENGAN NAMA '/lainnya' MAKA
          // MAKA AKAN MEMUNCULKAN BOTTOM MODAL YANG BERISI MODUL YANG SUDAH Di
          // MASUKAN TEMPORY
          item['path'] == '/lainnya'
              ? showModalBottomSheet(
                  context: context,
                  shape: const RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.vertical(top: Radius.circular(10.0))),
                  backgroundColor: AppColor.kWhiteColor,
                  builder: (context) => Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10, vertical: 15),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          title,
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontSize: 14,
                            kfontWeight: FontWeight.w600,
                          ),
                        ),
                        const SizedBox(height: 12),
                        GridView(
                            physics: const NeverScrollableScrollPhysics(),
                            gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 4,
                              crossAxisSpacing: 10,
                            ),
                            shrinkWrap: true,
                            children: [
                              ...temporary
                                  .map(
                                    (e) => GestureDetector(
                                      onTap: () {
                                        Get.back();
                                        Get.toNamed(e['path']);
                                      },
                                      child: Center(
                                        child: Column(
                                          children: [
                                            Container(
                                              height: 40,
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 5,
                                                      vertical: 1),
                                              decoration: BoxDecoration(
                                                color: AppColor.kGreyColor2,
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                border: Border.all(
                                                    color: AppColor
                                                        .kSoftGreyColor),
                                              ),
                                              child:
                                                  // Assets
                                                  //     .icons.iCONSRBSDataPelanggan
                                                  //     .svg(width: 20)
                                                  e['icon'],
                                              // SvgPicture.asset(
                                              //   e['icon'],
                                              //   width: 20,
                                              // ),
                                            ),
                                            const SizedBox(height: 2),
                                            Text(
                                              e['title'],
                                              textAlign: TextAlign.center,
                                              style:
                                                  BaseTextStyle.customTextStyle(
                                                kfontSize: 10,
                                                kfontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                  .toList(),
                            ]),
                      ],
                    ),
                  ),
                )
              : Get.toNamed(item['path']);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: 40,
              padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 1),
              decoration: BoxDecoration(
                color: AppColor.kGreyColor2,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: AppColor.kSoftGreyColor),
              ),
              child:

                  // Assets.icons.iCONSRBSDataPelanggan.svg(width: 20)
                  item['icon'],
              // SvgPicture.asset(
              //   item['icon'],
              //   width: 20,
              // ),
            ),
            const SizedBox(height: 2),
            Text(
              item['title'],
              textAlign: TextAlign.center,
              style: BaseTextStyle.customTextStyle(
                kfontSize: 10,
                kfontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      );
    }).toList();
  }

  static convertTextLong({
    required String text,
    required int maxLength,
    bool isClamp = false,
  }) {
    if (isClamp) {
      if (text.length > maxLength) {
        return "${text.substring(0, maxLength)}...";
      } else {
        return text;
      }
    } else {
      if (text.length > maxLength) {
        int index = maxLength;
        while (index < text.length) {
          text = text.substring(0, index) + '\n' + text.substring(index);
          index += maxLength + 1; // Menambah satu untuk karakter baris baru
        }
      }
    }

    return text;
  }

// code ini menghasilkan Rp. 1.000.000,00
// ubah agar menjadi

  static currencyLocalConvert(
      {dynamic nominal, bool isLocalCurency = true, int decimalDigits = 2}) {
    if (nominal is num) {
      // MENGECEK YANG MASUK ADALAH ANGKA
      if (nominal >= 0) {
        // MENGECEK TIDAK BOLEH MINUS
        if (isLocalCurency) {
          String formattedCurrency = NumberFormat.currency(
                  locale: 'id', symbol: 'Rp ', decimalDigits: 2)
              .format(nominal);
          return formattedCurrency;
        } else {
          // MENGHILANGKAN SYMBOL DI DEPAN ANGKA
          String formattedCurrency =
              NumberFormat.currency(decimalDigits: decimalDigits)
                  .format(nominal)
                  .replaceAll(RegExp(r'[^\d,.\-]'), '');
          return formattedCurrency;
        }
      } else {
        return '-';
      }
    } else {
      return '-';
    }
  }

  static Future loadingMoment(BuildContext context,
      [bool mounted = true]) async {
    // show the loading dialog
    showDialog<void>(
        barrierDismissible: false,
        context: context,
        builder: (context) => PopScope(
              canPop: false,
              child: AlertDialog(
                insetPadding: const EdgeInsets.all(120),
                contentPadding: const EdgeInsets.all(20),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    // BaseLoadingWidget.circularProgress(
                    //   width: 40,
                    //   height: 40,
                    //   color: AppColor.kPrimaryColor,
                    // ),
                    const SpinKitThreeBounce(
                      size: 30.0,
                      color: AppColor.kPrimaryColor,
                    ),
                    const SizedBox(height: 15),
                    Text(
                      "Loading...",
                      style: BaseTextStyle.customTextStyle(
                        kfontSize: 13,
                        kfontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
            ));

    // Your asynchronous computation here (fetching data from an API, processing files, inserting something to the database, etc)
    await Future.delayed(const Duration(milliseconds: 800));

    // Close the dialog programmatically
    // We use "mounted" variable to get rid of the "Do not use BuildContexts across async gaps" warning
    if (!mounted) return;
    // Get.back();
    Navigator.of(context).pop();
  }
}

class ThousandsSeparatorInputFormatter extends TextInputFormatter {
  static const separator = '.'; // Change this to '.' for other locales
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    // Short-circuit if the new value is empty
    if (newValue.text.isEmpty) {
      return newValue.copyWith(text: '');
    }
    // Handle "deletion" of separator character
    String oldValueText = oldValue.text.replaceAll(separator, '');
    String newValueText = newValue.text.replaceAll(separator, '');
    if (oldValue.text.endsWith(separator) &&
        oldValue.text.length == newValue.text.length + 1) {
      newValueText = newValueText.substring(0, newValueText.length - 1);
    }
    // Only process if the old value and new value are different
    if (oldValueText != newValueText) {
      int selectionIndex =
          newValue.text.length - newValue.selection.extentOffset;
      final chars = newValueText.split('');
      String newString = '';
      for (int i = chars.length - 1; i >= 0; i--) {
        if ((chars.length - 1 - i) % 3 == 0 && i != chars.length - 1)
          newString = separator + newString;
        newString = chars[i] + newString;
      }
      return TextEditingValue(
        text: newString.toString(),
        selection: TextSelection.collapsed(
          offset: newString.length - selectionIndex,
        ),
      );
    }
    // If the new value and old value are the same, just return as-is
    return newValue;
  }
}

// ubah formatter text ini agar menformat angka rupiah
// contoh : 1000 menjadi 1,000.00 lalu 1000000 menjadi 100,000.00 dan seterusnya
class RupiahSeparatorInputFormatter extends TextInputFormatter {
  static const separator = ','; // Change this to '.' for other locales
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    // Short-circuit if the new value is empty
    if (newValue.text.isEmpty) {
      return newValue.copyWith(text: '');
    }
    // Handle "deletion" of separator character
    String oldValueText = oldValue.text.replaceAll(separator, '');
    String newValueText = newValue.text.replaceAll(separator, '');
    if (oldValue.text.endsWith(separator) &&
        oldValue.text.length == newValue.text.length + 1) {
      newValueText = newValueText.substring(0, newValueText.length - 1);
    }
    // Only process if the old value and new value are different
    if (oldValueText != newValueText) {
      int selectionIndex =
          newValue.text.length - newValue.selection.extentOffset;
      final chars = newValueText.split('');
      String newString = '';
      for (int i = chars.length - 1; i >= 0; i--) {
        if ((chars.length - 1 - i) % 3 == 0 && i != chars.length - 1)
          newString = separator + newString;
        newString = chars[i] + newString;
      }
      return TextEditingValue(
        text: newString.toString(),
        selection: TextSelection.collapsed(
          offset: newString.length - selectionIndex,
        ),
      );
    }
    // If the new value and old value are the same, just return as-is
    return newValue;
  }
}

// class ThousandsSeparatorInputFormatter extends TextInputFormatter {
//   static const separator = '.';
//   static const decimalPart = '00';

//   @override
//   TextEditingValue formatEditUpdate(
//       TextEditingValue oldValue, TextEditingValue newValue) {
//     // Short-circuit if the new value is empty
//     if (newValue.text.isEmpty) {
//       return newValue.copyWith(text: '');
//     }

//     // Clean up the input by removing the separator
//     String newText = newValue.text.replaceAll(separator, '');

//     if (newText.length <= 3) {
//       // Add the ".00" part if the text is 3 digits or less
//       newText = newText + separator + decimalPart;
//     } else {
//       // Separate the integer and fractional parts
//       String integerPart = newText.substring(0, newText.length - 2);
//       String fractionalPart = newText.substring(newText.length - 2);

//       // Apply thousand separator formatting for the integer part
//       integerPart =
//           NumberFormat.decimalPattern().format(int.parse(integerPart));

//       newText = integerPart + separator + fractionalPart;
//     }

//     return TextEditingValue(
//       text: newText,
//       selection: TextSelection.collapsed(
//         offset: newText.length,
//       ),
//     );
//   }
// }

// class ThousandsSeparatorInputFormatter extends TextInputFormatter {
//   static const separator = '.';

//   @override
//   TextEditingValue formatEditUpdate(
//       TextEditingValue oldValue, TextEditingValue newValue) {
//     // Short-circuit if the new value is empty
//     if (newValue.text.isEmpty) {
//       return newValue.copyWith(text: '');
//     }

//     // Clean up the input by removing the separator
//     String newText = newValue.text.replaceAll(separator, '');

//     // If the length is 3, insert the separator at the second position
//     if (newText.length == 3) {
//       newText = newText.substring(0, 2) + separator + newText.substring(2);
//     } else if (newText.length > 3) {
//       // Apply thousand separator formatting
//       int selectionIndex = newText.length - newValue.selection.extentOffset;
//       final chars = newText.split('');
//       String newString = '';
//       for (int i = chars.length - 1; i >= 0; i--) {
//         if ((chars.length - 1 - i) % 3 == 0 && i != chars.length - 1) {
//           newString = separator + newString;
//         }
//         newString = chars[i] + newString;
//       }
//       newText = newString;
//     }

//     return TextEditingValue(
//       text: newText,
//       selection: TextSelection.collapsed(
//         offset: newText.length -
//             (newValue.text.length - newValue.selection.extentOffset),
//       ),
//     );
//   }
// }

class UppercaseInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}
