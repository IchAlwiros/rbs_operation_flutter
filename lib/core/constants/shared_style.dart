import 'package:rbs_mobile_operation/core/core.dart';

class AppColor {
  // Colors
  static const Color kPrimaryColor = Color(0xff1F4F94);
  static const Color kInactiveColor = Color(0xffC5DFF8);
  static const Color kSuccesColor = Color(0xff03C988);
  static const Color kGreenColor = Color(0xff04C8C8);
  static const Color kFailedColor = Color(0xffF64E60);
  static const Color kWarningColor = Color.fromARGB(255, 220, 157, 62);
  static const Color kProgressColor = Color.fromARGB(255, 179, 45, 168);
  static const Color kErrorColor = Color(0xffFD4F56);
  static const Color kBadgeProfressColor = Color.fromARGB(255, 239, 201, 247);
  static const Color kBadgeWarningColor = Color.fromARGB(255, 247, 247, 201);
  static const Color kBadgeSuccessColor = Color(0xffC9F7F5);
  static const Color kBadgeFailedColor = Color(0xffFFE2E5);
  static const Color kUnavaliableColor = Color(0xffC5DFF8);
  static const Color kAvailableColor = Color(0xffE0D9FF);
  static Color kShadowColor = const Color(0xff526D82).withOpacity(0.4);

  // Field Color
  static const Color kFieldColorBg = Color(0xffF1F0F5);

  // Pasif Colors
  static const Color kBlackColor = Color(0xff1F1449);
  static const Color kWhiteColor = Color(0xffffffff);
  static const Color kGreyColor = Color(0xffD1D1D1);
  static const Color kGreyColor2 = Color(0xffEFF5F5);
  static const Color kSoftGreyColor = Color(0xffDDE6ED);
  static const Color kSoftBlueColor = Color(0xff82AAE3);
  static const Color kYellowColor = Color(0xffFFC436);
}

class BaseTextStyle {
  // Custom TextStyle
  static TextStyle customTextStyle({
    double kfontSize = 15,
    FontWeight kfontWeight = FontWeight.w500,
    dynamic kcolor,
    double? letterSpacing,
  }) =>
      TextStyle(
        letterSpacing: letterSpacing ?? null,
        fontSize: kfontSize,
        fontWeight: kfontWeight,
        color: kcolor ?? null,
      );
  // GoogleFonts.poppins(
  //   letterSpacing: letterSpacing ?? null,
  //   fontSize: kfontSize,
  //   fontWeight: kfontWeight,
  //   color: kcolor ?? null,
  // );

  static TextStyle hintTextStyle = TextStyle(
    color: const Color(0xff96B6C5).withOpacity(0.5),
    fontSize: 12,
    fontWeight: FontWeight.w700,
  );

  // GoogleFonts.poppins(
  //   color: const Color(0xff96B6C5).withOpacity(0.5),
  //   fontSize: 12,
  //   fontWeight: FontWeight.w700,
  // );
}

class BaseShadowStyle {
  static List<BoxShadow> customBoxshadow = [
    BoxShadow(
      color: const Color(0xff526D82).withOpacity(0.4),
      blurRadius: 2.0,
      spreadRadius: 1.0,
      offset: const Offset(0, 3),
    )
  ];

  static List<BoxShadow> customBoxshadow2 = const [
    BoxShadow(
      color: Color.fromRGBO(50, 50, 93, 0.25),
      offset: Offset(0, 50),
      blurRadius: 100,
      spreadRadius: -20,
    ),
    BoxShadow(
      color: Color.fromRGBO(0, 0, 0, 0.3),
      offset: Offset(0, 30),
      blurRadius: 60,
      spreadRadius: -30,
    ),
    BoxShadow(
      color: Color.fromRGBO(10, 37, 64, 0.35),
      offset: Offset(0, -2),
      blurRadius: 6,
      spreadRadius: 0,
    ),
  ];
}

class BaseTextFieldWidgetStyle {
  static InputDecoration decoratorStyle1({
    Widget? suffixIcon,
    dynamic prefixIcon,
    double radius = 15,
    String? hintText,
    double horizontal = 26.0,
    double vertical = 20.0,
    borderSide,
  }) =>
      InputDecoration(
        suffixIconConstraints: const BoxConstraints(minWidth: 75),
        prefixIcon: prefixIcon,
        suffixIcon: suffixIcon,
        contentPadding:
            EdgeInsets.symmetric(horizontal: horizontal, vertical: vertical),
        fillColor: AppColor.kWhiteColor,
        filled: true,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius),
          borderSide: borderSide ?? BorderSide.none, //menunaktifkan border
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius),
          borderSide: const BorderSide(
            color: AppColor.kPrimaryColor,
          ), //menunaktifkan border
        ),
        hintText: hintText,
        hintStyle: BaseTextStyle.hintTextStyle,
        errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(radius),
            borderSide: BorderSide.none //menunaktifkan border
            ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius),
          borderSide: const BorderSide(
            color: AppColor.kErrorColor,
          ), //menunaktifkan border
        ),
        errorStyle: BaseTextStyle.customTextStyle(
          kcolor: AppColor.kErrorColor,
          kfontSize: 12,
          kfontWeight: FontWeight.w500,
        ),
      );

  static InputDecoration decorationStyle2({
    double rounded = 15,
    String? title,
    bool isLongText = false,
    bool suffixOnError = false,
    required Widget suffixIcon,
  }) {
    return InputDecoration(
      // alignLabelWithHint: expandHeight >= 75,
      alignLabelWithHint: isLongText,
      labelText: title,
      filled: true,
      fillColor: AppColor.kSoftGreyColor,
      contentPadding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
      suffixIconConstraints: const BoxConstraints(minWidth: 60),

      suffixIcon: suffixIcon,
      labelStyle: BaseTextStyle.customTextStyle(
        kfontSize: 14,
        kfontWeight: FontWeight.w500,
        kcolor: suffixOnError ? AppColor.kFailedColor : AppColor.kPrimaryColor,
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(rounded),
        borderSide: BorderSide.none,
      ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(rounded),
        borderSide: const BorderSide(
          color: AppColor.kPrimaryColor,
        ), //menunaktifkan border
      ),
      errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(rounded),
          borderSide: BorderSide.none //menunaktifkan border
          ),
      focusedErrorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(rounded),
        borderSide: const BorderSide(
          color: AppColor.kErrorColor,
        ), //menunaktifkan border
      ),
      errorStyle: BaseTextStyle.customTextStyle(
        kfontSize: 13,
        kcolor: AppColor.kFailedColor,
        kfontWeight: FontWeight.w500,
      ),
    );
  }
}

class BaseButtonStyle {
  static ButtonStyle customElevatedButtonStyle({
    double kradius = 5,
    double kborderThickness = 0.0,
    double? width,
    double? height,
    Color kcolorButton = Colors.blueGrey,
    Color kborderColor = Colors.blueGrey,
  }) =>
      ElevatedButton.styleFrom(
        fixedSize:
            width != null || height != null ? Size(width!, height!) : null,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(kradius),
          side: BorderSide(
            width: kborderThickness,
            color: kborderColor,
          ),
        ),
        backgroundColor: kcolorButton,
      );
}

class BaseFormTextFieldStyle {
  static basicDropdown({
    required String hintText,
    double radius = 10,
  }) =>
      InputDecoration(
        hintText: hintText,
        hintStyle: BaseTextStyle.customTextStyle(
          kfontSize: 15,
          kfontWeight: FontWeight.w500,
          kcolor: AppColor.kGreyColor,
        ),
        errorStyle: BaseTextStyle.customTextStyle(
          kfontSize: 13,
          kcolor: AppColor.kFailedColor,
          kfontWeight: FontWeight.w500,
        ),
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
        suffixIconConstraints: const BoxConstraints(minWidth: 60),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius),
          borderSide: BorderSide.none,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius),
          borderSide: const BorderSide(
            color: AppColor.kPrimaryColor,
          ), //menunaktifkan border
        ),
        errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(radius),
            borderSide: BorderSide.none //menunaktifkan border
            ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius),
          borderSide: BorderSide(
            color: const Color.fromARGB(255, 2, 2, 2),
          ), //menunaktifkan border
        ),
        filled: true,
        fillColor: AppColor.kSoftGreyColor,
      );

  static basicStyle2({
    required String labelText,
    double radius = 10,
    EdgeInsetsGeometry contentPadding =
        const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
  }) =>
      InputDecoration(
        filled: true,
        fillColor: AppColor.kSoftGreyColor,
        contentPadding: contentPadding,
        labelText: labelText,
        labelStyle: BaseTextStyle.customTextStyle(
          kcolor: AppColor.kPrimaryColor,
          kfontSize: 13,
          kfontWeight: FontWeight.w600,
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(radius),
            topRight: Radius.circular(radius),
          ),
          borderSide: BorderSide.none,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(radius),
            topRight: Radius.circular(radius),
          ),
          borderSide: BorderSide(
            color: AppColor.kPrimaryColor,
          ), //menunaktifkan border
        ),
        errorStyle: BaseTextStyle.customTextStyle(
          kfontSize: 13,
          kcolor: AppColor.kFailedColor,
          kfontWeight: FontWeight.w500,
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(radius),
            topRight: Radius.circular(radius),
          ),
          borderSide: BorderSide(
            color: AppColor.kErrorColor,
          ), //menunaktifkan border
        ),
      );

  static basicDateDecoration({
    required String labelText,
    double radius = 10,
    Widget? suffixIcon,
  }) =>
      InputDecoration(
        filled: true,
        fillColor: AppColor.kGreyColor.withOpacity(0.2),
        suffixIcon: suffixIcon,
        contentPadding: const EdgeInsets.only(bottom: -10.0, top: 20, left: 10),
        hintText: labelText,
        hintStyle: BaseTextStyle.customTextStyle(
          kcolor: AppColor.kPrimaryColor,
          kfontSize: 13,
          kfontWeight: FontWeight.w600,
        ),
        labelStyle: BaseTextStyle.customTextStyle(
          kcolor: AppColor.kPrimaryColor,
          kfontSize: 13,
          kfontWeight: FontWeight.w600,
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius),
          borderSide: BorderSide.none,
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius),
          borderSide: const BorderSide(
            color: AppColor.kPrimaryColor,
          ), //menunaktifkan border
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius),
          borderSide: const BorderSide(
            color: AppColor.kErrorColor,
          ), //menunaktifkan border
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(radius),
          borderSide: const BorderSide(
            color: AppColor.kErrorColor,
          ), //menunaktifkan border
        ),
        errorStyle: BaseTextStyle.customTextStyle(
          kfontSize: 12,
          kcolor: AppColor.kFailedColor,
          kfontWeight: FontWeight.w500,
        ),
      );
}
