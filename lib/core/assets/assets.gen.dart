/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';

class $AssetsIconsGen {
  const $AssetsIconsGen();

  /// File path: assets/icons/app_logo.png
  AssetGenImage get appLogo => const AssetGenImage('assets/icons/app_logo.png');

  /// File path: assets/icons/app_logo2.png
  AssetGenImage get appLogo2 =>
      const AssetGenImage('assets/icons/app_logo2.png');

  /// File path: assets/icons/catatan.svg
  SvgGenImage get catatan => const SvgGenImage('assets/icons/catatan.svg');

  /// File path: assets/icons/concrete.png
  AssetGenImage get concrete =>
      const AssetGenImage('assets/icons/concrete.png');

  /// File path: assets/icons/google_auth.png
  AssetGenImage get googleAuth =>
      const AssetGenImage('assets/icons/google_auth.png');

  /// File path: assets/icons/handsome-businessman-checking-emails-phone-removebg-preview.png
  AssetGenImage get handsomeBusinessmanCheckingEmailsPhoneRemovebgPreview =>
      const AssetGenImage(
          'assets/icons/handsome-businessman-checking-emails-phone-removebg-preview.png');

  /// File path: assets/icons/icon_logo.png
  AssetGenImage get iconLogoPng =>
      const AssetGenImage('assets/icons/icon_logo.png');

  /// File path: assets/icons/icon_logo.svg
  SvgGenImage get iconLogoSvg =>
      const SvgGenImage('assets/icons/icon_logo.svg');

  /// File path: assets/icons/industry.png
  AssetGenImage get industry =>
      const AssetGenImage('assets/icons/industry.png');

  /// File path: assets/icons/logo.png
  AssetGenImage get logo => const AssetGenImage('assets/icons/logo.png');

  /// File path: assets/icons/logo_icons_blue.png
  AssetGenImage get logoIconsBlue =>
      const AssetGenImage('assets/icons/logo_icons_blue.png');

  /// File path: assets/icons/medal.svg
  SvgGenImage get medal => const SvgGenImage('assets/icons/medal.svg');

  /// File path: assets/icons/message.svg
  SvgGenImage get message => const SvgGenImage('assets/icons/message.svg');

  /// File path: assets/icons/other.svg
  SvgGenImage get other => const SvgGenImage('assets/icons/other.svg');

  /// File path: assets/icons/person.svg
  SvgGenImage get person => const SvgGenImage('assets/icons/person.svg');

  /// File path: assets/icons/rbs_approvalkredit.png
  AssetGenImage get rbsApprovalkreditPng =>
      const AssetGenImage('assets/icons/rbs_approvalkredit.png');

  /// File path: assets/icons/rbs_approvalkredit.svg
  SvgGenImage get rbsApprovalkreditSvg =>
      const SvgGenImage('assets/icons/rbs_approvalkredit.svg');

  /// File path: assets/icons/rbs_datapelanggan.png
  AssetGenImage get rbsDatapelangganPng =>
      const AssetGenImage('assets/icons/rbs_datapelanggan.png');

  /// File path: assets/icons/rbs_datapelanggan.svg
  SvgGenImage get rbsDatapelangganSvg =>
      const SvgGenImage('assets/icons/rbs_datapelanggan.svg');

  /// File path: assets/icons/rbs_dataproyek.png
  AssetGenImage get rbsDataproyekPng =>
      const AssetGenImage('assets/icons/rbs_dataproyek.png');

  /// File path: assets/icons/rbs_dataproyek.svg
  SvgGenImage get rbsDataproyekSvg =>
      const SvgGenImage('assets/icons/rbs_dataproyek.svg');

  /// File path: assets/icons/rbs_fuelstock.png
  AssetGenImage get rbsFuelstockPng =>
      const AssetGenImage('assets/icons/rbs_fuelstock.png');

  /// File path: assets/icons/rbs_fuelstock.svg
  SvgGenImage get rbsFuelstockSvg =>
      const SvgGenImage('assets/icons/rbs_fuelstock.svg');

  /// File path: assets/icons/rbs_materialmasuk.png
  AssetGenImage get rbsMaterialmasukPng =>
      const AssetGenImage('assets/icons/rbs_materialmasuk.png');

  /// File path: assets/icons/rbs_materialmasuk.svg
  SvgGenImage get rbsMaterialmasukSvg =>
      const SvgGenImage('assets/icons/rbs_materialmasuk.svg');

  /// File path: assets/icons/rbs_materialstock.png
  AssetGenImage get rbsMaterialstockPng =>
      const AssetGenImage('assets/icons/rbs_materialstock.png');

  /// File path: assets/icons/rbs_materialstock.svg
  SvgGenImage get rbsMaterialstockSvg =>
      const SvgGenImage('assets/icons/rbs_materialstock.svg');

  /// File path: assets/icons/rbs_menulainnya.png
  AssetGenImage get rbsMenulainnyaPng =>
      const AssetGenImage('assets/icons/rbs_menulainnya.png');

  /// File path: assets/icons/rbs_menulainnya.svg
  SvgGenImage get rbsMenulainnyaSvg =>
      const SvgGenImage('assets/icons/rbs_menulainnya.svg');

  /// File path: assets/icons/rbs_penggunaanmaterial.png
  AssetGenImage get rbsPenggunaanmaterialPng =>
      const AssetGenImage('assets/icons/rbs_penggunaanmaterial.png');

  /// File path: assets/icons/rbs_penggunaanmaterial.svg
  SvgGenImage get rbsPenggunaanmaterialSvg =>
      const SvgGenImage('assets/icons/rbs_penggunaanmaterial.svg');

  /// File path: assets/icons/rbs_purchaseorder.png
  AssetGenImage get rbsPurchaseorderPng =>
      const AssetGenImage('assets/icons/rbs_purchaseorder.png');

  /// File path: assets/icons/rbs_purchaseorder.svg
  SvgGenImage get rbsPurchaseorderSvg =>
      const SvgGenImage('assets/icons/rbs_purchaseorder.svg');

  /// File path: assets/icons/rbs_realisasiproduksi.png
  AssetGenImage get rbsRealisasiproduksiPng =>
      const AssetGenImage('assets/icons/rbs_realisasiproduksi.png');

  /// File path: assets/icons/rbs_realisasiproduksi.svg
  SvgGenImage get rbsRealisasiproduksiSvg =>
      const SvgGenImage('assets/icons/rbs_realisasiproduksi.svg');

  /// File path: assets/icons/rbs_rencanaproduksi.png
  AssetGenImage get rbsRencanaproduksiPng =>
      const AssetGenImage('assets/icons/rbs_rencanaproduksi.png');

  /// File path: assets/icons/rbs_rencanaproduksi.svg
  SvgGenImage get rbsRencanaproduksiSvg =>
      const SvgGenImage('assets/icons/rbs_rencanaproduksi.svg');

  /// File path: assets/icons/rbs_salesorder.png
  AssetGenImage get rbsSalesorderPng =>
      const AssetGenImage('assets/icons/rbs_salesorder.png');

  /// File path: assets/icons/rbs_salesorder.svg
  SvgGenImage get rbsSalesorderSvg =>
      const SvgGenImage('assets/icons/rbs_salesorder.svg');

  /// File path: assets/icons/rbs_stocktake.png
  AssetGenImage get rbsStocktakePng =>
      const AssetGenImage('assets/icons/rbs_stocktake.png');

  /// File path: assets/icons/rbs_stocktake.svg
  SvgGenImage get rbsStocktakeSvg =>
      const SvgGenImage('assets/icons/rbs_stocktake.svg');

  /// File path: assets/icons/shape.svg
  SvgGenImage get shape => const SvgGenImage('assets/icons/shape.svg');

  /// File path: assets/icons/task.svg
  SvgGenImage get task => const SvgGenImage('assets/icons/task.svg');

  /// File path: assets/icons/tm_icons.svg
  SvgGenImage get tmIcons => const SvgGenImage('assets/icons/tm_icons.svg');

  /// List of all assets
  List<dynamic> get values => [
        appLogo,
        appLogo2,
        catatan,
        concrete,
        googleAuth,
        handsomeBusinessmanCheckingEmailsPhoneRemovebgPreview,
        iconLogoPng,
        iconLogoSvg,
        industry,
        logo,
        logoIconsBlue,
        medal,
        message,
        other,
        person,
        rbsApprovalkreditPng,
        rbsApprovalkreditSvg,
        rbsDatapelangganPng,
        rbsDatapelangganSvg,
        rbsDataproyekPng,
        rbsDataproyekSvg,
        rbsFuelstockPng,
        rbsFuelstockSvg,
        rbsMaterialmasukPng,
        rbsMaterialmasukSvg,
        rbsMaterialstockPng,
        rbsMaterialstockSvg,
        rbsMenulainnyaPng,
        rbsMenulainnyaSvg,
        rbsPenggunaanmaterialPng,
        rbsPenggunaanmaterialSvg,
        rbsPurchaseorderPng,
        rbsPurchaseorderSvg,
        rbsRealisasiproduksiPng,
        rbsRealisasiproduksiSvg,
        rbsRencanaproduksiPng,
        rbsRencanaproduksiSvg,
        rbsSalesorderPng,
        rbsSalesorderSvg,
        rbsStocktakePng,
        rbsStocktakeSvg,
        shape,
        task,
        tmIcons
      ];
}

class $AssetsImagesGen {
  const $AssetsImagesGen();

  /// File path: assets/images/b.json
  LottieGenImage get b => const LottieGenImage('assets/images/b.json');

  /// File path: assets/images/change-pwd.svg
  SvgGenImage get changePwd =>
      const SvgGenImage('assets/images/change-pwd.svg');

  /// File path: assets/images/dashboard.png
  AssetGenImage get dashboardPng =>
      const AssetGenImage('assets/images/dashboard.png');

  /// File path: assets/images/dashboard.svg
  SvgGenImage get dashboardSvg =>
      const SvgGenImage('assets/images/dashboard.svg');

  /// File path: assets/images/forgot-pwd.svg
  SvgGenImage get forgotPwd =>
      const SvgGenImage('assets/images/forgot-pwd.svg');

  /// File path: assets/images/gradient-blue-bottom.png
  AssetGenImage get gradientBlueBottom =>
      const AssetGenImage('assets/images/gradient-blue-bottom.png');

  /// File path: assets/images/gradient-blue-top.png
  AssetGenImage get gradientBlueTop =>
      const AssetGenImage('assets/images/gradient-blue-top.png');

  /// File path: assets/images/img-nodata.svg
  SvgGenImage get imgNodata =>
      const SvgGenImage('assets/images/img-nodata.svg');

  /// File path: assets/images/loginBg.png
  AssetGenImage get loginBg => const AssetGenImage('assets/images/loginBg.png');

  /// File path: assets/images/nodata.json
  LottieGenImage get nodata =>
      const LottieGenImage('assets/images/nodata.json');

  /// List of all assets
  List<dynamic> get values => [
        b,
        changePwd,
        dashboardPng,
        dashboardSvg,
        forgotPwd,
        gradientBlueBottom,
        gradientBlueTop,
        imgNodata,
        loginBg,
        nodata
      ];
}

class Assets {
  Assets._();

  static const $AssetsIconsGen icons = $AssetsIconsGen();
  static const $AssetsImagesGen images = $AssetsImagesGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

class SvgGenImage {
  const SvgGenImage(this._assetName);

  final String _assetName;

  SvgPicture svg({
    Key? key,
    bool matchTextDirection = false,
    AssetBundle? bundle,
    String? package,
    double? width,
    double? height,
    BoxFit fit = BoxFit.contain,
    AlignmentGeometry alignment = Alignment.center,
    bool allowDrawingOutsideViewBox = false,
    WidgetBuilder? placeholderBuilder,
    String? semanticsLabel,
    bool excludeFromSemantics = false,
    SvgTheme theme = const SvgTheme(),
    ColorFilter? colorFilter,
    Clip clipBehavior = Clip.hardEdge,
    @deprecated Color? color,
    @deprecated BlendMode colorBlendMode = BlendMode.srcIn,
    @deprecated bool cacheColorFilter = false,
  }) {
    return SvgPicture.asset(
      _assetName,
      key: key,
      matchTextDirection: matchTextDirection,
      bundle: bundle,
      package: package,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      allowDrawingOutsideViewBox: allowDrawingOutsideViewBox,
      placeholderBuilder: placeholderBuilder,
      semanticsLabel: semanticsLabel,
      excludeFromSemantics: excludeFromSemantics,
      theme: theme,
      colorFilter: colorFilter,
      color: color,
      colorBlendMode: colorBlendMode,
      clipBehavior: clipBehavior,
      cacheColorFilter: cacheColorFilter,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

class LottieGenImage {
  const LottieGenImage(this._assetName);

  final String _assetName;

  LottieBuilder lottie({
    Animation<double>? controller,
    bool? animate,
    FrameRate? frameRate,
    bool? repeat,
    bool? reverse,
    LottieDelegates? delegates,
    LottieOptions? options,
    void Function(LottieComposition)? onLoaded,
    LottieImageProviderFactory? imageProviderFactory,
    Key? key,
    AssetBundle? bundle,
    Widget Function(BuildContext, Widget, LottieComposition?)? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    double? width,
    double? height,
    BoxFit? fit,
    AlignmentGeometry? alignment,
    String? package,
    bool? addRepaintBoundary,
    FilterQuality? filterQuality,
    void Function(String)? onWarning,
  }) {
    return Lottie.asset(
      _assetName,
      controller: controller,
      animate: animate,
      frameRate: frameRate,
      repeat: repeat,
      reverse: reverse,
      delegates: delegates,
      options: options,
      onLoaded: onLoaded,
      imageProviderFactory: imageProviderFactory,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      width: width,
      height: height,
      fit: fit,
      alignment: alignment,
      package: package,
      addRepaintBoundary: addRepaintBoundary,
      filterQuality: filterQuality,
      onWarning: onWarning,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}
