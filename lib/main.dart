import 'package:firebase_core/firebase_core.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:rbs_mobile_operation/firebase_options.dart';
import 'package:rbs_mobile_operation/data/datasources/firebasenotification.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

final navigatorKey = GlobalKey<NavigatorState>();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  // await FirebaseApi().initNotifications();
  await FirebaseMessangingRemoteDatasource().initialize();
  await GetStorage.init();
  // if (Platform.isAndroid) {
  //   AndroidGoogleMapsFlutter.useAndroidViewSurface = true;
  // }

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: AppColor.kPrimaryColor,
      // statusBarBrightness: Brightness.dark,
      systemNavigationBarColor: AppColor.kWhiteColor,
    ));

    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    //   statusBarColor: Colors.transparent, // Transparent status bar
    //   statusBarBrightness: Brightness.dark, // Dark text for status bar
    // ));

    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: ScreenUtilInit(
        minTextAdapt: true,
        designSize: const Size(375, 812),
        useInheritedMediaQuery: true,

        // rebuildFactor: RebuildFactors.sizeAndViewInsets(old, data),
        child: GetMaterialApp(
          navigatorKey: navigatorKey,
          title: 'RBS Mobile Operation',
          getPages: Navigation.initialAllRoute,
          initialRoute: '/',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            colorScheme:
                ColorScheme.fromSeed(seedColor: AppColor.kPrimaryColor),
            useMaterial3: true,
            textTheme: GoogleFonts.outfitTextTheme(
              Theme.of(context).textTheme,
            ),
            appBarTheme: AppBarTheme(
              // iconTheme: IconThemeData(color: Colors.black),
              elevation: 0,
              color: AppColor.kPrimaryColor,
              foregroundColor: Colors.black,
              titleTextStyle: GoogleFonts.kumbhSans(
                color: AppColor.kWhiteColor,
                fontSize: 24.0,
                fontWeight: FontWeight.bold,
              ),
              systemOverlayStyle: const SystemUiOverlayStyle(
                //<-- SEE HERE
                // Status bar color
                statusBarColor: AppColor.kPrimaryColor,
                // systemNavigationBarColor: AppColor.kPrimaryColor,
                statusBarIconBrightness: Brightness.dark,
                statusBarBrightness: Brightness.light,
                // systemNavigationBarColor: AppColor.kWhiteColor,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
