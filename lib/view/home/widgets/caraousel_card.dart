import 'package:carousel_slider/carousel_slider.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/data/datasources/home.controllers.dart';

class CarouselCardBanner extends StatelessWidget {
  final List<dynamic> imageBanner;

  const CarouselCardBanner({required this.imageBanner, super.key});

  @override
  Widget build(BuildContext context) {
    var currentCard = 0.obs;
    final CarouselController controller = CarouselController();

// print(imageBanner[''])
    return Obx(() => Column(
          children: [
            if (imageBanner.isEmpty) ...[
              // Shimmer.fromColors(
              //     baseColor: AppColor.kGreyColor,
              //     highlightColor: AppColor.kWhiteColor,
              //     child: Container(
              //       height: 120,
              //       width: 320,
              //       decoration: BoxDecoration(
              //           color: AppColor.kGreyColor,
              //           borderRadius: BorderRadius.circular(8)),
              //     )),
              // Container(
              //   height: 120,
              //   width: double.infinity,
              //   margin: const EdgeInsets.symmetric(horizontal: 8),
              //   decoration: const BoxDecoration(
              //     color: AppColor.kPrimaryColor,
              //   ),
              // )
            ] else if (imageBanner.length < 2) ...[
              Container(
                height: 140,
                width: double.infinity,
                margin: const EdgeInsets.symmetric(horizontal: 10),
                decoration: BoxDecoration(
                    color: AppColor.kPrimaryColor,
                    borderRadius: BorderRadius.circular(5)),
                child: Image.network(
                  imageBanner[0]['banner'],
                  height: 80,
                  fit: BoxFit.cover,
                  loadingBuilder: (_, child, chunk) => chunk != null
                      ? Shimmer.fromColors(
                          baseColor: AppColor.kGreyColor,
                          highlightColor: AppColor.kWhiteColor,
                          child: Container(
                            height: 120,
                            // width: 200,
                            decoration: BoxDecoration(
                                color: AppColor.kGreyColor,
                                borderRadius: BorderRadius.circular(8)),
                          ))
                      : child,
                ),
              )
            ] else ...[
              CarouselSlider(
                carouselController: controller,
                items: List.generate(
                    imageBanner.length,
                    (index) => Container(
                          height: 200,
                          decoration: BoxDecoration(
                              color: AppColor.kPrimaryColor,
                              borderRadius: BorderRadius.circular(5)),
                          child: Image.network(
                            imageBanner[index]['banner'],
                            height: 80,
                            fit: BoxFit.cover,
                            loadingBuilder: (_, child, chunk) => chunk != null
                                ? Shimmer.fromColors(
                                    baseColor: AppColor.kGreyColor,
                                    highlightColor: AppColor.kWhiteColor,
                                    child: Container(
                                      height: 120,
                                      // width: 200,
                                      decoration: BoxDecoration(
                                          color: AppColor.kGreyColor,
                                          borderRadius:
                                              BorderRadius.circular(8)),
                                    ))
                                : child,
                          ),
                        )),
                options: CarouselOptions(
                    // height: 250.0,

                    // pageSnapping: true,
                    // viewportFraction: 0.5,
                    autoPlay: true,
                    aspectRatio: 2.8,
                    // enlargeCenterPage: true,
                    viewportFraction: 0.9,
                    onPageChanged: (index, reason) {
                      currentCard.value = index;
                    }),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(
                    imageBanner.length,
                    (index) => cardCarousel(
                          title: "",
                          img: imageBanner[index]['banner'],
                          subtitle: "",
                        )).asMap().entries.map((entry) {
                  return GestureDetector(
                    onTap: () => controller.animateToPage(entry.key),
                    child: Container(
                      width: 10.0,
                      height: 10.0,
                      margin: const EdgeInsets.symmetric(
                          vertical: 9.0, horizontal: 4.0),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: (Theme.of(context).brightness ==
                                      Brightness.dark
                                  ? Colors.white
                                  : AppColor.kPrimaryColor)
                              .withOpacity(
                                  currentCard.value == entry.key ? 0.9 : 0.4)),
                    ),
                  );
                }).toList(),
              ),
            ]
          ],
        ));
  }

  Widget cardCarousel({
    required String title,
    // required int index,
    String subtitle = "",
    required String img,
  }) {
    return Container(
      margin: const EdgeInsets.only(right: 20),
      child: SizedBox(
        height: 120,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Container(
              // height: 130,
              width: double.infinity,
              padding: const EdgeInsets.all(0.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
              ),
              child: Container(
                decoration: BoxDecoration(
                  // boxShadow: BaseShadowStyle.customBoxshadow,
                  gradient: const RadialGradient(
                    radius: 1.5,
                    center: Alignment(-0.9, 1.5),
                    colors: [
                      AppColor.kSoftBlueColor,
                      AppColor.kPrimaryColor,
                    ],
                    stops: [
                      0.10,
                      1,
                    ],
                    tileMode: TileMode.mirror,
                  ),
                  // boxShadow: BaseShadowStyle.customBoxshadow,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        BaseTextWidget.customText(
                          text: title,
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: AppColor.kWhiteColor,
                        ),
                        BaseTextWidget.customText(
                          text: subtitle,
                          fontSize: 12,
                          fontWeight: FontWeight.normal,
                          color: AppColor.kWhiteColor,
                        ),
                        // BaseTextWidget.customText(text: title),
                        // Text.rich(
                        //   TextSpan(
                        //     children: [
                        //       TextSpan(
                        //         text: "$title\n",
                        //         style: BaseTextStyle.customTextStyle(
                        //           kcolor: AppColor.kWhiteColor,
                        //           letterSpacing: 0.8,
                        //           kfontSize: 20,
                        //           kfontWeight: FontWeight.w700,
                        //         ),
                        //       ),
                        //       // TextSpan(
                        //       //   text: "Readymix Lebih Mudah",
                        //       //   style: BaseTextStyle.customTextStyle(
                        //       //     kfontSize: 12,
                        //       //     kcolor: AppColor.kWhiteColor,
                        //       //     kfontWeight: FontWeight.w400,
                        //       //     letterSpacing: 1,
                        //       //   ),
                        //       // ),
                        //     ],
                        //   ),
                        // ),
                        const SizedBox(
                          height: 10,
                        ),
                      ]),
                ),
              ),
            ),
            // Positioned(
            //   right: 0,
            //   bottom: 10,
            //   child: img.isNotEmpty
            //       ? Image.network(
            //           img,
            //           height: 80,
            //           fit: BoxFit.cover,
            //         )
            //       : Assets.images.dashboardPng.image(
            //           height: 80,
            //           fit: BoxFit.cover,
            //         ),
            // )
          ],
        ),
      ),
    );
  }
}
