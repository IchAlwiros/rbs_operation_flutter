import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:rbs_mobile_operation/data/datasources/home.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';
import 'package:rbs_mobile_operation/view/home/app_pages_home.dart';
import 'package:rbs_mobile_operation/view/home/widgets/caraousel_card.dart';

class HomePage extends StatefulWidget {
  final BuildContext contextIndex;

  const HomePage({Key? key, required this.contextIndex}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _current = 0;
  final CarouselController _controller = CarouselController();

  @override
  Widget build(BuildContext context) {
    // print(BaseGlobalVariable.getLocalVariable(key: 'corporate'));
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _yourListView(),
        ],
      ),
    );
  }

  Future onRefresh() async {
    homeControllers.refreshDataPU();
  }

  Widget _yourListView() {
    return RefreshIndicator(
      onRefresh: onRefresh,
      child: ListView(
        // controller: _controller,
        children: [
          Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height / 4.5,
                decoration: const BoxDecoration(
                  color: AppColor.kPrimaryColor,
                  gradient: RadialGradient(
                    radius: 1.2,
                    center: Alignment(0.9, 1.5),
                    colors: [
                      AppColor.kSoftBlueColor,
                      AppColor.kPrimaryColor,
                    ],
                    tileMode: TileMode.mirror,
                  ),
                ),
                child: Stack(
                  children: [
                    Positioned(
                        right: -50,
                        top: -25,
                        bottom: 0,
                        child: Container(
                          width: 200,
                          height: 200,
                          padding: const EdgeInsets.all(20),
                          // color: AppColor.kPrimaryColor,
                          child: ImageFiltered(
                            imageFilter: ImageFilter.blur(sigmaX: 6, sigmaY: 6),
                            child: Assets.icons.iconLogoSvg.svg(),
                          ),
                        )),
                  ],
                ),
              ),
              Container(
                // height: 80,
                width: double.infinity,
                margin: EdgeInsets.only(
                  top: MediaQuery.sizeOf(context).height / 7,
                  left: 10,
                  right: 10,
                ),
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                decoration: BoxDecoration(
                  color: AppColor.kWhiteColor,
                  border: Border.all(color: AppColor.kGreyColor),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        const Icon(
                          Icons.corporate_fare_rounded,
                          color: AppColor.kGreyColor,
                        ),
                        const SizedBox(width: 5),
                        BaseTextWidget.customText(
                          isLongText: true,
                          text: LocalDataSource.getLocalVariable(
                              key: 'corporate')['name'],
                          fontWeight: FontWeight.bold,
                        ),
                      ],
                    ),
                    const Divider(thickness: 0.4),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Obx(() => Expanded(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                    fixedSize: const Size(double.infinity, 10),
                                    side: const BorderSide(
                                      width: 1.0,
                                      color: AppColor.kPrimaryColor,
                                    )),
                                onPressed: () {
                                  showBottomFilter(widget.contextIndex);
                                },
                                child: BaseTextWidget.customText(
                                  text: productionUnitData['name'] == null
                                      ? LocalDataSource.getLocalVariable(
                                                key: 'lastestProduction',
                                              ) ==
                                              null
                                          ? "Pilih Unit Produksi"
                                          : "${LocalDataSource.getLocalVariable(
                                              key: 'lastestProduction',
                                            )['name']}"
                                      : productionUnitData["name"] == 'Semua'
                                          ? "Semua Unit Produksi"
                                          : "${productionUnitData["name"]}",
                                  color: AppColor.kPrimaryColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            )),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: AppColor.kInactiveColor,
                          borderRadius: BorderRadius.circular(10)),
                      child: Row(
                        children: [
                          Text.rich(
                            TextSpan(
                              children: [
                                TextSpan(
                                    text: 'Hi, ',
                                    style: BaseTextStyle.customTextStyle(
                                      kfontSize: 13,
                                      kcolor: AppColor.kPrimaryColor,
                                      kfontWeight: FontWeight.w500,
                                    )),
                                TextSpan(
                                    text:
                                        '${LocalDataSource.getLocalVariable(key: 'profile')['username']}',
                                    style: BaseTextStyle.customTextStyle(
                                      kfontSize: 13,
                                      kcolor: AppColor.kPrimaryColor,
                                      kfontWeight: FontWeight.w700,
                                    )),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Column(
            children: [
              const SizedBox(height: 15),
              // infoBanner(),
              CarouselCardBanner(
                imageBanner: homeControllers.dataGetBanner,
              ),
              IndexSales(contextIndex: widget.contextIndex),
              // IndexLogistic(contextIndex: widget.contextIndex),
              secondInfoBanner(),
            ],
          )
        ],
      ),
    );
  }

  Widget cardCarousel({
    required String title,
    // required int index,
    String subtitle = "",
    required String img,
  }) {
    return Container(
      margin: const EdgeInsets.only(right: 20),
      child: SizedBox(
        height: 120,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Container(
              // height: 130,
              width: double.infinity,
              padding: const EdgeInsets.all(0.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
              ),
              child: Container(
                decoration: BoxDecoration(
                  // boxShadow: BaseShadowStyle.customBoxshadow,
                  gradient: const RadialGradient(
                    radius: 1.5,
                    center: Alignment(-0.9, 1.5),
                    colors: [
                      AppColor.kSoftBlueColor,
                      AppColor.kPrimaryColor,
                    ],
                    stops: [
                      0.10,
                      1,
                    ],
                    tileMode: TileMode.mirror,
                  ),
                  // boxShadow: BaseShadowStyle.customBoxshadow,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        BaseTextWidget.customText(
                          text: title,
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: AppColor.kWhiteColor,
                        ),
                        BaseTextWidget.customText(
                          text: subtitle,
                          fontSize: 12,
                          fontWeight: FontWeight.normal,
                          color: AppColor.kWhiteColor,
                        ),
                        // BaseTextWidget.customText(text: title),
                        // Text.rich(
                        //   TextSpan(
                        //     children: [
                        //       TextSpan(
                        //         text: "$title\n",
                        //         style: BaseTextStyle.customTextStyle(
                        //           kcolor: AppColor.kWhiteColor,
                        //           letterSpacing: 0.8,
                        //           kfontSize: 20,
                        //           kfontWeight: FontWeight.w700,
                        //         ),
                        //       ),
                        //       // TextSpan(
                        //       //   text: "Readymix Lebih Mudah",
                        //       //   style: BaseTextStyle.customTextStyle(
                        //       //     kfontSize: 12,
                        //       //     kcolor: AppColor.kWhiteColor,
                        //       //     kfontWeight: FontWeight.w400,
                        //       //     letterSpacing: 1,
                        //       //   ),
                        //       // ),
                        //     ],
                        //   ),
                        // ),
                        const SizedBox(
                          height: 10,
                        ),
                      ]),
                ),
              ),
            ),
            Positioned(
              right: 0,
              bottom: 10,
              child: img.isNotEmpty
                  ? Image.network(
                      img,
                      height: 80,
                      fit: BoxFit.cover,
                    )
                  : Assets.images.dashboardPng.image(
                      height: 80,
                      fit: BoxFit.cover,
                    ),
            )
          ],
        ),
      ),
    );
  }

  // Widget infoBanner() {
  //   // print("INI LOADING");
  //   print(homeControllers.dataGetBanner);
  //   print(homeControllers.isLoading.value);
  //   // print(homeControllers.isLoading.value);
  //   return Obx(() {
  //     return Column(
  //       children: [
  //         if (homeControllers.dataGetBanner.isEmpty) ...[
  //           Shimmer.fromColors(
  //               baseColor: AppColor.kGreyColor,
  //               highlightColor: AppColor.kWhiteColor,
  //               child: Container(
  //                 height: 120,
  //                 width: 320,
  //                 decoration: BoxDecoration(
  //                     color: AppColor.kGreyColor,
  //                     borderRadius: BorderRadius.circular(8)),
  //               )),
  //         ] else if (homeControllers.dataGetBanner.isEmpty) ...[
  //           CarouselSlider(
  //             carouselController: _controller,
  //             items: List.generate(
  //               2,
  //               (index) => cardCarousel(
  //                 title: "Jalankan Bisnis Readymix",
  //                 img: "",
  //                 subtitle: "Lebih Efisien dan mudah",
  //               ),
  //             ),
  //             options: CarouselOptions(
  //                 autoPlay: true,
  //                 aspectRatio: 2.8,
  //                 // enlargeCenterPage: true,
  //                 viewportFraction: 0.9,
  //                 onPageChanged: (index, reason) {
  //                   setState(() {
  //                     _current = index;
  //                   });
  //                 }),
  //           ),
  //           Row(
  //             mainAxisAlignment: MainAxisAlignment.center,
  //             children: List.generate(
  //                 2,
  //                 (index) => cardCarousel(
  //                       title: "Jalankan Bisnis Readymix",
  //                       img: "",
  //                       subtitle: "Lebih Efisien dan mudah",
  //                     )).asMap().entries.map((entry) {
  //               return GestureDetector(
  //                 onTap: () => _controller.animateToPage(entry.key),
  //                 child: Container(
  //                   width: 10.0,
  //                   height: 10.0,
  //                   margin:
  //                       const EdgeInsets.only(top: 9.0, right: 4.0, left: 4.0),
  //                   decoration: BoxDecoration(
  //                       shape: BoxShape.circle,
  //                       color: (Theme.of(context).brightness == Brightness.dark
  //                               ? Colors.white
  //                               : AppColor.kPrimaryColor)
  //                           .withOpacity(_current == entry.key ? 0.9 : 0.4)),
  //                 ),
  //               );
  //             }).toList(),
  //           ),
  //         ] else ...[
  //           CarouselSlider(
  //             carouselController: _controller,
  //             items: List.generate(
  //                 homeControllers.dataGetBanner.length,
  //                 (index) => Container(
  //                       height: 200,
  //                       decoration: BoxDecoration(
  //                           color: AppColor.kPrimaryColor,
  //                           borderRadius: BorderRadius.circular(5)),
  //                       child: Image.network(
  //                         homeControllers.dataGetBanner[index]['banner'],
  //                         height: 80,
  //                         fit: BoxFit.cover,
  //                       ),
  //                     )

  //                 // cardCarousel(
  //                 //       title: homeControllers.dataGetBanner[index]['title'],
  //                 //       img: homeControllers.dataGetBanner[index]['banner'],
  //                 //       subtitle: homeControllers.dataGetBanner[index]
  //                 //           ['description'],
  //                 //     )
  //                 ),
  //             options: CarouselOptions(
  //                 // height: 250.0,

  //                 // pageSnapping: true,
  //                 // viewportFraction: 0.5,
  //                 autoPlay: true,
  //                 aspectRatio: 2.8,
  //                 // enlargeCenterPage: true,
  //                 viewportFraction: 0.9,
  //                 onPageChanged: (index, reason) {
  //                   setState(() {
  //                     _current = index;
  //                   });
  //                 }),
  //           ),
  //           Row(
  //             mainAxisAlignment: MainAxisAlignment.center,
  //             children: List.generate(
  //                 homeControllers.dataGetBanner.length,
  //                 (index) => cardCarousel(
  //                       title: homeControllers.dataGetBanner[index]['title'],
  //                       img: homeControllers.dataGetBanner[index]['banner'],
  //                       subtitle: homeControllers.dataGetBanner[index]
  //                           ['description'],
  //                     )).asMap().entries.map((entry) {
  //               return GestureDetector(
  //                 onTap: () => _controller.animateToPage(entry.key),
  //                 child: Container(
  //                   width: 10.0,
  //                   height: 10.0,
  //                   margin: const EdgeInsets.symmetric(
  //                       vertical: 9.0, horizontal: 4.0),
  //                   decoration: BoxDecoration(
  //                       shape: BoxShape.circle,
  //                       color: (Theme.of(context).brightness == Brightness.dark
  //                               ? Colors.white
  //                               : AppColor.kPrimaryColor)
  //                           .withOpacity(_current == entry.key ? 0.9 : 0.4)),
  //                 ),
  //               );
  //             }).toList(),
  //           ),
  //         ]
  //       ],
  //     );
  //   });
  // }

  Widget secondInfoBanner() {
    return Container(
      // height: 510,
      width: double.infinity,
      margin: const EdgeInsets.only(bottom: 50, top: 10, left: 10, right: 10),
      // padding: const EdgeInsets.all(20.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        gradient: const RadialGradient(
          radius: 1.5,
          center: Alignment(-0.9, 1.5),
          colors: [
            AppColor.kSoftBlueColor,
            AppColor.kPrimaryColor,
          ],
          stops: [
            0.10,
            1,
          ],
          tileMode: TileMode.mirror,
        ),
      ),
      child: Stack(
        children: [
          Positioned(
            bottom: -29,
            right: 10,
            top: 0,
            child: Assets.icons.industry.image(width: 100),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Jalankan Bisnis Lebih Efisien",
                  style: BaseTextStyle.customTextStyle(
                    kcolor: AppColor.kWhiteColor,
                    kfontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  "Tentukan produk pilihan anda \nmulai segera produksinya",
                  style: BaseTextStyle.customTextStyle(
                    kfontSize: 12,
                    kcolor: AppColor.kWhiteColor,
                    kfontWeight: FontWeight.w400,
                    letterSpacing: 1,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  var productionUnitData = ({}).obs;
  HomeControllers homeControllers = Get.put(HomeControllers());
  Future<void> showBottomFilter(BuildContext context) {
    return showModalBottomSheet<void>(
        context: context,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(10.0))),
        backgroundColor: AppColor.kWhiteColor,
        builder: (BuildContext context) {
          return Obx(() {
            final resultPU = homeControllers.dataLookupPU;
            return resultPU.isEmpty && homeControllers.isLoading.isFalse
                ? SizedBox(
                    height: MediaQuery.of(context).size.height / 1.2,
                    child: Center(
                      child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                    ),
                  )
                : ListView.separated(
                    padding: const EdgeInsets.all(10),
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      if (index < resultPU.length) {
                        return ListTile(
                            onTap: () {
                              if (resultPU[index]['name'] == 'Semua') {
                                ApiService.productionUnitGlobal = null;
                                LocalDataSource.removeLocalVariable(
                                    keys: 'lastestProduction');
                              } else {
                                // SET PRODUCTION UNIT DATA TO GLOBAL
                                ApiService.productionUnitGlobal = {
                                  'id': resultPU[index]['id'],
                                  'name': resultPU[index]['name']
                                };

                                LocalDataSource.setLocalVariable(
                                  key: "lastestProduction",
                                  value: {
                                    'id': resultPU[index]['id'],
                                    'name': resultPU[index]['name']
                                  },
                                );
                              }
                              productionUnitData.value = {
                                'name': resultPU[index]['name'],
                              };
                              Get.back();
                            },
                            title: Text(
                              resultPU[index]['name'],
                              style: BaseTextStyle.customTextStyle(
                                kcolor: AppColor.kPrimaryColor,
                              ),
                            ));
                      } else {
                        return BaseLoadingWidget.cardShimmer(
                          height: 80,
                          shimmerheight: 30,
                          baseColor: AppColor.kSoftGreyColor,
                          highlightColor: AppColor.kWhiteColor,
                          itemCount: 4,
                        );
                      }
                    },
                    separatorBuilder: (context, index) =>
                        const SizedBox(height: 2),
                    itemCount: homeControllers.dataLookupPU.length,
                  );
          });
        });
  }

  // Widget _yourContainer() {
  //   return Opacity(
  //     opacity: 1,
  //     child: Container(
  //       // height: _containerHeight,
  //       alignment: Alignment.center,
  //       padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
  //       decoration: BoxDecoration(
  //         color: AppColor.kWhiteColor,
  //         borderRadius: const BorderRadius.only(
  //           bottomLeft: Radius.circular(10),
  //           bottomRight: Radius.circular(10),
  //         ),
  //         boxShadow: BaseShadowStyle.customBoxshadow,
  //       ),
  //       child: Column(
  //         mainAxisAlignment: MainAxisAlignment.end,
  //         crossAxisAlignment: CrossAxisAlignment.end,
  //         children: [
  //           Row(
  //             crossAxisAlignment: CrossAxisAlignment.center,
  //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //             children: [
  //               Text(
  //                 'Hi, ${BaseGlobalVariable.getLocalVariable(key: 'profile')['username']}',
  //                 style: BaseTextStyle.customTextStyle(
  //                   kfontSize: 14,
  //                   kfontWeight: FontWeight.w700,
  //                   kcolor: AppColor.kPrimaryColor,
  //                 ),
  //               ),
  //               IconButton.filledTonal(
  //                 onPressed: () {
  //                   Get.toNamed('/nofitscreen');
  //                 },
  //                 icon: const Icon(
  //                   Icons.notifications,
  //                   color: AppColor.kPrimaryColor,
  //                 ),
  //               ),
  //             ],
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }
}
