import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class IndexLogistic extends StatelessWidget {
  final BuildContext contextIndex;

  const IndexLogistic({required this.contextIndex, super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20),
      child: _buildGridModule(context),
    );
  }

  Widget _buildGridModule(BuildContext context) {
    List<Map<String, dynamic>> permission = [
      {
        'title': 'Penerimaan\nMaterial',
        'name': 'read-incoming-material',
        'icon': 'assets/icons/task.svg',
        'path': '/incoming-material',
        'show': true
      },
      {
        'title': 'Penggunaan\nMaterial',
        'name': 'read-material-usage',
        'icon': 'assets/icons/task.svg',
        'path': '/usage-material',
        'show': true
      },
      {
        'title': 'Fuel\nUsage',
        'name': 'read-fuel-usage',
        'icon': 'assets/icons/task.svg',
        'path': '/usage-fuel',
        'show': true
      },
      {
        'title': 'Stock\nTake',
        'name': 'read-opname-stock',
        'icon': 'assets/icons/task.svg',
        'path': '/stock-opname',
        'show': true
      },
      // {
      //   'title': 'Data\nMaterial',
      //   'name': 'material-data',
      //   'icon': 'assets/icons/task.svg',
      //   'path': '/material-data',
      //   'show': true
      // },
      {
        'title': 'Purchase\nOrder',
        'name': 'read-purchase-order',
        'icon': 'assets/icons/task.svg',
        'path': '/purchase-order',
        'show': true
      },
      {
        'title': 'Material\nStock',
        'name': 'read-material-stock',
        'icon': 'assets/icons/task.svg',
        'path': '/stock-analytic',
        'show': true
      },
    ];

    // List<Widget> moduleMenus = BaseGlobalFuntion.hasPermission(permission)
    //     .where((obj) => obj['show'] == true)
    //     .map((item) {
    //   return GestureDetector(
    //     onTap: () {
    //       Get.toNamed(
    //         item['path'],
    //       );
    //     },
    //     child: Center(
    //       child: Column(
    //         children: [
    //           Container(
    //             height: 40,
    //             padding:
    //                 const EdgeInsets.symmetric(horizontal: 11, vertical: 8),
    //             decoration: BoxDecoration(
    //               color: AppColor.kSoftGreyColor,
    //               borderRadius: BorderRadius.circular(10),
    //               border: Border.all(color: AppColor.kSoftGreyColor),
    //             ),
    //             child: SvgPicture.asset(
    //               item['icon'],
    //               width: 20,
    //             ),
    //           ),
    //           const SizedBox(height: 2),
    //           Text(
    //             item['title'],
    //             textAlign: TextAlign.center,
    //             style: BaseTextStyle.customTextStyle(
    //               kfontSize: 10,
    //               kfontWeight: FontWeight.w500,
    //             ),
    //           ),
    //         ],
    //       ),
    //     ),
    //   );
    // }).toList();

    //* CARA PENGGUNAAN PLAN B */
    List moduleMenus = BaseGlobalFuntion.hasPermission(permission)
        .where((obj) => obj['show'] == true)
        .toList();

    List<Widget> setPrevMen = BaseGlobalFuntion.setterMenuOther(
      list: moduleMenus,
      context: contextIndex,
      title: "",
    );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (moduleMenus.isNotEmpty) ...[
          // BaseTextWidget.customText(
          //   text: "LOGISTIK",
          //   fontSize: 13,
          //   letterSpacing: 0.9,
          //   fontWeight: FontWeight.w600,
          //   color: AppColor.kPrimaryColor,
          // ),
          // const SizedBox(height: 10),
        ],
        SizedBox(
          width: double.infinity,
          child: GridView.count(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            crossAxisCount: 5,
            childAspectRatio: 0.95,
            children: setPrevMen,
          ),
        ),
      ],
    );
  }
}

// // //*  PLAN B TERDAPAT MENU LAINNYA PADA LIST MENU
// List<Widget> setterMenu({
//   required List list,
//   required BuildContext context,
//   required dynamic title,
// }) {
//   List temporary = [];

//   // print(list.length);
//   if (list.length > 4) {
//     // temporary.add(list.removeLast());
//     // temporary = List.from(list.getRange(5, list.length));
//     temporary.addAll(list.getRange(4, list.length));
//     list.removeRange(4, list.length);
//   }

//   if (list.length < 5) {
//     list.add(
//       {
//         'title': 'Menu\nLainnya',
//         'name': 'lainnya',
//         'icon': 'assets/icons/other.svg',
//         'path': '/lainnya',
//         'show': true
//       },
//     );
//   }

//   return list.map((item) {
//     return GestureDetector(
//       //  Memasukan Menu Lainya pada tab Menu lainnya
//       onTap: () {
//         item['path'] == '/lainnya'
//             ? showModalBottomSheet(
//                 context: context,
//                 builder: (context) => Padding(
//                   padding:
//                       const EdgeInsets.symmetric(horizontal: 10, vertical: 30),
//                   child: Column(
//                     mainAxisSize: MainAxisSize.min,
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Text(
//                         title,
//                         style: BaseTextStyle.customTextStyle(
//                           kcolor: AppColor.kPrimaryColor,
//                           kfontSize: 14,
//                           kfontWeight: FontWeight.w600,
//                         ),
//                       ),
//                       GridView(
//                           physics: const NeverScrollableScrollPhysics(),
//                           gridDelegate:
//                               const SliverGridDelegateWithFixedCrossAxisCount(
//                             crossAxisCount: 4,
//                             crossAxisSpacing: 10,
//                             // mainAxisSpacing: 10,
//                             // childAspectRatio: 0.8,
//                           ),
//                           shrinkWrap: true,
//                           children: [
//                             ...temporary
//                                 .map(
//                                   (e) => GestureDetector(
//                                     onTap: () {
//                                       // Get.back();
//                                       // Get.toNamed(e['path']);
//                                     },
//                                     child: Center(
//                                       child: Column(
//                                         children: [
//                                           Container(
//                                             height: 40,
//                                             padding: const EdgeInsets.symmetric(
//                                                 horizontal: 11, vertical: 8),
//                                             decoration: BoxDecoration(
//                                               borderRadius:
//                                                   BorderRadius.circular(10),
//                                               border: Border.all(
//                                                   color: AppColor
//                                                       .kSoftGreyColor),
//                                             ),
//                                             child: SvgPicture.asset(
//                                               e['icon'],
//                                               width: 20,
//                                             ),
//                                           ),
//                                           const SizedBox(height: 2),
//                                           Text(
//                                             e['title'],
//                                             textAlign: TextAlign.center,
//                                             style:
//                                                 BaseTextStyle.customTextStyle(
//                                               kfontSize: 10,
//                                               kfontWeight: FontWeight.w500,
//                                             ),
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                   ),
//                                 )
//                                 .toList(),
//                           ]),
//                     ],
//                   ),
//                 ),
//               )
//             : Get.toNamed(item['path']);

//         // print(item);
//       },
//       child: Center(
//         child: Column(
//           children: [
//             Container(
//               height: 40,
//               padding: const EdgeInsets.symmetric(horizontal: 11, vertical: 8),
//               decoration: BoxDecoration(
//                 color: AppColor.kSoftGreyColor,
//                 borderRadius: BorderRadius.circular(10),
//                 border: Border.all(color: AppColor.kSoftGreyColor),
//               ),
//               child: SvgPicture.asset(
//                 item['icon'],
//                 width: 20,
//               ),
//             ),
//             const SizedBox(height: 2),
//             Text(
//               item['title'],
//               textAlign: TextAlign.center,
//               style: BaseTextStyle.customTextStyle(
//                 kfontSize: 10,
//                 kfontWeight: FontWeight.w500,
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }).toList();
// }
