import 'package:rbs_mobile_operation/data/datasources/salesorder.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class ProductByCategory extends StatelessWidget {
  final String categoryId;

  const ProductByCategory({required this.categoryId, super.key});

  @override
  Widget build(BuildContext context) {
    AddSalesControllers salesController = Get.put(AddSalesControllers());
    salesController.fetchProduct(categoryId: categoryId);
    Future onRefresh() async {
      salesController.refreshDataProduct(categoryId: categoryId);
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        salesController.fetchProduct(categoryId: categoryId);
      }
    });

    TextEditingController puTextEditingController = TextEditingController();

    Future<void> showBottomFilter(BuildContext context) {
      return showModalBottomSheet<void>(
        context: context,
        backgroundColor: AppColor.kGreyColor.withOpacity(0.1),
        builder: (BuildContext context) {
          return Container(
            // height: 200,
            padding: const EdgeInsets.all(20),
            margin: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: AppColor.kWhiteColor,
              borderRadius: BorderRadius.circular(25),
            ),
            child: FormBuilder(
              // key: salesOrderController.salesFormKeyFilter,
              // onChanged: () {
              //   salesOrderController.salesFormKeyFilter.currentState!
              //       .save();
              // },
              autovalidateMode: AutovalidateMode.disabled,
              skipDisabled: true,
              child: Obx(
                () => SingleChildScrollView(
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Pilih Filter",
                            style: BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kPrimaryColor,
                              kfontSize: 14,
                              kfontWeight: FontWeight.w600,
                            ),
                          ),
                          IconButton.filledTonal(
                            onPressed: () {
                              Get.back();
                            },
                            icon: const Icon(Icons.close),
                          ),
                        ],
                      ),
                      const SizedBox(height: 10),
                      BaseButtonWidget.dropdownButton(
                        fieldName: "production_unit_id",
                        valueController: puTextEditingController,
                        valueData: salesController.dataLookupPU,
                        label: "Pilih unit produksi",
                        searchOnChange: (p0) {},
                        onChange: ({searchValue, valueItem}) {
                          //** ADDED VALUE SELECTION
                          salesController.productionUnit.value = {
                            'production_unit_id': valueItem,
                            'production_unit_name': searchValue,
                          };

                          //** LOAD NEW DATA
                          salesController.refreshDataProduct();

                          //** BACK
                          Get.back();
                        },
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter some text';
                          }
                          return null;
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      );
    }

    final listCardContent = Expanded(
        child: Obx(() => RefreshIndicator(
              onRefresh: onRefresh,
              child: ListView.separated(
                controller: scrollController,
                itemCount: salesController.hasMore.value
                    ? salesController.productData.length + 1
                    : salesController.productData.length,
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(height: 2),
                itemBuilder: (BuildContext context, int index) {
                  if (index < salesController.productData.length) {
                    final data = salesController.productData[index];
                    var colorBadge =
                        BaseGlobalFuntion.statusColor(data['active']);
                    return BaseCardWidget.cardStyle4(
                      ontap: () {},
                      title: data['name'],
                      subtitle: data['production_unit_name'],
                      bottomSubtitle: BaseTextWidget.customText(
                        text: data['product_category_name'],
                        isLongText: true,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                      trailling: BaseGlobalFuntion.currencyLocalConvert(
                          nominal: data['unit_price']),
                      extendWidget: Container(
                        padding: const EdgeInsets.all(4),
                        decoration: BoxDecoration(
                          color: colorBadge['background_color'],
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          data['active'] == '1' ? 'Aktif' : 'Non Aktif',
                          style: BaseTextStyle.customTextStyle(
                            kfontSize: 12,
                            kcolor: colorBadge['text_color'],
                            kfontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    );
                  } else {
                    return BaseLoadingWidget.cardShimmer(
                      height: 400,
                      baseColor: AppColor.kSoftGreyColor,
                      highlightColor: AppColor.kWhiteColor,
                      itemCount: 8,
                    );
                  }
                },
              ),
            )));

    final listFilter = Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: [
          Expanded(
            child: BaseTextFieldWidget.roundedStyleTextField(
              textEditingController: salesController.search,
              onChange: (val) {
                BaseGlobalFuntion.debounceRun(
                  action: () {
                    salesController.refreshDataProduct();
                  },
                  duration: const Duration(milliseconds: 1000),
                );
              },
              suffixIcon: const Icon(Icons.search),
              radius: 10,
              borderSide: const BorderSide(color: AppColor.kInactiveColor),
              hintText: 'Cari',
            ),
          ),
          const SizedBox(width: 5),
          Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
              color: AppColor.kSoftBlueColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: IconButton(
              onPressed: () {
                showBottomFilter(context);
              },
              icon: const Icon(Icons.filter_list),
            ),
          ),
        ],
      ),
    );

    return SafeArea(
      child: PopScope(
        canPop: true,
        onPopInvoked: (didPop) {
          salesController.cart.clear();
          salesController.cartSummary.clear();
        },
        child: Scaffold(
          appBar: BaseExtendWidget.appBar(
            title: 'Produk',
            context: context,
            extendBack: () {
              salesController.productData.clear();
            },
          ),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: Column(
              children: [
                listFilter,
                const SizedBox(height: 10),
                listCardContent,
              ],
            ),
          ),
          resizeToAvoidBottomInset: true,
        ),
      ),
    );
  }
}
