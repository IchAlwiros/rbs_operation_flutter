import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class IndexSales extends StatelessWidget {
  final BuildContext contextIndex;

  const IndexSales({required this.contextIndex, super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 0),
      child: _buildGridModule(contextIndex),
    );
  }

  Widget _buildGridModule(BuildContext context) {
    List<Map<String, dynamic>> permission = [
      {
        'title': 'Sales\nOrder',
        'name': 'read-sales-order',
        'icon': Assets.icons.rbsSalesorderSvg.svg(width: 30),
        'path': '/sales-order',
        'show': true
      },
      {
        'title': 'Rencana\n Produksi',
        'name': 'read-production-schedule',
        'icon': Assets.icons.rbsRencanaproduksiSvg.svg(width: 30),
        'path': '/production-schedule',
        'show': true
      },
      {
        'title': 'Data\nPelanggan',
        'name': 'read-customer',
        'icon': Assets.icons.rbsDatapelangganSvg.svg(width: 30),
        'path': '/data-customer',
        'show': true
      },
      {
        'title': 'Data\nProyek',
        'name': 'read-customer',
        'icon': Assets.icons.rbsDataproyekSvg.svg(width: 30),
        'path': '/project-customer',
        'show': true
      },
      {
        'title': 'Realisasi\nProduksi',
        'name': 'view-production',
        'icon': Assets.icons.rbsRealisasiproduksiSvg.svg(width: 30),
        'path': '/production-data',
        'show': true
      },
      {
        'title': 'Approval\nKredit',
        'name': 'read-submission-credit-approval',
        'icon': Assets.icons.rbsApprovalkreditSvg.svg(width: 30),
        'path': '/kredit-approval',
        'show': true
      },
      // {
      //   'title': 'Data\nProduk',
      //   'name': 'product',
      //   'icon': 'assets/icons/medal.svg',
      //   'path': '/product-data',
      //   'show': true
      // },
      // {
      //   'title': 'Riwayat\nPembayaran',
      //   'name': 'customer-project',
      //   'icon': 'assets/icons/task.svg',
      //   'path': '/payment-history',
      //   'show': true
      // },

      // ** LOGISTIK MENU

      {
        'title': 'Penerimaan\nMaterial',
        'name': 'read-incoming-material',
        'icon': Assets.icons.rbsMaterialmasukSvg.svg(width: 30),
        'path': '/incoming-material',
        'show': true
      },
      {
        'title': 'Penggunaan\nMaterial',
        'name': 'read-material-usage',
        'icon': Assets.icons.rbsPenggunaanmaterialSvg.svg(width: 30),
        'path': '/usage-material',
        'show': true
      },
      {
        'title': 'Fuel\nUsage',
        'name': 'read-fuel-usage',
        'icon': Assets.icons.rbsFuelstockSvg.svg(width: 30),
        'path': '/usage-fuel',
        'show': true
      },
      {
        'title': 'Stock\nTake',
        'name': 'read-opname-stock',
        'icon': Assets.icons.rbsStocktakeSvg.svg(width: 30),
        'path': '/stock-opname',
        'show': true
      },
      // {
      //   'title': 'Data\nMaterial',
      //   'name': 'material-data',
      //   'icon': 'assets/icons/task.svg',
      //   'path': '/material-data',
      //   'show': true
      // },
      {
        'title': 'Purchase\nOrder',
        'name': 'read-purchase-order',
        'icon': Assets.icons.rbsPurchaseorderSvg.svg(width: 30),
        'path': '/purchase-order',
        'show': true
      },
      {
        'title': 'Material\nStock',
        'name': 'read-material-stock',
        'icon': Assets.icons.rbsMaterialstockSvg.svg(width: 30),
        'path': '/stock-analytic',
        'show': true
      },
    ];

// * PLAN A BASIC GRID MENU
    // List<Widget> moduleMenus = BaseGlobalFuntion.hasPermission(permission)
    //     .where((obj) => obj['show'] == true)
    //     .map((item) {
    //   return GestureDetector(
    //     onTap: () {
    //       Get.toNamed(
    //         item['path'],
    //       );
    //     },
    //     child: Center(
    //       child: Column(
    //         children: [
    //           Container(
    //             height: 40,
    //             padding:
    //                 const EdgeInsets.symmetric(horizontal: 11, vertical: 8),
    //             decoration: BoxDecoration(
    //               color: AppColor.kSoftGreyColor,
    //               borderRadius: BorderRadius.circular(10),
    //               border: Border.all(color: AppColor.kSoftGreyColor),
    //             ),
    //             child: SvgPicture.asset(
    //               item['icon'],
    //               width: 30,
    //             ),
    //           ),
    //           const SizedBox(height: 2),
    //           Text(
    //             item['title'],
    //             textAlign: TextAlign.center,
    //             style: BaseTextStyle.customTextStyle(
    //               kfontSize: 10,
    //               kfontWeight: FontWeight.w500,
    //             ),
    //           ),
    //         ],
    //       ),
    //     ),
    //   );
    // }).toList();

    //* CARA PENGGUNAAN PLAN B */
    List moduleMenus = BaseGlobalFuntion.hasPermission(permission)
        .where((obj) => obj['show'] == true)
        .toList();

    List<Widget> setPrevMen = BaseGlobalFuntion.setterMenuOther(
      list: moduleMenus,
      context: contextIndex,
      title: '',
    );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 20),
        if (moduleMenus.isNotEmpty) ...[
          // BaseTextWidget.customText(
          //   text: "SALES",
          //   fontSize: 13,
          //   letterSpacing: 0.9,
          //   fontWeight: FontWeight.w600,
          //   color: AppColor.kPrimaryColor,
          // ),
          // const SizedBox(height: 15),
        ],
        SizedBox(
          width: double.infinity,
          child: GridView.count(
            shrinkWrap: true,
            childAspectRatio: 0.95,
            physics: const NeverScrollableScrollPhysics(),
            crossAxisCount: 5,
            children: setPrevMen,
          ),
        ),
      ],
    );
  }
}

// //*  PLAN B TERDAPAT MENU LAINNYA PADA MENU
// List<Widget> setterPrev({
//   required List list,
//   required BuildContext context,
//   required dynamic title,
// }) {
//   List temporary = [];

//   // print(list.length);
//   if (list.length > 4) {
//     // temporary.add(list.removeLast());
//     // temporary = List.from(list.getRange(5, list.length));
//     temporary.addAll(list.getRange(4, list.length));
//     list.removeRange(4, list.length);
//   }

//   if (list.length < 5) {
//     list.add(
//       {
//         'title': 'Menu\nLainnya',
//         'name': 'lainnya',
//         'icon': 'assets/icons/other.svg',
//         'path': '/lainnya',
//         'show': true
//       },
//     );
//   }

//   return list.map((item) {
//     return GestureDetector(
//       //  Memasukan Menu Lainya pada tab Menu lainnya
//       onTap: () {
//         item['path'] == '/lainnya'
//             ? showModalBottomSheet(
//                 context: context,
//                 builder: (context) => Padding(
//                   padding:
//                       const EdgeInsets.symmetric(horizontal: 10, vertical: 30),
//                   child: Column(
//                     mainAxisSize: MainAxisSize.min,
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Text(
//                         title,
//                         style: BaseTextStyle.customTextStyle(
//                           kcolor: AppColor.kPrimaryColor,
//                           kfontSize: 14,
//                           kfontWeight: FontWeight.w600,
//                         ),
//                       ),
//                       GridView(
//                           physics: const NeverScrollableScrollPhysics(),
//                           gridDelegate:
//                               const SliverGridDelegateWithFixedCrossAxisCount(
//                             crossAxisCount: 4,
//                             crossAxisSpacing: 10,
//                             // mainAxisSpacing: 10,
//                             // childAspectRatio: 0.8,
//                           ),
//                           shrinkWrap: true,
//                           children: [
//                             ...temporary
//                                 .map(
//                                   (e) => GestureDetector(
//                                     onTap: () async {
//                                       Get.back();
//                                       Get.toNamed(e['path']);
//                                     },
//                                     child: Center(
//                                       child: Column(
//                                         children: [
//                                           Container(
//                                             height: 40,
//                                             padding: const EdgeInsets.symmetric(
//                                                 horizontal: 11, vertical: 8),
//                                             decoration: BoxDecoration(
//                                               borderRadius:
//                                                   BorderRadius.circular(10),
//                                               border: Border.all(
//                                                   color: AppColor
//                                                       .kSoftGreyColor),
//                                             ),
//                                             child: SvgPicture.asset(
//                                               e['icon'],
//                                               width: 30,
//                                             ),
//                                           ),
//                                           const SizedBox(height: 2),
//                                           Text(
//                                             e['title'],
//                                             textAlign: TextAlign.center,
//                                             style:
//                                                 BaseTextStyle.customTextStyle(
//                                               kfontSize: 10,
//                                               kfontWeight: FontWeight.w500,
//                                             ),
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                   ),
//                                 )
//                                 .toList(),
//                           ]),
//                     ],
//                   ),
//                 ),
//               )
//             : Get.toNamed(item['path']);

//         // print(item);
//       },
//       child: Center(
//         child: Column(
//           children: [
//             Container(
//               height: 40,
//               padding: const EdgeInsets.symmetric(horizontal: 11, vertical: 8),
//               decoration: BoxDecoration(
//                 color: AppColor.kSoftGreyColor,
//                 borderRadius: BorderRadius.circular(10),
//                 border: Border.all(color: AppColor.kSoftGreyColor),
//               ),
//               child: SvgPicture.asset(
//                 item['icon'],
//                 width: 30,
//               ),
//             ),
//             const SizedBox(height: 2),
//             Text(
//               item['title'],
//               textAlign: TextAlign.center,
//               style: BaseTextStyle.customTextStyle(
//                 kfontSize: 10,
//                 kfontWeight: FontWeight.w500,
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }).toList();
// }
