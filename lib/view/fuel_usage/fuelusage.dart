import 'package:rbs_mobile_operation/core/extentions/date_time_extension.dart';
import 'package:rbs_mobile_operation/data/datasources/fuelusage.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/fuel_usage/create/add_fuel_usage.dart';
import 'package:rbs_mobile_operation/view/fuel_usage/detail/view_fuelusage.dart';

class FuelUsageList extends StatelessWidget {
  const FuelUsageList({super.key});

  @override
  Widget build(BuildContext context) {
    FuelUsageListControllers fuelUsageControllers =
        Get.put(FuelUsageListControllers());

    Future onRefresh() async {
      fuelUsageControllers.refreshFuelUsage();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        if (fuelUsageControllers.fuelUsageData.isNotEmpty) {
          fuelUsageControllers.fetchFuelUsage();
        }
      }
    });

    // ** SHOW FILTER MODAL BOTTOM
    final isActive = ['Semua', 'Success', 'Cancel'];
    TextEditingController equipmentCOntroller = TextEditingController();

    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          fuelUsageControllers.currentFilter.clear();
          fuelUsageControllers.filterFormKey.currentState!.reset();
          fuelUsageControllers.filterFormKey.currentState!.patchValue({
            'status': null,
            'equipment_id': null,
          });
        },
        onFilter: () {
          Get.back();
          fuelUsageControllers.refreshFuelUsage();
        },
        valueFilter: fuelUsageControllers.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: fuelUsageControllers.filterFormKey,
            onChanged: () {
              fuelUsageControllers.filterFormKey.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'equipment_id',
                    controller: equipmentCOntroller,
                    currentValue:
                        fuelUsageControllers.currentFilter['equipment_filter'],
                    label: "Peralatan",
                    hintText: "Pilih peralatan",
                    asyncItems: (String filter) async {
                      fuelUsageControllers.lookupEquipment(searching: filter);
                      return fuelUsageControllers.dataLookupEquipment;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      // print(data);
                      fuelUsageControllers.currentFilter.value = {
                        ...fuelUsageControllers.currentFilter,
                        'equipment_filter': searchValue,
                      };
                    },
                  ),
                  FormBuilderChoiceChip<String>(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      labelText: 'Status',
                      hintText: "Pilih Status",
                      contentPadding: const EdgeInsets.symmetric(vertical: 2),
                      labelStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 15,
                        kfontWeight: FontWeight.w500,
                        kcolor: AppColor.kPrimaryColor,
                      ),
                      errorStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 13,
                        kcolor: AppColor.kFailedColor,
                        kfontWeight: FontWeight.w500,
                      ),
                    ),
                    name: 'status',
                    initialValue: fuelUsageControllers.currentFilter['status'],
                    selectedColor: AppColor.kInactiveColor,
                    spacing: 6.0,
                    options: List.generate(isActive.length, (index) {
                      return FormBuilderChipOption(
                        value: isActive[index],
                        child: Text(
                          isActive[index],
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontWeight: FontWeight.w600,
                            kfontSize: 12,
                          ),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    // ** LIST OF CONTENT CARD

    final listItemContent = Flexible(
      child: Obx(
        () {
          final result = fuelUsageControllers.fuelUsageData;

          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: result.isEmpty && fuelUsageControllers.isLoading.isFalse
                ? SizedBox(
                    height: MediaQuery.of(context).size.height / 1.2,
                    child: Center(
                      child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                    ),
                  )
                : ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: result.length + 1,
                    separatorBuilder: (BuildContext context, int index) =>
                        const SizedBox(height: 5),
                    itemBuilder: (BuildContext context, int index) {
                      if (index < result.length) {
                        final data = result[index];

                        return BaseCardWidget.cardStyle1(
                          ontap: () {
                            Get.to(() => FuelUsageView(itemId: data['id']));
                          },
                          title: data['equipment_name'],
                          subTitle1: data['equipment_code'],
                          subTitle2: BaseTextWidget.customText(
                            text: data['production_unit_name'],
                            isLongText: true,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                          trailing: BaseTextWidget.customText(
                            text: data['quantity'],
                            extendText: "\nLiter",
                            textAlign: TextAlign.right,
                            fontWeight: FontWeight.w600,
                          ),
                          headerString: DateTime.parse(data['created_at'])
                              .toFormattedDateTime(),
                        );
                      } else if (result.length < 25) {
                        return fuelUsageControllers.hasMore.value &&
                                fuelUsageControllers.isLoading.isTrue
                            ? BaseLoadingWidget.cardShimmer(
                                height: MediaQuery.of(context).size.height,
                                shimmerheight: 80,
                                baseColor: AppColor.kSoftGreyColor,
                                highlightColor: AppColor.kWhiteColor,
                                itemCount: 25,
                              )
                            : const SizedBox();
                      } else {
                        return fuelUsageControllers.hasMore.value
                            ? BaseLoadingWidget.cardShimmer(
                                height: 200,
                                shimmerheight: 90,
                                baseColor: AppColor.kSoftGreyColor,
                                highlightColor: AppColor.kWhiteColor,
                                itemCount: 2,
                              )
                            : const SizedBox();
                      }
                    },
                  ),
          );
        },
      ),
    );

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
            title: 'Fuel Usage',
            context: context,
            isAllFill: true,
            searchController: fuelUsageControllers.search,
            searchText: 'Cari',
            onFillterChange: () {
              showBottomFilter(context);
            },
            onSearchChange: () {
              BaseGlobalFuntion.debounceRun(
                action: () {
                  fuelUsageControllers.refreshFuelUsage();
                },
                duration: const Duration(milliseconds: 1000),
              );
            }),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              RefreshIndicator(
                onRefresh: onRefresh,
                child: SingleChildScrollView(
                  controller: scrollController,
                  physics: const AlwaysScrollableScrollPhysics(),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const SizedBox(height: 10),
                      listItemContent,
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 30.0,
                right: 15.0,
                child: SizedBox(
                  height: 55.0,
                  width: 55.0,
                  child: IconButton.filled(
                    iconSize: 30,
                    onPressed: () {
                      Get.to(() => const AddFuelUsage());
                    },
                    icon: const Icon(
                      Icons.add,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
