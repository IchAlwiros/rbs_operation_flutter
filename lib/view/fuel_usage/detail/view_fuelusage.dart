import 'package:rbs_mobile_operation/core/extentions/date_time_extension.dart';
import 'package:rbs_mobile_operation/data/datasources/fuelusage.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class FuelUsageView extends StatelessWidget {
  final int itemId;

  const FuelUsageView({
    required this.itemId,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    FuelUsageListControllers fuelUsageControllers =
        Get.put(FuelUsageListControllers());

    Future.delayed(const Duration(milliseconds: 100), () {
      fuelUsageControllers.viewdetailFuel(itemId);
    });

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Detail Fuel Usage',
          context: context,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              // Obx(() => segmentTab1(
              //       data: fuelUsageControllers.detailFuelUsage,
              //       isLoading: fuelUsageControllers.isLoading.value,
              //     )),
              Obx(() {
                var colorBadge = BaseGlobalFuntion.statusColor(
                    fuelUsageControllers.detailFuelUsage['status_code']);
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          right: 20, left: 20, top: 15, bottom: 50),
                      child: BaseCardWidget.detailInfoCard2(
                        isLoading: fuelUsageControllers.isLoading.value,
                        contentList: [
                          {
                            'title': 'Unit Produksi',
                            'content': BaseTextWidget.customText(
                              text: fuelUsageControllers
                                  .detailFuelUsage['production_unit_name'],
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              maxLengthText: 35,
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.factory_rounded,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'Waktu Pengisian',
                            'content': BaseTextWidget.customText(
                              text: DateTime.parse(fuelUsageControllers
                                      .detailFuelUsage['date'])
                                  .toFormattedDateTime(),
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              maxLengthText: 35,
                              isLongText: true,
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.factory_rounded,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'Peralatan',
                            'content': Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                BaseTextWidget.customText(
                                  text: fuelUsageControllers
                                      .detailFuelUsage['equipment_name'],
                                  color: AppColor.kBlackColor,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500,
                                  isLongText: true,
                                ),
                                BaseTextWidget.customText(
                                  text: fuelUsageControllers
                                      .detailFuelUsage['equipment_code'],
                                  color: AppColor.kGreyColor,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500,
                                  isLongText: true,
                                  fontStyle: FontStyle.italic,
                                ),
                              ],
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.fire_truck_rounded,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'Gudang',
                            'content': BaseTextWidget.customText(
                              text: fuelUsageControllers
                                  .detailFuelUsage['warehouse_name'],
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              maxLengthText: 35,
                              isLongText: true,
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.location_on_rounded,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'Total Pengisian',
                            'content': BaseTextWidget.customText(
                              text: fuelUsageControllers
                                  .detailFuelUsage['quantity'],
                              extendText: " Liter",
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              maxLengthText: 35,
                              isLongText: true,
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.view_in_ar_outlined,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'Flow Meter Awal',
                            'content': BaseTextWidget.customText(
                              text: BaseGlobalFuntion.currencyLocalConvert(
                                nominal: fuelUsageControllers
                                    .detailFuelUsage['start_flow_meter'],
                                isLocalCurency: false,
                              ),
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              maxLengthText: 35,
                              isLongText: true,
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.speed_rounded,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'Flow Meter Akhir',
                            'content': BaseTextWidget.customText(
                              text: BaseGlobalFuntion.currencyLocalConvert(
                                nominal: fuelUsageControllers
                                    .detailFuelUsage['end_flow_meter'],
                                isLocalCurency: false,
                              ),
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              maxLengthText: 35,
                              isLongText: true,
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.speed_rounded,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'Odometer Awal',
                            'content': BaseTextWidget.customText(
                              text: BaseGlobalFuntion.currencyLocalConvert(
                                nominal: fuelUsageControllers
                                    .detailFuelUsage['odometer_actual'],
                                isLocalCurency: false,
                              ),
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              maxLengthText: 35,
                              isLongText: true,
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.speed_rounded,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'Odometer Akhir',
                            'content': BaseTextWidget.customText(
                              text: BaseGlobalFuntion.currencyLocalConvert(
                                nominal: fuelUsageControllers
                                    .detailFuelUsage['odometer_before'],
                                isLocalCurency: false,
                              ),
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              maxLengthText: 35,
                              isLongText: true,
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.speed_rounded,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'Jarak(KM)',
                            'content': BaseTextWidget.customText(
                              text: fuelUsageControllers
                                  .detailFuelUsage['distance'],
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              maxLengthText: 35,
                              isLongText: true,
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.add_road,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'Index KM/L',
                            'content': BaseTextWidget.customText(
                              text: fuelUsageControllers
                                  .detailFuelUsage['index_kml'],
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              maxLengthText: 35,
                              isLongText: true,
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.speed_rounded,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'Pengisi Solar',
                            'content': BaseTextWidget.customText(
                              text: fuelUsageControllers
                                  .detailFuelUsage['fuelman'],
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              maxLengthText: 35,
                              isLongText: true,
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.person_4_outlined,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'Status',
                            'content': Container(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 2, horizontal: 6),
                                decoration: BoxDecoration(
                                  color: colorBadge['background_color'],
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: BaseTextWidget.customText(
                                  text: fuelUsageControllers
                                      .detailFuelUsage['status_code'],
                                  fontSize: 11,
                                  color: colorBadge['text_color'],
                                )),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: Icon(
                                  Icons.radio_button_checked_sharp,
                                  size: 20,
                                  color: colorBadge['text_color'],
                                ))
                          },
                        ],
                      ),
                    ),
                  ],
                );
              })
            ],
          ),
        ),
      ),
    );
  }
}
