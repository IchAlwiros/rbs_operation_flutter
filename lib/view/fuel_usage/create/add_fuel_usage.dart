import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:rbs_mobile_operation/data/datasources/fuelusage.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';

class AddFuelUsage extends StatelessWidget {
  const AddFuelUsage({super.key});

  @override
  Widget build(BuildContext context) {
    AddFuelUsageControllers fuelUsageControllers =
        Get.put(AddFuelUsageControllers());

    final submitAction = Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Obx(() => Expanded(
                  child: Button.filled(
                    onPressed: () =>
                        fuelUsageControllers.addStockTake(loadingCtx: context),
                    disabled: fuelUsageControllers.isLoading.value,
                    color: AppColor.kPrimaryColor,
                    height: 60,
                    icon: const Icon(
                      Icons.add_circle,
                      color: AppColor.kWhiteColor,
                    ),
                    // textStyle: BaseTextStyle.customTextStyle(
                    //   kfontWeight: FontWeight.w600,
                    //   kfontSize: 12,
                    //   kcolor: AppColor.kWhiteColor,
                    // ),
                    label: "Simpan",
                  ),
                )),
          ],
        ),
      ),
    );

    final topContent = Container(
      height: 190,
      width: double.infinity,
      padding: const EdgeInsets.only(right: 20, left: 20, bottom: 20),
      decoration: const BoxDecoration(
        color: AppColor.kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
      ),
    );

    final detailPayment = Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      margin: const EdgeInsets.only(right: 10, left: 10, top: 10, bottom: 10),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: AppColor.kGreyColor),
      ),
      child: Column(
        children: [
          FormBuilder(
            key: fuelUsageControllers.fkAddStockTake,
            onChanged: () {
              fuelUsageControllers.fkAddStockTake.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Obx(() => fuelUsageControllers.dataLookupPU.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih unit produksi',
                          label: "Unit Produksi",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib diisi!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'production_unit_id',
                          isRequired: true,
                          controller:
                              fuelUsageControllers.productionUnitController,
                          initialValue: ApiService.productionUnitGlobal ??
                              LocalDataSource.getLocalVariable(
                                  key: 'lastestProduction'),
                          label: "Unit Produksi",
                          hintText: "Pilih Unit Produksi",
                          asyncItems: (String filter) async {
                            fuelUsageControllers.lookupProductionUnit(
                                searching: filter);
                            return fuelUsageControllers.dataLookupPU;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            fuelUsageControllers.currentFilter(
                                {"production_unit_id": valueItem});
                            fuelUsageControllers.lookupEquipment();
                            fuelUsageControllers.lookupFuel();

                            fuelUsageControllers.dataLookupEquipment.clear();
                            fuelUsageControllers.dataLookupFuel.clear();
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib diisi!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  Obx(() => fuelUsageControllers.dataLookupFuel.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih Bahan Bakar',
                          label: "Bahan Bakar",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Bahan bakar wajib diisi!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'fuel_id',
                          controller: fuelUsageControllers.materialController,
                          isRequired: true,
                          label: "Bahan Bakar",
                          hintText: "Pilih Bahan Bakar",
                          asyncItems: (String filter) async {
                            fuelUsageControllers.lookupFuel(searching: filter);
                            return fuelUsageControllers.dataLookupFuel;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            fuelUsageControllers.currentFilter({
                              ...fuelUsageControllers.currentFilter,
                              "material_id": valueItem,
                            });

                            fuelUsageControllers.lookupWarehouse();
                            fuelUsageControllers.dataLookupWarehouse.clear();
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Bahan bakar wajib diisi!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  Obx(() => fuelUsageControllers.dataLookupWarehouse.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih gudang',
                          label: "Gudang/Lokasi Penyimpanan",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Gudang wajib diisi!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'warehouse_id',
                          controller: fuelUsageControllers.warehouseController,
                          isRequired: true,
                          label: "Gudang/Lokasi Penyimpanan",
                          hintText: "Pilih gudang",
                          asyncItems: (String filter) async {
                            fuelUsageControllers.lookupWarehouse(
                                searching: filter);
                            return fuelUsageControllers.dataLookupWarehouse;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            //   fuelUsageControllers.currentFilter({
                            //   ...fuelUsageControllers.currentFilter,
                            //   "material_id": valueItem,
                            // });

                            fuelUsageControllers.viewLastFlow(
                                warehouseId: data['id']);
                            fuelUsageControllers.dataLastFlow.clear();
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Gudang wajib diisi!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  Obx(() => fuelUsageControllers.dataLookupEquipment.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih peralatan',
                          label: "Peralatan",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Peralatan wajib diisi!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'equipment_id',
                          controller: fuelUsageControllers.equipmentController,
                          isRequired: true,
                          label: "Peralatan",
                          hintText: "Pilih peralatan",
                          asyncItems: (String filter) async {
                            fuelUsageControllers.lookupEquipment(
                                searching: filter);
                            return fuelUsageControllers.dataLookupEquipment;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            // fuelUsageControllers.dataViewInventory.clear();
                            if (data['equipment_type_code'] == "TM") {
                              // print("ODOMETER AWAL");
                              // print(data['odometer']);
                              fuelUsageControllers
                                      .initialOdometerControllers.text =
                                  BaseGlobalFuntion.currencyLocalConvert(
                                nominal: data['odometer'],
                                isLocalCurency: false,
                                decimalDigits: 0,
                              );
                            }
                            fuelUsageControllers.currentFilter({
                              ...fuelUsageControllers.currentFilter,
                              "type_equipment": data['equipment_type_code'],
                            });
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Peralatan wajib diisi!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  Obx(() {
                    var typeEquipment =
                        fuelUsageControllers.currentFilter['type_equipment'];
                    if (typeEquipment == "TM") {
                      return Column(
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: CustomTextField.style2(
                                  title: 'Odometer Awal',
                                  hintText: "0.00",
                                  isRequired: true,
                                  enabled: false,
                                  readOnly: true,
                                  controller: fuelUsageControllers
                                      .initialOdometerControllers,
                                  fieldName: 'start_odometer',
                                  onChange: ({value}) {},
                                  validator: (value) {
                                    return null;
                                  },
                                ),
                              ),
                              const SizedBox(width: 5),
                              Expanded(
                                child: CustomTextField.style2(
                                  title: 'Odometer Aktual',
                                  hintText: "0.00",
                                  isRequired: true,
                                  fieldName: 'actual_miliage',
                                  controller: fuelUsageControllers
                                      .actualOdometerControllers,
                                  onChange: ({value}) {
                                    if (int.parse(fuelUsageControllers
                                            .initialOdometerControllers.text
                                            .replaceAll(',', '')) <
                                        int.parse(value.replaceAll('.', ''))) {
                                      final selisih = double.parse(
                                              value.replaceAll(',', '')) -
                                          double.parse(fuelUsageControllers
                                              .initialOdometerControllers.text
                                              .replaceAll(',', ''));

                                      fuelUsageControllers.selisihJarak.value =
                                          selisih;
                                      // double.parse(
                                      //         value.replaceAll(',', '') ??
                                      //             "0") -
                                      //     double.parse(fuelUsageControllers
                                      //         .initialFlowMeterControllers
                                      //         .text
                                      //         .replaceAll(',', ''));

                                      // printInfo(info: "HALLOOO");
                                    } else {
                                      fuelUsageControllers.selisihJarak.value =
                                          0;
                                    }
                                  },
                                  // onClearText: () => fuelUsageControllers
                                  //     .fkAddStockTake.currentState!
                                  //     .patchValue({'actual_miliage': ""}),
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Aktual wajib diisi!';
                                    }

                                    if (int.parse(fuelUsageControllers
                                            .initialOdometerControllers.text
                                            .replaceAll(',', '')) >
                                        int.parse(value.replaceAll('.', ''))) {
                                      return 'Odometer Aktual tidak boleh kurang dari ${fuelUsageControllers.initialOdometerControllers.text}';
                                    }

                                    return null;
                                  },
                                  keyboardType:
                                      const TextInputType.numberWithOptions(
                                          decimal: true),
                                  textInputAction: TextInputAction.done,
                                  inputFormater: <TextInputFormatter>[
                                    FilteringTextInputFormatter.allow(
                                        RegExp(r'^\d*\.?\d*')),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Container(
                            padding: const EdgeInsets.all(10),
                            margin: const EdgeInsets.only(top: 10),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: AppColor.kGreyColor2,
                            ),
                            child: Row(
                              children: [
                                const Icon(
                                  Icons.notifications,
                                  color: AppColor.kPrimaryColor,
                                ),
                                const SizedBox(width: 10),
                                BaseTextWidget.customText(
                                  text: "Jarak ",
                                  fontWeight: FontWeight.w600,
                                  color: AppColor.kPrimaryColor,
                                ),
                                Obx(
                                  () => BaseTextWidget.customText(
                                    text:
                                        fuelUsageControllers.selisihJarak.value,
                                    extendText: " KM",
                                    fontWeight: FontWeight.w500,
                                    color: AppColor.kPrimaryColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      );
                    } else {
                      return const SizedBox();
                    }
                  }),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Jumlah Solar',
                    hintText: "0.00",
                    isRequired: true,
                    fieldName: 'quantity',
                    onClearText: () => fuelUsageControllers
                        .fkAddStockTake.currentState!
                        .patchValue({'quantity': null}),
                    onChange: ({value}) {
                      if (value != null && value.isNotEmpty) {
                        final flowCurrentmeter =
                            double.parse(value.replaceAll(',', '')) +
                                double.parse(fuelUsageControllers
                                    .initialFlowMeterControllers.text
                                    .replaceAll(',', ''));
                        // double.parse(value.replaceAll(',', '.')) +
                        //     double.parse(fuelUsageControllers
                        //         .initialFlowMeterControllers.text
                        //         .replaceAll(',', '.'));

                        // print("PRINT FLOW METER: $flowCurrentmeter");
                        fuelUsageControllers.lastFlowMeterControllers.text =
                            BaseGlobalFuntion.currencyLocalConvert(
                          nominal: (flowCurrentmeter),
                          isLocalCurency: false,
                        );
                      } else {
                        fuelUsageControllers.lastFlowMeterControllers.text =
                            "0";
                      }
                    },
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Jumlah solar wajib diisi!';
                      }

                      if (num.parse(value) < 1) {
                        return 'Jumlah Solar tidak boleh kurang dari 1!';
                      }

                      return null;
                    },
                    keyboardType:
                        const TextInputType.numberWithOptions(decimal: true),
                    textInputAction: TextInputAction.done,
                    inputFormater: <TextInputFormatter>[
                      // FilteringTextInputFormatter.allow(RegExp(r'^[0-9.]+$')),
                      FilteringTextInputFormatter.allow(RegExp(r'^\d*\.?\d*')),
                    ],
                  ),
                  const SizedBox(height: 5),
                  Row(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: CustomTextField.style2(
                          title: 'Flow Meter Awal',
                          hintText: "0.00",
                          enabled: false,
                          controller:
                              fuelUsageControllers.initialFlowMeterControllers,
                          // isRequired: true,
                          readOnly: true,
                          fieldName: 'start_flow_meter',
                          onChange: ({value}) {},
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Flow meter awal wajib diisi!';
                            }

                            return null;
                          },
                          keyboardType: const TextInputType.numberWithOptions(
                              decimal: true),
                          textInputAction: TextInputAction.next,
                          inputFormater: <TextInputFormatter>[
                            FilteringTextInputFormatter.allow(
                                RegExp(r'^[0-9.]+$')),
                          ],
                        ),
                      ),
                      const SizedBox(width: 5),
                      Obx(() => Expanded(
                            child: CustomTextField.style2(
                              title: 'Flow Meter Akhir',
                              hintText:
                                  '${BaseGlobalFuntion.currencyLocalConvert(nominal: fuelUsageControllers.dataLastFlow['last_flow_meter'], isLocalCurency: false)}',
                              isRequired: true,
                              controller:
                                  fuelUsageControllers.lastFlowMeterControllers,
                              fieldName: 'end_flow_meter',
                              onChange: ({value}) {},
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Flow meter akhir wajib diisi!';
                                }

                                return null;
                              },
                              textInputAction: TextInputAction.next,
                              keyboardType:
                                  const TextInputType.numberWithOptions(
                                      decimal: true),
                              inputFormater: [
                                FilteringTextInputFormatter.allow(
                                    RegExp(r'^\d*\.?\d*')),
                              ],
                            ),
                          )),
                    ],
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Catatan',
                    hintText: "Ketik catatan...",
                    fieldName: 'description',
                    isLongText: true,
                    onClearText: () => fuelUsageControllers
                        .fkAddStockTake.currentState!
                        .patchValue({'description': null}),
                    onChange: ({value}) {},
                    validator: (value) {
                      return null;
                    },
                    textInputAction: TextInputAction.done,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        title: 'Tambah Penggunaan Bahan Bakar',
        centerTitle: true,
        context: context,
        extendBack: () {
          // salesController.dataLookupCustomerProject.clear();
          // salesController.dataDetailCustomer.clear();
        },
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            topContent,
            const SizedBox(height: 10),
            detailPayment,
          ],
        ),
      ),
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: submitAction,
    );
  }
}
