import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rbs_mobile_operation/core/components/regional_maps_picker.dart';
import 'package:rbs_mobile_operation/data/datasources/global/geolocator.dart';
import 'package:rbs_mobile_operation/data/datasources/global/helper_maps.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';
import 'package:rbs_mobile_operation/data/datasources/project.controller.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class AddProject extends StatelessWidget {
  const AddProject({super.key});

  @override
  Widget build(BuildContext context) {
    AddProjectControllers addProjectControllers =
        Get.put(AddProjectControllers());

    HelperMapsControllers helperMapsControllers =
        Get.put(HelperMapsControllers());

    var status = [
      {'text': 'Aktif', 'value': '1', 'isActive': true},
      {'text': 'Tidak Aktif', 'value': '0', 'isActive': false},
    ].obs;
    var isTriggerOther = false.obs;

    final submitAction = Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Obx(
              () => Expanded(
                child: Button.filled(
                  onPressed: () => addProjectControllers.addProject(
                    loadingCtx: context,
                    addressMaps: helperMapsControllers.dataMapProjectHelper,
                  ),
                  label: "Simpan",
                  disabled: addProjectControllers.isLoading.value,
                  // isLoading: addProjectControllers.isLoading.value,
                ),
              ),
            )
            // Obx(() => Expanded(
            //       child: BaseButtonWidget.primaryIconButton(
            //         onPressed: () =>
            //             addProjectControllers.addProject(loadingCtx: context),
            //         isLoading: addProjectControllers.isLoading.value,
            //         isDisable: addProjectControllers.isLoading.value,
            //         kcolor: AppColor.kPrimaryColor,
            //         // height: BoxShape.,
            //         height: queryData.,
            //         iconData: Icons.add_circle,
            //         textStyle: BaseTextStyle.customTextStyle(
            //           kfontWeight: FontWeight.w600,
            //           kfontSize: 12,
            //           kcolor: AppColor.kWhiteColor,
            //         ),
            //         title: "Simpan",
            //       ),
            //     )),
          ],
        ),
      ),
    );

    final topContent = Container(
      height: 190,
      width: double.infinity,
      padding: const EdgeInsets.only(right: 20, left: 20, bottom: 20),
      decoration: const BoxDecoration(
        color: AppColor.kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
      ),
    );

    final detailPayment = Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      margin: const EdgeInsets.only(right: 10, left: 10, top: 10, bottom: 10),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: AppColor.kGreyColor),
      ),
      child: Column(
        children: [
          FormBuilder(
            key: addProjectControllers.fkAddProject,
            onChanged: () {
              addProjectControllers.fkAddProject.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Obx(() => addProjectControllers.dataLookupPU.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih unit produksi',
                          label: "Unit Produksi",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib dipilih!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'production_unit_id',
                          asyncItems: (String filter) async {
                            addProjectControllers.lookupProductionUnit(
                                searching: filter);
                            return addProjectControllers.dataLookupPU;
                          },
                          controller:
                              addProjectControllers.productionUnitController,
                          onChange: ({searchValue, valueItem, data}) {
                            addProjectControllers.currentFilter(
                                {"production_unit_id": valueItem});

                            addProjectControllers.lookupCustomer();
                            addProjectControllers.dataLookupCustomer.clear();
                          },
                          label: "Unit Produksi",
                          hintText: "Pilih Unit Produksi",
                          isRequired: true,
                          initialValue: ApiService.productionUnitGlobal ??
                              LocalDataSource.getLocalVariable(
                                  key: 'lastestProduction'),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib dipilih!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  Obx(
                    () => addProjectControllers.dataLookupCustomer.isEmpty
                        ? BaseTextFieldWidget.disableTextForm2(
                            disabledText: 'Pilih Pelanggan',
                            label: "Pelanggan",
                            isRequired: true,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Pelanggan wajib dipilih!';
                              }
                              return null;
                            },
                          )
                        : CustomDropDownSearch(
                            fieldName: 'customer_id',
                            isRequired: true,
                            controller:
                                addProjectControllers.customerController,
                            label: "Pelanggan",
                            hintText: "Pilih Pelanggan",
                            asyncItems: (String filter) async {
                              addProjectControllers.lookupCustomer(
                                  searching: filter);
                              return addProjectControllers.dataLookupCustomer;
                            },
                            onChange: ({searchValue, valueItem, data}) {},
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Pelanggan wajib dipilih!';
                              }
                              return null;
                            },
                          ),
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Nama Proyek',
                    fieldName: 'project_name',
                    hintText: "Ketik Nama Proyek disini..",
                    isRequired: true,
                    onClearText: () => addProjectControllers
                        .fkAddProject.currentState!
                        .patchValue({'project_name': null}),
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Nama proyek wajib diisi!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                    inputFormater: <TextInputFormatter>[
                      // Formatter untuk mengubah input menjadi huruf kapital
                      UppercaseInputFormatter(),
                    ],
                  ),

                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Nama PIC',
                    fieldName: 'project_pic',
                    hintText: "Ketik Nama PIC disini..",
                    isRequired: true,
                    onClearText: () => addProjectControllers
                        .fkAddProject.currentState!
                        .patchValue({'project_pic': null}),
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Nama pic proyek wajib diisi!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                  // BaseTextFieldWidget.textFieldFormBuilder2(
                  //   title: 'Nama PIC',
                  //   hintText: "Ketik Nama PIC disini..",
                  //   fieldName: 'project_pic',
                  //   isRequired: true,
                  //   onClearText: () => addProjectControllers
                  //       .fkAddProject.currentState!
                  //       .patchValue({'project_pic': null}),
                  //   onChange: ({value}) {},
                  //   validator: (value) {
                  //     if (value == null || value.isEmpty) {
                  //       return 'Nama pic proyek wajib diisi!';
                  //     }
                  //     return null;
                  //   },
                  //   textInputAction: TextInputAction.next,
                  // ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'No.Telp',
                    fieldName: 'phone',
                    hintText: "Ketik No.Telp disini..",
                    // isRequired: true,
                    onClearText: () => addProjectControllers
                        .fkAddProject.currentState!
                        .patchValue({'phone': null}),
                    onChange: ({value}) {},
                    validator: (value) {
                      return null;
                    },
                    keyboardType: TextInputType.phone,
                    textInputAction: TextInputAction.next,
                    inputFormater: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                      // for version 2 and greater youcan also use this
                      FilteringTextInputFormatter.digitsOnly
                    ],
                  ),
                  //
                  const SizedBox(height: 5),

                  Obx(() {
                    isTriggerOther.isTrue;
                    return CustomRadioFormField(
                      title: "Status",
                      fieldName: "active",
                      isRequired: true,
                      dataSelect: status,
                      initialValue: "1",
                      onChanged: (value) {
                        isTriggerOther.value = !isTriggerOther.value;
                        for (var obj in status) {
                          if (obj['value'].toString() == value) {
                            obj['isActive'] = true;
                          } else {
                            obj['isActive'] = false;
                          }
                        }
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Status wajib dipilih!';
                        }
                        return null;
                      },
                    );
                    // return BaseTextFieldWidget.selectFieldFormBuilder(
                    //   title: "Status",
                    //   fieldName: "active",
                    //   isRequired: true,
                    //   dataSelect: status,
                    //   initialValue: "1",
                    //   onChanged: (value) {
                    //     isTriggerOther.value = !isTriggerOther.value;
                    //     for (var obj in status) {
                    //       if (obj['value'].toString() == value) {
                    //         obj['isActive'] = true;
                    //       } else {
                    //         obj['isActive'] = false;
                    //       }
                    //     }
                    //   },
                    //   validator: (value) {
                    //     if (value == null || value.isEmpty) {
                    //       return 'Status wajib dipilih!';
                    //     }
                    //     return null;
                    //   },
                    // );
                  }),

                  const SizedBox(height: 5),

                  CustomTextField.style2(
                    title: 'Alamat Lengkap',
                    fieldName: 'address',
                    hintText: 'Pilih Alamat',
                    // controller: addProjectControllers.detailAddressControllers,
                    controller: helperMapsControllers.detailAddressControllers,
                    isRequired: true,
                    readOnly: true,
                    isLongText: true,
                    onTap: () async {
                      // Position position = await Geolocator.getCurrentPosition(
                      //   desiredAccuracy: LocationAccuracy.high,
                      // );

                      // Position? currentPosition =
                      //     await LocationCoordinatesControllers()
                      //         .getCurrentPosition(
                      //   loadingCtx: context,
                      // );

                      // Position? currentPosition =
                      //     await Geolocator.getLastKnownPosition();

                      // showAboutDialog(context: context);

                      Get.to(
                        () => GMapsPicker(
                          // position: currentPosition!,
                          mapsHelper: helperMapsControllers.helperMaps,
                          detailMapsControllers:
                              helperMapsControllers.detailAddressControllers,
                          collectMapsData:
                              helperMapsControllers.dataMapProjectHelper,
                        ),
                      );
                      // BaseExtendWidget.mapsPicker(
                      //   context: context,
                      //   mapsHelper: helperMapsControllers.helperMaps,
                      //   detailMapsControllers:
                      //       helperMapsControllers.detailAddressControllers,
                      //   collectMapsData:
                      //       helperMapsControllers.dataMapProjectHelper,
                      // );
                    },
                    onClearText: () => addProjectControllers
                        .fkAddProject.currentState!
                        .patchValue({'address': null}),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Alamat wajib dipilih!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.done,
                  ),
                  // BaseTextFieldWidget.textFieldFormBuilder2(
                  //   title: 'Alamat Lengkap',
                  //   fieldName: 'address',
                  //   hintText: 'Pilih Alamat',
                  //   isRequired: true,
                  //   onClearText: () => addProjectControllers
                  //       .fkAddProject.currentState!
                  //       .patchValue({'address': null}),
                  //   readOnly: true,
                  //   isLongText: true,
                  //   controller: addProjectControllers.detailAddressControllers,
                  //   onTap: () {
                  //     // showBottomGoogleMap(context);
                  // BaseExtendWidget.mapsPicker(
                  //   context: context,
                  //   mapsHelper: addProjectControllers.helperMaps,
                  //   detailMapsControllers:
                  //       addProjectControllers.detailAddressControllers,
                  //   collectMapsData:
                  //       addProjectControllers.dataMapProjectHelper,
                  // );
                  //   },
                  //   onChange: ({value}) {},
                  //   validator: (value) {
                  //     if (value == null || value.isEmpty) {
                  //       return 'Alamat wajib dipilih!';
                  //     }
                  //     return null;
                  //   },
                  //   textInputAction: TextInputAction.next,
                  // ),
                ],
              ),
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        title: 'Tambah Proyek',
        centerTitle: true,
        context: context,
        extendBack: () {
          // salesController.dataLookupCustomerProject.clear();
          // salesController.dataDetailCustomer.clear();
        },
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            topContent,
            const SizedBox(height: 10),
            detailPayment,
          ],
        ),
      ),
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: submitAction,
    );
  }
}
