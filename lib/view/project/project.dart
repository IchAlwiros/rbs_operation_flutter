import 'package:rbs_mobile_operation/data/datasources/project.controller.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/app_view_resource.dart';
import 'package:rbs_mobile_operation/view/project/create/add_project.dart';

class ProjectCustomerList extends StatelessWidget {
  const ProjectCustomerList({super.key});

  @override
  Widget build(BuildContext context) {
    ProjectListControllers projecController = Get.put(ProjectListControllers());

    Future onRefresh() async {
      projecController.refreshDataProject();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        if (projecController.projectData.isNotEmpty) {
          projecController.fetchProjek();
        }
      }
    });

    final isActive = ['Semua', 'Aktif', 'Non Aktif'];
    TextEditingController customerFilterController = TextEditingController();
    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          projecController.currentFilter.clear();
          projecController.filterFormKey.currentState!.reset();
          projecController.filterFormKey.currentState!
              .patchValue({'active': null, 'customer_id': null});
        },
        onFilter: () {
          Get.back();
          projecController.refreshDataProject();
        },
        valueFilter: projecController.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: projecController.filterFormKey,
            onChanged: () {
              projecController.filterFormKey.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'customer_id',
                    controller: customerFilterController,
                    currentValue:
                        projecController.currentFilter['customer_preview'],
                    label: "Pelanggan",
                    hintText: "Pilih pelanggan",
                    asyncItems: (String filter) async {
                      projecController.lookupCustomer(searching: filter);
                      return projecController.dataCustomers;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      // print(data);
                      projecController.currentFilter.value = {
                        ...projecController.currentFilter,
                        'customer_preview': searchValue,
                      };
                    },
                  ),
                  FormBuilderChoiceChip<String>(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      labelText: 'Status',
                      hintText: "Pilih Status",
                      contentPadding: const EdgeInsets.symmetric(vertical: 2),
                      labelStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 15,
                        kfontWeight: FontWeight.w500,
                        kcolor: AppColor.kPrimaryColor,
                      ),
                      errorStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 13,
                        kcolor: AppColor.kFailedColor,
                        kfontWeight: FontWeight.w500,
                      ),
                    ),
                    name: 'active',
                    initialValue: projecController.currentFilter['active'],
                    selectedColor: AppColor.kInactiveColor,
                    spacing: 6.0,
                    options: List.generate(isActive.length, (index) {
                      return FormBuilderChipOption(
                        value: isActive[index],
                        child: Text(
                          isActive[index],
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontWeight: FontWeight.w600,
                            kfontSize: 12,
                          ),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    final listCardContent = Obx(
      () {
        final result = projecController.projectData;
        return Container(
          margin: const EdgeInsets.only(top: 10),
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: result.isEmpty && projecController.isLoading.isFalse
              ? SizedBox(
                  height: MediaQuery.of(context).size.height / 1.2,
                  child: Center(
                    child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                  ),
                )
              : ListView.separated(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: result.length + 1,
                  separatorBuilder: (BuildContext context, int index) =>
                      const SizedBox(height: 2),
                  itemBuilder: (BuildContext context, int index) {
                    if (index < result.length) {
                      final data = result[index];
                      var colorBadge = BaseGlobalFuntion.statusColor(
                          result[index]['active']);

                      return BaseCardWidget.cardStyle1(
                          ontap: () {
                            Get.to(() => ProjectView(itemId: data['id']));
                          },
                          title: data['name'],
                          trailing: Text.rich(
                            textAlign: TextAlign.right,
                            TextSpan(children: [
                              BaseTextWidget.customTextSpan(
                                text: data['distance'],
                                extendText: '\n',
                                color: AppColor.kPrimaryColor,
                                letterSpacing: 0.8,
                                fontSize: 13,
                                fontWeight: FontWeight.w600,
                              ),
                              BaseTextWidget.customTextSpan(
                                text: "KM",
                                color: AppColor.kBlackColor.withOpacity(0.5),
                                letterSpacing: 0.8,
                                fontSize: 13,
                                fontWeight: FontWeight.w700,
                              ),
                            ]),
                          ),
                          subTitle1: data['customer_name'],
                          subTitle2: BaseTextWidget.customText(
                            text: data['code'],
                            fontSize: 12,
                            isLongText: true,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                          statusHeader: {
                            'title':
                                data['active'] == "1" ? 'Aktif' : 'Non Aktif',
                            'color': colorBadge['background_color'],
                            'titleColor': colorBadge['text_color'],
                          });
                    } else if (result.length < 25) {
                      return projecController.hasMore.value &&
                              projecController.isLoading.isTrue
                          ? BaseLoadingWidget.cardShimmer(
                              height: MediaQuery.of(context).size.height,
                              shimmerheight: 80,
                              baseColor: AppColor.kSoftGreyColor,
                              highlightColor: AppColor.kWhiteColor,
                              itemCount: 25,
                            )
                          : const SizedBox();
                    } else {
                      return projecController.hasMore.value
                          ? BaseLoadingWidget.cardShimmer(
                              height: 200,
                              shimmerheight: 90,
                              baseColor: AppColor.kSoftGreyColor,
                              highlightColor: AppColor.kWhiteColor,
                              itemCount: 2,
                            )
                          : const SizedBox();
                    }
                  },
                ),
        );
      },
    );

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
            title: 'Proyek',
            context: context,
            isAllFill: true,
            searchController: projecController.search,
            searchText: 'Cari',
            onFillterChange: () {
              showBottomFilter(context);
            },
            onSearchChange: () {
              BaseGlobalFuntion.debounceRun(
                action: () {
                  projecController.refreshDataProject();
                },
                duration: const Duration(milliseconds: 1000),
              );
            }),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              RefreshIndicator(
                onRefresh: onRefresh,
                child: SingleChildScrollView(
                  controller: scrollController,
                  physics: const AlwaysScrollableScrollPhysics(),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      listCardContent,
                    ],
                  ),
                ),
              ),
              // contentAppBar,
              Positioned(
                bottom: 30.0,
                right: 15.0,
                child: SizedBox(
                  height: 55.0,
                  width: 55.0,
                  child: IconButton.filled(
                    iconSize: 30,
                    onPressed: () {
                      Get.to(() => const AddProject());
                    },
                    icon: const Icon(
                      Icons.add,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
