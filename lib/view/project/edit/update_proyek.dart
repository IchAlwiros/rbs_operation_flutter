import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rbs_mobile_operation/core/components/regional_maps_picker.dart';
import 'package:rbs_mobile_operation/data/datasources/global/helper_maps.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';
import 'package:rbs_mobile_operation/data/datasources/project.controller.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class EditProyek extends StatelessWidget {
  final dynamic detailData;
  const EditProyek({required this.detailData, super.key});

  @override
  Widget build(BuildContext context) {
    EditProjectControllers editProjectControllers =
        Get.put(EditProjectControllers());

    HelperMapsControllers helperMapsControllers =
        Get.put(HelperMapsControllers());

    var status = [
      {'text': 'Aktif', 'value': '1', 'isActive': true},
      {'text': 'Tidak Aktif', 'value': '0', 'isActive': false},
    ].obs;
    var isTriggerOther = false.obs;

    if (detailData['active'] != null) {
      for (var obj in status) {
        if (obj['value'].toString() == detailData['active']) {
          obj['isActive'] = true;
        } else {
          obj['isActive'] = false;
        }
      }
    }

    if (detailData['address'] != null) {
      helperMapsControllers.detailAddressControllers.text =
          detailData['address'];
    }

    if (ApiService.productionUnitGlobal == null ||
        LocalDataSource.getLocalVariable(key: 'lastestProduction') != null) {
      editProjectControllers.currentFilter(
          {'production_unit_id': detailData['production_unit_id']});
      editProjectControllers.lookupCustomer();
    }

    final submitAction = Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Obx(() => Expanded(
                  child: Button.filled(
                    onPressed: () => editProjectControllers.updateProject(
                      loadingCtx: context,
                      currentData: detailData,
                      addressMaps: helperMapsControllers.dataMapProjectHelper,
                    ),
                    // isLoading: editProjectControllers.isLoading.value,
                    disabled: editProjectControllers.isLoading.value,
                    color: AppColor.kPrimaryColor,
                    // height: 55,
                    icon: const Icon(
                      Icons.save,
                      color: AppColor.kWhiteColor,
                    ),
                    // textStyle: BaseTextStyle.customTextStyle(
                    //   kfontWeight: FontWeight.w600,
                    //   kfontSize: 12,
                    //   kcolor: AppColor.kWhiteColor,
                    // ),
                    label: "Simpan Perubahan",
                  ),
                )),
          ],
        ),
      ),
    );

    final topContent = Container(
      height: 190,
      width: double.infinity,
      padding: const EdgeInsets.only(right: 20, left: 20, bottom: 20),
      decoration: const BoxDecoration(
        color: AppColor.kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
      ),
    );

    final detailPayment = Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      margin: const EdgeInsets.only(right: 10, left: 10, top: 10, bottom: 10),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: AppColor.kGreyColor),
      ),
      child: Column(
        children: [
          FormBuilder(
            key: editProjectControllers.fkEditProyek,
            onChanged: () {
              editProjectControllers.fkEditProyek.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Obx(() => editProjectControllers.dataLookupPU.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih unit produksi',
                          label: "Unit Produksi",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib dipilih!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'production_unit_id',
                          isRequired: true,
                          enabled: false,
                          controller:
                              editProjectControllers.productionUnitController,
                          initialValue: {
                            'id': detailData['production_unit_id'],
                            'name': detailData['production_unit_name']
                          },
                          label: "Unit Produksi",
                          hintText: "Pilih Unit Produksi",
                          asyncItems: (String filter) async {
                            editProjectControllers.lookupProductionUnit(
                                searching: filter);
                            return editProjectControllers.dataLookupPU;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            editProjectControllers.currentFilter(
                                {"production_unit_id": valueItem});

                            editProjectControllers.lookupCustomer();
                            editProjectControllers.dataLookupCustomer.clear();
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib dipilih!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  Obx(
                    () => editProjectControllers.dataLookupCustomer.isEmpty
                        ? BaseTextFieldWidget.disableTextForm2(
                            disabledText: 'Pilih Pelanggan',
                            label: "Pelanggan",
                            isRequired: true,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Pelanggan wajib dipilih!';
                              }
                              return null;
                            },
                          )
                        : CustomDropDownSearch(
                            fieldName: 'customer_id',
                            isRequired: true,
                            enabled: false,
                            controller:
                                editProjectControllers.customerController,
                            initialValue: {
                              'id': detailData['customer_id'],
                              'name': detailData['customer_name']
                            },
                            label: "Pelanggan",
                            hintText: "Pilih Pelanggan",
                            asyncItems: (String filter) async {
                              editProjectControllers.lookupCustomer(
                                  searching: filter);
                              return editProjectControllers.dataLookupCustomer;
                            },
                            onChange: ({searchValue, valueItem, data}) {},
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Pelanggan wajib dipilih!';
                              }
                              return null;
                            },
                          ),
                  ),

                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Nama Proyek',
                    hintText: "Ketik Nama Proyek disini..",
                    fieldName: 'project_name',
                    initialValue: detailData['name'],
                    isRequired: true,
                    onClearText: () => editProjectControllers
                        .fkEditProyek.currentState!
                        .patchValue({'project_name': null}),
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Nama proyek wajib diisi!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                    inputFormater: <TextInputFormatter>[
                      // FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]')),
                      // Formatter untuk mengubah input menjadi huruf kapital
                      UppercaseInputFormatter(),
                    ],
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Nama PIC',
                    hintText: "Ketik Nama PIC disini..",
                    fieldName: 'project_pic',
                    initialValue: detailData['pic_name'],
                    isRequired: true,
                    onClearText: () => editProjectControllers
                        .fkEditProyek.currentState!
                        .patchValue({'project_pic': null}),
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Nama pic proyek wajib diisi!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                      title: 'No.Telp',
                      hintText: 'Ketik No.Telp disini..',
                      fieldName: 'phone',
                      initialValue: detailData['pic_phone'],
                      onChange: ({value}) {},
                      onClearText: () => editProjectControllers
                          .fkEditProyek.currentState!
                          .patchValue({'phone': null}),
                      validator: (value) {
                        return null;
                      },
                      keyboardType: TextInputType.phone,
                      textInputAction: TextInputAction.next,
                      inputFormater: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                        // for version 2 and greater youcan also use this
                        FilteringTextInputFormatter.digitsOnly
                      ]),
                  const SizedBox(height: 5),

                  Obx(() {
                    isTriggerOther.isTrue;
                    return BaseTextFieldWidget.selectFieldFormBuilder(
                      title: "Status",
                      fieldName: "active",
                      isRequired: true,
                      dataSelect: status,
                      initialValue: detailData['active'],
                      onChanged: (value) {
                        isTriggerOther.value = !isTriggerOther.value;
                        for (var obj in status) {
                          if (obj['value'].toString() == value) {
                            obj['isActive'] = true;
                          } else {
                            obj['isActive'] = false;
                          }
                        }
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Status wajib dipilih!';
                        }
                        return null;
                      },
                    );
                  }),
                  // FormBuilderChoiceChip<String>(
                  //   autovalidateMode: AutovalidateMode.onUserInteraction,
                  //   decoration: InputDecoration(
                  //     labelText: 'Status',
                  //     labelStyle: BaseTextStyle.customTextStyle(
                  //       kfontSize: 14,
                  //       kfontWeight: FontWeight.w500,
                  //     ),
                  //     errorStyle: BaseTextStyle.customTextStyle(
                  //       kfontSize: 12,
                  //       kcolor: AppColor.kFailedColor,
                  //       kfontWeight: FontWeight.w500,
                  //     ),
                  //   ),
                  //   name: 'active',
                  //   initialValue: "Aktif",
                  //   selectedColor: AppColor.kInactiveColor,
                  //   spacing: 6.0,
                  //   options: List.generate(status.length, (index) {
                  //     return FormBuilderChipOption(
                  //       avatar: const CircleAvatar(),
                  //       value: status[index],
                  //       child: Text(
                  //         status[index],
                  //         style: BaseTextStyle.customTextStyle(
                  //           kcolor: AppColor.kPrimaryColor,
                  //           kfontWeight: FontWeight.w600,
                  //           kfontSize: 12,
                  //         ),
                  //       ),
                  //     );
                  //   }),
                  //   // onChanged: _onChanged,
                  //   validator: (value) {
                  //     if (value == null || value.isEmpty) {
                  //       return 'status wajib diisi!';
                  //     }
                  //     return null;
                  //   },
                  // ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Alamat Lengkap',
                    fieldName: 'address',
                    hintText: 'Pilih Alamat',
                    isRequired: true,
                    readOnly: true,
                    isLongText: true,
                    onClearText: () => editProjectControllers
                        .fkEditProyek.currentState!
                        .patchValue({'address': null}),
                    controller: helperMapsControllers.detailAddressControllers,
                    // initialValue: detailData['address'],
                    onTap: () async {
                      // showBottomGoogleMap(context);
                      // BaseExtendWidget.mapsPicker(
                      //   context: context,
                      //   mapsHelper: helperMapsControllers.helperMaps,
                      //   detailMapsControllers:
                      //       helperMapsControllers.detailAddressControllers,
                      //   collectMapsData:
                      //       helperMapsControllers.dataMapProjectHelper,
                      // );

                      // showAboutDialog(context: context);

                      Get.to(
                        () => GMapsPicker(
                          // position: currentPosition!,
                          mapsHelper: helperMapsControllers.helperMaps,
                          detailMapsControllers:
                              helperMapsControllers.detailAddressControllers,
                          collectMapsData:
                              helperMapsControllers.dataMapProjectHelper,
                        ),
                      );
                    },
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Alamat wajib dipilih!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.done,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        title: 'Edit Proyek',
        centerTitle: true,
        context: context,
        extendBack: () {
          // salesController.dataLookupCustomerProject.clear();
          // salesController.dataDetailCustomer.clear();
        },
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            topContent,
            const SizedBox(height: 10),
            detailPayment,
          ],
        ),
      ),
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: submitAction,
    );
  }
}
