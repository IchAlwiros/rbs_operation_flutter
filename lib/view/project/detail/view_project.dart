import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rbs_mobile_operation/data/datasources/project.controller.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/project/edit/update_proyek.dart';

class ProjectView extends StatelessWidget {
  final int itemId;

  const ProjectView({
    required this.itemId,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    DetailsProjectControllers proyekControllers =
        Get.put(DetailsProjectControllers());

    // Future.delayed(const Duration(milliseconds: 200), () {
    proyekControllers.viewdetailProject(itemId);
    // });

    final Completer<GoogleMapController> _controller =
        Completer<GoogleMapController>();

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Detail Proyek',
          centerTitle: true,
          context: context,
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          child: Button.filled(
            onPressed: () {
              Get.to(() => EditProyek(
                    detailData: proyekControllers.detailProject,
                  ));
            },
            color: AppColor.kPrimaryColor,
            // height: 50,
            icon: const Icon(
              Icons.edit_note_rounded,
              color: AppColor.kWhiteColor,
            ),
            // textStyle: BaseTextStyle.customTextStyle(
            //   kfontWeight: FontWeight.w600,
            //   kfontSize: 12,
            //   kcolor: AppColor.kWhiteColor,
            // ),
            label: "Edit Proyek",
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: double.infinity,
                padding:
                    const EdgeInsets.symmetric(horizontal: 18, vertical: 15),
                decoration: const BoxDecoration(
                  color: AppColor.kPrimaryColor,
                ),
                child: Obx(() {
                  if (proyekControllers.isLoading.isFalse) {
                    var data = proyekControllers.detailProject;
                    var colorBadge =
                        BaseGlobalFuntion.statusColor(data['active']);

                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                padding: const EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                  color: AppColor.kWhiteColor,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: const Icon(
                                  Icons.info_rounded,
                                ),
                              ),
                              const SizedBox(width: 10),
                              Flexible(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    BaseTextWidget.customText(
                                      text: data['name'],
                                      color: AppColor.kWhiteColor,
                                      fontSize: 13,
                                      fontWeight: FontWeight.w700,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      isLongText: true,
                                    ),
                                    BaseTextWidget.customText(
                                      text: data['code'],
                                      color: AppColor.kWhiteColor,
                                      fontSize: 13,
                                      fontWeight: FontWeight.w500,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      isLongText: true,
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(width: 5)
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 2, horizontal: 6),
                          decoration: BoxDecoration(
                            color: colorBadge['background_color'],
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Text(
                            "${BaseGlobalFuntion.statusCheck(data['active'])}",
                            style: BaseTextStyle.customTextStyle(
                              kfontSize: 11,
                              kcolor: colorBadge['text_color'],
                            ),
                          ),
                        )
                      ],
                    );
                  } else {
                    return Shimmer.fromColors(
                      baseColor: AppColor.kSoftGreyColor,
                      highlightColor: AppColor.kWhiteColor,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: 40,
                            height: 40,
                            decoration: BoxDecoration(
                              color: Colors.amber,
                              borderRadius: BorderRadius.circular(8),
                            ),
                          ),
                          const SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 150,
                                height: 15,
                                decoration: BoxDecoration(
                                  color: Colors.amber,
                                  borderRadius: BorderRadius.circular(4),
                                ),
                              ),
                              const SizedBox(height: 10),
                              Container(
                                width: 200,
                                height: 15,
                                decoration: BoxDecoration(
                                  color: Colors.amber,
                                  borderRadius: BorderRadius.circular(4),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    );
                  }
                }),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    Obx(() {
                      var data = proyekControllers.detailProject;

                      return BaseCardWidget.detailInfoCard2(
                        isLoading: proyekControllers.isLoading.value,
                        contentList: [
                          {
                            'title': 'Unit Produksi',
                            'content': BaseTextWidget.customText(
                              text: proyekControllers
                                  .detailProject['production_unit_name'],
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              maxLengthText: 35,
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.factory_rounded,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'Pelanggan',
                            'content': BaseTextWidget.customText(
                              text: data['customer_name'],
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              maxLengthText: 35,
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.person_3,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'Nama PIC',
                            'content': BaseTextWidget.customText(
                              text: data['pic_name'],
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              maxLengthText: 35,
                              isLongText: true,
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.person_4_outlined,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'No.Telp PIC',
                            'content': BaseTextWidget.customText(
                              text: data['pic_phone'],
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              maxLengthText: 35,
                              isLongText: true,
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.phone_android_outlined,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                          {
                            'title': 'Alamat',
                            'content': Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                BaseTextWidget.customText(
                                  text: data['address'],
                                  color: AppColor.kBlackColor,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500,
                                  maxLengthText: 100,
                                  isLongText: true,
                                ),

                                // TANPILAN ALAMAT VIEW BOTTOM MODAL
                                // GestureDetector(
                                //   onTap: () {
                                //     showModalBottomSheet(
                                //       context: context,
                                //       builder: (context) {
                                //         return GoogleMap(
                                //           mapType: MapType.normal,
                                //           initialCameraPosition: kGooglePlex,
                                //           onMapCreated: (GoogleMapController
                                //               controller) async {
                                //             if (!_controller.isCompleted) {
                                //               _controller.complete(controller);
                                //             }
                                //           },
                                //           // gestureRecognizers: Set()
                                //           //   ..add(Factory<EagerGestureRecognizer>(
                                //           //       () => EagerGestureRecognizer())),
                                //           markers: {
                                //             Marker(
                                //               onTap: () => goToTheLake(),
                                //               markerId:
                                //                   const MarkerId('proyek_location'),
                                //               position: position,
                                //               infoWindow: InfoWindow(
                                //                 title: proyekControllers
                                //                     .detailProject['name'],
                                //                 snippet: proyekControllers
                                //                     .detailProject['address'],
                                //               ),
                                //             )
                                //           },
                                //         );
                                //       },
                                //     );
                                //   },
                                //   child: BaseTextWidget.customText(
                                //     text: 'detail',
                                //     color: AppColor.kSoftBlueColor,
                                //     fontSize: 13,
                                //     fontWeight: FontWeight.w400,
                                //     maxLengthText: 35,
                                //     isLongText: true,
                                //   ),
                                // ),
                              ],
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.location_on_rounded,
                                  size: 20,
                                  color: AppColor.kPrimaryColor,
                                ))
                          },
                        ],
                      );
                    }),

                    //

                    Container(
                      height: 200,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 10),
                      decoration: BoxDecoration(
                        color: AppColor.kWhiteColor,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(8)),
                        border: Border.all(color: AppColor.kAvailableColor),
                      ),
                      child: Obx(() {
                        var data = proyekControllers.detailProject;
                        var position = LatLng(
                          double.parse(data['latitude'] ?? '0.0'),
                          double.parse(data['longitude'] ?? '0.0'),
                        );
                        CameraPosition kGooglePlex =
                            CameraPosition(target: position, zoom: 14);

                        Future<void> goToTheLake() async {
                          final GoogleMapController controller =
                              await _controller.future;
                          await controller.animateCamera(
                              CameraUpdate.newCameraPosition(CameraPosition(
                            target: position,
                            zoom: 18.000,
                          )));
                        }

                        return data.isEmpty
                            ? const SpinKitThreeBounce(
                                size: 30.0,
                                color: AppColor.kPrimaryColor,
                              )
                            : ClipRRect(
                                borderRadius: BorderRadius.circular(8),
                                child: GoogleMap(
                                  mapType: MapType.normal,
                                  initialCameraPosition: kGooglePlex,
                                  onMapCreated:
                                      (GoogleMapController controller) async {
                                    if (!_controller.isCompleted) {
                                      _controller.complete(controller);
                                    }
                                  },
                                  // gestureRecognizers: Set()
                                  //   ..add(Factory<EagerGestureRecognizer>(
                                  //       () => EagerGestureRecognizer())),
                                  markers: {
                                    Marker(
                                      onTap: () => goToTheLake(),
                                      markerId:
                                          const MarkerId('proyek_location'),
                                      position: position,
                                      infoWindow: InfoWindow(
                                        title: proyekControllers
                                            .detailProject['name'],
                                        snippet: proyekControllers
                                            .detailProject['address'],
                                      ),
                                    )
                                  },
                                ),
                              );
                      }),
                    ),
                  ],
                ),
              ),

              const SizedBox(height: 10),

              // EDIT BUTTON
              // Positioned(
              //   bottom: 30.0,
              //   right: 15.0,
              //   child: SizedBox(
              //     height: 55.0,
              //     width: 55.0,
              //     child: IconButton.filled(
              //       padding: const EdgeInsets.all(5),
              //       iconSize: 20,
              //       color: AppColor.kYellowColor,
              //       onPressed: () {
              //         // Get.to(() => const AddProject());
              //       },
              //       icon: const Icon(
              //         Icons.edit_note_sharp,
              //       ),
              //     ),
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
