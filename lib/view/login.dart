import 'dart:ui';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/data/datasources/auth.controllers.dart';
import 'package:rbs_mobile_operation/view/forgot_password/forgot_password.dart';
import '../core/extentions/animation_fade_in_slide.dart';

class SignInView extends StatelessWidget {
  const SignInView({super.key});

  // ValueNotifier<bool> termsCheck = ValueNotifier(false);

  @override
  Widget build(BuildContext context) {
    var bottom = MediaQuery.of(context).viewInsets.bottom;

    final height = MediaQuery.sizeOf(context).height;
    // final isDark = MediaQuery.platformBrightnessOf(context) == Brightness.dark;
    final AuthControllers authControllers = Get.put(AuthControllers());

    var usernameHasError = false.obs;
    var passwordHasError = false.obs;

    // return Scaffold(
    //   resizeToAvoidBottomInset: false,
    //   // appBar: AppBar(
    //   //   elevation: 0,
    //   //   backgroundColor: AppColor.kWhiteColor,
    //   // ),
    //   body: SafeArea(
    //     child: FormBuilder(
    //       key: authControllers.authFormKey,
    //       onChanged: () {
    //         authControllers.authFormKey.currentState!.save();
    //         debugPrint(
    //           authControllers.authFormKey.currentState!.value.toString(),
    //         );
    //       },
    //       autovalidateMode: AutovalidateMode.disabled,
    //       skipDisabled: true,
    //       child: Stack(
    //         children: [
    //           Positioned(
    //               right: -80,
    //               top: -25,
    //               child: Container(
    //                 width: 200,
    //                 height: 200,
    //                 padding: const EdgeInsets.all(20),
    //                 // color: AppColor.kPrimaryColor,
    //                 child: ImageFiltered(
    //                   imageFilter: ImageFilter.blur(sigmaX: 6, sigmaY: 6),
    //                   child: Assets.icons.iconLogoSvg.svg(height: 50),
    //                   //  SvgPicture.asset(
    //                   //   'assets/icons/icon_logo.svg',
    //                   //   width: 20,
    //                   //   height: 20,
    //                   //   color: AppColor.kPrimaryColor,
    //                   // ),
    //                 ),
    //               )),
    //           Positioned(
    //               // left: 12,
    //               top: -250,
    //               left: -90,
    //               // bottom: -20,
    //               child: Container(
    //                 child: Assets.images.gradientBlue.image(),
    //               )),
    //           Positioned(
    //             left: -80,
    //             bottom: -20,
    //             child: Container(
    //               child: PhysicalShape(
    //                 color: AppColor.kSoftBlueColor.withOpacity(0.5),
    //                 shadowColor: const Color.fromARGB(255, 109, 162, 236),
    //                 elevation: 18,
    //                 clipper: const ShapeBorderClipper(shape: CircleBorder()),
    //                 child: const SizedBox(
    //                   height: 150,
    //                   width: 150,
    //                 ),
    //               ),
    //             ),
    //           ),
    //           Obx(
    //             () => ListView(
    //                 padding: const EdgeInsets.symmetric(
    //                   horizontal: 20,
    //                   vertical: 75,
    //                 ),
    //                 children: [
    //                   // Container(
    //                   //   padding: const EdgeInsets.all(5),
    //                   //   margin: const EdgeInsets.symmetric(horizontal: 100),
    //                   //   decoration: BoxDecoration(
    //                   //     borderRadius: BorderRadius.circular(10),
    //                   //     color: AppColor.kPrimaryColor,
    //                   //   ),
    //                   //   child: Assets.icons.appLogo.image(height: 100),
    //                   // ),

    //                   FadeInSlide(
    //                     duration: 1.2,
    //                     child: BaseTextWidget.customText(
    //                       text: "Selamat Datang! 👋",
    //                       fontSize: 20,
    //                       fontWeight: FontWeight.bold,
    //                     ),
    //                   ),
    //                   const SizedBox(height: 10),
    //                   FadeInSlide(
    //                     duration: 1.2,
    //                     child: BaseTextWidget.customText(
    //                         text: "Masuk untuk melanjutkan"),
    //                   ),
    //                   const SizedBox(height: 25),

    //                   FadeInSlide(
    //                     duration: 1.2,
    //                     child: CustomTextField.style1(
    //                       title: 'Username/Email',
    //                       fieldName: 'username',
    //                       hintText: "pengguna@rbs.com",
    //                       isRequired: true,
    //                       onClearText: () => authControllers
    //                           .authFormKey.currentState!
    //                           .patchValue({'username': null}),

    //                       // inputDecoration: InputDecoration(
    //                       //   filled: true,
    //                       //   fillColor: Colors.blueGrey.withOpacity(.1),
    //                       //   hintText: "Email",
    //                       //   border: OutlineInputBorder(
    //                       //     borderRadius: BorderRadius.circular(15),
    //                       //     borderSide: BorderSide.none,
    //                       //   ),
    //                       // ),
    //                       suffixOnError: usernameHasError.value,
    //                       onChange: ({value}) {
    //                         usernameHasError.value = !(authControllers
    //                                 .authFormKey
    //                                 .currentState
    //                                 ?.fields['username']
    //                                 ?.validate() ??
    //                             false);
    //                       },
    //                       validator: (value) {
    //                         if (value == null || value.isEmpty) {
    //                           return 'username wajib diisi!';
    //                         }

    //                         return null;
    //                       },
    //                       keyboardType: TextInputType.text,
    //                       textInputAction: TextInputAction.next,
    //                     ),
    //                   ),

    //                   const SizedBox(height: 5),
    //                   FadeInSlide(
    //                     duration: 1.2,
    //                     child: CustomTextField.style1(
    //                       title: 'Password',
    //                       fieldName: 'password',
    //                       hintText: "*******",
    //                       onSubmitted: (p0) async {
    //                         await authControllers.loginAuth();
    //                       },
    //                       isRequired: true,
    //                       secureViewText: true,
    //                       suffixOnError: passwordHasError.value,
    //                       onChange: ({value}) {
    //                         passwordHasError.value = !(authControllers
    //                                 .authFormKey
    //                                 .currentState
    //                                 ?.fields['password']
    //                                 ?.validate() ??
    //                             false);
    //                       },
    //                       validator: (value) {
    //                         if (value == null || value.isEmpty) {
    //                           return 'password wajib diisi!';
    //                         }

    //                         return null;
    //                       },
    //                       keyboardType: TextInputType.visiblePassword,
    //                       textInputAction: TextInputAction.done,
    //                     ),
    //                   ),
    //                   const SizedBox(height: 20),

    //                   FadeInSlide(
    //                     duration: 1.2,
    //                     child: Row(
    //                       children: [
    //                         const Spacer(),
    //                         TextButton(
    //                           onPressed: () {
    //                             // context.push(AppRoutes.forgotPassword.path);
    //                             // Get.toNamed('forgot-password');
    //                             Get.to(() => const ForgotPassword());
    //                           },
    //                           child: Text(
    //                             "Lupa Password?",
    //                             style: BaseTextStyle.customTextStyle(
    //                               kfontSize: 13,
    //                               kfontWeight: FontWeight.w500,
    //                               kcolor: AppColor.kPrimaryColor,
    //                             ),
    //                           ),
    //                         ),
    //                       ],
    //                     ),
    //                   ),

    //                   Obx(
    //                     () => FadeInSlide(
    //                       duration: 1,
    //                       // direction: FadeSlideDirection.,
    //                       child: Button.filled(
    //                         onPressed: () async {
    //                           await authControllers.loginAuth();
    //                         },
    //                         // isLoading: authControllers.isLoading.value,
    //                         label: "Login",
    //                         height: 55,
    //                         color: AppColor.kPrimaryColor,
    //                         disabled: authControllers.isLoading.value,
    //                         icon: const Icon(
    //                           Icons.login_rounded,
    //                           color: AppColor.kWhiteColor,
    //                         ),
    //                       ),
    //                     ),
    //                   ),
    //                   // FadeInSlide(
    //                   //   duration: 1,
    //                   //   direction: FadeSlideDirection.btt,
    //                   //   child: Padding(
    //                   //     padding: EdgeInsets.only(
    //                   //       bottom: bottom,
    //                   //     ),
    //                   //     child: Container(
    //                   //       padding: const EdgeInsets.symmetric(
    //                   //           vertical: 10, horizontal: 8),
    //                   //       decoration: const BoxDecoration(
    //                   //         border: Border(
    //                   //           top: BorderSide(width: .2, color: Colors.grey),
    //                   //         ),
    //                   //       ),
    //                   //       child: Obx(
    //                   //         () => Button.filled(
    //                   //           onPressed: () async {
    //                   //             await authControllers.loginAuth();
    //                   //           },
    //                   //           // isLoading: authControllers.isLoading.value,
    //                   //           label: "Login",
    //                   //           height: 55,
    //                   //           color: AppColor.kPrimaryColor,
    //                   //           disabled: authControllers.isLoading.value,
    //                   //           icon: const Icon(
    //                   //             Icons.login_rounded,
    //                   //             color: AppColor.kWhiteColor,
    //                   //           ),
    //                   //         ),
    //                   //       ),
    //                   //     ),
    //                   //   ),
    //                   // ),
    //                   const SizedBox(height: 20),
    //                   FadeInSlide(
    //                     duration: 1.2,
    //                     child: Row(
    //                       children: [
    //                         const Expanded(
    //                             child: Divider(
    //                           thickness: .3,
    //                         )),
    //                         Text("   atau   ",
    //                             style: BaseTextStyle.customTextStyle(
    //                               kfontSize: 12,
    //                               kfontWeight: FontWeight.w400,
    //                               kcolor: AppColor.kPrimaryColor,
    //                             )),
    //                         const Expanded(
    //                             child: Divider(
    //                           thickness: .3,
    //                         )),
    //                       ],
    //                     ),
    //                   ),

    //                   SizedBox(height: height * 0.02),
    //                   FadeInSlide(
    //                     duration: 1.2,
    //                     child: BaseButtonWidget.primaryIconButton(
    //                       onPressed: () async {
    //                         final user =
    //                             await authControllers.loginWithGoogle();

    //                         try {
    //                           if (user != null && context.mounted) {
    //                             // print(user);
    //                             // Navigator.pushReplacement(
    //                             //   context,
    //                             //   MaterialPageRoute(
    //                             //       builder: (context) =>
    //                             //           const ),
    //                             // );
    //                           }
    //                         } on FirebaseException catch (error) {
    //                           authControllers.isLoading.value = false;
    //                           if (kDebugMode) {
    //                             print("INI ERROR ON FIREBASE EXCEPTION");
    //                             print(error.message);
    //                           }
    //                           BaseInfo.log(
    //                             isError: true,
    //                             messageTitle: "Error",
    //                             message: "${error.message}",
    //                             snackPosition: SnackPosition.TOP,
    //                           );
    //                         } catch (e) {
    //                           authControllers.isLoading.value = false;
    //                           if (kDebugMode) {
    //                             print("INI ERROR ON CATCH");
    //                             print(e);
    //                           }
    //                           BaseInfo.log(
    //                             isError: true,
    //                             messageTitle: "Error",
    //                             message: "Error",
    //                             snackPosition: SnackPosition.TOP,
    //                           );
    //                         }
    //                       },
    //                       // isLoading: authControllers.isLoading.value,
    //                       isDisable: authControllers.isLoading.value,
    //                       title: "Masuk dengan Google",
    //                       height: 50,
    //                       width: double.infinity,
    //                       textStyle: BaseTextStyle.customTextStyle(
    //                         kcolor: AppColor.kPrimaryColor,
    //                         kfontSize: 12,
    //                         kfontWeight: FontWeight.w500,
    //                       ),
    //                       rounded: 25,
    //                       kcolor: AppColor.kWhiteColor,
    //                       assetIcon: Assets.icons.googleAuth.image(scale: 15),
    //                     ),
    //                   ),

    //                   SizedBox(height: height * 0.02),
    //                   // SizedBox(height: height * 0.02),
    //                   // FadeInSlide(
    //                   //   duration: 1.3,
    //                   //   child: LoginButton(
    //                   //     icon: Brand(Brands.twitter, size: 25),
    //                   //     text: "Continue with Twitter",
    //                   //     onPressed: () {},
    //                   //   ),
    //                   // ),
    //                 ]
    //                 // .animate(interval: const Duration(milliseconds: 10)).slide(
    //                 //       begin: const Offset(0, -40),
    //                 //       end: Offset.zero,
    //                 //       // curve: Curves.easeOut,
    //                 //       duration: const Duration(milliseconds: 2000),
    //                 //       delay: const Duration(milliseconds: 400),
    //                 //     ),
    //                 ),
    //           ),
    //         ],
    //       ),
    //     ),
    //   ),
    //   // bottomNavigationBar: FadeInSlide(
    //   //   duration: 1,
    //   //   direction: FadeSlideDirection.btt,
    //   //   child: Padding(
    //   //     padding: EdgeInsets.only(
    //   //       bottom: bottom,
    //   //     ),
    //   //     child: Container(
    //   //       padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 8),
    //   //       decoration: const BoxDecoration(
    //   //         border: Border(
    //   //           top: BorderSide(width: .2, color: Colors.grey),
    //   //         ),
    //   //       ),
    //   //       child: Obx(
    //   //         () => Button.filled(
    //   //           onPressed: () async {
    //   //             await authControllers.loginAuth();
    //   //           },
    //   //           // isLoading: authControllers.isLoading.value,
    //   //           label: "Login",
    //   //           height: 55,
    //   //           color: AppColor.kPrimaryColor,
    //   //           disabled: authControllers.isLoading.value,
    //   //           icon: const Icon(
    //   //             Icons.login_rounded,
    //   //             color: AppColor.kWhiteColor,
    //   //           ),
    //   //         ),
    //   //       ),
    //   //     ),
    //   //   ),
    //   // ),
    // );

    return Scaffold(
      backgroundColor: AppColor.kWhiteColor,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: Assets.images.gradientBlueTop.provider(),
            alignment: Alignment.topCenter,
          ),
        ),
        child: Stack(
          children: [
            // Positioned(
            //     // left: 12,
            //     top: -250,
            //     left: -90,
            //     // bottom: -20,
            //     child: Container(
            //       child: Assets.images.gradientBlueTop.image(),
            //     )),

            Positioned(
              left: -80,
              bottom: -20,
              child: SizedBox(
                child: PhysicalShape(
                  color: AppColor.kSoftBlueColor.withOpacity(0.5),
                  shadowColor: const Color.fromARGB(255, 109, 162, 236),
                  elevation: 18,
                  clipper: const ShapeBorderClipper(shape: CircleBorder()),
                  child: const SizedBox(
                    height: 150,
                    width: 150,
                  ),
                ),
              ),
            ),
            // SizedBox(
            //   height: 260.0,
            //   child: Center(
            //     child: Assets.icons.logo.image(height: 55.0),
            //   ),
            // ),
            Align(
              alignment: Alignment.bottomCenter,
              child: SingleChildScrollView(
                child: ClipRRect(
                  borderRadius:
                      const BorderRadius.vertical(top: Radius.circular(20.0)),
                  child: ColoredBox(
                    color: Colors.transparent,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 28.0),
                      child: FormBuilder(
                        key: authControllers.authFormKey,
                        onChanged: () {
                          authControllers.authFormKey.currentState!.save();
                          debugPrint(
                            authControllers.authFormKey.currentState!.value
                                .toString(),
                          );
                        },
                        autovalidateMode: AutovalidateMode.disabled,
                        skipDisabled: true,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            FadeInSlide(
                              duration: 1.2,
                              child: BaseTextWidget.customText(
                                text: "Selamat Datang! 👋",
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            const SizedBox(height: 10),
                            FadeInSlide(
                              duration: 1.2,
                              child: BaseTextWidget.customText(
                                  text: "Masuk untuk melanjutkan"),
                            ),
                            const SizedBox(height: 25),
                            FadeInSlide(
                              duration: 1.2,
                              child: CustomTextField.style1(
                                title: 'Username/Email',
                                fieldName: 'username',
                                hintText: "pengguna@rbs.com",
                                isRequired: true,
                                onClearText: () => authControllers
                                    .authFormKey.currentState!
                                    .patchValue({'username': null}),

                                // inputDecoration: InputDecoration(
                                //   filled: true,
                                //   fillColor: Colors.blueGrey.withOpacity(.1),
                                //   hintText: "Email",
                                //   border: OutlineInputBorder(
                                //     borderRadius: BorderRadius.circular(15),
                                //     borderSide: BorderSide.none,
                                //   ),
                                // ),
                                suffixOnError: usernameHasError.value,
                                onChange: ({value}) {
                                  usernameHasError.value = !(authControllers
                                          .authFormKey
                                          .currentState
                                          ?.fields['username']
                                          ?.validate() ??
                                      false);
                                },
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'username wajib diisi!';
                                  }

                                  return null;
                                },
                                keyboardType: TextInputType.text,
                                textInputAction: TextInputAction.next,
                              ),
                            ),

                            const SizedBox(height: 16.0),
                            FadeInSlide(
                              duration: 1.2,
                              child: CustomTextField.style1(
                                title: 'Password',
                                fieldName: 'password',
                                hintText: "*******",
                                onSubmitted: (p0) async {
                                  await authControllers.loginAuth();
                                },
                                isRequired: true,
                                secureViewText: true,
                                suffixOnError: passwordHasError.value,
                                onChange: ({value}) {
                                  passwordHasError.value = !(authControllers
                                          .authFormKey
                                          .currentState
                                          ?.fields['password']
                                          ?.validate() ??
                                      false);
                                },
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'password wajib diisi!';
                                  }

                                  return null;
                                },
                                keyboardType: TextInputType.visiblePassword,
                                textInputAction: TextInputAction.done,
                              ),
                            ),
                            const SizedBox(height: 26.0),
                            FadeInSlide(
                              duration: 1.2,
                              child: Row(
                                children: [
                                  const Spacer(),
                                  TextButton(
                                    onPressed: () {
                                      // context.push(AppRoutes.forgotPassword.path);
                                      // Get.toNamed('forgot-password');
                                      Get.to(() => const ForgotPassword());
                                    },
                                    child: Text(
                                      "Lupa Password?",
                                      style: BaseTextStyle.customTextStyle(
                                        kfontSize: 13,
                                        kfontWeight: FontWeight.w500,
                                        kcolor: AppColor.kPrimaryColor,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),

                            // const SpaceHeight(36.0),
                            // CustomTextField(
                            //   controller: passwordController,
                            //   label: 'Password',
                            //   isOutlineBorder: false,
                            //   obscureText: true,
                            // ),
                            const SizedBox(height: 26.0),
                            // const SpaceHeight(86.0),
                            // BlocListener<LoginBloc, LoginState>(
                            //   listener: (context, state) {
                            //     state.maybeWhen(
                            //       orElse: () {},
                            //       success: (data) {
                            //         context.pushReplacement(const MainPage());
                            //       },
                            //       error: (error) {
                            //         // print(error);
                            //         context.showErrorSnackbar(error);
                            //         // ScaffoldMessenger.of(context).showSnackBar(
                            //         //   SnackBar(
                            //         //     content: Text(error),
                            //         //     backgroundColor: Colors.red,
                            //         //   ),
                            //         // );
                            //       },
                            //     );
                            //   },
                            // child:
                            Obx(
                              () => FadeInSlide(
                                duration: 1,
                                // direction: FadeSlideDirection.,
                                child: Button.filled(
                                  onPressed: () async {
                                    await authControllers.loginAuth();
                                  },
                                  // isLoading: authControllers.isLoading.value,
                                  label: "Login",
                                  height: 55,
                                  color: AppColor.kPrimaryColor,
                                  disabled: authControllers.isLoading.value,
                                  icon: const Icon(
                                    Icons.login_rounded,
                                    color: AppColor.kWhiteColor,
                                  ),
                                ),
                              ),
                            ),
                            // ),
                            SizedBox(height: height * 0.02),

                            FadeInSlide(
                              duration: 1.2,
                              child: Row(
                                children: [
                                  const Expanded(
                                      child: Divider(
                                    thickness: .3,
                                  )),
                                  Text("   atau   ",
                                      style: BaseTextStyle.customTextStyle(
                                        kfontSize: 12,
                                        kfontWeight: FontWeight.w400,
                                        kcolor: AppColor.kPrimaryColor,
                                      )),
                                  const Expanded(
                                      child: Divider(
                                    thickness: .3,
                                  )),
                                ],
                              ),
                            ),

                            SizedBox(height: height * 0.02),
                            FadeInSlide(
                              duration: 1.2,
                              child: BaseButtonWidget.primaryIconButton(
                                onPressed: () async {
                                  final user =
                                      await authControllers.loginWithGoogle();

                                  try {
                                    if (user != null && context.mounted) {
                                      // print(user);
                                      // Navigator.pushReplacement(
                                      //   context,
                                      //   MaterialPageRoute(
                                      //       builder: (context) =>
                                      //           const ),
                                      // );
                                    }
                                  } on FirebaseException catch (error) {
                                    authControllers.isLoading.value = false;
                                    if (kDebugMode) {
                                      print("INI ERROR ON FIREBASE EXCEPTION");
                                      print(error.message);
                                    }
                                    BaseInfo.log(
                                      isError: true,
                                      messageTitle: "Error",
                                      message: "${error.message}",
                                      snackPosition: SnackPosition.TOP,
                                    );
                                  } catch (e) {
                                    authControllers.isLoading.value = false;
                                    if (kDebugMode) {
                                      print("INI ERROR ON CATCH");
                                      print(e);
                                    }
                                    BaseInfo.log(
                                      isError: true,
                                      messageTitle: "Error",
                                      message: "Error",
                                      snackPosition: SnackPosition.TOP,
                                    );
                                  }
                                },
                                // isLoading: authControllers.isLoading.value,
                                isDisable: authControllers.isLoading.value,
                                title: "Masuk dengan Google",
                                height: 50,
                                width: double.infinity,
                                textStyle: BaseTextStyle.customTextStyle(
                                  kcolor: AppColor.kPrimaryColor,
                                  kfontSize: 12,
                                  kfontWeight: FontWeight.w500,
                                ),
                                rounded: 25,
                                kcolor: AppColor.kWhiteColor,
                                assetIcon:
                                    Assets.icons.googleAuth.image(scale: 15),
                              ),
                            ),
                            const SizedBox(height: 118.0),
                            // Center(
                            //   child: Assets.icons.logo.image(height: 40.0),
                            // ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
