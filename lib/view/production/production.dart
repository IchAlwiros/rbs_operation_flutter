import 'package:rbs_mobile_operation/core/extentions/date_time_extension.dart';
import 'package:rbs_mobile_operation/data/datasources/production.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/app_view_resource.dart';

class ProductionList extends StatelessWidget {
  const ProductionList({super.key});

  @override
  Widget build(BuildContext context) {
    ProductionListControllers productionControllers =
        Get.put(ProductionListControllers());

    Future onRefresh() async {
      productionControllers.refreshDataProduction();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        if (productionControllers.productionData.isNotEmpty) {
          productionControllers.fetchProduction();
        }
      }
    });

    // ** MODAL SHEET FILTER
    TextEditingController customerFilterController = TextEditingController();
    TextEditingController productFilterController = TextEditingController();

    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          productionControllers.currentFilter.clear();
          productionControllers.filterFormKey.currentState!.reset();
          productionControllers.filterFormKey.currentState!.patchValue({
            'customer': null,
            'product_id': null,
            'date_range': null,
          });
        },
        onFilter: () {
          Get.back();
          productionControllers.refreshDataProduction();
        },
        valueFilter: productionControllers.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: productionControllers.filterFormKey,
            onChanged: () {
              productionControllers.filterFormKey.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'customer',
                    controller: customerFilterController,
                    currentValue:
                        productionControllers.currentFilter['customer_preview'],
                    label: "Pelanggan",
                    hintText: "Pilih pelanggan",
                    asyncItems: (String filter) async {
                      productionControllers.lookupCustomers(searching: filter);
                      return productionControllers.dataCustomers;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      // print(data);
                      productionControllers.currentFilter.value = {
                        ...productionControllers.currentFilter,
                        'customer_preview': searchValue,
                      };
                    },
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'product_id',
                    controller: productFilterController,
                    currentValue:
                        productionControllers.currentFilter['category_preview'],
                    label: "Kategori Produk",
                    hintText: "Pilih Kategori",
                    asyncItems: (String filter) async {
                      productionControllers.lookupProductCategory(
                          searching: filter);
                      return productionControllers.dataLookupProductCategory;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      productionControllers.currentFilter.value = {
                        ...productionControllers.currentFilter,
                        'category_preview': searchValue,
                      };
                    },
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.formDateRangePicker2(
                    context: context,
                    fieldName: 'date_range',
                    title: "Tanggal",
                    hintText: "Pilih tanggal",
                    currentValue: BaseGlobalFuntion.formatDateRange(
                      productionControllers.byDate['start-date'] ?? "",
                      productionControllers.byDate['end-date'] ?? "",
                    ),
                    startDateController: TextEditingController(),
                    endDateController: TextEditingController(),
                    onChange: ({endDate, startDate}) {
                      productionControllers.filterByRangeDate(
                        startDate: startDate,
                        endDate: endDate,
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    // ** LIST OF CONTENT CARD
    final listItemContent = Flexible(child: Obx(() {
      final result = productionControllers.productionData;
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: result.isEmpty && productionControllers.isLoading.isFalse
            ? SizedBox(
                height: MediaQuery.of(context).size.height / 1.2,
                child: Center(
                  child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                ),
              )
            : ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: result.length + 1,
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(height: 10),
                itemBuilder: (BuildContext context, int index) {
                  if (index < result.length) {
                    final data = result[index];
                    var colorBadge = BaseGlobalFuntion.statusColor(
                        data['production_status_code']);
                    return BaseCardWidget.cardStyle1(
                        ontap: () {
                          Get.to(
                            ProductionView(itemId: data['id']),
                          );
                        },
                        title: data['product_name'],
                        subTitle1: data['order_number'],
                        subTitle2: BaseTextWidget.customText(
                          text: data['customer_project_name'],
                          fontSize: 12,
                          isLongText: true,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                        headerString: DateTime.parse(data['production_date'])
                            .toFormattedDateTime(),
                        statusHeader: {
                          'title': data['production_status_code'] ?? "",
                          'color': colorBadge['background_color'],
                          'titleColor': colorBadge['text_color'],
                        });
                  } else if (result.length < 25) {
                    return productionControllers.hasMore.value &&
                            productionControllers.isLoading.isTrue
                        ? BaseLoadingWidget.cardShimmer(
                            height: MediaQuery.of(context).size.height,
                            shimmerheight: 80,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 25,
                          )
                        : const SizedBox();
                  } else {
                    return productionControllers.hasMore.value
                        ? BaseLoadingWidget.cardShimmer(
                            height: 200,
                            shimmerheight: 90,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 2,
                          )
                        : const SizedBox();
                  }
                },
              ),
      );
    }));

    Widget cardMonitoring({volume, send, waiting}) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          BaseCardWidget.cardMonitoring(
            title: "Volume",
            kcontentColor: AppColor.kPrimaryColor,
            content: BaseGlobalFuntion.currencyLocalConvert(
              nominal: volume,
              isLocalCurency: false,
            ),
          ),
          const SizedBox(width: 2),
          BaseCardWidget.cardMonitoring(
            title: "Dikirim",
            kcontentColor: AppColor.kSuccesColor,
            content: BaseGlobalFuntion.currencyLocalConvert(
              nominal: send,
              isLocalCurency: false,
            ),
          ),
          const SizedBox(width: 2),
          BaseCardWidget.cardMonitoring(
            title: "Belum dikirim",
            kcontentColor: AppColor.kFailedColor,
            content: BaseGlobalFuntion.currencyLocalConvert(
              nominal: waiting,
              isLocalCurency: false,
            ),
          ),
        ],
      );
    }

    Widget loadingCardSummary({itemCount}) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: List.generate(
          itemCount,
          (index) => Expanded(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 5),
              width: double.infinity,
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: AppColor.kWhiteColor,
                borderRadius: const BorderRadius.all(Radius.circular(8)),
                border: Border.all(color: AppColor.kAvailableColor),
              ),
              child: Shimmer.fromColors(
                  baseColor: AppColor.kSoftGreyColor,
                  highlightColor: AppColor.kWhiteColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 40,
                        height: 18,
                        decoration: BoxDecoration(
                          color: Colors.amber,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                      const SizedBox(height: 5),
                      Container(
                        width: 80,
                        height: 18,
                        decoration: BoxDecoration(
                          color: Colors.amber,
                          borderRadius: BorderRadius.circular(4),
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        ),
      );
    }

    final contentTop = Positioned(
      top: 15.0,
      left: 0.0,
      right: 0.0,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: Obx(() {
          final data = productionControllers.summaryProduction;
          if (productionControllers.productionData.isEmpty) {
            return productionControllers.hasMore.value
                ? loadingCardSummary(itemCount: 3)
                : cardMonitoring(
                    volume: "0",
                    send: "0",
                    waiting: "0",
                  );
          } else {
            return cardMonitoring(
              volume: data['quantity_total'],
              send: data['delivery'],
              waiting: data['production_cancel'],
            );
          }
        }),
      ),
    );

    final contentAppBar = SizedBox(
      height: 80,
      child: Stack(
        children: [
          Container(
            height: 50,
            width: double.infinity,
            padding: const EdgeInsets.only(left: 10, right: 10, top: 15),
            decoration: const BoxDecoration(
              color: AppColor.kPrimaryColor,
              gradient: RadialGradient(
                radius: 1.2,
                center: Alignment(0.2, 1.4),
                colors: [
                  AppColor.kSoftBlueColor,
                  AppColor.kPrimaryColor,
                ],
              ),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10),
              ),
            ),
          ),
          contentTop,
        ],
      ),
    );

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
            title: 'Realisasi Produksi',
            context: context,
            isAllFill: true,
            searchController: productionControllers.search,
            searchText: 'Cari',
            onFillterChange: () {
              showBottomFilter(context);
            },
            onSearchChange: () {
              BaseGlobalFuntion.debounceRun(
                action: () {
                  productionControllers.refreshDataProduction();
                },
                duration: const Duration(milliseconds: 1000),
              );
            }),
        body: Stack(
          children: [
            RefreshIndicator(
              onRefresh: onRefresh,
              child: SingleChildScrollView(
                controller: scrollController,
                physics: const AlwaysScrollableScrollPhysics(),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    contentAppBar,
                    const SizedBox(height: 10),
                    listItemContent,
                  ],
                ),
              ),
            ),
            // contentTop,
          ],
        ),
      ),
    );
  }
}
