import 'package:rbs_mobile_operation/core/core.dart';

class SegmentMaterial extends StatelessWidget {
  final dynamic dataMaterial;
  // final dynamic dataCustomer;

  const SegmentMaterial({
    super.key,
    required this.dataMaterial,
    // required this.dataCustomer,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
      child: SizedBox(
        height: double.infinity,
        child: Stack(
          children: [
            dataMaterial.isNotEmpty
                ? ListView.separated(
                    shrinkWrap: true,
                    itemCount: dataMaterial.length,
                    separatorBuilder: (BuildContext context, int index) =>
                        const SizedBox(height: 5),
                    itemBuilder: (BuildContext context, int index) {
                      // var colorBadge = BaseGlobalFuntion.statusColor(
                      //     dataProyek[index]['active']);

                      return BaseCardWidget.cardStyle4(
                        ontap: () {
                          // Get.to(
                          //   () => ProjectView(itemId: dataProyek[index]['id']),
                          // );
                        },
                        title: dataMaterial[index]['material_name'],
                        subtitle: dataMaterial[index]['warehouse_name'],
                        bottomSubtitle: Row(
                            // crossAxisAlignment:
                            //     CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <dynamic>[
                              {
                                'title': 'Target',
                                'content': dataMaterial[index]['actual'],
                                // .toStringAsFixed(2),
                                'colors': AppColor.kSuccesColor
                              },
                              {
                                'title': 'Aktual',
                                'content': dataMaterial[index]['target'],
                                // .toStringAsFixed(2),
                                'colors': AppColor.kPrimaryColor
                              },
                              {
                                'title': 'Error',
                                'content': dataMaterial[index]['error'],
                                // .toStringAsFixed(2),
                                'colors': AppColor.kWarningColor
                              }
                            ]
                                .map(
                                  (item) => Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      BaseTextWidget.customText(
                                        text: item['title'],
                                        color: AppColor.kPrimaryColor,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600,
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            width: 10,
                                            height: 10,
                                            margin:
                                                const EdgeInsets.only(right: 5),
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(19),
                                              color: item['colors'],
                                            ),
                                          ),
                                          BaseTextWidget.customText(
                                            text: item['content'],
                                            color: item['content'] < 0
                                                ? AppColor.kFailedColor
                                                : AppColor.kPrimaryColor,
                                            fontSize: 12,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                                .toList()),

                        // BaseTextWidget.customText(
                        //   text: dataMaterial[index]['actual'],
                        //   isLongText: true,
                        //   overflow: TextOverflow.ellipsis,
                        //   maxLines: 2,
                        // ),
                        // extendWidget: Container(
                        //   padding: const EdgeInsets.all(4),
                        //   decoration: BoxDecoration(
                        //     color: colorBadge['background_color'],
                        //     borderRadius: BorderRadius.circular(5),
                        //   ),
                        //   child: Text(
                        //     BaseGlobalFuntion.statusCheck(
                        //             dataProyek[index]['active']) ??
                        //         "",
                        //     style: BaseTextStyle.customTextStyle(
                        //       kfontSize: 12,
                        //       kcolor: colorBadge['text_color'],
                        //       kfontWeight: FontWeight.w500,
                        //     ),
                        //   ),
                        // ),
                      );
                    },
                  )
                : Center(
                    child: BaseLoadingWidget.noMoreDataWidget(),
                  ),
          ],
        ),
      ),
    );
  }
}
