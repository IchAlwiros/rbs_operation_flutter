import 'package:rbs_mobile_operation/core/extentions/date_time_extension.dart';
import 'package:rbs_mobile_operation/data/datasources/production.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/production/detail/segment_material.dart';

class ProductionView extends StatelessWidget {
  final int itemId;

  const ProductionView({
    required this.itemId,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    DetailsProductionControllers productionControllers =
        Get.put(DetailsProductionControllers());

    productionControllers.fetchViewProduction(itemId);

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Detail Realisasi Produksi',
          centerTitle: true,
          context: context,
        ),
        body: Column(
          children: [
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 15),
              decoration: const BoxDecoration(
                color: AppColor.kPrimaryColor,
                border: Border(
                  top: BorderSide(
                    color: AppColor.kPrimaryColor,
                  ),
                ),
              ),
              child: Obx(() {
                if (productionControllers.viewProductionData.isNotEmpty) {
                  var data = productionControllers.viewProductionData;

                  var dataProduction = data['production'];
                  var dataOrder = data['sales_order'];
                  var dataDelivery = data['delivery'];

                  var colorBadge = BaseGlobalFuntion.statusColor(
                      dataProduction['status_code']);

                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              padding: const EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                color: AppColor.kWhiteColor,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: const Icon(
                                Icons.info_rounded,
                              ),
                            ),
                            const SizedBox(width: 10),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  BaseTextWidget.customText(
                                    text: dataOrder['product_name'],
                                    color: AppColor.kWhiteColor,
                                    fontSize: 13,
                                    fontWeight: FontWeight.w700,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    isLongText: true,
                                  ),
                                  BaseTextWidget.customText(
                                    text: dataDelivery['docket_number'],
                                    color: AppColor.kWhiteColor,
                                    fontSize: 13,
                                    fontWeight: FontWeight.w500,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    isLongText: true,
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(width: 5),
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 2, horizontal: 6),
                        decoration: BoxDecoration(
                          color: colorBadge['background_color'],
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          "${dataProduction['status_code']}",
                          style: BaseTextStyle.customTextStyle(
                            kfontSize: 11,
                            kcolor: colorBadge['text_color'],
                          ),
                        ),
                      )
                    ],
                  );
                } else {
                  return Shimmer.fromColors(
                      baseColor: AppColor.kSoftGreyColor,
                      highlightColor: AppColor.kWhiteColor,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: 40,
                            height: 40,
                            decoration: BoxDecoration(
                              color: Colors.amber,
                              borderRadius: BorderRadius.circular(8),
                            ),
                          ),
                          const SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 150,
                                height: 15,
                                decoration: BoxDecoration(
                                  color: Colors.amber,
                                  borderRadius: BorderRadius.circular(4),
                                ),
                              ),
                              const SizedBox(height: 10),
                              Container(
                                width: 200,
                                height: 15,
                                decoration: BoxDecoration(
                                  color: Colors.amber,
                                  borderRadius: BorderRadius.circular(4),
                                ),
                              )
                            ],
                          )
                        ],
                      ));
                }
              }),
            ),
            // Obx(
            //   () => contentInfo(
            //     data: productionControllers.viewProductionData,
            //     isLoading: productionControllers.isLoading.value,
            //   ),
            // )
            Expanded(
              child: DefaultTabController(
                  length: 3,
                  child: Scaffold(
                    appBar: PreferredSize(
                      preferredSize: const Size.fromHeight(kToolbarHeight),
                      child: Container(
                        color: AppColor.kPrimaryColor,
                        child: SafeArea(
                          child: Column(
                            children: <Widget>[
                              Expanded(child: Container()),
                              TabBar(
                                indicatorColor: AppColor.kInactiveColor,
                                unselectedLabelStyle:
                                    BaseTextStyle.customTextStyle(
                                  kfontWeight: FontWeight.w400,
                                  kfontSize: 12,
                                ),
                                unselectedLabelColor: AppColor.kWhiteColor,
                                labelColor: AppColor.kInactiveColor,
                                labelStyle: BaseTextStyle.customTextStyle(
                                  kfontWeight: FontWeight.w700,
                                  kfontSize: 14,
                                ),
                                tabs: const [
                                  Tab(text: 'Detail Produksi'),
                                  Tab(text: 'Info Pengiriman'),
                                  Tab(text: 'Material'),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    body: TabBarView(
                      children: [
                        Obx(() => segmentTab1(
                            data: productionControllers.viewProductionData,
                            isLoading: productionControllers.isLoading.value,
                            context: context,
                            controllers: productionControllers)),
                        Obx(() => segmentTab2(
                            data: productionControllers.viewProductionData,
                            isLoading: productionControllers.isLoading.value,
                            context: context,
                            controllers: productionControllers)),
                        // Obx(
                        //   () =>
                        // RefreshIndicator(
                        //       onRefresh: onRefresh,
                        //       child:
                        SegmentMaterial(
                          dataMaterial: productionControllers.materialList,
                        ),
                        // )
                        // )
                      ],
                    ),
                  )),
            )
          ],
        ),
      ),
    );
  }

  Widget segmentTab1({
    required BuildContext context,
    required controllers,
    required dynamic data,
    bool isLoading = true,
  }) {
    var dataProduction = data['production'];
    var dataSalesOrder = data['sales_order'];

    return data.isNotEmpty
        ? Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: ListView(
              physics: const AlwaysScrollableScrollPhysics(),
              children: [
                BaseCardWidget.detailInfoCard2(
                  isLoading: isLoading,
                  contentList: [
                    {
                      'title': 'Unit Produksi',
                      'content': BaseTextWidget.customText(
                        text: dataProduction['production_unit_name'],
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.factory_rounded,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                    {
                      'title': 'Tgl.Produksi',
                      'content': BaseTextWidget.customText(
                        text: DateTime.parse(dataProduction['start_production'])
                            .toFormattedDateTime(),
                        // BaseGlobalFuntion.datetimeConvert(
                        //     dataProduction['start_production'],
                        //     "dd MMM yyyy HH:mm"),
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.calendar_today_rounded,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                    {
                      'title': 'No. Order',
                      'content': BaseTextWidget.customText(
                        text: dataSalesOrder['sales_order_number'],
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.numbers,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                    {
                      'title': 'Produk/Mutu',
                      'content': Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          BaseTextWidget.customText(
                            text: dataSalesOrder['product_name'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          BaseTextWidget.customText(
                            text: dataSalesOrder['product_code'],
                            color: AppColor.kGreyColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                        ],
                      ),
                      'icon': Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            color: AppColor.kGreyColor,
                          ),
                        ),
                        child: const Icon(
                          Icons.assignment_rounded,
                          size: 20,
                          color: AppColor.kPrimaryColor,
                        ),
                      )
                    },
                    {
                      'title': 'Volume',
                      'content': BaseTextWidget.customText(
                        text: dataSalesOrder['produced_quantity'],
                        extendText: " ${dataSalesOrder['uom_name']}",
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.tornado_outlined,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                  ],
                ),
                const SizedBox(height: 3),
              ],
            ),
          )
        : BaseCardWidget.detailInfoCard(
            isLoading: true,
            title: "Detail Produksi",
            contentList: [],
          );
  }

  Widget segmentTab2({
    required BuildContext context,
    required controllers,
    required dynamic data,
    bool isLoading = true,
  }) {
    var dataDelivery = data['delivery'];

    return data.isNotEmpty
        ? Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: ListView(
              physics: const AlwaysScrollableScrollPhysics(),
              children: [
                BaseCardWidget.detailInfoCard2(
                  isLoading: isLoading,
                  contentList: [
                    {
                      'title': 'No. Docket',
                      'content': BaseTextWidget.customText(
                        text: dataDelivery['docket_number'],
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            color: AppColor.kGreyColor,
                          ),
                        ),
                        child: const Icon(
                          Icons.numbers,
                          size: 20,
                          color: AppColor.kPrimaryColor,
                        ),
                      )
                    },
                    {
                      'title': 'No. Segel',
                      'content': BaseTextWidget.customText(
                        text: dataDelivery['seal_number'],
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            color: AppColor.kGreyColor,
                          ),
                        ),
                        child: const Icon(
                          Icons.numbers,
                          size: 20,
                          color: AppColor.kPrimaryColor,
                        ),
                      )
                    },
                    {
                      'title': 'Tgl.Pengiriman',
                      'content': BaseTextWidget.customText(
                        text: BaseGlobalFuntion.datetimeConvert(
                            dataDelivery['start_delivery'],
                            "dd MMM yyyy HH:mm"),
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.calendar_today_rounded,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                    {
                      'title': 'Pelanggan',
                      'content': BaseTextWidget.customText(
                        text: dataDelivery['customer_name'],
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            color: AppColor.kGreyColor,
                          ),
                        ),
                        child: const Icon(
                          Icons.person_2_sharp,
                          size: 20,
                          color: AppColor.kPrimaryColor,
                        ),
                      )
                    },
                    {
                      'title': 'Proyek',
                      'content': Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          BaseTextWidget.customText(
                            text: dataDelivery['site_name'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          BaseTextWidget.customText(
                            text: dataDelivery['address'],
                            color: AppColor.kGreyColor,
                            fontSize: 11,
                            isLongText: true,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                        ],
                      ),
                      'icon': Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            color: AppColor.kGreyColor,
                          ),
                        ),
                        child: const Icon(
                          Icons.pages_rounded,
                          size: 20,
                          color: AppColor.kPrimaryColor,
                        ),
                      )
                    },
                    {
                      'title': 'Armada',
                      'content': Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          BaseTextWidget.customText(
                            text: dataDelivery['equipment_name'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          BaseTextWidget.customText(
                            text: dataDelivery['fleet_number'],
                            color: AppColor.kGreyColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                        ],
                      ),
                      'icon': Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            color: AppColor.kGreyColor,
                          ),
                        ),
                        child: const Icon(
                          Icons.fire_truck_rounded,
                          size: 20,
                          color: AppColor.kPrimaryColor,
                        ),
                      )
                    },
                    {
                      'title': 'Sopir',
                      'content': BaseTextWidget.customText(
                        text: dataDelivery['driver_name'],
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            color: AppColor.kGreyColor,
                          ),
                        ),
                        child: const Icon(
                          Icons.person_4_rounded,
                          size: 20,
                          color: AppColor.kPrimaryColor,
                        ),
                      )
                    },
                  ],
                ),
                const SizedBox(height: 3),
              ],
            ),
          )
        : BaseCardWidget.detailInfoCard(
            isLoading: true,
            title: "Detail Pengiriman",
            contentList: [],
          );
  }
}
