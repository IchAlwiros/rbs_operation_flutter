import 'package:rbs_mobile_operation/data/datasources/product.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/app_view_resource.dart';

class ProductList extends StatelessWidget {
  const ProductList({super.key});

  @override
  Widget build(BuildContext context) {
    ProductListControllers productControllers =
        Get.put(ProductListControllers());

    Future onRefresh() async {
      productControllers.refreshDataProduct();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        if (productControllers.productData.isNotEmpty) {
          productControllers.fetchProduct();
        }
      }
    });

    // ** MODAL SHEET FILTER
    final isActive = ['Semua', 'Aktif', 'Non Aktif'];

    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          productControllers.currentFilter.clear();
          productControllers.filterFormKey.currentState!
              .patchValue({'active': null});
        },
        onFilter: () {
          Get.back();
          productControllers.refreshDataProduct();
        },
        valueFilter: productControllers.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: productControllers.filterFormKey,
            onChanged: () {
              productControllers.filterFormKey.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  FormBuilderChoiceChip<String>(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      labelText: 'Status',
                      hintText: "Pilih Status",
                      labelStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 14,
                        kfontWeight: FontWeight.w500,
                      ),
                      errorStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 13,
                        kcolor: AppColor.kFailedColor,
                        kfontWeight: FontWeight.w500,
                      ),
                    ),
                    name: 'active',
                    initialValue: productControllers.currentFilter['active'],
                    selectedColor: AppColor.kInactiveColor,
                    spacing: 6.0,
                    options: List.generate(isActive.length, (index) {
                      return FormBuilderChipOption(
                        value: isActive[index],
                        child: Text(
                          isActive[index],
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontWeight: FontWeight.w600,
                            kfontSize: 12,
                          ),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    // ** LIST OF CONTENT CARD
    final listItemContent = Flexible(child: Obx(() {
      final result = productControllers.productData;
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: result.isEmpty && productControllers.isLoading.isFalse
            ? SizedBox(
                height: MediaQuery.of(context).size.height / 1.2,
                child: Center(
                  child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                ),
              )
            : ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: result.length + 1,
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(height: 5),
                itemBuilder: (BuildContext context, int index) {
                  if (index < result.length) {
                    final data = result[index];
                    var colorBadge =
                        BaseGlobalFuntion.statusColor(data['active']);
                    return BaseCardWidget.cardStyle4(
                        ontap: () {
                          Get.to(ProductView(itemId: data['id']));
                        },
                        title: data['name'],
                        subtitle: data['production_unit_name'],
                        bottomSubtitle: BaseTextWidget.customText(
                          text: data['code'],
                          isLongText: true,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                        trailling: BaseTextWidget.customText(
                          text: BaseGlobalFuntion.currencyLocalConvert(
                              nominal: data['unit_price']),
                          extendText: "\n ${data['uom_name']}",
                          textAlign: TextAlign.center,
                        ),
                        extendWidget: Container(
                          padding: const EdgeInsets.all(4),
                          decoration: BoxDecoration(
                            color: colorBadge['background_color'],
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Text(
                            data['active'] == '1' ? 'Aktif' : 'Non Aktif',
                            style: BaseTextStyle.customTextStyle(
                              kfontSize: 12,
                              kcolor: colorBadge['text_color'],
                              kfontWeight: FontWeight.w500,
                            ),
                          ),
                        ));
                  } else if (result.length < 25) {
                    return productControllers.hasMore.value &&
                            productControllers.isLoading.isTrue
                        ? BaseLoadingWidget.cardShimmer(
                            height: MediaQuery.of(context).size.height,
                            shimmerheight: 80,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 25,
                          )
                        : const SizedBox();
                  } else {
                    return productControllers.hasMore.value
                        ? BaseLoadingWidget.cardShimmer(
                            height: 200,
                            shimmerheight: 90,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 2,
                          )
                        : const SizedBox();
                  }
                },
              ),
      );
    }));

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
            title: 'Data Produk',
            context: context,
            isAllFill: true,
            searchController: productControllers.search,
            searchText: 'Cari',
            onFillterChange: () {
              showBottomFilter(context);
            },
            onSearchChange: () {
              BaseGlobalFuntion.debounceRun(
                action: () {
                  productControllers.refreshDataProduct();
                },
                duration: const Duration(milliseconds: 1000),
              );
            }),
        body: Stack(
          children: [
            RefreshIndicator(
              onRefresh: onRefresh,
              child: SingleChildScrollView(
                controller: scrollController,
                physics: const AlwaysScrollableScrollPhysics(),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(height: 10),
                    listItemContent,
                  ],
                ),
              ),
            ),
            // filterCard,
          ],
        ),
      ),
    );
  }
}
