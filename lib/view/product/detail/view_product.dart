import 'package:rbs_mobile_operation/data/datasources/product.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class ProductView extends StatelessWidget {
  final int itemId;

  const ProductView({
    required this.itemId,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    DetailsProductControllers productControllers =
        Get.put(DetailsProductControllers());

    productControllers.viewDetailProduct(productId: itemId, isLoadData: true);

    final contentAppBar = SingleChildScrollView(
      child: Column(
        children: [
          Container(
            width: double.infinity,
            padding:
                const EdgeInsets.only(right: 20, left: 20, top: 15, bottom: 50),
            decoration: const BoxDecoration(
              color: AppColor.kPrimaryColor,
            ),
            child: Obx(() {
              if (productControllers.detailProduct.isNotEmpty &&
                  productControllers.isLoading.isFalse) {
                var data = productControllers.detailProduct;
                var colorBadge = BaseGlobalFuntion.statusColor(data['active']);

                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            padding: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              color: AppColor.kWhiteColor,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: const Icon(
                              Icons.content_paste_rounded,
                            ),
                          ),
                          const SizedBox(width: 10),
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                BaseTextWidget.customText(
                                  text: data['name'],
                                  color: AppColor.kWhiteColor,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w700,
                                  maxLengthText: 25,
                                ),
                                BaseTextWidget.customText(
                                  text: data['code'],
                                  color: AppColor.kWhiteColor,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500,
                                  maxLengthText: 25,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 2, horizontal: 6),
                      decoration: BoxDecoration(
                        color: colorBadge['background_color'],
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Text(
                        "${BaseGlobalFuntion.statusCheck(data['active'])}",
                        style: BaseTextStyle.customTextStyle(
                          kfontSize: 11,
                          kcolor: colorBadge['text_color'],
                        ),
                      ),
                    )
                  ],
                );
              } else {
                return Shimmer.fromColors(
                    baseColor: AppColor.kSoftGreyColor,
                    highlightColor: AppColor.kWhiteColor,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                            color: Colors.amber,
                            borderRadius: BorderRadius.circular(8),
                          ),
                        ),
                        const SizedBox(width: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 150,
                              height: 15,
                              decoration: BoxDecoration(
                                color: Colors.amber,
                                borderRadius: BorderRadius.circular(4),
                              ),
                            ),
                            const SizedBox(height: 10),
                            Container(
                              width: 200,
                              height: 15,
                              decoration: BoxDecoration(
                                color: Colors.amber,
                                borderRadius: BorderRadius.circular(4),
                              ),
                            )
                          ],
                        )
                      ],
                    ));
              }
            }),
          ),
          Obx(
            () => contentInfo(
              data: productControllers.detailProduct,
              isLoading: productControllers.isLoading.value,
            ),
          )
        ],
      ),
    );

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Detail Product',
          context: context,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              contentAppBar,
            ],
          ),
        ),
      ),
    );
  }

  Widget contentInfo({
    dynamic data,
    bool isLoading = true,
  }) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Container(
        margin: const EdgeInsets.only(left: 8, right: 8, top: 10),
        child: data.isNotEmpty
            ? Column(
                children: [
                  BaseCardWidget.detailInfoCard(
                    isLoading: isLoading,
                    title: "Info Produk",
                    contentList: [
                      {
                        'title': 'Tgl.Pengiriman',
                        'content': BaseGlobalFuntion.datetimeConvert(
                            data['created_at'], "dd MMM yyyy"),
                      },
                      {
                        'title': 'Deskripsi',
                        'content': "${data['description']}",
                      },
                      {
                        'title': 'Alamat',
                        'content': "${data['product_category_name']}"
                      },
                      {
                        'title': 'Satuan',
                        'content': "${data['uom_name']}",
                      },
                      {
                        'title': 'Harga',
                        'content':
                            "${BaseGlobalFuntion.currencyLocalConvert(nominal: data['unit_price'])}",
                      },
                    ],
                  )
                ],
              )
            : BaseCardWidget.detailInfoCard(
                isLoading: true,
                title: "Informasi Pelanggan & Proyek",
                contentList: [],
              ),
      ),
    );
  }
}
