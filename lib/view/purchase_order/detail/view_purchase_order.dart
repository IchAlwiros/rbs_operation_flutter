import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rbs_mobile_operation/data/datasources/purchaseorder.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class PurchaseOrderView extends StatelessWidget {
  final int itemId;

  const PurchaseOrderView({
    required this.itemId,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    DetailPurchaseOrderControllers purchaseOrderControllers =
        Get.put(DetailPurchaseOrderControllers());

    Future.delayed(const Duration(milliseconds: 200), () {
      purchaseOrderControllers.viewPurchaseOrder(itemId);
    }).then(
      (value) => purchaseOrderControllers.listPOeMaterial(itemId),
    );

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Detail Purchase Order',
          context: context,
        ),
        body: Column(
          children: [
            Expanded(
              child: DefaultTabController(
                  length: 2,
                  child: Scaffold(
                    appBar: PreferredSize(
                      preferredSize: const Size.fromHeight(kToolbarHeight),
                      child: Container(
                        color: AppColor.kPrimaryColor,
                        child: SafeArea(
                          child: Column(
                            children: <Widget>[
                              Expanded(child: Container()),
                              TabBar(
                                indicatorColor: AppColor.kInactiveColor,
                                unselectedLabelStyle:
                                    BaseTextStyle.customTextStyle(
                                  kfontWeight: FontWeight.w400,
                                  kfontSize: 12,
                                ),
                                unselectedLabelColor: AppColor.kWhiteColor,
                                labelColor: AppColor.kInactiveColor,
                                labelStyle: BaseTextStyle.customTextStyle(
                                  kfontWeight: FontWeight.w700,
                                  kfontSize: 14,
                                ),
                                tabs: const [
                                  Tab(text: 'Info'),
                                  Tab(text: 'Material'),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    body: TabBarView(
                      children: [
                        Obx(() => segmentTab1(
                              data:
                                  purchaseOrderControllers.detailPurchaseOrder,
                              isLoading:
                                  purchaseOrderControllers.isLoading.value,
                            )),
                        Obx(() => segmentTab2(
                              data: purchaseOrderControllers.dataListPOMaterial,
                            ))
                      ],
                    ),
                  )),
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }

  Widget segmentTab1({
    dynamic data,
    bool isLoading = true,
  }) {
    if (data.isNotEmpty) {
      var colorBadge = BaseGlobalFuntion.statusColor(data['status_code']);
      return SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Container(
            margin: const EdgeInsets.only(left: 8, right: 8),
            child: Column(
              children: [
                BaseCardWidget.detailInfoCard2(
                  isLoading: isLoading,
                  contentList: [
                    {
                      'title': 'Unit Produksi',
                      'content': BaseTextWidget.customText(
                        text: data['production_name'],
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.factory_rounded,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                    {
                      'title': 'No.Order',
                      'content': BaseTextWidget.customText(
                        text: data['order_number'],
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.numbers_rounded,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                    {
                      'title': 'Vendor',
                      'content': BaseTextWidget.customText(
                        text: data['vendor_name'],
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.home_repair_service_rounded,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                    {
                      'title': 'Tgl.Order',
                      'content': BaseTextWidget.customText(
                        text: BaseGlobalFuntion.datetimeConvert(
                            data['date'], "dd MMM yyyy"),
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.calendar_today_rounded,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                    {
                      'title': 'Vendor PIC',
                      'content': BaseTextWidget.customText(
                        text: data['vendor_pic_name'],
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.person_4_outlined,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                    {
                      'title': 'No.Telp PIC',
                      'content': BaseTextWidget.customText(
                        text: data['vendor_pic_phone'],
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.contact_phone_sharp,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                    {
                      'title': 'Status',
                      'content': Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: 2,
                          horizontal: 6,
                        ),
                        decoration: BoxDecoration(
                          color: colorBadge['background_color'],
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: BaseTextWidget.customText(
                          text: data['status_code'],
                          textAlign: TextAlign.center,
                          color: colorBadge['text_color'],
                        ),
                      ),
                      'icon': Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            color: AppColor.kGreyColor,
                          ),
                        ),
                        child: BaseTextWidget.customText(
                            text: "${data['progress_percentage']}",
                            extendText: '%'),
                      )
                    },
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    } else {
      return const SpinKitThreeBounce(
        size: 30.0,
        color: AppColor.kPrimaryColor,
      );
    }
  }

  Widget segmentTab2({dynamic data}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
      child: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 0),
              child: data.isNotEmpty
                  ? ListView.separated(
                      shrinkWrap: true,
                      itemCount: data.length,
                      separatorBuilder: (BuildContext context, int index) =>
                          const SizedBox(height: 5),
                      itemBuilder: (BuildContext context, int index) {
                        // var colorBadge = BaseGlobalFuntion.statusColor(
                        //     data[index]['active']);

                        return BaseCardWidget.cardStyle4(
                          ontap: () {},
                          title: data[index]['material_category_name'],
                          subtitle: data[index]['material_name'],
                          bottomSubtitle: BaseTextWidget.customText(
                            text: data[index]['quantity'],
                            extendText: data[index]['uom_name'],
                            isLongText: true,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            fontWeight: FontWeight.w500,
                          ),
                          trailling: BaseTextWidget.customText(
                            text: data[index]['total_price'],
                            isLongText: true,
                            localCurency: true,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            color: AppColor.kGreenColor,
                            fontWeight: FontWeight.w600,
                          ),
                          extendWidget: Container(
                            padding: const EdgeInsets.all(4),
                            decoration: BoxDecoration(
                              // color: colorBadge['background_color'],
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: BaseTextWidget.customText(
                              text: "${data[index]['progress']}",
                              extendText: "%",
                              maxLines: 1,
                              color: AppColor.kPrimaryColor,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        );
                      },
                    )
                  : Center(
                      child: BaseLoadingWidget.noMoreDataWidget(),
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
