import 'package:flutter/services.dart';
import 'package:rbs_mobile_operation/data/datasources/purchaseorder.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class AddMaterialOrder extends StatelessWidget {
  const AddMaterialOrder({super.key});

  @override
  Widget build(BuildContext context) {
    AddPurchaseOrderControllers purchaseOrderControllers =
        Get.put(AddPurchaseOrderControllers());

    Future<void> showBottomAddCart(BuildContext context, bool isEmptyCart) {
      TextEditingController subtotalControllers = TextEditingController();

      return showModalBottomSheet(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(15.0))),
        backgroundColor: AppColor.kWhiteColor,
        // backgroundColor: Colors.transparent,
        isDismissible: !isEmptyCart,
        context: context,
        isScrollControlled: true,
        builder: (context) => PopScope(
          canPop: !isEmptyCart,
          child: Padding(
            padding: EdgeInsets.only(
                top: 10,
                right: 15,
                left: 15,
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    BaseTextWidget.customText(
                      text: "Tambahkan Material",
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                    ),
                    IconButton.filledTonal(
                      onPressed: () {
                        Get.back();
                        if (isEmptyCart) {
                          Get.back();
                        }
                      },
                      icon: const Icon(Icons.close),
                    ),
                  ],
                ),
                const Divider(),
                FormBuilder(
                  key: purchaseOrderControllers.fkMaterialPO,
                  onChanged: () {
                    purchaseOrderControllers.fkMaterialPO.currentState!.save();
                  },
                  autovalidateMode: AutovalidateMode.disabled,
                  skipDisabled: true,
                  child: Flexible(
                    child: ListView(
                      shrinkWrap: true,
                      physics: const AlwaysScrollableScrollPhysics(),
                      children: [
                        Obx(() => purchaseOrderControllers
                                .dataLookupMaterial.isEmpty
                            ? BaseTextFieldWidget.disableTextForm2(
                                label: "Material",
                                disabledText: 'Pilih Material',
                                isRequired: true,
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Material wajib dipilih!';
                                  }
                                  return null;
                                },
                              )
                            : CustomDropDownSearch(
                                fieldName: 'material_id',
                                controller: purchaseOrderControllers
                                    .materialControllers,
                                isRequired: true,
                                label: "Material",
                                hintText: "Pilih Material",
                                asyncItems: (String filter) async {
                                  purchaseOrderControllers.lookupMaterial(
                                      searching: filter);
                                  return purchaseOrderControllers
                                      .dataLookupMaterial;
                                },
                                onChange: ({searchValue, valueItem, data}) {
                                  purchaseOrderControllers.temporaryDataPo({
                                    ...purchaseOrderControllers.temporaryDataPo,
                                    'material_selected': data,
                                  });
                                },
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Material wajib dipilih!';
                                  }
                                  return null;
                                },
                              )),
                        const SizedBox(height: 5),
                        CustomTextField.style2(
                          title: 'Jumlah Volume',
                          hintText: "0.00",
                          fieldName: 'quantity',
                          isRequired: true,
                          // controller: purchaseOrderControllers.volumeControllers,
                          onChange: ({value}) {
                            if (purchaseOrderControllers.fkMaterialPO
                                    .currentState?.value['unit_price'] !=
                                null) {
                              var subTotal = double.parse(
                                      purchaseOrderControllers.fkMaterialPO
                                          .currentState?.value['unit_price']
                                          .replaceAll(',', '')) *
                                  double.parse(value);

                              subtotalControllers.text =
                                  BaseGlobalFuntion.currencyLocalConvert(
                                      isLocalCurency: false, nominal: subTotal);
                            }
                          },
                          extendSuffixItem: Container(
                              // margin: const EdgeInsets.all(1),
                              padding: const EdgeInsets.all(14),
                              decoration: const BoxDecoration(
                                color: AppColor.kGreyColor,
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(7),
                                  bottomRight: Radius.circular(7),
                                ),
                              ),
                              child: Obx(
                                () => BaseTextWidget.customText(
                                  text: purchaseOrderControllers
                                          .temporaryDataPo['material_selected']
                                      ?['uom_name'],
                                  color: AppColor.kPrimaryColor,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 15,
                                ),
                              )),
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Volume wajib diisi!';
                            }

                            return null;
                          },
                          keyboardType: const TextInputType.numberWithOptions(
                              decimal: true),
                          textInputAction: TextInputAction.next,
                          inputFormater: <TextInputFormatter>[
                            FilteringTextInputFormatter.allow(
                                RegExp(r'^[0-9.]+$')),
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                        ),
                        const SizedBox(height: 5),
                        CustomTextField.style2(
                          title: 'Harga Satuan',
                          hintText: "0.00",
                          isRequired: true,
                          fieldName: 'unit_price',
                          onChange: ({value}) {
                            // ** MENDAPATKAN SUBTOTAL PER MATERIAL DENGAN MENKALI JUMLAH VOLUME x HARGA SATUAN

                            if (purchaseOrderControllers.fkMaterialPO
                                        .currentState?.value['quantity'] !=
                                    null &&
                                (value != null || value != '')) {
                              var subTotal = double.parse(
                                      purchaseOrderControllers.fkMaterialPO
                                          .currentState?.value['quantity']
                                          .replaceAll(',', '')) *
                                  double.parse(value.replaceAll(',', ''));

                              // printInfo(info: "INI INFO $value");

                              subtotalControllers.text =
                                  BaseGlobalFuntion.currencyLocalConvert(
                                      isLocalCurency: false, nominal: subTotal);
                            }
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              // subtotalControllers.clear();
                              return 'harga satuan wajib diisi!';
                            }

                            return null;
                          },
                          extendPrefixItem: Container(
                            margin: const EdgeInsets.only(right: 10),
                            padding: const EdgeInsets.all(14),
                            decoration: const BoxDecoration(
                              color: AppColor.kGreyColor,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(7),
                                bottomLeft: Radius.circular(7),
                              ),
                            ),
                            child: BaseTextWidget.customText(
                              text: 'RP',
                              color: AppColor.kPrimaryColor,
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                            ),
                          ),
                          keyboardType: const TextInputType.numberWithOptions(
                              decimal: true),
                          textInputAction: TextInputAction.done,
                          inputFormater: <TextInputFormatter>[
                            FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                            FilteringTextInputFormatter.digitsOnly,
                            BaseGlobalFuntion.rupiahSeparatorInputFormatter,
                          ],
                        ),
                        const SizedBox(height: 10),
                        CustomTextField.style2(
                          title: 'Sub Total',
                          hintText: "0.00",
                          // isRequired: true,
                          readOnly: true,
                          enabled: false,
                          fieldName: 'sub_total',
                          controller: subtotalControllers,
                          onChange: ({value}) {},
                          validator: (value) {
                            return null;
                          },
                          extendPrefixItem: Container(
                            margin: const EdgeInsets.only(right: 10),
                            padding: const EdgeInsets.all(14),
                            decoration: const BoxDecoration(
                              // color: AppColor.kGreyColor,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(7),
                                bottomLeft: Radius.circular(7),
                              ),
                            ),
                            child: BaseTextWidget.customText(
                              text: 'RP',
                              color: AppColor.kPrimaryColor,
                              fontWeight: FontWeight.w600,
                              fontSize: 15,
                            ),
                          ),
                          keyboardType: const TextInputType.numberWithOptions(
                              decimal: true),
                          textInputAction: TextInputAction.done,
                          inputFormater: <TextInputFormatter>[
                            FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                            FilteringTextInputFormatter.digitsOnly,
                            BaseGlobalFuntion.thousandsSeparatorInputFormatter,
                          ],
                        ),
                        const SizedBox(height: 15),
                      ],
                    ),
                  ),
                ),
                Obx(
                  () => Button.filled(
                    onPressed: () {
                      var materialSelected = purchaseOrderControllers
                          .temporaryDataPo['material_selected'];

                      purchaseOrderControllers.addMaterilPO(otherData: {
                        'material_name': materialSelected['name'],
                        'material_category_name':
                            materialSelected['material_category_name'],
                      });
                    },
                    width: double.infinity,
                    disabled: purchaseOrderControllers
                            .temporaryDataPo['material_selected'] ==
                        null,
                    // isLoading: purchaseOrderControllers.isLoading.isTrue,
                    color: AppColor.kPrimaryColor,
                    // height: 60,
                    icon: const Icon(
                      Icons.queue_play_next,
                      color: AppColor.kWhiteColor,
                    ),
                    // textStyle: BaseTextStyle.customTextStyle(
                    //   kfontWeight: FontWeight.w600,
                    //   kfontSize: 12,
                    //   kcolor: AppColor.kWhiteColor,
                    // ),
                    label: "Tambah",
                  ),
                ),
                const SizedBox(height: 15),
              ],
            ),
          ),
        ),
      );
    }

    final listCartMaterial = Obx(() {
      // var poTemp = purchaseOrderControllers.temporaryDataPo;
      return purchaseOrderControllers.tempDataMaterial.isEmpty
          ? const SizedBox()
          : Flexible(
              child: ListView.builder(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                // shrinkWrap: true,
                // physics: const NeverScrollableScrollPhysics(),
                itemCount: purchaseOrderControllers.tempDataMaterial.length,
                itemBuilder: (context, index) {
                  return BaseCardWidget.cardStyle1(
                      title: purchaseOrderControllers.tempDataMaterial[index]
                          ?['material_name'],
                      subTitle1: purchaseOrderControllers
                          .tempDataMaterial[index]?['material_category_name'],
                      trailing: IconButton.filledTonal(
                        splashColor: AppColor.kErrorColor,
                        onPressed: () {
                          purchaseOrderControllers.tempDataMaterial.remove(
                              purchaseOrderControllers.tempDataMaterial[index]);

                          // print(purchaseOrderControllers.temporaryDataPo);
                          // if (addCustomerControllers.dataProyekTemp.isEmpty) {
                          //   // Get.back();
                          //   // onStepTapped(0);
                          // }
                        },
                        icon: const Icon(
                          Icons.delete,
                          color: AppColor.kErrorColor,
                        ),
                        style: FilledButton.styleFrom(
                          backgroundColor: AppColor.kBadgeFailedColor,
                        ),
                      ));
                },
              ),
            );
    });

    final mainContent = Column(
      children: [
        Container(
          // height: 190,
          // width: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          decoration: const BoxDecoration(
            color: AppColor.kPrimaryColor,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Obx(
                () => Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: AppColor.kWhiteColor,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: const Icon(
                        Icons.home_work_rounded,
                      ),
                    ),
                    const SizedBox(width: 15),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        BaseTextWidget.customText(
                          text: purchaseOrderControllers
                              .temporaryDataPo['production_unit_name'],
                          isLongText: true,
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: AppColor.kWhiteColor,
                        ),
                        BaseTextWidget.customText(
                          text: purchaseOrderControllers
                              .temporaryDataPo['suplier_name'],
                          isLongText: true,
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          color: AppColor.kWhiteColor,
                        ),
                        const SizedBox(height: 2),
                        Row(
                          children: [
                            BaseTextWidget.customText(
                              text: 'TOTAL : ',
                              fontWeight: FontWeight.bold,
                              color: AppColor.kWhiteColor,
                            ),
                            BaseTextWidget.customText(
                              text:
                                  "${purchaseOrderControllers.tempDataMaterial.length} Material",
                              color: AppColor.kWhiteColor,
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              IconButton.filledTonal(
                iconSize: 20,
                onPressed: () {
                  showBottomAddCart(context, false);
                },
                icon: const Icon(
                  Icons.add,
                ),
              ),
            ],
          ),
        ),
        listCartMaterial,
      ],
    );

    final submitAction = Padding(
      padding: const EdgeInsets.all(10.0),
      child: Obx(() => Button.filled(
            onPressed: () =>
                purchaseOrderControllers.addPO(loadingCtx: context),
            disabled: purchaseOrderControllers.tempDataMaterial.isEmpty ||
                purchaseOrderControllers.isLoading.value,

            // isLoading: purchaseOrderControllers.isLoading.value,
            color: AppColor.kPrimaryColor,
            // height: 50,
            icon: const Icon(
              Icons.add_circle,
              color: AppColor.kWhiteColor,
            ),
            // textStyle: BaseTextStyle.customTextStyle(
            //   kfontWeight: FontWeight.w600,
            //   kfontSize: 12,
            //   kcolor: AppColor.kWhiteColor,
            // ),
            label: "Simpan",
          )),
    );

    if (purchaseOrderControllers.tempDataMaterial.isEmpty) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        purchaseOrderControllers.lookupMaterial();
        showBottomAddCart(context, true);
      });
    }

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        title: 'Tambahkan Material',
        context: context,
        extendBack: () {
          // salesController.dataLookupCustomerProject.clear();
          // salesController.dataDetailCustomer.clear();
        },
      ),
      body: mainContent,

      // Column(
      //   children: [
      //     mainContent,
      //     listCartMaterial,
      //   ],
      // ),
      // resizeToAvoidBottomInset: false,
      bottomNavigationBar: submitAction,
    );
  }
}
