import 'package:flutter/services.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';
import 'package:rbs_mobile_operation/data/datasources/purchaseorder.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/view/purchase_order/create/finish_purchase_order.dart';

class AddPurchaseOrder extends StatelessWidget {
  const AddPurchaseOrder({super.key});

  @override
  Widget build(BuildContext context) {
    AddPurchaseOrderControllers purchaseOrderControllers =
        Get.put(AddPurchaseOrderControllers());

    final submitAction = Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Directionality(
                textDirection: TextDirection.rtl,
                child: Button.filled(
                  onPressed: () {
                    var currentState =
                        purchaseOrderControllers.fkAddPO.currentState;
                    if (currentState?.validate() ?? false) {
                      Get.to(() => const AddMaterialOrder());
                    }
                    // fuelUsageControllers.addStockTake();
                  },
                  // isLoading: fuelUsageControllers.isLoading.value,
                  color: AppColor.kPrimaryColor,
                  // height: 50,
                  icon: const Icon(
                    Icons.arrow_circle_right_rounded,
                    color: AppColor.kWhiteColor,
                  ),
                  // rightIcon: true,
                  // textStyle: BaseTextStyle.customTextStyle(
                  //   kfontWeight: FontWeight.w600,
                  //   kfontSize: 12,
                  //   kcolor: AppColor.kWhiteColor,
                  // ),
                  label: "Selanjutnya",
                ),
              ),
            ),
          ],
        ),
      ),
    );

    final topContent = Container(
      height: 190,
      width: double.infinity,
      padding: const EdgeInsets.only(right: 20, left: 20, bottom: 20),
      decoration: const BoxDecoration(
        color: AppColor.kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
      ),
    );

    final detailPayment = Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      margin: const EdgeInsets.only(right: 10, left: 10, top: 10, bottom: 10),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: AppColor.kGreyColor),
      ),
      child: Column(
        children: [
          FormBuilder(
            key: purchaseOrderControllers.fkAddPO,
            onChanged: () {
              purchaseOrderControllers.fkAddPO.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Obx(() => purchaseOrderControllers.dataLookupPU.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih unit produksi',
                          label: "Unit Produksi",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib diisi!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'production_unit_id',
                          isRequired: true,
                          controller: purchaseOrderControllers
                              .productionUnitControllers,
                          initialValue: ApiService.productionUnitGlobal ??
                              LocalDataSource.getLocalVariable(
                                  key: 'lastestProduction'),
                          label: "Unit Produksi",
                          hintText: "Pilih Unit Produksi",
                          asyncItems: (String filter) async {
                            purchaseOrderControllers.lookupProductionUnit(
                                searching: filter);
                            return purchaseOrderControllers.dataLookupPU;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            purchaseOrderControllers.currentFilter(
                                {"production_unit_id": valueItem});

                            purchaseOrderControllers.temporaryDataPo({
                              "production_unit_id": data['id'],
                              "production_unit_name": data['name'],
                            });

                            purchaseOrderControllers.lookuVendor();

                            purchaseOrderControllers.dataLookupVendor.clear();
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib diisi!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  Obx(() => purchaseOrderControllers.dataLookupVendor.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih Suplier',
                          label: "Suplier",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Suplier wajib dipilih!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'vendor_id',
                          controller:
                              purchaseOrderControllers.vendorControllers,
                          isRequired: true,
                          label: "Suplier",
                          hintText: "Pilih Suplier",
                          asyncItems: (String filter) async {
                            purchaseOrderControllers.lookuVendor(
                                searching: filter);
                            return purchaseOrderControllers.dataLookupVendor;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            purchaseOrderControllers.temporaryDataPo({
                              ...purchaseOrderControllers.temporaryDataPo,
                              "suplier_id": data['id'],
                              "suplier_name": data['name'],
                            });
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Suplier wajib dipilih!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'No. Order',
                    hintText: "Ketik no.order...",
                    fieldName: 'order_number',
                    isRequired: true,
                    onClearText: () => purchaseOrderControllers
                        .fkAddPO.currentState!
                        .patchValue({'order_number': null}),
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'No. order wajib diisi!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                    inputFormater: <TextInputFormatter>[
                      // FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]')),
                      // Formatter untuk mengubah input menjadi huruf kapital
                    ],
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.formDatePicker2(
                    context: context,
                    title: "Tanggal Order",
                    hintText: "Pilih tanggal",
                    isRequired: true,
                    selectDateController:
                        purchaseOrderControllers.datePOControllers,
                    onChange: ({dateValue}) {},
                    fieldName: "date",
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Tanggal order wajib dipilih!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Catatan',
                    hintText: "Ketik catatan...",
                    fieldName: 'description',
                    isLongText: true,
                    onClearText: () => purchaseOrderControllers
                        .fkAddPO.currentState!
                        .patchValue({'description': null}),
                    onChange: ({value}) {},
                    validator: (value) {
                      return null;
                    },
                    textInputAction: TextInputAction.done,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        title: 'Tambah Purchase Order',
        context: context,
        extendBack: () {
          // salesController.dataLookupCustomerProject.clear();
          // salesController.dataDetailCustomer.clear();
        },
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            topContent,
            const SizedBox(height: 10),
            detailPayment,
          ],
        ),
      ),
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: submitAction,
    );
  }
}
