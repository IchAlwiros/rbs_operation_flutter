import 'package:rbs_mobile_operation/data/datasources/purchaseorder.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/app_view_resource.dart';
import 'package:rbs_mobile_operation/view/purchase_order/create/add_purchase_order.dart';

class PurchaseOrder extends StatelessWidget {
  const PurchaseOrder({super.key});

  @override
  Widget build(BuildContext context) {
    PurchaseOrderListControllers purchaseOrderControllers =
        Get.put(PurchaseOrderListControllers());

    Future onRefresh() async {
      purchaseOrderControllers.refreshPurchaseOrder();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        if (purchaseOrderControllers.purchaseData.isNotEmpty) {
          purchaseOrderControllers.fetchPurchaseOrder();
        }
      }
    });

    // ** SHOW FILTER MODAL BOTTOM

    TextEditingController vendorFilterController = TextEditingController();
    TextEditingController materialFilterController = TextEditingController();

    final statusFill = [
      'Semua',
      'Sukses',
      'Menunggu',
      'Diproses',
      "Dibatalkan"
    ];

    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          purchaseOrderControllers.byDate.clear();
          purchaseOrderControllers.currentFilter.clear();
          purchaseOrderControllers.filterFormKey.currentState!.reset();
          purchaseOrderControllers.filterFormKey.currentState!.patchValue({
            'material': null,
            'vendor': null,
            'date': null,
            'status': null,
          });
        },
        onFilter: () {
          Get.back();
          purchaseOrderControllers.refreshPurchaseOrder();
        },
        valueFilter: purchaseOrderControllers.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: purchaseOrderControllers.filterFormKey,
            onChanged: () {
              purchaseOrderControllers.filterFormKey.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'material',
                    controller: materialFilterController,
                    currentValue: purchaseOrderControllers
                        .currentFilter['material_filter'],
                    label: "Material",
                    hintText: "Pilih material",
                    asyncItems: (String filter) async {
                      purchaseOrderControllers.lookupMaterial(
                          searching: filter);
                      return purchaseOrderControllers.dataLookupMaterial;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      purchaseOrderControllers.currentFilter.value = {
                        ...purchaseOrderControllers.currentFilter,
                        'material_filter': searchValue,
                      };
                    },
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'vendor',
                    controller: vendorFilterController,
                    currentValue:
                        purchaseOrderControllers.currentFilter['vendor_filter'],
                    label: "Vendor",
                    hintText: "Pilih vendor",
                    asyncItems: (String filter) async {
                      purchaseOrderControllers.lookupVendor(searching: filter);
                      return purchaseOrderControllers.dataLookupVendor;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      purchaseOrderControllers.currentFilter.value = {
                        ...purchaseOrderControllers.currentFilter,
                        'vendor_filter': searchValue,
                      };
                    },
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.formDatePicker2(
                    context: context,
                    title: 'Tanggal',
                    hintText: 'Pilih Tanggal',
                    currentValue:
                        purchaseOrderControllers.currentFilter['date'] ?? "",
                    selectDateController: TextEditingController(),
                    onChange: ({dateValue}) {
                      purchaseOrderControllers.filterByRangeDate(
                        dateValue: dateValue,
                      );
                    },
                    fieldName: 'date',
                  ),
                  const SizedBox(height: 5),
                  FormBuilderChoiceChip<String>(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      labelText: 'Status',
                      hintText: 'Pilih Status',
                      contentPadding: const EdgeInsets.symmetric(vertical: 2),
                      labelStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 15,
                        kfontWeight: FontWeight.w500,
                        kcolor: AppColor.kPrimaryColor,
                      ),
                      errorStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 13,
                        kcolor: AppColor.kFailedColor,
                        kfontWeight: FontWeight.w500,
                      ),
                    ),
                    name: 'status',
                    initialValue:
                        purchaseOrderControllers.currentFilter['status'],
                    selectedColor: AppColor.kInactiveColor,
                    spacing: 6.0,
                    options: List.generate(statusFill.length, (index) {
                      return FormBuilderChipOption(
                        value: statusFill[index],
                        child: Text(
                          statusFill[index],
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontWeight: FontWeight.w600,
                            kfontSize: 12,
                          ),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    // ** LIST OF CARD CONTENT
    final listItemContent = Flexible(child: Obx(() {
      final result = purchaseOrderControllers.purchaseData;
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: result.isEmpty && purchaseOrderControllers.isLoading.isFalse
            ? SizedBox(
                height: MediaQuery.of(context).size.height / 1.2,
                child: Center(
                  child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                ),
              )
            : ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: result.length + 1,
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(height: 5),
                itemBuilder: (BuildContext context, int index) {
                  if (index < result.length) {
                    final data = result[index];
                    var colorBadge =
                        BaseGlobalFuntion.statusColor(data['status_code']);
                    return BaseCardWidget.cardStyle4(
                      ontap: () {
                        Get.to(
                          () => PurchaseOrderView(
                              itemId: data['purchase_order_id']),
                        );
                      },
                      title: data['order_number'],
                      subtitle: data['material_name'],
                      bottomSubtitle: BaseTextWidget.customText(
                        text: data['vendor_name'],
                        isLongText: true,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                      bottomSubtitle2: BaseTextWidget.customText(
                          text: BaseGlobalFuntion.datetimeConvert(
                              data['order_date'], "dd MMM yyyy")),
                      trailling: BaseTextWidget.customText(
                        text: data['quantity'],
                        extendText: "\n ${data['uom_name']}",
                        textAlign: TextAlign.right,
                      ),
                      extendWidget: Container(
                        padding: const EdgeInsets.all(4),
                        decoration: BoxDecoration(
                          color: colorBadge['background_color'],
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          data['status_code'],
                          style: BaseTextStyle.customTextStyle(
                            kfontSize: 12,
                            kcolor: colorBadge['text_color'],
                            kfontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    );
                  } else if (result.length < 25) {
                    return purchaseOrderControllers.hasMore.value &&
                            purchaseOrderControllers.isLoading.isTrue
                        ? BaseLoadingWidget.cardShimmer(
                            height: MediaQuery.of(context).size.height,
                            shimmerheight: 80,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 25,
                          )
                        : const SizedBox();
                  } else {
                    return purchaseOrderControllers.hasMore.value
                        ? BaseLoadingWidget.cardShimmer(
                            height: 200,
                            shimmerheight: 90,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 2,
                          )
                        : const SizedBox();
                  }
                },
              ),
      );
    }));

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
            title: 'Purchase Order',
            context: context,
            isAllFill: true,
            searchController: purchaseOrderControllers.search,
            searchText: 'Cari',
            onFillterChange: () {
              showBottomFilter(context);
            },
            onSearchChange: () {
              BaseGlobalFuntion.debounceRun(
                action: () {
                  purchaseOrderControllers.refreshPurchaseOrder();
                },
                duration: const Duration(milliseconds: 1000),
              );
            }),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              RefreshIndicator(
                onRefresh: onRefresh,
                child: SingleChildScrollView(
                  controller: scrollController,
                  physics: const AlwaysScrollableScrollPhysics(),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const SizedBox(height: 10),
                      listItemContent,
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 30.0,
                right: 15.0,
                child: SizedBox(
                  height: 55.0,
                  width: 55.0,
                  child: IconButton.filled(
                    iconSize: 30,
                    onPressed: () {
                      Get.to(() => const AddPurchaseOrder());
                    },
                    icon: const Icon(
                      Icons.add,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
