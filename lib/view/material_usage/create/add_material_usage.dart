import 'package:flutter/services.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';
import 'package:rbs_mobile_operation/data/datasources/materialusage.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class AddMaterialUsage extends StatelessWidget {
  const AddMaterialUsage({super.key});

  @override
  Widget build(BuildContext context) {
    AddMaterialUsageControllers materialUsageControllers =
        Get.put(AddMaterialUsageControllers());

    final submitAction = Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Obx(() => Expanded(
                  child: Button.filled(
                    onPressed: () => materialUsageControllers.addMaterialUsage(
                        loadingCtx: context),
                    isLoading: materialUsageControllers.isLoading.value,
                    color: AppColor.kPrimaryColor,
                    height: 60,
                    icon: const Icon(
                      Icons.add_circle,
                      color: AppColor.kWhiteColor,
                    ),
                    // textStyle: BaseTextStyle.customTextStyle(
                    //   kfontWeight: FontWeight.w600,
                    //   kfontSize: 12,
                    //   kcolor: AppColor.kWhiteColor,
                    // ),
                    label: "Simpan",
                  ),
                )),
          ],
        ),
      ),
    );

    final topContent = Container(
      height: 190,
      width: double.infinity,
      padding: const EdgeInsets.only(right: 20, left: 20, bottom: 20),
      decoration: const BoxDecoration(
        color: AppColor.kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
      ),
    );

    final detailPayment = Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      margin: const EdgeInsets.only(right: 10, left: 10, top: 10, bottom: 10),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: AppColor.kGreyColor),
      ),
      child: Column(
        children: [
          FormBuilder(
            key: materialUsageControllers.fkAddStockTake,
            onChanged: () {
              materialUsageControllers.fkAddStockTake.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Obx(() => materialUsageControllers.dataLookupPU.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih unit produksi',
                          label: "Unit Produksi",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'unit produksi wajib diisi!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'production_unit_id',
                          isRequired: true,
                          controller:
                              materialUsageControllers.productionUnitController,
                          initialValue: ApiService.productionUnitGlobal ??
                              LocalDataSource.getLocalVariable(
                                  key: 'lastestProduction'),
                          label: "Unit Produksi",
                          hintText: "Pilih Unit Produksi",
                          asyncItems: (String filter) async {
                            materialUsageControllers.lookupProductionUnit(
                                searching: filter);
                            return materialUsageControllers.dataLookupPU;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            materialUsageControllers.currentFilter(
                                {"production_unit_id": valueItem});
                            materialUsageControllers.lookupMaterial();
                            materialUsageControllers.dataLookupMaterial.clear();
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib diisi!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  Obx(() => materialUsageControllers.dataLookupMaterial.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih material',
                          label: "Material",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Material wajib diisi!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'material_id',
                          controller:
                              materialUsageControllers.materialController,
                          isRequired: true,
                          label: "Material",
                          hintText: "Pilih Material",
                          asyncItems: (String filter) async {
                            materialUsageControllers.lookupMaterial(
                                searching: filter);
                            return materialUsageControllers.dataLookupMaterial;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            materialUsageControllers.currentFilter({
                              ...materialUsageControllers.currentFilter,
                              "material_id": data['id']
                            });

                            materialUsageControllers
                                .uomName({'uom_name': data['uom_name']});

                            materialUsageControllers.lookupWarehouse();

                            materialUsageControllers.selisihVolume.value = 0.0;

                            materialUsageControllers.dataLookupWarehouse
                                .clear();
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Material wajib diisi!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  Obx(() => materialUsageControllers.dataLookupWarehouse.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih gudang',
                          label: "Gudang/Lokasi Penyimpanan",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Gudang wajib diisi!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'warehouse_id',
                          controller:
                              materialUsageControllers.warehouseController,
                          isRequired: true,
                          label: "Gudang/Lokasi Penyimpanan",
                          hintText: "Pilih gudang",
                          asyncItems: (String filter) async {
                            materialUsageControllers.lookupWarehouse(
                                searching: filter);
                            return materialUsageControllers.dataLookupWarehouse;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            // materialUsageControllers.dataViewInventory.clear();
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Gudang wajib diisi!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  BaseButtonWidget.formDatePicker2(
                    context: context,
                    title: "Tanggal",
                    hintText: "Pilih tanggal",
                    isRequired: true,
                    selectDateController: materialUsageControllers.stokTakeDate,
                    onChange: ({dateValue}) {},
                    fieldName: "date",
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Tanggal wajib diisi!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.timePicker(
                    context: context,
                    isRequired: true,
                    title: "Jam",
                    hintText: 'Pilih jam',
                    valueController: materialUsageControllers.timeControllers,
                    // onChange: ({dateValue}) {},
                    fieldName: "time",
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Jam wajib diisi!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Jumlah Material',
                    hintText: "0.00",
                    isRequired: true,
                    fieldName: 'quantity',
                    onClearText: () => materialUsageControllers
                        .fkAddStockTake.currentState!
                        .patchValue({'quantity': null}),
                    onChange: ({value}) {},
                    extendSuffixItem: Container(
                      margin: const EdgeInsets.all(1),
                      padding: const EdgeInsets.all(14),
                      decoration: const BoxDecoration(
                        color: AppColor.kGreyColor,
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(7),
                          bottomRight: Radius.circular(7),
                        ),
                      ),
                      child: Obx(() => BaseTextWidget.customText(
                            text: materialUsageControllers.uomName['uom_name'],
                            color: AppColor.kPrimaryColor,
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                          )),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Jumlah material wajib diisi!';
                      }

                      return null;
                    },
                    keyboardType:
                        const TextInputType.numberWithOptions(decimal: true),
                    textInputAction: TextInputAction.next,
                    inputFormater: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'^[0-9.]+$')),
                    ],
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Catatan',
                    hintText: "Ketik catatan...",
                    fieldName: 'description',
                    isLongText: true,
                    onClearText: () => materialUsageControllers
                        .fkAddStockTake.currentState!
                        .patchValue({'description': null}),
                    onChange: ({value}) {},
                    validator: (value) {
                      return null;
                    },
                    textInputAction: TextInputAction.done,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        title: 'Tambah Penggunaan Material',
        centerTitle: true,
        context: context,
        extendBack: () {
          // salesController.dataLookupCustomerProject.clear();
          // salesController.dataDetailCustomer.clear();
        },
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            topContent,
            const SizedBox(height: 10),
            detailPayment,
          ],
        ),
      ),
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: submitAction,
    );
  }
}
