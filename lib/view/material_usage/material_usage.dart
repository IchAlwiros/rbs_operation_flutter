import 'package:rbs_mobile_operation/core/extentions/date_time_extension.dart';
import 'package:rbs_mobile_operation/data/datasources/materialusage.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/material_usage/create/add_material_usage.dart';

class MaterialUsageList extends StatelessWidget {
  const MaterialUsageList({super.key});

  @override
  Widget build(BuildContext context) {
    MaterialUsageListControllers materialUsageControllers =
        Get.put(MaterialUsageListControllers());

    Future onRefresh() async {
      materialUsageControllers.refreshMaterialUsage();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        if (materialUsageControllers.materialUsageData.isNotEmpty) {
          materialUsageControllers.fetchmaterialUsage();
        }
      }
    });

    //  ** SHOW FILTER MODAL

    TextEditingController materialFilterController = TextEditingController();
    TextEditingController warehouseFilterController = TextEditingController();

    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          materialUsageControllers.byDate.clear();
          materialUsageControllers.currentFilter.clear();
          materialUsageControllers.filterFormKey.currentState!.reset();
          materialUsageControllers.filterFormKey.currentState!.patchValue({
            'warehouse': null,
            'date_range': null,
            'material': null,
          });
        },
        onFilter: () {
          Get.back();
          materialUsageControllers.refreshMaterialUsage();
        },
        valueFilter: materialUsageControllers.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: materialUsageControllers.filterFormKey,
            onChanged: () {
              materialUsageControllers.filterFormKey.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'warehouse',
                    controller: warehouseFilterController,
                    currentValue: materialUsageControllers
                        .currentFilter['warehouse_filter'],
                    label: "Gudang/Lokasi Penyimpanan",
                    hintText: "Pilih Gudang/Lokasi Penyimpanan",
                    asyncItems: (String filter) async {
                      materialUsageControllers.lookupWarehouse(
                          searching: filter);
                      return materialUsageControllers.dataLookupWarehouse;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      // print(data);
                      materialUsageControllers.currentFilter.value = {
                        ...materialUsageControllers.currentFilter,
                        'warehouse_filter': searchValue,
                      };
                    },
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.formDateRangePicker2(
                    context: context,
                    title: 'Tanggal',
                    hintText: "Pilih tanggal",
                    currentValue: BaseGlobalFuntion.formatDateRange(
                      materialUsageControllers.byDate['start-date'] ?? "",
                      materialUsageControllers.byDate['end-date'] ?? "",
                    ),
                    startDateController: TextEditingController(),
                    endDateController: TextEditingController(),
                    onChange: ({endDate, startDate}) {
                      materialUsageControllers.filterByRangeDate(
                        startDate: startDate,
                        endDate: endDate,
                      );
                    },
                    fieldName: 'date_range',
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'material',
                    controller: materialFilterController,
                    currentValue: materialUsageControllers
                        .currentFilter['material_filter'],
                    label: "Material",
                    hintText: "Pilih material",
                    asyncItems: (String filter) async {
                      materialUsageControllers.lookupMaterial(
                          searching: filter);
                      return materialUsageControllers.dataLookupMaterial;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      // print(data);
                      materialUsageControllers.currentFilter.value = {
                        ...materialUsageControllers.currentFilter,
                        'material_filter': searchValue,
                      };
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    final listItemContent = Flexible(child: Obx(() {
      final result = materialUsageControllers.materialUsageData;
      return result.isEmpty && materialUsageControllers.isLoading.isFalse
          ? SizedBox(
              height: MediaQuery.of(context).size.height / 1.2,
              child: Center(
                child: BaseLoadingWidget.noMoreDataWidget(height: 200),
              ),
            )
          : Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: result.length + 1,
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(height: 9),
                itemBuilder: (BuildContext context, int index) {
                  if (index < result.length) {
                    final data = result[index];

                    return BaseCardWidget.cardStyle4(
                      title: data['material_name'],
                      subtitle: data['warehouse_name'],
                      bottomSubtitle: BaseTextWidget.customText(
                        text: data['production_unit_name'],
                        isLongText: true,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                      bottomSubtitle2: BaseTextWidget.customText(
                          text: DateTime.parse(data['date'])
                              .toFormattedDateTime()),
                      trailling: BaseTextWidget.customText(
                        text: data['quantity'],
                        extendText: "\n ${data['uom_name']}",
                        textAlign: TextAlign.right,
                        fontWeight: FontWeight.w600,
                      ),
                    );
                  } else if (result.length < 25) {
                    return materialUsageControllers.hasMore.value &&
                            materialUsageControllers.isLoading.isTrue
                        ? BaseLoadingWidget.cardShimmer(
                            height: MediaQuery.of(context).size.height,
                            shimmerheight: 80,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 25,
                          )
                        : const SizedBox();
                  } else {
                    return materialUsageControllers.hasMore.value
                        ? BaseLoadingWidget.cardShimmer(
                            height: 200,
                            shimmerheight: 90,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 2,
                          )
                        : const SizedBox();
                  }
                },
              ),
            );
    }));

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
            title: 'Penggunaan Material',
            context: context,
            isAllFill: true,
            searchController: materialUsageControllers.search,
            searchText: 'Cari',
            onFillterChange: () {
              showBottomFilter(context);
            },
            onSearchChange: () {
              BaseGlobalFuntion.debounceRun(
                action: () {
                  materialUsageControllers.refreshMaterialUsage();
                },
                duration: const Duration(milliseconds: 1000),
              );
            }),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              RefreshIndicator(
                onRefresh: onRefresh,
                child: SingleChildScrollView(
                  controller: scrollController,
                  physics: const AlwaysScrollableScrollPhysics(),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const SizedBox(height: 10),
                      listItemContent,
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 30.0,
                right: 15.0,
                child: SizedBox(
                  height: 55.0,
                  width: 55.0,
                  child: IconButton.filled(
                    iconSize: 30,
                    onPressed: () {
                      Get.to(() => const AddMaterialUsage());
                    },
                    icon: const Icon(
                      Icons.add,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
