import 'package:rbs_mobile_operation/data/datasources/auth.controllers.dart';
import 'package:rbs_mobile_operation/core/extentions/animation_fade_in_slide.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    AuthControllers authControllers = Get.put(AuthControllers());

    // Future.delayed(const Duration(milliseconds: 200), () {
    authControllers.checkAuth(context);
    // });
    // .then(
    //   (value) => orderViewControllers.viewDetailOrder(itemId),
    // );

    return Scaffold(
      backgroundColor: AppColor.kPrimaryColor,
      body: Stack(
        children: [
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FadeInSlide(
                  duration: .5,
                  child: Container(
                    width: 90,
                    height: 90,
                    padding: const EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(500),
                      // color: kWhiteColor,
                    ),
                    child: Assets.icons.iconLogoSvg.svg(height: 50),

                    //  SvgPicture.asset(
                    //   'assets/icons/icon_logo.svg',
                    //   width: 50,
                    // )
                  ),
                ),
                // const SizedBox(height: 30),
                FadeInSlide(
                  duration: .5,
                  child: Text(
                    'RBS MOBILE',
                    style: BaseTextStyle.customTextStyle(
                      kfontSize: 18,
                      kcolor: AppColor.kWhiteColor,
                      kfontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                FadeInSlide(
                  duration: .5,
                  child: Text(
                    'Readymix Operation Mobile',
                    style: BaseTextStyle.customTextStyle(
                      kfontSize: 11,
                      kcolor: AppColor.kWhiteColor,
                      kfontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
      bottomNavigationBar: const SizedBox(
        height: 80.0,
        child: Align(
          alignment: Alignment.center,
          // child: Assets.images.logoCwb.image(width: 96.0),
          child: CircularProgressIndicator(
            color: AppColor.kWhiteColor,
          ),
        ),
      ),
    );
  }
}
