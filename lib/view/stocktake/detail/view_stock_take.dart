import 'package:rbs_mobile_operation/data/datasources/stocktake.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class StockTakeDetail extends StatelessWidget {
  final int itemId;

  const StockTakeDetail({
    required this.itemId,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    StockTakeListControllers stockTakeDetail =
        Get.put(StockTakeListControllers());

    Future.delayed(const Duration(milliseconds: 200), () {
      stockTakeDetail.viewStockTake(itemId);
    });

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Detail Stock Take',
          context: context,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: Obx(
                  () => BaseCardWidget.detailInfoCard2(
                    isLoading: stockTakeDetail.isLoading.value,
                    contentList: [
                      {
                        'title': 'Unit Produksi',
                        'content': BaseTextWidget.customText(
                          text: stockTakeDetail
                              .detailStockTake['production_unit_name'],
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.factory_rounded,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'Material',
                        'content': Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            BaseTextWidget.customText(
                              text: stockTakeDetail
                                  .detailStockTake['material_name'],
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              isLongText: true,
                            ),
                            BaseTextWidget.customText(
                              text: stockTakeDetail
                                  .detailStockTake['material_code'],
                              color: AppColor.kGreyColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              isLongText: true,
                              fontStyle: FontStyle.italic,
                            ),
                          ],
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.aod_rounded,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'Gudang',
                        'content': BaseTextWidget.customText(
                          text:
                              stockTakeDetail.detailStockTake['warehouse_name'],
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                          isLongText: true,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.warehouse,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'Gain/Loss',
                        'content': BaseTextWidget.customText(
                          text: stockTakeDetail.detailStockTake['difference'],
                          extendText:
                              stockTakeDetail.detailStockTake['uom_name'],
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                          isLongText: true,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.stacked_line_chart,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'Stok Akhir',
                        'content': BaseTextWidget.customText(
                          text: stockTakeDetail
                              .detailStockTake['actual_quantity'],
                          extendText:
                              stockTakeDetail.detailStockTake['uom_name'],
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                          isLongText: true,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.view_in_ar_outlined,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'Tanggal',
                        'content': BaseTextWidget.customText(
                          text: BaseGlobalFuntion.datetimeConvert(
                              stockTakeDetail.detailStockTake['date'],
                              'dd MMM yyyy HH:mm'),
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                          isLongText: true,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.view_in_ar_outlined,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'Dibuat Oleh',
                        'content': BaseTextWidget.customText(
                          text: stockTakeDetail
                              .detailStockTake['created_by_name'],
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                          isLongText: true,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.person_4_outlined,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
