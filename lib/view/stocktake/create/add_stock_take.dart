import 'package:flutter/services.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';
import 'package:rbs_mobile_operation/data/datasources/stocktake.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class AddStockTake extends StatelessWidget {
  const AddStockTake({super.key});

  @override
  Widget build(BuildContext context) {
    AddStockTakeControllers stockTakeControllers =
        Get.put(AddStockTakeControllers());

    final submitAction = Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Obx(() => Expanded(
                  child: Button.filled(
                    onPressed: () =>
                        stockTakeControllers.addStockTake(loadingCtx: context),
                    // isLoading: stockTakeControllers.isLoading.value,
                    disabled: stockTakeControllers.isLoading.value,
                    color: AppColor.kPrimaryColor,
                    // height: 60,
                    icon: const Icon(
                      Icons.add_circle,
                      color: AppColor.kWhiteColor,
                    ),
                    // textStyle: BaseTextStyle.customTextStyle(
                    //   kfontWeight: FontWeight.w600,
                    //   kfontSize: 12,
                    //   kcolor: AppColor.kWhiteColor,
                    // ),
                    label: "Simpan",
                  ),
                )),
          ],
        ),
      ),
    );

    final topContent = Container(
      height: 190,
      width: double.infinity,
      padding: const EdgeInsets.only(right: 20, left: 20, bottom: 20),
      decoration: const BoxDecoration(
        color: AppColor.kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
      ),
    );

    final detailPayment = Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      margin: const EdgeInsets.only(right: 10, left: 10, top: 10, bottom: 10),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: AppColor.kGreyColor),
      ),
      child: Column(
        children: [
          FormBuilder(
            key: stockTakeControllers.fkAddStockTake,
            onChanged: () {
              stockTakeControllers.fkAddStockTake.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Obx(() => stockTakeControllers.dataLookupPU.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih unit produksi',
                          label: "Unit Produksi",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib dipilih!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'production_unit_id',
                          isRequired: true,
                          controller:
                              stockTakeControllers.productionUnitController,
                          initialValue: ApiService.productionUnitGlobal ??
                              LocalDataSource.getLocalVariable(
                                  key: 'lastestProduction'),
                          label: "Unit Produksi",
                          hintText: "Pilih Unit Produksi",
                          asyncItems: (String filter) async {
                            stockTakeControllers.lookupProductionUnit(
                                searching: filter);
                            return stockTakeControllers.dataLookupPU;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            stockTakeControllers.currentFilter(
                                {"production_unit_id": valueItem});
                            stockTakeControllers.lookupMaterial();
                            stockTakeControllers.dataLookupMaterial.clear();
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib dipilih!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  Obx(() => stockTakeControllers.dataLookupMaterial.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih material',
                          label: "Material",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Material wajib dipilih!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'material_id',
                          controller: stockTakeControllers.materialController,
                          isRequired: true,
                          label: "Material",
                          hintText: "Pilih Material",
                          asyncItems: (String filter) async {
                            stockTakeControllers.lookupMaterial(
                                searching: filter);
                            return stockTakeControllers.dataLookupMaterial;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            stockTakeControllers.viewInventory(
                              productionUnitId: stockTakeControllers
                                  .productionUnitController.text,
                              materialId: valueItem,
                            );

                            stockTakeControllers.currentFilter({
                              ...stockTakeControllers.currentFilter,
                              "material_id": data['id']
                            });

                            stockTakeControllers.lookupWarehouse();

                            stockTakeControllers.selisihVolume.value = 0.0;

                            stockTakeControllers.initialNewVolumControllers
                                .clear();
                            stockTakeControllers.dataLookupWarehouse.clear();
                            stockTakeControllers.dataViewInventory.clear();
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Material wajib dipilih!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  Obx(() => stockTakeControllers.dataLookupWarehouse.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih gudang',
                          label: "Gudang/Lokasi Penyimpanan",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Gudang wajib dipilih!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'warehouse_id',
                          controller: stockTakeControllers.warehouseController,
                          isRequired: true,
                          label: "Gudang/Lokasi Penyimpanan",
                          hintText: "Pilih gudang",
                          asyncItems: (String filter) async {
                            stockTakeControllers.lookupWarehouse(
                                searching: filter);
                            return stockTakeControllers.dataLookupWarehouse;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            // stockTakeControllers.dataViewInventory.clear();
                            // print(data);
                            stockTakeControllers.initialNewVolumControllers
                                .text = BaseGlobalFuntion.currencyLocalConvert(
                              nominal: data['quantity'],
                              isLocalCurency: false,
                              decimalDigits: 0,
                            );

                            // data['quantity'].toString();
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Gudang wajib dipilih!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  BaseButtonWidget.formDatePicker2(
                    context: context,
                    title: "Tanggal Stock Take",
                    hintText: "Pilih tanggal",
                    isRequired: true,
                    selectDateController: stockTakeControllers.stokTakeDate,
                    onChange: ({dateValue}) {},
                    fieldName: "date",
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Tanggal wajib dipilih!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.timePicker(
                    context: context,
                    isRequired: true,
                    title: "Waktu Stock Take",
                    hintText: 'Pilih jam',
                    valueController: stockTakeControllers.timeControllers,
                    // onChange: ({dateValue}) {},
                    fieldName: "time",
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Waktu wajib dipilih!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 5),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Obx(() {
                          return CustomTextField.style2(
                            title: 'Vol. Sebelumnya',
                            hintText: "100.00",
                            fieldName: 'volume_before',
                            enabled: false,
                            controller:
                                stockTakeControllers.initialVolumControllers,
                            readOnly: true,
                            onChange: ({value}) {},
                            extendSuffixItem: Container(
                              margin: const EdgeInsets.all(1),
                              padding: const EdgeInsets.all(14),
                              decoration: const BoxDecoration(
                                color: AppColor.kGreyColor,
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(7),
                                  bottomRight: Radius.circular(7),
                                ),
                              ),
                              child: BaseTextWidget.customText(
                                text: stockTakeControllers
                                    .dataViewInventory['uom_name'],
                                color: AppColor.kPrimaryColor,
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                              ),
                            ),
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Vol. sebelumnya wajib diisi!';
                              }

                              return null;
                            },
                            keyboardType: const TextInputType.numberWithOptions(
                                decimal: true),
                            textInputAction: TextInputAction.next,
                            inputFormater: <TextInputFormatter>[
                              FilteringTextInputFormatter.allow(
                                  RegExp(r'^[0-9.]+$')),
                              FilteringTextInputFormatter.digitsOnly,
                              // BaseGlobalFuntion
                              //     .thousandsSeparatorInputFormatter,
                            ],
                          );
                        }),
                      ),
                      const SizedBox(width: 5),
                      Expanded(
                        child: Obx(() => CustomTextField.style2(
                              title: 'Vol. Saat Ini',
                              hintText: "100.00",
                              isRequired: true,
                              controller: stockTakeControllers
                                  .initialNewVolumControllers,
                              fieldName: 'new_quantity',
                              onClearText: () {
                                // stockTakeControllers
                                //     .initialNewVolumControllers
                                //     .clear();

                                stockTakeControllers
                                    .fkAddStockTake.currentState!
                                    .patchValue({'new_quantity': null});
                              },
                              onChange: ({value}) {
                                if (value != null) {
                                  // print(value.replaceAll(',', ''));
                                  final valueString = value.replaceAll(',', '');

                                  stockTakeControllers.selisihVolume.value =
                                      double.parse(valueString ?? "0") -
                                          stockTakeControllers
                                                  .dataViewInventory[
                                              'current_stock'];
                                }
                              },
                              extendSuffixItem: Container(
                                margin: const EdgeInsets.all(1),
                                padding: const EdgeInsets.all(14),
                                decoration: const BoxDecoration(
                                  color: AppColor.kGreyColor,
                                  borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(7),
                                    bottomRight: Radius.circular(7),
                                  ),
                                ),
                                child: BaseTextWidget.customText(
                                  text: stockTakeControllers
                                      .dataViewInventory['uom_name'],
                                  color: AppColor.kPrimaryColor,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 15,
                                ),
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Vol. saat ini wajib diisi!';
                                }

                                return null;
                              },
                              textInputAction: TextInputAction.next,
                              // keyboardType:
                              //     const TextInputType.numberWithOptions(
                              //         decimal: true),
                              // inputFormater: <TextInputFormatter>[
                              //   FilteringTextInputFormatter.allow(
                              //       RegExp(r'[0-9]')),
                              //   FilteringTextInputFormatter.digitsOnly,
                              //   BaseGlobalFuntion
                              //       .thousandsSeparatorInputFormatter,
                              // ],
                              keyboardType:
                                  const TextInputType.numberWithOptions(
                                      decimal: true),
                              inputFormater: [
                                FilteringTextInputFormatter.allow(
                                    RegExp(r'^\d*\.?\d*')),
                              ],
                            )),
                      ),
                    ],
                  ),
                  const SizedBox(height: 5),
                  Container(
                    padding: const EdgeInsets.all(10),
                    margin: const EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: AppColor.kGreyColor2,
                    ),
                    child: Row(
                      children: [
                        const Icon(
                          Icons.notifications,
                          color: AppColor.kPrimaryColor,
                        ),
                        const SizedBox(width: 10),
                        BaseTextWidget.customText(
                          text: "Selisih ",
                          fontWeight: FontWeight.w600,
                          color: AppColor.kPrimaryColor,
                        ),
                        Obx(
                          () => BaseTextWidget.customText(
                            text: stockTakeControllers.selisihVolume.value > 0
                                ? stockTakeControllers.selisihVolume.value
                                : stockTakeControllers.selisihVolume.value,
                            // BaseGlobalFuntion.currencyLocalConvert(
                            //     nominal: stockTakeControllers
                            //         .selisihVolume.value,
                            //     isLocalCurency: false,
                            //   ),
                            // stockTakeControllers.selisihVolume.value
                            //                             .toStringAsFixed(2),
                            fontWeight: FontWeight.w500,
                            extendOnFirst:
                                stockTakeControllers.selisihVolume.value > 0
                                    ? "+"
                                    : "",
                            color: stockTakeControllers.selisihVolume.value < 0
                                ? AppColor.kFailedColor
                                : AppColor.kSuccesColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Catatan',
                    hintText: "Ketik catatan...",
                    fieldName: 'description',
                    isLongText: true,
                    onClearText: () => stockTakeControllers
                        .fkAddStockTake.currentState!
                        .patchValue({'description': null}),
                    onChange: ({value}) {},
                    validator: (value) {
                      return null;
                    },
                    textInputAction: TextInputAction.done,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        title: 'Tambah Stock Take',
        centerTitle: true,
        context: context,
        extendBack: () {
          // salesController.dataLookupCustomerProject.clear();
          // salesController.dataDetailCustomer.clear();
        },
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            topContent,
            const SizedBox(height: 10),
            detailPayment,
          ],
        ),
      ),
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: submitAction,
    );
  }
}
