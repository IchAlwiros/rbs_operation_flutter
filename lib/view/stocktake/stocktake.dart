import 'package:rbs_mobile_operation/data/datasources/stocktake.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/stocktake/create/add_stock_take.dart';
import 'package:rbs_mobile_operation/view/stocktake/detail/view_stock_take.dart';

class StockTakeList extends StatelessWidget {
  const StockTakeList({super.key});

  @override
  Widget build(BuildContext context) {
    StockTakeListControllers stockTakeControllers =
        Get.put(StockTakeListControllers());

    Future onRefresh() async {
      stockTakeControllers.refreshStockTake();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        if (stockTakeControllers.stockTakeData.isNotEmpty) {
          stockTakeControllers.fetchStockTake();
        }
      }
    });

    // ** SHOW FILTER MODAL BOTTOM
    TextEditingController warehouseFilterController = TextEditingController();
    TextEditingController materialFilterController = TextEditingController();

    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          stockTakeControllers.byDate.clear();
          stockTakeControllers.currentFilter.clear();
          stockTakeControllers.filterFormKey.currentState!.reset();
          stockTakeControllers.filterFormKey.currentState!.patchValue({
            'status': null,
            'warehouse': null,
            'date_range': null,
            'material': null,
            'unit_production': null,
          });
        },
        onFilter: () {
          Get.back();
          stockTakeControllers.refreshStockTake();
        },
        valueFilter: stockTakeControllers.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: stockTakeControllers.filterFormKey,
            onChanged: () {
              stockTakeControllers.filterFormKey.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'warehouse',
                    controller: warehouseFilterController,
                    currentValue:
                        stockTakeControllers.currentFilter['warehouse_filter'],
                    label: "Gudang/Lokasi Penyimpanan",
                    hintText: "Pilih Gudang/Lokasi Penyimpanan",
                    asyncItems: (String filter) async {
                      stockTakeControllers.lookupWarehouse(searching: filter);
                      return stockTakeControllers.dataLookupWarehouse;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      // print(data);
                      stockTakeControllers.currentFilter.value = {
                        ...stockTakeControllers.currentFilter,
                        'warehouse_filter': searchValue,
                      };
                    },
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'material',
                    controller: materialFilterController,
                    currentValue:
                        stockTakeControllers.currentFilter['material_filter'],
                    label: "Material",
                    hintText: "Pilih material",
                    asyncItems: (String filter) async {
                      stockTakeControllers.lookupMaterial(searching: filter);
                      return stockTakeControllers.dataLookupMaterial;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      stockTakeControllers.currentFilter.value = {
                        ...stockTakeControllers.currentFilter,
                        'material_filter': searchValue,
                      };
                    },
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.formDateRangePicker2(
                    context: context,
                    title: 'Tanggal',
                    hintText: "Pilih tanggal",
                    fieldName: 'date_range',
                    currentValue: BaseGlobalFuntion.formatDateRange(
                      stockTakeControllers.byDate['start-date'] ?? "",
                      stockTakeControllers.byDate['end-date'] ?? "",
                    ),
                    startDateController: TextEditingController(),
                    endDateController: TextEditingController(),
                    onChange: ({endDate, startDate}) {
                      stockTakeControllers.filterByRangeDate(
                        startDate: startDate,
                        endDate: endDate,
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    final listItemContent = Flexible(
      child: Obx(
        () {
          final result = stockTakeControllers.stockTakeData;
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: result.isEmpty && stockTakeControllers.isLoading.isFalse
                ? SizedBox(
                    height: MediaQuery.of(context).size.height / 1.2,
                    child: Center(
                      child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                    ),
                  )
                : ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: result.length + 1,
                    separatorBuilder: (BuildContext context, int index) =>
                        const SizedBox(height: 5),
                    itemBuilder: (BuildContext context, int index) {
                      if (index < result.length) {
                        final data = result[index];

                        return BaseCardWidget.cardStyle1(
                          ontap: () {
                            Get.to(
                              () => StockTakeDetail(itemId: data['id']),
                            );
                          },
                          title: data['material_name'],
                          subTitle1: data['warehouse_name'],
                          subTitle2: BaseTextWidget.customText(
                            text: data['production_unit_name'],
                            fontWeight: FontWeight.w500,
                            isLongText: true,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                          trailing: BaseTextWidget.customText(
                            text: data['difference'],
                            extendOnFirst: data['difference'] > 0 ? "+" : "",
                            extendText: "\n ${data['uom_name']}",
                            textAlign: TextAlign.right,
                            fontWeight: FontWeight.w600,
                            color: data['difference'] < 0
                                ? AppColor.kFailedColor
                                : AppColor.kSuccesColor,
                          ),
                          headerString: BaseGlobalFuntion.datetimeConvert(
                              data['date'], "dd MMM yyyy HH:mm"),
                        );
                      } else if (result.length < 25) {
                        return stockTakeControllers.hasMore.value &&
                                stockTakeControllers.isLoading.isTrue
                            ? BaseLoadingWidget.cardShimmer(
                                height: MediaQuery.of(context).size.height,
                                shimmerheight: 80,
                                baseColor: AppColor.kSoftGreyColor,
                                highlightColor: AppColor.kWhiteColor,
                                itemCount: 25,
                              )
                            : const SizedBox();
                      } else {
                        return stockTakeControllers.hasMore.value
                            ? BaseLoadingWidget.cardShimmer(
                                height: 200,
                                shimmerheight: 90,
                                baseColor: AppColor.kSoftGreyColor,
                                highlightColor: AppColor.kWhiteColor,
                                itemCount: 2,
                              )
                            : const SizedBox();
                      }
                    },
                  ),
          );
        },
      ),
    );

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
            title: 'Stock Take',
            context: context,
            isAllFill: true,
            searchController: stockTakeControllers.search,
            searchText: 'Cari',
            onFillterChange: () {
              showBottomFilter(context);
            },
            onSearchChange: () {
              BaseGlobalFuntion.debounceRun(
                action: () {
                  stockTakeControllers.refreshStockTake();
                },
                duration: const Duration(milliseconds: 1000),
              );
            }),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              RefreshIndicator(
                onRefresh: onRefresh,
                child: SingleChildScrollView(
                  controller: scrollController,
                  physics: const AlwaysScrollableScrollPhysics(),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const SizedBox(height: 10),
                      listItemContent,
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 30.0,
                right: 15.0,
                child: SizedBox(
                  height: 55.0,
                  width: 55.0,
                  child: IconButton.filled(
                    iconSize: 30,
                    onPressed: () {
                      Get.to(() => const AddStockTake());
                    },
                    icon: const Icon(
                      Icons.add,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
