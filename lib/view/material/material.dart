import 'package:rbs_mobile_operation/data/datasources/material.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/core/components/base_button_widget.dart';
import 'package:rbs_mobile_operation/core/components/base_loading_widget.dart';

class MaterialDataList extends StatelessWidget {
  const MaterialDataList({super.key});

  @override
  Widget build(BuildContext context) {
    MaterialListControllers materialDataControllers =
        Get.put(MaterialListControllers());

    Future onRefresh() async {
      materialDataControllers.refreshMaterialData();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        if (materialDataControllers.materialData.isNotEmpty) {
          materialDataControllers.fetchMaterialData();
        }
      }
    });

    // ** SHOW FILTER MODAL BOTTOM
    TextEditingController materialCategoryFilter = TextEditingController();
    final isActive = ['Semua', 'Aktif', 'Non Aktif'];

    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          materialDataControllers.currentFilter.clear();
          materialDataControllers.filterFormKey.currentState!.patchValue({
            'active': null,
            'material_category_id': null,
          });
        },
        onFilter: () {
          Get.back();
          materialDataControllers.refreshMaterialData();
        },
        valueFilter: materialDataControllers.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: materialDataControllers.filterFormKey,
            onChanged: () {
              materialDataControllers.filterFormKey.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  BaseButtonWidget.dropdownButton2(
                    fieldName: "material_category_id",
                    valueController: materialCategoryFilter,
                    valueData:
                        materialDataControllers.dataLookupMaterialCategory,
                    label: "Kategori material",
                    hintText: materialDataControllers
                            .currentFilter['material_category_filter'] ??
                        "Pilih kategori material",
                    searchOnChange: (p0) {},
                    onChange: ({searchValue, valueItem}) {
                      materialDataControllers.currentFilter.value = {
                        ...materialDataControllers.currentFilter,
                        'material_category_filter': searchValue,
                      };
                    },
                  ),
                  const SizedBox(height: 5),
                  FormBuilderChoiceChip<String>(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      labelText: 'Status',
                      hintText: "Pilih Status",
                      labelStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 14,
                        kfontWeight: FontWeight.w500,
                      ),
                      errorStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 13,
                        kcolor: AppColor.kFailedColor,
                        kfontWeight: FontWeight.w500,
                      ),
                    ),
                    name: 'active',
                    initialValue:
                        materialDataControllers.currentFilter['active'],
                    selectedColor: AppColor.kInactiveColor,
                    spacing: 6.0,
                    options: List.generate(isActive.length, (index) {
                      return FormBuilderChipOption(
                        value: isActive[index],
                        child: Text(
                          isActive[index],
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontWeight: FontWeight.w600,
                            kfontSize: 12,
                          ),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    final listItemContent = Flexible(child: Obx(() {
      final result = materialDataControllers.materialData;

      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: result.isEmpty && materialDataControllers.isLoading.isFalse
            ? SizedBox(
                height: MediaQuery.of(context).size.height / 1.2,
                child: Center(
                  child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                ),
              )
            : ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: materialDataControllers.hasMore.value
                    ? result.length + 1
                    : result.length,
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(height: 5),
                itemBuilder: (BuildContext context, int index) {
                  if (index < result.length) {
                    final data = result[index];
                    var colorBadge =
                        BaseGlobalFuntion.statusColor(result[index]['active']);
                    return BaseCardWidget.cardStyle5(
                      context: context,
                      paddingTop: 7,
                      title: data['name'],
                      subtitle: data['material_category_name'],
                      expandTitle:
                          "MIN STOK :  ${data['min_stock'].toStringAsFixed(2)} ",
                      expandSubtitle:
                          "MAX STOK : ${data['max_stock'].toStringAsFixed(2)}",
                      bottomSubtitle: data['production_unit_name'] +
                          '\n${data['description']}',
                      trailling:
                          "${BaseGlobalFuntion.currencyLocalConvert(nominal: data['unit_price'])}/${data['uom_name']}",
                      traillingTextStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 12,
                        kcolor: AppColor.kSuccesColor,
                        kfontWeight: FontWeight.w500,
                      ),
                      extendWidget: Container(
                        padding: const EdgeInsets.all(4),
                        decoration: BoxDecoration(
                          color: colorBadge['background_color'],
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          data['active'] == '1' ? "Aktif" : "Tidak Aktif",
                          style: BaseTextStyle.customTextStyle(
                            kfontSize: 12,
                            kcolor: colorBadge['text_color'],
                            kfontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    );
                  } else if (result.length < 25) {
                    return materialDataControllers.hasMore.value &&
                            materialDataControllers.isLoading.isTrue
                        ? BaseLoadingWidget.cardShimmer(
                            height: MediaQuery.of(context).size.height,
                            shimmerheight: 80,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 25,
                          )
                        : const SizedBox();
                  } else {
                    return materialDataControllers.hasMore.value
                        ? BaseLoadingWidget.cardShimmer(
                            height: 200,
                            shimmerheight: 90,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 2,
                          )
                        : const SizedBox();
                  }
                },
              ),
      );
    }));

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
            title: 'Data Material',
            context: context,
            isAllFill: true,
            searchController: materialDataControllers.search,
            searchText: 'Cari',
            onFillterChange: () {
              showBottomFilter(context);
            },
            onSearchChange: () {
              BaseGlobalFuntion.debounceRun(
                action: () {
                  materialDataControllers.refreshMaterialData();
                },
                duration: const Duration(milliseconds: 1000),
              );
            }),
        body: Stack(
          children: [
            RefreshIndicator(
              onRefresh: onRefresh,
              child: SingleChildScrollView(
                controller: scrollController,
                physics: const AlwaysScrollableScrollPhysics(),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(height: 10),
                    listItemContent,
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
