import 'package:rbs_mobile_operation/core/components/components.dart';
import 'package:rbs_mobile_operation/core/extentions/date_time_extension.dart';
import 'package:rbs_mobile_operation/data/datasources/salesorder.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/view/app_view_resource.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class SalesOrderList extends StatelessWidget {
  const SalesOrderList({super.key});

  @override
  Widget build(BuildContext context) {
    SalesListController salesOrderController = Get.put(SalesListController());

    Future onRefresh() async {
      salesOrderController.refreshDataOrder();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.position.pixels) {
        if (salesOrderController.orderData.isNotEmpty) {
          salesOrderController.fetchOrderData();
        }
      }
    });

    // WidgetsBinding.instance.addPostFrameCallback((_) {
    // salesOrderController.refreshDataOrder();
    // });

    // ** MODAL SHEET FILTER
    final isActive = ['Semua', 'Sukses', 'Menunggu', 'Diproses', "Dibatalkan"];
    TextEditingController customerFilterController = TextEditingController();
    TextEditingController productFilterController = TextEditingController();
    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          salesOrderController.byDate.clear();
          salesOrderController.currentFilter.clear();
          salesOrderController.salesFormKeyFilter.currentState!.reset();

          salesOrderController.salesFormKeyFilter.currentState!.patchValue({
            'customer': null,
            'status_code': null,
            'product': null,
            'date_range': null,
          });

          // print(salesOrderController.currentFilter);
        },
        onFilter: () {
          Get.back();
          salesOrderController.refreshDataOrder();
        },
        valueFilter: salesOrderController.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: salesOrderController.salesFormKeyFilter,
            onChanged: () {
              salesOrderController.salesFormKeyFilter.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  // Obx(() =>
                  CustomDropDownSearch(
                    fieldName: 'customer',
                    controller: customerFilterController,
                    currentValue:
                        salesOrderController.currentFilter['customer_preview'],
                    label: "Pelanggan",
                    hintText: "Pilih pelanggan",
                    asyncItems: (String filter) async {
                      salesOrderController.lookupCustomer(searching: filter);
                      return salesOrderController.dataCustomer;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      // print(data);
                      salesOrderController.currentFilter.value = {
                        ...salesOrderController.currentFilter,
                        'customer_preview': searchValue,
                      };
                    },
                  ),

                  const SizedBox(height: 5),
                  CustomDropDownSearch(
                    fieldName: 'product',
                    controller: productFilterController,
                    currentValue:
                        salesOrderController.currentFilter['product_preview'],
                    label: "Produk/Mutu",
                    hintText: "Pilih Produk/Mutu",
                    asyncItems: (String filter) async {
                      salesOrderController.lookupProduct(searching: filter);
                      return salesOrderController.dataProduct;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      // print(data);
                      salesOrderController.currentFilter.value = {
                        ...salesOrderController.currentFilter,
                        'product_preview': searchValue,
                      };
                    },
                  ),

                  const SizedBox(height: 5),
                  BaseButtonWidget.formDateRangePicker2(
                    context: context,
                    fieldName: 'date_range',
                    title: "Tanggal",
                    hintText: "Pilih tanggal",
                    currentValue: BaseGlobalFuntion.formatDateRange(
                      salesOrderController.byDate['start-date'] ?? "",
                      salesOrderController.byDate['end-date'] ?? "",
                    ),
                    startDateController: TextEditingController(),
                    endDateController: TextEditingController(),
                    onChange: ({endDate, startDate}) {
                      salesOrderController.filterByRangeDate(
                        startDate: startDate,
                        endDate: endDate,
                      );
                    },
                  ),
                  FormBuilderChoiceChip<String>(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      labelText: 'Status ',
                      hintText: "Pilih Status",
                      contentPadding: const EdgeInsets.symmetric(vertical: 2),
                      labelStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 15,
                        kfontWeight: FontWeight.w500,
                        kcolor: AppColor.kPrimaryColor,
                      ),
                      errorStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 13,
                        kcolor: AppColor.kFailedColor,
                        kfontWeight: FontWeight.w500,
                      ),
                    ),
                    name: 'status_code',
                    initialValue:
                        salesOrderController.currentFilter['status_code'],
                    selectedColor: AppColor.kInactiveColor,
                    spacing: 6.0,
                    options: List.generate(isActive.length, (index) {
                      return FormBuilderChipOption(
                        value: isActive[index],
                        child: Text(
                          isActive[index],
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontWeight: FontWeight.w600,
                            kfontSize: 12,
                          ),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    // ** LIST OF CONTENT CARD
    final listItemContent = Flexible(
      child: Obx(
        () {
          final result = salesOrderController.orderData;

          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: result.isEmpty && salesOrderController.isLoading.isFalse
                ? SizedBox(
                    height: MediaQuery.of(context).size.height / 1.2,
                    child: Center(
                      child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                    ),
                  )
                : ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: result.length + 1,
                    separatorBuilder: (BuildContext context, int index) =>
                        const SizedBox(height: 3),
                    itemBuilder: (BuildContext context, int index) {
                      if (index < result.length) {
                        final data = result[index];
                        var colorBadge = BaseGlobalFuntion.statusColor(
                            result[index]['status_code']);

                        return BaseCardWidget.cardStyle1(
                          ontap: () {
                            Get.to(
                              () => SalesOrderView(
                                  itemId: data['sales_order_id']),
                            );
                          },
                          title: data['product_name'],
                          subTitle1: data['order_number'],
                          subTitle2: BaseTextWidget.customText(
                            text: data['customer_project_name'],
                            fontSize: 12,
                            isLongText: true,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                          trailing: BaseTextWidget.customText(
                            text: data['quantity_order'],
                            extendText: "\n${data['uom_name']}",
                            textAlign: TextAlign.center,
                          ),
                          statusHeader: {
                            'title': data['status_code'] ?? "",
                            'color': colorBadge['background_color'],
                            'titleColor': colorBadge['text_color'],
                          },
                          headerString:
                              DateTime.parse(data['date']).toFormattedDate(),
                          // BaseGlobalFuntion.datetimeConvert(
                          //     data['date'], "dd MMMM yyyy"),
                        );
                      } else if (result.length < salesOrderController.limit) {
                        return salesOrderController.hasMore.value &&
                                salesOrderController.isLoading.isTrue
                            ? BaseLoadingWidget.cardShimmer(
                                height: MediaQuery.of(context).size.height,
                                shimmerheight: 80,
                                baseColor: AppColor.kSoftGreyColor,
                                highlightColor: AppColor.kWhiteColor,
                                itemCount: 25,
                              )
                            : const SizedBox();
                      } else {
                        return salesOrderController.hasMore.value
                            ? BaseLoadingWidget.cardShimmer(
                                height: 200,
                                shimmerheight: 90,
                                baseColor: AppColor.kSoftGreyColor,
                                highlightColor: AppColor.kWhiteColor,
                                itemCount: 2,
                              )
                            : const SizedBox();
                      }
                    },
                  ),
          );
        },
      ),
    );

    // ** CUSTOM CARD MONITORING SUMMARY
    Widget cardSummary({volume, send, unsend}) {
      return Row(
        children: [
          BaseCardWidget.cardMonitoring(
            content: BaseGlobalFuntion.currencyLocalConvert(
              nominal: volume,
              isLocalCurency: false,
            ),
            title: "Total Order",
            kcontentColor: AppColor.kPrimaryColor,
          ),
          const SizedBox(width: 2),
          BaseCardWidget.cardMonitoring(
            content: BaseGlobalFuntion.currencyLocalConvert(
              nominal: send,
              isLocalCurency: false,
            ),
            title: "Produksi",
            kcontentColor: AppColor.kSuccesColor,
          ),
          const SizedBox(width: 2),
          BaseCardWidget.cardMonitoring(
            content: BaseGlobalFuntion.currencyLocalConvert(
              nominal: unsend,
              isLocalCurency: false,
            ),
            title: "Rencana Produksi",
            kcontentColor: AppColor.kFailedColor,
          ),
        ],
      );
    }

    // ** CUSTOM LOADING CARD SUMMARY
    Widget loadingCardSummary({itemCount}) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: List.generate(
          itemCount,
          (index) => Expanded(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 5),
              width: double.infinity,
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: AppColor.kWhiteColor,
                borderRadius: const BorderRadius.all(Radius.circular(8)),
                border: Border.all(color: AppColor.kAvailableColor),
              ),
              child: Shimmer.fromColors(
                  baseColor: AppColor.kSoftGreyColor,
                  highlightColor: AppColor.kWhiteColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 40,
                        height: 18,
                        decoration: BoxDecoration(
                          color: Colors.amber,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                      const SizedBox(height: 5),
                      Container(
                        width: 80,
                        height: 18,
                        decoration: BoxDecoration(
                          color: Colors.amber,
                          borderRadius: BorderRadius.circular(4),
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        ),
      );
    }

    final summaryCard = Positioned(
      top: 10.0,
      left: 0.0,
      right: 0.0,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 5.0),
        child: Obx(() {
          final data = salesOrderController.summaryOrder.value;
          if (salesOrderController.orderData.isEmpty) {
            return salesOrderController.hasMore.value
                ? loadingCardSummary(itemCount: 3)
                : cardSummary(
                    volume: "0",
                    send: "0",
                    unsend: "0",
                  );
          } else {
            return cardSummary(
              volume: data['production_quantity'],
              send: data['order_quantity'],
              unsend: data['remaining_quantity'],
            );
          }
        }),
      ),
    );

    // ** CONTENT STRUCTURE STYLE
    final topContent = SizedBox(
      height: 80,
      width: double.infinity,
      child: SizedBox(
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: [
            Container(
              height: 50,
              padding: const EdgeInsets.only(left: 10, right: 10, top: 15),
              decoration: const BoxDecoration(
                color: AppColor.kPrimaryColor,
                gradient: RadialGradient(
                  radius: 1.2,
                  center: Alignment(0.2, 1.4),
                  colors: [
                    AppColor.kSoftBlueColor,
                    AppColor.kPrimaryColor,
                  ],
                ),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
              ),
            ),
            summaryCard,
          ],
        ),
      ),
    );

    // ** MAIN RETURN WIDGET
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: BaseExtendWidget.appBar(
            title: 'Sales Order',
            context: context,
            isAllFill: true,
            searchController: salesOrderController.search,
            searchText: 'Cari No.Order',
            onFillterChange: () {
              showBottomFilter(context);
            },
            onBack: () => Get.back(),
            onSearchChange: () {
              BaseGlobalFuntion.debounceRun(
                action: () {
                  salesOrderController.refreshDataOrder();
                },
                duration: const Duration(milliseconds: 1000),
              );
            }),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              RefreshIndicator(
                onRefresh: onRefresh,
                child: SingleChildScrollView(
                  controller: scrollController,
                  physics: const AlwaysScrollableScrollPhysics(),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      topContent,
                      const SizedBox(height: 0),
                      listItemContent,
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 30.0,
                right: 15.0,
                child: SizedBox(
                  height: 55.0,
                  width: 55.0,
                  child: IconButton.filled(
                    iconSize: 30,
                    onPressed: () {
                      Get.to(() => const AddSalesOrder());
                    },
                    icon: const Icon(
                      Icons.add,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
