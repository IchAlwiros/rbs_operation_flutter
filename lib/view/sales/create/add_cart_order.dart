import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';
import 'package:rbs_mobile_operation/data/datasources/salesorder.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/app_view_resource.dart';

class AddSalesOrder extends StatelessWidget {
  const AddSalesOrder({super.key});

  @override
  Widget build(BuildContext context) {
    // ProductControllers productControllers = Get.put(ProductControllers());
    // MasterControllers masterControllers = Get.put(MasterControllers());
    AddSalesControllers salesController = Get.put(AddSalesControllers());

    Future onRefresh() async {
      salesController.refreshDataProduct();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        salesController.fetchProduct();
      }
    });

    // TextEditingController puTextEditingController = TextEditingController();

    Future<void> showDialogBuilder(BuildContext context) {
      return showModalBottomSheet<void>(
          context: context,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(top: Radius.circular(15.0))),
          builder: (BuildContext context) {
            return SizedBox(
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 15,
                  vertical: 10,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Keranjang",
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontSize: 15,
                            kfontWeight: FontWeight.w600,
                          ),
                        ),
                        IconButton.filledTonal(
                          onPressed: () {
                            Get.back();
                          },
                          icon: const Icon(Icons.close),
                        ),
                      ],
                    ),
                    const Divider(),
                    Obx(() {
                      final resultCart = salesController.cart;
                      return resultCart.isEmpty
                          ? DottedBorder(
                              borderType: BorderType.RRect,
                              dashPattern: const [8, 8],
                              radius: const Radius.circular(8),
                              padding: const EdgeInsets.symmetric(
                                horizontal: 12,
                                vertical: 50,
                              ),
                              color: AppColor.kGreyColor,
                              strokeWidth: 2,
                              child: const Center(
                                child: Text(
                                  'Belum ada produk yang dipilih',
                                  style: TextStyle(
                                    color: AppColor.kGreyColor,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            )
                          : Flexible(
                              child: ListView.separated(
                                shrinkWrap: true,
                                physics: const AlwaysScrollableScrollPhysics(),
                                itemCount: resultCart.length,
                                separatorBuilder: (context, index) =>
                                    const SizedBox(height: 5),
                                itemBuilder: (context, index) {
                                  return ListTile(
                                    shape: RoundedRectangleBorder(
                                      side: const BorderSide(
                                        width: 1.5,
                                        color: AppColor.kGreyColor,
                                      ),
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    title: BaseTextWidget.customText(
                                      text: resultCart[index]['product_name'],
                                      isLongText: true,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      color: AppColor.kPrimaryColor,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    subtitle: Text.rich(TextSpan(
                                      children: [
                                        BaseTextWidget.customTextSpan(
                                          text: resultCart[index]['unit_price'],
                                          extendText:
                                              " x ${BaseGlobalFuntion.currencyLocalConvert(nominal: resultCart[index]['quantity'], isLocalCurency: false)} \n",
                                          localCurency: true,
                                        ),
                                        BaseTextWidget.customTextSpan(
                                          text: (resultCart[index]
                                                  ['unit_price'] *
                                              resultCart[index]['quantity']),
                                          localCurency: true,
                                        ),
                                      ],
                                    )),
                                    trailing: IconButton(
                                      onPressed: () {
                                        salesController.deleteItemCart(index);
                                        if (salesController.cart.isEmpty) {
                                          Get.back();
                                        }
                                      },
                                      icon: const Icon(
                                        Icons.delete,
                                        size: 15,
                                        color: AppColor.kFailedColor,
                                      ),
                                    ),
                                  );
                                },
                              ),
                            );
                    }),
                    const Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text.rich(TextSpan(
                          children: [
                            TextSpan(
                                text: 'Total Volume\n',
                                style: BaseTextStyle.customTextStyle(
                                  kfontSize: 13,
                                  kcolor: AppColor.kPrimaryColor,
                                  kfontWeight: FontWeight.w600,
                                )),
                            TextSpan(
                                text: 'Total Harga',
                                style: BaseTextStyle.customTextStyle(
                                  kfontSize: 13,
                                  kcolor: AppColor.kPrimaryColor,
                                  kfontWeight: FontWeight.w600,
                                )),
                          ],
                        )),
                        Obx(() {
                          final resultCartSummary = salesController.cartSummary;

                          return Text.rich(TextSpan(
                            children: [
                              BaseTextWidget.customTextSpan(
                                text: resultCartSummary['volume_total'],
                                extendText: ' M3\n',
                              ),
                              BaseTextWidget.customTextSpan(
                                text: BaseGlobalFuntion.currencyLocalConvert(
                                    nominal:
                                        resultCartSummary['price_total'] ?? 0),
                                extendText: '',
                              ),
                            ],
                          ));
                        })
                      ],
                    ),
                  ],
                ),
              ),
            );
          });
    }

    TextEditingController castingTypeControllers = TextEditingController();

    final listCardContent = Flexible(child: Obx(() {
      final resultProduct = salesController.productData;

      return RefreshIndicator(
        onRefresh: onRefresh,
        child: resultProduct.isEmpty && salesController.isLoading.isFalse
            ? SizedBox(
                height: MediaQuery.of(context).size.height / 1.2,
                child: Center(
                  child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                ),
              )
            : ListView.separated(
                controller: scrollController,
                itemCount: resultProduct.length + 1,
                physics: const AlwaysScrollableScrollPhysics(),
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(height: 8),
                itemBuilder: (BuildContext context, int index) {
                  if (index < resultProduct.length) {
                    final data = resultProduct[index];
                    return BaseCardWidget.cardStyle4(
                      ontap: () {
                        final itemInfo = Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Flexible(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      BaseTextWidget.customText(
                                        text: data['name'],
                                        fontSize: 12,
                                        fontWeight: FontWeight.w600,
                                        color: AppColor.kPrimaryColor,
                                        isLongText: true,
                                      ),
                                      BaseTextWidget.customText(
                                        text: data['code'],
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        color: AppColor.kPrimaryColor,
                                      ),
                                    ],
                                  ),
                                ),

                                // Text(int.parse("${data['unit_price']}")
                                //     .currencyFormatRp)

                                BaseTextWidget.customText(
                                  text:
                                      // int.parse(data['unit_price'])
                                      //     .currencyFormatRp,
                                      BaseGlobalFuntion.currencyLocalConvert(
                                          nominal: data['unit_price'] ?? 0),
                                  fontSize: 14,
                                  fontWeight: FontWeight.w700,
                                  color: AppColor.kPrimaryColor,
                                ),
                              ],
                            ),
                            BaseTextWidget.customText(
                              text: data['product_category_name'] ?? "",
                              fontSize: 12,
                              fontWeight: FontWeight.w500,
                              color: AppColor.kPrimaryColor,
                            ),
                            const Divider(),
                          ],
                        );

                        final formList = Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            FormBuilder(
                              key: salesController.salesKeyAddOrder,
                              onChanged: () {
                                salesController.salesKeyAddOrder.currentState!
                                    .save();
                              },
                              autovalidateMode: AutovalidateMode.disabled,
                              skipDisabled: true,
                              child: Flexible(
                                child: ListView(
                                  shrinkWrap: true,
                                  children: [
                                    Obx(() => salesController
                                                .productionUnit.isEmpty ||
                                            salesController
                                                .dataLookupCastingType.isEmpty
                                        ? BaseTextFieldWidget.disableTextForm2(
                                            disabledText:
                                                'Pilih tipe pengecoran',
                                            isRequired: true,
                                            validator: (value) {
                                              if (value == null ||
                                                  value.isEmpty) {
                                                return 'Tipe pengecoran wajib diisi!';
                                              }
                                              return null;
                                            },
                                          )
                                        :

                                        // BaseButtonWidget.dropdownButton2(
                                        //     fieldName: "casting_category",
                                        //     valueController:
                                        //         TextEditingController(),
                                        //     valueData: salesController
                                        //         .dataLookupCastingType,
                                        //     hintText: "Pilih tipe pengecoran",
                                        //     label: 'Tipe pengecoran',
                                        //     isRequired: true,
                                        //     searchOnChange: (p0) {},
                                        //     onChange: (
                                        //         {searchValue, valueItem}) {},
                                        //     validator: (value) {
                                        //       if (value == null ||
                                        //           value.isEmpty) {
                                        //         return 'tipe pengecoran wajib diisi!';
                                        //       }
                                        //       return null;
                                        //     },
                                        //   ),
                                        CustomDropDownSearch(
                                            fieldName: 'casting_category',
                                            controller: castingTypeControllers,
                                            isRequired: true,
                                            label: "Tipe pengecoran",
                                            hintText: "Pilih tipe pengecoran",
                                            asyncItems: (String filter) async {
                                              salesController.lookupCastingType(
                                                  searching: filter);
                                              return salesController
                                                  .dataLookupCastingType;
                                            },
                                            onChange: (
                                                {searchValue,
                                                valueItem,
                                                data}) {},
                                            validator: (value) {
                                              if (value == null ||
                                                  value.isEmpty) {
                                                return 'Tipe pengecoran wajib diisi!';
                                              }
                                              return null;
                                            },
                                          )),
                                    const SizedBox(height: 5),
                                    CustomTextField.style2(
                                      title: 'Harga Satuan',
                                      fieldName: 'price',
                                      isRequired: true,
                                      onClearText: () => salesController
                                          .salesKeyAddOrder.currentState!
                                          .patchValue({'price': null}),
                                      hintText:
                                          '${BaseGlobalFuntion.currencyLocalConvert(nominal: data['unit_price'], isLocalCurency: false)}',
                                      // initialValue:
                                      //     BaseGlobalFuntion.currencyLocalConvert(
                                      //         nominal: data['unit_price'],
                                      //         isLocalCurency: false),
                                      onChange: ({value}) {},
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Harga wajib diisi!';
                                        }
                                        return null;
                                      },
                                      keyboardType: TextInputType.number,
                                      textInputAction: TextInputAction.next,
                                      inputFormater: <TextInputFormatter>[
                                        FilteringTextInputFormatter.allow(
                                            RegExp(r'[0-9]')),
                                        FilteringTextInputFormatter.digitsOnly,
                                        BaseGlobalFuntion
                                            .rupiahSeparatorInputFormatter,
                                      ],
                                    ),
                                    const SizedBox(height: 5),
                                    Obx(() => salesController
                                            .productionUnit.isEmpty
                                        ? BaseTextFieldWidget.disableTextForm2(
                                            disabledText:
                                                'Masukan jumlah volume',
                                            label: "Volume",
                                            isRequired: true,
                                            validator: (value) {
                                              if (value == null ||
                                                  value.isEmpty) {
                                                return 'jumlah volume wajib diisi!';
                                              }
                                              return null;
                                            },
                                          )
                                        : CustomTextField.style2(
                                            title: 'Volume',
                                            hintText: "100.00",
                                            fieldName: 'volume',
                                            isRequired: true,
                                            onClearText: () => salesController
                                                .salesKeyAddOrder.currentState!
                                                .patchValue({'volume': null}),
                                            onChange: ({value}) {},
                                            extendSuffixItem: Container(
                                              margin: const EdgeInsets.all(1),
                                              padding: const EdgeInsets.all(14),
                                              decoration: const BoxDecoration(
                                                color: AppColor.kGreyColor,
                                                borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(7),
                                                  bottomRight:
                                                      Radius.circular(7),
                                                ),
                                              ),
                                              child: Text(
                                                "M3",
                                                style: BaseTextStyle
                                                    .customTextStyle(
                                                  kcolor:
                                                      AppColor.kPrimaryColor,
                                                  kfontWeight: FontWeight.w500,
                                                  kfontSize: 15,
                                                ),
                                              ),
                                            ),
                                            validator: (value) {
                                              if (value == null ||
                                                  value.isEmpty) {
                                                return 'Volume wajib diisi!';
                                              }

                                              if (double.parse(value) < 0.25) {
                                                return 'Volume (M3) tidak boleh kurang dari 0.25';
                                              }

                                              return null;
                                            },
                                            textInputAction:
                                                TextInputAction.done,
                                            // keyboardType: TextInputType.number,
                                            // //  const TextInputType
                                            // //     .numberWithOptions(
                                            // //     decimal: true),
                                            // inputFormater: <TextInputFormatter>[
                                            //   FilteringTextInputFormatter.allow(
                                            //       RegExp(r'^[0-9.]+$')),
                                            // ],
                                            keyboardType: const TextInputType
                                                .numberWithOptions(
                                                decimal: true),
                                            inputFormater: [
                                              FilteringTextInputFormatter.allow(
                                                  RegExp(r'^\d*\.?\d*')),
                                            ],
                                          )),
                                    const SizedBox(height: 10),
                                  ],
                                ),
                              ),
                            ),
                            Button.filled(
                              onPressed: () {
                                salesController.addSalesOrder(
                                  extendData: {
                                    "product_id": data['id'],
                                    "product_name": data['name'],
                                    "category_name":
                                        data['product_category_name'],
                                    "product_category_id":
                                        data['product_category_id'],
                                  },
                                );
                              },
                              color: AppColor.kPrimaryColor,
                              // height: 50,
                              // rounded: 10,
                              icon: const Icon(
                                Icons.add_shopping_cart_rounded,
                                color: AppColor.kWhiteColor,
                              ),
                              // textStyle: BaseTextStyle.customTextStyle(
                              //   kfontWeight: FontWeight.w600,
                              //   kfontSize: 12,
                              //   kcolor: AppColor.kWhiteColor,
                              // ),
                              // width: double.infinity,
                              label: "Tambah Produk",
                            ),
                            const SizedBox(height: 15),
                          ],
                        );

                        _showBottomAddCart(context, [
                          itemInfo,
                          formList,
                        ]);
                      },
                      title: data['name'],
                      subtitle: data['code'],
                      bottomSubtitle: BaseTextWidget.customText(
                        text: data['product_category_name'],
                        isLongText: true,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                      trailling: BaseTextWidget.customText(
                        text: BaseGlobalFuntion.currencyLocalConvert(
                          nominal: data['unit_price'],
                        ),
                        color: AppColor.kSuccesColor,
                        fontWeight: FontWeight.w600,
                        textAlign: TextAlign.right,
                      ),
                    );
                  } else if (resultProduct.length < 25) {
                    return salesController.hasMore.value &&
                            salesController.isLoading.isTrue
                        ? BaseLoadingWidget.cardShimmer(
                            height: MediaQuery.of(context).size.height,
                            shimmerheight: 80,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 25,
                          )
                        : const SizedBox();
                  } else {
                    return salesController.hasMore.value
                        ? BaseLoadingWidget.cardShimmer(
                            height: 200,
                            shimmerheight: 90,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 2,
                          )
                        : const SizedBox();
                  }
                },
              ),
      );
    }));

    Widget fillList() {
      return Obx(() {
        var dataFilter = BaseGlobalFuntion.listStringFuntion(List.generate(
          salesController.dataLookupProductCategory.length,
          (index) {
            final name = BaseGlobalFuntion.fillManange(
                salesController.dataLookupProductCategory)[index]['name'];
            final id = BaseGlobalFuntion.fillManange(
                salesController.dataLookupProductCategory)[index]['id'];
            return {
              'key': name,
              'value': () {
                salesController.currentFilter['product_category_id'] = id;
                salesController.refreshDataProduct();
              }
            };
          },
        ));

        if (dataFilter.isNotEmpty) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 2),
            // child: ShaderMask(
            //   shaderCallback: (Rect bounds) {
            //     return LinearGradient(
            //       colors: [Colors.white, Colors.white.withOpacity(0.05)],
            //       stops: [0.7, 1],
            //       tileMode: TileMode.mirror,
            //     ).createShader(bounds);
            //   },
            //   blendMode: BlendMode.dstOut,
            child: BaseButtonWidget.buttonFilter(
              title: List.generate(
                dataFilter.length,
                (index) => dataFilter[index]['string'],
              ),
              onPressed: List.generate(
                dataFilter.length,
                (index) => dataFilter[index]['action'],
              ),
              initialActive: 1,
              bColor: AppColor.kInactiveColor,
              contentColor: AppColor.kPrimaryColor,
              height: 10,
              // width: 95,
            ),
            // ),
          );
        } else {
          return BaseLoadingWidget.cardShimmer(
            baseColor: AppColor.kSoftGreyColor,
            highlightColor: AppColor.kWhiteColor,
            itemCount: 1,
            shimmerheight: 50,
            height: 50,
            width: MediaQuery.of(context).size.width / 1.2,
          );
        }
      });
    }

    final listFilter = Container(
      padding: const EdgeInsets.symmetric(vertical: 0),
      child: Row(
        children: [
          Expanded(
              child: ShaderMask(
            shaderCallback: (Rect bounds) {
              return LinearGradient(
                colors: [
                  Colors.white,
                  AppColor.kSoftGreyColor.withOpacity(0.1)
                ],
                stops: const [0.9, 1],
                tileMode: TileMode.mirror,
              ).createShader(bounds);
            },
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              physics: const BouncingScrollPhysics(),
              child: Row(
                children: [
                  fillList(),
                ],
              ),
            ),
          ))
        ],
      ),
    );

    final submitAction = Padding(
      // padding: MediaQuery.of(context).viewInsets,
      padding: const EdgeInsets.all(15),
      child: Row(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Obx(
            () => GestureDetector(
              onTap: () {
                showDialogBuilder(context);
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    children: [
                      Text("${salesController.cart.length} Produk",
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontSize: 14,
                            kfontWeight: FontWeight.w700,
                          )),
                      GestureDetector(
                        onTap: () => showDialogBuilder(context),
                        child: const Icon(
                          Icons.info_outline_rounded,
                          size: 16,
                        ),
                      ),
                    ],
                  ),
                  BaseTextWidget.customText(
                    text: salesController.cartSummary['volume_total'],
                    extendText: 'M3',
                    fontSize: 14,
                    color: AppColor.kPrimaryColor,
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(width: 40),
          Expanded(
            child: Obx(() => Directionality(
                  textDirection: TextDirection.rtl,
                  child: Button.filled(
                    onPressed: () {
                      Get.to(() => const CartOrder());
                    },
                    color: AppColor.kPrimaryColor,
                    // height: 45,
                    icon: const Icon(
                      Icons.arrow_circle_right,
                      color: AppColor.kWhiteColor,
                    ),
                    fontSize: 13,
                    // rightIcon: true,
                    // textStyle: BaseTextStyle.customTextStyle(
                    //   kfontWeight: FontWeight.w600,
                    //   kfontSize: 12,
                    //   kcolor: AppColor.kWhiteColor,
                    // ),
                    label: "Selanjutnya",
                    disabled: salesController.cart.isEmpty,
                    // isLoading: false,
                    // isLoading: salesController.cart.isNotEmpty ? false : true,
                  ),
                )),
          ),
        ],
      ),
    );

    if (salesController.productionUnit.isEmpty) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (ApiService.productionUnitGlobal != null ||
            LocalDataSource.getLocalVariable(
                  key: 'lastestProduction',
                ) !=
                null) {
          salesController.productionUnit.value = {
            'production_unit_id': ApiService.productionUnitGlobal?['id'] ??
                LocalDataSource.getLocalVariable(
                  key: 'lastestProduction',
                )?['id'],
            'production_unit_name': ApiService.productionUnitGlobal?['name'] ??
                LocalDataSource.getLocalVariable(
                  key: 'lastestProduction',
                )?['name'],
          };
        } else {
          _showBottomFilter(context, false, true);
        }
      });
    }

    return SafeArea(
      child: PopScope(
        canPop: true,
        onPopInvoked: (didPop) {
          salesController.cart.clear();
          salesController.cartSummary.clear();
        },
        child: LayoutBuilder(
          builder: (context, constraint) => Scaffold(
            appBar: BaseExtendWidget.appBar(
              title: 'Pilih Produk/Mutu',
              context: context,
              extendBack: () {
                salesController.cart.clear();
                salesController.cartSummary.clear();
              },
              searchController: salesController.search,
              // isAllFill: true,
              isSearchFill: true,
              onSearchChange: () {
                BaseGlobalFuntion.debounceRun(
                  action: () {
                    salesController.refreshDataProduct();
                  },
                  duration: const Duration(milliseconds: 1000),
                );
              },
            ),
            body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 10),
              child: Column(
                children: [
                  listFilter,
                  const SizedBox(height: 5),
                  listCardContent,
                ],
              ),
            ),
            resizeToAvoidBottomInset: false,
            bottomNavigationBar: submitAction,
          ),
        ),
      ),
    );
  }

  Future<void> _showBottomAddCart(context, itemContent) {
    return showModalBottomSheet(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(15.0))),
      backgroundColor: AppColor.kWhiteColor,
      // backgroundColor: Colors.transparent,
      context: context,
      isScrollControlled: true,
      builder: (context) {
        var bottom = MediaQuery.of(context).viewInsets.bottom;
        return Padding(
          padding: EdgeInsets.only(
            top: 10,
            right: 10,
            left: 10,
            bottom: bottom,
            // bottom: MediaQuery.of(context).viewInsets.bottom,
            // bottom: MediaQuery.viewInsetsOf(context).bottom,
            // bottom: MediaQueryData.viewInset()
            // bottom: MediaQuery.sizeOf(context).aspectRatio,
          ),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Detail Order",
                      style: BaseTextStyle.customTextStyle(
                        kfontSize: 14,
                        kfontWeight: FontWeight.w600,
                      ),
                    ),
                    IconButton.filledTonal(
                      onPressed: () {
                        Get.back();
                      },
                      icon: const Icon(Icons.close),
                    ),
                  ],
                ),
                const Divider(),
                ...itemContent,
              ],
            ),
          ),
        );
      },
    );
  }

  Future<void> _showBottomFilter(
    BuildContext context,
    bool isDismissible,
    bool isFirst,
  ) {
    AddSalesControllers salesController = Get.put(AddSalesControllers());
    TextEditingController puTextEditingController = TextEditingController();
    return showModalBottomSheet<void>(
      context: context,
      isDismissible: isDismissible,
      shape: isFirst
          ? const RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(top: Radius.circular(15.0)))
          : null,
      backgroundColor:
          isFirst ? AppColor.kWhiteColor : AppColor.kGreyColor.withOpacity(0.1),
      builder: (BuildContext context) {
        return PopScope(
          canPop: isDismissible,
          child: isFirst
              ? Obx(
                  () => Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 12, vertical: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Pilih Unit Produksi",
                              style: BaseTextStyle.customTextStyle(
                                kcolor: AppColor.kPrimaryColor,
                                kfontSize: 14,
                                kfontWeight: FontWeight.w600,
                              ),
                            ),
                            IconButton.filledTonal(
                              onPressed: () {
                                Get.back();
                                if (isFirst) {
                                  Get.back();
                                }
                              },
                              icon: const Icon(Icons.close),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 10),
                      Flexible(
                        child: salesController.dataLookupPU.isEmpty
                            ? salesController.isLoading.value
                                ? const SpinKitThreeBounce(
                                    size: 40.0,
                                    color: AppColor.kPrimaryColor,
                                  )
                                : BaseLoadingWidget.noMoreDataWidget(
                                    height: 100)
                            : ListView.separated(
                                shrinkWrap: true,
                                physics: const AlwaysScrollableScrollPhysics(),
                                itemBuilder: (context, index) {
                                  final result = salesController.dataLookupPU;

                                  return ListTile(
                                      onTap: () {
                                        //** DELETE TEMPORARY DATA
                                        salesController.cartSummary.clear();
                                        salesController.cart.clear();
                                        salesController.dataLookupCastingType
                                            .clear();

                                        //** ADDED VALUE SELECTION
                                        salesController.productionUnit.value = {
                                          'production_unit_id': result[index]
                                              ['id'],
                                          'production_unit_name': result[index]
                                              ['name'],
                                        };

                                        //** SET SALESMAN DATA
                                        salesController.dataLookupSalesman
                                            .clear();
                                        salesController.lookupSalesman();

                                        //** SET CUSTOMER DATA
                                        salesController.dataLookupCustomer
                                            .clear();
                                        salesController.lookupCustomer();

                                        //** SET CUSTOMER DATA
                                        salesController
                                            .dataLookupCustomerProject
                                            .clear();
                                        salesController.lookupCustomerProject();

                                        // ** SET PRODUCT CATEGORY DATA
                                        salesController.lookupProductCategory(
                                            productionUnitId: result[index]
                                                ['id']);

                                        //** LOAD NEW DATA
                                        salesController.refreshDataProduct();
                                        salesController.lookupCastingType();

                                        //** BACK
                                        Get.back();
                                      },
                                      title: Text(
                                        result[index]['name'],
                                        style: BaseTextStyle.customTextStyle(
                                          kcolor: AppColor.kPrimaryColor,
                                        ),
                                      ));
                                },
                                separatorBuilder: (context, index) =>
                                    const SizedBox(height: 2),
                                itemCount: salesController.dataLookupPU.length,
                              ),
                      ),
                    ],
                  ),
                )
              : Container(
                  // height: 200,
                  padding: const EdgeInsets.all(10),
                  margin: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: AppColor.kWhiteColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: FormBuilder(
                    autovalidateMode: AutovalidateMode.disabled,
                    skipDisabled: true,
                    child: Obx(
                      () => SingleChildScrollView(
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Pilih Filter",
                                  style: BaseTextStyle.customTextStyle(
                                    kcolor: AppColor.kPrimaryColor,
                                    kfontSize: 14,
                                    kfontWeight: FontWeight.w600,
                                  ),
                                ),
                                IconButton.filledTonal(
                                  iconSize: 20,
                                  onPressed: () {
                                    Get.back();
                                    if (isFirst) {
                                      Get.back();
                                    }
                                  },
                                  icon: const Icon(Icons.close),
                                ),
                              ],
                            ),
                            const SizedBox(height: 10),
                            BaseButtonWidget.dropdownButton(
                              fieldName: "production_unit_id",
                              valueController: puTextEditingController,
                              valueData: salesController.dataLookupPU,
                              hintText: salesController
                                      .currentFilter['production_unit_name'] ??
                                  "Pilih unit produksi",
                              label: "Pilih unit produksi",
                              searchOnChange: (p0) {},
                              onChange: ({searchValue, valueItem}) {
                                //** DELETE TEMPORARY DATA
                                salesController.cartSummary.clear();
                                salesController.cart.clear();
                                salesController.dataLookupCastingType.clear();

                                //** ADDED VALUE SELECTION
                                salesController.productionUnit.value = {
                                  'production_unit_id': valueItem,
                                  'production_unit_name': searchValue,
                                };

                                //** SET SALESMAN DATA
                                salesController.dataLookupSalesman.clear();
                                salesController.lookupSalesman();

                                //** LOAD NEW DATA
                                salesController.refreshDataProduct();
                                salesController.lookupCastingType();

                                //** BACK
                                Get.back();
                              },
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Please enter some text';
                                }
                                return null;
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
        );
      },
    );
  }
}
