import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:rbs_mobile_operation/data/datasources/salesorder.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class CartOrder extends StatelessWidget {
  const CartOrder({super.key});

  @override
  Widget build(BuildContext context) {
    AddSalesControllers salesController = Get.put(AddSalesControllers());

    var isPPN = [
      {'text': 'Ya', 'value': '1', 'isActive': false},
      {'text': 'Tidak', 'value': '0', 'isActive': true}
    ].obs;
    var automaticPayment = [
      {'text': 'Aktif', 'value': '1', 'isActive': false},
      {'text': 'Tidak Aktif', 'value': '0', 'isActive': true},
    ].obs;
    var isAutoActive = false.obs;
    // var isTop = false.obs;

    // print(salesController.currentDataTemp);

    var isReloadList = true.obs;
    var isTriggerOther = false.obs;

    final topContent = Container(
      height: 190,
      width: double.infinity,
      padding: const EdgeInsets.only(right: 20, left: 20, top: 15, bottom: 50),
      decoration: const BoxDecoration(
        color: AppColor.kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: AppColor.kWhiteColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: const Icon(
              Icons.fire_truck_rounded,
            ),
          ),
          const SizedBox(width: 15),
          Column(
            children: [
              Text(
                "${salesController.cartSummary['production_unit_name']}",
                style: BaseTextStyle.customTextStyle(
                  kcolor: AppColor.kWhiteColor,
                  kfontSize: 14,
                  kfontWeight: FontWeight.w700,
                ),
              ),
            ],
          )
        ],
      ),
    );

    // JIKA PPN DEFAULT PERTAMA DI INISIASI
    if (salesController.currentDataTemp.isEmpty) {
      Future.delayed(const Duration(milliseconds: 200), () {
        salesController.previewCheckoutSalesOrder();
      });
    }

    // JIKA TEMPORATY DATA TERISI
    if (salesController.currentDataTemp.isNotEmpty) {
      // salesController.lookupCustomerProject(
      //     customerId: salesController.currentDataTemp['pelanggan']?['id']);
      // salesController.fetchDetailCustomer(
      //     salesController.currentDataTemp['pelanggan']?['id']);
      salesController.currentDataTemp.forEach((key, value) {
        switch (key) {
          case 'ppn':
            for (var obj in isPPN) {
              if (obj['value'].toString() ==
                  salesController.currentDataTemp['ppn']) {
                obj['isActive'] = true;
              } else {
                obj['isActive'] = false;
              }
            }
            break;
          case 'auto_close':
            isAutoActive.value =
                salesController.currentDataTemp['auto_close'] == '1'
                    ? true
                    : false;
            for (var obj in automaticPayment) {
              if (obj['value'].toString() ==
                  salesController.currentDataTemp['auto_close']) {
                obj['isActive'] = true;
              } else {
                obj['isActive'] = false;
              }
            }
            break;
          // case 'pelanggan':
          //   salesController.lookupCustomerProject(
          //       customerId: salesController.currentDataTemp['pelanggan']
          //           ?['id']);
          //   // salesController.fetchDetailCustomer(
          //   //     salesController.currentDataTemp['pelanggan']?['id']);
          //   break;
          default:
          // salesController.salesKeyAddOrder.currentState?.save();
          // print(salesController.currentDataTemp);
        }
      });
    }

    // TextEditingController customerController = TextEditingController();
    // TextEditingController salesmanController = TextEditingController();
    // TextEditingController projectController = TextEditingController();
    // TextEditingController salesDate = TextEditingController();

    final detailPayment = Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      margin: const EdgeInsets.only(right: 10, left: 10, top: 80, bottom: 10),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: AppColor.kGreyColor),
      ),
      child: Column(
        children: [
          FormBuilder(
            key: salesController.salesKeyAddOrder,
            onChanged: () {
              salesController.salesKeyAddOrder.currentState!.save();
              salesController.previewCheckoutSalesOrder();
              isReloadList.value = !isReloadList.value;
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  BaseTextFieldWidget.textFieldFormBuilder2(
                    title: 'No.Po Customer',
                    fieldName: 'customer_po_number',
                    initialValue:
                        salesController.currentDataTemp['customer_po_number'],
                    onClearText: () => salesController
                        .salesKeyAddOrder.currentState!
                        .patchValue({'customer_po_number': null}),
                    hintText: 'Masukan No. Po Customer',
                    onChange: ({value}) {
                      salesController.currentDataTemp.addAll({
                        'customer_po_number': value,
                      });
                    },
                    validator: (value) {
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                  ),
                  const SizedBox(height: 5),
                  BaseTextFieldWidget.textFieldFormBuilder2(
                    title: 'No. Quotation ',
                    fieldName: 'quotation_number',
                    initialValue:
                        salesController.currentDataTemp['quotation_number'],
                    onClearText: () => salesController
                        .salesKeyAddOrder.currentState!
                        .patchValue({'quotation_number': null}),
                    hintText: 'Masukan No. Quotation',
                    onChange: ({value}) {
                      salesController.currentDataTemp.addAll({
                        'quotation_number': value,
                      });
                    },
                    validator: (value) {
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                  ),
                  const SizedBox(height: 5),
                  Obx(() => salesController.dataLookupSalesman.isEmpty
                          ? BaseTextFieldWidget.disableTextForm2(
                              disabledText: 'Pilih Salesman',
                              label: "Salesman",
                              isRequired: true,
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Salesman wajib dipilih!';
                                }
                                return null;
                              },
                            )
                          : BaseButtonWidget.dropdownSearch(
                              fieldName: 'salesman_id',
                              controller: salesController.salesmanController,
                              isRequired: true,
                              label: "Salesman",
                              hintText: "Pilih Salesman",
                              asyncItems: (String filter) async {
                                salesController.lookupSalesman(
                                    searching: filter);
                                return salesController.dataLookupSalesman;
                              },
                              initialValue:
                                  salesController.currentDataTemp['salesman'],
                              onChange: ({searchValue, valueItem, data}) {
                                salesController.currentDataTemp.addAll({
                                  'salesman': {
                                    'id': valueItem,
                                    'name': searchValue,
                                  },
                                });
                              },
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Salesman wajib dipilih!';
                                }
                                return null;
                              },
                            )

                      // BaseButtonWidget.dropdownButton2(
                      //     fieldName: "salesman_id",
                      //     hintText: "Pilih Salesman",
                      //     valueController: salesController.salesmanController,
                      //     valueData: salesController.dataLookupSalesman,
                      //     label: "Salesman",
                      //     value: salesController.currentDataTemp['salesman'],
                      //     isRequired: true,
                      //     searchOnChange: (p0) {},
                      //     onChange: ({searchValue, valueItem}) {
                      //       // MEMASUKANNYA PADA TEMPORARY UNTUK DITAMPILKAN JIKA DIA KEMBALI DARI PAGE INI

                      //       salesController.currentDataTemp.addAll({
                      //         'salesman': {
                      //           'id': valueItem,
                      //           'name': searchValue,
                      //         },
                      //       });
                      //     },
                      //     validator: (value) {
                      //       if (value == null || value.isEmpty) {
                      //         return 'salesman wajib diisi!';
                      //       }
                      //       return null;
                      //     },
                      //   ),
                      ),
                  const SizedBox(height: 5),
                  Obx(() => salesController.dataLookupCustomer.isEmpty
                          ? BaseTextFieldWidget.disableTextForm2(
                              disabledText: 'Pilih Pelanggan',
                              label: "Pelanggan",
                              isRequired: true,
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Pelanggan wajib dipilih!';
                                }
                                return null;
                              },
                            )
                          : BaseButtonWidget.dropdownSearch(
                              fieldName: 'customer_id',
                              controller: salesController.customerController,
                              isRequired: true,
                              label: "Pelanggan",
                              hintText: "Pilih Pelanggan",
                              asyncItems: (String filter) async {
                                salesController.lookupCustomer(
                                    searching: filter);
                                return salesController.dataLookupCustomer;
                              },
                              initialValue:
                                  salesController.currentDataTemp['pelanggan'],
                              onChange: ({searchValue, valueItem, data}) {
                                salesController.currentDataTemp.addAll({
                                  'pelanggan': {
                                    'id': valueItem.toString(),
                                    'name': searchValue,
                                  },
                                  'proyek': null,
                                  'payment': null,
                                });

                                salesController.currentFilter({
                                  ...salesController.currentFilter,
                                  'customer_id': valueItem,
                                });

                                salesController.lookupCustomerProject();
                                salesController.dataLookupCustomerProject
                                    .clear();
                              },
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Pelanggan wajib dipilih!';
                                }
                                return null;
                              },
                            )

                      // BaseButtonWidget.dropdownButton2(
                      //     fieldName: "customer_id",
                      //     hintText: "Pilih Pelanggan",
                      //     valueController: salesController.customerController,
                      //     valueData: salesController.dataLookupCustomer,
                      //     label: "Pelanggan",
                      //     value: salesController.currentDataTemp['pelanggan'],
                      //     isRequired: true,
                      //     searchOnChange: (p0) {},
                      //     onChange: ({searchValue, valueItem}) {
                      //       // MEMASUKANNYA PADA TEMPORARY UNTUK DITAMPILKAN JIKA DIA KEMBALI DARI PAGE INI

                      //       salesController.currentDataTemp.addAll({
                      //         'pelanggan': {
                      //           'id': valueItem,
                      //           'name': searchValue,
                      //         },
                      //         'proyek': null,
                      //         'payment': null,
                      //       });
                      //       salesController.currentFilter({
                      //         ...salesController.currentFilter,
                      //         'customer_id': valueItem,
                      //       });
                      //       salesController.dataLookupCustomerProject.clear();
                      //       salesController.lookupCustomerProject();
                      //     },
                      //     validator: (value) {
                      //       if (value == null || value.isEmpty) {
                      //         return 'pelanggan wajib diisi!';
                      //       }
                      //       return null;
                      //     },
                      //   ),
                      ),
                  const SizedBox(height: 5),
                  Obx(
                    () => salesController.dataLookupCustomerProject.isNotEmpty
                        ? BaseButtonWidget.dropdownSearch(
                            fieldName: 'customer_project_id',
                            controller: salesController.projectController,
                            isRequired: true,
                            label: "Proyek",
                            hintText: "Pilih Proyek",
                            asyncItems: (String filter) async {
                              salesController.lookupCustomerProject(
                                  searching: filter);
                              return salesController.dataLookupCustomerProject;
                            },
                            initialValue:
                                salesController.currentDataTemp['proyek'],
                            onChange: ({searchValue, valueItem, data}) {
                              salesController.currentDataTemp.addAll({
                                'proyek': {'id': valueItem, 'name': searchValue}
                              });
                            },
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Proyek wajib dipilih!';
                              }
                              return null;
                            },
                          )

                        // BaseButtonWidget.dropdownButton2(
                        //     fieldName: "customer_project_id",
                        //     valueController: salesController.projectController,
                        //     valueData:
                        //         salesController.dataLookupCustomerProject,
                        //     label: "Proyek",
                        //     hintText: "Pilih Proyek",
                        //     value: salesController.currentDataTemp['proyek'],
                        //     isRequired: true,
                        //     searchOnChange: (p0) {},
                        //     onChange: ({searchValue, valueItem}) {
                        //       // MEMASUKANNYA PADA TEMPORARY UNTUK DITAMPILKAN JIKA DIA KEMBALI DARI PAGE INI
                        //       // salesController.currentDataTemp.addAll({
                        //       //   'proyek': {'id': valueItem, 'name': searchValue}
                        //       // });
                        //     },
                        //     validator: (value) {
                        //       if (value == null || value.isEmpty) {
                        //         return 'proyek wajib diisi!';
                        //       }
                        //       return null;
                        //     },
                        //   )
                        : BaseTextFieldWidget.disableTextForm2(
                            disabledText: 'Pilih proyek ',
                            label: 'Proyek',
                            isRequired: true,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Proyek wajib dipilih!';
                              }
                              return null;
                            },
                          ),
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.formDatePicker2(
                    context: context,
                    title: "Tanggal",
                    hintText: "Pilih tanggal",
                    isRequired: true,
                    selectDateController: salesController.salesDate,
                    // currentValue: salesController.currentDataTemp['date'],
                    onChange: ({dateValue}) {
                      salesController.currentDataTemp.addAll({
                        'date': dateValue,
                      });
                    },
                    fieldName: "date",
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Tanggal wajib dipilih!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 5),
                  Obx(() {
                    // isTop.isTrue;
                    var paymentMethod =
                        salesController.dataLookupCustomer.firstWhereOrNull(
                      (data) =>
                          data['id'] ==
                          int.tryParse(
                            salesController.currentDataTemp['pelanggan']
                                    ?['id'] ??
                                '0',
                          ),
                    )?['access_payment'];

                    if (paymentMethod != null) {
                      // var paymentMethod =
                      //     salesController.dataLookupCustomer.where((data) {
                      //   return data['id'] == 359;
                      // });

                      return Column(
                        children: [
                          FormBuilderChoiceChip<String>(
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            decoration: InputDecoration(
                              label: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                    "Metode Pembayaran",
                                    style: BaseTextStyle.customTextStyle(
                                      kcolor: AppColor.kPrimaryColor,
                                      kfontSize: 14,
                                      kfontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  Text(
                                    "*",
                                    style: BaseTextStyle.customTextStyle(
                                      kcolor: AppColor.kErrorColor,
                                      kfontSize: 15,
                                      kfontWeight: FontWeight.w600,
                                    ),
                                  )
                                ],
                              ),
                              errorStyle: BaseTextStyle.customTextStyle(
                                kfontSize: 13,
                                kcolor: AppColor.kFailedColor,
                                kfontWeight: FontWeight.w500,
                              ),
                            ),
                            name: 'payment_type_code',
                            initialValue:
                                salesController.currentDataTemp['payment'],
                            selectedColor: AppColor.kInactiveColor,
                            spacing: 6.0,
                            options:
                                List.generate(paymentMethod.length, (index) {
                              return FormBuilderChipOption(
                                value: paymentMethod[index]['code'],
                                child: Text(
                                  paymentMethod[index]['code'],
                                  style: BaseTextStyle.customTextStyle(
                                    kcolor: AppColor.kPrimaryColor,
                                    kfontWeight: FontWeight.w600,
                                    kfontSize: 12,
                                  ),
                                ),
                              );
                            }),
                            onChanged: (value) {
                              // isTop.isTrue;
                              salesController.currentDataTemp.addAll({
                                'payment': value,
                              });

                              if (value == 'TOP') {
                                salesController.tolerance.text = "30";
                                salesController.currentDataTemp.addAll({
                                  'tolerance': "30",
                                });
                                salesController.isTop.value = true;
                              } else {
                                // salesController.salesKeyAddOrder.currentState!
                                //     .patchValue({'tolerance': null});

                                salesController.isTop.value = false;
                              }
                            },
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Metode pembayaran wajib dipilih!';
                              }

                              return null;
                            },
                          ),
                          const SizedBox(height: 5),
                          salesController.isTop.isTrue
                              ? BaseTextFieldWidget.textFieldFormBuilder2(
                                  title: 'Periode Pembayaran',
                                  hintText: "30",
                                  fieldName: 'tolerance',
                                  // initialValue: '30',
                                  controller: salesController.tolerance,
                                  isRequired: true,
                                  onClearText: () =>
                                      salesController.tolerance.text = "",
                                  // onClearText: () => salesController
                                  //     .salesKeyAddOrder.currentState!
                                  //     .patchValue({'tolerance': ""}),
                                  // onClearText: () => salesController
                                  //     .salesKeyAddOrder.currentState!
                                  //     .patchValue({'tolerance': "32"}),
                                  extendSuffixItem: Container(
                                    margin: const EdgeInsets.all(1.2),
                                    padding: const EdgeInsets.all(14),
                                    decoration: const BoxDecoration(
                                      color: AppColor.kPrimaryColor,
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(7.2),
                                          bottomRight: Radius.circular(7.2)),
                                    ),
                                    child: Text(
                                      "HARI",
                                      style: BaseTextStyle.customTextStyle(
                                        kcolor: AppColor.kSoftBlueColor,
                                        kfontWeight: FontWeight.w600,
                                        kfontSize: 13,
                                      ),
                                    ),
                                  ),
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Toleransi wajib diisi!';
                                    }
                                    if (double.parse(value) < 1) {
                                      return 'Toleransi tidak boleh kurang dari 1 hari';
                                    }
                                    return null;
                                  },
                                  keyboardType:
                                      const TextInputType.numberWithOptions(
                                          decimal: true),
                                  textInputAction: TextInputAction.done,
                                  inputFormater: <TextInputFormatter>[
                                    FilteringTextInputFormatter.digitsOnly
                                  ],
                                )
                              : const SizedBox(),
                        ],
                      );
                    } else {
                      return FormBuilderChoiceChip<String>(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        enabled: false,
                        decoration: InputDecoration(
                          label: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                "Metode Pembayaran",
                                style: BaseTextStyle.customTextStyle(
                                  kcolor: AppColor.kPrimaryColor,
                                  kfontSize: 14,
                                  kfontWeight: FontWeight.w500,
                                ),
                              ),
                              Text(
                                "*",
                                style: BaseTextStyle.customTextStyle(
                                  kcolor: AppColor.kErrorColor,
                                  kfontSize: 15,
                                  kfontWeight: FontWeight.w600,
                                ),
                              )
                            ],
                          ),
                          errorStyle: BaseTextStyle.customTextStyle(
                            kfontSize: 13,
                            kcolor: AppColor.kFailedColor,
                            kfontWeight: FontWeight.w500,
                          ),
                        ),
                        name: 'payment_type_code',
                        // initialValue: .currentFilter['active'],
                        selectedColor: AppColor.kInactiveColor,
                        spacing: 6.0,
                        options: List.generate([].length, (index) {
                          return FormBuilderChipOption(
                            value: [][index],
                            child: Text(
                              [][index],
                              style: BaseTextStyle.customTextStyle(
                                kcolor: AppColor.kPrimaryColor,
                                kfontWeight: FontWeight.w600,
                                kfontSize: 12,
                              ),
                            ),
                          );
                        }),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Metode pembayaran wajib dipilih!';
                          }
                          return null;
                        },
                      );
                    }
                  }),
                  const SizedBox(height: 5),
                  Obx(() {
                    isTriggerOther.isTrue;
                    return BaseTextFieldWidget.selectFieldFormBuilder(
                      title: "Termasuk PPN",
                      fieldName: "tax",
                      isRequired: true,
                      dataSelect: isPPN,
                      initialValue:
                          salesController.currentDataTemp['ppn'] ?? '0',
                      onChanged: (value) {
                        salesController.currentDataTemp.addAll({
                          'ppn': value,
                        });

                        isTriggerOther.value = !isTriggerOther.value;
                        for (var obj in isPPN) {
                          if (obj['value'].toString() == value) {
                            obj['isActive'] = true;
                          } else {
                            obj['isActive'] = false;
                          }
                        }
                      },
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Ppn wajib dipilih!';
                        }
                        return null;
                      },
                    );
                  }),
                  const SizedBox(height: 5),
                  Obx(() {
                    return Column(children: [
                      Obx(() {
                        isTriggerOther.isTrue;
                        return BaseTextFieldWidget.selectFieldFormBuilder(
                          title: "Selesaikan Otomatis",
                          fieldName: "auto_close",
                          isRequired: true,
                          dataSelect: automaticPayment,
                          initialValue:
                              salesController.currentDataTemp['auto_close'] ??
                                  '0',
                          onChanged: (value) {
                            salesController.currentDataTemp.addAll({
                              'auto_close': value,
                            });

                            isAutoActive.value = value == '1' ? true : false;
                            for (var obj in automaticPayment) {
                              if (obj['value'].toString() == value) {
                                obj['isActive'] = true;
                              } else {
                                obj['isActive'] = false;
                              }
                            }
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Masa berlaku wajib diisi';
                            }
                            return null;
                          },
                        );
                      }),
                      const SizedBox(height: 5),
                      isAutoActive.isTrue
                          ? BaseButtonWidget.formDatePicker2(
                              context: context,
                              title: "Batas waktu",
                              hintText: "Pilih tanggal",
                              fieldName: "deadline",
                              isRequired: true,
                              selectDateController:
                                  salesController.datelineDate,
                              enablePastDates: false,
                              onChange: ({dateValue}) {
                                salesController.currentDataTemp.addAll({
                                  'deadline': dateValue,
                                });
                              },
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Batas waktu wajib diisi';
                                }
                                return null;
                              },
                            )
                          : const SizedBox(),
                    ]);
                  }),
                ],
              ),
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        context: context,
        centerTitle: true,
        title: 'Tambah Sales Order',
        extendBack: () {
          // salesController.dataLookupCustomerProject.clear();
          // salesController.dataDetailCustomer.clear();
        },
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            topContent,
            const SizedBox(height: 10),
            detailPayment,
          ],
        ),
      ),
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: submitAction(context),
    );
  }

  Widget submitAction(context) {
    var position = MediaQuery.of(context).viewInsets;
    AddSalesControllers salesController = Get.put(AddSalesControllers());
    return Padding(
      // padding: EdgeInsets.only(bottom: bottom),
      padding: position,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Row(
          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Obx(() {
              return GestureDetector(
                onTap: () {
                  _showDialogBuilder(context, false);
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      children: [
                        Text("${salesController.cart.length} Produk",
                            style: BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kPrimaryColor,
                              kfontSize: 15,
                              kfontWeight: FontWeight.w500,
                            )),
                        GestureDetector(
                          onTap: () {
                            _showDialogBuilder(context, false);
                          },
                          child: const Icon(
                            Icons.info_outline_rounded,
                            size: 16,
                          ),
                        ),
                      ],
                    ),
                    Text(
                      BaseGlobalFuntion.currencyLocalConvert(
                          nominal: salesController
                                  .cartSummary['price_total_preview'] ??
                              salesController.cartSummary['price_total']),
                      style: BaseTextStyle.customTextStyle(
                        kcolor: AppColor.kPrimaryColor,
                        kfontSize: 12,
                        kfontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
              );
            }),
            const SizedBox(width: 40),
            Expanded(
              child: Obx(() => Directionality(
                    textDirection: TextDirection.rtl,
                    child: Button.filled(
                      onPressed: () {
                        if (salesController.salesKeyAddOrder.currentState
                                ?.validate() ??
                            false) {
                          _showDialogBuilder(context, true);
                        }
                      },
                      fontSize: 13,
                      disabled: salesController.cart.isEmpty,
                      color: AppColor.kPrimaryColor,

                      icon: const Icon(
                        Icons.arrow_circle_right,
                        color: AppColor.kWhiteColor,
                      ),
                      // rightIcon: true,
                      // textStyle: BaseTextStyle.customTextStyle(
                      //   kfontWeight: FontWeight.w600,
                      //   kfontSize: 12,
                      //   kcolor: AppColor.kWhiteColor,
                      // ),
                      label: "Selanjutnya",
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _showDialogBuilder(context, bool isSave) {
    AddSalesControllers salesController = Get.put(AddSalesControllers());

    return showModalBottomSheet<void>(
        context: context,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        )),
        builder: (BuildContext context) {
          return Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 18,
              vertical: 10,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Detail Order",
                      style: BaseTextStyle.customTextStyle(
                        kfontSize: 14,
                        kfontWeight: FontWeight.w600,
                        kcolor: AppColor.kPrimaryColor,
                      ),
                    ),
                    IconButton.filledTonal(
                      onPressed: () {
                        Get.back();
                      },
                      icon: const Icon(Icons.close),
                    ),
                  ],
                ),
                const Divider(),
                Obx(() {
                  return Flexible(
                    child: ListView.separated(
                      shrinkWrap: true,
                      physics: const AlwaysScrollableScrollPhysics(),
                      itemCount: salesController.cart.length,
                      separatorBuilder: (context, index) =>
                          const SizedBox(height: 5),
                      itemBuilder: (context, index) {
                        var data = salesController.cart;
                        return ListTile(
                          contentPadding:
                              const EdgeInsets.symmetric(horizontal: 0),
                          // shape: RoundedRectangleBorder(
                          //   side: const BorderSide(
                          //     width: 1,
                          //     color: AppColor.kPrimaryColor,
                          //   ),
                          //   borderRadius: BorderRadius.circular(10),
                          // ),
                          title: BaseTextWidget.customText(
                            text: data[index]['product_name'],
                            fontSize: 12,
                            fontWeight: FontWeight.w600,
                          ),

                          subtitle: BaseTextWidget.customText(
                            // text: BaseGlobalFuntion.currencyLocalConvert(
                            //     nominal: data[index]['unit_price_preview'] ??
                            //         data[index]['unit_price']),
                            text: BaseGlobalFuntion.currencyLocalConvert(
                              nominal: data[index]['unit_price'],
                            ),
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                          ),
                          trailing: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              BaseTextWidget.customText(
                                text: data[index]["quantity"],
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                              ),
                              BaseTextWidget.customText(
                                text: "M3",
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  );
                }),
                const Divider(),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    // horizontal: ,
                    vertical: 10,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text.rich(TextSpan(
                        children: [
                          BaseTextWidget.customTextSpan(
                            text: "Volume",
                            extendText: '\n',
                            fontSize: 13,
                            fontWeight: FontWeight.w600,
                            color: AppColor.kPrimaryColor,
                          ),
                          BaseTextWidget.customTextSpan(
                            text: "PPN",
                            extendText: '\n',
                            fontSize: 13,
                            fontWeight: FontWeight.w600,
                            color: AppColor.kPrimaryColor,
                          ),
                          BaseTextWidget.customTextSpan(
                            text: "Harga Total",
                            extendText: '\n',
                            fontSize: 13,
                            fontWeight: FontWeight.w600,
                            color: AppColor.kPrimaryColor,
                          ),
                        ],
                      )),
                      Obx(
                        () => Text.rich(
                          TextSpan(
                            children: [
                              BaseTextWidget.customTextSpan(
                                text:
                                    salesController.cartSummary['volume_total'],
                                extendText: ' M3\n',
                                fontSize: 13,
                                fontWeight: FontWeight.w500,
                                color: AppColor.kPrimaryColor,
                              ),
                              BaseTextWidget.customTextSpan(
                                text: BaseGlobalFuntion.currencyLocalConvert(
                                    nominal: salesController
                                        .cartSummary['ppn_total_preview']),
                                extendText: '\n',
                                fontSize: 13,
                                fontWeight: FontWeight.w500,
                                color: AppColor.kPrimaryColor,
                              ),
                              BaseTextWidget.customTextSpan(
                                text: BaseGlobalFuntion.currencyLocalConvert(
                                    nominal: salesController.cartSummary[
                                            'price_total_preview'] ??
                                        salesController
                                            .cartSummary['price_total']),
                                extendText: '\n',
                                fontSize: 13,
                                fontWeight: FontWeight.w500,
                                color: AppColor.kPrimaryColor,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                const Divider(),
                if (isSave)
                  Obx(() {
                    return Button.filled(
                      onPressed: () =>
                          salesController.checkoutOrder(loadingCtx: context),
                      disabled: salesController.isLoading.isTrue,
                      color: AppColor.kPrimaryColor,
                      width: double.infinity,
                      icon: const Icon(
                        Icons.add_circle,
                        color: AppColor.kWhiteColor,
                      ),
                      // textStyle: BaseTextStyle.customTextStyle(
                      //   kfontWeight: FontWeight.w600,
                      //   kfontSize: 12,
                      //   kcolor: AppColor.kWhiteColor,
                      // ),
                      label: "Simpan",
                    );
                  }),
              ],
            ),
          );
        });
  }
}
