import 'package:rbs_mobile_operation/core/extentions/date_time_extension.dart';
import 'package:rbs_mobile_operation/data/datasources/productionschedule.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/salesorder.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/production_scedule/create/add_production_schedule.dart';

class SalesOrderView extends StatelessWidget {
  final int itemId;

  const SalesOrderView({
    required this.itemId,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    DetailsOrderControllers orderViewControllers =
        Get.put(DetailsOrderControllers());

    Future onRefresh() async {
      orderViewControllers.viewSalesOrder(itemId);
    }

    Future.delayed(const Duration(milliseconds: 200), () {
      orderViewControllers.viewSalesOrder(itemId);
    }).then(
      (value) => orderViewControllers.viewDetailOrder(itemId),
    );

    final contentAppBar = RefreshIndicator(
      onRefresh: onRefresh,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 15),
            decoration: const BoxDecoration(
              color: AppColor.kPrimaryColor,
              border: Border(
                top: BorderSide(
                  color: AppColor.kPrimaryColor,
                ),
              ),
            ),
            child: Obx(() {
              if (orderViewControllers.viewDataSO.isNotEmpty &&
                  orderViewControllers.isLoading.isFalse) {
                var data = orderViewControllers.viewDataSO;

                var colorBadge =
                    BaseGlobalFuntion.statusColor(data['status_code']);
                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Container(
                            padding: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              color: AppColor.kWhiteColor,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: const Icon(
                              Icons.info_rounded,
                            ),
                          ),
                          const SizedBox(width: 10),
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                BaseTextWidget.customText(
                                  text: data['order_number'],
                                  color: AppColor.kWhiteColor,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w700,
                                  maxLengthText: 25,
                                ),
                                BaseTextWidget.customReadmore(
                                  text: data['customer_project']['name'],
                                  color: AppColor.kWhiteColor,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500,
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(width: 5),
                        ],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 2, horizontal: 6),
                      decoration: BoxDecoration(
                        color: colorBadge['background_color'],
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Text(
                        "${data['status_code']}",
                        style: BaseTextStyle.customTextStyle(
                          kfontSize: 11,
                          kcolor: colorBadge['text_color'],
                        ),
                      ),
                    ),
                  ],
                );
              } else {
                return Shimmer.fromColors(
                    baseColor: AppColor.kSoftGreyColor,
                    highlightColor: AppColor.kWhiteColor,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                            color: Colors.amber,
                            borderRadius: BorderRadius.circular(8),
                          ),
                        ),
                        const SizedBox(width: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 150,
                              height: 15,
                              decoration: BoxDecoration(
                                color: Colors.amber,
                                borderRadius: BorderRadius.circular(4),
                              ),
                            ),
                            const SizedBox(height: 10),
                            Container(
                              width: 200,
                              height: 15,
                              decoration: BoxDecoration(
                                color: Colors.amber,
                                borderRadius: BorderRadius.circular(4),
                              ),
                            )
                          ],
                        )
                      ],
                    ));
              }
            }),
          ),
          Expanded(
            child: DefaultTabController(
                length: 2,
                child: Scaffold(
                  appBar: PreferredSize(
                    preferredSize: const Size.fromHeight(kToolbarHeight),
                    child: Container(
                      decoration: const BoxDecoration(
                        color: AppColor.kPrimaryColor,
                      ),
                      child: SafeArea(
                        child: Column(
                          children: <Widget>[
                            const Expanded(child: SizedBox()),
                            TabBar(
                              dividerColor: AppColor.kPrimaryColor,
                              indicatorColor: AppColor.kInactiveColor,
                              unselectedLabelStyle:
                                  BaseTextStyle.customTextStyle(
                                kfontWeight: FontWeight.w400,
                                kfontSize: 12,
                              ),
                              unselectedLabelColor: AppColor.kWhiteColor,
                              labelColor: AppColor.kInactiveColor,
                              labelStyle: BaseTextStyle.customTextStyle(
                                kfontWeight: FontWeight.w700,
                                kfontSize: 14,
                              ),
                              tabs: const [
                                Tab(text: 'Info Order'),
                                Tab(text: 'Produk'),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  body: TabBarView(
                    children: [
                      Obx(() => segmentTab1(
                            data: orderViewControllers.viewDataSO,
                            isLoading: orderViewControllers.isLoading.value,
                          )),
                      Obx(() => segmentTab2(
                            data: orderViewControllers.detailOrder,
                          )),
                    ],
                  ),
                )),
          )
        ],
      ),
    );

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Detail Sales Order',
          context: context,
        ),
        body: Stack(
          children: [
            contentAppBar,
          ],
        ),
      ),
    );
  }

  Widget segmentTab1({
    dynamic data,
    bool isLoading = true,
  }) {
    var dataProyek = data['customer_project'];
    var dataDetailOrder = data['order_detail'];
    var dataCustomer = data['customer'];

    AddScheduleControllers addScheduleControllers =
        Get.put(AddScheduleControllers());

    return data.isNotEmpty
        ? Stack(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: ListView(
                  physics: const AlwaysScrollableScrollPhysics(),
                  children: [
                    BaseCardWidget.detailInfoCard2(
                      isLoading: isLoading,
                      contentList: [
                        {
                          'title': 'Tgl.Order',
                          'content': BaseTextWidget.customText(
                            text: DateTime.parse(data['order_date'])
                                .toFormattedDate(),
                            // BaseGlobalFuntion.datetimeConvert(
                            //     data['order_date'], "dd MMM yyyy"),
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.calendar_today_rounded,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'Unit Produksi',
                          'content': BaseTextWidget.customText(
                            text: data['production_unit_name'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.factory_rounded,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'Salesman',
                          'content': BaseTextWidget.customText(
                            text: data['salesman_name'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.person_4_outlined,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'Metode Pembayaran',
                          'content': BaseTextWidget.customText(
                            text: data['payment_type_name'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.payment_rounded,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'Harga',
                          'content': BaseTextWidget.customText(
                            text: BaseGlobalFuntion.currencyLocalConvert(
                                nominal: dataDetailOrder['price']),
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.price_change_outlined,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'PPN (11%)',
                          'content': BaseTextWidget.customText(
                            text: BaseGlobalFuntion.currencyLocalConvert(
                                nominal: dataDetailOrder['tax_amount']),
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.price_change_outlined,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'Total Harga',
                          'content': BaseTextWidget.customText(
                            text: BaseGlobalFuntion.currencyLocalConvert(
                                nominal: dataDetailOrder['total_price']),
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.price_change_rounded,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                      ],
                    ),
                    const SizedBox(height: 10),
                    BaseCardWidget.detailInfoCard2(
                      isLoading: isLoading,
                      title: "Informasi Pelanggan & Proyek",
                      contentList: [
                        {
                          'title': 'Pelanggan',
                          'content': BaseTextWidget.customText(
                            text: dataCustomer['name'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            isLongText: true,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.factory_rounded,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'Proyek',
                          'content': BaseTextWidget.customText(
                            text: dataProyek['name'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            isLongText: true,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.calendar_today_rounded,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'Alamat Proyek',
                          'content': BaseTextWidget.customText(
                            text: dataProyek['address'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            isLongText: true,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.person_4_outlined,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                      ],
                    ),
                    const SizedBox(height: 120),
                  ],
                ),
              ),
              if (data['status_code'] == "PENDING" ||
                  data['status_code'] == "PROGRESS")
                Positioned(
                  bottom: 30.0,
                  right: 15.0,
                  child: SizedBox(
                    // height: 45.0,
                    child: Button.filled(
                      onPressed: () async {
                        // Get.to(
                        //   () => AddProductionSchedule(
                        //     orderId: data['id'],
                        //     productionUnitId: data['production_unit_id'],
                        //   ),
                        // );

                        Future.delayed(const Duration(milliseconds: 200),
                            () async {
                          if (data['id'] !=
                              addScheduleControllers
                                  .bodySchedule['sales_order_id']) {
                            // JIKA ID YANG SEBELUMNYA TIDAK SAMA & PADA CART ADA ISINYA
                            // MAKA DATA CARTNYA DI HAPUS
                            addScheduleControllers.bodySchedule.clear();
                            addScheduleControllers.cartSchedule.clear();
                            addScheduleControllers.summarySchedule.clear();
                            // CONTENT
                            addScheduleControllers.dataSODetail.clear();
                            addScheduleControllers.detailSalesOrder.clear();
                          }
                          await addScheduleControllers.lookupSODetail(
                            soId: data['id'],
                          );
                        }).then(
                          (value) => addScheduleControllers.viewSalesOrder(
                            salesOrderId: data['id'],
                          ),
                        );

                        Get.to(
                          () => AddProductionSchedule(
                            orderId: data['id'],
                            productionUnitId: data['production_unit_id'],
                          ),
                        );
                      },
                      // isDisable: addProjectControllers.isLoading.value,
                      color: AppColor.kPrimaryColor,
                      width: 200,
                      fontSize: 12,
                      // rounded: 10,
                      icon: const Icon(Icons.add_circle,
                          color: AppColor.kWhiteColor),
                      // textStyle: BaseTextStyle.customTextStyle(
                      //   kfontWeight: FontWeight.w600,
                      //   kfontSize: 12,
                      //   kcolor: AppColor.kWhiteColor,
                      // ),
                      label: "Rencana Produksi",
                    ),
                  ),
                )
            ],
          )
        : BaseCardWidget.detailInfoCard(
            isLoading: true,
            title: "Informasi Pelanggan & Proyek",
            contentList: [],
          );
  }

  Widget segmentTab2({dynamic data}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
      child: Column(
        children: [
          Expanded(
              child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 0),
            child: data.length > 0
                ? ListView.separated(
                    shrinkWrap: true,
                    itemCount: data.length,
                    separatorBuilder: (BuildContext context, int index) =>
                        const SizedBox(height: 5),
                    itemBuilder: (BuildContext context, int index) {
                      return Obx(() {
                        return BaseCardWidget.cardStyle1(
                          title: data[index]['product_name'],
                          subTitle1: data[index]['product_code'],
                          trailing: BaseTextWidget.customText(
                            text: data[index]['quantity'],
                            extendText: "\n${data[index]['uom_name']}",
                            textAlign: TextAlign.center,
                            fontWeight: FontWeight.w700,
                            color: AppColor.kSuccesColor,
                          ),
                          subTitle2: BaseTextWidget.customText(
                            text: "${BaseGlobalFuntion.currencyLocalConvert(
                              nominal: data[index]['unit_price'],
                            )}",
                            fontSize: 12,
                          ),
                        );
                      });
                    },
                  )
                : BaseLoadingWidget.noMoreDataWidget(),
          )),
        ],
      ),
    );
  }
}
