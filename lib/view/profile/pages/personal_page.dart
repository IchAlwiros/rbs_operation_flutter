import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';
import 'package:rbs_mobile_operation/data/datasources/profile.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class PersonalPage extends StatelessWidget {
  const PersonalPage({super.key});

  @override
  Widget build(BuildContext context) {
    final ProfileControllers profileController = Get.put(ProfileControllers());

    final bodyInformation = Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      margin: const EdgeInsets.only(top: 0),
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.end,
        children: [
          // const Divider(),
          Obx(() {
            if (profileController.profileData.isEmpty) {
              return Center(
                child: BaseLoadingWidget.cardShimmer(
                  baseColor: AppColor.kGreyColor,
                  highlightColor: AppColor.kWhiteColor,
                  itemCount: 1,
                  shimmerheight: 60,
                  height: 60,
                  width: 280,
                ),
              );
            } else {
              return BaseExtendWidget.detailContent(
                title: "INFORMASI AKSES",
                titleColor: AppColor.kGreyColor,
                isLoading: profileController.profileData.isEmpty,
                data: [
                  {
                    'header': "Hak Akses",
                    'content': "${profileController.profileData['role_code']}",
                    'type': 'basic'
                  },
                  {
                    'header': "Level Akses",
                    'content':
                        "${profileController.profileData['corporate_level'] == true ? "Corporate" : "Production Unit :  ${LocalDataSource.getLocalVariable(key: 'production_unit')['name']} "} ",
                    'type': 'basic'
                  },
                ],
              );
            }
          }),
          const Divider(),
          Obx(() {
            return BaseExtendWidget.detailContent(
              title: "INFORMASI USER",
              titleColor: AppColor.kGreyColor,
              isLoading: profileController.profileData.isEmpty,
              data: [
                {
                  'header': "Nama",
                  'content': profileController.profileData['name'],
                  'type': 'basic',
                },
                {
                  'header': "Perusahaan",
                  'content': LocalDataSource.getLocalVariable(
                      key: 'corporate')['name'],
                  'type': 'basic'
                },
                {
                  'header': "Email",
                  'content': profileController.profileData['email'],
                  'type': 'basic'
                },
                {
                  'header': "No.Telp",
                  'content': profileController.profileData['phone'],
                  'type': 'basic'
                },
                {
                  'header': "Alamat",
                  'content': profileController.profileData['address'],
                  'type': 'longtext'
                },
              ],
            );
          }),
        ],
      ),
    );

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Personal Data',
          centerTitle: true,
          context: context,
        ),
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                height: 105,
                width: double.infinity,
                padding: const EdgeInsets.symmetric(vertical: 10),
                decoration: const BoxDecoration(
                  color: AppColor.kPrimaryColor,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                ),
              ),
              bodyInformation,
            ],
          ),
        ),
      ),
    );
  }
}
