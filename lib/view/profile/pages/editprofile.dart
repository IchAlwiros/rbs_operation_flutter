import 'package:rbs_mobile_operation/data/datasources/global/address.controlers.dart';
import 'package:rbs_mobile_operation/data/datasources/global/image_upload_controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/profile.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class EditProfile extends StatelessWidget {
  const EditProfile({super.key});

  @override
  Widget build(BuildContext context) {
    MasterLocationDetail masterLocationDetail = Get.put(MasterLocationDetail());

    final ProfileControllers profileController = Get.put(ProfileControllers());

    var genderOptions = ['Laki-laki', 'Perempuan', 'Other'];

    TextEditingController birtdateValue = TextEditingController();
    if (profileController.profileData['birth_date'] != null) {
      birtdateValue.text = profileController.profileData['birth_date'];
    }

    final formEdit = Container(
      padding: const EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 20),
      margin: const EdgeInsets.only(top: 170),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        boxShadow: BaseShadowStyle.customBoxshadow,
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FormBuilder(
            key: profileController.profileFormEditKey,
            // enabled: false,
            onChanged: () {
              profileController.profileFormEditKey.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child:
                //  Obx(() {
                Column(
              children: <Widget>[
                const SizedBox(height: 15),
                CustomTextField.style2(
                  title: 'Username',
                  fieldName: 'username',
                  hintText: "Masukan username",
                  isRequired: true,
                  onChange: ({value}) {},
                  onClearText: () => profileController
                      .profileFormEditKey.currentState!
                      .patchValue({'username': null}),
                  initialValue: profileController.profileData['username'],
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'username wajib diisi!';
                    }
                    return null;
                  },
                  keyboardType: TextInputType.name,
                  textInputAction: TextInputAction.next,
                ),
                const SizedBox(height: 10),
                CustomTextField.style2(
                  title: 'Nama',
                  fieldName: 'name',
                  hintText: "Masukan nama lengkap",
                  isRequired: true,
                  onClearText: () => profileController
                      .profileFormEditKey.currentState!
                      .patchValue({'name': null}),
                  initialValue: profileController.profileData['name'],
                  onChange: ({value}) {},
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'nama wajib diisi!';
                    }
                    return null;
                  },
                  keyboardType: TextInputType.name,
                  textInputAction: TextInputAction.next,
                ),
                const SizedBox(height: 10),
                CustomTextField.style2(
                  title: 'Email',
                  fieldName: 'email',
                  hintText: "Masukan email",
                  isRequired: true,
                  onClearText: () => profileController
                      .profileFormEditKey.currentState!
                      .patchValue({'email': null}),
                  initialValue: profileController.profileData['email'],
                  onChange: ({value}) {},
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'email wajib diisi!';
                    }

                    if (value.isNotEmpty) {
                      if (!value.contains('@')) {
                        return 'Alamat email tidak sesuai!';
                      }
                    }

                    return null;
                  },
                  keyboardType: TextInputType.emailAddress,
                  textInputAction: TextInputAction.next,
                ),
                const SizedBox(height: 10),
                CustomTextField.style2(
                  title: 'No.Telp',
                  fieldName: 'phone',
                  hintText: "Masukan nomor telepon",
                  isRequired: true,
                  onClearText: () => profileController
                      .profileFormEditKey.currentState!
                      .patchValue({'phone': null}),
                  initialValue: profileController.profileData['phone'],
                  onChange: ({value}) {},
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'no.telp wajib diisi!';
                    }
                    return null;
                  },
                  keyboardType: TextInputType.phone,
                  textInputAction: TextInputAction.next,
                ),
                const SizedBox(height: 10),
                BaseButtonWidget.formDatePicker2(
                  context: context,
                  title: "Tanggal lahir",
                  hintText: 'Pilih Tanggal Lahir',
                  isRequired: true,
                  selectDateController: birtdateValue,
                  enablePastDates: true,
                  onChange: ({dateValue}) {},
                  fieldName: "birth_date",
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'tanggal lahir wajib diisi!';
                    }
                    return null;
                  },
                ),

                const SizedBox(height: 10),
                // if (BaseGlobalVariable.getLocalVariable(
                //   key: 'permissions',
                // ).contains('lookup-province')) ...[
                Obx(() => BaseTextFieldWidget.locationFieldFormBuilder(
                      context: context,
                      controllerField: profileController,
                      title: 'Alamat',
                      havePermission: true,
                      controllerData: masterLocationDetail,
                    )),
                const SizedBox(height: 10),
                CustomTextField.style2(
                  title: 'Alamat Lengkap',
                  fieldName: 'address',
                  hintText: "Masukan alamat lengkap",
                  isRequired: true,
                  isLongText: true,
                  onClearText: () => profileController
                      .profileFormEditKey.currentState!
                      .patchValue({'address': null}),
                  initialValue: profileController.profileData['address'],
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'alamat wajib diisi!';
                    }
                    return null;
                  },
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.next,
                ),
                // ],
                const SizedBox(height: 10),
                FormBuilderChoiceChip<String>(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  showCheckmark: false,
                  decoration: InputDecoration(
                    labelText: 'Gender',
                    labelStyle: BaseTextStyle.customTextStyle(
                      kfontSize: 15,
                      kfontWeight: FontWeight.w600,
                    ),
                    errorStyle: BaseTextStyle.customTextStyle(
                      kfontSize: 13,
                      kcolor: AppColor.kFailedColor,
                      kfontWeight: FontWeight.w500,
                    ),
                  ),
                  selectedColor: AppColor.kInactiveColor,
                  labelStyle: BaseTextStyle.customTextStyle(
                      kcolor: AppColor.kPrimaryColor, kfontSize: 12),
                  name: 'gender',
                  initialValue: profileController.profileData['gender'] == "L"
                      ? 'Laki-laki'
                      : profileController.profileData['gender'] == 'P'
                          ? "Perempuan"
                          : "Other",
                  spacing: 6.0,

                  options: genderOptions.map((gender) {
                    return FormBuilderChipOption(
                      value: gender,
                      avatar: CircleAvatar(
                          backgroundColor: AppColor.kPrimaryColor,
                          child: Text(
                            gender[0],
                            style: BaseTextStyle.customTextStyle(
                              kfontWeight: FontWeight.w600,
                              kfontSize: 10,
                              kcolor: AppColor.kWhiteColor,
                            ),
                          )),
                    );
                  }).toList(),
                  // onChanged: _onChanged,
                  validator: (value) {
                    // if (value == null || value.isEmpty) {
                    //   return 'gender wajib diisi!';
                    // }
                    return null;
                  },
                ),
                const SizedBox(height: 10),
                Obx(
                  () => profileController.dataLookupReligion.isNotEmpty
                      ? FormBuilderChoiceChip<String>(
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          decoration: InputDecoration(
                            labelText: 'Agama',
                            labelStyle: BaseTextStyle.customTextStyle(
                              kfontSize: 15,
                              kfontWeight: FontWeight.w600,
                            ),
                            errorStyle: BaseTextStyle.customTextStyle(
                              kfontSize: 13,
                              kcolor: AppColor.kFailedColor,
                              kfontWeight: FontWeight.w500,
                            ),
                          ),
                          name: 'religion',
                          initialValue: profileController
                              .profileData['religion_id']
                              .toString(),
                          selectedColor: AppColor.kInactiveColor,
                          spacing: 6.0,
                          options: List.generate(
                              profileController.dataLookupReligion.length,
                              (index) {
                            return FormBuilderChipOption(
                              value: profileController.dataLookupReligion[index]
                                      ['id']
                                  .toString(),
                              child: Text(
                                profileController.dataLookupReligion[index]
                                    ['name'],
                                style: BaseTextStyle.customTextStyle(
                                  kcolor: AppColor.kPrimaryColor,
                                  kfontWeight: FontWeight.w600,
                                  kfontSize: 12,
                                ),
                              ),
                            );
                          }),
                          // onChanged: _onChanged,
                          validator: (value) {
                            // if (value == null || value.isEmpty) {
                            //   return 'agama wajib diisi!';
                            // }
                            return null;
                          },
                        )
                      : const SizedBox(),
                ),
              ],
            ),
            // }),
          ),
        ],
      ),
    );

    ImageUploadControllers imageUploadControllers =
        Get.put(ImageUploadControllers());

    void showImagePickerOption() {
      showModalBottomSheet(
          backgroundColor: AppColor.kGreyColor.withOpacity(0.1),
          isDismissible: false,
          context: context,
          builder: (builder) {
            return Padding(
              padding: const EdgeInsets.all(18.0),
              child: Container(
                padding: const EdgeInsets.all(15.0),
                width: MediaQuery.of(context).size.width,
                // height: MediaQuery.of(context).size.height / 4.5,
                decoration: BoxDecoration(
                  color: AppColor.kWhiteColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Ambil Gambar Dari',
                      style: BaseTextStyle.customTextStyle(
                        kfontSize: 12,
                        kfontWeight: FontWeight.w500,
                        // kcolor: Base
                      ),
                    ),
                    const SizedBox(height: 10),
                    Obx(() => Row(
                          children: [
                            Expanded(
                              child: imageUploadControllers.isLoading.value
                                  ? Center(
                                      child: Column(
                                        children: [
                                          const Icon(
                                            Icons.image,
                                            size: 70,
                                            color: AppColor.kGreyColor,
                                          ),
                                          BaseTextWidget.customText(
                                            text: 'Galeri',
                                            fontSize: 12,
                                            fontWeight: FontWeight.w600,
                                            color: AppColor.kGreyColor,
                                          ),
                                        ],
                                      ),
                                    )
                                  : InkWell(
                                      onTap: () {
                                        // profileController.uploadPhoto(
                                        //   pickedGalleryType: true,
                                        // );
                                        imageUploadControllers.uploadPhoto(
                                          pickedGalleryType: true,
                                        );
                                        // _pickImageFromGallery();
                                        // Navigator.of(context).pop();
                                        // Get.back();
                                      },
                                      child: SizedBox(
                                        child: Column(
                                          children: [
                                            const Icon(
                                              Icons.image,
                                              size: 70,
                                            ),
                                            Text(
                                              "Galeri",
                                              style:
                                                  BaseTextStyle.customTextStyle(
                                                kfontSize: 12,
                                                kfontWeight: FontWeight.w600,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                            ),
                            Expanded(
                              child: imageUploadControllers.isLoading.value
                                  ? Center(
                                      child: Column(
                                        children: [
                                          const Icon(
                                            Icons.camera_alt_rounded,
                                            size: 70,
                                            color: AppColor.kGreyColor,
                                          ),
                                          BaseTextWidget.customText(
                                            text: 'Kamera',
                                            fontSize: 12,
                                            fontWeight: FontWeight.w600,
                                            color: AppColor.kGreyColor,
                                          ),
                                        ],
                                      ),
                                    )
                                  : InkWell(
                                      onTap: () {
                                        // profileController.changePhoto(
                                        //   pickedGalleryType: false,
                                        // );

                                        imageUploadControllers.uploadPhoto(
                                          pickedGalleryType: false,
                                        );

                                        // Navigator.of(context).pop(); //
                                      },
                                      child: SizedBox(
                                        child: Column(
                                          children: [
                                            const Icon(
                                              Icons.camera_alt,
                                              size: 70,
                                            ),
                                            Text(
                                              "Kamera",
                                              style:
                                                  BaseTextStyle.customTextStyle(
                                                kfontSize: 12,
                                                kfontWeight: FontWeight.w600,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                            ),
                          ],
                        )),
                  ],
                ),
              ),
            );
          });
    }

    final profilePhoto = Container(
      height: 200,
      width: double.infinity,
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            AppColor.kPrimaryColor,
            AppColor.kSoftBlueColor,
          ],
          stops: [0.0, 1.0],
        ),
      ),
      child: Column(
        children: [
          Center(
            child: Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 1.5,
                  color: AppColor.kSoftBlueColor,
                ),
                borderRadius: BorderRadius.circular(20),
              ),
              child: Obx(
                () => profileController.isLoading.value ||
                        imageUploadControllers.isLoading.value
                    ? Shimmer.fromColors(
                        baseColor: AppColor.kSoftGreyColor,
                        highlightColor: AppColor.kWhiteColor,
                        child: Container(
                          height: 120,
                          width: 120,
                          decoration: BoxDecoration(
                            color: Colors.amber,
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      )
                    : Column(
                        children: [
                          // JIKA DATA PROFILE DITEMUKAN
                          if (profileController.profileData.isEmpty) ...[
                            Shimmer.fromColors(
                              baseColor: AppColor.kSoftGreyColor,
                              highlightColor: AppColor.kWhiteColor,
                              child: Container(
                                height: 120,
                                width: 120,
                                decoration: BoxDecoration(
                                  color: Colors.amber,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            )
                          ] else ...[
                            if (imageUploadControllers.selectedIMage !=
                                null) ...[
                              // PREVIEW PICKED PHOTO
                              GestureDetector(
                                onTap: () {
                                  Get.to(
                                    BaseExtendWidget.viewPhoto(
                                      context: context,
                                      imageProvider: NetworkImage(
                                          "${imageUploadControllers.photoUploadData['preview']}"
                                          // "${profileController.photoUploadData['preview']}",
                                          ),
                                    ),
                                  );
                                },
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(20.0),
                                  child: Image.file(
                                    // profileController.selectedIMage!,
                                    imageUploadControllers.selectedIMage!,
                                    fit: BoxFit.cover,
                                    width: 100,
                                    height: 100,
                                  ),
                                ),
                              ),
                            ] else ...[
                              // CURRENT PHOTO
                              profileController.profileData['photo_preview'] ==
                                          "" ||
                                      profileController
                                              .profileData['photo_preview'] ==
                                          null
                                  ? Container(
                                      width: 100,
                                      height: 100,
                                      // padding: const EdgeInsets.all(14),
                                      decoration: BoxDecoration(
                                        color: AppColor.kWhiteColor,
                                        borderRadius: BorderRadius.circular(10),
                                        boxShadow:
                                            BaseShadowStyle.customBoxshadow,
                                        image: const DecorationImage(
                                          fit: BoxFit.cover,
                                          image: AssetImage(
                                            'assets/icons/app_logo2.png',
                                          ),
                                        ),
                                      ),
                                    )
                                  : GestureDetector(
                                      onTap: () {
                                        Get.to(
                                          () => BaseExtendWidget.viewPhoto(
                                            context: context,
                                            imageProvider: NetworkImage(
                                              "${profileController.profileData['photo_preview']}",
                                            ),
                                          ),
                                        );
                                      },
                                      child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(20.0),
                                        child: profileController.profileData[
                                                    'photo_preview'] !=
                                                null
                                            ? Image.network(
                                                profileController.profileData[
                                                    'photo_preview'],
                                                fit: BoxFit.cover,
                                                loadingBuilder: (_, child,
                                                        chunk) =>
                                                    chunk != null
                                                        ? Shimmer.fromColors(
                                                            baseColor: AppColor
                                                                .kSoftGreyColor,
                                                            highlightColor:
                                                                AppColor
                                                                    .kWhiteColor,
                                                            child: Container(
                                                              height: 120,
                                                              width: 120,
                                                              decoration:
                                                                  BoxDecoration(
                                                                color: Colors
                                                                    .amber,
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            10),
                                                              ),
                                                            ),
                                                          )
                                                        : child,
                                                width: 100,
                                                height: 100,
                                              )
                                            : const Icon(Icons.image),
                                      ),
                                    )
                            ],
                          ],
                        ],
                      ),
              ),
            ),
          ),
        ],
      ),
    );

    final wrapContent = SingleChildScrollView(
      child: Stack(
        children: [
          profilePhoto,
          // ** BUTTON EDIT FOTO
          Positioned(
            top: 90.0,
            right: 110.0,
            child: Container(
              width: 40,
              height: 40,
              decoration: BoxDecoration(
                color: AppColor.kWhiteColor,
                boxShadow: BaseShadowStyle.customBoxshadow,
                borderRadius: BorderRadius.circular(50),
              ),
              child: Obx(
                () => Transform.scale(
                  scale: 0.8,
                  child: profileController.isLoading.value == true
                      ? Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: BaseLoadingWidget.circularProgress(
                              height: 5, width: 5),
                        )
                      : IconButton(
                          color: AppColor.kPrimaryColor,
                          onPressed: () {
                            showImagePickerOption();
                          },
                          icon: const Icon(
                            Icons.edit_square,
                          ),
                        ),
                ),
              ),
            ),
          ),
          // ** WIDGET FORM LIST
          formEdit,
        ],
      ),
    );

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Edit Profile',
          centerTitle: true,
          context: context,
          extendBack: () {
            profileController.collectLocationIdentifier.value = {};
            profileController.photoUploadData.value = {};
            // profileController.photoEditingController.clear();
            // profileController.selectedIMage = null;
            imageUploadControllers.photoEditingController.clear();
            imageUploadControllers.selectedIMage = null;
            // profileController.pickedFile = null;
          },
        ),
        body: Stack(
          children: [
            wrapContent,
            const SizedBox(height: 10.0),
          ],
        ),
        bottomNavigationBar: Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Obx(
              () => Button.filled(
                onPressed: () => profileController.submitEditProfile(
                  loadingCtx: context,
                  extendData: imageUploadControllers.photoUploadData,
                ),
                icon: const Icon(
                  Icons.edit_square,
                  color: AppColor.kWhiteColor,
                ),
                color: AppColor.kPrimaryColor,
                // width: double.infinity,
                // height: 50,
                disabled: profileController.isLoading.value,
                // textStyle: BaseTextStyle.customTextStyle(
                //   kcolor: AppColor.kWhiteColor,
                // ),
                label: "Simpan Perubahan",
              ),
            ),
          ),
        ),
      ),
    );
  }
}
