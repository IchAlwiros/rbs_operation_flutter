import 'package:rbs_mobile_operation/core/extentions/animation_fade_in_slide.dart';
import 'package:rbs_mobile_operation/data/datasources/profile.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class ChangePassword extends StatelessWidget {
  const ChangePassword({super.key});

  @override
  Widget build(BuildContext context) {
    ProfileControllers profileControllers = Get.put(ProfileControllers());

    final changePassworBody = FadeInSlide(
      duration: 1.0,
      child: FormBuilder(
        key: profileControllers.changePwdKey,
        onChanged: () {
          profileControllers.changePwdKey.currentState!.save();
        },
        autovalidateMode: AutovalidateMode.disabled,
        skipDisabled: true,
        child: ListView(
          shrinkWrap: true,
          physics: const AlwaysScrollableScrollPhysics(),
          children: [
            CustomTextField.style2(
              title: 'kata sandi lama',
              fieldName: 'old_password',
              hintText: "Masukan kata sandi lama",
              isRequired: true,
              onClearText: () => profileControllers.changePwdKey.currentState!
                  .patchValue({'old_password': null}),
              onChange: ({value}) {},
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'kata sandi lama harus diisi!';
                }
                return null;
              },
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
            ),
            const SizedBox(height: 10),
            CustomTextField.style2(
              title: 'kata sandi baru',
              fieldName: 'new_password',
              hintText: "Masukan kata sandi baru",
              isRequired: true,
              onClearText: () => profileControllers.changePwdKey.currentState!
                  .patchValue({'new_password': null}),
              onChange: ({value}) {},
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'kata sandi baru wajib diisi!';
                }

                return null;
              },
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
            ),
            const SizedBox(height: 10),
            CustomTextField.style2(
              title: 'konfirmasi kata sandi baru',
              fieldName: 'verify_password',
              hintText: "Masukan konfirmasi kata sandi",
              isRequired: true,
              onClearText: () => profileControllers.changePwdKey.currentState!
                  .patchValue({'verify_password': null}),
              onChange: ({value}) {},
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'konfirmasi wajib diisi!';
                }
                var data = profileControllers.changePwdKey.currentState?.value;

                if (data!['new_password'] != data['verify_password']) {
                  return 'new password must be correct.';
                }
                return null;
              },
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.done,
            ),
          ],
        ),
      ),
    );

    final submitAction = Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Obx(() => Button.filled(
              onPressed: () {
                BaseInfo.simpleConfirmDialog(
                    topTitle: "Anda Yakin?",
                    contentDescription: "anda yakin ingin mengubah password",
                    context: context,
                    onKlikButton: () {
                      profileControllers.changePassword(loadingCtx: context);
                    });
              },
              // isDisable: salesController.cart.isNotEmpty ? false : true,
              disabled: profileControllers.isLoading.value,
              color: AppColor.kPrimaryColor,
              height: 60,
              icon: const Icon(
                Icons.password_sharp,
                color: AppColor.kWhiteColor,
              ),
              // textStyle: BaseTextStyle.customTextStyle(
              //   kfontWeight: FontWeight.w600,
              //   kfontSize: 12,
              //   kcolor: AppColor.kWhiteColor,
              // ),
              label: "Ubah Kata Sandi",
            )),
      ),
    );

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        title: 'Ubah Kata Sandi',
        centerTitle: true,
        context: context,
        extendBack: () {},
      ),
      body: Container(
        decoration: const BoxDecoration(
          color: AppColor.kPrimaryColor,
        ),
        child: Column(
          children: [
            Expanded(
              child: Container(
                child: Assets.images.changePwd.svg(height: 200),
              ),
            ),
            // this will be you container
            Container(
              padding: const EdgeInsets.all(15.0),
              decoration: const BoxDecoration(
                color: AppColor.kWhiteColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BaseTextWidget.customText(
                    text: "Ubah Kata Sandi",
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    isLongText: true,
                  ),
                  BaseTextWidget.customText(
                    text: "Pastikan mengingat kata sandi baru anda!",
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    isLongText: true,
                  ),
                  const SizedBox(height: 5),
                  changePassworBody,
                ],
              ),
            )
          ],
        ),
      ),
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: submitAction,
    );
  }
}
