import 'dart:ui';

import 'package:rbs_mobile_operation/core/components/show_info/alret_info.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/data/datasources/auth.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/profile.controllers.dart';
import 'package:rbs_mobile_operation/view/profile/pages/changepassword.dart';
import 'package:rbs_mobile_operation/view/profile/pages/editprofile.dart';
import 'package:rbs_mobile_operation/view/profile/pages/personal_page.dart';

class Profile extends StatelessWidget {
  const Profile({super.key});

  @override
  Widget build(BuildContext context) {
    final AuthControllers authControllers = Get.put(AuthControllers());
    final ProfileControllers profileController = Get.put(ProfileControllers());

    final profileInfo = Obx(() {
      return Column(
        children: [
          Container(
            padding: const EdgeInsets.all(6),
            decoration: BoxDecoration(
              border: Border.all(
                width: 1.5,
                color: AppColor.kPrimaryColor,
              ),
              borderRadius: BorderRadius.circular(50),
            ),
            child: profileController.profileData.isEmpty
                ? Shimmer.fromColors(
                    baseColor: AppColor.kSoftGreyColor,
                    highlightColor: AppColor.kWhiteColor,
                    child: Container(
                      width: 80,
                      height: 80,
                      decoration: BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.circular(50),
                      ),
                    ))
                : profileController.profileData['photo_preview'] == "" ||
                        profileController.profileData['photo_preview'] == null
                    ? Container(
                        width: 80,
                        height: 80,
                        // padding: const EdgeInsets.all(14),
                        decoration: BoxDecoration(
                          color: AppColor.kWhiteColor,
                          borderRadius: BorderRadius.circular(50),
                          boxShadow: BaseShadowStyle.customBoxshadow,
                          image: const DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage('assets/icons/app_logo.png'),
                          ),
                        ),
                      )
                    : GestureDetector(
                        onTap: () {
                          Get.to(
                            BaseExtendWidget.viewPhoto(
                              context: context,
                              imageProvider: NetworkImage(
                                "${profileController.profileData['photo_preview']}",
                              ),
                            ),
                          );
                        },
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(50.0),
                          child: profileController
                                      .profileData['photo_preview'] !=
                                  null
                              ? Image.network(
                                  profileController
                                      .profileData['photo_preview'],
                                  fit: BoxFit.cover,
                                  width: 80,
                                  height: 80,
                                  loadingBuilder: (_, child, chunk) => chunk !=
                                          null
                                      ? BaseLoadingWidget.cardShimmer(
                                          baseColor: AppColor.kGreyColor,
                                          highlightColor: AppColor.kWhiteColor,
                                          itemCount: 1,
                                          shimmerheight: 80,
                                          height: 80,
                                          width: 80,
                                          roundedShimer: 10,
                                        )
                                      : child,
                                )
                              : const Icon(Icons.image),
                        ),
                      ),
          ),
          const SizedBox(height: 10),
          Obx(
            () {
              if (profileController.profileData.isEmpty) {
                return SizedBox(
                    width: 100,
                    child: Column(
                      children: [
                        BaseLoadingWidget.cardShimmer(
                          baseColor: AppColor.kInactiveColor,
                          highlightColor: AppColor.kWhiteColor,
                          itemCount: 1,
                          shimmerheight: 25,
                          height: 25,
                        ),
                      ],
                    ));
              } else {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    BaseTextWidget.customText(
                      text: profileController.profileData['username'],
                      color: AppColor.kPrimaryColor,
                      fontSize: 13,
                      fontWeight: FontWeight.w700,
                      maxLengthText: 25,
                    ),
                    BaseTextWidget.customText(
                      text: profileController.profileData['role_name'],
                      color: AppColor.kSoftBlueColor,
                      fontSize: 13,
                      fontWeight: FontWeight.w500,
                      maxLengthText: 25,
                    ),
                  ],
                );
              }
            },
          ),
        ],
      );
    });

    final cardProfileMenu = Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
      margin: const EdgeInsets.only(top: 70, left: 10, right: 10),
      decoration: BoxDecoration(
          color: AppColor.kWhiteColor,
          borderRadius: BorderRadius.circular(15),
          // boxShadow: BaseShadowStyle.customBoxshadow,
          border: Border.all(color: AppColor.kGreyColor, width: 1.2)),
      child: Column(
        children: [
          profileInfo,
          const SizedBox(height: 20),
          _listProperty(
            data: [
              {
                'title': 'Personal Data',
                'trailing': const Icon(
                  Icons.arrow_forward_ios_rounded,
                  size: 18,
                ),
                'fungtion': () {
                  Get.to(() => const PersonalPage());
                },
                'leading': const Icon(Icons.person_2_rounded),
              },
              {
                'title': 'Edit Profile',
                'fungtion': () {
                  Get.to(() => const EditProfile());
                },
                'trailing': const Icon(
                  Icons.arrow_forward_ios_rounded,
                  size: 18,
                ),
                'leading': const Icon(Icons.edit_outlined),
              },
              {
                'title': 'Ubah Sandi',
                'fungtion': () {
                  Get.to(() => const ChangePassword());
                },
                'trailing': const Icon(
                  Icons.arrow_forward_ios_rounded,
                  size: 18,
                ),
                'leading': const Icon(Icons.person_4_sharp),
              },
              {
                'title': 'Support',
                'trailing': const Icon(
                  Icons.arrow_forward_ios_rounded,
                  size: 18,
                ),
                'leading': const Icon(Icons.info),
              },
              {
                'title': 'Keluar',
                'tx-color-title': AppColor.kErrorColor,
                'fungtion': () {
                  BaseInfo.simpleConfirmDialog(
                      topTitle: "Anda Yakin?",
                      contentDescription: "anda yakin untuk keluar",
                      context: context,
                      onKlikButton: () {
                        authControllers.onLogout();
                      });
                },
                'trailing': const Icon(
                  Icons.arrow_forward_ios_rounded,
                  size: 18,
                  color: AppColor.kErrorColor,
                ),
                'leading': const Icon(
                  Icons.logout_outlined,
                  color: AppColor.kWhiteColor,
                ),
                'bg-color-leading': AppColor.kErrorColor,
              }
            ],
          ),
        ],
      ),
    );

    final backgroundTop = Container(
      height: MediaQuery.of(context).size.height / 4.5,
      width: double.infinity,
      // padding: const EdgeInsets.symmetric(vertical: 10),
      decoration: const BoxDecoration(
        gradient: RadialGradient(
          radius: 1.5,
          center: Alignment(-1.4, 1.0),
          colors: [
            AppColor.kSoftBlueColor,
            AppColor.kPrimaryColor,
          ],
          stops: [0.10, 1],
          tileMode: TileMode.mirror,
        ),
      ),
      child: Assets.images.loginBg.image(
        // width: 120,
        // height: 120,
        opacity: const AlwaysStoppedAnimation(.6),
        fit: BoxFit.cover,
      ),
      // child: Stack(
      //   children: [
      //     Positioned(
      //       right: -15,
      //       top: 0,
      //       bottom: 0,
      //       child: ImageFiltered(
      //         imageFilter: ImageFilter.blur(sigmaX: 6, sigmaY: 6),
      //         child: Assets.icons.iconLogoSvg.svg(
      //           width: 120,
      //           height: 120,
      //         ),
      //       ),
      //     ),
      //   ],
      // ),
    );

    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Stack(
            children: [
              backgroundTop,
              cardProfileMenu,
            ],
          ),
        ),
      ),
    );
  }

  Widget _listProperty({
    required List<Map<dynamic, dynamic>> data,
  }) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: data.length,
      itemBuilder: (context, index) => ListTile(
        onTap: () {
          if (data[index]['fungtion'] != null &&
              data[index]['fungtion'] is Function) {
            data[index]['fungtion']!();
          }
        },
        leading: Container(
            padding: const EdgeInsets.all(3),
            decoration: BoxDecoration(
                color: data[index]['bg-color-leading'] ?? AppColor.kGreyColor2,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(
                    color: data[index]['bg-color-leading'] ??
                        AppColor.kSoftGreyColor)),
            child: data[index]['leading']),
        title: Text(
          data[index]['title'],
          style: BaseTextStyle.customTextStyle(
            kfontSize: 12,
            kfontWeight: FontWeight.w600,
            kcolor: data[index]['tx-color-title'] ?? AppColor.kPrimaryColor,
          ),
        ),
        trailing: data[index]['trailing'],
      ),
    );
  }
}
