import 'package:rbs_mobile_operation/core/extentions/date_time_extension.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/data/datasources/approval&kredit.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class ApprovalDetailPage extends StatelessWidget {
  // final dynamic approveId;

  const ApprovalDetailPage({
    // required this.approveId,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    ApprovalDetailControllers approvalControllers =
        Get.put(ApprovalDetailControllers());

    // Future.delayed(const Duration(milliseconds: 200), () {
    //   approvalControllers.detailAproval(approvalId: approveId);
    // });

    final detailApprovalItem = SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        child: Column(
          children: [
            if (approvalControllers.detailApprovalData['user_allow_verify'] ==
                false)
              Obx(() {
                final approvalStatus =
                    approvalControllers.detailApprovalData['status_code'];

                return Container(
                  width: double.infinity,
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      color: approvalStatus == "APPROVE"
                          ? AppColor.kBadgeSuccessColor
                          : approvalStatus == "REJECT"
                              ? AppColor.kBadgeFailedColor
                              : AppColor.kBadgeWarningColor,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(6),
                      ),
                      border: Border.all(
                        color: approvalStatus == "APPROVE"
                            ? AppColor.kSuccesColor
                            : approvalStatus == "REJECT"
                                ? AppColor.kErrorColor
                                : AppColor.kWarningColor,
                      )),
                  child: Row(
                    children: [
                      Icon(
                        color: approvalStatus == "APPROVE"
                            ? AppColor.kSuccesColor
                            : approvalStatus == "REJECT"
                                ? AppColor.kErrorColor
                                : AppColor.kWarningColor,
                        Icons.notifications,
                      ),
                      const SizedBox(width: 5),
                      BaseTextWidget.customText(
                        text: approvalStatus == "APPROVE"
                            ? "Pengajuan telah disetujui"
                            : approvalStatus == "REJECT"
                                ? "Pengajuan telah ditolak"
                                : "Pengajuan telah diproses",
                        isLongText: true,
                        fontWeight: FontWeight.bold,
                        color: approvalStatus == "APPROVE"
                            ? AppColor.kSuccesColor
                            : approvalStatus == "REJECT"
                                ? AppColor.kErrorColor
                                : AppColor.kWarningColor,
                      ),
                    ],
                  ),
                );
              }),
            const SizedBox(height: 5),
            Obx(
              () => BaseCardWidget.detailInfoCard2(
                isLoading: approvalControllers.isLoading.value,
                contentList: [
                  // {
                  //   'title': 'Pelanggan',
                  //   'content': BaseTextWidget.customText(
                  //     text: approvalControllers
                  //         .detailApprovalData['customer_name'],
                  //     color: AppColor.kBlackColor,
                  //     fontSize: 13,
                  //     fontWeight: FontWeight.w500,
                  //     maxLengthText: 35,
                  //   ),
                  //   'icon': Container(
                  //       padding: const EdgeInsets.all(5),
                  //       decoration: BoxDecoration(
                  //         borderRadius: BorderRadius.circular(5),
                  //         border: Border.all(
                  //           color: AppColor.kGreyColor,
                  //         ),
                  //       ),
                  //       child: const Icon(
                  //         Icons.portrait_rounded,
                  //         size: 20,
                  //         color: AppColor.kPrimaryColor,
                  //       ))
                  // },
                  // {
                  //   'title': 'Proyek',
                  //   'content': Column(
                  //     crossAxisAlignment: CrossAxisAlignment.start,
                  //     children: [
                  //       BaseTextWidget.customText(
                  //         text: approvalControllers
                  //             .detailApprovalData['customer_project_name'],
                  //         color: AppColor.kBlackColor,
                  //         fontSize: 13,
                  //         fontWeight: FontWeight.w500,
                  //         isLongText: true,
                  //       ),
                  //       BaseTextWidget.customText(
                  //         text: approvalControllers
                  //             .detailApprovalData['customer_project_code'],
                  //         color: AppColor.kGreyColor,
                  //         fontSize: 13,
                  //         fontWeight: FontWeight.w600,
                  //         isLongText: true,
                  //         fontStyle: FontStyle.italic,
                  //       ),
                  //     ],
                  //   ),
                  //   'icon': Container(
                  //       padding: const EdgeInsets.all(5),
                  //       decoration: BoxDecoration(
                  //         borderRadius: BorderRadius.circular(5),
                  //         border: Border.all(
                  //           color: AppColor.kGreyColor,
                  //         ),
                  //       ),
                  //       child: const Icon(
                  //         Icons.all_out_rounded,
                  //         size: 20,
                  //         color: AppColor.kPrimaryColor,
                  //       ))
                  // },
                  // {
                  //   'title': 'Tanggal Pengajuan',
                  //   'content': BaseTextWidget.customText(
                  //     text: DateTime.parse(approvalControllers
                  //             .detailApprovalData['submission_date'])
                  //         .toFormattedDate(),
                  //     color: AppColor.kBlackColor,
                  //     fontSize: 13,
                  //     fontWeight: FontWeight.w500,
                  //     maxLengthText: 35,
                  //   ),
                  //   'icon': Container(
                  //       padding: const EdgeInsets.all(5),
                  //       decoration: BoxDecoration(
                  //         borderRadius: BorderRadius.circular(5),
                  //         border: Border.all(
                  //           color: AppColor.kGreyColor,
                  //         ),
                  //       ),
                  //       child: const Icon(
                  //         Icons.calendar_month,
                  //         size: 20,
                  //         color: AppColor.kPrimaryColor,
                  //       ))
                  // },
                  // {
                  //   'title': 'Pembuatan Pengajuan',
                  //   'content': BaseTextWidget.customText(
                  //     text: approvalControllers
                  //         .detailApprovalData['created_by_name'],
                  //     color: AppColor.kBlackColor,
                  //     fontSize: 13,
                  //     fontWeight: FontWeight.w500,
                  //     maxLengthText: 35,
                  //   ),
                  //   'icon': Container(
                  //       padding: const EdgeInsets.all(5),
                  //       decoration: BoxDecoration(
                  //         borderRadius: BorderRadius.circular(5),
                  //         border: Border.all(
                  //           color: AppColor.kGreyColor,
                  //         ),
                  //       ),
                  //       child: const Icon(
                  //         Icons.portrait_rounded,
                  //         size: 20,
                  //         color: AppColor.kPrimaryColor,
                  //       ))
                  // },
                  // {
                  //   'title': 'Jumlah Pengajuan',
                  //   'content': BaseTextWidget.customText(
                  //     text: BaseGlobalFuntion.currencyLocalConvert(
                  //         nominal:
                  //             approvalControllers.detailApprovalData['amount']),
                  //     color: AppColor.kBlackColor,
                  //     fontSize: 13,
                  //     fontWeight: FontWeight.w500,
                  //     maxLengthText: 35,
                  //   ),
                  //   'icon': Container(
                  //       padding: const EdgeInsets.all(5),
                  //       decoration: BoxDecoration(
                  //         borderRadius: BorderRadius.circular(5),
                  //         border: Border.all(
                  //           color: AppColor.kGreyColor,
                  //         ),
                  //       ),
                  //       child: const Icon(
                  //         Icons.money_rounded,
                  //         size: 20,
                  //         color: AppColor.kPrimaryColor,
                  //       ))
                  // },
                  // {
                  //   'title': 'Alamat',
                  //   'content': BaseTextWidget.customText(
                  //       text: approvalControllers
                  //           .detailApprovalData['customer_project_address'],
                  //       color: AppColor.kBlackColor,
                  //       fontSize: 13,
                  //       fontWeight: FontWeight.w500,
                  //       isLongText: true),
                  //   'icon': Container(
                  //       padding: const EdgeInsets.all(5),
                  //       decoration: BoxDecoration(
                  //         borderRadius: BorderRadius.circular(5),
                  //         border: Border.all(
                  //           color: AppColor.kGreyColor,
                  //         ),
                  //       ),
                  //       child: const Icon(
                  //         Icons.location_on,
                  //         size: 20,
                  //         color: AppColor.kPrimaryColor,
                  //       ))
                  // },
                ],
              ),
            ),
            const SizedBox(height: 5),
            Obx(
              () => BaseCardWidget.detailInfoCard2(
                title: "PROGRESS PENGAJUAN",
                isLoading: approvalControllers.isLoading.value,
                contentList: [
                  ...List.generate(
                      approvalControllers.detailApprovalData['approval'].length,
                      (index) {
                    final dataApproval = approvalControllers
                        .detailApprovalData['approval'][index];

                    dynamic statusAproval({
                      required onReject,
                      required onApprove,
                      required onProgress,
                    }) {
                      return dataApproval['status_code'] == 'REJECT'
                          ? onReject
                          : dataApproval['status_code'] == 'APPROVE'
                              ? onApprove
                              : onProgress;
                    }

                    return {
                      'title': 'Verifikasi ${dataApproval['group_level']}',
                      'content': Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              BaseTextWidget.customText(
                                text: dataApproval['verified_at'] ?? "",
                              ),
                              Container(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 2, horizontal: 4),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4),
                                  border: Border.all(
                                    color: statusAproval(
                                      onApprove: AppColor.kSuccesColor,
                                      onReject: AppColor.kErrorColor,
                                      onProgress: AppColor.kYellowColor,
                                    ),
                                  ),
                                  color: statusAproval(
                                    onApprove: AppColor.kBadgeSuccessColor,
                                    onReject: AppColor.kBadgeFailedColor,
                                    onProgress: AppColor.kBadgeWarningColor,
                                  ),
                                ),
                                child: BaseTextWidget.customText(
                                  text: statusAproval(
                                    onApprove: "DiSetujui",
                                    onReject: "Ditolak",
                                    onProgress: "Menunggu Persetujuan",
                                  ),
                                  color: statusAproval(
                                    onApprove: AppColor.kSuccesColor,
                                    onReject: AppColor.kErrorColor,
                                    onProgress: AppColor.kYellowColor,
                                  ),
                                  fontSize: 13,
                                  fontWeight: FontWeight.w600,
                                  isLongText: true,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 2),
                          const Divider(),
                          statusAproval(
                              onApprove: BaseTextWidget.customText(
                                text: dataApproval['verified_by'],
                                color: AppColor.kBlackColor,
                                fontSize: 13,
                                fontWeight: FontWeight.w500,
                                isLongText: true,
                              ),
                              onReject: BaseTextWidget.customText(
                                text: dataApproval['verified_by'],
                                color: AppColor.kBlackColor,
                                fontSize: 13,
                                fontWeight: FontWeight.w500,
                                isLongText: true,
                              ),
                              onProgress: SizedBox(
                                width: double.infinity,
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Row(
                                    children: List.generate(
                                      dataApproval['verificator'].length,
                                      // 10,
                                      (verifikatorIndex) => Container(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 2, horizontal: 5),
                                          margin:
                                              const EdgeInsets.only(right: 5),
                                          decoration: BoxDecoration(
                                              color: AppColor.kGreyColor
                                                  .withOpacity(0.5),
                                              borderRadius:
                                                  BorderRadius.circular(4)),
                                          child: Text(
                                            dataApproval['verificator']
                                                [verifikatorIndex]['role_name'],
                                          )),
                                    ),
                                  ),
                                ),
                              )),
                          const SizedBox(height: 2),
                          BaseTextWidget.customText(
                            text: "Catatan :",
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w600,
                            isLongText: true,
                          ),
                          BaseTextWidget.customText(
                            text: dataApproval['notes'],
                            color: AppColor.kBlackColor,
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            isLongText: true,
                          ),
                        ],
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: statusAproval(
                                onApprove: AppColor.kSuccesColor,
                                onReject: AppColor.kErrorColor,
                                onProgress: AppColor.kYellowColor,
                              ),
                            ),
                          ),
                          child: Icon(
                            statusAproval(
                              onApprove:
                                  Icons.playlist_add_check_circle_rounded,
                              onReject: Icons.closed_caption_disabled_rounded,
                              onProgress: Icons.approval_rounded,
                            ),
                            size: 20,
                            color: statusAproval(
                              onApprove: AppColor.kSuccesColor,
                              onReject: AppColor.kErrorColor,
                              onProgress: AppColor.kYellowColor,
                            ),
                          ))
                    };
                  })
                ],
              ),
            ),
          ],
        ),
      ),
    );

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Detail Approval Kredit',
          centerTitle: true,
          context: context,
        ),
        body: approvalControllers.detailApprovalData.isEmpty
            ? SizedBox(
                height: MediaQuery.of(context).size.height / 1.2,
                child: Center(
                  child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                ),
              )
            : detailApprovalItem,
        bottomNavigationBar: Obx(() {
          final approvalData = approvalControllers.detailApprovalData;
          print(approvalData);
          if (approvalData.isEmpty) {
            return const SizedBox();
          } else {
            return approvalData['user_allow_verify']
                ? Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10, vertical: 15),
                    decoration: BoxDecoration(
                        color: AppColor.kWhiteColor,
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10),
                        ),
                        boxShadow: [
                          BoxShadow(
                            color: AppColor.kGreyColor.withOpacity(0.5),
                            spreadRadius: 1,
                            blurRadius: 10,
                            offset: const Offset(
                                0, -5), // changes position of shadow
                          ),
                        ]),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        BaseTextWidget.customText(
                          text: "Tindak Lanjut",
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                        const Divider(),
                        Container(
                          width: double.infinity,
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: AppColor.kGreyColor.withOpacity(0.3),
                              borderRadius: const BorderRadius.all(
                                Radius.circular(10),
                              )),
                          child: Row(
                            children: [
                              const Icon(
                                color: AppColor.kGreyColor,
                                Icons.notifications,
                              ),
                              const SizedBox(width: 5),
                              BaseTextWidget.customText(
                                text:
                                    "Menunggu Persetujuan Admin Unit Produksi",
                                isLongText: true,
                                fontWeight: FontWeight.bold,
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(height: 10),
                        Row(
                          children: [
                            Expanded(
                              child: Button.filled(
                                onPressed: () {
                                  _dialogBuilder(
                                      context, "REJECT", approvalData);
                                },
                                label: 'Tolak',
                                fontSize: 15,
                                color: AppColor.kErrorColor,
                              ),
                            ),
                            const SizedBox(width: 8),
                            Expanded(
                              child: Button.filled(
                                onPressed: () {
                                  _dialogBuilder(
                                      context, "APPROVE", approvalData);
                                },
                                label: 'Setujui',
                                fontSize: 15,
                                color: AppColor.kSuccesColor,
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  )
                : const SizedBox();
          }
        }),
      ),
    );
  }

  Future<void> _dialogBuilder(
    context,
    String statusApproval,
    dynamic approvalData,
  ) {
    ApprovalDetailControllers approvalControllers =
        Get.put(ApprovalDetailControllers());
    TextEditingController notesControllers = TextEditingController();
    return showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      backgroundColor: AppColor.kGreyColor.withOpacity(0.1),
      builder: (context) {
        var bottom = MediaQuery.of(context).viewInsets.bottom;
        return Container(
          padding:
              EdgeInsets.only(right: 10, left: 10, top: 10, bottom: bottom),
          margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          decoration: BoxDecoration(
            color: AppColor.kWhiteColor,
            borderRadius: BorderRadius.circular(10),
          ),
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BaseTextWidget.customText(
                  text: statusApproval == "REJECT"
                      ? 'Tolak Pengajuan Kredit'
                      : 'Setujui Pengajuan Kredit',
                  color: AppColor.kBlackColor,
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                  maxLengthText: 35,
                ),
                const Divider(),
                BaseCardWidget.detailInfoCard2(
                  // isLoading: approvalControllers.isLoading.value,
                  contentList: [
                    {
                      'title': 'Tgl.Pengajuan',
                      'content': BaseTextWidget.customText(
                        text: DateTime.parse(approvalData['submission_date'])
                            .toFormattedDate(),
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.calendar_month,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                    {
                      'title': 'Pelanggan',
                      'content': BaseTextWidget.customText(
                        text: approvalData['customer_name'],
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        maxLengthText: 35,
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.portrait_rounded,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                    {
                      'title': 'Proyek',
                      'content': Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          BaseTextWidget.customText(
                            text: approvalData['customer_project_name'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            isLongText: true,
                          ),
                          BaseTextWidget.customText(
                            text: approvalData['customer_project_code'],
                            color: AppColor.kGreyColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w600,
                            isLongText: true,
                            fontStyle: FontStyle.italic,
                          ),
                        ],
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.all_out_rounded,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                    {
                      'title': 'Alamat',
                      'content': BaseTextWidget.customText(
                          text: approvalData['customer_project_address'],
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          isLongText: true),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.location_on,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                    {
                      'title': 'Jumlah Pengajuan',
                      'content': BaseTextWidget.customText(
                        text: approvalData['amount'],
                        localCurency: true,
                        color: AppColor.kBlackColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.money_rounded,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                    {
                      'title': 'Tindak Lanjut',
                      'content': Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: 2,
                          horizontal: 4,
                        ),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: statusApproval == "REJECT"
                                ? AppColor.kBadgeFailedColor
                                : AppColor.kBadgeSuccessColor,
                            border: Border.all(
                              color: statusApproval == "REJECT"
                                  ? AppColor.kErrorColor
                                  : AppColor.kSuccesColor,
                            )),
                        child: BaseTextWidget.customText(
                          text: statusApproval == "REJECT"
                              ? 'Tolak Pengajuan Kredit'
                              : 'Setujui Pengajuan Kredit',
                          localCurency: true,
                          color: statusApproval == "REJECT"
                              ? AppColor.kErrorColor
                              : AppColor.kSuccesColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      'icon': Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                              color: AppColor.kGreyColor,
                            ),
                          ),
                          child: const Icon(
                            Icons.pending_actions_rounded,
                            size: 20,
                            color: AppColor.kPrimaryColor,
                          ))
                    },
                  ],
                ),
                CustomTextField.style2(
                  title: 'Catatan',
                  hintText: "Berikan pesan",
                  fieldName: 'notes',
                  isLongText: true,
                  controller: notesControllers,
                  onChange: ({value}) {},
                  textInputAction: TextInputAction.done,
                ),
                const SizedBox(height: 5),
                Button.filled(
                  onPressed: () {
                    approvalControllers
                        .actionAproval(loadingCtx: context, approvalBody: {
                      "notes": notesControllers.text,
                      "production_unit_id": approvalData['production_unit_id'],
                      "status_code": statusApproval,
                      "submission_id": approvalData['id'],
                    });
                  },
                  label: statusApproval == "REJECT" ? "Tolak" : "Setujui",
                  fontSize: 15,
                  color: statusApproval == "REJECT"
                      ? AppColor.kErrorColor
                      : AppColor.kSuccesColor,
                ),
                const SizedBox(height: 10),
              ],
            ),
          ),
        );
      },
    );
  }
}
