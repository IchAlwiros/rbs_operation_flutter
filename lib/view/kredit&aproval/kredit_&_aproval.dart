import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/view/app_view_resource.dart';

class KreditApprovalList extends StatelessWidget {
  const KreditApprovalList({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: BaseExtendWidget.appBarBottomTab(
          title: 'Kredit & Approval',
          bottomTabbar: TabBar(
            indicatorColor: AppColor.kInactiveColor,
            unselectedLabelStyle: BaseTextStyle.customTextStyle(
              kfontWeight: FontWeight.w400,
              kfontSize: 12,
            ),
            unselectedLabelColor: AppColor.kWhiteColor,
            labelColor: AppColor.kInactiveColor,
            labelStyle: BaseTextStyle.customTextStyle(
              kfontWeight: FontWeight.w700,
              kfontSize: 14,
            ),
            tabs: const [
              Tab(text: 'Kredit'),
              Tab(text: 'Approval'),
            ],
          ),
        ),
        body: const TabBarView(
          children: [
            KrediTab(title: "Kredit"),
            ApprovalTab(title: "Approval"),
          ],
        ),
      ),
    );
  }
}
