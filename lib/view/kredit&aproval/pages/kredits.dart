import 'package:rbs_mobile_operation/data/datasources/approval&kredit.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class KrediTab extends StatelessWidget {
  final String title;

  const KrediTab({
    required this.title,
    super.key,
  });
  @override
  Widget build(BuildContext context) {
    KreditControllers kreditControllers = Get.put(KreditControllers());

    TextEditingController startEditing = TextEditingController();
    TextEditingController endEditing = TextEditingController();

    Future onRefresh() async {
      kreditControllers.refreshDataApproval();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        kreditControllers.fetchCredit();
      }
    });

    if (kDebugMode) {
      print(startEditing.text);
      print(endEditing.text);
    }

    final listCardContent = Obx(() => Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 0),
            child: ListView.separated(
              shrinkWrap: true,
              controller: scrollController,
              itemCount: kreditControllers.hasMore.value
                  ? kreditControllers.approvalData.length + 1
                  : kreditControllers.approvalData.length,
              separatorBuilder: (BuildContext context, int index) =>
                  const SizedBox(height: 5),
              itemBuilder: (BuildContext context, int index) {
                final resultData = kreditControllers.approvalData;
                if (index < resultData.length) {
                  final data = resultData[index];
                  var colorBadge = BaseGlobalFuntion.statusColor(
                      kreditControllers.approvalData[index]['status_code']);
                  return BaseCardWidget.cardStyle1(
                    title: data['submission_number'] ?? "",
                    // trailing: "",
                    subTitle1: "${data['customer_project_name']}",
                    subTitle2: BaseTextWidget.customText(
                      text:
                          "${BaseGlobalFuntion.datetimeConvert(data['submission_date'], 'dd MMM yyyy HH:mm')}",
                      fontSize: 12,
                    ),

                    statusHeader: {
                      'title': data['status_code'] ?? '',
                      'color': colorBadge['background_color'],
                      'titleColor': colorBadge['text_color'],
                    },
                  );
                } else if (resultData.length < 25) {
                  return kreditControllers.hasMore.value &&
                          kreditControllers.isLoading.isTrue
                      ? BaseLoadingWidget.cardShimmer(
                          height: MediaQuery.of(context).size.height,
                          shimmerheight: 80,
                          baseColor: AppColor.kSoftGreyColor,
                          highlightColor: AppColor.kWhiteColor,
                          itemCount: 25,
                        )
                      : const SizedBox();
                } else {
                  return kreditControllers.hasMore.value
                      ? BaseLoadingWidget.cardShimmer(
                          height: 200,
                          shimmerheight: 90,
                          baseColor: AppColor.kSoftGreyColor,
                          highlightColor: AppColor.kWhiteColor,
                          itemCount: 2,
                        )
                      : const SizedBox();
                }
                // if (index < kreditControllers.approvalData.length) {
                //   final data = kreditControllers.approvalData[index];
                //   var colorBadge = BaseGlobalFuntion.statusColor(
                //       kreditControllers.approvalData[index]['status_code']);
                //   return BaseCardWidget.cardStyle1(
                //     title: data['submission_number'] ?? "",
                //     // trailing: "",
                //     subTitle1: "${data['customer_project_name']}",
                //     subTitle2: BaseTextWidget.customText(
                //       text:
                //           "${BaseGlobalFuntion.datetimeConvert(data['submission_date'], 'dd MMM yyyy HH:mm')}",
                //       fontSize: 12,
                //     ),

                //     statusHeader: {
                //       'title': data['status_code'] ?? '',
                //       'color': colorBadge['background_color'],
                //       'titleColor': colorBadge['text_color'],
                //     },
                //   );
                // } else {
                //   return BaseLoadingWidget.cardShimmer(
                //     height: 400,
                //     baseColor: AppColor.kSoftGreyColor,
                //     highlightColor: AppColor.kWhiteColor,
                //     itemCount: 3,
                //   );
                // }
              },
            ),
          ),
        ));

    final content = Padding(
      padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 8.0),
      child: RefreshIndicator(
        onRefresh: onRefresh,
        child: Column(
          children: [
            const SizedBox(height: 10),
            BaseButtonWidget.formDateRangePicker(
              context: context,
              title: 'Tanggal',
              startDateController: TextEditingController(),
              endDateController: TextEditingController(),
              onChange: ({endDate, startDate}) {
                kreditControllers.filterByRangeDate(
                  startDate: startDate,
                  endDate: endDate,
                );
              },
              fieldName: 'date_range',
            ),
            const SizedBox(height: 10.0),
            Container(
              padding: const EdgeInsets.only(top: 0.0),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                physics: const BouncingScrollPhysics(),
                child: Row(
                  children: [
                    BaseButtonWidget.buttonFilter(
                      title: [
                        "Semua",
                        "Menunggu Persetujuan",
                        "Disetujui",
                        "Ditolak"
                      ],
                      onPressed: [
                        () => kreditControllers.filterByStatus(['']),
                        () => kreditControllers.filterByStatus(
                              [
                                'status_code[]=PENDING',
                                'status_code[]=PROGRESS'
                              ],
                            ),
                        () => kreditControllers.filterByStatus(
                              ['status_code[]=APPROVE'],
                            ),
                        () => kreditControllers.filterByStatus(
                              ['status_code[]=REJECT'],
                            ),
                      ],
                      initialActive: 1,
                      bColor: AppColor.kInactiveColor,
                      contentColor: AppColor.kPrimaryColor,
                      height: 10,
                      width: 105,
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(height: 20.0),
            listCardContent,
          ],
        ),
      ),
    );

    return content;
  }
}
