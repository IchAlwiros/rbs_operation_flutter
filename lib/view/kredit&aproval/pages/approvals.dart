import 'package:rbs_mobile_operation/data/datasources/approval&kredit.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class ApprovalTab extends StatelessWidget {
  final String title;

  const ApprovalTab({
    required this.title,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    ApprovalControllers approvalControllers = Get.put(ApprovalControllers());

    Future onRefresh() async {
      approvalControllers.refreshDataApproval();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        approvalControllers.fetchAproval();
      }
    });

    final listCardContent = Obx(() => Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 0),
            child: ListView.separated(
              shrinkWrap: true,
              controller: scrollController,
              itemCount: approvalControllers.hasMore.value
                  ? approvalControllers.approvalData.length + 1
                  : approvalControllers.approvalData.length,
              separatorBuilder: (BuildContext context, int index) =>
                  const SizedBox(height: 5),
              itemBuilder: (BuildContext context, int index) {
                final resultData = approvalControllers.approvalData;
                if (index < resultData.length) {
                  final data = approvalControllers.approvalData[index];
                  var colorBadge = BaseGlobalFuntion.statusColor(
                      approvalControllers.approvalData[index]['status_code']);
                  return BaseCardWidget.cardStyle1(
                    title: data['submission_number'] ?? "",
                    // trailing: "",
                    subTitle1: "${data['customer_project_name']}",
                    subTitle2: BaseTextWidget.customText(
                      text: BaseGlobalFuntion.datetimeConvert(
                          data['submission_date'], 'dd MMM yyyy HH:mm'),
                      fontSize: 12,
                    ),
                    // headerString:
                    //     "${BaseGlobalFuntion.datetimeConvert(data['submission_date'], 'dd MMM yyyy HH:mm')}",
                    statusHeader: {
                      'title': data['status_code'] ?? '',
                      'color': colorBadge['background_color'],
                      'titleColor': colorBadge['text_color'],
                    },
                  );
                } else if (resultData.length < 25) {
                  return approvalControllers.hasMore.value &&
                          approvalControllers.isLoading.isTrue
                      ? BaseLoadingWidget.cardShimmer(
                          height: MediaQuery.of(context).size.height,
                          shimmerheight: 80,
                          baseColor: AppColor.kSoftGreyColor,
                          highlightColor: AppColor.kWhiteColor,
                          itemCount: 25,
                        )
                      : const SizedBox();
                } else {
                  return approvalControllers.hasMore.value
                      ? BaseLoadingWidget.cardShimmer(
                          height: 200,
                          shimmerheight: 90,
                          baseColor: AppColor.kSoftGreyColor,
                          highlightColor: AppColor.kWhiteColor,
                          itemCount: 2,
                        )
                      : const SizedBox();
                }
                // if (index < approvalControllers.approvalData.length) {
                //   final data = approvalControllers.approvalData[index];
                //   var colorBadge = BaseGlobalFuntion.statusColor(
                //       approvalControllers.approvalData[index]['status_code']);
                //   return BaseCardWidget.cardStyle1(
                //     title: data['submission_number'] ?? "",
                //     // trailing: "",
                //     subTitle1: "${data['customer_project_name']}",
                //     subTitle2: BaseTextWidget.customText(
                //       text: BaseGlobalFuntion.datetimeConvert(
                //           data['submission_date'], 'dd MMM yyyy HH:mm'),
                //       fontSize: 12,
                //     ),
                //     // headerString:
                //     //     "${BaseGlobalFuntion.datetimeConvert(data['submission_date'], 'dd MMM yyyy HH:mm')}",
                //     statusHeader: {
                //       'title': data['status_code'] ?? '',
                //       'color': colorBadge['background_color'],
                //       'titleColor': colorBadge['text_color'],
                //     },
                //   );
                // } else {
                //   return BaseLoadingWidget.cardShimmer(
                //     height: 400,
                //     baseColor: AppColor.kSoftGreyColor,
                //     highlightColor: AppColor.kWhiteColor,
                //     itemCount: 3,
                //   );
                // }
              },
            ),
          ),
        ));

    final content = Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10),
      child: RefreshIndicator(
        onRefresh: onRefresh,
        child: Column(
          children: [
            BaseButtonWidget.formDateRangePicker(
              context: context,
              title: 'Tanggal',
              startDateController: TextEditingController(),
              endDateController: TextEditingController(),
              onChange: ({endDate, startDate}) {
                approvalControllers.filterByRangeDate(
                  startDate: startDate,
                  endDate: endDate,
                );
              },
              fieldName: 'date_range',
            ),
            Container(
              padding: const EdgeInsets.only(top: 10.0),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                physics: const BouncingScrollPhysics(),
                child: Row(
                  children: [
                    BaseButtonWidget.buttonFilter(
                      title: [
                        "Semua",
                        "Menunggu Persetujuan",
                        "Disetujui",
                        "Ditolak"
                      ],
                      onPressed: [
                        () {
                          approvalControllers.filterByStatus(['']);
                        },
                        () {
                          approvalControllers.filterByStatus(
                            ['status_code[]=PENDING', 'status_code[]=PROGRESS'],
                          );
                        },
                        () {
                          approvalControllers.filterByStatus(
                            ['status_code[]=APPROVE'],
                          );
                        },
                        () {
                          approvalControllers.filterByStatus(
                            ['status_code[]=REJECT'],
                          );
                        },
                      ],
                      initialActive: 1,
                      bColor: AppColor.kInactiveColor,
                      contentColor: AppColor.kPrimaryColor,
                      height: 10,
                      width: 105,
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(height: 10),
            listCardContent,
          ],
        ),
      ),
    );

    return content;
  }
}
