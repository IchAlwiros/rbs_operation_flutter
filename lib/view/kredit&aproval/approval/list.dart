import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/date_time_extension.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/data/datasources/approval&kredit.controllers.dart';
import 'package:rbs_mobile_operation/view/kredit&aproval/detail/detail_approval_pages.dart';

class AprovePage extends StatelessWidget {
  const AprovePage({super.key});

  @override
  Widget build(BuildContext context) {
    ApprovalControllers approvalControllers = Get.put(ApprovalControllers());
    ApprovalDetailControllers detailapprovalControllers =
        Get.put(ApprovalDetailControllers());

    Future onRefresh() async {
      approvalControllers.refreshDataApproval();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.position.pixels) {
        if (approvalControllers.approvalData.isNotEmpty) {
          approvalControllers.fetchAproval();
        }
      }
    });

    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          approvalControllers.byRangeDate.clear();
          approvalControllers.currentFilter.clear();
          approvalControllers.formKeyFilter.currentState!.reset();

          approvalControllers.formKeyFilter.currentState!.patchValue({
            'date_range': null,
          });
        },
        onFilter: () {
          Get.back();
          approvalControllers.refreshDataApproval();
        },
        valueFilter: approvalControllers.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: approvalControllers.formKeyFilter,
            onChanged: () {
              approvalControllers.formKeyFilter.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  BaseButtonWidget.formDateRangePicker2(
                    context: context,
                    fieldName: 'date_range',
                    title: "Tanggal",
                    hintText: "Pilih tanggal",
                    currentValue: BaseGlobalFuntion.formatDateRange(
                      approvalControllers.byRangeDate['start-date'] ?? "",
                      approvalControllers.byRangeDate['end-date'] ?? "",
                    ),
                    startDateController: TextEditingController(),
                    endDateController: TextEditingController(),
                    onChange: ({endDate, startDate}) {
                      approvalControllers.filterByRangeDate(
                        startDate: startDate,
                        endDate: endDate,
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    final listItemContent = Flexible(
      child: Obx(
        () {
          final result = approvalControllers.approvalData;
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: result.isEmpty && approvalControllers.isLoading.isFalse
                ? SizedBox(
                    height: MediaQuery.of(context).size.height / 1.2,
                    child: Center(
                      child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                    ),
                  )
                : ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: result.length + 1,
                    separatorBuilder: (BuildContext context, int index) =>
                        const SizedBox(height: 5),
                    itemBuilder: (BuildContext context, int index) {
                      if (index < result.length) {
                        final data = result[index];

                        var colorBadge = BaseGlobalFuntion.statusColor(
                            result[index]['status_code']);
                        return BaseCardWidget.cardStyle1(
                          ontap: () {
                            Future.delayed(const Duration(milliseconds: 200),
                                () async {
                              await detailapprovalControllers.detailAproval(
                                approvalId: data['id'],
                              );
                            }).then(
                              (value) => Get.to(
                                () => const ApprovalDetailPage(),
                              ),
                            );
                            // Get.to(
                            //   () => ApprovalDetailPage(approveId: data['id']),
                            // );
                          },
                          title: data['submission_number'] ?? "",
                          trailing: BaseTextWidget.customText(
                            text: data['amount'],
                            fontSize: 12,
                            localCurency: true,
                            fontWeight: FontWeight.bold,
                          ),
                          subTitle1: "${data['customer_project_name']}",
                          subTitle2: BaseTextWidget.customText(
                            text: DateTime.parse(data['submission_date'])
                                .toFormattedDate(),
                            // BaseGlobalFuntion.datetimeConvert(
                            //     data['submission_date'], 'dd MMM yyyy HH:mm'),
                            fontSize: 12,
                          ),
                          // headerString:
                          //     "${BaseGlobalFuntion.datetimeConvert(data['submission_date'], 'dd MMM yyyy HH:mm')}",
                          statusHeader: {
                            'title': data['status_code'] ?? '',
                            'color': colorBadge['background_color'],
                            'titleColor': colorBadge['text_color'],
                          },
                        );
                      } else if (result.length < 25) {
                        return approvalControllers.hasMore.value &&
                                approvalControllers.isLoading.isTrue
                            ? BaseLoadingWidget.cardShimmer(
                                height: MediaQuery.of(context).size.height,
                                shimmerheight: 80,
                                baseColor: AppColor.kSoftGreyColor,
                                highlightColor: AppColor.kWhiteColor,
                                itemCount: 25,
                              )
                            : const SizedBox();
                      } else {
                        return approvalControllers.hasMore.value
                            ? BaseLoadingWidget.cardShimmer(
                                height: 200,
                                shimmerheight: 90,
                                baseColor: AppColor.kSoftGreyColor,
                                highlightColor: AppColor.kWhiteColor,
                                itemCount: 2,
                              )
                            : const SizedBox();
                      }
                    },
                  ),
          );
        },
      ),
    );

    final statusFilter = Container(
      padding: const EdgeInsets.only(top: 10.0),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        physics: const BouncingScrollPhysics(),
        child: Row(
          children: [
            BaseButtonWidget.buttonFilter(
              title: ["Semua", "Menunggu Persetujuan", "Disetujui", "Ditolak"],
              onPressed: [
                () {
                  approvalControllers.filterByStatus(['']);
                },
                () {
                  approvalControllers.filterByStatus(
                    ['status_code[]=PENDING', 'status_code[]=PROGRESS'],
                  );
                },
                () {
                  approvalControllers.filterByStatus(
                    ['status_code[]=APPROVE'],
                  );
                },
                () {
                  approvalControllers.filterByStatus(
                    ['status_code[]=REJECT'],
                  );
                },
              ],
              initialActive: 1,
              bColor: AppColor.kInactiveColor,
              contentColor: AppColor.kPrimaryColor,
              height: 10,
              width: 105,
            )
          ],
        ),
      ),
    );

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Aproval Kredit',
          context: context,
          isAllFill: true,
          searchController: approvalControllers.search,
          searchText: 'Cari',
          onSearchChange: () {
            BaseGlobalFuntion.debounceRun(
              action: () {
                approvalControllers.refreshDataApproval();
              },
              duration: const Duration(milliseconds: 1000),
            );
          },
          onFillterChange: () {
            showBottomFilter(context);
          },
        ),
        body: RefreshIndicator(
          onRefresh: onRefresh,
          child: SingleChildScrollView(
            controller: scrollController,
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                statusFilter,
                const SizedBox(height: 10),
                listItemContent,
              ],
            ),
          ),
        ),
      ),
    );
  }
}
