import 'package:flutter/services.dart';
import 'package:rbs_mobile_operation/data/datasources/customer.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/data/datasources/global/helper_maps.controllers.dart';

class AddProyekCustomer extends StatelessWidget {
  const AddProyekCustomer({super.key});

  @override
  Widget build(BuildContext context) {
    AddCustomerControllers addCustomerControllers =
        Get.put(AddCustomerControllers());

    HelperMapsControllers helperMapsControllers =
        Get.put(HelperMapsControllers());

    final cartCollapseContent = Card(
      margin: const EdgeInsets.all(0),
      child: Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: Obx(
          () => ExpansionTile(
            tilePadding: const EdgeInsets.symmetric(horizontal: 8),
            title: Text(
              "TOTAL",
              style: BaseTextStyle.customTextStyle(
                kfontSize: 14,
                kcolor: AppColor.kPrimaryColor,
                kfontWeight: FontWeight.w600,
              ),
            ),
            subtitle: Text(
              "${addCustomerControllers.dataProyekTemp.length} Proyek",
              style: BaseTextStyle.customTextStyle(
                kfontSize: 13,
                kfontWeight: FontWeight.w500,
              ),
            ),
            children: [
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                child: Obx(() {
                  var projectTemp = addCustomerControllers.dataProyekTemp;
                  return ListView.separated(
                    shrinkWrap: true,
                    itemCount: projectTemp.length,
                    separatorBuilder: (context, index) =>
                        const SizedBox(height: 5),
                    itemBuilder: (context, index) {
                      return ListTile(
                          contentPadding:
                              const EdgeInsets.symmetric(horizontal: 0),
                          title: Text(
                            "${projectTemp[index]['name']}",
                            style: BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kPrimaryColor,
                              kfontSize: 12,
                              kfontWeight: FontWeight.w500,
                            ),
                          ),
                          subtitle: Text(
                            BaseGlobalFuntion.convertTextLong(
                                text: "${projectTemp[index]['address']}",
                                maxLength: 25),
                            style: BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kPrimaryColor,
                              kfontSize: 12,
                              kfontWeight: FontWeight.w400,
                            ),
                          ),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              // IconButton.filledTonal(
                              //   onPressed: () {
                              //     // salesController.deleteItemCart(index);
                              //     // print(resultTemp);
                              //     // if (salesController.cart.isEmpty) {
                              //     //   Get.back();
                              //     // }
                              //     // addCustomerControllers.indexEdit['index'] =
                              //     //     index;
                              //     addCustomerControllers
                              //         .fkAddProyek.currentState!
                              //         .patchValue({
                              //       "customer_proyek": projectTemp[index]
                              //           ['name'],
                              //       "pic_proyek": projectTemp[index]
                              //           ['pic_name'],
                              //       "pic_phone": projectTemp[index]
                              //           ['pic_phone'],
                              //       "proyek_address": projectTemp[index]
                              //           ['address']
                              //     });
                              //   },
                              //   icon: const Icon(
                              //     Icons.edit,
                              //     size: 16,
                              //     color: AppColor.kPrimaryColor,
                              //   ),
                              //   style: FilledButton.styleFrom(
                              //     backgroundColor:
                              //         AppColor.kSoftBlueColor,
                              //   ),
                              // ),
                              IconButton.filledTonal(
                                onPressed: () {
                                  addCustomerControllers.dataProyekTemp.remove(
                                      addCustomerControllers
                                          .dataProyekTemp[index]);
                                  if (addCustomerControllers
                                      .dataProyekTemp.isEmpty) {
                                    Get.back();
                                  }
                                },
                                icon: const Icon(
                                  Icons.delete_rounded,
                                  size: 16,
                                  color: AppColor.kFailedColor,
                                ),
                                style: FilledButton.styleFrom(
                                  backgroundColor: AppColor.kBadgeFailedColor,
                                ),
                              ),
                            ],
                          ));
                    },
                  );
                }),
              )
            ],
          ),
        ),
      ),
    );

    final topContent = Container(
      height: 190,
      width: double.infinity,
      padding: const EdgeInsets.only(right: 20, left: 20, top: 15, bottom: 50),
      decoration: const BoxDecoration(
        color: AppColor.kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: AppColor.kWhiteColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: const Icon(
              Icons.fire_truck_rounded,
            ),
          ),
          const SizedBox(width: 15),
          Obx(() => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BaseTextWidget.customText(
                    text: addCustomerControllers.dataCustomerTemp['name'] ??
                        "CUSTOMER PROYEK",
                    maxLengthText: 25,
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                    color: AppColor.kWhiteColor,
                  ),
                  BaseTextWidget.customText(
                    text: addCustomerControllers.dataCustomerTemp['phone'] ??
                        "customer phone",
                    maxLengthText: 25,
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                    color: AppColor.kWhiteColor,
                  )
                ],
              ))
        ],
      ),
    );

    // Future<void> showBottomGoogleMap(BuildContext context) async {
    //   const String GOOGLE_MAPS_API_KEY =
    //       "AIzaSyAS-EGz_3eTlr0bTs9OoA2bmCphA1v0qvA";

    //   // LocationCoordinatesControllers locationCoordinatesControllers =
    //   //     Get.put(LocationCoordinatesControllers());

    //   Position position = await Geolocator.getCurrentPosition(
    //     desiredAccuracy: LocationAccuracy.medium,
    //   );

    //   // ignore: use_build_context_synchronously
    //   return showModalBottomSheet<void>(
    //     context: context,
    //     isScrollControlled: true,
    //     // constraints: BoxConstraints.tight(Size(
    //     //     MediaQuery.of(context).size.width,
    //     //     MediaQuery.of(context).size.height * .8)),
    //     backgroundColor: AppColor.kGreyColor.withOpacity(0.1),
    //     builder: (BuildContext context) {
    //       WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
    //         addCustomerControllers.helperMaps(
    //           lat: position.latitude,
    //           lng: position.longitude,
    //           addressPreview: addCustomerControllers.addressProjectControllers,
    //         );
    //       });

    //       return Container(
    //         padding: EdgeInsets.only(
    //           top: 10,
    //           left: 10,
    //           right: 10,
    //           bottom: MediaQuery.of(context).viewInsets.bottom,
    //         ),
    //         margin: const EdgeInsets.all(10),
    //         decoration: BoxDecoration(
    //           color: AppColor.kWhiteColor,
    //           borderRadius: BorderRadius.circular(15),
    //         ),
    //         child: Column(
    //           crossAxisAlignment: CrossAxisAlignment.start,
    //           mainAxisSize: MainAxisSize.min,
    //           children: [
    //             Flexible(
    //               child: PlacePicker(
    //                 apiKey: GOOGLE_MAPS_API_KEY,
    //                 initialPosition: LatLng(
    //                   position.latitude,
    //                   position.longitude,
    //                 ),
    //                 selectedPlaceWidgetBuilder:
    //                     (context, selectedPlace, state, isSearchBarFocused) {
    //                   WidgetsBinding.instance
    //                       .addPostFrameCallback((timeStamp) async {
    //                     if (selectedPlace != null) {
    //                       addCustomerControllers.helperMaps(
    //                         lat: selectedPlace.geometry!.location.lat,
    //                         lng: selectedPlace.geometry!.location.lng,
    //                       );

    //                       addCustomerControllers.addressProjectControllers
    //                           .text = selectedPlace.formattedAddress!;
    //                     }
    //                     // List<Placemark> addressNow =
    //                     //     await placemarkFromCoordinates(
    //                     //         , position.longitude);
    //                   });
    //                   return FloatingCard(
    //                     bottomPosition:
    //                         MediaQuery.of(context).size.height * 0.05,
    //                     leftPosition: MediaQuery.of(context).size.width * 0.02,
    //                     rightPosition: MediaQuery.of(context).size.width * 0.02,
    //                     width: MediaQuery.of(context).size.width * 0.9,
    //                     borderRadius: BorderRadius.circular(12.0),
    //                     child: state == SearchingState.Searching
    //                         ? const Padding(
    //                             padding: EdgeInsets.all(8.0),
    //                             child:
    //                                 Center(child: CircularProgressIndicator()),
    //                           )
    //                         : Container(
    //                             padding: const EdgeInsets.all(8.0),
    //                             width: double.infinity,
    //                             child: Column(
    //                               children: [
    //                                 Text(
    //                                   selectedPlace!.formattedAddress ?? "",
    //                                   style: BaseTextStyle.customTextStyle(
    //                                     kfontSize: 12,
    //                                     kcolor: AppColor.kPrimaryColor,
    //                                     kfontWeight: FontWeight.w400,
    //                                   ),
    //                                 ),
    //                                 // const SizedBox(height: 10),
    //                               ],
    //                             ),
    //                           ),
    //                   );
    //                 },
    //               ),
    //             ),
    //             const SizedBox(height: 10),
    //             Obx(() => BaseTextFieldWidget.textFieldFormBuilder2(
    //                   title: 'Kota/Kabupaten',
    //                   fieldName: 'kota',
    //                   readOnly: true,
    //                   controller: TextEditingController(
    //                       text: addCustomerControllers
    //                           .dataMapCustomerHelper['city_name']),
    //                   onChange: ({value}) {},
    //                   validator: (value) {
    //                     if (value == null || value.isEmpty) {
    //                       return 'alamat pelanggan wajib diisi!';
    //                     }
    //                     return null;
    //                   },
    //                   textInputAction: TextInputAction.next,
    //                 )),
    //             const SizedBox(height: 5),
    //             Obx(() => BaseTextFieldWidget.textFieldFormBuilder2(
    //                   title: 'Desa/Kelurahan',
    //                   fieldName: 'desa',
    //                   readOnly: true,
    //                   controller: TextEditingController(
    //                     text: addCustomerControllers
    //                         .dataMapCustomerHelper['district_name'],
    //                   ),
    //                   onChange: ({value}) {},
    //                   validator: (value) {
    //                     if (value == null || value.isEmpty) {
    //                       return 'alamat pelanggan wajib diisi!';
    //                     }
    //                     return null;
    //                   },
    //                   textInputAction: TextInputAction.next,
    //                 )),
    //             const SizedBox(height: 5),
    //             Obx(() => BaseTextFieldWidget.textFieldFormBuilder2(
    //                   title: 'Kecamatan',
    //                   fieldName: 'kecamatan',
    //                   readOnly: true,
    //                   controller: TextEditingController(
    //                     text: addCustomerControllers
    //                         .dataMapCustomerHelper['sub_district_name'],
    //                   ),
    //                   onChange: ({value}) {},
    //                   validator: (value) {
    //                     if (value == null || value.isEmpty) {
    //                       return 'alamat pelanggan wajib diisi!';
    //                     }
    //                     return null;
    //                   },
    //                   textInputAction: TextInputAction.next,
    //                 )),
    //             const SizedBox(height: 5),
    //             BaseTextFieldWidget.textFieldFormBuilder2(
    //               isRequired: true,
    //               title: 'Alamat Lengkap',
    //               fieldName: 'address',
    //               isLongText: true,
    //               controller: addCustomerControllers.addressProjectControllers,
    //               onChange: ({value}) {},
    //               validator: (value) {
    //                 if (value == null || value.isEmpty) {
    //                   return 'alamat pelanggan wajib diisi!';
    //                 }
    //                 return null;
    //               },
    //               textInputAction: TextInputAction.done,
    //             ),
    //             ElevatedButton.icon(
    //               icon: const Icon(Icons.check_circle_outline_outlined),
    //               onPressed: () async {
    //                 // addCustomerControllers.helperMaps(
    //                 //   lat: selectedPlace
    //                 //       .geometry!.location.lat,
    //                 //   lng: selectedPlace
    //                 //       .geometry!.location.lng,
    //                 // );

    //                 // addCustomerControllers
    //                 //         .detailAddressControllers.text =
    //                 //     selectedPlace.formattedAddress!;

    //                 Get.back();
    //               },
    //               style: ElevatedButton.styleFrom(
    //                 fixedSize: const Size(500, 40),
    //                 shape: RoundedRectangleBorder(
    //                   borderRadius: BorderRadius.circular(10.0),
    //                 ),
    //               ),
    //               label: Text(
    //                 "Pilih Lokasi",
    //                 style: BaseTextStyle.customTextStyle(
    //                   kfontSize: 12,
    //                   kcolor: AppColor.kPrimaryColor,
    //                   kfontWeight: FontWeight.w500,
    //                 ),
    //               ),
    //             ),
    //           ],
    //         ),
    //       );
    //     },
    //   );
    // }

    Future<void> showBottomAddCart(BuildContext context, bool isEmptyCart) {
      return showModalBottomSheet(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(15.0))),
        backgroundColor: AppColor.kWhiteColor,
        // backgroundColor: Colors.transparent,
        isDismissible: !isEmptyCart,
        context: context,
        isScrollControlled: true,
        builder: (context) => PopScope(
          canPop: !isEmptyCart,
          child: Padding(
            padding: EdgeInsets.only(
                top: 10,
                right: 15,
                left: 15,
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Tambahkan Proyek",
                      style: BaseTextStyle.customTextStyle(
                        kfontSize: 14,
                        kfontWeight: FontWeight.w600,
                      ),
                    ),
                    IconButton.filledTonal(
                      onPressed: () {
                        Get.back();
                        if (isEmptyCart) {
                          Get.back();
                        }
                      },
                      icon: const Icon(Icons.close),
                    ),
                  ],
                ),
                const Divider(),
                FormBuilder(
                  key: addCustomerControllers.fkAddProyek,
                  onChanged: () {
                    addCustomerControllers.fkAddProyek.currentState!.save();
                  },
                  autovalidateMode: AutovalidateMode.disabled,
                  skipDisabled: true,
                  child: Flexible(
                    child: ListView(
                      shrinkWrap: true,
                      physics: const AlwaysScrollableScrollPhysics(),
                      children: [
                        BaseTextFieldWidget.textFieldFormBuilder2(
                          title: 'Nama Proyek',
                          fieldName: 'customer_proyek',
                          hintText: "Masukan nama proyek",
                          isRequired: true,
                          onClearText: () => addCustomerControllers
                              .fkAddProyek.currentState!
                              .patchValue({'customer_proyek': null}),
                          onChange: ({value}) {},
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Nama proyek wajib diisi!';
                            }
                            return null;
                          },
                          textInputAction: TextInputAction.next,
                          inputFormater: <TextInputFormatter>[
                            // FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]')),
                            // Formatter untuk mengubah input menjadi huruf kapital
                            UppercaseInputFormatter(),
                          ],
                        ),
                        const SizedBox(height: 5),
                        BaseTextFieldWidget.textFieldFormBuilder2(
                          title: 'Nama PIC',
                          fieldName: 'pic_proyek',
                          hintText: "Ketik pic proyek disini..",
                          isRequired: true,
                          onClearText: () => addCustomerControllers
                              .fkAddProyek.currentState!
                              .patchValue({'pic_proyek': null}),
                          onChange: ({value}) {},
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Pic proyek wajib diisi!';
                            }
                            return null;
                          },
                          textInputAction: TextInputAction.next,
                        ),
                        const SizedBox(height: 5),
                        BaseTextFieldWidget.textFieldFormBuilder2(
                          title: 'No.Telp',
                          fieldName: 'pic_phone',
                          hintText: "Masukan nomor telepon",
                          isRequired: true,
                          onClearText: () => addCustomerControllers
                              .fkAddProyek.currentState!
                              .patchValue({'pic_phone': null}),
                          onChange: ({value}) {},
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'No.telp wajib diisi!';
                            }
                            return null;
                          },
                          keyboardType: TextInputType.phone,
                          textInputAction: TextInputAction.next,
                          inputFormater: <TextInputFormatter>[
                            FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                            // for version 2 and greater youcan also use this
                            FilteringTextInputFormatter.digitsOnly
                          ],
                        ),
                        const SizedBox(height: 5),
                        BaseTextFieldWidget.textFieldFormBuilder2(
                          title: 'Alamat Lengkap',
                          fieldName: 'proyek_address',
                          hintText: "Pilih alamat proyek",
                          isRequired: true,
                          onClearText: () => addCustomerControllers
                              .fkAddProyek.currentState!
                              .patchValue({'proyek_address': null}),
                          readOnly: true,
                          isLongText: true,
                          controller:
                              addCustomerControllers.addressProjectControllers,
                          onTap: () {
                            BaseExtendWidget.mapsPicker(
                              context: context,
                              mapsHelper: helperMapsControllers.helperMaps,
                              detailMapsControllers: addCustomerControllers
                                  .addressProjectControllers,
                              collectMapsData:
                                  helperMapsControllers.dataMapProjectHelper,
                            );
                          },
                          onChange: ({value}) {},
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Alamat proyek wajib diisi!';
                            }
                            return null;
                          },
                          textInputAction: TextInputAction.next,
                        ),
                        const SizedBox(height: 15),
                      ],
                    ),
                  ),
                ),
                Obx(
                  () => BaseButtonWidget.primaryIconButton(
                    onPressed: () {
                      addCustomerControllers.addCustomerProject(
                        addressMap: helperMapsControllers.dataMapProjectHelper,
                      );
                      // loadingTest(context);
                    },
                    width: double.infinity,
                    // isDisable: addCustomerControllers.isLoading.isTrue,
                    isLoading: addCustomerControllers.isLoading.isTrue,
                    kcolor: AppColor.kPrimaryColor,
                    height: 60,
                    iconData: Icons.queue_play_next,
                    textStyle: BaseTextStyle.customTextStyle(
                      kfontWeight: FontWeight.w600,
                      kfontSize: 12,
                      kcolor: AppColor.kWhiteColor,
                    ),
                    title: "Tambah",
                  ),
                ),
                const SizedBox(height: 15),
              ],
            ),
          ),
        ),
      );
    }

    final detailPayment = Container(
      padding: const EdgeInsets.all(15.0),
      margin: const EdgeInsets.only(right: 10, left: 10, top: 80),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: AppColor.kGreyColor),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          cartCollapseContent,
          const SizedBox(height: 20),
          IconButton.filledTonal(
            iconSize: 20,
            onPressed: () {
              showBottomAddCart(context, false);
            },
            icon: const Icon(
              Icons.add,
            ),
          )
        ],
      ),
    );

    // padding: MediaQuery.of(context).viewInsets,
    final submitAction = Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Obx(() => BaseButtonWidget.primaryIconButton(
                  onPressed: () => addCustomerControllers.addDataCustomer(
                      loadingCtx: context),
                  isDisable: addCustomerControllers.dataProyekTemp.isEmpty,
                  isLoading: addCustomerControllers.isLoading.value,
                  kcolor: AppColor.kPrimaryColor,
                  height: 60,
                  iconData: Icons.add_circle,
                  textStyle: BaseTextStyle.customTextStyle(
                    kfontWeight: FontWeight.w600,
                    kfontSize: 12,
                    kcolor: AppColor.kWhiteColor,
                  ),
                  title: "Simpan",
                )),
          ),
        ],
      ),
    );

    // if (addCustomerControllers.dataProyekTemp.isEmpty) {
    //   // WidgetsBinding.instance.addPostFrameCallback((_) {
    //   showBottomAddCart(context);
    //   // });
    // }
    if (addCustomerControllers.dataProyekTemp.isEmpty) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        showBottomAddCart(context, true);
      });
    }

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        title: 'Tambahkan Proyek',
        context: context,
        extendBack: () {
          // salesController.dataLookupCustomerProject.clear();
          // salesController.dataDetailCustomer.clear();
        },
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            topContent,
            detailPayment,
          ],
        ),
      ),
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: submitAction,
    );
  }
}
