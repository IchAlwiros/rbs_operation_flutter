import 'package:flutter/services.dart';
import 'package:rbs_mobile_operation/data/datasources/customer.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/data/datasources/global/helper_maps.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';

class AddCustomer extends StatelessWidget {
  const AddCustomer({super.key});

  @override
  Widget build(BuildContext context) {
    AddCustomerControllers addCustomerControllers =
        Get.put(AddCustomerControllers());

    HelperMapsControllers helperMapsControllers =
        Get.put(HelperMapsControllers());

    final submitAction = Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: BaseButtonWidget.primaryIconButton(
                onPressed: () {
                  // addCustomerControllers.addCustomer();
                  helperMapsControllers.dataMapProjectHelper.clear();
                },
                isDisable: false,
                kcolor: AppColor.kPrimaryColor,
                height: 60,
                iconData: Icons.arrow_circle_right_rounded,
                rightIcon: true,
                textStyle: BaseTextStyle.customTextStyle(
                  kfontWeight: FontWeight.w600,
                  kfontSize: 12,
                  kcolor: AppColor.kWhiteColor,
                ),
                title: "Selanjutnya",
              ),
            ),
          ],
        ),
      ),
    );

    final topContent = Container(
      height: 190,
      width: double.infinity,
      padding: const EdgeInsets.only(right: 20, left: 20, bottom: 20),
      decoration: const BoxDecoration(
        color: AppColor.kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
      ),
    );

    final paymentMethod = ['CASH', 'CREDIT', 'TOP'];

    final detailPayment = Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      margin: const EdgeInsets.only(right: 10, left: 10, top: 10, bottom: 10),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: AppColor.kGreyColor),
      ),
      child: Column(
        children: [
          FormBuilder(
            key: addCustomerControllers.fkAddCustomer,
            onChanged: () {
              addCustomerControllers.fkAddCustomer.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Obx(() => addCustomerControllers.dataLookupPU.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih unit produksi',
                          label: "Unit Produksi",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib dipilih!';
                            }
                            return null;
                          },
                        )
                      : BaseButtonWidget.dropdownSearch(
                          fieldName: 'production_unit_id',
                          isRequired: true,
                          controller:
                              addCustomerControllers.productionUnitController,
                          initialValue: ApiService.productionUnitGlobal ??
                              LocalDataSource.getLocalVariable(
                                  key: 'lastestProduction'),
                          label: "Unit Produksi",
                          hintText: "Pilih Unit Produksi",
                          asyncItems: (String filter) async {
                            addCustomerControllers.lookupProductionUnit(
                                searching: filter);
                            return addCustomerControllers.dataLookupPU;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            addCustomerControllers.currentFilter(
                                {"production_unit_id": valueItem});

                            addCustomerControllers.lookupCustomerCategory();
                            addCustomerControllers.dataLookupCustomerCategory
                                .clear();
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib dipilih!';
                            }
                            return null;
                          },
                        )),

                  const SizedBox(height: 5),
                  Obx(
                    () => addCustomerControllers
                            .dataLookupCustomerCategory.isEmpty
                        ? BaseTextFieldWidget.disableTextForm2(
                            disabledText: 'Pilih tipe pelanggan',
                            label: "Tipe Pelanggan",
                            isRequired: true,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Tipe pelanggan wajib dipilih!';
                              }
                              return null;
                            },
                          )
                        : BaseButtonWidget.dropdownSearch(
                            fieldName: 'customer_category_id',
                            isRequired: true,
                            controller:
                                addCustomerControllers.customerTypeController,
                            label: "Tipe Pelanggan",
                            hintText: "Pilih Tipe Pelanggan",
                            asyncItems: (String filter) async {
                              addCustomerControllers.lookupCustomerCategory(
                                  searching: filter);
                              return addCustomerControllers
                                  .dataLookupCustomerCategory;
                            },
                            onChange: ({searchValue, valueItem, data}) {},
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Tipe pelanggan wajib dipilih!';
                              }
                              return null;
                            },
                          ),
                  ),
                  // Obx(
                  //   () => addCustomerControllers
                  //           .dataLookupCustomerCategory.isEmpty
                  //       ? BaseTextFieldWidget.disableTextForm2(
                  //           disabledText: 'Pilih tipe pelanggan',
                  //           label: "Tipe Pelanggan",
                  //           isRequired: true,
                  //           validator: (value) {
                  //             if (value == null || value.isEmpty) {
                  //               return 'tipe pelanggan wajib diisi!';
                  //             }
                  //             return null;
                  //           },
                  //         )
                  //       : BaseButtonWidget.dropdownButton2(
                  //           fieldName: "customer_category_id",
                  //           isRequired: true,
                  //           valueController: customerTypeController,
                  //           valueData: addCustomerControllers
                  //               .dataLookupCustomerCategory,
                  //           label: "Tipe pelanggan",
                  //           hintText: 'Pilih tipe pelanggan',
                  //           searchOnChange: (p0) {},
                  //           onChange: ({searchValue, valueItem}) {},
                  //           validator: (value) {
                  //             if (value == null || value.isEmpty) {
                  //               return 'tipe pelanggan wajib diisi!';
                  //             }
                  //             return null;
                  //           },
                  //         ),
                  // ),
                  const SizedBox(height: 5),
                  BaseTextFieldWidget.textFieldFormBuilder2(
                    title: 'Nama Pelanggan',
                    hintText: "Ketik Nama disini..",
                    fieldName: 'customer_name',
                    isRequired: true,
                    onClearText: () => addCustomerControllers
                        .fkAddCustomer.currentState!
                        .patchValue({'customer_name': null}),
                    // controller: TextEditingController(),
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Nama wajib diisi!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                    inputFormater: <TextInputFormatter>[
                      // FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]')),
                      // Formatter untuk mengubah input menjadi huruf kapital
                      UppercaseInputFormatter(),
                    ],
                  ),
                  const SizedBox(height: 5),
                  BaseTextFieldWidget.textFieldFormBuilder2(
                    title: 'Email',
                    fieldName: 'email',
                    hintText: "Ketik Email disini..",
                    onClearText: () => addCustomerControllers
                        .fkAddCustomer.currentState!
                        .patchValue({'email': null}),
                    // controller: TextEditingController(),
                    onChange: ({value}) {},
                    validator: (value) {
                      // if (value != null || value!.isNotEmpty) {
                      //   if (!value.contains("@")) {
                      //     return 'Alamat email tidak sesuai!';
                      //   }
                      // }
                      if (value!.isNotEmpty) {
                        if (!value.contains('@')) {
                          return 'Alamat email tidak sesuai!';
                        }
                      }

                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                  const SizedBox(height: 10),
                  BaseTextFieldWidget.textFieldFormBuilder2(
                      title: 'No.Telp',
                      fieldName: 'phone',
                      hintText: "Ketik No.Telp disini..",
                      isRequired: true,
                      onClearText: () => addCustomerControllers
                          .fkAddCustomer.currentState!
                          .patchValue({'phone': null}),
                      onChange: ({value}) {},
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'No telepon wajib diisi!';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.phone,
                      textInputAction: TextInputAction.next,
                      inputFormater: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                        // for version 2 and greater youcan also use this
                        FilteringTextInputFormatter.digitsOnly
                      ]),
                  const SizedBox(height: 10),
                  FormBuilderFilterChip<String>(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      label: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            "Metode Pembayaran",
                            style: BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kPrimaryColor,
                              kfontSize: 16,
                              kfontWeight: FontWeight.w500,
                            ),
                          ),
                          Text(
                            "*",
                            style: BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kErrorColor,
                              kfontSize: 15,
                              kfontWeight: FontWeight.w600,
                            ),
                          )
                        ],
                      ),
                      errorStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 12,
                        kcolor: AppColor.kFailedColor,
                        kfontWeight: FontWeight.w500,
                      ),
                    ),
                    name: 'payment_type_code',
                    selectedColor: AppColor.kInactiveColor,
                    spacing: 6.0,
                    options: List.generate(paymentMethod.length, (index) {
                      return FormBuilderChipOption(
                        value: paymentMethod[index],
                        child: Text(
                          paymentMethod[index],
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontWeight: FontWeight.w600,
                            kfontSize: 12,
                          ),
                        ),
                      );
                    }),
                    // onChanged: _onChanged,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Metode pembayaran wajib dipilih!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 10),
                  BaseTextFieldWidget.textFieldFormBuilder2(
                    title: 'Alamat Lengkap',
                    hintText: "Masukan Alamat",
                    fieldName: 'address',
                    isRequired: true,
                    onClearText: () => addCustomerControllers
                        .fkAddCustomer.currentState!
                        .patchValue({'address': null}),
                    readOnly: true,
                    isLongText: true,
                    controller: helperMapsControllers.detailAddressControllers,
                    onTap: () {
                      // showBottomGoogleMap(context);
                      BaseExtendWidget.mapsPicker(
                        context: context,
                        mapsHelper: helperMapsControllers.helperMaps,
                        detailMapsControllers:
                            helperMapsControllers.detailAddressControllers,
                        collectMapsData:
                            helperMapsControllers.dataMapProjectHelper,
                      );
                    },
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Alamat pelanggan wajib diisi!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.done,
                  ),
                  const SizedBox(height: 10),
                ],
              ),
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        title: 'Tambah Pelanggan',
        context: context,
        extendBack: () {
          // salesController.dataLookupCustomerProject.clear();
          // salesController.dataDetailCustomer.clear();
        },
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            topContent,
            const SizedBox(height: 10),
            detailPayment,
          ],
        ),
      ),
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: submitAction,
    );
  }
}
