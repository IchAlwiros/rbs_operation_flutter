import 'package:flutter/services.dart';
import 'package:rbs_mobile_operation/core/components/components.dart';
import 'package:rbs_mobile_operation/data/datasources/customer.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/data/datasources/global/helper_maps.controllers.dart';

class AddProjectCustomer extends StatelessWidget {
  final dynamic detailCustomer;
  // final dynamic customerId;
  const AddProjectCustomer({
    required this.detailCustomer,
    // required this.customerId,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    AddCustomerProjectControllers customerProjectControllers =
        Get.put(AddCustomerProjectControllers());

    HelperMapsControllers helperMapsControllers =
        Get.put(HelperMapsControllers());

    final submitAction = Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Obx(() => Expanded(
                  child: Button.filled(
                    onPressed: () => customerProjectControllers.addProject(
                      detailCustomer: detailCustomer,
                      addressMaps: helperMapsControllers.dataMapProjectHelper,
                      loadingCtx: context,
                    ),
                    disabled: customerProjectControllers.isLoading.value,
                    color: AppColor.kPrimaryColor,
                    // height: 50
                    icon: const Icon(
                      Icons.add_circle,
                      color: AppColor.kWhiteColor,
                    ),
                    // textStyle: BaseTextStyle.customTextStyle(
                    //   kfontWeight: FontWeight.w600,
                    //   kfontSize: 12,
                    //   kcolor: AppColor.kWhiteColor,
                    // ),
                    label: "Simpan",
                  ),
                )),
          ],
        ),
      ),
    );

    final topContent = Container(
      height: 190,
      width: double.infinity,
      padding: const EdgeInsets.only(right: 20, left: 20, bottom: 20),
      decoration: const BoxDecoration(
        color: AppColor.kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
      ),
    );

    final detailPayment = Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      margin: const EdgeInsets.only(right: 10, left: 10, top: 10, bottom: 10),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: AppColor.kGreyColor),
      ),
      child: Column(
        children: [
          FormBuilder(
            key: customerProjectControllers.fkAddProject,
            onChanged: () {
              customerProjectControllers.fkAddProject.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  const SizedBox(height: 10),
                  CustomTextField.style2(
                    title: 'Nama Proyek',
                    hintText: "Masukan Nama Proyek",
                    fieldName: 'project_name',
                    isRequired: true,
                    onClearText: () => customerProjectControllers
                        .fkAddProject.currentState!
                        .patchValue({'project_name': null}),
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'nama proyek wajib diisi!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                    inputFormater: <TextInputFormatter>[
                      // FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]')),
                      // Formatter untuk mengubah input menjadi huruf kapital
                      UppercaseInputFormatter(),
                    ],
                  ),
                  const SizedBox(height: 10),
                  CustomTextField.style2(
                    title: 'Nama PIC',
                    hintText: "Masukan Nama PIC",
                    fieldName: 'project_pic',
                    isRequired: true,
                    onClearText: () => customerProjectControllers
                        .fkAddProject.currentState!
                        .patchValue({'project_pic': null}),
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'nama pic proyek wajib diisi!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                  const SizedBox(height: 10),
                  CustomTextField.style2(
                      title: 'No.Telp',
                      hintText: 'Masukan No.Telp',
                      fieldName: 'phone',
                      onChange: ({value}) {},
                      onClearText: () => customerProjectControllers
                          .fkAddProject.currentState!
                          .patchValue({'phone': null}),
                      validator: (value) {
                        return null;
                      },
                      keyboardType: TextInputType.phone,
                      textInputAction: TextInputAction.next,
                      inputFormater: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                        // for version 2 and greater youcan also use this
                        FilteringTextInputFormatter.digitsOnly
                      ]),
                  const SizedBox(height: 10),
                  CustomTextField.style2(
                    title: 'Alamat Lengkap',
                    fieldName: 'address',
                    hintText: 'Pilih Alamat',
                    isRequired: true,
                    onClearText: () => customerProjectControllers
                        .fkAddProject.currentState!
                        .patchValue({'address': null}),
                    readOnly: true,
                    isLongText: true,
                    controller: helperMapsControllers.detailAddressControllers,
                    onTap: () {
                      BaseExtendWidget.mapsPicker(
                        context: context,
                        mapsHelper: helperMapsControllers.helperMaps,
                        detailMapsControllers:
                            helperMapsControllers.detailAddressControllers,
                        collectMapsData:
                            helperMapsControllers.dataMapProjectHelper,
                      );
                    },
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'alamat wajib diisi!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        title: 'Tambah Proyek',
        centerTitle: true,
        context: context,
        extendBack: () {
          // salesController.dataLookupCustomerProject.clear();
          // salesController.dataDetailCustomer.clear();
        },
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            topContent,
            const SizedBox(height: 10),
            detailPayment,
          ],
        ),
      ),
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: submitAction,
    );
  }
}
