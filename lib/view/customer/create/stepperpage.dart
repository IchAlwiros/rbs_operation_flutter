import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rbs_mobile_operation/core/components/regional_maps_picker.dart';
import 'package:rbs_mobile_operation/data/datasources/customer.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/data/datasources/global/helper_maps.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';

class StepperAddCustomer extends StatelessWidget {
  const StepperAddCustomer({super.key});

  @override
  Widget build(BuildContext context) {
    AddCustomerControllers addCustomerControllers =
        Get.put(AddCustomerControllers());
    // var addCustomerControllers.currentStep = 0.obs;

    HelperMapsControllers helperMapsControllers =
        Get.put(HelperMapsControllers());

    Widget controlBuilders(context, details) {
      return Padding(
        padding: const EdgeInsets.all(0.0),
        child: Row(
          children: [
            if (addCustomerControllers.currentStep.value == 2) ...[
              // ElevatedButton(onPressed: () {}, child: Text("TELO")),
              // ElevatedButton(
              //     onPressed: () {
              //       identifier['preview'] = locationPreview.value;
              //       collectInputController.collectLocationIdentifier
              //           .assignAll(identifier);

              //       Navigator.pop(context);
              //     },
              //     style: ElevatedButton.styleFrom(
              //       backgroundColor: kPrimaryColor,
              //       // fixedSize: const Size(150, 50),
              //       shape: RoundedRectangleBorder(
              //         borderRadius: BorderRadius.circular(10),
              //       ),
              //     ),
              //     child: Text(
              //       "Gunakan Data",
              //       style: customTextStyle(
              //         kcolor: kWhiteColor,
              //         kfontSize: 14,
              //         kfontWeight: FontWeight.w600,
              //       ),
              //     ))
            ]
            // ElevatedButton(
            //   onPressed: () {
            //     details.onStepContinue();
            //   },
            //   child: const Text('Next'),
            // ),
            // const SizedBox(width: 10),
            // OutlinedButton(
            //   onPressed: details.onStepCancel,
            //   child: const Text('Back'),
            // ),
          ],
        ),
      );
    }

    continueStep() {
      if (addCustomerControllers.currentStep < 1) {
        // controllerData.searchFilter.clear();
        addCustomerControllers.currentStep.value = addCustomerControllers
            .currentStep.value += 1; //addCustomerControllers.currentStep+=1;
      }
    }

    cancelStep() {
      if (addCustomerControllers.currentStep > 0) {
        addCustomerControllers.currentStep.value =
            addCustomerControllers.currentStep.value -
                1; //addCustomerControllers.currentStep-=1;
      }
    }

    onStepTapped(int value) {
      addCustomerControllers.currentStep.value = value;
    }

    final paymentMethod = ['CASH', 'CREDIT', 'TOP'];

    final formDetailsCustomer = Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      margin: const EdgeInsets.only(right: 0, left: 0, top: 0, bottom: 0),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: AppColor.kGreyColor),
      ),
      child: Column(
        children: [
          FormBuilder(
            key: addCustomerControllers.fkAddCustomer,
            onChanged: () {
              addCustomerControllers.fkAddCustomer.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Obx(() => addCustomerControllers.dataLookupPU.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih unit produksi',
                          label: "Unit Produksi",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib dipilih!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'production_unit_id',
                          isRequired: true,
                          controller:
                              addCustomerControllers.productionUnitController,
                          initialValue: ApiService.productionUnitGlobal ??
                              LocalDataSource.getLocalVariable(
                                  key: 'lastestProduction'),
                          label: "Unit Produksi",
                          hintText: "Pilih Unit Produksi",
                          asyncItems: (String filter) async {
                            addCustomerControllers.lookupProductionUnit(
                                searching: filter);
                            return addCustomerControllers.dataLookupPU;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            addCustomerControllers.currentFilter(
                                {"production_unit_id": valueItem});

                            addCustomerControllers.lookupCustomerCategory();
                            addCustomerControllers.dataLookupCustomerCategory
                                .clear();
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib dipilih!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  Obx(
                    () => addCustomerControllers
                            .dataLookupCustomerCategory.isEmpty
                        ? BaseTextFieldWidget.disableTextForm2(
                            disabledText: 'Pilih tipe pelanggan',
                            label: "Tipe Pelanggan",
                            isRequired: true,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Tipe pelanggan wajib dipilih!';
                              }
                              return null;
                            },
                          )
                        : CustomDropDownSearch(
                            fieldName: 'customer_category_id',
                            isRequired: true,
                            controller:
                                addCustomerControllers.customerTypeController,
                            label: "Tipe Pelanggan",
                            hintText: "Pilih Tipe Pelanggan",
                            asyncItems: (String filter) async {
                              addCustomerControllers.lookupCustomerCategory(
                                  searching: filter);
                              return addCustomerControllers
                                  .dataLookupCustomerCategory;
                            },
                            onChange: ({searchValue, valueItem, data}) {},
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Tipe pelanggan wajib dipilih!';
                              }
                              return null;
                            },
                          ),
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Nama Pelanggan',
                    hintText: "Ketik Nama disini..",
                    fieldName: 'customer_name',
                    isRequired: true,
                    onClearText: () => addCustomerControllers
                        .fkAddCustomer.currentState!
                        .patchValue({'customer_name': null}),
                    // controller: TextEditingController(),
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Nama wajib diisi!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                    inputFormater: <TextInputFormatter>[
                      // FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]')),
                      // Formatter untuk mengubah input menjadi huruf kapital
                      UppercaseInputFormatter(),
                    ],
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Email',
                    fieldName: 'email',
                    hintText: "Ketik Email disini..",
                    onClearText: () => addCustomerControllers
                        .fkAddCustomer.currentState!
                        .patchValue({'email': null}),
                    // controller: TextEditingController(),
                    onChange: ({value}) {},
                    keyboardType: TextInputType.emailAddress,
                    validator: (value) {
                      if (value != null) {
                        if (!value.contains('@')) {
                          return 'Alamat email tidak sesuai!';
                        }
                      }

                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                  const SizedBox(height: 10),
                  CustomTextField.style2(
                      title: 'No.Telp',
                      fieldName: 'phone',
                      hintText: "Ketik No.Telp disini..",
                      isRequired: true,
                      onClearText: () => addCustomerControllers
                          .fkAddCustomer.currentState!
                          .patchValue({'phone': null}),
                      onChange: ({value}) {},
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'No telepon wajib diisi!';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.phone,
                      textInputAction: TextInputAction.next,
                      inputFormater: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                        // for version 2 and greater youcan also use this
                        FilteringTextInputFormatter.digitsOnly
                      ]),
                  const SizedBox(height: 10),
                  FormBuilderFilterChip<String>(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      label: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            "Metode Pembayaran",
                            style: BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kPrimaryColor,
                              kfontSize: 16,
                              kfontWeight: FontWeight.w600,
                            ),
                          ),
                          Text(
                            "*",
                            style: BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kErrorColor,
                              kfontSize: 15,
                              kfontWeight: FontWeight.w600,
                            ),
                          )
                        ],
                      ),
                      errorStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 12,
                        kcolor: AppColor.kFailedColor,
                        kfontWeight: FontWeight.w500,
                      ),
                    ),
                    name: 'payment_type_code',
                    selectedColor: AppColor.kInactiveColor,
                    spacing: 6.0,
                    options: List.generate(paymentMethod.length, (index) {
                      return FormBuilderChipOption(
                        value: paymentMethod[index],
                        child: Text(
                          paymentMethod[index],
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontWeight: FontWeight.w600,
                            kfontSize: 12,
                          ),
                        ),
                      );
                    }),
                    // onChanged: _onChanged,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Metode pembayaran wajib dipilih!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 10),
                  CustomTextField.style2(
                    title: 'Alamat Lengkap',
                    hintText: "Masukan Alamat",
                    fieldName: 'address',
                    isRequired: true,
                    onClearText: () => addCustomerControllers
                        .fkAddCustomer.currentState!
                        .patchValue({'address': null}),
                    readOnly: true,
                    isLongText: true,
                    controller: helperMapsControllers.detailAddressControllers,
                    onTap: () async {
                      // showBottomGoogleMap(context);
                      // Position? currentPosition =
                      //     await Geolocator.getLastKnownPosition();

                      // showAboutDialog(context: context);

                      Get.to(
                        () => GMapsPicker(
                          // position: currentPosition!,
                          mapsHelper: helperMapsControllers.helperMaps,
                          detailMapsControllers:
                              helperMapsControllers.detailAddressControllers,
                          collectMapsData:
                              helperMapsControllers.dataMapProjectHelper,
                        ),
                      );
                      // BaseExtendWidget.mapsPicker(
                      //   context: context,
                      //   mapsHelper: helperMapsControllers.helperMaps,
                      //   detailMapsControllers:
                      //       helperMapsControllers.detailAddressControllers,
                      //   collectMapsData:
                      //       helperMapsControllers.dataMapProjectHelper,
                      // );
                    },
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Alamat pelanggan wajib diisi!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.done,
                  ),
                  const SizedBox(height: 10),
                ],
              ),
            ),
          ),
        ],
      ),
    );

    // PROYEK
    // final cartCollapseContent = Card(
    //   margin: const EdgeInsets.all(0),
    //   child: Theme(
    //     data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
    //     child: Obx(
    //       () => ExpansionTile(
    //         tilePadding: const EdgeInsets.symmetric(horizontal: 8),
    //         title: Text(
    //           "TOTAL",
    //           style: BaseTextStyle.customTextStyle(
    //             kfontSize: 14,
    //             kcolor: AppColor.kPrimaryColor,
    //             kfontWeight: FontWeight.w600,
    //           ),
    //         ),
    //         subtitle: Text(
    //           "${addCustomerControllers.dataProyekTemp.length} Proyek",
    //           style: BaseTextStyle.customTextStyle(
    //             kfontSize: 13,
    //             kfontWeight: FontWeight.w500,
    //           ),
    //         ),
    //         children: [
    //           Padding(
    //             padding:
    //                 const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
    //             child: Obx(() {
    //               var projectTemp = addCustomerControllers.dataProyekTemp;
    //               return ListView.separated(
    //                 shrinkWrap: true,
    //                 itemCount: projectTemp.length,
    //                 separatorBuilder: (context, index) =>
    //                     const SizedBox(height: 5),
    //                 itemBuilder: (context, index) {
    //                   return ListTile(
    //                       contentPadding:
    //                           const EdgeInsets.symmetric(horizontal: 0),
    //                       title: Text(
    //                         "${projectTemp[index]['name']}",
    //                         style: BaseTextStyle.customTextStyle(
    //                           kcolor: AppColor.kPrimaryColor,
    //                           kfontSize: 12,
    //                           kfontWeight: FontWeight.w500,
    //                         ),
    //                       ),
    //                       subtitle: Text(
    //                         BaseGlobalFuntion.convertTextLong(
    //                             text: "${projectTemp[index]['address']}",
    //                             maxLength: 25),
    //                         style: BaseTextStyle.customTextStyle(
    //                           kcolor: AppColor.kPrimaryColor,
    //                           kfontSize: 12,
    //                           kfontWeight: FontWeight.w400,
    //                         ),
    //                       ),
    //                       trailing: Row(
    //                         mainAxisSize: MainAxisSize.min,
    //                         children: [
    //                           // IconButton.filledTonal(
    //                           //   onPressed: () {
    //                           //     // salesController.deleteItemCart(index);
    //                           //     // print(resultTemp);
    //                           //     // if (salesController.cart.isEmpty) {
    //                           //     //   Get.back();
    //                           //     // }
    //                           //     // addCustomerControllers.indexEdit['index'] =
    //                           //     //     index;
    //                           //     addCustomerControllers
    //                           //         .fkAddProyek.currentState!
    //                           //         .patchValue({
    //                           //       "customer_proyek": projectTemp[index]
    //                           //           ['name'],
    //                           //       "pic_proyek": projectTemp[index]
    //                           //           ['pic_name'],
    //                           //       "pic_phone": projectTemp[index]
    //                           //           ['pic_phone'],
    //                           //       "proyek_address": projectTemp[index]
    //                           //           ['address']
    //                           //     });
    //                           //   },
    //                           //   icon: const Icon(
    //                           //     Icons.edit,
    //                           //     size: 16,
    //                           //     color: AppColor.kPrimaryColor,
    //                           //   ),
    //                           //   style: FilledButton.styleFrom(
    //                           //     backgroundColor:
    //                           //         AppColor.kSoftBlueColor,
    //                           //   ),
    //                           // ),
    //                           IconButton.filledTonal(
    //                             onPressed: () {
    //                               addCustomerControllers.dataProyekTemp.remove(
    //                                   addCustomerControllers
    //                                       .dataProyekTemp[index]);
    //                               if (addCustomerControllers
    //                                   .dataProyekTemp.isEmpty) {
    //                                 // Get.back();
    //                                 // onStepTapped(0);
    //                               }
    //                             },
    //                             icon: const Icon(
    //                               Icons.delete_rounded,
    //                               size: 16,
    //                               color: AppColor.kFailedColor,
    //                             ),
    //                             style: FilledButton.styleFrom(
    //                               backgroundColor:
    //                                   AppColor.kBadgeFailedColor,
    //                             ),
    //                           ),
    //                         ],
    //                       ));
    //                 },
    //               );
    //             }),
    //           )
    //         ],
    //       ),
    //     ),
    //   ),
    // );

    // final topContent = Container(
    //   height: 190,
    //   width: double.infinity,
    //   padding: const EdgeInsets.only(right: 20, left: 20, top: 15, bottom: 50),
    //   decoration: const BoxDecoration(
    //     color: AppColor.kPrimaryColor,
    //     borderRadius: BorderRadius.only(
    //       bottomLeft: Radius.circular(20),
    //       bottomRight: Radius.circular(20),
    //     ),
    //   ),
    //   child: Row(
    //     mainAxisAlignment: MainAxisAlignment.start,
    //     crossAxisAlignment: CrossAxisAlignment.start,
    //     children: [
    //       Container(
    //         padding: const EdgeInsets.all(10),
    //         decoration: BoxDecoration(
    //           color: AppColor.kWhiteColor,
    //           borderRadius: BorderRadius.circular(10),
    //         ),
    //         child: const Icon(
    //           Icons.fire_truck_rounded,
    //         ),
    //       ),
    //       const SizedBox(width: 15),
    //       Obx(() => Column(
    //             crossAxisAlignment: CrossAxisAlignment.start,
    //             children: [
    //               BaseTextWidget.customText(
    //                 text: addCustomerControllers.dataCustomerTemp['name'] ??
    //                     "CUSTOMER PROYEK",
    //                 maxLengthText: 25,
    //                 fontSize: 14,
    //                 fontWeight: FontWeight.w600,
    //                 color: AppColor.kWhiteColor,
    //               ),
    //               BaseTextWidget.customText(
    //                 text: addCustomerControllers.dataCustomerTemp['phone'] ??
    //                     "customer phone",
    //                 maxLengthText: 25,
    //                 fontSize: 12,
    //                 fontWeight: FontWeight.w600,
    //                 color: AppColor.kWhiteColor,
    //               )
    //             ],
    //           ))
    //     ],
    //   ),
    // );

    Future<void> showBottomAddCart(BuildContext context, bool isEmptyCart) {
      return showModalBottomSheet(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(15.0))),
        backgroundColor: AppColor.kWhiteColor,
        // backgroundColor: Colors.transparent,
        isDismissible: !isEmptyCart,
        context: context,
        isScrollControlled: true,
        builder: (context) => PopScope(
          canPop: !isEmptyCart,
          child: Padding(
            padding: EdgeInsets.only(
                top: 10,
                right: 15,
                left: 15,
                bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Tambahkan Proyek",
                      style: BaseTextStyle.customTextStyle(
                        kfontSize: 14,
                        kfontWeight: FontWeight.w600,
                      ),
                    ),
                    IconButton.filledTonal(
                      onPressed: () {
                        // print(isEmptyCart);
                        Get.back();
                        // if (isEmptyCart) {
                        //   Get.back();
                        //   onStepTapped(0);
                        // } else {
                        //   Get.back();
                        // }
                      },
                      icon: const Icon(Icons.close),
                    ),
                  ],
                ),
                const Divider(),
                FormBuilder(
                  key: addCustomerControllers.fkAddProyek,
                  onChanged: () {
                    addCustomerControllers.fkAddProyek.currentState!.save();
                  },
                  autovalidateMode: AutovalidateMode.disabled,
                  skipDisabled: true,
                  child: Flexible(
                    child: ListView(
                      shrinkWrap: true,
                      physics: const AlwaysScrollableScrollPhysics(),
                      children: [
                        CustomTextField.style2(
                          title: 'Nama Proyek',
                          fieldName: 'customer_proyek',
                          hintText: "Masukan nama proyek",
                          isRequired: true,
                          onClearText: () => addCustomerControllers
                              .fkAddProyek.currentState!
                              .patchValue({'customer_proyek': null}),
                          onChange: ({value}) {},
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Nama proyek wajib diisi!';
                            }
                            return null;
                          },
                          textInputAction: TextInputAction.next,
                          inputFormater: <TextInputFormatter>[
                            // FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]')),
                            // Formatter untuk mengubah input menjadi huruf kapital
                            UppercaseInputFormatter(),
                          ],
                        ),
                        const SizedBox(height: 5),
                        CustomTextField.style2(
                          title: 'Nama PIC',
                          fieldName: 'pic_proyek',
                          hintText: "Ketik pic proyek disini..",
                          isRequired: true,
                          onClearText: () => addCustomerControllers
                              .fkAddProyek.currentState!
                              .patchValue({'pic_proyek': null}),
                          onChange: ({value}) {},
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Pic proyek wajib diisi!';
                            }
                            return null;
                          },
                          textInputAction: TextInputAction.next,
                        ),
                        const SizedBox(height: 5),
                        CustomTextField.style2(
                          title: 'No.Telp',
                          fieldName: 'pic_phone',
                          hintText: "Masukan nomor telepon",
                          isRequired: true,
                          onClearText: () => addCustomerControllers
                              .fkAddProyek.currentState!
                              .patchValue({'pic_phone': null}),
                          onChange: ({value}) {},
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'No.telp wajib diisi!';
                            }
                            return null;
                          },
                          keyboardType: TextInputType.phone,
                          textInputAction: TextInputAction.next,
                          inputFormater: <TextInputFormatter>[
                            FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                            // for version 2 and greater youcan also use this
                            FilteringTextInputFormatter.digitsOnly
                          ],
                        ),
                        const SizedBox(height: 5),
                        CustomTextField.style2(
                          title: 'Lokasi Proyek',
                          fieldName: 'proyek_address',
                          hintText: "Pilih lokasi proyek",
                          isRequired: true,
                          onClearText: () => addCustomerControllers
                              .fkAddProyek.currentState!
                              .patchValue({'proyek_address': null}),
                          readOnly: true,
                          isLongText: true,
                          controller:
                              addCustomerControllers.addressProjectControllers,
                          onTap: () async {
                            // Position? currentPosition =
                            //     await Geolocator.getLastKnownPosition();

                            // showAboutDialog(context: context);

                            Get.to(
                              () => GMapsPicker(
                                // position: currentPosition!,
                                mapsHelper: helperMapsControllers.helperMaps,
                                detailMapsControllers: addCustomerControllers
                                    .addressProjectControllers,
                                collectMapsData:
                                    helperMapsControllers.dataMapProjectHelper,
                              ),
                            );

                            // BaseExtendWidget.mapsPicker(
                            //   context: context,
                            //   mapsHelper: helperMapsControllers.helperMaps,
                            //   detailMapsControllers: addCustomerControllers
                            //       .addressProjectControllers,
                            //   collectMapsData:
                            //       helperMapsControllers.dataMapProjectHelper,
                            // );
                          },
                          onChange: ({value}) {},
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Alamat proyek wajib diisi!';
                            }
                            return null;
                          },
                          textInputAction: TextInputAction.next,
                        ),
                        const SizedBox(height: 15),
                      ],
                    ),
                  ),
                ),
                // Obx(() =>
                Button.filled(
                  onPressed: () {
                    addCustomerControllers.addCustomerProject(
                      addressMap: helperMapsControllers.dataMapProjectHelper,
                    );
                  },
                  label: "Tambah",
                  color: AppColor.kPrimaryColor,
                  icon: const Icon(
                    Icons.add_home_work_rounded,
                    color: AppColor.kWhiteColor,
                  ),
                ),

                // BaseButtonWidget.primaryIconButton(
                //   onPressed: () {
                //     addCustomerControllers.addCustomerProject(
                //       addressMap: helperMapsControllers.dataMapProjectHelper,
                //     );
                //     // loadingTest(context);
                //   },
                //   width: double.infinity,
                //   // isDisable: addCustomerControllers.isLoading.isTrue,
                //   isLoading: addCustomerControllers.isLoading.isTrue,
                //   kcolor: AppColor.kPrimaryColor,
                //   height: 60,
                //   iconData: Icons.queue_play_next,
                //   textStyle: BaseTextStyle.customTextStyle(
                //     kfontWeight: FontWeight.w600,
                //     kfontSize: 12,
                //     kcolor: AppColor.kWhiteColor,
                //   ),
                //   title: "Tambah",
                // ),
                // ),
                const SizedBox(height: 15),
              ],
            ),
          ),
        ),
      );
    }

    // final detailPayment = Container(
    //   padding: const EdgeInsets.all(15.0),
    //   // margin: const EdgeInsets.only(right: 10, left: 10, top: 80),
    //   decoration: BoxDecoration(
    //     color: AppColor.kWhiteColor,
    //     borderRadius: BorderRadius.circular(10),
    //     border: Border.all(color: AppColor.kGreyColor),
    //   ),
    //   child: Column(
    //     crossAxisAlignment: CrossAxisAlignment.end,
    //     children: [
    //       cartCollapseContent,
    //       const SizedBox(height: 20),
    //       IconButton.filledTonal(
    //         iconSize: 20,
    //         onPressed: () {
    //           showBottomAddCart(context, false);
    //         },
    //         icon: const Icon(
    //           Icons.add,
    //         ),
    //       )
    //     ],
    //   ),
    // );

    final listCartProject = Obx(
      () {
        var projectTemp = addCustomerControllers.dataProyekTemp;
        return SingleChildScrollView(
          // physics: const AlwaysScrollableScrollPhysics(),
          child: Column(
            // mainAxisSize: MainAxisSize.min,
            children: [
              ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: projectTemp.length,
                itemBuilder: (context, index) {
                  return BaseCardWidget.cardStyle1(
                      title: projectTemp[index]['name'],
                      subTitle1: projectTemp[index]['address'],
                      trailing: IconButton.filledTonal(
                        splashColor: AppColor.kErrorColor,
                        onPressed: () {
                          addCustomerControllers.dataProyekTemp.remove(
                              addCustomerControllers.dataProyekTemp[index]);
                          if (addCustomerControllers.dataProyekTemp.isEmpty) {
                            // Get.back();
                            // onStepTapped(0);
                          }
                        },
                        icon: const Icon(
                          Icons.delete,
                          color: AppColor.kErrorColor,
                        ),
                        style: FilledButton.styleFrom(
                          backgroundColor: AppColor.kBadgeFailedColor,
                        ),
                      ));
                },
              ),
            ],
          ),
        );
      },
    );

    Widget baseContent(
        {required mainContent, required int tab, bool isFinish = false}) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          // Text("HALLO DATA"),
          // ElevatedButton(
          //     onPressed: () {
          //       continueStep();
          //     },
          // child: Text("NEXT"))
          mainContent
        ],
      );
    }

    final submitAction = Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Obx(() => Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   crossAxisAlignment: CrossAxisAlignment.center,
                //   children: [
                //     Row(
                //       mainAxisAlignment: MainAxisAlignment.start,
                //       crossAxisAlignment: CrossAxisAlignment.start,
                //       children: [
                //         Column(
                //           crossAxisAlignment: CrossAxisAlignment.start,
                //           children: [
                //             BaseTextWidget.customText(
                //               text: 'TOTAL',
                //               fontWeight: FontWeight.bold,
                //             ),
                //             BaseTextWidget.customText(
                //               text:
                //                   "${addCustomerControllers.dataProyekTemp.length} Proyek",
                //             )
                //           ],
                //         )
                //       ],
                //     ),
                //     IconButton.filledTonal(
                //       iconSize: 20,
                //       onPressed: () {
                //         showBottomAddCart(context, false);
                //       },
                //       icon: const Icon(
                //         Icons.add,
                //       ),
                //     )
                //   ],
                // ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: addCustomerControllers.currentStep.value == 1
                          ? Button.outlined(
                              onPressed: () {
                                cancelStep();
                              },
                              icon:
                                  const Icon(Icons.arrow_circle_left_outlined),
                              fontSize: 15,
                              label: 'Kembali',
                            )

                          // TextButton.icon(
                          //     onPressed: () {
                          //       cancelStep();
                          //     },
                          //     icon: const Icon(Icons.arrow_circle_left_outlined),
                          //     label: BaseTextWidget.customText(text: 'Kembali'),
                          //   )
                          : const SizedBox(),
                    ),
                    const SizedBox(width: 5),
                    Expanded(
                        child: Directionality(
                      textDirection: TextDirection.rtl,
                      child: Button.filled(
                        onPressed: () {
                          if (addCustomerControllers.currentStep.value == 0) {
                            addCustomerControllers.addCustomer(
                              nextStep: continueStep,
                              addressMap:
                                  helperMapsControllers.dataMapProjectHelper,
                            );
                            if (addCustomerControllers.dataProyekTemp.isEmpty &&
                                addCustomerControllers.currentStep.value == 1) {
                              showBottomAddCart(context, true);
                            }
                            helperMapsControllers.dataMapProjectHelper.clear();
                          } else if (addCustomerControllers.currentStep.value ==
                              1) {
                            addCustomerControllers.addDataCustomer(
                                loadingCtx: context);
                          }
                        },
                        icon: Icon(
                          addCustomerControllers.currentStep.value == 1
                              ? Icons.add_circle
                              : Icons.arrow_circle_right,
                          color: AppColor.kWhiteColor,
                        ),
                        color: AppColor.kPrimaryColor,
                        disabled: addCustomerControllers.currentStep.value == 1
                            ? addCustomerControllers.dataProyekTemp.isEmpty
                            : false,
                        fontSize: 15,
                        label: addCustomerControllers.currentStep.value == 1
                            ? "Simpan"
                            : "Selanjutnya",
                      ),
                    )

                        // Directionality(
                        //   textDirection: TextDirection.rtl,
                        //   child: TextButton.icon(
                        //     onPressed: () {
                        //       if (addCustomerControllers.currentStep.value == 0) {
                        //         addCustomerControllers.addCustomer(
                        //           nextStep: continueStep,
                        //           addressMap:
                        //               helperMapsControllers.dataMapProjectHelper,
                        //         );
                        //         if (addCustomerControllers.dataProyekTemp.isEmpty &&
                        //             addCustomerControllers.currentStep.value == 1) {
                        //           showBottomAddCart(context, true);
                        //         }
                        //         helperMapsControllers.dataMapProjectHelper.clear();
                        //       } else if (addCustomerControllers.currentStep.value ==
                        //           1) {
                        //         addCustomerControllers.addDataCustomer(
                        //             loadingCtx: context);
                        //       }
                        //     },
                        //     icon: Icon(addCustomerControllers.currentStep.value == 1
                        //         ? Icons.add_circle
                        //         : Icons.arrow_circle_right),
                        //     label: BaseTextWidget.customText(
                        //       text: addCustomerControllers.currentStep.value == 1
                        //           ? "Simpan"
                        //           : "Selanjutnya",
                        //     ),
                        //   ),
                        // ),

                        // BaseButtonWidget.primaryIconButton(
                        //   onPressed: () {
                        //     if (addCustomerControllers.currentStep.value == 0) {
                        //       addCustomerControllers.addCustomer(
                        //           nextStep: continueStep);
                        //       addCustomerControllers.dataMapCustomerHelper.clear();
                        //     } else if (addCustomerControllers.currentStep.value ==
                        //         1) {
                        //       addCustomerControllers.addDataCustomer(
                        //           loadingCtx: context);
                        //     }
                        //   },
                        //   isDisable: false,
                        //   kcolor: AppColor.kPrimaryColor,
                        //   height: 60,
                        //   iconData: addCustomerControllers.currentStep.value == 1
                        //       ? Icons.add_circle
                        //       : Icons.arrow_circle_right_rounded,
                        //   // rightIcon: true,
                        //   textStyle: BaseTextStyle.customTextStyle(
                        //     kfontWeight: FontWeight.w600,
                        //     kfontSize: 12,
                        //     kcolor: AppColor.kWhiteColor,
                        //   ),
                        //   title: addCustomerControllers.currentStep.value == 1
                        //       ? "Simpan"
                        //       : "Selanjutnya",
                        // ),
                        ),
                  ],
                ),
              ],
            )),
      ),
    );

    // if (addCustomerControllers.currentStep.value > 0) {
    //   WidgetsBinding.instance.addPostFrameCallback((_) {
    //     showBottomAddCart(context, true);
    //   });
    // }

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Tambah Pelanggan',
          centerTitle: true,
          context: context,
          extendBack: () {
            // salesController.dataLookupCustomerProject.clear();
            // salesController.dataDetailCustomer.clear();
          },
        ),
        body: Obx(() => Stepper(
              elevation: 0, //Horizontal Impact
              margin: const EdgeInsets.all(0), //vertical impact
              controlsBuilder: controlBuilders,
              type: StepperType.horizontal,

              physics: const ScrollPhysics(),
              // physics: addCustomerControllers.currentStep.value == 1
              //     ? null
              //     : const ScrollPhysics(),

              onStepTapped: onStepTapped,
              onStepContinue: continueStep,
              onStepCancel: cancelStep,
              currentStep: addCustomerControllers.currentStep.value, //0, 1, 2
              steps: [
                Step(
                  title: BaseTextWidget.customText(
                    text: addCustomerControllers.currentStep.value == 0
                        ? 'Detail Pelanggan'
                        : '',
                  ),
                  content:
                      baseContent(mainContent: formDetailsCustomer, tab: 1),
                  isActive: addCustomerControllers.currentStep >= 0,
                  state: addCustomerControllers.currentStep > 0
                      ? StepState.complete
                      : StepState.editing,
                ),
                Step(
                  title: BaseTextWidget.customText(
                    text: addCustomerControllers.currentStep.value == 1
                        ? 'Tambah Proyek'
                        : '',
                  ),
                  content: baseContent(
                      mainContent: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      color: AppColor.kPrimaryColor,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: const Icon(
                                      Icons.home_work,
                                      color: AppColor.kWhiteColor,
                                    ),
                                  ),
                                  const SizedBox(width: 15),
                                  Obx(() => Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          BaseTextWidget.customText(
                                            text: addCustomerControllers
                                                    .dataCustomerTemp['name'] ??
                                                "CUSTOMER PROYEK",
                                            maxLengthText: 25,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w600,
                                            color: AppColor.kPrimaryColor,
                                          ),
                                          Row(
                                            children: [
                                              BaseTextWidget.customText(
                                                text: 'TOTAL : ',
                                                fontWeight: FontWeight.bold,
                                              ),
                                              BaseTextWidget.customText(
                                                text:
                                                    "${addCustomerControllers.dataProyekTemp.length} Proyek",
                                              )
                                            ],
                                          ),
                                        ],
                                      )),
                                ],
                              ),
                              IconButton.filledTonal(
                                iconSize: 20,
                                onPressed: () {
                                  showBottomAddCart(context, false);
                                },
                                icon: const Icon(
                                  Icons.add,
                                ),
                              )
                            ],
                          ),
                          const SizedBox(height: 10),
                          const Divider(),
                          // detailPayment,
                          listCartProject,
                        ],
                      ),
                      tab: 2),
                  isActive: addCustomerControllers.currentStep >= 0,
                  state: addCustomerControllers.currentStep >= 1
                      ? StepState.editing
                      : StepState.indexed,
                ),
              ],
            )),
        bottomNavigationBar: submitAction,
      ),
    );
  }
}
