import 'package:rbs_mobile_operation/data/datasources/customer.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/view/app_view_resource.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/customer/create/stepperpage.dart';

class CustomerDataList extends StatelessWidget {
  const CustomerDataList({super.key});

  @override
  Widget build(BuildContext context) {
    CustomerListControllers customerControllers =
        Get.put(CustomerListControllers());

    Future onRefresh() async {
      customerControllers.refreshDataCustomer();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        if (customerControllers.customerData.isNotEmpty) {
          customerControllers.fetchCustomer();
        }
      }
    });

    // ** SHOW FILTER MODAL SHEET
    final isActive = ['Semua', 'Aktif', 'Non Aktif'];

    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          customerControllers.currentFilter.clear();
          customerControllers.filterFormKey.currentState!.reset();
          customerControllers.filterFormKey.currentState!
              .patchValue({'status': null});
        },
        onFilter: () {
          Get.back();
          customerControllers.refreshDataCustomer();
        },
        valueFilter: customerControllers.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: customerControllers.filterFormKey,
            onChanged: () {
              customerControllers.filterFormKey.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  FormBuilderChoiceChip<String>(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      labelText: 'Status',
                      hintText: "Pilih Status",
                      contentPadding: const EdgeInsets.symmetric(vertical: 2),
                      labelStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 15,
                        kfontWeight: FontWeight.w500,
                        kcolor: AppColor.kPrimaryColor,
                      ),
                      errorStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 13,
                        kcolor: AppColor.kFailedColor,
                        kfontWeight: FontWeight.w500,
                      ),
                    ),
                    name: 'status',
                    initialValue: customerControllers.currentFilter['status'],
                    selectedColor: AppColor.kInactiveColor,
                    spacing: 6.0,
                    options: List.generate(isActive.length, (index) {
                      return FormBuilderChipOption(
                        value: isActive[index],
                        child: Text(
                          isActive[index],
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontWeight: FontWeight.w600,
                            kfontSize: 12,
                          ),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    // ** LIST CONTENT
    final listItemContent = Obx(() {
      final result = customerControllers.customerData;

      return Container(
        margin: const EdgeInsets.only(top: 10),
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: result.isEmpty && customerControllers.isLoading.isFalse
            ? SizedBox(
                height: MediaQuery.of(context).size.height / 1.2,
                child: Center(
                  child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                ),
              )
            : ListView.separated(
                controller: scrollController,
                physics: const AlwaysScrollableScrollPhysics(),
                itemCount:
                    // customerControllers.hasMore.value
                    //     ?
                    result.length + 1,
                // : result.length,
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(height: 2),
                itemBuilder: (BuildContext context, int index) {
                  if (index < result.length) {
                    final data = result[index];
                    var colorBadge =
                        BaseGlobalFuntion.statusColor(result[index]['active']);
                    return BaseCardWidget.cardStyle1(
                        ontap: () {
                          Get.to(
                            () => CustomerView(
                              itemId: data['id'],
                              // customerControllers: customerControllers,
                            ),
                            // preventDuplicates: false
                            //     binding: BindingsBuilder(() {
                            //   customerControllers;
                            // })
                            // transition: Transition.fadeIn,
                            // duration: const Duration(milliseconds: 500),
                          );
                        },
                        leading: CircleAvatar(
                          child: BaseTextWidget.customText(
                            text: data['name'].length < 2
                                ? data['name'].substring(0, 1).toUpperCase()
                                : data['name'].substring(0, 2).toUpperCase(),
                            fontSize: 14,
                            color: AppColor.kPrimaryColor,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        title: data['name'],
                        subTitle1: data['phone'],
                        subTitle2: BaseTextWidget.customText(
                          text: data['production_unit_name'],
                          isLongText: true,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                        trailing: Text.rich(
                          textAlign: TextAlign.right,
                          TextSpan(children: [
                            BaseTextWidget.customTextSpan(
                              text: "${data['total_site']}",
                              extendText: '\n',
                              color: AppColor.kPrimaryColor,
                              letterSpacing: 0.8,
                              fontSize: 13,
                              fontWeight: FontWeight.w600,
                            ),
                            BaseTextWidget.customTextSpan(
                              text: "Proyek",
                              color: AppColor.kPrimaryColor,
                              fontSize: 12,
                              fontWeight: FontWeight.w500,
                            ),
                          ]),
                        ),
                        statusHeader: {
                          'title':
                              data['active'] == "1" ? 'Aktif' : 'Non Aktif',
                          'color': colorBadge['background_color'],
                          'titleColor': colorBadge['text_color'],
                        });
                  } else if (result.length < 25) {
                    return customerControllers.hasMore.value &&
                            customerControllers.isLoading.isTrue
                        ? BaseLoadingWidget.cardShimmer(
                            height: MediaQuery.of(context).size.height,
                            shimmerheight: 80,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 25,
                          )
                        : const SizedBox();
                  } else {
                    return customerControllers.hasMore.value
                        ? BaseLoadingWidget.cardShimmer(
                            height: 200,
                            shimmerheight: 90,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 2,
                          )
                        : const SizedBox();
                  }
                },
              ),
      );
    });

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
            title: 'Data Pelanggan',
            context: context,
            isAllFill: true,
            searchController: customerControllers.search,
            searchText: 'Cari',
            onFillterChange: () {
              showBottomFilter(context);
            },
            onSearchChange: () {
              BaseGlobalFuntion.debounceRun(
                action: () {
                  customerControllers.refreshDataCustomer();
                },
                duration: const Duration(milliseconds: 1000),
              );
            }),
        body: RefreshIndicator(
          onRefresh: onRefresh,
          child: SizedBox(
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: [
                listItemContent,
                Positioned(
                  bottom: 30.0,
                  right: 15.0,
                  child: SizedBox(
                    height: 55.0,
                    width: 55.0,
                    child: IconButton.filled(
                      iconSize: 30,
                      onPressed: () {
                        // Get.to(() => const AddCustomer());
                        Get.to(() => const StepperAddCustomer());
                      },
                      icon: const Icon(
                        Icons.add,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
