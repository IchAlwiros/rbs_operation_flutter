import 'package:rbs_mobile_operation/data/datasources/customer.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/view/app_view_resource.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/customer/create/add_customer_project.dart';
import 'package:rbs_mobile_operation/view/customer/update/update_customer.dart';

class CustomerView extends StatelessWidget {
  final int itemId;
  // final CustomerControllers customerControllers;

  const CustomerView({
    required this.itemId,
    // required this.customerControllers,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    DetailsCustomerControllers customerControllers =
        Get.put(DetailsCustomerControllers());
    customerControllers.viewCustomer(customerId: itemId);
    customerControllers.viewCustomerProject(customerId: itemId);

    // print(itemId);

    Future onRefresh() async {
      customerControllers.customerProject.clear();
      customerControllers.viewCustomerProject(customerId: itemId);
    }

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Detail Pelanggan',
          centerTitle: true,
          context: context,
        ),
        body: Column(
          children: [
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 15),
              decoration: const BoxDecoration(
                color: AppColor.kPrimaryColor,
                border: Border(
                  top: BorderSide(
                    color: AppColor.kPrimaryColor,
                  ),
                ),
              ),
              child: Obx(() {
                if (customerControllers.isLoading.isFalse) {
                  var data = customerControllers.viewCustomerData;
                  var colorBadge =
                      BaseGlobalFuntion.statusColor(data['active']);

                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Row(
                          children: [
                            Container(
                              padding: const EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                color: AppColor.kWhiteColor,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: const Icon(
                                Icons.info,
                              ),
                            ),
                            const SizedBox(width: 10),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  BaseTextWidget.customText(
                                    text: data['name'],
                                    color: AppColor.kWhiteColor,
                                    fontSize: 13,
                                    fontWeight: FontWeight.w700,
                                    isLongText: true,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  BaseTextWidget.customText(
                                    text: data['customer_category_name'],
                                    color: AppColor.kWhiteColor,
                                    fontSize: 13,
                                    fontWeight: FontWeight.w500,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    isLongText: true,
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(width: 5)
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 2, horizontal: 6),
                        decoration: BoxDecoration(
                          color: colorBadge['background_color'],
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          "${BaseGlobalFuntion.statusCheck(data['active'])}",
                          style: BaseTextStyle.customTextStyle(
                            kfontSize: 11,
                            kcolor: colorBadge['text_color'],
                          ),
                        ),
                      ),
                    ],
                  );
                } else {
                  return Shimmer.fromColors(
                      baseColor: AppColor.kSoftGreyColor,
                      highlightColor: AppColor.kWhiteColor,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: 40,
                            height: 40,
                            decoration: BoxDecoration(
                              color: Colors.amber,
                              borderRadius: BorderRadius.circular(8),
                            ),
                          ),
                          const SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 150,
                                height: 15,
                                decoration: BoxDecoration(
                                  color: Colors.amber,
                                  borderRadius: BorderRadius.circular(4),
                                ),
                              ),
                              const SizedBox(height: 10),
                              Container(
                                width: 200,
                                height: 15,
                                decoration: BoxDecoration(
                                  color: Colors.amber,
                                  borderRadius: BorderRadius.circular(4),
                                ),
                              )
                            ],
                          )
                        ],
                      ));
                }
              }),
            ),
            Expanded(
              child: DefaultTabController(
                  length: 2,
                  child: Scaffold(
                    appBar: PreferredSize(
                      preferredSize: const Size.fromHeight(kToolbarHeight),
                      child: Container(
                        color: AppColor.kPrimaryColor,
                        child: SafeArea(
                          child: Column(
                            children: <Widget>[
                              Expanded(child: Container()),
                              TabBar(
                                indicatorColor: AppColor.kInactiveColor,
                                unselectedLabelStyle:
                                    BaseTextStyle.customTextStyle(
                                  kfontWeight: FontWeight.w400,
                                  kfontSize: 12,
                                ),
                                unselectedLabelColor: AppColor.kWhiteColor,
                                labelColor: AppColor.kInactiveColor,
                                labelStyle: BaseTextStyle.customTextStyle(
                                  kfontWeight: FontWeight.w700,
                                  kfontSize: 14,
                                ),
                                tabs: const [
                                  Tab(text: 'Info'),
                                  Tab(text: 'Proyek'),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    body: TabBarView(
                      children: [
                        Obx(() => segmentTab1(
                              data: customerControllers.viewCustomerData,
                              isLoading: customerControllers.isLoading.value,
                            )),
                        Obx(() => RefreshIndicator(
                              onRefresh: onRefresh,
                              child: segmentTab2(
                                dataProyek: customerControllers.customerProject,
                                dataCustomer:
                                    customerControllers.viewCustomerData,
                              ),
                            ))
                      ],
                    ),
                  )),
            )
          ],
        ),
      ),
    );
  }

  Widget segmentTab1({
    dynamic data,
    bool isLoading = true,
  }) {
    return data.isNotEmpty
        ? Stack(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: Column(
                  children: [
                    BaseCardWidget.detailInfoCard2(
                      isLoading: isLoading,
                      contentList: [
                        {
                          'title': 'Unit Produksi',
                          'content': BaseTextWidget.customText(
                            text: data['production_unit_name'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.factory_rounded,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'No.Telp',
                          'content': BaseTextWidget.customText(
                            text: data['phone'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 30,
                            isLongText: true,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.phone_android_rounded,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'Email',
                          'content': BaseTextWidget.customText(
                            text: data['email'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 30,
                            isLongText: true,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.email,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'Alamat',
                          'content': BaseTextWidget.customText(
                            text: data['address'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 30,
                            isLongText: true,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.location_on,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'Akses Pembayaran',
                          'content': Row(
                            children: [
                              ...data['access_payment'].map((method) {
                                return Container(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 5,
                                      vertical: 2,
                                    ),
                                    margin:
                                        data['access_payment'].last == method
                                            ? null
                                            : const EdgeInsets.only(right: 5),
                                    decoration: BoxDecoration(
                                      color: method['allowed'] == true
                                          ? AppColor.kBadgeSuccessColor
                                          : AppColor.kGreyColor,
                                      borderRadius: BorderRadius.circular(4),
                                    ),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                          method['allowed'] == true
                                              ? Icons.radio_button_on
                                              : Icons.radio_button_off,
                                          color: method['allowed'] == true
                                              ? AppColor.kGreenColor
                                              : AppColor.kWhiteColor,
                                          size: 13,
                                        ),
                                        const SizedBox(width: 2),
                                        BaseTextWidget.customText(
                                          text: method['name'],
                                          color: method['allowed'] == true
                                              ? AppColor.kGreenColor
                                              : AppColor.kWhiteColor,
                                        ),
                                      ],
                                    ));
                              })
                            ],
                          ),
                          'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.payment,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ),
                          )
                        },
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                bottom: 30.0,
                right: 15.0,
                child: SizedBox(
                  height: 45.0,
                  child: Button.filled(
                    onPressed: () {
                      Get.to(() => UpdateCustomer(detailData: data));
                    },
                    // isDisable: addProjectControllers.isLoading.value,
                    color: AppColor.kPrimaryColor,
                    width: 200,
                    // rounded: 10,
                    icon: const Icon(
                      Icons.edit_note_rounded,
                      color: AppColor.kWhiteColor,
                    ),
                    // textStyle: BaseTextStyle.customTextStyle(
                    //   kfontWeight: FontWeight.w600,
                    //   kfontSize: 12,
                    //   kcolor: AppColor.kWhiteColor,
                    // ),
                    fontSize: 15,
                    label: "Edit Customer",
                  ),
                ),
              )
            ],
          )
        : BaseCardWidget.detailInfoCard(
            isLoading: true,
            title: "Informasi Rencana Produksi",
            contentList: [],
          );
  }

  Widget segmentTab2({dynamic dataProyek, dynamic dataCustomer}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
      child: SizedBox(
        height: double.infinity,
        child: Stack(
          children: [
            dataProyek.isNotEmpty
                ? ListView.separated(
                    shrinkWrap: true,
                    itemCount: dataProyek.length,
                    separatorBuilder: (BuildContext context, int index) =>
                        const SizedBox(height: 5),
                    itemBuilder: (BuildContext context, int index) {
                      var colorBadge = BaseGlobalFuntion.statusColor(
                          dataProyek[index]['active']);

                      return BaseCardWidget.cardStyle4(
                        ontap: () {
                          Get.to(
                            () => ProjectView(itemId: dataProyek[index]['id']),
                          );
                        },
                        title: dataProyek[index]['name'],
                        subtitle: dataProyek[index]['code'],
                        bottomSubtitle: BaseTextWidget.customText(
                          text: dataProyek[index]['address'],
                          isLongText: true,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                        extendWidget: Container(
                          padding: const EdgeInsets.all(4),
                          decoration: BoxDecoration(
                            color: colorBadge['background_color'],
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Text(
                            BaseGlobalFuntion.statusCheck(
                                    dataProyek[index]['active']) ??
                                "",
                            style: BaseTextStyle.customTextStyle(
                              kfontSize: 12,
                              kcolor: colorBadge['text_color'],
                              kfontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      );
                    },
                  )
                : Center(
                    child: BaseLoadingWidget.noMoreDataWidget(),
                  ),
            Positioned(
              bottom: 30.0,
              right: 15.0,
              child: SizedBox(
                height: 45.0,
                child: Button.filled(
                  onPressed: () {
                    Get.to(
                      () => AddProjectCustomer(
                        detailCustomer: dataCustomer,
                      ),
                    );
                  },
                  // isDisable: addProjectControllers.isLoading.value,
                  color: AppColor.kPrimaryColor,
                  width: 140,
                  fontSize: 14,
                  // rounded: 10,
                  icon: const Icon(
                    Icons.add_circle,
                    color: AppColor.kWhiteColor,
                  ),
                  // textStyle: BaseTextStyle.customTextStyle(
                  //   kfontWeight: FontWeight.w600,
                  //   kfontSize: 12,
                  //   kcolor: AppColor.kWhiteColor,
                  // ),
                  label: "Proyek",
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
