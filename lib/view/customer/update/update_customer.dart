import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rbs_mobile_operation/core/components/regional_maps_picker.dart';
import 'package:rbs_mobile_operation/data/datasources/customer.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/data/datasources/global/helper_maps.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';

class UpdateCustomer extends StatelessWidget {
  final dynamic detailData;

  const UpdateCustomer({this.detailData, super.key});

  @override
  Widget build(BuildContext context) {
    UpdateCustomerControllers updateCustomerControllers =
        Get.put(UpdateCustomerControllers());

    HelperMapsControllers helperMapsControllers =
        Get.put(HelperMapsControllers());

    if (detailData['address'] != null) {
      helperMapsControllers.detailAddressControllers.text =
          detailData['address'];
    }

    if (ApiService.productionUnitGlobal == null ||
        LocalDataSource.getLocalVariable(key: 'lastestProduction') != null) {
      updateCustomerControllers.currentFilter(
          {'production_unit_id': detailData['productsimion_unit_id']});
      updateCustomerControllers.lookupCustomerCategory();
    }

    final submitAction = Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Obx(() => Expanded(
                  child: Button.filled(
                    onPressed: () => updateCustomerControllers.updateCustomer(
                      extendData: detailData,
                      loadingCtx: context,
                      addressMaps: helperMapsControllers.dataMapProjectHelper,
                    ),
                    disabled: updateCustomerControllers.isLoading.value,
                    // isLoading: updateCustomerControllers.isLoading.value,
                    // color: AppColor.kPrimaryColor,
                    label: "Simpan",
                    icon: const Icon(
                      Icons.save_as_rounded,
                      color: AppColor.kWhiteColor,
                    ),

                    // iconData: Icons.save,
                    // textStyle: BaseTextStyle.customTextStyle(
                    //   kfontWeight: FontWeight.w600,
                    //   kfontSize: 12,
                    //   kcolor: AppColor.kWhiteColor,
                    // ),
                    // title: "Simpan Perubahan",
                  ),
                )),
          ],
        ),
      ),
    );

    final topContent = Container(
      height: 190,
      width: double.infinity,
      padding: const EdgeInsets.only(right: 20, left: 20, bottom: 20),
      decoration: const BoxDecoration(
        color: AppColor.kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
      ),
    );

    final paymentMethod = ['CASH', 'CREDIT', 'TOP'];

    final detailPayment = Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      margin: const EdgeInsets.only(right: 10, left: 10, top: 10, bottom: 10),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(color: AppColor.kGreyColor),
      ),
      child: Column(
        children: [
          FormBuilder(
            key: updateCustomerControllers.fkAddCustomer,
            onChanged: () {
              updateCustomerControllers.fkAddCustomer.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Obx(() => updateCustomerControllers.dataLookupPU.isEmpty
                      ? BaseTextFieldWidget.disableTextForm2(
                          disabledText: 'Pilih unit produksi',
                          label: "Unit Produksi",
                          isRequired: true,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib dipilih!';
                            }
                            return null;
                          },
                        )
                      : CustomDropDownSearch(
                          fieldName: 'production_unit_id',
                          isRequired: true,
                          enabled: false,
                          controller: updateCustomerControllers
                              .productionUnitController,
                          initialValue: {
                            'id': detailData['production_unit_id'],
                            'name': detailData['production_unit_name']
                          },
                          label: "Unit Produksi",
                          hintText: "Pilih Unit Produksi",
                          asyncItems: (String filter) async {
                            updateCustomerControllers.lookupProductionUnit(
                                searching: filter);
                            return updateCustomerControllers.dataLookupPU;
                          },
                          onChange: ({searchValue, valueItem, data}) {
                            updateCustomerControllers.currentFilter(
                                {"production_unit_id": valueItem});

                            updateCustomerControllers.lookupProductionUnit();
                            updateCustomerControllers.dataLookupPU.clear();
                          },
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Unit produksi wajib dipilih!';
                            }
                            return null;
                          },
                        )),
                  const SizedBox(height: 5),
                  Obx(
                    () => updateCustomerControllers
                            .dataLookupCustomerCategory.isEmpty
                        ? BaseTextFieldWidget.disableTextForm2(
                            disabledText: 'Pilih tipe pelanggan',
                            label: "Tipe Pelanggan",
                            isRequired: true,
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Tipe pelanggan wajib dipilih!';
                              }
                              return null;
                            },
                          )
                        : CustomDropDownSearch(
                            fieldName: 'customer_category_id',
                            isRequired: true,
                            controller: updateCustomerControllers
                                .customerTypeController,
                            initialValue: {
                              'id': detailData['customer_category_id'],
                              'name': detailData['customer_category_name']
                            },
                            label: "Tipe Pelanggan",
                            hintText: "Pilih Tipe Pelanggan",
                            asyncItems: (String filter) async {
                              updateCustomerControllers.lookupCustomerCategory(
                                  searching: filter);
                              return updateCustomerControllers
                                  .dataLookupCustomerCategory;
                            },
                            onChange: ({searchValue, valueItem, data}) {},
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Tipe pelanggan wajib dipilih!';
                              }
                              return null;
                            },
                          ),
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Nama Pelanggan',
                    hintText: "Ketik Nama disini..",
                    fieldName: 'customer_name',
                    initialValue: detailData['name'],
                    isRequired: true,
                    onClearText: () => updateCustomerControllers
                        .fkAddCustomer.currentState!
                        .patchValue({'customer_name': null}),
                    // controller: TextEditingController(),
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Nama wajib diisi!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                    inputFormater: <TextInputFormatter>[
                      // FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]')),
                      // Formatter untuk mengubah input menjadi huruf kapital
                      UppercaseInputFormatter(),
                    ],
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Email',
                    fieldName: 'email',
                    initialValue: detailData['email'],
                    hintText: "Ketik Email disini..",
                    onClearText: () => updateCustomerControllers
                        .fkAddCustomer.currentState!
                        .patchValue({'email': null}),
                    // controller: TextEditingController(),
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value!.isNotEmpty) {
                        if (!value.contains('@')) {
                          return 'Alamat email tidak sesuai!';
                        }
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.next,
                  ),
                  const SizedBox(height: 10),
                  CustomTextField.style2(
                      title: 'No.Telp',
                      fieldName: 'phone',
                      hintText: "Ketik No.Telp disini..",
                      initialValue: detailData['phone'],
                      isRequired: true,
                      onClearText: () => updateCustomerControllers
                          .fkAddCustomer.currentState!
                          .patchValue({'phone': null}),
                      onChange: ({value}) {},
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'No telepon wajib diisi!';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.phone,
                      textInputAction: TextInputAction.next,
                      inputFormater: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                        // for version 2 and greater youcan also use this
                        FilteringTextInputFormatter.digitsOnly
                      ]),
                  const SizedBox(height: 10),
                  FormBuilderFilterChip<String>(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      label: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            "Metode Pembayaran",
                            style: BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kPrimaryColor,
                              kfontSize: 16,
                              kfontWeight: FontWeight.w600,
                            ),
                          ),
                          Text(
                            "*",
                            style: BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kErrorColor,
                              kfontSize: 15,
                              kfontWeight: FontWeight.w600,
                            ),
                          )
                        ],
                      ),
                      errorStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 12,
                        kcolor: AppColor.kFailedColor,
                        kfontWeight: FontWeight.w500,
                      ),
                    ),
                    name: 'payment_type_code',
                    selectedColor: AppColor.kInactiveColor,
                    spacing: 6.0,
                    initialValue: List.generate(
                        detailData['access_payment'].length, (index) {
                      return detailData['access_payment'][index]['allowed'] ==
                              true
                          ? detailData['access_payment'][index]['code']
                          : '';
                    }),
                    options: List.generate(paymentMethod.length, (index) {
                      // print(detailData['access_payment']);
                      return FormBuilderChipOption(
                        value: paymentMethod[index],
                        child: Text(
                          paymentMethod[index],
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontWeight: FontWeight.w600,
                            kfontSize: 12,
                          ),
                        ),
                      );
                    }),
                    // onChanged: _onChanged,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Metode pembayaran wajib dipilih!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 10),
                  CustomTextField.style2(
                    title: 'Alamat Lengkap',
                    hintText: "Masukan Alamat",
                    fieldName: 'address',
                    isRequired: true,
                    onClearText: () => updateCustomerControllers
                        .fkAddCustomer.currentState!
                        .patchValue({'address': null}),
                    readOnly: true,
                    isLongText: true,
                    controller: helperMapsControllers.detailAddressControllers,
                    onTap: () async {
                      // showBottomGoogleMap(context);
                      // BaseExtendWidget.mapsPicker(
                      //   context: context,
                      //   mapsHelper: helperMapsControllers.helperMaps,
                      //   detailMapsControllers:
                      //       helperMapsControllers.detailAddressControllers,
                      //   collectMapsData:
                      //       helperMapsControllers.dataMapProjectHelper,
                      // );

                      // Position? currentPosition =
                      //     await Geolocator.getLastKnownPosition();

                      // showAboutDialog(context: context);

                      Get.to(
                        () => GMapsPicker(
                          // position: currentPosition!,
                          mapsHelper: helperMapsControllers.helperMaps,
                          detailMapsControllers:
                              helperMapsControllers.detailAddressControllers,
                          collectMapsData:
                              helperMapsControllers.dataMapProjectHelper,
                        ),
                      );
                    },
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Alamat pelanggan wajib diisi!';
                      }
                      return null;
                    },
                    textInputAction: TextInputAction.done,
                  ),
                  const SizedBox(height: 10),
                ],
              ),
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        title: 'Ubah Data Pelanggan',
        centerTitle: true,
        context: context,
        extendBack: () {
          // salesController.dataLookupCustomerProject.clear();
          // salesController.dataDetailCustomer.clear();
        },
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            topContent,
            const SizedBox(height: 10),
            detailPayment,
          ],
        ),
      ),
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: submitAction,
    );
  }
}
