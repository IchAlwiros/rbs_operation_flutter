import 'package:rbs_mobile_operation/core/extentions/date_time_extension.dart';
import 'package:rbs_mobile_operation/data/datasources/productionschedule.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/app_view_resource.dart';
import 'package:rbs_mobile_operation/view/production_scedule/create/new_ps.dart';

class ProductionScheduleList extends StatelessWidget {
  const ProductionScheduleList({super.key});

  @override
  Widget build(BuildContext context) {
    ProductionScheduleControllers productionControllers =
        Get.put(ProductionScheduleControllers());

    Future onRefresh() async {
      productionControllers.refreshDataProductionSchedule();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        if (productionControllers.productionScheduleData.isNotEmpty) {
          productionControllers.fetchProductionSchedule();
        }
      }
    });

    // ** SHOW FILTER MODAL BOTTOM SHEET
    final isActive = ['Semua', 'Sukses', 'Menunggu', 'Diproses', "Dibatalkan"];
    TextEditingController customerFilterController = TextEditingController();
    TextEditingController productFilterController = TextEditingController();
    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          productionControllers.byDate.clear();
          productionControllers.currentFilter.clear();
          productionControllers.filterFormKey.currentState!.reset();
          productionControllers.filterFormKey.currentState!.patchValue({
            'customer': null,
            'date_range': null,
            'status_code': null,
            'product': null,
          });
        },
        onFilter: () {
          // print(customerFilterController.text);
          // print(productFilterController.text);
          Get.back();
          // print("DISINI OKE");
          // print(productionControllers.filterFormKey.currentState!.value);
          // print(productionControllers.currentFilter);
          productionControllers.refreshDataProductionSchedule();
        },
        valueFilter: productionControllers.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: productionControllers.filterFormKey,
            onChanged: () {
              productionControllers.filterFormKey.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'customer',
                    controller: customerFilterController,
                    // currentValue:
                    //     // productionControllers.currentFilter['customer_preview'],
                    label: "Pelanggan",
                    hintText: "Pilih pelanggan",
                    asyncItems: (String filter) async {
                      productionControllers.lookupCustomer(searching: filter);
                      return productionControllers.dataCustomer;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      // print(data);
                      // productionControllers.currentFilter.value = {
                      //   ...productionControllers.currentFilter,
                      //   'customer_preview': searchValue,
                      // };
                    },
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'product',
                    controller: productFilterController,
                    currentValue:
                        productionControllers.currentFilter['product_preview'],
                    label: "Produk/Mutu",
                    hintText: "Pilih Produk/Mutu",
                    asyncItems: (String filter) async {
                      productionControllers.lookupProductMutu(
                          searching: filter);
                      return productionControllers.dataProductMutu;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      // print(data);
                      // productionControllers.currentFilter.value = {
                      //   ...productionControllers.currentFilter,
                      //   'product_preview': searchValue,
                      // };
                    },
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.formDateRangePicker2(
                    context: context,
                    fieldName: 'date_range',
                    title: "Tanggal",
                    hintText: "Pilih tanggal",
                    currentValue: BaseGlobalFuntion.formatDateRange(
                      productionControllers.byDate['start-date'] ?? "",
                      productionControllers.byDate['end-date'] ?? "",
                    ),
                    startDateController: TextEditingController(),
                    endDateController: TextEditingController(),
                    onChange: ({endDate, startDate}) {
                      productionControllers.filterByRangeDate(
                        startDate: startDate,
                        endDate: endDate,
                      );
                    },
                  ),
                  FormBuilderChoiceChip<String>(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      labelText: 'Status',
                      hintText: "Pilih Status",
                      contentPadding: const EdgeInsets.symmetric(vertical: 2),
                      labelStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 15,
                        kfontWeight: FontWeight.w500,
                        kcolor: AppColor.kPrimaryColor,
                      ),
                      errorStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 13,
                        kcolor: AppColor.kFailedColor,
                        kfontWeight: FontWeight.w500,
                      ),
                    ),
                    name: 'status_code',
                    initialValue:
                        productionControllers.currentFilter['status_code'],
                    selectedColor: AppColor.kInactiveColor,
                    spacing: 6.0,
                    options: List.generate(isActive.length, (index) {
                      return FormBuilderChipOption(
                        value: isActive[index],
                        child: Text(
                          isActive[index],
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontWeight: FontWeight.w600,
                            kfontSize: 12,
                          ),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    // ** LIST OF CONTENT CARD
    final listItemContent = Flexible(
      child: Obx(() {
        final result = productionControllers.productionScheduleData;

        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: result.isEmpty && productionControllers.isLoading.isFalse
              ? SizedBox(
                  height: MediaQuery.of(context).size.height / 1.2,
                  child: Center(
                    child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                  ),
                )
              : ListView.separated(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount:
                      // productionControllers.hasMore.value
                      // ?
                      result.length + 1,
                  // : result.length,
                  separatorBuilder: (BuildContext context, int index) =>
                      const SizedBox(height: 5),
                  itemBuilder: (BuildContext context, int index) {
                    if (index < result.length) {
                      final data = result[index];
                      var colorBadge = BaseGlobalFuntion.statusColor(
                          result[index]['status_code']);
                      return BaseCardWidget.cardStyle1(
                          ontap: () {
                            Get.to(
                              () => ProductionScheduleView(itemId: data['id']),
                            );
                          },
                          title: data['product_name'],
                          trailing: Text.rich(
                            textAlign: TextAlign.center,
                            TextSpan(children: [
                              BaseTextWidget.customTextSpan(
                                text: data['quantity'],
                                extendText: '\n',
                                letterSpacing: 0.8,
                                fontSize: 13,
                                fontWeight: FontWeight.w600,
                              ),
                              BaseTextWidget.customTextSpan(
                                text: data['uom_name'],
                                color: AppColor.kSoftGreyColor,
                                letterSpacing: 0.8,
                                fontSize: 13,
                                fontWeight: FontWeight.w400,
                              ),
                            ]),
                          ),
                          subTitle1: data['customer_project_name'],
                          subTitle2: BaseTextWidget.customText(
                            text: data['product_code'],
                            fontSize: 12,
                            isLongText: true,
                            overflow: TextOverflow.ellipsis,
                          ),
                          headerString: DateTime.parse(data['production_date'])
                              .toFormattedDateTime(),
                          // BaseGlobalFuntion.datetimeConvert(
                          //     data['production_date'], "dd MMM yyyy HH:mm"),
                          statusHeader: {
                            'title': data['status_code'] ?? "",
                            'color': colorBadge['background_color'],
                            'titleColor': colorBadge['text_color'],
                          });
                    } else if (result.length < 25) {
                      return productionControllers.hasMore.value &&
                              productionControllers.isLoading.isTrue
                          ? BaseLoadingWidget.cardShimmer(
                              height: MediaQuery.of(context).size.height,
                              shimmerheight: 80,
                              baseColor: AppColor.kSoftGreyColor,
                              highlightColor: AppColor.kWhiteColor,
                              itemCount: 25,
                            )
                          : const SizedBox();
                    } else {
                      return productionControllers.hasMore.value
                          ? BaseLoadingWidget.cardShimmer(
                              height: 200,
                              shimmerheight: 90,
                              baseColor: AppColor.kSoftGreyColor,
                              highlightColor: AppColor.kWhiteColor,
                              itemCount: 2,
                            )
                          : const SizedBox();
                    }
                  },
                ),
        );
      }),
    );

    Widget loadingCardSummary({itemCount}) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: List.generate(
          itemCount,
          (index) => Expanded(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 5),
              width: double.infinity,
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: AppColor.kWhiteColor,
                borderRadius: const BorderRadius.all(Radius.circular(8)),
                border: Border.all(color: AppColor.kAvailableColor),
              ),
              child: Shimmer.fromColors(
                  baseColor: AppColor.kSoftGreyColor,
                  highlightColor: AppColor.kWhiteColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 40,
                        height: 18,
                        decoration: BoxDecoration(
                          color: Colors.amber,
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                      const SizedBox(height: 5),
                      Container(
                        width: 80,
                        height: 18,
                        decoration: BoxDecoration(
                          color: Colors.amber,
                          borderRadius: BorderRadius.circular(4),
                        ),
                      ),
                    ],
                  )),
            ),
          ),
        ),
      );
    }

    // ** SUMMARY CONTENT
    final summaryCard = Positioned(
      top: 10.0,
      left: 0.0,
      right: 0.0,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 5.0),
        child: Obx(() {
          final data = productionControllers.summaryProductionSchedule;
          if (data.isEmpty) {
            return loadingCardSummary(itemCount: 3);
          } else {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                BaseCardWidget.cardMonitoring(
                  title: "Vol.Dijadwalkan",
                  kcontentColor: AppColor.kPrimaryColor,
                  content: BaseGlobalFuntion.currencyLocalConvert(
                    nominal: data['quantity'],
                    isLocalCurency: false,
                  ),
                ),
                const SizedBox(width: 2),
                BaseCardWidget.cardMonitoring(
                  title: "Vol.Diproduksi",
                  kcontentColor: AppColor.kSuccesColor,
                  content: BaseGlobalFuntion.currencyLocalConvert(
                    nominal: data['actual_quantity'],
                    isLocalCurency: false,
                  ),
                ),
                const SizedBox(width: 2),
                BaseCardWidget.cardMonitoring(
                  title: "Vol.Sisa",
                  kcontentColor: AppColor.kWarningColor,
                  content: BaseGlobalFuntion.currencyLocalConvert(
                    nominal: data['remaining_quantity'],
                    isLocalCurency: false,
                  ),
                ),
              ],
            );
          }
        }),
      ),
    );

    // ** STRUCTURE STYLE
    final topContent = SizedBox(
      height: 80,
      width: double.infinity,
      child: Stack(
        children: [
          Container(
            height: 50,
            width: double.infinity,
            padding: const EdgeInsets.only(left: 10, right: 10, top: 15),
            decoration: const BoxDecoration(
              color: AppColor.kPrimaryColor,
              gradient: RadialGradient(
                radius: 1.2,
                center: Alignment(0.2, 1.4),
                colors: [
                  AppColor.kSoftBlueColor,
                  AppColor.kPrimaryColor,
                ],
              ),
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10),
              ),
            ),
          ),
          summaryCard,
        ],
      ),
    );

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
            title: 'Rencana Produksi',
            context: context,
            isAllFill: true,
            searchController: productionControllers.search,
            searchText: 'Pencarian',
            onFillterChange: () {
              showBottomFilter(context);
            },
            onSearchChange: () {
              BaseGlobalFuntion.debounceRun(
                action: () {
                  productionControllers.refreshDataProductionSchedule();
                },
                duration: const Duration(milliseconds: 1000),
              );
            }),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              RefreshIndicator(
                onRefresh: onRefresh,
                child: SingleChildScrollView(
                  physics: const AlwaysScrollableScrollPhysics(),
                  controller: scrollController,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      topContent,
                      const SizedBox(height: 10.0),
                      listItemContent
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 30.0,
                right: 15.0,
                child: SizedBox(
                  height: 55.0,
                  width: 55.0,
                  child: IconButton.filled(
                    iconSize: 30,
                    onPressed: () {
                      Get.to(() => const AddPSNew());
                    },
                    icon: const Icon(
                      Icons.add,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
