import 'package:flutter/services.dart';
import 'package:rbs_mobile_operation/data/datasources/productionschedule.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class AddProductionSchedule extends StatelessWidget {
  final dynamic orderId;
  final dynamic productionUnitId;

  const AddProductionSchedule({
    required this.orderId,
    required this.productionUnitId,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    AddScheduleControllers addProductionSchedule =
        Get.put(AddScheduleControllers());

    // Future.delayed(const Duration(milliseconds: 200), () async {
    //   if (orderId != addProductionSchedule.bodySchedule['sales_order_id']) {
    //     // JIKA ID YANG SEBELUMNYA TIDAK SAMA & PADA CART ADA ISINYA
    //     // MAKA DATA CARTNYA DI HAPUS
    //     addProductionSchedule.bodySchedule.clear();
    //     addProductionSchedule.cartSchedule.clear();
    //     addProductionSchedule.summarySchedule.clear();
    //     // CONTENT
    //     addProductionSchedule.dataSODetail.clear();
    //     addProductionSchedule.detailSalesOrder.clear();
    //   }
    //   await addProductionSchedule.lookupSODetail(
    //     soId: orderId,
    //   );
    // }).then(
    //   (value) => addProductionSchedule.viewSalesOrder(
    //     salesOrderId: orderId,
    //   ),
    // );

    Future<void> showDialogBuilder(BuildContext context, bool isCart) {
      return showModalBottomSheet<void>(
          context: context,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(top: Radius.circular(10.0))),
          backgroundColor: AppColor.kWhiteColor,
          builder: (BuildContext context) {
            return Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 10,
                ),
                child: Obx(() {
                  var dataSO = addProductionSchedule.detailSalesOrder;
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Detail Rencana Produksi ",
                            style: BaseTextStyle.customTextStyle(
                              kfontSize: 14,
                              kfontWeight: FontWeight.w600,
                              kcolor: AppColor.kPrimaryColor,
                            ),
                          ),
                          IconButton.filledTonal(
                            onPressed: () {
                              Get.back();
                            },
                            icon: const Icon(Icons.close),
                          ),
                        ],
                      ),
                      BaseTextWidget.customText(
                        text: dataSO['order_number'],
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        isLongText: true,
                      ),
                      BaseTextWidget.customText(
                        text: dataSO['customer_project']['name'],
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                        isLongText: true,
                      ),
                      const Divider(),
                      Obx(() {
                        var resultTemp = addProductionSchedule.cartSchedule;

                        return Flexible(
                          child: ListView.separated(
                            shrinkWrap: true,
                            itemCount: resultTemp.length,
                            separatorBuilder: (context, index) =>
                                const Divider(),
                            itemBuilder: (context, index) {
                              return ListTile(
                                  // contentPadding:
                                  //     const EdgeInsets.symmetric(horizontal: 0),
                                  // shape: RoundedRectangleBorder(
                                  //   side: const BorderSide(
                                  //     width: 1.5,
                                  //     color: AppColor.kGreyColor,
                                  //   ),
                                  //   borderRadius: BorderRadius.circular(8),
                                  // ),
                                  title: BaseTextWidget.customText(
                                    text: resultTemp[index]['product_name'],
                                    isLongText: true,
                                    fontSize: 13,
                                    color: AppColor.kPrimaryColor,
                                    fontWeight: FontWeight.w600,
                                  ),
                                  subtitle: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text.rich(
                                        textAlign: TextAlign.start,
                                        TextSpan(children: [
                                          BaseTextWidget.customTextSpan(
                                            text: BaseGlobalFuntion
                                                .datetimeConvert(
                                                    resultTemp[index]['date'],
                                                    'dd MMM yyyy HH:mm'),
                                            extendText: ' \n',
                                            style:
                                                BaseTextStyle.customTextStyle(
                                              letterSpacing: 0.8,
                                              kfontSize: 12,
                                              kfontWeight: FontWeight.w500,
                                            ),
                                          ),
                                          BaseTextWidget.customTextSpan(
                                            text: resultTemp[index]['quantity'],
                                            extendText: ' M3',
                                            color: AppColor.kPrimaryColor,
                                            fontSize: 12,
                                            fontWeight: FontWeight.w600,
                                          )
                                          // BaseTextWidget.customTextSpan(
                                          //   text: resultTemp[index]['quantity'],
                                          //   extendText: ' M3',
                                          //   color: AppColor.kPrimaryColor,
                                          //   fontSize: 12,
                                          //   fontWeight: FontWeight.w600,
                                          // )
                                        ]),
                                      ),
                                    ],
                                  ),
                                  // trailing:
                                  // BaseTextWidget.customText(
                                  //   text: resultTemp[index]['quantity'],
                                  //   extendText: ' M3',
                                  //   color: AppColor.kPrimaryColor,
                                  //   fontSize: 12,
                                  //   fontWeight: FontWeight.w600,
                                  // ),
                                  trailing: IconButton.filledTonal(
                                    splashColor: AppColor.kErrorColor,
                                    onPressed: () {
                                      resultTemp.remove(resultTemp[index]);

                                      if (resultTemp.isEmpty) {
                                        Get.back();
                                      }
                                    },
                                    icon: const Icon(
                                      Icons.delete,
                                      color: AppColor.kErrorColor,
                                    ),
                                    style: FilledButton.styleFrom(
                                      backgroundColor:
                                          AppColor.kBadgeFailedColor,
                                    ),
                                  ));
                            },
                          ),
                        );
                      }),
                      const SizedBox(height: 5),
                      // const Divider(),
                      if (isCart == false)
                        Obx(() => Button.filled(
                              onPressed: () =>
                                  addProductionSchedule.addScheduleProduction(
                                loadingCtx: context,
                                saveData: addProductionSchedule.bodySchedule,
                              ),
                              color: AppColor.kPrimaryColor,
                              // height: 50,
                              width: double.infinity,
                              // isLoading: addProductionSchedule.isLoading.value,
                              disabled: addProductionSchedule.isLoading.value,
                              icon: const Icon(
                                Icons.add_circle,
                                color: AppColor.kWhiteColor,
                              ),
                              // rightIcon: true,
                              // textStyle: BaseTextStyle.customTextStyle(
                              //   kfontWeight: FontWeight.w600,
                              //   kfontSize: 12,
                              //   kcolor: AppColor.kWhiteColor,
                              // ),
                              label: "Simpan",
                            ))
                    ],
                  );
                }));
          });
    }

    final submitAction = Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Obx(() => Expanded(
                    child: GestureDetector(
                  onTap: () {},
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      GestureDetector(
                        // onTap: () => print(addProductionSchedule.cartSchedule),
                        onTap: () => showDialogBuilder(context, true),
                        child: Row(
                          children: [
                            Text(
                                "${addProductionSchedule.cartSchedule.length} Jadwal",
                                style: BaseTextStyle.customTextStyle(
                                  kcolor: AppColor.kPrimaryColor,
                                  kfontSize: 12,
                                  kfontWeight: FontWeight.w500,
                                )),
                            GestureDetector(
                              onTap: () => showDialogBuilder(context, true),
                              child: const Icon(
                                Icons.info_outline_rounded,
                                size: 16,
                              ),
                            ),
                          ],
                        ),
                      ),
                      BaseTextWidget.customText(
                        text: addProductionSchedule
                            .summarySchedule['total_quantity'],
                        extendText: ' M3',
                        color: AppColor.kPrimaryColor,
                        fontSize: 13,
                        fontWeight: FontWeight.w700,
                      ),
                    ],
                  ),
                ))),
            const SizedBox(width: 10),
            Obx(() => Expanded(
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: Button.filled(
                      onPressed: () => showDialogBuilder(context, false),
                      color: AppColor.kPrimaryColor,
                      // height: 50,
                      disabled: addProductionSchedule.cartSchedule.isEmpty,
                      // isLoading: addProductionSchedule.isLoading.value,
                      icon: const Icon(
                        Icons.arrow_circle_right_rounded,
                        color: AppColor.kWhiteColor,
                      ),
                      fontSize: 12,
                      // rightIcon: true,
                      // textStyle: BaseTextStyle.customTextStyle(
                      //   kfontWeight: FontWeight.w600,
                      //   kfontSize: 12,
                      //   kcolor: AppColor.kWhiteColor,
                      // ),
                      label: "Selanjutnya",
                    ),
                  ),
                )),
          ],
        ),
      ),
    );

    final listContent = Container(
      // height: 80,
      width: double.infinity,
      padding: const EdgeInsets.only(right: 20, left: 20, top: 15, bottom: 50),
      decoration: const BoxDecoration(
        color: AppColor.kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(10),
          bottomRight: Radius.circular(10),
        ),
      ),
      child: Obx(() {
        var dataSO = addProductionSchedule.detailSalesOrder;

        return dataSO.isEmpty
            ? Shimmer.fromColors(
                baseColor: AppColor.kSoftGreyColor,
                highlightColor: AppColor.kWhiteColor,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                    const SizedBox(width: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 150,
                          height: 15,
                          decoration: BoxDecoration(
                            color: Colors.amber,
                            borderRadius: BorderRadius.circular(4),
                          ),
                        ),
                        const SizedBox(height: 10),
                        Container(
                          width: 200,
                          height: 15,
                          decoration: BoxDecoration(
                            color: Colors.amber,
                            borderRadius: BorderRadius.circular(4),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: AppColor.kWhiteColor,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Icon(
                            Icons.fire_truck_rounded,
                          ),
                        ),
                        const SizedBox(width: 15),
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              BaseTextWidget.customText(
                                text: dataSO['order_number'],
                                fontSize: 13,
                                color: AppColor.kWhiteColor,
                                fontWeight: FontWeight.bold,
                                isLongText: true,
                              ),
                              BaseTextWidget.customText(
                                text: dataSO['customer_project']['name'],
                                fontSize: 13,
                                color: AppColor.kWhiteColor,
                                fontWeight: FontWeight.w600,
                                isLongText: true,
                              ),
                              BaseTextWidget.customText(
                                text: dataSO['customer']['name'],
                                fontSize: 13,
                                color: AppColor.kWhiteColor,
                                fontWeight: FontWeight.w500,
                                isLongText: true,
                              ),
                              // BaseTextWidget.customReadmore(
                              //   text: dataSO['customer_project']['name'] +
                              //       "ASDSDASDASDSDASDASDASDASDASASDDDSSDASDASsdasdadsasdasdasdasdsdasdadsasdasdasdadads",
                              //   color: AppColor.kWhiteColor,
                              //   fontSize: 13,
                              //   fontWeight: FontWeight.w500,
                              // ),
                              // const SizedBox(height: 2),
                              // BaseTextWidget.customReadmore(
                              //   text: dataSO['customer']['name'],
                              //   color: AppColor.kWhiteColor,
                              //   fontSize: 13,
                              //   fontWeight: FontWeight.w500,
                              // ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              );
      }),
    );

    Future<void> showBottomAddCart(BuildContext contexts, itemContent) {
      return showModalBottomSheet(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(10.0))),
        backgroundColor: AppColor.kWhiteColor,
        context: contexts,
        isScrollControlled: true,
        builder: (context) => Padding(
          padding: EdgeInsets.only(
              top: 20,
              right: 15,
              left: 15,
              bottom: MediaQuery.of(context).viewInsets.bottom),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Tambah Rencana Produksi",
                    style: BaseTextStyle.customTextStyle(
                      kfontSize: 14,
                      kfontWeight: FontWeight.w600,
                    ),
                  ),
                  IconButton.filledTonal(
                    onPressed: () {
                      Get.back();
                    },
                    icon: const Icon(Icons.close),
                  ),
                ],
              ),
              const Divider(),
              ...itemContent,
            ],
          ),
        ),
      );
    }

    final listCardContent = Flexible(child: Obx(() {
      var dataSODetail = addProductionSchedule.dataSODetail;

      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: dataSODetail.isEmpty
            ? BaseLoadingWidget.cardShimmer(
                height: MediaQuery.of(context).size.height,
                shimmerheight: 90,
                width: double.infinity,
                baseColor: AppColor.kSoftGreyColor,
                highlightColor: AppColor.kWhiteColor,
                itemCount: 15,
              )
            : ListView.separated(
                // controller: scrollController,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: dataSODetail.length,
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(height: 7),
                itemBuilder: (BuildContext listContext, int index) {
                  return BaseCardWidget.cardStyle4(
                    ontap: () {
                      final itemInfo = Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              BaseTextWidget.customText(
                                text: dataSODetail[index]['product_name'],
                                fontWeight: FontWeight.w600,
                                fontSize: 15,
                              ),
                              BaseTextWidget.customText(
                                text: dataSODetail[index]
                                    ['product_category_name'],
                                color: AppColor.kPrimaryColor,
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                              ),
                              const SizedBox(height: 5),
                              Row(
                                  // crossAxisAlignment:
                                  //     CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <dynamic>[
                                    {
                                      'title': 'Total Order',
                                      'content': dataSODetail[index]['quantity']
                                          .toStringAsFixed(2),
                                      'colors': AppColor.kPrimaryColor
                                    },
                                    {
                                      'title': 'Dijadwalkan',
                                      'content': dataSODetail[index]
                                              ['scheduled']
                                          .toStringAsFixed(2),
                                      'colors': AppColor.kSuccesColor
                                    },
                                    {
                                      'title': 'Belum Dijadwalkan',
                                      'content': dataSODetail[index]
                                              ['unschedule']
                                          .toStringAsFixed(2),
                                      'colors': AppColor.kWarningColor
                                    }
                                  ]
                                      .map(
                                        (item) => Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            BaseTextWidget.customText(
                                              text: item['title'],
                                              color: AppColor.kPrimaryColor,
                                              fontSize: 12,
                                              fontWeight: FontWeight.w600,
                                            ),
                                            Row(
                                              children: [
                                                Container(
                                                  width: 10,
                                                  height: 10,
                                                  margin: const EdgeInsets.only(
                                                      right: 5),
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            19),
                                                    color: item['colors'],
                                                  ),
                                                ),
                                                BaseTextWidget.customText(
                                                  text: item['content'],
                                                  color: AppColor.kPrimaryColor,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      )
                                      .toList()),
                            ],
                          ),
                        ],
                      );

                      final formList = FormBuilder(
                        key: addProductionSchedule.kFormKey,
                        onChanged: () {
                          addProductionSchedule.kFormKey.currentState!.save();
                        },
                        autovalidateMode: AutovalidateMode.disabled,
                        skipDisabled: true,
                        child: Column(
                          children: [
                            BaseTextFieldWidget.textFieldFormBuilder2(
                              title: 'Volume',
                              hintText: "75.00",
                              fieldName: 'quantity',
                              isRequired: true,
                              onClearText: () => addProductionSchedule
                                  .kFormKey.currentState!
                                  .patchValue({'quantity': null}),
                              onChange: ({value}) {},
                              extendSuffixItem: Container(
                                margin: const EdgeInsets.all(1.2),
                                padding: const EdgeInsets.all(14),
                                decoration: const BoxDecoration(
                                  color: AppColor.kPrimaryColor,
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(6),
                                      bottomRight: Radius.circular(6)),
                                ),
                                child: Text(
                                  "M3",
                                  style: BaseTextStyle.customTextStyle(
                                    kcolor: AppColor.kSoftBlueColor,
                                    kfontWeight: FontWeight.w600,
                                    kfontSize: 15,
                                  ),
                                ),
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Volume wajib diisi!';
                                }

                                if (double.parse(value) < 0.25) {
                                  return 'Volume (M3) tidak boleh kurang dari 0.25';
                                }

                                if (dataSODetail.isNotEmpty) {
                                  if (double.parse(value) >
                                      dataSODetail[index]['unschedule']) {
                                    return 'Volume (M3) tidak boleh lebih dari ${dataSODetail[index]['unschedule'].toStringAsFixed(2)}';
                                  }
                                }

                                // print(double.parse(value));
                                // print(index);
                                // print(dataSODetail);

                                // if (double.parse(value) >
                                //     dataSODetail[index]['unschedule']) {
                                // }

                                return null;
                              },
                              keyboardType:
                                  const TextInputType.numberWithOptions(
                                      decimal: true),
                              textInputAction: TextInputAction.next,
                              inputFormater: <TextInputFormatter>[
                                // FilteringTextInputFormatter.allow(
                                //     RegExp(r'^[0-9.]+$')),
                                FilteringTextInputFormatter.allow(
                                    RegExp(r'^\d*\.?\d*')),
                              ],
                            ),
                            const SizedBox(height: 5),
                            BaseButtonWidget.formDatePicker2(
                              context: context,
                              title: "Tanggal",
                              hintText: "Pilih tanggal",
                              isRequired: true,
                              selectDateController: TextEditingController(),
                              onChange: ({dateValue}) {},
                              fieldName: "date",
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Tanggal produksi wajib diisi!';
                                }
                                return null;
                              },
                            ),
                            const SizedBox(height: 5),
                            BaseButtonWidget.timePicker(
                              context: context,
                              isRequired: true,
                              title: "Jam",
                              hintText: 'Pilih jam',
                              valueController: TextEditingController(),
                              // onChange: ({dateValue}) {},
                              fieldName: "time",
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Waktu produksi wajib diisi!';
                                }
                                return null;
                              },
                            ),
                          ],
                        ),
                      );

                      showBottomAddCart(context, [
                        itemInfo,
                        const Divider(),
                        // const SizedBox(height: 15),
                        Flexible(
                          child: ListView(
                            shrinkWrap: true,
                            physics: const AlwaysScrollableScrollPhysics(),
                            children: [formList],
                          ),
                        ),
                        const SizedBox(height: 20),
                        Button.filled(
                          onPressed: () {
                            addProductionSchedule
                                .scheduleProductionOrder(extendData: {
                              "production_unit_id": productionUnitId,
                              "sales_order_id": orderId,
                              "product_id": dataSODetail[index]['product_id'],
                              "product_name": dataSODetail[index]
                                  ['product_name']
                            });
                          },
                          color: AppColor.kPrimaryColor,
                          // height: 50,
                          width: double.infinity,
                          icon: const Icon(
                            Icons.add_circle,
                            color: AppColor.kWhiteColor,
                          ),
                          // rightIcon: true,
                          // textStyle: BaseTextStyle.customTextStyle(
                          //   kfontWeight: FontWeight.w600,
                          //   kfontSize: 12,
                          //   kcolor: AppColor.kWhiteColor,
                          // ),
                          label: "Jadwalkan Produk",
                        ),
                        const SizedBox(height: 5),
                      ]);
                    },
                    title: dataSODetail[index]['product_name'],
                    subtitle: dataSODetail[index]['product_code'],
                    bottomSubtitle: BaseTextWidget.customText(
                      text: dataSODetail[index]['product_category_name'],
                      isLongText: true,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  );
                },
              ),
      );
    }));

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        title: 'Pilih Produk/Mutu',
        context: context,
        extendBack: () {
          // addProductionSchedule.bodySchedule.clear();
          // addProductionSchedule.cartSchedule.clear();
          // addProductionSchedule.summarySchedule.clear();
          // addProductionSchedule.dataSODetail.clear();
          // addProductionSchedule.detailSalesOrder.clear();
        },
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                listContent,
                const SizedBox(height: 10),
                listCardContent,
              ],
            ),
          ),
        ],
      ),
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: submitAction,
    );
  }
}
