import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rbs_mobile_operation/data/datasources/productionschedule.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/production_scedule/create/add_production_schedule.dart';

class AddPSNew extends StatelessWidget {
  const AddPSNew({super.key});

  @override
  Widget build(BuildContext context) {
    // AddSalesControllers salesController = Get.put(AddSalesControllers());
    AddScheduleControllers addScheduleControllers =
        Get.put(AddScheduleControllers());

    Future onRefresh() async {
      addScheduleControllers.refreshDataSo();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        addScheduleControllers.lookupSalesOrder();
      }
    });

    // TextEditingController puTextEditingController = TextEditingController();

    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          addScheduleControllers.currentFilter.clear();
          addScheduleControllers.formKeyFilter.currentState!
              .patchValue({'product_id': null});
        },
        onFilter: () {
          Get.back();
          addScheduleControllers.refreshDataSo();
        },
        valueFilter: addScheduleControllers.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: addScheduleControllers.formKeyFilter,
            onChanged: () {
              addScheduleControllers.formKeyFilter.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  // const SizedBox(height: 10),
                  // BaseButtonWidget.dropdownButton(
                  //   fieldName: "production_unit_id",
                  //   valueController: puTextEditingController,
                  //   valueData: addScheduleControllers.dataLookupPU,
                  //   label: "Unit Produksi",
                  //   hintText:
                  //       addScheduleControllers.currentFilter['unit_preview'] ??
                  //           "Pilih unit produksi",
                  //   searchOnChange: (p0) {},
                  //   onChange: ({searchValue, valueItem}) {
                  //     addScheduleControllers.currentFilter.value = {
                  //       ...addScheduleControllers.currentFilter,
                  //       'unit_preview': searchValue,
                  //     };
                  //   },
                  // ),
                  const SizedBox(height: 10),
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'product_id',
                    controller: TextEditingController(),
                    currentValue:
                        addScheduleControllers.currentFilter['product_preview'],
                    label: "Produk/Mutu",
                    hintText: "Pilih Produk/Mutu",
                    asyncItems: (String filter) async {
                      addScheduleControllers.lookupProdukMutu(
                          searching: filter);
                      return addScheduleControllers.dataProdukMutu;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      // print(data);
                      addScheduleControllers.currentFilter.value = {
                        ...addScheduleControllers.currentFilter,
                        'product_preview': searchValue,
                      };
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    Future<void> showBottomAddCart(BuildContext context, itemContent) {
      return showModalBottomSheet(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        backgroundColor: AppColor.kWhiteColor,
        // backgroundColor: Colors.transparent,
        context: context,
        isScrollControlled: true,
        builder: (context) => Padding(
          padding: EdgeInsets.only(
              top: 20,
              right: 20,
              left: 20,
              bottom: MediaQuery.of(context).viewInsets.bottom),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Pilih Mutu/Produk",
                    style: BaseTextStyle.customTextStyle(
                      kfontSize: 14,
                      kfontWeight: FontWeight.w600,
                    ),
                  ),
                  IconButton.filledTonal(
                    onPressed: () {
                      Get.back();
                    },
                    icon: const Icon(Icons.close),
                  ),
                ],
              ),
              const Divider(),
              ...itemContent,
            ],
          ),
        ),
      );
    }

    Future<void> showDialogBuilder(BuildContext context) {
      return showModalBottomSheet<void>(
          context: context,
          builder: (BuildContext context) {
            return SizedBox(
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 18,
                  vertical: 30,
                ),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "Keranjang Order",
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontSize: 15,
                            kfontWeight: FontWeight.w600,
                          ),
                        )
                      ],
                    ),
                    const SizedBox(height: 5),
                  ],
                ),
              ),
            );
          });
    }

    final listCardContent = Flexible(
      child: Obx(() {
        final resultSO = addScheduleControllers.dataLookupSO;

        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 0),
          child: RefreshIndicator(
            onRefresh: onRefresh,
            child: resultSO.isEmpty
                ? SingleChildScrollView(
                    physics: const AlwaysScrollableScrollPhysics(),
                    child: SizedBox(
                      height: MediaQuery.of(context).size.height / 1.2,
                      child: Center(
                        child: addScheduleControllers.isLoading.isTrue
                            ? const SpinKitThreeBounce(
                                size: 30.0,
                                color: AppColor.kPrimaryColor,
                              )
                            : BaseLoadingWidget.noMoreDataWidget(height: 200),
                      ),
                    ),
                  )
                : ListView.separated(
                    controller: scrollController,
                    itemCount: resultSO.length + 1,
                    physics: const AlwaysScrollableScrollPhysics(),
                    separatorBuilder: (BuildContext context, int index) =>
                        const SizedBox(height: 7),
                    itemBuilder: (BuildContext context, int index) {
                      if (index < resultSO.length) {
                        final data = resultSO[index];

                        return BaseCardWidget.cardStyle4(
                          ontap: () {
                            // Future.delayed(const Duration(milliseconds: 500),
                            //     () async {
                            //   addScheduleControllers.lookupSODetail(
                            //     soId: data['id'],
                            //   );

                            //   addScheduleControllers.viewSalesOrder(
                            //     salesOrderId: data['id'],
                            //   );
                            // });

                            Future.delayed(const Duration(milliseconds: 200),
                                () async {
                              if (data['id'] !=
                                  addScheduleControllers
                                      .bodySchedule['sales_order_id']) {
                                // JIKA ID YANG SEBELUMNYA TIDAK SAMA & PADA CART ADA ISINYA
                                // MAKA DATA CARTNYA DI HAPUS
                                addScheduleControllers.bodySchedule.clear();
                                addScheduleControllers.cartSchedule.clear();
                                addScheduleControllers.summarySchedule.clear();
                                // CONTENT
                                addScheduleControllers.dataSODetail.clear();
                                addScheduleControllers.detailSalesOrder.clear();
                              }
                              await addScheduleControllers.lookupSODetail(
                                soId: data['id'],
                              );
                            }).then(
                              (value) => addScheduleControllers.viewSalesOrder(
                                salesOrderId: data['id'],
                              ),
                            );

                            Get.to(
                              () => AddProductionSchedule(
                                orderId: data['id'],
                                productionUnitId: data['production_unit_id'],
                              ),
                            );
                          },
                          title: data['order_number'],
                          subtitle: data['customer_name'],
                          bottomSubtitle: BaseTextWidget.customText(
                            text: data['customer_project_name'],
                            isLongText: true,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                          extendWidget: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              BaseTextWidget.customText(
                                text: data['quantity'],
                                extendText: 'M3',
                                color: AppColor.kPrimaryColor,
                                fontWeight: FontWeight.w600,
                              ),
                            ],
                          ),
                          trailling: BaseTextWidget.customText(
                            text: BaseGlobalFuntion.datetimeConvert(
                                data['date'], "dd MMM yyyy"),
                            fontSize: 12,
                            color: AppColor.kSuccesColor,
                            fontWeight: FontWeight.w500,
                          ),
                        );
                      } else if (resultSO.length < 25) {
                        return addScheduleControllers.hasMore.value &&
                                addScheduleControllers.isLoading.isTrue
                            ? BaseLoadingWidget.cardShimmer(
                                height: MediaQuery.of(context).size.height,
                                shimmerheight: 80,
                                baseColor: AppColor.kSoftGreyColor,
                                highlightColor: AppColor.kWhiteColor,
                                itemCount: 25,
                              )
                            : const SizedBox();
                      } else {
                        return addScheduleControllers.hasMore.value
                            ? BaseLoadingWidget.cardShimmer(
                                height: 200,
                                shimmerheight: 90,
                                baseColor: AppColor.kSoftGreyColor,
                                highlightColor: AppColor.kWhiteColor,
                                itemCount: 2,
                              )
                            : const SizedBox();
                      }
                    },
                  ),
          ),
        );
      }),
    );

    final listFilter = Container(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        children: [
          Expanded(
            child: BaseTextFieldWidget.roundedStyleTextField(
              textEditingController: TextEditingController(),
              onChange: (val) {
                BaseGlobalFuntion.debounceRun(
                  action: () {
                    // salesController.refreshDataProduct();
                  },
                  duration: const Duration(milliseconds: 1000),
                );
              },
              suffixIcon: const Icon(Icons.search),
              radius: 10,
              height: 35,
              borderSide: const BorderSide(color: AppColor.kInactiveColor),
              hintText: 'Cari',
            ),
          ),
          const SizedBox(width: 5),
          Container(
            width: 35,
            height: 35,
            decoration: BoxDecoration(
              color: AppColor.kSoftBlueColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: IconButton(
              iconSize: 20,
              onPressed: () {
                showBottomFilter(context);
              },
              icon: const Icon(Icons.filter_list),
            ),
          ),
        ],
      ),
    );

    return SafeArea(
      child: PopScope(
        canPop: true,
        child: Scaffold(
          appBar: BaseExtendWidget.appBar(
            title: 'Tambah Jadwal Produksi',
            context: context,
            extendBack: () {
              // salesController.cart.clear();
              // salesController.cartSummary.clear();
            },
          ),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Column(
              children: [
                listFilter,
                const SizedBox(height: 10),
                listCardContent,
              ],
            ),
          ),
          resizeToAvoidBottomInset: true,
          // bottomNavigationBar: submitAction,
        ),
      ),
    );
  }
}
