import 'package:flutter/services.dart';
import 'package:rbs_mobile_operation/core/extentions/date_time_extension.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';
import 'package:rbs_mobile_operation/data/datasources/productionschedule.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/app_view_resource.dart';

class ProductionScheduleView extends StatelessWidget {
  final int itemId;

  const ProductionScheduleView({
    required this.itemId,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    ViewScheduleControllers productionControllers =
        Get.put(ViewScheduleControllers());

    productionControllers.fetchViewProductionScedule(itemId);
    productionControllers.lookupProduction(itemId);

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Detail Rencana Produksi',
          context: context,
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 15),
              decoration: const BoxDecoration(
                color: AppColor.kPrimaryColor,
                border: Border(
                  top: BorderSide(
                    color: AppColor.kPrimaryColor,
                  ),
                ),
              ),
              child: Obx(() {
                if (productionControllers.viewProductionScedule.isNotEmpty) {
                  var data = productionControllers.viewProductionScedule;

                  var colorBadge =
                      BaseGlobalFuntion.statusColor(data['status_code']);

                  return Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                              padding: const EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                color: AppColor.kWhiteColor,
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: const Icon(
                                Icons.info_rounded,
                              ),
                            ),
                            const SizedBox(width: 10),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  BaseTextWidget.customText(
                                    text: data['product_name'],
                                    color: AppColor.kWhiteColor,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w700,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    isLongText: true,
                                  ),
                                  BaseTextWidget.customText(
                                    text: data['product_code'],
                                    color: AppColor.kWhiteColor,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500,
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    isLongText: true,
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(width: 5),
                          ],
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 2, horizontal: 6),
                        decoration: BoxDecoration(
                          color: colorBadge['background_color'],
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Text(
                          "${data['status_code']}",
                          style: BaseTextStyle.customTextStyle(
                            kfontSize: 11,
                            kcolor: colorBadge['text_color'],
                          ),
                        ),
                      )
                    ],
                  );
                } else {
                  return Shimmer.fromColors(
                      baseColor: AppColor.kSoftGreyColor,
                      highlightColor: AppColor.kWhiteColor,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: 40,
                            height: 40,
                            decoration: BoxDecoration(
                              color: Colors.amber,
                              borderRadius: BorderRadius.circular(8),
                            ),
                          ),
                          const SizedBox(width: 10),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 150,
                                height: 15,
                                decoration: BoxDecoration(
                                  color: Colors.amber,
                                  borderRadius: BorderRadius.circular(4),
                                ),
                              ),
                              const SizedBox(height: 10),
                              Container(
                                width: 200,
                                height: 15,
                                decoration: BoxDecoration(
                                  color: Colors.amber,
                                  borderRadius: BorderRadius.circular(4),
                                ),
                              )
                            ],
                          )
                        ],
                      ));
                }
              }),
            ),
            // contentAppBar,
            Expanded(
              child: DefaultTabController(
                  length: 2,
                  child: Scaffold(
                    appBar: PreferredSize(
                      preferredSize: const Size.fromHeight(kToolbarHeight),
                      child: Container(
                        color: AppColor.kPrimaryColor,
                        child: SafeArea(
                          child: Column(
                            children: <Widget>[
                              const Expanded(child: SizedBox()),
                              TabBar(
                                indicatorColor: AppColor.kInactiveColor,
                                unselectedLabelStyle:
                                    BaseTextStyle.customTextStyle(
                                  kfontWeight: FontWeight.w400,
                                  kfontSize: 12,
                                ),
                                unselectedLabelColor: AppColor.kWhiteColor,
                                labelColor: AppColor.kInactiveColor,
                                labelStyle: BaseTextStyle.customTextStyle(
                                  kfontWeight: FontWeight.w700,
                                  kfontSize: 14,
                                ),
                                tabs: const [
                                  Tab(text: 'Info'),
                                  Tab(text: 'Realisasi'),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    body: TabBarView(
                      children: [
                        Obx(() => segmentTab1(
                            data: productionControllers.viewProductionScedule,
                            isLoading: productionControllers.isLoading.value,
                            context: context,
                            controllers: productionControllers)),
                        Obx(
                          () => segmentTab2(
                            data: productionControllers.datalookupProduction,
                          ),
                        )
                      ],
                    ),
                  )),
            )
          ],
        ),
      ),
    );
  }

  Widget segmentTab1({
    required BuildContext context,
    required ViewScheduleControllers controllers,
    required dynamic data,
    bool isLoading = true,
  }) {
    var dataMixDesign = data['mix_design'];

    Future<void> showAdjustVolume(BuildContext context) {
      return showModalBottomSheet(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(15.0))),
        backgroundColor: AppColor.kWhiteColor,
        // backgroundColor: Colors.transparent,
        context: context,
        isScrollControlled: true,
        builder: (context) => Padding(
          padding: EdgeInsets.only(
              top: 5,
              right: 10,
              left: 10,
              bottom: MediaQuery.of(context).viewInsets.bottom),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  BaseTextWidget.customText(
                    text: "Tambah Volume",
                    color: AppColor.kPrimaryColor,
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                  IconButton.filledTonal(
                    iconSize: 18,
                    onPressed: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      Get.back();
                    },
                    icon: const Icon(Icons.close),
                  ),
                ],
              ),
              FormBuilder(
                key: controllers.viewScheduleFormKey,
                onChanged: () {
                  controllers.viewScheduleFormKey.currentState!.save();
                },
                autovalidateMode: AutovalidateMode.disabled,
                skipDisabled: true,
                child: Flexible(
                  child: ListView(
                    shrinkWrap: true,
                    physics: const AlwaysScrollableScrollPhysics(),
                    children: [
                      SizedBox(
                        width: double.infinity,
                        child: BaseExtendWidget.detailContent(
                          titleColor: AppColor.kGreyColor,
                          isLoading: isLoading,
                          data: [
                            {
                              'header': 'No.Order',
                              'content': data['order_number'],
                            },
                            {
                              'header': 'Tgl.Produksi',
                              'content': DateTime.parse(data['production_date'])
                                  .toFormattedDateTime(),
                            },
                            {
                              'header': 'Pelanggan',
                              'content': data['customer_name'],
                            },
                            {
                              'header': 'Proyek',
                              'content': data['customer_project_name'],
                            },
                            {
                              'header': 'Status',
                              'content': Container(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 5,
                                    vertical: 2,
                                  ),
                                  margin: const EdgeInsets.only(right: 5),
                                  decoration: BoxDecoration(
                                    color: AppColor.kBadgeProfressColor,
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                  child: BaseTextWidget.customText(
                                    text: data['status_code'],
                                    color: AppColor.kProgressColor,
                                  )),
                            },
                          ],
                        ),
                      ),
                      BaseTextFieldWidget.textFieldFormBuilder2(
                        title: 'Volume',
                        hintText: "25.00",
                        fieldName: 'quantity',
                        isRequired: true,
                        onClearText: () => controllers
                            .viewScheduleFormKey.currentState!
                            .patchValue({'quantity': null}),
                        onChange: ({value}) {},
                        extendSuffixItem: Container(
                          margin: const EdgeInsets.all(1),
                          padding: const EdgeInsets.all(14),
                          decoration: const BoxDecoration(
                            color: AppColor.kGreyColor,
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(7),
                              bottomRight: Radius.circular(7),
                            ),
                          ),
                          child: Text(
                            "M3",
                            style: BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kPrimaryColor,
                              kfontWeight: FontWeight.w500,
                              kfontSize: 15,
                            ),
                          ),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'volume produk wajib diisi!';
                          }

                          if (double.parse(value) < 0.25) {
                            return 'volume (M3) tidak boleh kurang dari 0.25';
                          }

                          return null;
                        },
                        keyboardType: const TextInputType.numberWithOptions(
                            decimal: true),
                        textInputAction: TextInputAction.done,
                        inputFormater: <TextInputFormatter>[
                          FilteringTextInputFormatter.allow(
                              RegExp(r'^\d*\.?\d*')),
                        ],
                      ),
                      BaseTextFieldWidget.textFieldFormBuilder2(
                        title: 'Alasan',
                        fieldName: 'description',
                        isLongText: true,
                        hintText: "Ketik alasan disini...",
                        // isRequired: true,
                        onClearText: () => controllers
                            .viewScheduleFormKey.currentState!
                            .patchValue({'description': null}),
                        validator: (value) {
                          // if (value == null || value.isEmpty) {
                          //   return 'alasan wajib diisi!';
                          // }
                          return null;
                        },
                        keyboardType: TextInputType.multiline,
                        textInputAction: TextInputAction.done,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Obx(
                () => BaseButtonWidget.primaryIconButton(
                  onPressed: () => controllers.adujsVolume(
                      loadingCtx: context, extendData: {'id': data['id']}),
                  kcolor: AppColor.kPrimaryColor,
                  width: double.infinity,
                  isLoading: controllers.isLoading.value,
                  height: 50,
                  rounded: 10,
                  iconData: Icons.autorenew_rounded,
                  textStyle: BaseTextStyle.customTextStyle(
                    kcolor: AppColor.kWhiteColor,
                    kfontSize: 12,
                  ),
                  title: "Tambah Volume",
                ),
              ),
              const SizedBox(height: 10),
            ],
          ),
        ),
      );
    }

    Future<void> showCancelJadwal(BuildContext context) {
      return showModalBottomSheet(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(15.0))),
        backgroundColor: AppColor.kWhiteColor,
        // backgroundColor: Colors.transparent,
        context: context,
        isScrollControlled: true,
        builder: (context) => Padding(
          padding: EdgeInsets.only(
              top: 5,
              right: 10,
              left: 10,
              bottom: MediaQuery.of(context).viewInsets.bottom),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  BaseTextWidget.customText(
                    text: "Batalkan Jadwal",
                    color: AppColor.kPrimaryColor,
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                  IconButton.filledTonal(
                    iconSize: 18,
                    onPressed: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      Get.back();
                    },
                    icon: const Icon(Icons.close),
                  ),
                ],
              ),
              FormBuilder(
                key: controllers.viewScheduleFormKey,
                onChanged: () {
                  controllers.viewScheduleFormKey.currentState!.save();
                },
                autovalidateMode: AutovalidateMode.disabled,
                skipDisabled: true,
                child: Flexible(
                  child: ListView(
                    shrinkWrap: true,
                    physics: const AlwaysScrollableScrollPhysics(),
                    children: [
                      SizedBox(
                        width: double.infinity,
                        child: BaseExtendWidget.detailContent(
                          titleColor: AppColor.kGreyColor,
                          isLoading: isLoading,
                          data: [
                            {
                              'header': 'No.Order',
                              'content': data['order_number'],
                            },
                            {
                              'header': 'Tgl.Produksi',
                              'content': DateTime.parse(data['production_date'])
                                  .toFormattedDateTime(),
                            },
                            {
                              'header': 'Pelanggan',
                              'content': data['customer_name'],
                            },
                            {
                              'header': 'Proyek',
                              'content': data['customer_project_name'],
                            },
                            {
                              'header': 'Status',
                              'content': Container(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 5,
                                    vertical: 2,
                                  ),
                                  margin: const EdgeInsets.only(right: 5),
                                  decoration: BoxDecoration(
                                    color: AppColor.kBadgeProfressColor,
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                  child: BaseTextWidget.customText(
                                    text: data['status_code'],
                                    color: AppColor.kProgressColor,
                                  )),
                            },
                          ],
                        ),
                      ),
                      BaseTextFieldWidget.textFieldFormBuilder2(
                        title: 'Alasan',
                        fieldName: 'description',
                        isLongText: true,
                        hintText: "Ketik alasan disini...",
                        // isRequired: true,
                        onClearText: () => controllers
                            .viewScheduleFormKey.currentState!
                            .patchValue({'description': null}),
                        validator: (value) {
                          // if (value == null || value.isEmpty) {
                          //   return 'alasan wajib diisi!';
                          // }
                          return null;
                        },
                        keyboardType: TextInputType.multiline,
                        textInputAction: TextInputAction.done,
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Obx(
                () => Button.filled(
                  onPressed: () => controllers.cancelJadwal(
                      loadingCtx: context, extendData: {'id': data['id']}),
                  color: AppColor.kErrorColor,
                  width: double.infinity,
                  isLoading: controllers.isLoading.value,
                  height: 50,
                  borderRadius: 5,
                  label: "Batalakan",
                ),
              ),
              const SizedBox(height: 10),
            ],
          ),
        ),
      );
    }

    return data.isNotEmpty
        ? Stack(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
                child: ListView(
                  physics: const AlwaysScrollableScrollPhysics(),
                  children: [
                    BaseCardWidget.detailInfoCard2(
                      isLoading: isLoading,
                      contentList: [
                        {
                          'title': 'Tgl.Produksi',
                          'content': BaseTextWidget.customText(
                            text: BaseGlobalFuntion.datetimeConvert(
                                data['production_date'], "dd MMM yyyy HH:mm"),
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.calendar_today_rounded,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'Dijadwalkan',
                          'content': BaseTextWidget.customText(
                            text: data['quantity'],
                            extendText: " ${data['uom_name']}",
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.call_to_action_outlined,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'Produk/Mutu',
                          'content': BaseTextWidget.customText(
                            text:
                                "${data['product_name']}\n${data['product_code']}",
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.dataset,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'Unit Produksi',
                          'content': BaseTextWidget.customText(
                            text: data['production_unit_name'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          'icon': Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(
                                  color: AppColor.kGreyColor,
                                ),
                              ),
                              child: const Icon(
                                Icons.factory_rounded,
                                size: 20,
                                color: AppColor.kPrimaryColor,
                              ))
                        },
                        {
                          'title': 'Pelanggan',
                          'content': BaseTextWidget.customText(
                            text: data['customer_name'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.person_4_sharp,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ),
                          )
                        },
                        {
                          'title': 'Proyek',
                          'content': BaseTextWidget.customText(
                            text: data['customer_project_name'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 35,
                          ),
                          'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.location_city_rounded,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ),
                          )
                        },
                        {
                          'title': 'Alamat Proyek',
                          'content': BaseTextWidget.customText(
                            text: data['customer_project_address'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 30,
                            isLongText: true,
                          ),
                          'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.location_on_sharp,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ),
                          )
                        },
                        {
                          'title': 'Tipe Pengecoran',
                          'content': BaseTextWidget.customText(
                            text: data['casting_category_name'],
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 45,
                          ),
                          'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.type_specimen_outlined,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ),
                          )
                        },
                        {
                          'title': 'Ditambahkan',
                          'content': BaseTextWidget.customText(
                            text:
                                "${data['created_by_name']}\n${BaseGlobalFuntion.datetimeConvert(data['created_at'], "dd MMM yyyy HH:mm")}",
                            color: AppColor.kBlackColor,
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            maxLengthText: 45,
                          ),
                          'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.add_home,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ),
                          )
                        },
                        // ...{
                        //   if (data['status_code'] == 'CANCEL')
                        //     {
                        //       'title': 'Dibatalkan',
                        //       'content': BaseTextWidget.customText(
                        //         text:
                        //             "${BaseGlobalFuntion.datetimeConvert(data['updated_at'], "dd MMM yyyy HH:mm")}",
                        //         color: AppColor.kBlackColor,
                        //         fontSize: 13,
                        //         fontWeight: FontWeight.w500,
                        //         maxLengthText: 45,
                        //       ),
                        //       'icon': Container(
                        //         padding: const EdgeInsets.all(5),
                        //         decoration: BoxDecoration(
                        //           borderRadius: BorderRadius.circular(5),
                        //           border: Border.all(
                        //             color: AppColor.kGreyColor,
                        //           ),
                        //         ),
                        //         child: const Icon(
                        //           Icons.free_cancellation_rounded,
                        //           size: 20,
                        //           color: AppColor.kErrorColor,
                        //         ),
                        //       )
                        //     },
                        // },
                        ...{
                          if (data['status_code'] == 'PENDING' &&
                              LocalDataSource.getLocalVariable(
                                key: 'permissions',
                              ).contains('cancel-production-schedule'))
                            {
                              'content': Button.outlined(
                                onPressed: () => showCancelJadwal(context),
                                color: AppColor.kErrorColor,
                                width: 180,
                                height: 30,
                                fontSize: 12,
                                borderRadius: 5,
                                label: "Batalkan Volume",
                              ),
                              'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: AppColor.kGreyColor,
                                  ),
                                ),
                                child: const Icon(
                                  Icons.cancel_rounded,
                                  size: 20,
                                  color: AppColor.kErrorColor,
                                ),
                              )
                            },
                        }
                      ],
                    ),
                    const SizedBox(height: 5),
                    BaseCardWidget.detailInfoCard2(
                      isLoading: isLoading,
                      contentList: [
                        ...List.generate(data['transaction_log'].length,
                            (index) {
                          final dataTransaction =
                              data['transaction_log'][index];

                          dynamic statusTransaction({
                            required onReject,
                            required onApprove,
                            required onProgress,
                          }) {
                            return dataTransaction['status_code'] == 'CANCEL'
                                ? onReject
                                : dataTransaction['status_code'] == 'SUCCESS'
                                    ? onApprove
                                    : onProgress;
                          }

                          return {
                            'title': statusTransaction(
                              onApprove: "Diselesaikan",
                              onReject: "Dibatalkan",
                              onProgress: "Adjust Volume",
                            ),
                            'content': Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    BaseTextWidget.customText(
                                      text: DateTime.parse(
                                              dataTransaction['processed_at'] ??
                                                  "")
                                          .toFormattedDateTime(),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 2),
                                const Divider(),
                                Row(
                                  children: [
                                    const Icon(
                                      Icons.person_3_rounded,
                                      size: 15,
                                      color: AppColor.kPrimaryColor,
                                    ),
                                    const SizedBox(width: 3),
                                    BaseTextWidget.customText(
                                      text: dataTransaction['processed_by'],
                                      color: AppColor.kBlackColor,
                                      fontSize: 13,
                                      fontWeight: FontWeight.w500,
                                      isLongText: true,
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 3),
                                BaseTextWidget.customText(
                                  text: dataTransaction['notes'],
                                  color: AppColor.kBlackColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                  isLongText: true,
                                ),
                              ],
                            ),
                            'icon': Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: statusTransaction(
                                      onApprove: AppColor.kSuccesColor,
                                      onReject: AppColor.kErrorColor,
                                      onProgress: AppColor.kYellowColor,
                                    ),
                                  ),
                                ),
                                child: Icon(
                                  statusTransaction(
                                    onApprove:
                                        Icons.playlist_add_check_circle_rounded,
                                    onReject:
                                        Icons.closed_caption_disabled_rounded,
                                    onProgress: Icons.approval_rounded,
                                  ),
                                  size: 20,
                                  color: statusTransaction(
                                    onApprove: AppColor.kSuccesColor,
                                    onReject: AppColor.kErrorColor,
                                    onProgress: AppColor.kYellowColor,
                                  ),
                                ))
                          };
                        }),
                      ],
                    ),
                    const SizedBox(height: 3),
                    BaseCardWidget.detailInfoCard(
                      isLoading: isLoading,
                      title: "Komposisi",
                      contentList: dataMixDesign
                          .map((item) => {
                                'title': "${item['material_name']}",
                                'content':
                                    "${item['weight'].toStringAsFixed(2)} Kg",
                              })
                          .toList(),
                    ),
                    const SizedBox(height: 120),
                  ],
                ),
              ),
              if (data['status_code'] != 'CANCEL' &&
                  data['status_code'] != 'SUCCESS')
                Positioned(
                  bottom: 30.0,
                  right: 15.0,
                  child: SizedBox(
                    height: 45.0,
                    child: BaseButtonWidget.primaryIconButton(
                      onPressed: () => showAdjustVolume(context),
                      kcolor: AppColor.kPrimaryColor,
                      width: 180,
                      rounded: 10,
                      iconData: Icons.add_circle,
                      textStyle: BaseTextStyle.customTextStyle(
                        kfontWeight: FontWeight.w600,
                        kfontSize: 12,
                        kcolor: AppColor.kWhiteColor,
                      ),
                      title: "Tambah Volume",
                    ),
                  ),
                ),
            ],
          )
        : BaseCardWidget.detailInfoCard(
            isLoading: true,
            title: "Informasi Rencana Produksi",
            contentList: [],
          );
  }

  Widget segmentTab2({dynamic data}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
      child: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 0),
              child: data.length > 0
                  ? ListView.separated(
                      shrinkWrap: true,
                      itemCount: data.length,
                      separatorBuilder: (BuildContext context, int index) =>
                          const SizedBox(height: 2),
                      itemBuilder: (BuildContext context, int index) {
                        return Obx(() {
                          return BaseCardWidget.cardStyle1(
                              ontap: () {
                                Get.to(
                                  () =>
                                      ProductionView(itemId: data[index]['id']),
                                );
                              },
                              title: "${data[index]['docket_number']}",
                              subTitle1:
                                  "${BaseGlobalFuntion.datetimeConvert(data[index]['production_date'], 'dd MMM yyyy HH:mm')}",
                              trailing: BaseTextWidget.customText(
                                text: data[index]['quantity'],
                                extendText: '\n${data[index]['uom_name']}',
                                textAlign: TextAlign.center,
                                fontWeight: FontWeight.w700,
                                color: AppColor.kSuccesColor,
                              ),
                              subTitle2: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  BaseTextWidget.customText(
                                    text: data[index]['driver_name'],
                                    textAlign: TextAlign.center,
                                    fontWeight: FontWeight.w600,
                                  ),
                                  BaseTextWidget.customText(
                                    text: data[index]['vehicle_name'],
                                    textAlign: TextAlign.center,
                                    fontWeight: FontWeight.w600,
                                    color: AppColor.kGreyColor,
                                  ),
                                ],
                              ));
                        });
                      },
                    )
                  : BaseLoadingWidget.noMoreDataWidget(),
            ),
          ),
        ],
      ),
    );
  }
}
