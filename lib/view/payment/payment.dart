import 'package:rbs_mobile_operation/data/datasources/payment.contollers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/core/components/base_button_widget.dart';
import 'package:rbs_mobile_operation/core/components/base_loading_widget.dart';

class PaymentHistoryList extends StatelessWidget {
  const PaymentHistoryList({super.key});

  @override
  Widget build(BuildContext context) {
    PaymentControllers paymentControllers = Get.put(PaymentControllers());

    Future onRefresh() async {
      paymentControllers.refreshDataPayment();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        if (paymentControllers.paymentData.isNotEmpty) {
          paymentControllers.fetchPayment();
        }
      }
    });

    // ** SHOW FILTER MODDAL SHEET
    final isActive = ['Semua', 'Aktif', 'Non Aktif'];
    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          paymentControllers.currentFilter.clear();
          paymentControllers.filterFormKey.currentState!
              .patchValue({'active': null, 'customer': null});
        },
        onFilter: () {
          Get.back();
          paymentControllers.refreshDataPayment();
        },
        valueFilter: paymentControllers.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: paymentControllers.filterFormKey,
            onChanged: () {
              paymentControllers.filterFormKey.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  BaseButtonWidget.dropdownButton2(
                    fieldName: "customer",
                    valueController: TextEditingController(),
                    valueData: paymentControllers.dataLookupCustomer,
                    label: "Pelanggan",
                    hintText:
                        paymentControllers.currentFilter['customer_filter'] ??
                            "Pilih Pelanggan",
                    searchOnChange: (p0) {
                      // BaseGlobalFuntion.debounceRun(
                      //   action: () {
                      //     // print(p0);
                      //     productionControllers.dataLookupCustomer.clear();
                      //     productionControllers.lookupCustomer(searching: p0);
                      //   },
                      //   duration: const Duration(
                      //     milliseconds: 500,
                      //   ),
                      // );
                    },
                    onChange: ({searchValue, valueItem}) {
                      paymentControllers.currentFilter.value = {
                        ...paymentControllers.currentFilter,
                        'customer_filter': searchValue,
                      };
                    },
                  ),
                  const SizedBox(height: 10),
                  FormBuilderChoiceChip<String>(
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                      labelText: 'Status',
                      hintText: "Pilih Status",
                      labelStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 14,
                        kfontWeight: FontWeight.w500,
                      ),
                      errorStyle: BaseTextStyle.customTextStyle(
                        kfontSize: 13,
                        kcolor: AppColor.kFailedColor,
                        kfontWeight: FontWeight.w500,
                      ),
                    ),
                    name: 'active',
                    initialValue: paymentControllers.currentFilter['active'],
                    selectedColor: AppColor.kInactiveColor,
                    spacing: 6.0,
                    options: List.generate(isActive.length, (index) {
                      return FormBuilderChipOption(
                        value: isActive[index],
                        child: Text(
                          isActive[index],
                          style: BaseTextStyle.customTextStyle(
                            kcolor: AppColor.kPrimaryColor,
                            kfontWeight: FontWeight.w600,
                            kfontSize: 12,
                          ),
                        ),
                      );
                    }),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    // ** LIST OF CONTENT CARD
    final listItemContent = Flexible(child: Obx(() {
      final result = paymentControllers.paymentData;
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: result.isEmpty && paymentControllers.isLoading.isFalse
            ? SizedBox(
                height: MediaQuery.of(context).size.height / 1.2,
                child: Center(
                  child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                ),
              )
            : ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: result.length + 1,
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(height: 10),
                itemBuilder: (BuildContext context, int index) {
                  if (index < result.length) {
                    final data = result[index];
                    var colorBadge =
                        BaseGlobalFuntion.statusColor(data['active']);
                    return BaseCardWidget.cardStyle2(
                      title: data['customer_name'] ?? "",
                      trailing: "${data['code']}",
                      subTitle1:
                          paymentControllers.paymentData[index]['name'] ?? "",
                      subTitle2: BaseGlobalFuntion.convertTextLong(
                          text: data['address'] ?? '', maxLength: 25),
                      headerString: "${data['customer_category_name']}",
                      statusHeader: {
                        'title': data['active'] == "1" ? 'Aktif' : 'Non Aktif',
                        'color': colorBadge['background_color'],
                        'titleColor': colorBadge['text_color'],
                      },
                      badgeTag: data['access_payment'] != null
                          ? List.generate(
                              data['access_payment'].length,
                              (index) => Container(
                                margin: const EdgeInsets.only(right: 10),
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(6),
                                  boxShadow: BaseShadowStyle.customBoxshadow,
                                  color: AppColor.kWhiteColor,
                                ),
                                child: Text(
                                  data['access_payment'][index]['name'],
                                  style: BaseTextStyle.customTextStyle(
                                    kfontSize: 11,
                                    kfontWeight: FontWeight.w600,
                                  ),
                                ),
                              ),
                            )
                          : null,
                    );
                  } else if (result.length < 25) {
                    return paymentControllers.hasMore.value &&
                            paymentControllers.isLoading.isTrue
                        ? BaseLoadingWidget.cardShimmer(
                            height: MediaQuery.of(context).size.height,
                            shimmerheight: 80,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 25,
                          )
                        : const SizedBox();
                  } else {
                    return paymentControllers.hasMore.value
                        ? BaseLoadingWidget.cardShimmer(
                            height: 200,
                            shimmerheight: 90,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 2,
                          )
                        : const SizedBox();
                  }
                },
              ),
      );
    }));

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
            title: 'Riwayat Pembayaran',
            context: context,
            isAllFill: true,
            searchController: paymentControllers.search,
            searchText: 'Cari',
            onFillterChange: () {
              showBottomFilter(context);
            },
            onSearchChange: () {
              BaseGlobalFuntion.debounceRun(
                action: () {
                  paymentControllers.refreshDataPayment();
                },
                duration: const Duration(milliseconds: 1000),
              );
            }),
        body: Stack(
          children: [
            RefreshIndicator(
              onRefresh: onRefresh,
              child: SingleChildScrollView(
                controller: scrollController,
                physics: const AlwaysScrollableScrollPhysics(),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    listItemContent,
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
