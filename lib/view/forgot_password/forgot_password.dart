import 'package:rbs_mobile_operation/data/datasources/forgotpwd.controllers.dart';
import 'package:rbs_mobile_operation/core/extentions/animation_fade_in_slide.dart';
import 'package:rbs_mobile_operation/core/core.dart';

class ForgotPassword extends StatelessWidget {
  const ForgotPassword({super.key});

  @override
  Widget build(BuildContext context) {
    ForgotPwdControllers forgotPwdControllers = Get.put(ForgotPwdControllers());

    final forgorPassworBody = FadeInSlide(
      duration: 1.2,
      child: Column(
        children: [
          FormBuilder(
            key: forgotPwdControllers.fkForgotPwd,
            onChanged: () {
              forgotPwdControllers.fkForgotPwd.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: CustomTextField.style2(
              title: 'Email',
              isRequired: true,
              fieldName: 'email',
              hintText: "Masukan email",
              onClearText: () => forgotPwdControllers.fkForgotPwd.currentState!
                  .patchValue({'email': null}),
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'email harus diisi!';
                }

                if (value.isNotEmpty) {
                  if (!value.contains('@')) {
                    return 'Alamat email tidak sesuai!';
                  }
                }
                return null;
              },
              onChange: ({value}) {},
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.done,
            ),
          ),
        ],
      ),
    );

    final submitAction = Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Obx(
          () => Button.filled(
            onPressed: () =>
                forgotPwdControllers.sendEmailVerification(loadingCtx: context),
            // isDisable: salesController.cart.isNotEmpty ? false : true,
            disabled: forgotPwdControllers.isLoading.value,
            color: AppColor.kPrimaryColor,
            // height: 60,
            icon: const Icon(
              Icons.password_sharp,
              color: AppColor.kWhiteColor,
            ),
            // textStyle: BaseTextStyle.customTextStyle(
            //   kfontWeight: FontWeight.w600,
            //   kfontSize: 12,
            //   kcolor: AppColor.kWhiteColor,
            // ),
            label: "Kirim Email",
          ),
        ),
      ),
    );

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        title: 'Lupa Password',
        context: context,
        extendBack: () {},
      ),
      body: Container(
        decoration: const BoxDecoration(
          color: AppColor.kPrimaryColor,
        ),
        child: Column(
          children: [
            Expanded(
              child: Container(
                child: Assets.images.forgotPwd.svg(height: 200),
              ),
            ),
            // this will be you container
            Container(
              padding: const EdgeInsets.all(15.0),
              decoration: const BoxDecoration(
                color: AppColor.kWhiteColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10),
                  topRight: Radius.circular(10),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BaseTextWidget.customText(
                    text: "Kirim Email Verifikasi",
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    isLongText: true,
                  ),
                  BaseTextWidget.customText(
                    text: "Anda akan mendapatkan email untuk memulihkan akun",
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    isLongText: true,
                  ),
                  const SizedBox(height: 5),
                  forgorPassworBody,
                ],
              ),
            )
          ],
        ),
      ),

      // Container(
      //   decoration: const BoxDecoration(
      //     color: AppColor.kPrimaryColor,
      //   ),
      //   child: Column(
      //     mainAxisAlignment: MainAxisAlignment.end,
      //     children: [
      //       Assets.images.forgotPwd.svg(height: 200),
      //       const SizedBox(height: 20),
      //       Container(
      //         padding: const EdgeInsets.all(15.0),
      //         decoration: const BoxDecoration(
      //           color: AppColor.kWhiteColor,
      //           borderRadius: BorderRadius.only(
      //             topLeft: Radius.circular(10),
      //             topRight: Radius.circular(10),
      //           ),
      //         ),
      //         child: Column(
      //           crossAxisAlignment: CrossAxisAlignment.start,
      //           children: [
      //             BaseTextWidget.customText(
      //               text: "Kirim Email Verifikasi",
      //               fontSize: 15,
      //               fontWeight: FontWeight.bold,
      //               isLongText: true,
      //             ),
      //             BaseTextWidget.customText(
      //               text: "Anda akan mendapatkan email untuk memulihkan akun",
      //               fontSize: 15,
      //               fontWeight: FontWeight.w500,
      //               isLongText: true,
      //             ),
      //             const SizedBox(height: 5),
      //             forgorPassworBody,
      //           ],
      //         ),
      //       )
      //     ],
      //   ),
      // ),
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: submitAction,
    );
  }
}
