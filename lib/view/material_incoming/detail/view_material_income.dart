import 'package:rbs_mobile_operation/data/datasources/materialincoming.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class IncomingMaterialView extends StatelessWidget {
  final int itemId;

  const IncomingMaterialView({
    required this.itemId,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    DetailMaterialIncomingControllers materialIncomigControllers =
        Get.put(DetailMaterialIncomingControllers());

    materialIncomigControllers.viewDetailIncomingMatrial(itemId);

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Detail Penerimaan Material',
          context: context,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: double.infinity,
                padding: const EdgeInsets.only(
                    right: 20, left: 20, top: 15, bottom: 50),
                decoration: const BoxDecoration(
                  color: AppColor.kPrimaryColor,
                ),
                child: Obx(() {
                  if (materialIncomigControllers
                      .detailIncomingMaterial.isNotEmpty) {
                    var data =
                        materialIncomigControllers.detailIncomingMaterial;

                    var colorBadge =
                        BaseGlobalFuntion.statusColor(data['status_code']);

                    return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Container(
                                padding: const EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                  color: AppColor.kWhiteColor,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: const Icon(
                                  Icons.info_rounded,
                                ),
                              ),
                              const SizedBox(width: 10),
                              Flexible(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    BaseTextWidget.customText(
                                      text: data['purchase_order_number'],
                                      color: AppColor.kWhiteColor,
                                      fontSize: 13,
                                      fontWeight: FontWeight.w700,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      isLongText: true,
                                    ),
                                    BaseTextWidget.customText(
                                      text: data['material_name'],
                                      color: AppColor.kWhiteColor,
                                      fontSize: 13,
                                      fontWeight: FontWeight.w500,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      isLongText: true,
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(width: 5)
                            ],
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 2, horizontal: 6),
                          decoration: BoxDecoration(
                            color: colorBadge['background_color'],
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Text(
                            "${data['status_code']}",
                            style: BaseTextStyle.customTextStyle(
                              kfontSize: 11,
                              kcolor: colorBadge['text_color'],
                            ),
                          ),
                        )
                      ],
                    );
                  } else {
                    return Shimmer.fromColors(
                        baseColor: AppColor.kSoftGreyColor,
                        highlightColor: AppColor.kWhiteColor,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 40,
                              height: 40,
                              decoration: BoxDecoration(
                                color: Colors.amber,
                                borderRadius: BorderRadius.circular(8),
                              ),
                            ),
                            const SizedBox(width: 10),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: 150,
                                  height: 15,
                                  decoration: BoxDecoration(
                                    color: Colors.amber,
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                ),
                                const SizedBox(height: 10),
                                Container(
                                  width: 200,
                                  height: 15,
                                  decoration: BoxDecoration(
                                    color: Colors.amber,
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                )
                              ],
                            )
                          ],
                        ));
                  }
                }),
              ),
              const SizedBox(height: 10),
              Obx(
                () => contentInfo(
                  data: materialIncomigControllers.detailIncomingMaterial,
                  isLoading: materialIncomigControllers.isLoading.value,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget contentInfo({
    dynamic data,
    bool isLoading = true,
  }) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Container(
        margin: const EdgeInsets.only(left: 8, right: 8),
        child: data.isNotEmpty
            ? Column(
                children: [
                  BaseCardWidget.detailInfoCard2(
                    isLoading: isLoading,
                    contentList: [
                      {
                        'title': 'Unit Produksi',
                        'content': BaseTextWidget.customText(
                          text: data['production_name'],
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.factory_rounded,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'Tanggal penerimaan',
                        'content': BaseTextWidget.customText(
                          text: BaseGlobalFuntion.datetimeConvert(
                              data['date'], "dd MMM yyyy HH:mm"),
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.calendar_month,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'No.Order',
                        'content': BaseTextWidget.customText(
                          text: data['purchase_order_number'],
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.numbers_rounded,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'No.Docket',
                        'content': BaseTextWidget.customText(
                          text: data['docket_number'],
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.numbers_rounded,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'Suplier',
                        'content': BaseTextWidget.customText(
                          text: data['vendor_name'],
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.all_out_rounded,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'Nama Material',
                        'content': Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            BaseTextWidget.customText(
                              text: data['material_name'],
                              color: AppColor.kBlackColor,
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              isLongText: true,
                            ),
                            BaseTextWidget.customText(
                                text: data['material_code'],
                                color: AppColor.kGreyColor,
                                fontSize: 13,
                                fontWeight: FontWeight.w600,
                                isLongText: true,
                                fontStyle: FontStyle.italic),
                          ],
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.align_horizontal_center_rounded,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'Volume',
                        'content': BaseTextWidget.customText(
                          text: data['quantity'],
                          extendText: data['uom_name'],
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.vertical_shades_sharp,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'Gudang/Lokasi Penyimpanan',
                        'content': BaseTextWidget.customText(
                          text: data['warehouse_name'],
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.warehouse,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'Sopir',
                        'content': BaseTextWidget.customText(
                          text: data['driver_name'],
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.person_2_rounded,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'No.Polisi',
                        'content': BaseTextWidget.customText(
                          text: data['fleet_number'],
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.fire_truck_rounded,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                      {
                        'title': 'Deskripsi',
                        'content': BaseTextWidget.customText(
                          text: data['description'],
                          color: AppColor.kBlackColor,
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                          maxLengthText: 35,
                        ),
                        'icon': Container(
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                color: AppColor.kGreyColor,
                              ),
                            ),
                            child: const Icon(
                              Icons.text_snippet_rounded,
                              size: 20,
                              color: AppColor.kPrimaryColor,
                            ))
                      },
                    ],
                  ),
                  // const SizedBox(height: 10),
                  // BaseCardWidget.detailInfoCard2(
                  //   isLoading: isLoading,
                  //   title: "Info Pengiriman",
                  //   contentList: [

                  //     // {
                  //     //   'title': 'Lampiran',
                  //     //   'content': Container(
                  //     //     height: 80,
                  //     //     width: 80,
                  //     //     decoration: BoxDecoration(
                  //     //         borderRadius: BorderRadius.circular(5)),
                  //     //     child: Image.network(
                  //     //       data['attachment'],
                  //     //       fit: BoxFit.cover,
                  //     //       height: 60,
                  //     //       width: 60,
                  //     //       loadingBuilder: (_, child, chunk) => chunk != null
                  //     //           ? Shimmer.fromColors(
                  //     //               baseColor: AppColor.kSoftGreyColor,
                  //     //               highlightColor: AppColor.kWhiteColor,
                  //     //               child: Container(
                  //     //                 height: 60,
                  //     //                 width: 60,
                  //     //                 decoration: BoxDecoration(
                  //     //                   color: Colors.amber,
                  //     //                   borderRadius: BorderRadius.circular(5),
                  //     //                 ),
                  //     //               ),
                  //     //             )
                  //     //           : child,
                  //     //     ),
                  //     //   ),
                  //     //   'icon': Container(
                  //     //       padding: const EdgeInsets.all(5),
                  //     //       decoration: BoxDecoration(
                  //     //         borderRadius: BorderRadius.circular(5),
                  //     //         border: Border.all(
                  //     //           color: AppColor.kGreyColor,
                  //     //         ),
                  //     //       ),
                  //     //       child: const Icon(
                  //     //         Icons.image,
                  //     //         size: 20,
                  //     //         color: AppColor.kPrimaryColor,
                  //     //       ))
                  //     // },
                  //   ],
                  // ),
                ],
              )
            : BaseCardWidget.detailInfoCard(
                isLoading: true,
                title: "Informasi Pelanggan & Proyek",
                contentList: [],
              ),
      ),
    );
  }
}
