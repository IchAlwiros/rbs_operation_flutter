import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/services.dart';
import 'package:rbs_mobile_operation/data/datasources/materialincoming.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/view/material_incoming/create/simpan_material_incoming.dart';

Size get deviceSize =>
    WidgetsBinding.instance.platformDispatcher.views.first.physicalSize /
    WidgetsBinding.instance.platformDispatcher.views.first.devicePixelRatio;

class AddMaterialIncoming extends StatelessWidget {
  final dynamic poId;

  const AddMaterialIncoming({required this.poId, super.key});

  @override
  Widget build(BuildContext context) {
    AddMaterialIncomingControllers materialIncomingControllers =
        Get.put(AddMaterialIncomingControllers());

    // Future.delayed(const Duration(milliseconds: 200), () {
    //   materialIncomingControllers.lookupPOdetail(poId: poId);
    //   // if (orderId != addProductionSchedule.bodySchedule['sales_order_id']) {
    //   //   // JIKA ID YANG SEBELUMNYA TIDAK SAMA & PADA CART ADA ISINYA
    //   //   // MAKA DATA CARTNYA DI HAPUS
    //   //   addProductionSchedule.bodySchedule.clear();
    //   //   addProductionSchedule.cartSchedule.clear();
    //   //   addProductionSchedule.summarySchedule.clear();
    //   // }
    //   // addProductionSchedule.lookupSODetail(
    //   //   soId: orderId,
    //   // );
    // });
    // .then(
    //   (value) => materialIncomingControllers.currentFilter({}),
    // );

    // padding: MediaQuery.of(context).viewInsets,
    final submitAction = Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Obx(() => Expanded(
                child: Directionality(
                  textDirection: TextDirection.rtl,
                  child: Button.filled(
                    onPressed: () {
                      Get.to(() => const SaveMaterial());
                    },
                    disabled:
                        materialIncomingControllers.cartMaterialPO.isEmpty,
                    // isLoading: materialIncomingControllers.isLoading.value,
                    color: AppColor.kPrimaryColor,
                    // height: 50,
                    // rightIcon: true,
                    icon: const Icon(
                      Icons.arrow_circle_right_rounded,
                      color: AppColor.kWhiteColor,
                    ),
                    // textStyle: BaseTextStyle.customTextStyle(
                    //   kfontWeight: FontWeight.w600,
                    //   kfontSize: 12,
                    //   kcolor: AppColor.kWhiteColor,
                    // ),
                    label: "Selanjutnya",
                  ),
                ),
              )),
        ],
      ),
    );

    final listCardContent = Flexible(child: Obx(() {
      var dataSODetail = materialIncomingControllers.dataPOdetail;

      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: dataSODetail.isEmpty ||
                materialIncomingControllers.isLoading.isTrue
            ? BaseLoadingWidget.cardShimmer(
                // height: MediaQuery.of(context).size.height,
                shimmerheight: 90,
                width: double.infinity,
                baseColor: AppColor.kSoftGreyColor,
                highlightColor: AppColor.kWhiteColor,
                itemCount: 15,
              )
            : ListView.separated(
                // controller: scrollController,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: dataSODetail.length,
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(height: 7),
                itemBuilder: (BuildContext listContext, int index) {
                  return BaseCardWidget.cardStyle4(
                      title: dataSODetail[index]['material_name'],
                      subtitle: dataSODetail[index]['material_code'],
                      ontap: () {
                        // ** MENGAMBIL MATERIAL CATEGORY ID DARI TIAP ITEM UNTUK MENDAPATKAN WAREHOUSE
                        materialIncomingControllers.currentFilter({
                          ...materialIncomingControllers.currentFilter,
                          'material_category_id': dataSODetail[index]
                              ['material_category_id'],
                        });
                        materialIncomingControllers.lookupWarehouse();

                        // ** MODAL BOTTOM MENGISI KEDALAM CART MATERIAL PO
                        final itemInfo = Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                BaseTextWidget.customText(
                                  text: dataSODetail[index]['material_name'],
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  isLongText: true,
                                ),
                              ],
                            ),
                            BaseTextWidget.customText(
                              text: dataSODetail[index]['material_code'],
                              fontSize: 14,
                              fontWeight: FontWeight.w500,
                              isLongText: true,
                            ),
                            const SizedBox(height: 5),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <dynamic>[
                                {
                                  'title': 'Vol. Order',
                                  'content': dataSODetail[index]['quantity'],
                                  'colors': AppColor.kPrimaryColor
                                },
                                {
                                  'title': 'Vol. Diterima',
                                  'content': dataSODetail[index]
                                      ['received_quantity'],
                                  'colors': AppColor.kSuccesColor
                                },
                                {
                                  'title': 'Vol. Sisa',
                                  'content': dataSODetail[index]
                                      ['remaining_quantity'],
                                  'colors': AppColor.kWarningColor
                                }
                              ]
                                  .map(
                                    (item) => Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        BaseTextWidget.customText(
                                            text: item['title']),
                                        Row(
                                          children: [
                                            Container(
                                              width: 10,
                                              height: 10,
                                              margin: const EdgeInsets.only(
                                                  right: 5),
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(19),
                                                color: item['colors'],
                                              ),
                                            ),
                                            BaseTextWidget.customText(
                                              text: item['content'],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  )
                                  .toList(),
                            ),
                            const Divider(),
                          ],
                        );

                        final formList = Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            FormBuilder(
                              key: materialIncomingControllers.fkMaterialCart,
                              onChanged: () {
                                materialIncomingControllers
                                    .fkMaterialCart.currentState!
                                    .save();
                              },
                              autovalidateMode: AutovalidateMode.disabled,
                              skipDisabled: true,
                              child: Flexible(
                                child: ListView(
                                  shrinkWrap: true,
                                  children: [
                                    BaseTextFieldWidget.textFieldFormBuilder2(
                                      title: 'Volume',
                                      hintText: "75.00",
                                      fieldName: 'quantity',
                                      isRequired: true,
                                      onClearText: () =>
                                          materialIncomingControllers
                                              .fkMaterialCart.currentState!
                                              .patchValue({'quantity': null}),
                                      onChange: ({value}) {},
                                      extendSuffixItem: Container(
                                        margin: const EdgeInsets.all(1.2),
                                        padding: const EdgeInsets.all(14),
                                        decoration: const BoxDecoration(
                                          color: AppColor.kPrimaryColor,
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(6),
                                              bottomRight: Radius.circular(6)),
                                        ),
                                        child: BaseTextWidget.customText(
                                          text: dataSODetail[index]['uom_name'],
                                          color: AppColor.kSoftBlueColor,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 15,
                                        ),
                                      ),
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Volume wajib diisi!';
                                        }

                                        if (double.parse(value) >
                                            dataSODetail[index]
                                                ['remaining_quantity']) {
                                          return 'Volume (${dataSODetail[index]['uom_name']}) tidak boleh lebih dari ${dataSODetail[index]['remaining_quantity'].toStringAsFixed(2)}';
                                        }

                                        return null;
                                      },
                                      keyboardType:
                                          const TextInputType.numberWithOptions(
                                              decimal: true),
                                      textInputAction: TextInputAction.next,
                                      inputFormater: <TextInputFormatter>[
                                        FilteringTextInputFormatter.allow(
                                            RegExp(r'^[0-9.]+$')),
                                      ],
                                    ),
                                    const SizedBox(height: 5),
                                    Obx(() => materialIncomingControllers
                                            .dataLookupWarehouse.isEmpty
                                        ? BaseTextFieldWidget.disableTextForm2(
                                            disabledText: 'Pilih gudang',
                                            label: "Gudang/Lokasi Penyimpanan",
                                            isRequired: true,
                                            validator: (value) {
                                              if (value == null ||
                                                  value.isEmpty) {
                                                return 'Gudang wajib diisi!';
                                              }
                                              return null;
                                            },
                                          )
                                        : BaseButtonWidget.dropdownSearch(
                                            fieldName: 'warehouse_id',
                                            controller:
                                                materialIncomingControllers
                                                    .warehouseController,
                                            isRequired: true,
                                            label: "Gudang/Lokasi Penyimpanan",
                                            hintText: "Pilih gudang",
                                            asyncItems: (String filter) async {
                                              materialIncomingControllers
                                                  .lookupWarehouse(
                                                      searching: filter);
                                              return materialIncomingControllers
                                                  .dataLookupWarehouse;
                                            },
                                            onChange: (
                                                {searchValue,
                                                valueItem,
                                                data}) {
                                              // stockTakeControllers.dataViewInventory.clear();
                                            },
                                            validator: (value) {
                                              if (value == null ||
                                                  value.isEmpty) {
                                                return 'Gudang wajib dipilih!';
                                              }
                                              return null;
                                            },
                                          )),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(height: 10),
                            Button.filled(
                              onPressed: () {
                                if (materialIncomingControllers.cartMaterialPO
                                        .any((item) =>
                                            item["material_id"] !=
                                            dataSODetail[index]
                                                ['material_id']) ||
                                    materialIncomingControllers
                                        .cartMaterialPO.isEmpty) {
                                  materialIncomingControllers
                                      .addMaterilPO(otherData: {
                                    'material_id': dataSODetail[index]
                                        ['material_id'],
                                    'material_name': dataSODetail[index]
                                        ['material_name'],
                                    'material_code': dataSODetail[index]
                                        ['material_code'],
                                    'uom_name': dataSODetail[index]['uom_name'],
                                  });
                                } else {
                                  BaseInfo.log(
                                    isError: true,
                                    messageTitle: "Something Wrong!",
                                    message: "Material tidak boleh sama",
                                    snackPosition: SnackPosition.TOP,
                                  );
                                }
                              },
                              color: AppColor.kPrimaryColor,
                              // height: 50,
                              // rounded: 10,
                              icon: const Icon(
                                Icons.add_shopping_cart_rounded,
                                color: AppColor.kWhiteColor,
                              ),
                              // textStyle: BaseTextStyle.customTextStyle(
                              //   kfontWeight: FontWeight.w600,
                              //   kfontSize: 12,
                              //   kcolor: AppColor.kWhiteColor,
                              // ),
                              width: double.infinity,
                              label: "Simpan Material",
                            ),
                            const SizedBox(height: 15),
                          ],
                        );

                        _showDialogBuilder(context, [
                          itemInfo,
                          formList,
                        ]);
                      },
                      bottomSubtitle: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <dynamic>[
                          {
                            'content': dataSODetail[index]['quantity'],
                            'colors': AppColor.kPrimaryColor
                          },
                          {
                            'content': dataSODetail[index]['received_quantity'],
                            'colors': AppColor.kSuccesColor
                          },
                          {
                            'content': dataSODetail[index]
                                ['remaining_quantity'],
                            'colors': AppColor.kWarningColor
                          }
                        ]
                            .map(
                              (item) => Row(
                                children: [
                                  Container(
                                    width: 10,
                                    height: 10,
                                    margin: const EdgeInsets.only(right: 5),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(19),
                                      color: item['colors'],
                                    ),
                                  ),
                                  BaseTextWidget.customText(
                                    text: item['content'],
                                  ),
                                ],
                              ),
                            )
                            .toList(),
                      ));
                },
              ),
      );
    }));

    final topContent = Container(
      // height: 80,
      width: double.infinity,
      padding: const EdgeInsets.only(right: 20, left: 20, top: 15, bottom: 50),
      decoration: const BoxDecoration(
        color: AppColor.kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(10),
          bottomRight: Radius.circular(10),
        ),
      ),
      child: Obx(() {
        var dataSO = materialIncomingControllers.dataPreview;

        // print(dataSO);

        return dataSO.isEmpty
            ? Shimmer.fromColors(
                baseColor: AppColor.kSoftGreyColor,
                highlightColor: AppColor.kWhiteColor,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                    const SizedBox(width: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 150,
                          height: 15,
                          decoration: BoxDecoration(
                            color: Colors.amber,
                            borderRadius: BorderRadius.circular(4),
                          ),
                        ),
                        const SizedBox(height: 10),
                        Container(
                          width: 200,
                          height: 15,
                          decoration: BoxDecoration(
                            color: Colors.amber,
                            borderRadius: BorderRadius.circular(4),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: AppColor.kWhiteColor,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: const Icon(
                            Icons.fire_truck_rounded,
                          ),
                        ),
                        const SizedBox(width: 15),
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              BaseTextWidget.customText(
                                text: dataSO['order_number'],
                                fontSize: 13,
                                color: AppColor.kWhiteColor,
                                fontWeight: FontWeight.w600,
                                isLongText: true,
                              ),
                              BaseTextWidget.customText(
                                text: dataSO['vendor_name'],
                                color: AppColor.kWhiteColor,
                                fontSize: 13,
                                fontWeight: FontWeight.w500,
                                isLongText: true,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              );
      }),
    );

    //  if (materialIncomingControllers.cartMaterialPO.isEmpty) {
    //   WidgetsBinding.instance.addPostFrameCallback((_) {
    //     showBottomAddCart(context, true);
    //   });
    // }

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
          title: 'Tambah Penerimaan Material',
          context: context,
          extendBack: () {
            materialIncomingControllers.dataPOdetail.clear();
            materialIncomingControllers.cartMaterialPO.clear();
            // salesController.dataLookupCustomerProject.clear();
            // salesController.dataDetailCustomer.clear();
          },
          extendIconButton: IconButton(
            onPressed: () {
              final itemInfo = Obx(() {
                final resultCart = materialIncomingControllers.cartMaterialPO;

                return Flexible(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10, vertical: 20),
                    child: materialIncomingControllers.cartMaterialPO.isEmpty
                        ? DottedBorder(
                            borderType: BorderType.RRect,
                            dashPattern: const [8, 8],
                            radius: const Radius.circular(8),
                            padding: const EdgeInsets.symmetric(
                              horizontal: 12,
                              vertical: 50,
                            ),
                            color: AppColor.kGreyColor,
                            strokeWidth: 2,
                            child: const Center(
                              child: Text(
                                'Belum ada material yang dipilih',
                                style: TextStyle(
                                  color: AppColor.kGreyColor,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          )
                        : ListView.separated(
                            shrinkWrap: true,
                            physics: const AlwaysScrollableScrollPhysics(),
                            itemCount: resultCart.length,
                            separatorBuilder: (context, index) =>
                                const SizedBox(height: 5),
                            itemBuilder: (context, index) {
                              // print(resultCart[index]['quantity']);
                              return ListTile(
                                  shape: RoundedRectangleBorder(
                                    side: const BorderSide(
                                      width: 1.5,
                                      color: AppColor.kGreyColor,
                                    ),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  title: BaseTextWidget.customText(
                                    text: resultCart[index]['material_name'],
                                    isLongText: true,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    color: AppColor.kPrimaryColor,
                                    fontWeight: FontWeight.w600,
                                  ),
                                  subtitle: BaseTextWidget.customText(
                                    text: num.parse(
                                        resultCart[index]['quantity']),
                                    extendText: " M3",
                                    textAlign: TextAlign.left,
                                    fontWeight: FontWeight.w600,
                                  ),
                                  trailing: IconButton.filledTonal(
                                    splashColor: AppColor.kErrorColor,
                                    onPressed: () {
                                      materialIncomingControllers
                                          .deleteMaterialPO(index);
                                      if (materialIncomingControllers
                                          .cartMaterialPO.isEmpty) {
                                        Get.back();
                                      }
                                    },
                                    icon: const Icon(
                                      Icons.delete,
                                      color: AppColor.kErrorColor,
                                    ),
                                    style: FilledButton.styleFrom(
                                      backgroundColor:
                                          AppColor.kBadgeFailedColor,
                                    ),
                                  ));

                              //    IconButton.filled(
                              //     onPressed: () {
                              //       materialIncomingControllers
                              //           .deleteMaterialPO(index);
                              //       if (materialIncomingControllers
                              //           .cartMaterialPO.isEmpty) {
                              //         Get.back();
                              //       }
                              //     },
                              //     icon: const Icon(
                              //       Icons.delete,
                              //       size: 15,
                              //       color: AppColor.kFailedColor,
                              //     ),
                              //   ),
                              // );
                            },
                          ),
                  ),
                );
              });
              _showDialogBuilder(context, [itemInfo]);
            },
            icon: const Icon(
              Icons.shopping_cart_rounded,
              color: AppColor.kWhiteColor,
            ),
          )),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            topContent,
            const SizedBox(height: 10),
            // detailPayment,
            listCardContent,
          ],
        ),
      ),
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: submitAction,
    );
  }

  Future<void> _showDialogBuilder(context, itemWidget) {
    // MediaQueryData mediaQueryData = MediaQuery.of(context);
    // late Size size;
    // size = deviceSize;
    return showModalBottomSheet<void>(
        context: context,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(10.0))),
        backgroundColor: AppColor.kWhiteColor,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return Padding(
            padding: EdgeInsets.only(
              top: 10,
              right: 10,
              left: 10,
              // bottom: size.height * .02,
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Detail Material",
                        style: BaseTextStyle.customTextStyle(
                          kfontSize: 14,
                          kfontWeight: FontWeight.w600,
                        ),
                      ),
                      IconButton.filledTonal(
                        onPressed: () {
                          Get.back();
                        },
                        icon: const Icon(Icons.close),
                      ),
                    ],
                  ),
                  const Divider(),
                  ...itemWidget,
                ],
              ),
            ),
          );
        });
  }
}
