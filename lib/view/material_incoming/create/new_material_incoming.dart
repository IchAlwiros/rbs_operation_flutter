import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rbs_mobile_operation/data/datasources/local_storage/local_storage_datasource.dart';
import 'package:rbs_mobile_operation/data/datasources/materialincoming.controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/global/apicall.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/material_incoming/create/add_material_incoming.dart';

class CollectMaterialIncoming extends StatelessWidget {
  const CollectMaterialIncoming({super.key});

  @override
  Widget build(BuildContext context) {
    AddMaterialIncomingControllers materialIncomingControllers =
        Get.put(AddMaterialIncomingControllers());

    Future onRefresh() async {
      materialIncomingControllers.refreshLookupPO();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        materialIncomingControllers.lookupPurchaseOrder();
      }
    });

    Future<void> showModalSelectPu(BuildContext context) {
      return showModalBottomSheet<void>(
          context: context,
          isDismissible: false,
          builder: (BuildContext context) {
            return PopScope(
              canPop: false,
              child: SizedBox(
                width: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 18,
                    vertical: 20,
                  ),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Pilih Unit Produksi",
                            style: BaseTextStyle.customTextStyle(
                              kcolor: AppColor.kPrimaryColor,
                              kfontSize: 15,
                              kfontWeight: FontWeight.w600,
                            ),
                          ),
                          IconButton.filledTonal(
                            onPressed: () {
                              Get.back();
                              Get.back();
                            },
                            icon: const Icon(Icons.close),
                          ),
                        ],
                      ),
                      // const SizedBox(height: 5),
                      Flexible(
                        child: Obx(() => materialIncomingControllers
                                .dataLookupPU.isEmpty
                            ? materialIncomingControllers.isLoading.value
                                ? const SpinKitThreeBounce(
                                    size: 40.0,
                                    color: AppColor.kPrimaryColor,
                                  )
                                : BaseLoadingWidget.noMoreDataWidget(
                                    height: 100)
                            : ListView.separated(
                                itemBuilder: (context, index) {
                                  var resultPu =
                                      materialIncomingControllers.dataLookupPU;
                                  return ListTile(
                                    onTap: () {
                                      //** ADDED VALUE SELECTION
                                      materialIncomingControllers
                                          .productionUnit.value = {
                                        'production_unit_id': resultPu[index]
                                            ['id'],
                                        'production_unit_name': resultPu[index]
                                            ['name'],
                                      };
                                      materialIncomingControllers
                                          .currentFilter({
                                        "production_unit_id": resultPu[index]
                                            ['id']
                                      });

                                      //** RELOAD DATA
                                      materialIncomingControllers.dataPO
                                          .clear();
                                      materialIncomingControllers
                                          .refreshLookupPO();

                                      //** BACK
                                      Get.back();
                                    },
                                    title: BaseTextWidget.customText(
                                      text: resultPu[index]['name'],
                                    ),
                                  );
                                },
                                separatorBuilder: (context, index) =>
                                    const Divider(),
                                itemCount: materialIncomingControllers
                                    .dataLookupPU.length,
                              )),
                      ),
                    ],
                  ),
                ),
              ),
            );
          });
    }

    final listCardContent = Flexible(
      child: Obx(() {
        final resultPO = materialIncomingControllers.dataPO;

        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 0),
          child: RefreshIndicator(
            onRefresh: onRefresh,
            child: resultPO.isEmpty
                ? SingleChildScrollView(
                    physics: const AlwaysScrollableScrollPhysics(),
                    child: SizedBox(
                      height: MediaQuery.of(context).size.height / 1.2,
                      child: Center(
                        child: materialIncomingControllers.isLoading.isTrue
                            ? const SpinKitThreeBounce(
                                size: 30.0,
                                color: AppColor.kPrimaryColor,
                              )
                            : BaseLoadingWidget.noMoreDataWidget(height: 200),
                      ),
                    ),
                  )
                : ListView.separated(
                    controller: scrollController,
                    itemCount: resultPO.length + 1,
                    physics: const AlwaysScrollableScrollPhysics(),
                    separatorBuilder: (BuildContext context, int index) =>
                        const SizedBox(height: 7),
                    itemBuilder: (BuildContext context, int index) {
                      if (index < resultPO.length) {
                        final data = resultPO[index];
                        var colorBadge =
                            BaseGlobalFuntion.statusColor(data['status_code']);
                        return BaseCardWidget.cardStyle4(
                          ontap: () async {
                            // Get.to(
                            //   () => AddMaterialIncoming(
                            //     poId: data['id'],
                            //   ),
                            // );

                            await Future.delayed(
                                const Duration(microseconds: 500), () async {
                              await materialIncomingControllers.lookupPOdetail(
                                  poId: data['id']);

                              materialIncomingControllers.dataPreview({
                                'production_unit_id':
                                    data['production_unit_id'],
                                'production_unit_name':
                                    data['production_unit_name'],
                                'purchase_order_id': data['id'],
                                'vendor_id': data['vendor_id'],
                                'vendor_name': data['vendor_name'],
                                'order_number': data['order_number'],
                              });
                            }).then(
                              (value) => Get.to(
                                () => AddMaterialIncoming(
                                  poId: data['id'],
                                ),
                              ),
                            );
                          },
                          title: data['order_number'],
                          subtitle: data['vendor_name'],
                          trailling: Container(
                            padding: const EdgeInsets.all(4),
                            decoration: BoxDecoration(
                              color: colorBadge['background_color'],
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Text(
                              data['status_code'],
                              style: BaseTextStyle.customTextStyle(
                                kfontSize: 12,
                                kcolor: colorBadge['text_color'],
                                kfontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          bottomSubtitle: BaseTextWidget.customText(
                            text: data['production_unit_name'],
                            isLongText: true,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                          // extendWidget: Column(
                          //   crossAxisAlignment: CrossAxisAlignment.end,
                          //   children: [
                          //     BaseTextWidget.customText(
                          //       text: data['quantity'],
                          //       extendText: 'M3',
                          //       color: AppColor.kPrimaryColor,
                          //       fontWeight: FontWeight.w600,
                          //     ),
                          //   ],
                          // ),
                          // trailling: BaseTextWidget.customText(
                          //   text: BaseGlobalFuntion.datetimeConvert(
                          //       data['date'], "dd MMM yyyy"),
                          //   fontSize: 12,
                          //   color: AppColor.kSuccesColor,
                          //   fontWeight: FontWeight.w500,
                          // ),
                        );
                      } else if (resultPO.length < 25) {
                        return materialIncomingControllers.hasMore.value &&
                                materialIncomingControllers.isLoading.isTrue
                            ? BaseLoadingWidget.cardShimmer(
                                height: MediaQuery.of(context).size.height,
                                shimmerheight: 80,
                                baseColor: AppColor.kSoftGreyColor,
                                highlightColor: AppColor.kWhiteColor,
                                itemCount: 25,
                              )
                            : const SizedBox();
                      } else {
                        return materialIncomingControllers.hasMore.value
                            ? BaseLoadingWidget.cardShimmer(
                                height: 200,
                                shimmerheight: 90,
                                baseColor: AppColor.kSoftGreyColor,
                                highlightColor: AppColor.kWhiteColor,
                                itemCount: 2,
                              )
                            : const SizedBox();
                      }
                    },
                  ),
          ),
        );
      }),
    );

    final listFilter = Container(
      padding: const EdgeInsets.only(top: 10),
      child: Row(
        children: [
          Expanded(
            child: BaseTextFieldWidget.roundedStyleTextField(
              textEditingController:
                  materialIncomingControllers.searchPOController,
              onChange: (val) {
                BaseGlobalFuntion.debounceRun(
                  action: () {
                    materialIncomingControllers.refreshLookupPO();
                  },
                  duration: const Duration(milliseconds: 1000),
                );
              },
              suffixIcon: const Icon(Icons.search),
              radius: 10,
              height: 35,
              borderSide: const BorderSide(color: AppColor.kInactiveColor),
              hintText: 'Cari',
            ),
          ),
        ],
      ),
    );

    if (materialIncomingControllers.productionUnit.isEmpty) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (ApiService.productionUnitGlobal != null ||
            LocalDataSource.getLocalVariable(key: 'lastestProduction') !=
                null) {
          materialIncomingControllers.productionUnit.value = {
            'production_unit_id': ApiService.productionUnitGlobal?['id'] ??
                LocalDataSource.getLocalVariable(
                  key: 'lastestProduction',
                )?['id'],
            'production_unit_name': ApiService.productionUnitGlobal?['name'] ??
                LocalDataSource.getLocalVariable(
                  key: 'lastestProduction',
                )?['name'],
          };
        } else {
          showModalSelectPu(context);
        }
      });
    }

    return SafeArea(
      child: PopScope(
        canPop: true,
        child: Scaffold(
          appBar: BaseExtendWidget.appBar(
            title: 'Tambah Penerimaan Material',
            context: context,
            extendBack: () {
              // salesController.cart.clear();
              // salesController.cartSummary.clear();
            },
          ),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: Column(
              children: [
                listFilter,
                const SizedBox(height: 10),
                listCardContent,
              ],
            ),
          ),
          resizeToAvoidBottomInset: true,
          // bottomNavigationBar: submitAction,
        ),
      ),
    );
  }
}
