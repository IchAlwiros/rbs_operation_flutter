import 'package:flutter/services.dart';
import 'package:rbs_mobile_operation/core/components/components.dart';
import 'package:rbs_mobile_operation/data/datasources/global/image_upload_controllers.dart';
import 'package:rbs_mobile_operation/data/datasources/materialincoming.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class SaveMaterial extends StatelessWidget {
  const SaveMaterial({super.key});

  @override
  Widget build(BuildContext context) {
    AddMaterialIncomingControllers materialIncomingControllers =
        Get.put(AddMaterialIncomingControllers());

    ImageUploadControllers imageUploadControllers =
        Get.put(ImageUploadControllers());

    Future<void> _showDialogBuilder(BuildContext context, bool isSave) {
      return showModalBottomSheet<void>(
          context: context,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15),
            topRight: Radius.circular(15),
          )),
          builder: (BuildContext context) {
            return Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 18,
                vertical: 10,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Detail Penerimaan Material",
                        style: BaseTextStyle.customTextStyle(
                          kfontSize: 14,
                          kfontWeight: FontWeight.w600,
                          kcolor: AppColor.kPrimaryColor,
                        ),
                      ),
                      IconButton.filledTonal(
                        onPressed: () {
                          Get.back();
                        },
                        icon: const Icon(Icons.close),
                      ),
                    ],
                  ),
                  const Divider(),
                  BaseTextWidget.customText(
                    text:
                        materialIncomingControllers.dataPreview['vendor_name'],
                    fontWeight: FontWeight.w600,
                    fontSize: 14,
                  ),
                  BaseTextWidget.customText(
                    text:
                        materialIncomingControllers.dataPreview['order_number'],
                    fontWeight: FontWeight.w500,
                    fontSize: 13,
                  ),
                  const Divider(),
                  Obx(() {
                    return Flexible(
                      child: ListView.separated(
                        shrinkWrap: true,
                        physics: const AlwaysScrollableScrollPhysics(),
                        itemCount:
                            materialIncomingControllers.cartMaterialPO.length,
                        separatorBuilder: (context, index) =>
                            const SizedBox(height: 5),
                        itemBuilder: (context, index) {
                          var data = materialIncomingControllers.cartMaterialPO;

                          return ListTile(
                            // contentPadding: const EdgeInsets.symmetric(
                            //     horizontal: 10, vertical: 0),
                            shape: RoundedRectangleBorder(
                              side: const BorderSide(
                                width: 1.5,
                                color: AppColor.kGreyColor,
                              ),
                              borderRadius: BorderRadius.circular(8),
                            ),
                            title: BaseTextWidget.customText(
                              text: data[index]['material_name'],
                              fontSize: 12,
                              fontWeight: FontWeight.w600,
                            ),
                            subtitle: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                BaseTextWidget.customText(
                                  text: data[index]['material_code'],
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                ),
                                Row(
                                  children: [
                                    BaseTextWidget.customText(
                                      text:
                                          double.parse(data[index]['quantity']),
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    BaseTextWidget.customText(
                                      text: data[index]['uom_name'],
                                      fontSize: 12,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ],
                                )
                              ],
                            ),

                            trailing: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                BaseTextWidget.customText(
                                  text: double.parse(data[index]['quantity']),
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600,
                                ),
                                BaseTextWidget.customText(
                                  text: data[index]['uom_name'],
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600,
                                ),
                              ],
                            ),
                            // trailing: IconButton.filled(
                            //   splashColor: AppColor.kErrorColor,
                            //   onPressed: () {
                            //     materialIncomingControllers
                            //         .deleteMaterialPO(index);
                            //     if (materialIncomingControllers
                            //         .cartMaterialPO.isEmpty) {
                            //       // Get.back();
                            //       // Get.back();
                            //       Get.close(2);
                            //     }
                            //   },
                            //   icon: const Icon(
                            //     Icons.delete,
                            //     color: AppColor.kErrorColor,
                            //   ),
                            //   style: FilledButton.styleFrom(
                            //     backgroundColor:
                            //         AppColor.kBadgeFailedColor,
                            //   ),
                            // ),
                          );
                        },
                      ),
                    );
                  }),

                  // Container(
                  //         width: double.infinity,
                  //         padding: const EdgeInsets.symmetric(
                  //             horizontal: 5, vertical: 2),
                  //         decoration: BoxDecoration(
                  //           borderRadius: BorderRadius.circular(2),
                  //           border:
                  //               Border.all(color: AppColor.kGreyColor),
                  //         ),
                  //         child: Row(
                  //           mainAxisAlignment: MainAxisAlignment.spaceAround,
                  //           children: [
                  //             BaseTextWidget.customText(
                  //               text: "Tambahkan Material Untuk Melanjutkan",
                  //               fontSize: 13,
                  //               fontWeight: FontWeight.w600,
                  //               color: AppColor.kPrimaryColor,
                  //             ),
                  //             IconButton.filled(
                  //               onPressed: () {
                  //                 print(materialIncomingControllers
                  //                     .dataPreview);
                  //                 if (materialIncomingControllers
                  //                     .cartMaterialPO.isEmpty) {
                  //                   // Get.close(2);
                  //                   // Get.to(()=>AddMaterialIncoming(poId: poId))
                  //                 }
                  //               },
                  //               icon: const Icon(
                  //                 Icons.add_to_home_screen_sharp,
                  //                 color: AppColor.kPrimaryColor,
                  //               ),
                  //               style: FilledButton.styleFrom(
                  //                 backgroundColor: AppColor
                  //                     .kPrimaryColor
                  //                     .withOpacity(0.3),
                  //               ),
                  //             )
                  //           ],
                  //         ),
                  //       )
                  const Divider(),
                  if (isSave)
                    Obx(() {
                      return Button.filled(
                        onPressed: () =>
                            materialIncomingControllers.addMaterialIncomingPO(
                                loadingCtx: context,
                                extendData:
                                    imageUploadControllers.photoUploadData),
                        disabled: materialIncomingControllers.isLoading.value ||
                            materialIncomingControllers.cartMaterialPO.isEmpty,
                        color: AppColor.kPrimaryColor,
                        // height: 45,
                        // width: double.infinity,
                        icon: const Icon(
                          Icons.add_circle,
                          color: AppColor.kWhiteColor,
                        ),
                        // textStyle: BaseTextStyle.customTextStyle(
                        //   kfontWeight: FontWeight.w600,
                        //   kfontSize: 12,
                        //   kcolor: AppColor.kWhiteColor,
                        // ),
                        label: "Simpan",
                      );
                    }),
                ],
              ),
            );
          });
    }

    final submitAction = Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Obx(() => Directionality(
                    textDirection: TextDirection.rtl,
                    child: Button.filled(
                      onPressed: () {
                        if (materialIncomingControllers
                                .fkAddMaterialIncoming.currentState
                                ?.validate() ??
                            false) {
                          _showDialogBuilder(context, true);
                        }
                      },
                      disabled:
                          materialIncomingControllers.cartMaterialPO.isEmpty,
                      color: AppColor.kPrimaryColor,
                      // height: 50,
                      icon: const Icon(
                        Icons.arrow_circle_right,
                        color: AppColor.kWhiteColor,
                      ),
                      // rightIcon: true,
                      // textStyle: BaseTextStyle.customTextStyle(
                      //   kfontWeight: FontWeight.w600,
                      //   kfontSize: 12,
                      //   kcolor: AppColor.kWhiteColor,
                      // ),
                      label: "Selanjutnya",
                    ),
                  )),
            ),
          ],
        ),
      ),
    );

    final topContent = Container(
      height: 190,
      width: double.infinity,
      padding: const EdgeInsets.only(right: 20, left: 20, bottom: 20),
      decoration: const BoxDecoration(
        color: AppColor.kPrimaryColor,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: AppColor.kWhiteColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: const Icon(
              Icons.fire_truck_rounded,
            ),
          ),
          const SizedBox(width: 15),
          Obx(() {
            var dataPO = materialIncomingControllers.dataPreview;
            return Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  BaseTextWidget.customText(
                    text: dataPO['order_number'],
                    fontSize: 13,
                    color: AppColor.kWhiteColor,
                    fontWeight: FontWeight.w600,
                    isLongText: true,
                  ),
                  BaseTextWidget.customText(
                    text: dataPO['vendor_name'],
                    color: AppColor.kWhiteColor,
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                    isLongText: true,
                  ),
                ],
              ),
            );
          }),
        ],
      ),
    );

    void showImagePickerOption() {
      showModalBottomSheet(
          backgroundColor: AppColor.kGreyColor.withOpacity(0.1),
          isDismissible: false,
          context: context,
          builder: (builder) {
            return Padding(
              padding: const EdgeInsets.all(18.0),
              child: Container(
                padding: const EdgeInsets.all(15.0),
                width: MediaQuery.of(context).size.width,
                // height: MediaQuery.of(context).size.height / 4.5,
                decoration: BoxDecoration(
                  color: AppColor.kWhiteColor,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      'Ambil Gambar Dari',
                      style: BaseTextStyle.customTextStyle(
                        kfontSize: 12,
                        kfontWeight: FontWeight.w500,
                        // kcolor: Base
                      ),
                    ),
                    const SizedBox(height: 10),
                    Obx(() => Row(
                          children: [
                            Expanded(
                              child: imageUploadControllers.isLoading.value
                                  ? Center(
                                      child: Column(
                                        children: [
                                          const Icon(
                                            Icons.image,
                                            size: 70,
                                            color: AppColor.kGreyColor,
                                          ),
                                          BaseTextWidget.customText(
                                            text: 'Galeri',
                                            fontSize: 12,
                                            fontWeight: FontWeight.w600,
                                            color: AppColor.kGreyColor,
                                          ),
                                        ],
                                      ),
                                    )
                                  : InkWell(
                                      onTap: () {
                                        imageUploadControllers.uploadPhoto(
                                          pickedGalleryType: true,
                                        );

                                        // _pickImageFromGallery();
                                        // Navigator.of(context).pop();
                                        // Get.back();
                                      },
                                      child: SizedBox(
                                        child: Column(
                                          children: [
                                            const Icon(
                                              Icons.image,
                                              size: 70,
                                            ),
                                            Text(
                                              "Galeri",
                                              style:
                                                  BaseTextStyle.customTextStyle(
                                                kfontSize: 12,
                                                kfontWeight: FontWeight.w600,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                            ),
                            Expanded(
                              child: imageUploadControllers.isLoading.value
                                  ? Center(
                                      child: Column(
                                        children: [
                                          const Icon(
                                            Icons.camera_alt_rounded,
                                            size: 70,
                                            color: AppColor.kGreyColor,
                                          ),
                                          BaseTextWidget.customText(
                                            text: 'Kamera',
                                            fontSize: 12,
                                            fontWeight: FontWeight.w600,
                                            color: AppColor.kGreyColor,
                                          ),
                                        ],
                                      ),
                                    )
                                  : InkWell(
                                      onTap: () {
                                        imageUploadControllers.uploadPhoto(
                                          pickedGalleryType: false,
                                        );
                                        // _pickImageFromCamera();

                                        // Get.back(); //
                                      },
                                      child: SizedBox(
                                        child: Column(
                                          children: [
                                            const Icon(
                                              Icons.camera_alt,
                                              size: 70,
                                            ),
                                            Text(
                                              "Kamera",
                                              style:
                                                  BaseTextStyle.customTextStyle(
                                                kfontSize: 12,
                                                kfontWeight: FontWeight.w600,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                            ),
                          ],
                        )),
                  ],
                ),
              ),
            );
          });
    }

    final detailPayment = Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
      margin: const EdgeInsets.only(right: 10, left: 10, top: 55, bottom: 10),
      decoration: BoxDecoration(
        color: AppColor.kWhiteColor,
        borderRadius: BorderRadius.circular(8),
        border: Border.all(color: AppColor.kGreyColor),
      ),
      child: Column(
        children: [
          FormBuilder(
            key: materialIncomingControllers.fkAddMaterialIncoming,
            onChanged: () {
              materialIncomingControllers.fkAddMaterialIncoming.currentState!
                  .save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomTextField.style2(
                    title: 'No. Docket',
                    hintText: "Ketik No.Docket disini..",
                    isRequired: true,
                    controller:
                        materialIncomingControllers.docketNumberController,
                    fieldName: 'docket_number',
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Nomor docket wajib diisi!';
                      }

                      return null;
                    },
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    // inputFormater: <TextInputFormatter>[
                    // FilteringTextInputFormatter.allow(RegExp(r'^[0-9.]+$')),
                    // ],
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.formDatePicker2(
                    context: context,
                    title: "Tanggal Masuk",
                    hintText: "Pilih tanggal",
                    isRequired: true,
                    selectDateController:
                        materialIncomingControllers.dateControllers,
                    onChange: ({dateValue}) {},
                    fieldName: "date",
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Tanggal masuk wajib dipilih!';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.timePicker(
                    context: context,
                    isRequired: true,
                    title: "Jam Masuk",
                    hintText: 'Pilih jam',
                    valueController:
                        materialIncomingControllers.timeControllers,
                    // onChange: ({dateValue}) {},
                    fieldName: "time",
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Jam wajib dipilih!';
                      }
                      return null;
                    },
                  ),
                  const Divider(height: 20.0),
                  BaseTextWidget.customText(
                    text: "DATA PENGIRIM",
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Armada',
                    hintText: "Ketik Armada disini..",
                    controller: materialIncomingControllers.armadaController,
                    fieldName: 'fleet_name',
                    onChange: ({value}) {},
                    validator: (value) {
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    // inputFormater: <TextInputFormatter>[
                    // FilteringTextInputFormatter.allow(RegExp(r'^[0-9.]+$')),
                    // ],
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'No.Polisi',
                    hintText: "Ketik No.Polisi disini..",
                    isRequired: true,
                    fieldName: 'fleet_number',
                    controller: materialIncomingControllers.noPolisiController,
                    onChange: ({value}) {},
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'No. polisi wajib diisi!';
                      }

                      return null;
                    },
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    inputFormater: <TextInputFormatter>[
                      // FilteringTextInputFormatter.allow(RegExp(r'^[0-9.]+$')),
                      UppercaseInputFormatter()
                    ],
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Sopir',
                    hintText: "Ketik Sopir disini..",
                    controller: materialIncomingControllers.driverController,
                    fieldName: 'driver_name',
                    onChange: ({value}) {},
                    validator: (value) {
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                    inputFormater: <TextInputFormatter>[
                      UppercaseInputFormatter()
                    ],
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'No.Hp Sopir',
                    hintText: "Ketik No.Hp Sopir disini..",
                    fieldName: 'driver_phone',
                    controller: materialIncomingControllers.noDriverController,
                    onChange: ({value}) {},
                    validator: (value) {
                      return null;
                    },
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next,
                    inputFormater: <TextInputFormatter>[
                      FilteringTextInputFormatter.allow(RegExp(r'^[0-9.]+$')),
                    ],
                  ),
                  const SizedBox(height: 5),
                  CustomTextField.style2(
                    title: 'Catatan',
                    hintText: "Ketik catatan disini...",
                    fieldName: 'description',
                    isLongText: true,
                    // onClearText: () => fuelUsageControllers
                    //     .fkAddStockTake.currentState!
                    //     .patchValue({'description': null}),
                    onChange: ({value}) {},
                    validator: (value) {
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.done,
                  ),
                  const SizedBox(height: 8),
                  Container(
                    padding: const EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      border: Border.all(color: AppColor.kGreyColor),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: () => showImagePickerOption(),
                          child: Obx(() => Container(
                                padding: imageUploadControllers
                                        .photoUploadData.isEmpty
                                    ? const EdgeInsets.all(20)
                                    : const EdgeInsets.all(0),
                                decoration: BoxDecoration(
                                    color: AppColor.kSoftGreyColor,
                                    borderRadius: BorderRadius.circular(5)),
                                child: imageUploadControllers
                                        .photoUploadData.isEmpty
                                    ? const Icon(
                                        Icons.camera_alt,
                                      )
                                    : Image.network(
                                        imageUploadControllers
                                            .photoUploadData['preview'],
                                        fit: BoxFit.cover,
                                        height: 60,
                                        width: 60,
                                        loadingBuilder: (_, child, chunk) =>
                                            chunk != null
                                                ? Shimmer.fromColors(
                                                    baseColor:
                                                        AppColor.kSoftGreyColor,
                                                    highlightColor:
                                                        AppColor.kWhiteColor,
                                                    child: Container(
                                                      height: 60,
                                                      width: 60,
                                                      decoration: BoxDecoration(
                                                        color: Colors.amber,
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(5),
                                                      ),
                                                    ),
                                                  )
                                                : child,
                                      ),
                              )),
                        ),
                        const SizedBox(width: 5),
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                child: BaseTextWidget.customText(
                                  text:
                                      "Klik gambar untuk mengunggah. Maksimal Ukuran 1 Mb",
                                  isLongText: true,
                                ),
                              ),
                              Obx(
                                () => imageUploadControllers
                                        .photoUploadData.isEmpty
                                    ? const SizedBox()
                                    : ConstrainedBox(
                                        constraints:
                                            const BoxConstraints.tightFor(
                                                height: 25),
                                        child: ElevatedButton.icon(
                                          onPressed: () {
                                            imageUploadControllers
                                                .photoEditingController
                                                .clear();
                                            imageUploadControllers
                                                .photoUploadData
                                                .clear();
                                          },
                                          icon: const Icon(
                                            Icons.close,
                                            size: 12,
                                            color: AppColor.kWhiteColor,
                                          ),
                                          label: BaseTextWidget.customText(
                                            text: "Hapus",
                                            color: AppColor.kWhiteColor,
                                          ),
                                          style: ElevatedButton.styleFrom(
                                            backgroundColor:
                                                AppColor.kErrorColor,
                                          ),
                                        ),
                                      ),
                              ),
                              // ElevatedButton.icon(
                              //   onPressed: () {
                              //     print(imageUploadControllers
                              //         .photoEditingController.text);
                              //     print(imageUploadControllers.photoUploadData);
                              //   },
                              //   icon: const Icon(
                              //     Icons.close,
                              //     size: 12,
                              //     color: AppColor.kWhiteColor,
                              //   ),
                              //   label: BaseTextWidget.customText(
                              //     text: "Hapus",
                              //     color: AppColor.kWhiteColor,
                              //   ),
                              //   style: ElevatedButton.styleFrom(
                              //     backgroundColor: AppColor.kErrorColor,
                              //   ),
                              // )
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        context: context,
        title: 'Tambah Penerimaan Material',
        extendBack: () {
          // salesController.dataLookupCustomerProject.clear();
          // salesController.dataDetailCustomer.clear();
        },
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            topContent,
            const SizedBox(height: 10),
            detailPayment,
          ],
        ),
      ),
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: submitAction,
    );
  }
}
