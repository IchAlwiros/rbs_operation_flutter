import 'package:rbs_mobile_operation/data/datasources/materialincoming.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';
import 'package:rbs_mobile_operation/view/app_view_resource.dart';
import 'package:rbs_mobile_operation/view/material_incoming/create/new_material_incoming.dart';

class MaterialIncomingList extends StatelessWidget {
  const MaterialIncomingList({super.key});

  @override
  Widget build(BuildContext context) {
    MaterialIncomingListControllers materialIncomigControllers =
        Get.put(MaterialIncomingListControllers());

    Future onRefresh() async {
      materialIncomigControllers.refreshMaterialIncoming();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        if (materialIncomigControllers.dataLookupMaterial.isNotEmpty) {
          materialIncomigControllers.fetchmaterialUsage();
        }
      }
    });

    // ** SHOW FILTER MODAL SHEET

    TextEditingController materialFilterController = TextEditingController();
    TextEditingController warehouseFilterController = TextEditingController();

    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          materialIncomigControllers.currentFilter.clear();
          materialIncomigControllers.filterFormKey.currentState!.reset();
          materialIncomigControllers.filterFormKey.currentState!.patchValue({
            'material_id': null,
            'warehouse_id': null,
          });
        },
        onFilter: () {
          Get.back();
          materialIncomigControllers.refreshMaterialIncoming();
        },
        valueFilter: materialIncomigControllers.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: materialIncomigControllers.filterFormKey,
            onChanged: () {
              materialIncomigControllers.filterFormKey.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'material_id',
                    controller: materialFilterController,
                    currentValue: materialIncomigControllers
                        .currentFilter['material_filter'],
                    label: "Material",
                    hintText: "Pilih material",
                    asyncItems: (String filter) async {
                      materialIncomigControllers.lookupMaterial(
                          searching: filter);
                      return materialIncomigControllers.dataLookupMaterial;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      // print(data);
                      materialIncomigControllers.currentFilter.value = {
                        ...materialIncomigControllers.currentFilter,
                        'material_filter': searchValue,
                      };
                    },
                  ),
                  const SizedBox(height: 5),
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'warehouse_id',
                    controller: warehouseFilterController,
                    currentValue: materialIncomigControllers
                        .currentFilter['warehouse_filter'],
                    label: "Gudang/Lokasi Penyimpanan",
                    hintText: "Pilih gudang",
                    asyncItems: (String filter) async {
                      materialIncomigControllers.lookupWarehouse(
                          searching: filter);
                      return materialIncomigControllers.dataLookupWarehouse;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      // print(data);
                      materialIncomigControllers.currentFilter.value = {
                        ...materialIncomigControllers.currentFilter,
                        'warehouse_filter': searchValue,
                      };
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    final listItemContent = Flexible(
      child: Obx(
        () {
          final result = materialIncomigControllers.materialIncomingData;
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: result.isEmpty &&
                    materialIncomigControllers.isLoading.isFalse
                ? SizedBox(
                    height: MediaQuery.of(context).size.height / 1.2,
                    child: Center(
                      child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                    ),
                  )
                : ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: result.length + 1,
                    separatorBuilder: (BuildContext context, int index) =>
                        const SizedBox(height: 5),
                    itemBuilder: (BuildContext context, int index) {
                      if (index < result.length) {
                        final data = result[index];

                        var colorBadge = BaseGlobalFuntion.statusColor(
                            result[index]['status_code']);
                        return BaseCardWidget.cardStyle4(
                          ontap: () {
                            Get.to(
                              () => IncomingMaterialView(itemId: data['id']),
                            );
                          },
                          title: data['material_name'],
                          subtitle: data['docket_number'],
                          bottomSubtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              BaseTextWidget.customText(
                                text: data['order_number'],
                                isLongText: true,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                              ),
                              BaseTextWidget.customText(
                                text: data['production_unit_name'],
                                isLongText: true,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                              ),
                            ],
                          ),
                          bottomSubtitle2: BaseTextWidget.customText(
                            text: BaseGlobalFuntion.datetimeConvert(
                                data['date'], "dd MMM yyyy  HH:mm"),
                          ),
                          trailling: BaseTextWidget.customText(
                            text: data['quantity'],
                            extendText: "\n ${data['uom_name']}",
                            textAlign: TextAlign.right,
                            fontWeight: FontWeight.w600,
                          ),
                          extendWidget: Container(
                            padding: const EdgeInsets.all(4),
                            decoration: BoxDecoration(
                              color: colorBadge['background_color'],
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Text(
                              data['status_code'],
                              style: BaseTextStyle.customTextStyle(
                                kfontSize: 12,
                                kcolor: colorBadge['text_color'],
                                kfontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        );
                      } else if (result.length < 25) {
                        return materialIncomigControllers.hasMore.value &&
                                materialIncomigControllers.isLoading.isTrue
                            ? BaseLoadingWidget.cardShimmer(
                                height: MediaQuery.of(context).size.height,
                                shimmerheight: 80,
                                baseColor: AppColor.kSoftGreyColor,
                                highlightColor: AppColor.kWhiteColor,
                                itemCount: 25,
                              )
                            : const SizedBox();
                      } else {
                        return materialIncomigControllers.hasMore.value
                            ? BaseLoadingWidget.cardShimmer(
                                height: 200,
                                shimmerheight: 90,
                                baseColor: AppColor.kSoftGreyColor,
                                highlightColor: AppColor.kWhiteColor,
                                itemCount: 2,
                              )
                            : const SizedBox();
                      }
                    },
                  ),
          );
        },
      ),
    );

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
            title: 'Penerimaan Material',
            context: context,
            isAllFill: true,
            searchController: materialIncomigControllers.search,
            searchText: 'Cari',
            onFillterChange: () {
              showBottomFilter(context);
            },
            onSearchChange: () {
              BaseGlobalFuntion.debounceRun(
                action: () {
                  materialIncomigControllers.refreshMaterialIncoming();
                },
                duration: const Duration(milliseconds: 1000),
              );
            }),
        body: SizedBox(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              RefreshIndicator(
                onRefresh: onRefresh,
                child: SingleChildScrollView(
                  controller: scrollController,
                  physics: const AlwaysScrollableScrollPhysics(),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const SizedBox(height: 10),
                      listItemContent,
                    ],
                  ),
                ),
              ),
              Positioned(
                bottom: 30.0,
                right: 15.0,
                child: SizedBox(
                  height: 55.0,
                  width: 55.0,
                  child: IconButton.filled(
                    iconSize: 30,
                    onPressed: () {
                      Get.to(() => const CollectMaterialIncoming());
                    },
                    icon: const Icon(
                      Icons.add,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
