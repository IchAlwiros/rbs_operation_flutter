// MAIN VIEW MODULE
export 'package:rbs_mobile_operation/view/trialpage/login2.dart';
export 'package:rbs_mobile_operation/view/spalsh_screen.dart';

// INDEX NAVIGATION
export 'package:rbs_mobile_operation/view/index.dart';
export 'package:rbs_mobile_operation/view/notification/notif.dart';

// SALES
export 'package:rbs_mobile_operation/view/sales/sales_order.dart';
export 'package:rbs_mobile_operation/view/customer/customer.dart';
export 'package:rbs_mobile_operation/view/project/project.dart';
export 'package:rbs_mobile_operation/view/kredit&aproval/kredit_&_aproval.dart';
export 'package:rbs_mobile_operation/view/kredit&aproval/pages/kredits.dart';
export 'package:rbs_mobile_operation/view/kredit&aproval/pages/approvals.dart';
export 'package:rbs_mobile_operation/view/payment/payment.dart';
export 'package:rbs_mobile_operation/view/product/product.dart';
export 'package:rbs_mobile_operation/view/production/production.dart';
export 'package:rbs_mobile_operation/view/production_scedule/production_schedule.dart';
export 'package:rbs_mobile_operation/view/customer/detail/view_customer.dart';
export 'package:rbs_mobile_operation/view/sales/create/add_cart_order.dart';
export 'package:rbs_mobile_operation/view/sales/detail/view_sales_order.dart';
export 'package:rbs_mobile_operation/view/sales/create/add_sales_order.dart';
export 'package:rbs_mobile_operation/view/purchase_order/detail/view_purchase_order.dart';
export 'package:rbs_mobile_operation/view/project/detail/view_project.dart';
export 'package:rbs_mobile_operation/view/production_scedule/detail/view_production_schedule.dart';
export 'package:rbs_mobile_operation/view/production/detail/view_production.dart';
export 'package:rbs_mobile_operation/view/product/detail/view_product.dart';
export 'package:rbs_mobile_operation/view/material_incoming/detail/view_material_income.dart';

// LOGISTIK
export 'package:rbs_mobile_operation/view/material_incoming/material_incoming.dart';
export 'package:rbs_mobile_operation/view/material_usage/material_usage.dart';
export 'package:rbs_mobile_operation/view/fuel_usage/fuelusage.dart';
export 'package:rbs_mobile_operation/view/material/material.dart';
export 'package:rbs_mobile_operation/view/purchase_order/purchase_order.dart';
export 'package:rbs_mobile_operation/view/material_stock/stockreport.dart';
export 'package:rbs_mobile_operation/view/stocktake/stocktake.dart';
