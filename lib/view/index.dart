import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:rbs_mobile_operation/data/datasources/global/geolocator.dart';
import 'package:rbs_mobile_operation/data/datasources/home.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/view/home/home.dart';
import 'package:rbs_mobile_operation/view/profile/profile.dart';

class IndexNavigation extends StatelessWidget {
  const IndexNavigation({super.key});

  @override
  Widget build(BuildContext context) {
    // LocationCoordinatesControllers locationCoordinatesControllers =
    Get.put(LocationCoordinatesControllers());

    PersistentTabController controller;
    controller = PersistentTabController(initialIndex: 0);

    DateTime timeBackPassed = DateTime.now();

    return PopScope(
      canPop: false,
      onPopInvoked: (didPop) {
        print('INI DID POP $didPop');
        showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8.0))),
              title: Row(
                children: [
                  const Icon(
                    Icons.warning,
                    color: AppColor.kErrorColor,
                  ),
                  const SizedBox(width: 10),
                  BaseTextWidget.customText(
                    text: "Apakah anda ingin keluar dari aplikasi?",
                    fontSize: 15,
                  ),
                ],
              ),
              actions: [
                Row(
                  children: [
                    Expanded(
                      child: Button.filled(
                        height: 50,
                        fontSize: 14,
                        color: AppColor.kGreyColor,
                        textColor: AppColor.kBlackColor.withOpacity(0.5),
                        onPressed: () async {
                          Get.close(1);
                        },
                        label: "Cancel",
                      ),
                    ),
                    const SizedBox(width: 5),
                    Expanded(
                      child: Button.filled(
                        height: 50,
                        fontSize: 14,
                        color: AppColor.kErrorColor,
                        onPressed: () async {
                          // final difference =
                          //     DateTime.now().difference(timeBackPassed);
                          // final isExitWarning = difference >= Duration(seconds: 2);

                          // timeBackPassed = DateTime.now();

                          // if (isExitWarning) {
                          //   final message = 'Press back again to exit';
                          //   // Flutter
                          //   return false;
                          // }
                          SystemNavigator.pop();
                        },
                        label: "Tutup App",
                      ),
                    ),
                  ],
                ),
              ],
            );
          },
        );
      },
      child: PersistentTabView(
        context,
        navBarHeight: 70,
        controller: controller,
        screens: _buildScreens(context),
        items: _navBarsItems(),
        confineInSafeArea: true,
        backgroundColor: AppColor.kWhiteColor, // Default is Colors.white.
        handleAndroidBackButtonPress: true, // Default is true.
        hideNavigationBar: false,
        resizeToAvoidBottomInset:
            true, // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
        stateManagement: true, // Default is true.
        hideNavigationBarWhenKeyboardShows:
            true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
        decoration: const NavBarDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15),
              topRight: Radius.circular(15),
            ),
            colorBehindNavBar: AppColor.kWhiteColor,
            boxShadow: [
              BoxShadow(
                offset: Offset(3.0, 2.0),
                blurRadius: 20,
                color: AppColor.kGreyColor,
              )
            ]),
        popAllScreensOnTapOfSelectedTab: true,
        popAllScreensOnTapAnyTabs: true,
        popActionScreens: PopActionScreensType.all,
        itemAnimationProperties: const ItemAnimationProperties(
          // Navigation Bar's items animation properties.
          duration: Duration(milliseconds: 200),
          curve: Curves.ease,
        ),
        screenTransitionAnimation: const ScreenTransitionAnimation(
          // Screen transition animation on change of selected tab.
          animateTabTransition: true,
          curve: Curves.ease,
          duration: Duration(milliseconds: 500),
        ),
        navBarStyle:
            NavBarStyle.style13, // Choose the nav bar style with this property.
        onItemSelected: (value) {},
      ),
    );
  }

  List<Widget> _buildScreens(BuildContext context) {
    return [
      // HomePage(
      //   contextIndex: context,
      // ),
      // const Profile(),
      buildNoInternetConnection(context, HomePage(contextIndex: context)),
      buildNoInternetConnection(context, const Profile())
    ];
  }

  Widget buildNoInternetConnection(BuildContext context, Widget pages) {
    HomeControllers homeControllers = Get.put(HomeControllers());

    return Obx(
      () => homeControllers.isInternetConnect.value
          ? pages
          : Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 180,
                    height: 180,
                    child: Assets.images.b.lottie(),
                    //  Lottie.asset('assets/images/b.json'),
                  ),
                  homeControllers.isLoading.value
                      ? BaseLoadingWidget.circularProgress(
                          width: 20,
                          height: 20,
                          color: AppColor.kPrimaryColor,
                        )
                      : MaterialButton(
                          minWidth: 130,
                          height: 45,
                          onPressed: () async {
                            homeControllers.isLoading.value = true;
                            if (await InternetConnectionChecker()
                                    .hasConnection ==
                                true) {
                              homeControllers.isInternatConnect();
                            } else {
                              BaseInfo.log(
                                isError: true,
                                messageTitle: "Sambungkan ke Internet",
                                message: "internet tidak terhubung",
                                snackPosition: SnackPosition.TOP,
                              );
                            }
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          color: AppColor.kPrimaryColor,
                          child: BaseTextWidget.customText(
                            text: "Coba Lagi",
                            color: AppColor.kWhiteColor,
                            fontWeight: FontWeight.w600,
                          ),
                        )
                ],
              ),
            ),
    );
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: const Icon(Icons.dashboard_rounded),
        title: ("Home"),
        activeColorPrimary: AppColor.kPrimaryColor,
        inactiveColorPrimary: AppColor.kInactiveColor,
      ),
      // PersistentBottomNavBarItem(
      //   icon: const Icon(Icons.stacked_bar_chart_outlined),
      //   title: ("Report"),
      //   activeColorPrimary: AppColor.kPrimaryColor,
      //   inactiveColorPrimary: AppColor.kInactiveColor,
      // ),
      PersistentBottomNavBarItem(
        icon: const Icon(Icons.person_2_rounded),
        title: ("Profile"),
        activeColorPrimary: AppColor.kPrimaryColor,
        inactiveColorPrimary: AppColor.kInactiveColor,
      ),
    ];
  }
}
