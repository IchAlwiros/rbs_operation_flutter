import 'package:rbs_mobile_operation/core/core.dart';

class NotificationScreen extends StatelessWidget {
  const NotificationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final listCardContent = ListView.separated(
      itemCount: 10,
      separatorBuilder: (BuildContext context, int index) =>
          const SizedBox(height: 10),
      itemBuilder: (BuildContext context, int index) =>
          BaseCardWidget.cardStyle2(
        title: "title",
        subTitle1: "subtitle1",
        subTitle2: "subtitle2",
        headerString: "header",
      ),
    );

    return Scaffold(
      appBar: BaseExtendWidget.appBar(
        title: 'Notifikasi',
        context: context,
      ),
      body: listCardContent,
    );
  }
}
