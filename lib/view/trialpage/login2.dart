import 'package:rbs_mobile_operation/data/datasources/auth.controllers.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Login extends StatelessWidget {
  const Login({super.key});

  @override
  Widget build(BuildContext context) {
    final AuthControllers authControllers = Get.put(AuthControllers());

    var usernameHasError = false.obs;
    var passwordHasError = false.obs;

    Widget formLogin() {
      return Padding(
        padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
        child: FormBuilder(
          key: authControllers.authFormKey,
          onChanged: () {
            authControllers.authFormKey.currentState!.save();
            debugPrint(
              authControllers.authFormKey.currentState!.value.toString(),
            );
          },
          autovalidateMode: AutovalidateMode.disabled,
          skipDisabled: true,
          child: Obx(() => Column(
                children: [
                  // ElevatedButton(
                  //     onPressed: () {
                  //       authControllers.authFormKey.currentState!
                  //           .patchValue({'username': null});
                  //     },
                  //     child: Text("data")),
                  BaseTextFieldWidget.textFieldFormBuilder2(
                    title: 'Username/Email',
                    fieldName: 'username',
                    hintText: "pengguna@rbs.com",
                    isRequired: true,
                    onClearText: () => authControllers.authFormKey.currentState!
                        .patchValue({'username': null}),
                    radius: 10,
                    suffixOnError: usernameHasError.value,
                    onChange: ({value}) {
                      usernameHasError.value = !(authControllers
                              .authFormKey.currentState?.fields['username']
                              ?.validate() ??
                          false);
                    },
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'username wajib diisi!';
                      }

                      return null;
                    },
                    keyboardType: TextInputType.text,
                    textInputAction: TextInputAction.next,
                  ),
                  const SizedBox(height: 5),
                  BaseTextFieldWidget.textFieldFormBuilder2(
                    title: 'Password',
                    fieldName: 'password',
                    hintText: "*******",
                    isRequired: true,
                    secureViewText: true,
                    radius: 10,
                    suffixOnError: passwordHasError.value,
                    onChange: ({value}) {
                      passwordHasError.value = !(authControllers
                              .authFormKey.currentState?.fields['password']
                              ?.validate() ??
                          false);
                    },
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'password wajib diisi!';
                      }

                      return null;
                    },
                    keyboardType: TextInputType.visiblePassword,
                    textInputAction: TextInputAction.done,
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextButton(
                          onPressed: () async {
                            await AuthControllers.signOut();
                            // final box = GetStorage();
                            // print(box.read('token'));
                            // print(box.read('profile'));
                            // print(box.read('permissions'));

                            // print(BaseGlobalFuntion.hasPermission(permission));
                          },
                          child: Text(
                            "Lupa Password?",
                            style: BaseTextStyle.customTextStyle(
                              kfontSize: 12,
                              kfontWeight: FontWeight.w500,
                              kcolor: AppColor.kPrimaryColor,
                            ),
                          )),
                      BaseButtonWidget.primaryIconButton(
                        onPressed: () async {
                          await authControllers.loginAuth();
                        },
                        isLoading: authControllers.isLoading.value,
                        title: "Login",
                        height: 50,
                        textStyle: BaseTextStyle.customTextStyle(
                          kcolor: AppColor.kWhiteColor,
                          kfontSize: 12,
                          kfontWeight: FontWeight.w600,
                        ),
                        kcolor: AppColor.kPrimaryColor,
                        iconData: Icons.login_rounded,
                        rounded: 10,
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Expanded(
                            child: Padding(
                          padding: EdgeInsets.only(right: 5.0),
                          child: Divider(height: 20.0, thickness: 1.5),
                        )),
                        Text("Atau",
                            style: BaseTextStyle.customTextStyle(
                              kfontSize: 12,
                              kfontWeight: FontWeight.w400,
                              kcolor: AppColor.kPrimaryColor,
                            )),
                        const Expanded(
                            child: Padding(
                          padding: EdgeInsets.only(left: 5.0),
                          child: Divider(height: 20.0, thickness: 1.5),
                        ))
                      ],
                    ),
                  ),
                  BaseButtonWidget.primaryIconButton(
                    onPressed: () async {
                      // await authControllers.loginAuth();
                      final user = await authControllers.loginWithGoogle();
                      // await AuthControllers.signOut();

                      try {
                        if (kDebugMode) {
                          // print("controller");
                          // print(user);
                        }

                        if (user != null && context.mounted) {
                          // print(user);
                          // Navigator.pushReplacement(
                          //   context,
                          //   MaterialPageRoute(
                          //       builder: (context) =>
                          //           const ),
                          // );
                        }
                      } on FirebaseException catch (error) {
                        if (kDebugMode) {
                          print("INI ERROR ON FIREBASE EXCEPTION");
                          print(error.message);
                        }
                      } catch (e) {
                        if (kDebugMode) {
                          print("INI ERROR ON CATCH");
                          print(e);
                        }
                      }
                    },
                    isLoading: authControllers.isLoading.value,
                    title: "Masuk dengan Google",
                    height: 50,
                    width: double.infinity,
                    textStyle: BaseTextStyle.customTextStyle(
                      kcolor: AppColor.kPrimaryColor,
                      kfontSize: 12,
                      kfontWeight: FontWeight.w500,
                    ),
                    rounded: 10,
                    kcolor: AppColor.kWhiteColor,
                    // assetIcon: "assets/icons/google_auth.png",
                  )
                ],
              )),
        ),
      );
    }

    Widget contentTitle() {
      return Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.all(10),
        width: double.infinity,
        decoration: BoxDecoration(
          color: AppColor.kWhiteColor.withOpacity(0.92),
          borderRadius: BorderRadius.circular(18),
          boxShadow: BaseShadowStyle.customBoxshadow,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                      text: "Selamat Datang!\n",
                      style: BaseTextStyle.customTextStyle(
                        letterSpacing: 1.0,
                        kcolor: AppColor.kPrimaryColor,
                        kfontWeight: FontWeight.w700,
                        kfontSize: 16,
                      )),
                  TextSpan(
                      text: "Masuk untuk melanjutkan",
                      style: BaseTextStyle.customTextStyle(
                        kcolor: AppColor.kPrimaryColor,
                        kfontWeight: FontWeight.w400,
                        kfontSize: 12,
                      )),
                ],
              ),
            ),
            const SizedBox(height: 10),
            formLogin(),
          ],
        ),
      );
    }

    return SafeArea(
        child: GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        body: Stack(
          children: [
            Assets.images.loginBg.image(
              height: double.infinity,
              width: double.infinity,
              fit: BoxFit.cover,
            ),
            // Image.asset(
            //   'assets/images/loginBg.png',
            //   height: double.infinity,
            //   width: double.infinity,
            //   fit: BoxFit.cover,
            // ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 28.0,
                vertical: 33.0,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Assets.icons.logo.image(width: 200),
                  // Image.asset(
                  //   'assets/icons/logo.png',
                  //   width: 200,
                  // ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 60.0,
                    ),
                    child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                              text: "Kelola ",
                              style: BaseTextStyle.customTextStyle(
                                kcolor: AppColor.kWhiteColor,
                                kfontWeight: FontWeight.w700,
                                kfontSize: 22,
                              )),
                          TextSpan(
                              text: "Order & Jadwal",
                              style: BaseTextStyle.customTextStyle(
                                kcolor: AppColor.kWhiteColor,
                                kfontWeight: FontWeight.w500,
                                kfontSize: 20,
                              )),
                          TextSpan(
                              text: " \nLebih Efisien",
                              style: BaseTextStyle.customTextStyle(
                                kcolor: AppColor.kWhiteColor,
                                kfontWeight: FontWeight.w700,
                                kfontSize: 22,
                              )),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),

            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                contentTitle(),
              ],
            ),

            // Column(
            //   mainAxisAlignment: MainAxisAlignment.end,
            //   mainAxisSize: MainAxisSize.min,
            //   children: [
            //     contentTitle(),
            //   ],
            // ),
          ],
        ),
      ),
    ));
  }
}
