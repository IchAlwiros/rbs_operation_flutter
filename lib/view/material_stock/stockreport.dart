import 'package:rbs_mobile_operation/data/datasources/stockreport.dart';
import 'package:rbs_mobile_operation/core/core.dart';
import 'package:rbs_mobile_operation/core/extentions/global_function.dart';

class MaterialStockList extends StatelessWidget {
  const MaterialStockList({super.key});

  @override
  Widget build(BuildContext context) {
    StockReportListControllers stockReportControllers =
        Get.put(StockReportListControllers());

    Future onRefresh() async {
      stockReportControllers.refreshMaterialAnalysis();
    }

    final ScrollController scrollController = ScrollController();
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.position.pixels) {
        if (stockReportControllers.materialAnalysis.isNotEmpty) {
          stockReportControllers.fetchMaterialAnalysis();
        }
      }
    });

    Widget cardPreview({
      required String title,
      backgroundColor,
      String? dataPreview,
      String? labelContent,
    }) =>
        SingleChildScrollView(
          physics: const NeverScrollableScrollPhysics(),
          child: Column(
            children: [
              Container(
                  width: double.infinity,
                  padding: const EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      color: backgroundColor,
                      border: Border.all(
                        color: AppColor.kPrimaryColor,
                        width: 2,
                      ),
                      borderRadius: BorderRadius.circular(5)),
                  child: Column(
                    children: [
                      Text(
                        dataPreview ?? "",
                        maxLines: 2,
                        textAlign: TextAlign.center,
                        style: BaseTextStyle.customTextStyle(
                          kfontSize: 12,
                          kfontWeight: FontWeight.w400,
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.amber,
                        ),
                        child: Text(
                          labelContent ?? "",
                          style: BaseTextStyle.customTextStyle(
                            kfontSize: 12,
                            kfontWeight: FontWeight.w400,
                          ),
                        ),
                      )
                    ],
                  )),
              Text(
                title,
                textAlign: TextAlign.center,
                style: BaseTextStyle.customTextStyle(
                  kfontSize: 12,
                  kfontWeight: FontWeight.w500,
                ),
              )
            ],
          ),
        );

    final listItemContent = Flexible(child: Obx(() {
      final result = stockReportControllers.materialAnalysis;

      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: result.isEmpty && stockReportControllers.isLoading.isFalse
            ? SizedBox(
                height: MediaQuery.of(context).size.height / 1.2,
                child: Center(
                  child: BaseLoadingWidget.noMoreDataWidget(height: 200),
                ),
              )
            : ListView.separated(
                // controller: scrollController,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: result.length + 1,
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(height: 5),
                itemBuilder: (BuildContext context, int index) {
                  if (index < result.length) {
                    final data = result[index];
                    return BaseCardWidget.cardStyle4(
                      ontap: () {},
                      title: data['material_name'],
                      subtitle: data['warehouse_name'],
                      bottomSubtitle: BaseTextWidget.customText(
                        text: data['production_unit_name'],
                        isLongText: true,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                      trailling: BaseTextWidget.customText(
                        text: BaseGlobalFuntion.currencyLocalConvert(
                          nominal: data['quantity'],
                          isLocalCurency: false,
                        ),
                        extendText: "\n ${data['uom_name']}",
                        textAlign: TextAlign.right,
                        fontWeight: FontWeight.w600,
                      ),
                    );
                  } else if (result.length < 25) {
                    return stockReportControllers.hasMore.value &&
                            stockReportControllers.isLoading.isTrue
                        ? BaseLoadingWidget.cardShimmer(
                            height: MediaQuery.of(context).size.height,
                            shimmerheight: 80,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 25,
                          )
                        : const SizedBox();
                  } else {
                    return stockReportControllers.hasMore.value
                        ? BaseLoadingWidget.cardShimmer(
                            height: 200,
                            shimmerheight: 90,
                            baseColor: AppColor.kSoftGreyColor,
                            highlightColor: AppColor.kWhiteColor,
                            itemCount: 2,
                          )
                        : const SizedBox();
                  }
                },
              ),
      );
    }));

    TextEditingController warehouseFilterController = TextEditingController();
    Future<void> showBottomFilter(BuildContext context) {
      return BaseExtendWidget.modalBottomFilter(
        context: context,
        onReset: () {
          stockReportControllers.currentFilter.clear();
          stockReportControllers.formKeyFilter.currentState!.reset();
          stockReportControllers.formKeyFilter.currentState!.patchValue({
            'warehouse': null,
          });
        },
        onFilter: () {
          Get.back();
          stockReportControllers.refreshMaterialAnalysis();
        },
        valueFilter: stockReportControllers.currentFilter,
        filterForm: Obx(
          () => FormBuilder(
            key: stockReportControllers.formKeyFilter,
            onChanged: () {
              stockReportControllers.formKeyFilter.currentState!.save();
            },
            autovalidateMode: AutovalidateMode.disabled,
            skipDisabled: true,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  BaseButtonWidget.dropdownSearch(
                    fieldName: 'warehouse',
                    controller: warehouseFilterController,
                    currentValue: stockReportControllers
                        .currentFilter['warehouse_filter'],
                    label: "Gudang/Lokasi Penyimpanan",
                    hintText: "Pilih Gudang/Lokasi Penyimpanan",
                    asyncItems: (String filter) async {
                      stockReportControllers.lookupWarehouse(searching: filter);
                      return stockReportControllers.dataLookupWarehouse;
                    },
                    onChange: ({searchValue, valueItem, data}) {
                      stockReportControllers.currentFilter.value = {
                        ...stockReportControllers.currentFilter,
                        'warehouse_filter': searchValue,
                      };
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }

    return SafeArea(
      child: Scaffold(
        appBar: BaseExtendWidget.appBar(
          title: 'Material Stock',
          context: context,
          isAllFill: true,
          searchController: stockReportControllers.search,
          searchText: 'Cari',
          onSearchChange: () {
            BaseGlobalFuntion.debounceRun(
              action: () {
                stockReportControllers.refreshMaterialAnalysis();
              },
              duration: const Duration(milliseconds: 1000),
            );
          },
          onFillterChange: () {
            showBottomFilter(context);
          },
        ),
        body: RefreshIndicator(
          onRefresh: onRefresh,
          child: SingleChildScrollView(
            controller: scrollController,
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(height: 10),
                listItemContent,
              ],
            ),
          ),
        ),
      ),
    );
  }
}
